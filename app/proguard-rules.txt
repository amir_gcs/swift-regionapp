-optimizations !class/unboxing/enum
#Crashlytics
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable
-keep public class * extends android.support.v4.** {*;}
-keep public class * extends android.app.Fragment

-keepclassmembers class * extends android.app.Application
-keepclassmembers class * extends android.app.Service
-keepclassmembers class * extends android.content.BroadcastReceiver
-keepclassmembers class * extends android.content.ContentProvider

#-keep class * implements com.goodcoresoftware.android.common.utils.ITPParcelable {
#  public static final android.os.Parcelable$Creator *;
#  public static final com.goodcoresoftware.android.common.utils.ITPParcelableCreator *;
#}

-dontwarn com.squareup.**
-dontwarn org.apache.**
-dontwarn com.parse.**
-dontwarn com.qozix.**

# Require, else gson won't be able in instantiate nested collections

-keep class org.apache.** { *; }
-keep class com.squareup.** { *; }
# ApiCall is instantiated via reflection, not keeping it means ApiService won't find the class
-keep class com.swift.SWIFTRegional.api.** { *; }
# Models may be instantiated via reflection (especially when Gson is used)
-keep class com.google.gson.** { *; }
-keepattributes Signature,*Annotation*
-keep class com.swift.SWIFTRegional.models.** { *; }
# Support library is not to be touched
-keep class android.support.** { *; }

-keep class org.codehaus.mojo.** { *; }
-keep interface org.codehaus.mojo.** { *; }
-dontwarn org.codehaus.mojo.**
-dontwarn okio.**

#retrofit
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**

-dontwarn rx.**
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

-dontwarn org.joda.time.**
-dontwarn org.xbill.**

# Quickblox
-keep class org.jivesoftware.smack.** { *; }
-keep class org.jivesoftware.smackx.** { *; }
-keep class com.quickblox.** { *; }
-keep class * extends org.jivesoftware.smack { *; }
-keep class * implements org.jivesoftware.smack.debugger.SmackDebugger { *; }

#Beacon SDK
-keep class mobi.inthepocket.beacons.** { *; }

#added for removing warnings

-dontwarn org.jivesoftware.smackx.**

-keep class com.android.volley.** { *; }
-dontwarn com.android.volley.**

