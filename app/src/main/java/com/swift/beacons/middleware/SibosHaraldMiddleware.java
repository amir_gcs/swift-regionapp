package com.swift.beacons.middleware;

import android.content.Context;

import com.goodcoresoftware.android.common.utils.StringUtils;
import mobi.inthepocket.beacons.sdk.api.models.Action;
import mobi.inthepocket.beacons.sdk.api.models.Beacon;
import mobi.inthepocket.beacons.sdk.api.models.Campaign;
import mobi.inthepocket.beacons.sdk.interfaces.ITPBeaconMiddleware;
import mobi.inthepocket.beacons.sdk.models.ActionValidationResult;

public class SibosHaraldMiddleware implements ITPBeaconMiddleware
{
    private final static String LOCATE_ME_URL_SCHEME = "sibos://locate.me/?code=";

    private static String locateMeBeaconCode = "";
    private static String locateMeMessage = "";
    private static Beacon locateMeBeacon = null;

    @Override
    public boolean shouldTriggerAction(Context context, Campaign campaign, Action action, Beacon beacon, ActionValidationResult actionValidationResult)
    {
        if(isLocateMeActionUrl(action.getUrl()))
        {
            final String beaconCode = parseBeaconCode(action.getUrl());

            if(!StringUtils.isEmpty(beaconCode))
            {
                locateMeBeacon = beacon;
                locateMeBeaconCode = beaconCode;
                locateMeMessage = action.getAlert();
            }
        }

        return true;
    }

    @Override
    public void didEnterBeaconRange(Context context, Beacon beacon)
    {


    }

    @Override
    public void didExitBeaconRange(Context context, Beacon beacon)
    {
        if(locateMeBeacon != null && beacon.equals(locateMeBeacon))
        {
            reset();
        }
    }

    public static boolean isLocateMeActionUrl(String url)
    {
        if(!StringUtils.isEmpty(url))
        {
            return url.contains(LOCATE_ME_URL_SCHEME);
        }

        return false;
    }

    private static String parseBeaconCode(String url)
    {
        if(!StringUtils.isEmpty(url))
        {
            return url.substring(LOCATE_ME_URL_SCHEME.length());
        }

        return "";
    }

    public static void reset()
    {
        locateMeBeacon = null;
        locateMeBeaconCode = "";
        locateMeMessage = "";
    }

    public static boolean isLocateMeBeaconDetected()
    {
        return !StringUtils.isEmpty(locateMeBeaconCode);
    }

    public static String getLocateMeMessage()
    {
        return locateMeMessage;
    }

    public static String getLocateMeBeaconCode()
    {
        return locateMeBeaconCode;
    }
}
