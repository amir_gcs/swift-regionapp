package com.swift.SWIFTRegional.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStateReceiver extends BroadcastReceiver
{
	public interface OnNetworkStateChangedListener
	{
		void onNetworkConnected();
		void onNetworkDisconnected();
	}
	
	private OnNetworkStateChangedListener listener;
	
	@Override
	public void onReceive(Context context, Intent intent)
	{
		final String action = intent.getAction();
		
		if (!ConnectivityManager.CONNECTIVITY_ACTION.equals(action))
		{
			return;
		}
		
		if (this.listener != null)
		{
			final boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
			
			if (noConnectivity)
			{
				this.listener.onNetworkDisconnected();
			}
			else
			{
				final NetworkInfo activeNetInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
			    if (activeNetInfo != null && activeNetInfo.isConnected()) 
			    {
			    	this.listener.onNetworkConnected();
			    } 
			}
		}				
	}
	
	public void setOnNetworkStateChangedListener(OnNetworkStateChangedListener listener)
	{
		this.listener = listener;
	}
}
