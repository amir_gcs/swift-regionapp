package com.swift.SWIFTRegional.fragments.activityfeed;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.adapters.activityfeed.InstagramCursorAdapter;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.instagram.InstagramApiCall;
import com.swift.SWIFTRegional.contentproviders.InstagramContentProvider;
import com.swift.SWIFTRegional.database.tables.InstagramTable;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.instagram.Instagram;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by wesley on 24/06/14.
 */
public class InstagramFragment extends AbstractCardsListFragment<InstagramCursorAdapter> implements AdapterView.OnItemClickListener, SearchableListInterface
{
    private boolean dataLoaded;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        GAHelper.trackScreen(view.getContext(), "/activity/instagram");

        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);
    }

    @Override
    public InstagramCursorAdapter getAdapter(Context context)
    {
        return new InstagramCursorAdapter(context);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof Instagram)
            {
                final Instagram instagram = (Instagram) o;

                GAHelper.trackEvent(view.getContext(), "instagram", "open_instagram", "");

                final Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData((Uri.parse(instagram.getLink())));

                this.startActivity(intent);

                return;
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    @Override
    public void startApiCalls()
    {
        this.doApiCall(false);
    }

    @Override
    public void onRefresh()
    {
        this.doApiCall(true);
    }

    public void doApiCall(boolean force)
    {
        if (super.startApiCalls(force))
        {
            if (UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.INSTAGRAM))
            {
                this.startApiCallForResult(Api.instagram, InstagramApiCall.class);
            }
            else
            {
                onRefreshCompleted();
            }
        }
    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(LoaderIDs.instagram, null, this);
    }

    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.instagram, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        switch (id)
        {
            case LoaderIDs.instagram:

                return new CursorLoader(this.getActivity(), getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {
        return StringUtils.isEmpty(this.searchText)
                ? InstagramContentProvider.CONTENT_URI
                : InstagramContentProvider.CONTENT_URI_SEARCH.buildUpon().appendPath(this.searchText).build();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.instagram:

                this.adapter.swapCursor(data);

                //mark as read
                if (data.moveToFirst())
                {
                    final long date = DatabaseUtils.getLong(data, InstagramTable.COLUMN_DATE);

                    //if the wall is opened we mark everything as read
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.INSTAGRAM, date);
                }

                this.dataLoaded = true;

                break;
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.instagram:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.instagram);
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch (inRequestCode)
        {
            case Api.instagram:

                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_SUCCESS)
                    {
                        this.startApiCalls();
                    }
                    else
                    {
                        ApiErrorHelper.showError(this.getActivity());
                    }
                }

                this.onRefreshCompleted();

                break;
        }
    }
}
