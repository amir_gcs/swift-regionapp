package com.swift.SWIFTRegional.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.swift.SWIFTRegional.interfaces.SpinnerFilterFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;
import com.swift.SWIFTRegional.views.NotLoggedInView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;

/**
 * Created by Wesley on 23/05/14.
 * A subclass of {@link AbstractQuickScrollListFragment} that adds a "Login required" view that is shown when
 * selecting certain filters (favourites, etc).
 */
public abstract class LoginViewAbstractQuickScrollListFragment<T extends BaseAdapter> extends AbstractQuickScrollListFragment<T> implements SpinnerFilterFragment, NotLoggedInView.LoggedInViewLoginListener, AdapterView.OnItemClickListener, SearchableListInterface
{
    private NotLoggedInView notLoggedInView;
    private SwipeRefreshLayout layoutSwipeRefresh;
    private boolean notLoggedMode;

    protected TextView tvErrorMessage;

    protected ViewLoaderStrategyInterface viewLoader;

    protected boolean dataLoaded;
    protected int selectedSpinnerPosition;

    protected enum UriMode
    {
        ALL,
        SUGGESTED,
        FAVOURITES,
    }

    protected UriMode uriMode = UriMode.ALL;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);


        try
        {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach()
    {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View v = super.onCreateView(inflater, container, savedInstanceState);

        this.notLoggedInView = (NotLoggedInView) v.findViewById(R.id.notLoggedInView);
        this.notLoggedInView.setColor(getNotLoggedInViewColor());
        this.notLoggedInView.setVisibility(View.GONE);
        this.notLoggedInView.setListener(this);
        this.layoutSwipeRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        this.tvErrorMessage = (TextView) v.findViewById(R.id.error_message);
        this.tvErrorMessage.setVisibility(View.GONE);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_red);

        ((MainActivity) this.getActivity()).updateSpinnerContent();
    }

    @Override
    public void startApiCalls()
    {
        this.doApiCalls(false);
    }

    @Override
    public void onRefresh()
    {
        this.doApiCalls(true);
    }

    protected abstract NotLoggedInView.NotLoggedColor getNotLoggedInViewColor();

    @Override
    public int getLayoutResourceId()
    {
        return R.layout.fragment_list_quick_scroll_loginview;
    }

    @Override
    public void itemSelected(Object item, int position)
    {
        this.notLoggedInView.setVisibility(View.GONE);
        this.layoutSwipeRefresh.setVisibility(View.VISIBLE);
        this.tvErrorMessage.setVisibility(View.GONE);
        this.notLoggedMode = false;
    }

    protected abstract void doApiCalls(boolean force);


    @Override
    protected boolean isAllDataLoaded()
    {
        return this.dataLoaded;
    }

    protected void loginCheck(UriMode uriMode, String logText)
    {
        if (this.getView() != null)
        {
            this.uriMode = uriMode;
            if (!UserPreferencesHelper.loggedIn(getView().getContext()))
            {
                this.notLoggedInView.setText(logText);
                this.notLoggedInView.setVisibility(View.VISIBLE);
                this.layoutSwipeRefresh.setVisibility(View.GONE);
                this.tvErrorMessage.setVisibility(View.GONE);
                this.notLoggedMode = true;
            }
        }
    }

    @Override
    public void logInClicked()
    {
    }

    public void loadErrorView(boolean isShow){
        //this.dataLoaded = true;
        changeContentState(ContentState.LOADED);

            this.tvErrorMessage.setVisibility(View.GONE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainActivity.LOGIN_REQUEST && resultCode == Activity.RESULT_OK)
        {
            // Refresh content and hide login view
            this.doApiCalls(true);
            this.startLoaders();
            this.showContent();
        }
    }

    protected void showContent()
    {
        this.notLoggedInView.setVisibility(View.GONE);
        this.layoutSwipeRefresh.setVisibility(View.VISIBLE);
        this.tvErrorMessage.setVisibility(View.GONE);
    }


    @Override
    public int getSpinnerPosition()
    {
        return this.selectedSpinnerPosition;
    }
}
