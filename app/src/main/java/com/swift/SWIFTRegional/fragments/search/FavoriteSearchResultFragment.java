package com.swift.SWIFTRegional.fragments.search;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.utils.SessionLoader;

import com.swift.SWIFTRegional.R;

public class FavoriteSearchResultFragment extends AbstractSearchResultFragment
{
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if(loader != null)
        {
            return loader;
        }

        String searchText = "";
        if(args != null)
        {
            searchText = args.getString(AbstractSearchResultFragment.EXTRA_SEARCH);
        }

        switch (id)
        {
            case LoaderIDs.exhibitor:

                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(ExhibitorContentProvider.CONTENT_URI_FAVORITES, searchText), null, null, null, null);

            case LoaderIDs.speaker:

                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(SpeakerContentProvider.CONTENT_URI_FAVORITES, searchText), null, null, null, null);
        }

        return null;
    }
    protected SessionLoader.UriMode getUriMode()
    {
        return SessionLoader.UriMode.FAVOURITES;
    }
}
