package com.swift.SWIFTRegional.fragments.sponsor;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.sponsors.SponsorDetailsActivity;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;

import com.goodcoresoftware.android.common.views.TextView;

public class SponsorDetailFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_sponsor_details,container,false);
        final Gson gson = new Gson();

        Bundle args = this.getArguments();
        final Sponsor sponsor = gson.fromJson(args.getString("sponsorInfo"),Sponsor.class);

        //centralize the title
        if(getActivity() instanceof SponsorDetailsActivity)
        {

            ((SponsorDetailsActivity)getActivity()).setToolbarText("Partners");
            ((SponsorDetailsActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ImageView mainLayout = (ImageView) view.findViewById(R.id.compView_TitleNImage);
        if(sponsor.getPictureUrl()!=null && !sponsor.getPictureUrl().equals(""))
        {
            ((ImageView) view.findViewById(R.id.compView_TitleNImage)).setVisibility(View.VISIBLE);
            applyBackgroundImage(mainLayout,sponsor.getPictureUrl());
        }
        else {
            ((ImageView) view.findViewById(R.id.compView_TitleNImage)).setVisibility(View.GONE);
        }

        if(sponsor.getUrl()!=null && !sponsor.getUrl().equals(""))
        {
            mainLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sponsor.getUrl()));
                    startActivity(browserIntent);


                }
            });
            TextView urlView=(TextView)view.findViewById(R.id.textView_sponsorURL);
            urlView.setText(sponsor.getUrl().replace("https://","").replace("http://",""));
            ((TextView)view.findViewById(R.id.textView_sponsorURL)).setVisibility(View.VISIBLE);
        }
        else
        {
            ((TextView)view.findViewById(R.id.textView_sponsorURL)).setVisibility(View.GONE);
        }
        //input details and data
        TextView titleView=(TextView)view.findViewById(R.id.textView_eventTitle);
        titleView.setText(sponsor.getName());

        TextView descView=(TextView)view.findViewById(R.id.textView_eventDescription);


        if(sponsor.getDescription()!=null && !sponsor.getDescription().equals(""))
        {
            descView.setVisibility(View.VISIBLE);
            descView.setText(Html.fromHtml(sponsor.getDescription()));
            descView.setMovementMethod(new ScrollingMovementMethod());
        }
        else
        {
            descView.setVisibility(View.GONE);
        }




        return view;
    }


    private void applyBackgroundImage(final ImageView imageView, String imageUrl)
    {

        try
        {

            Picasso.Builder builder = new Picasso.Builder(getActivity().getApplicationContext());
            builder.listener(new Picasso.Listener()
            {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    exception.printStackTrace();
                }


            });
            builder.build().load(imageUrl).into(imageView);




        }
        catch(Exception e)
        {
            Log.e("imageLoad",e.toString());
        }

    }




}
