package com.swift.SWIFTRegional.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.goodcoresoftware.android.common.utils.Connectivity;
import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.SearchViewUtils;
import com.goodcoresoftware.android.common.utils.StringUtils;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.WebViewActivity;
import com.swift.SWIFTRegional.application.SibosApplication;
import com.swift.SWIFTRegional.contentproviders.AdvertisementContentProvider;
import com.swift.SWIFTRegional.fragments.programme.SessionListFragment;
import com.swift.SWIFTRegional.fragments.programme.SpeakerListFragment;
import com.swift.SWIFTRegional.interfaces.AdAdapterInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.Advertisement;
import com.swift.SWIFTRegional.receivers.NetworkStateReceiver;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.OnAdShownListener;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Wesley on 23/05/14.
 */
public abstract class AbstractListFragment<T> extends LoadingBaseFragment
        implements SwipeRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener, NetworkStateReceiver.OnNetworkStateChangedListener, SearchView.OnQueryTextListener {
    private SwipeRefreshLayout swipeLayout;

    protected T listView;

    private boolean dataLoaded;
    protected boolean loadingDataFromApi;
    private boolean hasConnection;

    private NetworkStateReceiver networkStateReceiver;

    protected int adCycle;
    protected String searchText;
    protected SearchView searchView;

    private OnAdShownListener adListener = new OnAdShownListener() {
        @Override
        public void trackAdUrl(String url) {
            if (AbstractListFragment.this.getView() != null) {
                GAHelper.trackEvent(AbstractListFragment.this.getView().getContext(), "sibos-ads", "view", url);
            }
        }

        @Override
        public Object getItem(int position) {
            if (AbstractListFragment.this.listView instanceof ListView) {
                return ((ListView) AbstractListFragment.this.listView).getAdapter().getItem(position);
            } else if (AbstractListFragment.this.listView instanceof StickyListHeadersListView) {
                return ((StickyListHeadersListView) AbstractListFragment.this.listView).getAdapter().getItem(position);
            } else {
                return null;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (this.searchEnabled()) {
            this.setHasOptionsMenu(true);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        this.adCycle = ((SibosApplication) activity.getApplicationContext()).getNextAdCycle();

        this.hasConnection = Connectivity.hasConnection(activity);

        if (!hasConnection) {
            this.networkStateReceiver = new NetworkStateReceiver();
            this.getActivity().registerReceiver(this.networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            this.networkStateReceiver.setOnNetworkStateChangedListener(this);
        }
    }

    @Override
    public void onDetach() {
        destroyNetworkReceiver();
        super.onDetach();
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected View getContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(this.getLayoutResourceId(), container, false);

        this.swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        this.listView = (T) view.findViewById(R.id.listView);

        this.swipeLayout.setOnRefreshListener(this);


        this.swipeLayout.setEnabled(this.isPullToRefreshEnabled());

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (this.listView instanceof ListView) {
            ((ListView) this.listView).setOnItemClickListener(this);
            ((ListView) this.listView).setOnScrollListener(this.adListener);
        } else if (this.listView instanceof StickyListHeadersListView) {
            ((StickyListHeadersListView) this.listView).setOnScrollListener(this.adListener);
        }

        this.startLoaders();
        this.startApiCalls();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        if (menu.findItem(R.id.home_search) != null) {
            return;
        }

        if (!searchEnabled()) {
            return;
        }

        inflater.inflate(R.menu.menu_search, menu);


        final MenuItem searchMenuItem = menu.findItem(R.id.home_search);
        this.searchView = (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(searchMenuItem);
        final View searchPlate = SearchViewUtils.getSearchViewEditTextFrame(MenuItemCompat.getActionView(searchMenuItem));
        if (searchPlate != null) {
            searchPlate.setBackgroundResource(R.drawable.textfield_searchview_holo_dark);
        }

        this.searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                AbstractListFragment.this.onQueryTextChange("");
                return true;
            }
        });
    }

    @Override
    protected View getEmptyView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nodata, container, false);
    }

    @Override
    protected View getNoConnectionView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.searchEnabled()) {
            return inflater.inflate(R.layout.view_empty_no_connection, container, false);
        }
        return null;
    }

    @Override
    protected View getNoSearchResultsView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_empty_result, container, false);
    }

    public int getLayoutResourceId() {
        return R.layout.fragment_list;
    }

    public void startLoaders() {

    }

    public abstract AdapterState isAdapterLoaded();

    public void setSwipeToRefreshColorScheme(int arrayResourceId) {
        TypedArray ta = this.getResources().obtainTypedArray(arrayResourceId);
        final int[] colors = new int[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            colors[i] = ta.getResourceId(i, 0);
        }
        ta.recycle();

        if (colors.length == 4) {
            this.swipeLayout.setColorScheme(colors[0], colors[1], colors[2], colors[3]);
        } else {
            throw new IllegalStateException("You need to define an array of 4 colors");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object o = null;
        if (this.listView instanceof ListView) {
            if (((ListView) this.listView).getAdapter() instanceof AdAdapterInterface) {
                o = ((ListView) this.listView).getAdapter().getItem(position);
            }
        } else if (this.listView instanceof StickyListHeadersListView) {
            if (((StickyListHeadersListView) this.listView).getAdapter() instanceof AdAdapterInterface) {
                o = ((StickyListHeadersListView) this.listView).getAdapter().getItem(position);
            }
        }

        if (o != null) {
            if (o instanceof Advertisement) {
                this.openAdvertisement(parent.getContext(), (Advertisement) o);
            }
        }
    }

    public void openAdvertisement(Context context, Advertisement ad) {
        String url = ad.getLink();
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }

        final Intent intent;
        if (url.startsWith("http://www.sibos.com") || url.startsWith("https://www.sibos.com")) {
            int currentTheme;
            try {
                if (this.getActivity() instanceof MainActivity) {
                    currentTheme = ((MainActivity) this.getActivity()).getCurrentTheme();
                } else {
                    currentTheme = context.getPackageManager().getActivityInfo(this.getActivity().getComponentName(), 0).theme;
                }
            } catch (PackageManager.NameNotFoundException e) {
                currentTheme = R.style.SibosTheme_Purple;
            }

            GAHelper.trackScreen(context, "/ads/" + url);
            intent = WebViewActivity.getIntent(context, url, currentTheme, R.string.advertising);

        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        }

        GAHelper.trackEvent(this.getView().getContext(), "sibos-ads", "click", url);
        this.startActivity(intent);
    }

    public boolean doingApiCalls() {
        return this.loadingDataFromApi;
    }

    public void startApiCalls() {
        this.startApiCalls(false);
    }

    public boolean startApiCalls(boolean force) {
        if (this.isAdded()) {
            if (Connectivity.hasConnection(this.getActivity())) {
                if (this.isPullToRefreshEnabled()) {
                    this.loadingDataFromApi = true;
                }
                return true;
            } else {
                this.setRefreshing(false);
                if (force) {
                    ApiErrorHelper.showError(this.getActivity());
                }
            }
        }
        return false;
    }

    public boolean isPullToRefreshEnabled() {
        return true;
    }

    /**
     * Call this method to stop the refreshing
     */

    public void onRefreshCompleted() {
        this.loadingDataFromApi = false;
        if (this.swipeLayout != null) {
            this.swipeLayout.setRefreshing(false);
        }
        this.checkAdapterStatus();
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (!this.dataLoaded && !this.isAllDataLoaded()) {
            if(this instanceof SessionListFragment) {

                if (ModulePreferenceHelper.getModuleDataLoaded(getActivity().getApplicationContext(), ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SESSION))
                    this.changeContentState(ContentState.LOADED);
                else
                    this.changeContentState(ContentState.LOADED);

            } else if (this instanceof SpeakerListFragment){

                if (ModulePreferenceHelper.getModuleDataLoaded(getActivity().getApplicationContext(), ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SPEAKER))
                    this.changeContentState(ContentState.LOADED);
                else
                    this.changeContentState(ContentState.LOADED);

            } else {
                this.changeContentState(ContentState.LOADING);
            }

        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.checkAdapterStatus();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.dataLoaded = false;


    }

    @Override
    public void onNetworkConnected() {
        this.hasConnection = true;

        destroyLoadersInternal();
        startLoaders();
        startApiCalls(false);
    }

    @Override
    public void onNetworkDisconnected() {
        this.hasConnection = false;
    }

    private void destroyLoadersInternal() {

        destroyLoaders();
    }

    private void destroyNetworkReceiver() {
        if (this.networkStateReceiver != null) {
            this.getActivity().unregisterReceiver(this.networkStateReceiver);
            this.networkStateReceiver.setOnNetworkStateChangedListener(null);
            this.networkStateReceiver = null;
        }
    }

    public void checkAdapterStatus() {
        if (this.isAllDataLoaded()) {
            if (!this.dataLoaded && isAdapterLoaded() == AdapterState.LOADED) {
                this.dataLoaded = true;
                this.changeContentStateNoAnimation(ContentState.LOADED);

                //if we have data we don't need to check network connection again
                this.destroyNetworkReceiver();
            } else if (isAdapterLoaded() == AdapterState.EMPTY) {
                if (!StringUtils.isEmpty(this.searchText)) {
                    this.changeContentState(ContentState.NO_SEARCH_RESULTS);
                    return;
                }

                // The adapter can be empty but if api call is running wait
                if (this.doingApiCalls()) {
                    return;
                }

                this.dataLoaded = false;

                if (this.isEmptyViewEnabled()) {
                    //if pull to refresh is disabled we don't do Apicalls (Connection not needed)
                    //if a search result returns no items,§
                    if (this.hasConnection || !this.isPullToRefreshEnabled()) {
                        this.changeContentState(ContentState.EMPTY);
                    } else {
                        this.changeContentState(ContentState.NO_CONNECTION);
                    }
                } else {
                    this.dataLoaded = true;
                    changeContentState(ContentState.LOADED);

                    //if we have data we don't need to check network connection again
                    destroyNetworkReceiver();
                }
            } else if (isAdapterLoaded() == AdapterState.LOADED && this.searchEnabled()) {
                // switch from empty results to loaded view
                this.changeContentState(ContentState.LOADED);
                this.dataLoaded = true;
            } else {
                this.dataLoaded = false;
            }
        }
    }

    protected final void setRefreshing(boolean refreshing) {
        if (this.swipeLayout != null)
            this.swipeLayout.setRefreshing(refreshing);
    }

    protected boolean isEmptyViewEnabled() {
        return true;
    }

    protected abstract void destroyLoaders();

    protected abstract boolean isAllDataLoaded();

    @Override
    public boolean onQueryTextSubmit(String text) {
        if (this.searchView != null) {
            this.searchView.clearFocus();
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String text) {
        if (searchEnabled() && this.searchView != null) {
            // Remove "/" signs so queries with a "/" don't link to a non-existing URI.
            this.searchText = text.replace("/", " ");
            try {
                ((SearchableListInterface) (AbstractListFragment.this)).restartSearchLoaders();
            } catch (IllegalStateException e) {
                Log.d(SessionListFragment.class.getSimpleName(), "could not start loader");
            }
        }
        return true;
    }

    public enum AdapterState {
        LOADED,
        NOT_LOADED,
        EMPTY
    }

    private boolean searchEnabled() {
        return this instanceof SearchableListInterface;
    }


}