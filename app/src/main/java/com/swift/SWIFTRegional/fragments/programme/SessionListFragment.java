package com.swift.SWIFTRegional.fragments.programme;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.adapters.RSessionListAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.program.session.CategoryApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.session.SessionApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.speaker.SpeakerCategoryApiCall;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;
import com.swift.SWIFTRegional.interfaces.SpinnerFilterFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.Category;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import com.swift.SWIFTRegional.views.NotLoggedInView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.prefs.NodeChangeEvent;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.adapters.SessionListAdapter;
import com.swift.SWIFTRegional.adapters.SessionListAdapter600DP;
import com.swift.SWIFTRegional.contentproviders.CategoryContentProvider;
import com.swift.SWIFTRegional.fragments.AbstractExpandableListFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.EventTimingTwoItems;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.SessionLoader;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.views.Button;

public class SessionListFragment extends AbstractExpandableListFragment<SessionListAdapter>
        implements
        SearchableListInterface, SessionLoader.SessionLoaderListener, SpinnerFilterFragment, NotLoggedInView.LoggedInViewLoginListener {
    protected EventTiming[] eventTimings;
    private ViewLoaderStrategyInterface viewLoader;

    private int selectedSpinnerPosition;

    private SessionLoader.UriMode uriMode = SessionLoader.UriMode.ALL;

    private SessionLoader sessionLoader;

    private SpinnerItem streamItems;
    private String selectedTitle;
    private String selectedStream;
    private boolean forcedApi;

    private SpinnerItem tagItems;

    private boolean loadersRunning;

    private boolean dataLoaded;

    private NotLoggedInView notLoggedInView;
    private SwipeRefreshLayout layoutSwipeRefresh;
    private boolean notLoggedMode;

    private String[] streamsToDisplay;

    private Long currentHeaderId = null;

    private Long roomTypeHeaderId = null;
    public static Map<Long, Integer> headerCount = new HashMap<>();

    public boolean firstTime;
    public static String selectedMode;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public SessionListAdapter getAdapter(Context context) {
        return SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext()) ?
                new SessionListAdapter600DP(context, this.viewLoader) : new SessionListAdapter(context);
    }


    @Override
    public void onNetworkConnected() {
        super.onNetworkConnected();
        noConnection=false;
        if(this.isCanShow())
            doApiCalls(true);
    }



    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_list_programme;
    }

    @Override
    public void startApiCalls() {

        doApiCalls(false);
    }



    @Override
    public void onRefresh() {
        this.eventTimings = null;
        this.doApiCalls(true);
    }

    public void doApiCalls(boolean force) {

        forcedApi = force;

        if (super.startApiCalls(force)) {

            this.setIsApiHappening(true);
//            startLoadingView(true);

            this.startApiCallForResult(Api.session, SessionApiCall.class, null);
            totalApiCount++;


        } else {
            this.setRefreshing(false);
        }
    }

    private boolean isEmpty(Object[] objects)
    {
        boolean empty = true;
        if(objects==null)
            return empty;


        for (Object ob : objects) {
            if (ob != null) {
                empty = false;
                break;
            }
        }

        return  empty;
    }

    @Override
    public void sessionsLoaded(EventTiming[] localeventTimings) {


        selectedMode = selectedMode != null ? selectedMode : "DAY";

        if (localeventTimings != null) {


            if(isEmpty(localeventTimings))
                return;

            if (firstTime && localeventTimings.length == 0) {
                //force first time
                doApiCalls(true);
            }
            this.eventTimings = localeventTimings;
            if (SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())) {


                List<EventTimingTwoItems> ets = new ArrayList<>();
                String dateStr = "";


                if (this.uriMode.equals(SessionLoader.UriMode.SESSIONDAY)) {
                    selectedMode = "DAY";
                    Arrays.sort(this.eventTimings);
                    EventTimingTwoItems[] eventTimingTwoItems = getEventTimingTwoItems(this.eventTimings);

                    List<Long> headerList = getHeadersFromData(this.eventTimings);


                    headerCount = new HashMap<>();
                    for (int i = 0; i < eventTimingTwoItems.length; i++) {
                        EventTimingTwoItems et2 = eventTimingTwoItems[i];
                        if (et2 != null && et2.event1 != null) {
                            Long headerId = Long.parseLong(et2.event1.getStartDateOnlyDate());
                            int count = headerCount.get(headerId) == null ? 0 : headerCount.get(headerId);
                            count += et2.event2 == null ? 1 : 2;
                            headerCount.put(headerId, count);
                            if (currentHeaderId != null && currentHeaderId.toString().equals(et2.event1.getStartDateOnlyDate())) {
                                ets.add(et2);
                            } else if (!dateStr.equals(et2.event1.getStartDateOnlyDate())) {
                                ets.add(et2);
                                dateStr = et2.event1.getStartDateOnlyDate();
                            }
                        }

                    }
                } else if (this.uriMode.equals(SessionLoader.UriMode.SESSIONTYPE)) {

                    List<Long> headerList = getTypeHeadersFromData(this.eventTimings);
                    selectedMode = "TYPE";
                    headerCount = new HashMap<>();
                    Arrays.sort(this.eventTimings);
                    EventTimingTwoItems[] eventTimingTwoItems = getEventTimingTwoItems(this.eventTimings);
                    for (int i = 0; i < eventTimingTwoItems.length; i++) {
                        EventTimingTwoItems et2 = eventTimingTwoItems[i];
                        if (et2 != null && et2.event1 != null) {
                            Long headerId = (long) et2.event1.getEvent().getType().getSessionId().hashCode();
                            int count = headerCount.get(headerId) == null ? 0 : headerCount.get(headerId);
                            count += et2.event2 == null ? 1 : 2;
                            headerCount.put(headerId, count);
                            ets.add(et2);
                        }
                    }

                }

                this.adapter.setItems(ets.toArray(new EventTimingTwoItems[0]));


            } else {
                List<EventTiming> ets = new ArrayList<>();
                String dateStr = "";


                if (this.uriMode.equals(SessionLoader.UriMode.SESSIONDAY)) {
                    selectedMode = "DAY";
                    List<Long> headerList = getHeadersFromData(this.eventTimings);
                    headerCount = new HashMap<>();
                    for (int i = 0; i < this.eventTimings.length; i++) {
                        EventTiming et = this.eventTimings[i];
                        if (et != null) {
                            Long headerId = Long.parseLong(et.getStartDateOnlyDate());
                            int count = headerCount.get(headerId) == null ? 0 : headerCount.get(headerId);
                            headerCount.put(headerId, ++count);
                            ets.add(et);
                        }

                    }

                } else if (this.uriMode.equals(SessionLoader.UriMode.SESSIONTYPE)) {
                    selectedMode = "TYPE";
                    Arrays.sort(this.eventTimings);
                    List<Long> headerList = getTypeHeadersFromData(this.eventTimings);
                    headerCount = new HashMap<>();
                    for (int i = 0; i < this.eventTimings.length; i++) {
                        EventTiming et = this.eventTimings[i];
                        if (et != null) {
                            Long headerId = (long) et.getEvent().getType().getSessionId().hashCode();
                            int count = headerCount.get(headerId) == null ? 0 : headerCount.get(headerId);
                            headerCount.put(headerId, ++count);
                            ets.add(et);
                        }

                    }


                }


                this.adapter.setItems(ets.toArray(new EventTiming[0]));


            }

            togglingHeader = false;
            //this.expandGroups();
            this.expandGroups2();

        } else {
            this.adapter.setItems(null);
        }

        this.setCanShow(isEmpty(localeventTimings));
        this.dataLoaded = true;
        this.loadersRunning = false;

        this.onRefreshCompleted();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }

    private List<Long> getHeadersFromData(EventTiming[] eventTimings) {
        List<Long> headers = new ArrayList<>();
        for (int i = 0; i < eventTimings.length; i++) {
            EventTiming et = eventTimings[i];
            if (et != null) {
                Long header = Long.parseLong(et.getStartDateOnlyDate());
                if (!headers.contains(header)) {
                    headers.add(header);
                }
            }

        }

        return headers;
    }


    private List<Long> getTypeHeadersFromData(EventTiming[] eventTimings) {
        List<Long> headers = new ArrayList<>();
        for (int i = 0; i < eventTimings.length; i++) {
            EventTiming et = eventTimings[i];
            if (et != null) {
                Long sessionTypeId = (long) et.getEvent().getType().getSessionId().hashCode();
                if (!headers.contains(sessionTypeId)) {
                    headers.add(sessionTypeId);
                }
            }


        }

        return headers;
    }

    private EventTimingTwoItems[] getEventTimingTwoItems(EventTiming[] eventTimings) {

        List<EventTimingTwoItems> et2Items = new ArrayList<>();
        for (int i = 0; i < eventTimings.length; ) {
            EventTimingTwoItems e2 = new EventTimingTwoItems();
            e2.event1 = eventTimings[i++];
            if (i < eventTimings.length) {
                if (selectedMode.equals("TYPE")) {

                    if ((long) eventTimings[i].getEvent().getType().getSessionId().hashCode()
                            == ((long) eventTimings[i - 1].getEvent().getType().getSessionId().hashCode())) {
                        e2.event2 = eventTimings[i];
                        e2.hasSecondEvent = true;
                        i++;
                    } else {
                        e2.hasSecondEvent = false;
                    }
                } else {
                    if (DateUtils.prettyDayString(eventTimings[i].getStartDate())
                            .equals(DateUtils.prettyDayString(eventTimings[i - 1].getStartDate()))) {
                        e2.event2 = eventTimings[i];
                        e2.hasSecondEvent = true;
                        i++;
                    } else {
                        e2.hasSecondEvent = false;
                    }
                }
            }
            et2Items.add(e2);
        }

        return et2Items.toArray(new EventTimingTwoItems[0]);
    }

    @Override
    public AdapterState isAdapterLoaded() {
        if (this.notLoggedMode) {
            return AdapterState.LOADED;
        }
        if (this.getActivity() != null && ((MainActivity) getActivity()).isDoingSessionApiCalls()) {
            return AdapterState.NOT_LOADED;
        }
        return super.isAdapterLoaded();
    }

    @Override
    public void startLoaders() {
        super.startLoaders();
        this.dataLoaded = false;
        this.startListLoaders();
    }

    @Override
    public void onNetworkDisconnected() {
        super.onNetworkDisconnected();
        this.noConnection=true;
        if(this.isCanShow())
        {
            this.changeContentState(ContentState.NO_CONNECTION);
        }
    }

    @Override
    public void restartSearchLoaders() {
        togglingHeader = true;
        this.startListLoaders();
    }

    public void startListLoaders() {
        if (!this.loadersRunning) {
            this.loadersRunning = true;
            this.sessionLoader = new SessionLoader(getActivity(), this.getLoaderManager(), this);
            this.sessionLoader.getEventTimings(this.uriMode, TextUtils.isEmpty(this.selectedStream) ? "" : this.selectedStream, this.searchText);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null) {
            return loader;
        }

        switch (id) {
            case LoaderIDs.streams:

                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(CategoryContentProvider.CONTENT_URI_CATEGORY_TYPE, "streams"), null, null, null, null);

            case LoaderIDs.tags:
                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(CategoryContentProvider.CONTENT_URI_CATEGORY_TYPE, "tags"), null, null, null, null);

        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        this.layoutSwipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        firstTime = true;
        this.notLoggedInView = (NotLoggedInView) view.findViewById(R.id.notLoggedInView_programme);
        this.notLoggedInView.setColor(NotLoggedInView.NotLoggedColor.RED);
        this.notLoggedInView.setVisibility(View.GONE);
        this.notLoggedInView.setListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (this.adapter.getItem(position) instanceof EventTiming) {
            // Only execute for phone clicks. Tablet clicks are handled inside Session View helpers
            if (!SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())) {
                final EventTiming eventTiming = (EventTiming) this.adapter.getItem(position);

                final Intent intent = SessionDetailsActivity.createIntent(parent.getContext(), eventTiming.getEventId());
                this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.SESSIONS);
            }
        } else {
            super.onItemClick(parent, view, position, id);
        }
    }

    public String[] getStreamsToDisplay() {
        if (streamsToDisplay == null) {
            return getActivity().getApplicationContext().getResources().getStringArray(R.array.streams);

        } else {
            return streamsToDisplay;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.setCanShow(data.getCount() == 0);

        switch (loader.getId()) {
            case LoaderIDs.streams:
                if (data.getCount() > 0) {
                    if (streamsToDisplay == null) {
                        streamsToDisplay = getActivity().getApplicationContext().getResources().getStringArray(R.array.streams);
                    }

                    Category[] categories = new Category[data.getCount()];

                    while (data.moveToNext()) {
                        Category streamCategory = new Category();
                        streamCategory.constructFromCursor(data);
                        categories[data.getPosition()] = streamCategory;
                    }

                    List<SpinnerItem> streamItems = new ArrayList<>();

                    for (int i = 0; i < streamsToDisplay.length; i++) {
                        for (int j = 0; j < categories.length; j++) {
                            if (categories[j].getName().equalsIgnoreCase(streamsToDisplay[i])) {
                                streamItems.add(new SpinnerItem(categories[j].getName() + " (" + categories[j].getCount() + ")", categories[j].getId(), SpinnerItem.SpinnerItemType.Stream));
                                break;
                            }
                        }
                    }

                    if (streamItems.size() > 0) {
                        this.streamItems = new SpinnerItem("Streams", streamItems.toArray(new SpinnerItem[0]));
                    }

                    ((MainActivity) this.getActivity()).updateSpinnerContent();
                }

                break;

            case LoaderIDs.tags:
                if (data.getCount() > 0) {

                    Category[] categories = new Category[data.getCount()];
                    while (data.moveToNext()) {
                        Category tagCategory = new Category();
                        tagCategory.constructFromCursor(data);
                        categories[data.getPosition()] = tagCategory;
                    }

                    List<SpinnerItem> tagsItems = new ArrayList<>();

                    for (int j = 0; j < categories.length; j++) {
                        tagsItems.add(new SpinnerItem(categories[j].getName() + " (" + categories[j].getCount() + ")", categories[j].getId(), SpinnerItem.SpinnerItemType.Tag));
                    }

                    if (tagsItems.size() > 0) {
                        this.tagItems = new SpinnerItem("Tracks", tagsItems.toArray(new SpinnerItem[0]));
                    }

                    ((MainActivity) this.getActivity()).updateSpinnerContent();


                }

                break;
        }

        super.onLoadFinished(loader, data);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ColorDrawable dividerColor = new ColorDrawable();
        dividerColor.setColor(getResources().getColor(R.color.separator));

        this.listView.setDivider(dividerColor);
        this.listView.setDividerHeight(1);

        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_red);

        ((MainActivity) this.getActivity()).updateSpinnerContent();

        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });


    }

    @Override
    protected void destroyLoaders() {
        if (this.sessionLoader != null) {
            this.sessionLoader.destroyLoaders();
        }
    }

    @Override
    protected boolean isAllDataLoaded() {
        return this.dataLoaded;
    }

    protected void searchText(String text) {
        this.searchText = text;
    }

    public void expandGroups2() {

        try{
            if (!togglingHeader) {
                Long[] headers = this.adapter.getHeaderIds();
                SibosUtils.gcLog("headers count = " + headers.length);
                for (int i = 0; i < headers.length; i++) {
                    Long header = headers[i];
                    SibosUtils.gcLog(" i = " + i + " | header = " + header + " | cHeader = " + currentHeaderId);

                    if ((firstTime && i == 0) || header.equals(currentHeaderId)) {
                        SibosUtils.gcLog("expanding header " + header);
                        firstTime = false;
                        this.listView.expand(header);
                        this.adapter.setHeaderIdCollapsed(header, false);
                        currentHeaderId = header;
                    } else if (!header.equals(currentHeaderId)) {
                        SibosUtils.gcLog("collapsing header " + header);
                        this.listView.collapse(header);
                        this.adapter.setHeaderIdCollapsed(header, true);
                    }


                }
                this.listView.invalidateViews();
            }
        }
        catch(Exception e)
        {

        }


    }

    @Override
    protected void closeHeadersExcept(Long headerId) {
        Long[] headers = this.adapter.getHeaderIds();

        for (int i = 0; i < headers.length; i++) {
            if (headerId != headers[i]) {
                this.listView.collapse(headers[i]);
                this.adapter.setHeaderIdCollapsed(headers[i], true);
            }
        }
    }

    /**
     * NotLoggedInView.LoggedInViewLoginListener
     */

    @Override
    public void logInClicked() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainActivity.LOGIN_REQUEST && resultCode == Activity.RESULT_OK) {
            // Refresh content and hide login view
            // Loaders only, API calls are already handled by the login listener in the Main Activity.
            this.startLoaders();

            this.notLoggedInView.setVisibility(View.GONE);
            this.layoutSwipeRefresh.setVisibility(View.VISIBLE);
        }
    }

    private int totalApiCount;

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle) {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        if (inRequestCode == Api.session) {


            if (this.getActivity() != null && !this.isRemoving() && !this.isDetached()) {
                if (inResultCode == ApiService.RESULT_FAILED) {
                    //ApiErrorHelper.showError(this.getActivity());


                }
            }

            onSessionApiCompleted();


        }

        if (inRequestCode == Api.category) {
            if (forcedApi || UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.SPEAKERCATEGORIES)) {
                this.startApiCallForResult(Api.speakercategory, SpeakerCategoryApiCall.class, null);
                totalApiCount++;
            } else {
                this.setIsApiHappening(false);
                this.onRefreshCompleted();
            }
        }


        if (inRequestCode == Api.speakercategory) {
            this.setIsApiHappening(false);
            this.onRefreshCompleted();
        }

    }

    private void onSessionApiCompleted() {

        try {

            if (forcedApi || UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.CATEGORIES)) {
                this.startApiCallForResult(Api.category, CategoryApiCall.class, null);
                totalApiCount++;
            } else {
                this.setIsApiHappening(false);
                this.onRefreshCompleted();
            }
        } catch (Exception e) {
            this.setIsApiHappening(false);
            this.onRefreshCompleted();
        }

    }

    private boolean togglingHeader = false;

    @Override
    protected void toggleHeadersFast(int itemPosition, long headerId, Boolean isCollapsed) {
        SibosUtils.gcLog("Item Position = " + itemPosition + " | headerId = " + headerId);
        currentHeaderId = isCollapsed ? headerId : null;
        togglingHeader = true;
        this.startLoaders();
    }

    @Override
    public void itemSelected(Object item, int position) {
        this.selectedTitle = "";
        this.selectedStream = "";
        togglingHeader = false;
        currentHeaderId = null;
        expandGroups2();
        this.selectedSpinnerPosition = position;
        firstTime = true;
        Log.d("spinner position", position + "");

        this.notLoggedInView.setVisibility(View.GONE);
        this.layoutSwipeRefresh.setVisibility(View.VISIBLE);
        this.notLoggedMode = false;

        if (item instanceof SpinnerItem) {
            SpinnerItem.SpinnerItemType itemType = ((SpinnerItem) item).getType();

            this.selectedTitle = ((SpinnerItem) item).getTitle();

            switch (itemType) {
                case SessionFull:
                    this.uriMode = SessionLoader.UriMode.ALL;
                    GAHelper.trackScreen(this.getView().getContext(), "/programme/sessions");
                    break;
                case SessionDay:
                    this.uriMode = SessionLoader.UriMode.SESSIONDAY;
                    GAHelper.trackScreen(this.getView().getContext(), "/programme/sessions/" + selectedTitle);
                    break;
                case SessionType:
                    this.uriMode = SessionLoader.UriMode.SESSIONTYPE;
                    GAHelper.trackScreen(this.getView().getContext(), "/programme/sessions/" + selectedTitle);
                    break;
            }

            this.startLoaders();
        }
    }

    private void loginCheck(SessionLoader.UriMode uriMode, String logText) {
        if (this.getView() != null) {
            this.uriMode = uriMode;
            if (!UserPreferencesHelper.loggedIn(getView().getContext())) {
                this.notLoggedInView.setText(logText);
                this.notLoggedInView.setVisibility(View.VISIBLE);
                this.layoutSwipeRefresh.setVisibility(View.GONE);
                this.notLoggedMode = true;
            }
        }
    }

    @Override
    public SpinnerItem[] getSpinnerItems() {
        List<SpinnerItem> spinnerItems = new ArrayList<>();

        spinnerItems.add(new SpinnerItem(
                getActivity().getString(R.string.spinner_programme_types),
                new SpinnerItem[]{
                        new SpinnerItem(getActivity().getString(R.string.spinner_session_day), R.drawable.selector_spinner_tv, SpinnerItem.SpinnerItemType.SessionDay),
                        new SpinnerItem(getActivity().getString(R.string.spinner_session_type), R.drawable.selector_spinner_favourites, SpinnerItem.SpinnerItemType.SessionType)

                }));

        return spinnerItems.toArray(new SpinnerItem[0]);
    }

    @Override
    public int getSpinnerPosition() {
        return this.selectedSpinnerPosition;
    }

}