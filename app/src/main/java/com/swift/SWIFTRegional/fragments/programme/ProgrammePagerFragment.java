package com.swift.SWIFTRegional.fragments.programme;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.fragments.speakers.AbstractFilterPagerFragment;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.spinner.SpinnerItem;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.ToolbarUtils;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.views.BlockableViewPager;

import java.util.ArrayList;
import java.util.List;

public class ProgrammePagerFragment extends AbstractFilterPagerFragment {
    public int selectedTabPosition;
    public static int SESSION_TAB = 0;
    public static int SPEAKER_TAB = 1;
    public static int TOTAL_TABS = 2;

    private LinearLayout tabs;
    private ArrayList<Fragment> fragmentArray = new ArrayList<Fragment>();


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentArray.add(new SessionListFragment());
        fragmentArray.add(new SpeakerListFragment());
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).isWall=false;
        if (selectedTabPosition == 1) {
            ((MainActivity) getActivity()).HideSpinner(false);
            ((MainActivity) getActivity()).setToolbarText("Speaker");
        } else {
            ((MainActivity) getActivity()).setToolbarText("");
            ((MainActivity) getActivity()).HideSpinner(true);

        }
    }

    @Override
    public SpinnerItem[] getSpinnerItems() {
        List<SpinnerItem> spinnerItems = new ArrayList<>();

        spinnerItems.add(new SpinnerItem(
                getActivity().getString(R.string.spinner_programme_types),
                new SpinnerItem[]{
                        new SpinnerItem(getActivity().getString(R.string.spinner_session_day), R.drawable.selector_spinner_tv, SpinnerItem.SpinnerItemType.SessionDay),
                        new SpinnerItem(getActivity().getString(R.string.spinner_session_type), R.drawable.selector_spinner_favourites, SpinnerItem.SpinnerItemType.SessionType)

                }));

        return spinnerItems.toArray(new SpinnerItem[0]);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        GAHelper.trackScreen(this.getView().getContext(), "/programme/sessions");

        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.region_app_backgroundcolor)));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = super.onCreateView(inflater, container, savedInstanceState);
        SetupTabStrip(view);
        if(!ModulePreferenceHelper.getModuleDataAvailable(view.getContext(), ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SESSION)){
            this.removeTabByIndex(SESSION_TAB);
        }else if(!ModulePreferenceHelper.getModuleDataAvailable(view.getContext(), ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SPEAKER)){
            this.removeTabByIndex(SPEAKER_TAB);
        }

//        if (selectedTabPosition == 1) {
//            viewPager.setCurrentItem(1);
//        } else {
//            viewPager.setCurrentItem(0);
//        }


        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_programme_viewpager;
    }

    @Override
    protected ArrayList<Fragment> getFragments() {
        return fragmentArray;
    }

    @Override
    protected int getTitles() {
        return R.array.programmeTabTitles;
    }

    private void SetupTabStrip(View view) {
        tabs = (LinearLayout) ((PagerSlidingTabStrip) view.findViewById(R.id.tabs)).getChildAt(0);
        for (int i = 0; i < tabs.getChildCount(); i++) {
            TextView tv = (TextView) tabs.getChildAt(i);
            tv.setTextSize(15);
            if (i == 0) {
                tv.setTextColor(getResources().getColor(R.color.white_foreground_color));
            } else {
                tv.setTextColor(getResources().getColor(R.color.region_app_backgroundcolor));
            }
        }
    }

    private void setActiveTab(int position) {
        for (int i = 0; i < tabs.getChildCount(); i++) {
            TextView tv = (TextView) tabs.getChildAt(i);
            if (i == position) {
                tv.setTextColor(getResources().getColor(R.color.white_foreground_color));
            } else {
                tv.setTextColor(getResources().getColor(R.color.region_app_backgroundcolor));
            }
        }

    }

    @Override
    protected void tabSelected(int position) {
        switch (position) {
            case 0:
                ((MainActivity) getActivity()).setToolbarText("");
                ((MainActivity) getActivity()).HideSpinner(true);
                GAHelper.trackScreen(this.getView().getContext(), "/programme/sessions");
                selectedTabPosition = position;
                break;
            case 1:
                ((MainActivity) getActivity()).setToolbarText("Speakers");
                ((MainActivity) getActivity()).HideSpinner(false);
                GAHelper.trackScreen(this.getView().getContext(), "/programme/speakers");
                selectedTabPosition = position;
                break;
        }
        setActiveTab(position);
    }

    public int getSelectedTabPosition() {
        return selectedTabPosition;
    }

    public void removeTabByIndex(final int index){

        tabs.getChildAt(index).setVisibility(View.GONE);
        this.viewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem((index == SESSION_TAB) ? SPEAKER_TAB : SESSION_TAB);
            }
        }, 50);

        ((BlockableViewPager)this.viewPager).setCanScroll(false);
        pagerAdapter.notifyDataSetChanged();


    }
}