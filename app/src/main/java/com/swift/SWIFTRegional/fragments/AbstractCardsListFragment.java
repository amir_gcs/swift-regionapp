package com.swift.SWIFTRegional.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.swift.SWIFTRegional.R;

/**
 * Created by Wesley on 23/05/14.
 */
public abstract class AbstractCardsListFragment<T extends BaseAdapter> extends AbstractDefaultListFragment<T, ListView>
{
    @Override
    public int getLayoutResourceId()
    {
        return R.layout.fragment_list_activity_wall;
    }

    @Override
    protected View getNoConnectionView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.view_empty_no_connection, container, false);
    }
}
