package com.swift.SWIFTRegional.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.utils.TabletUtils;

import com.goodcoresoftware.android.common.utils.Connectivity;


/**
 * Created by iker on 23/05/14.
 */
public abstract class LoadingBaseFragment extends BaseFragment
{
    static final int INTERNAL_EMPTY_CONTAINER_ID = 0x00ff0001;
    static final int INTERNAL_PROGRESS_CONTAINER_ID = 0x00ff0002;
    static final int INTERNAL_VIEW_CONTAINER_ID = 0x00ff0003;
    static final int INTERNAL_CONNECTION_CONTAINER_ID = 0x00ff0004;
    static final int INTERNAL_NO_SEARCH_RESULTS_CONTAINER_ID = 0x00ff0005;

    private boolean canShow;
    private boolean isApiCallHappening;

    public boolean noConnection;



    protected abstract View getContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    protected abstract View getEmptyView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    protected abstract View getNoConnectionView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    protected abstract View getNoSearchResultsView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    private View progressContainer;
    private View rootViewContainer;
    private View emptyViewContainer;
    private View noConnectionViewContainer;
    private View noSearchResultsViewContainer;

    private ContentState currentLoadingState;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        this.ensureView();
    }

    @SuppressWarnings("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final Context context = getActivity();

        FrameLayout root = new FrameLayout(context);

        TabletUtils.setWindowBackground(getActivity());

        // ------------------------------------------------------------------

        FrameLayout pframe = new FrameLayout(context);
        pframe.setId(INTERNAL_PROGRESS_CONTAINER_ID);
        pframe.setVisibility(View.GONE);
        pframe.setBackground(new ColorDrawable(getResources().getColor(R.color.general_window_background_white)));

        this.addFakeShadow(context, pframe);

        ProgressBar progress = new ProgressBar(context, null,
                android.R.attr.progressBarStyleLarge);

        pframe.addView(progress, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) progress.getLayoutParams();
        params.gravity = Gravity.CENTER;
        progress.setLayoutParams(params);

        root.addView(pframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // ------------------------------------------------------------------

        FrameLayout vframe = new FrameLayout(context);
        vframe.setId(INTERNAL_VIEW_CONTAINER_ID);

        View viewContent = getContentView(inflater, container, savedInstanceState);
        vframe.addView(viewContent, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        this.addFakeShadow(context, vframe);

        root.addView(vframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // ------------------------------------------------------------------

        FrameLayout eframe = new FrameLayout(context);
        eframe.setVisibility(View.GONE);
        eframe.setId(INTERNAL_EMPTY_CONTAINER_ID);
        eframe.setBackground(new ColorDrawable(getResources().getColor(R.color.general_window_background_white)));
        View viewEmpty = getEmptyView(inflater, container, savedInstanceState);

        // Add a blank empty view
        if (viewEmpty == null)
        {
            viewEmpty = new View(getActivity());
        }

        this.addFakeShadow(context, eframe);

        //For debug
        //viewEmpty.setBackgroundColor(Color.GREEN);

        eframe.addView(viewEmpty, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        root.addView(eframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        root.setBackground(new ColorDrawable(getResources().getColor(R.color.general_window_background_white)));
        // ------------------------------------------------------------------

        FrameLayout cframe = new FrameLayout(context);
        cframe.setVisibility(View.GONE);
        cframe.setId(INTERNAL_CONNECTION_CONTAINER_ID);

        View viewNoConnection = getNoConnectionView(inflater, container, savedInstanceState);

        // Add a blank empty view
        if (viewNoConnection == null)
        {
            viewNoConnection = new View(getActivity());
        }

        this.addFakeShadow(context, cframe);

        //For debug
        //viewNoConnection.setBackgroundColor(Color.GREEN);

        cframe.addView(viewNoConnection, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        root.addView(cframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // ------------------------------------------------------------------

        FrameLayout sframe = new FrameLayout(context);
        sframe.setVisibility(View.GONE);
        sframe.setId(INTERNAL_NO_SEARCH_RESULTS_CONTAINER_ID);

        View viewNoSearchResults = getNoSearchResultsView(inflater, container, savedInstanceState);

        // Add a blank empty view
        if (viewNoSearchResults == null)
        {
            viewNoSearchResults = new View(getActivity());
        }

        this.addFakeShadow(context, sframe);

        sframe.addView(viewNoSearchResults, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        root.addView(sframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // ------------------------------------------------------------------

        root.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        return root;
    }

    protected boolean needsToolbarShadow()
    {
        return true;
    }

    private void addFakeShadow(Context context, ViewGroup frame)
    {
        if (this.needsToolbarShadow())
        {
            ImageView imageViewShadow = new ImageView(context);
            imageViewShadow.setBackgroundResource(R.drawable.fake_shadow);
            frame.addView(imageViewShadow, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    public void changeContentState(ContentState inState)
    {
        changeContentState(inState, true);
    }

    public void changeContentStateNoAnimation(ContentState inState)
    {
        changeContentState(inState, false);
    }

    public void startLoadingView(boolean status)
    {
        if(status)
        {
          currentLoadingState = ContentState.LOADING.EMPTY;
          changeContentState(ContentState.LOADING,true);
        }

    }

    private void changeContentState(ContentState inState, boolean inAnimate)
    {
        noConnection = !Connectivity.hasConnection(this.getActivity());

        if(inState== ContentState.EMPTY && isApiCallHappening)
        {
            //let me know.
            inState=ContentState.LOADING;
        }

        if (this.currentLoadingState == inState)
        {
            return;
        }

        if (this.ensureView())
        {
            if (inState == ContentState.LOADING)
            {
                if (inAnimate)
                {
                    this.progressContainer.startAnimation(AnimationUtils.loadAnimation(
                            getActivity(), android.R.anim.fade_in));

                    if (this.currentLoadingState == ContentState.EMPTY)
                    {
                        this.emptyViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.LOADED)
                    {
                        this.rootViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.NO_CONNECTION)
                    {
                        this.noConnectionViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.NO_SEARCH_RESULTS)
                    {
                        this.noSearchResultsViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                }

                this.progressContainer.setVisibility(View.VISIBLE);
                this.rootViewContainer.setVisibility(View.GONE);
                this.emptyViewContainer.setVisibility(View.GONE);
                this.noConnectionViewContainer.setVisibility(View.GONE);
                this.noSearchResultsViewContainer.setVisibility(View.GONE);
            }
            else if (inState == ContentState.EMPTY)
            {
                if (inAnimate)
                {
                    this.emptyViewContainer.startAnimation(AnimationUtils.loadAnimation(
                            getActivity(), android.R.anim.fade_in));

                    if (this.currentLoadingState == ContentState.LOADING)
                    {
                        this.progressContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.LOADED)
                    {
                        this.rootViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.NO_CONNECTION)
                    {
                        this.noConnectionViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.NO_SEARCH_RESULTS)
                    {
                        this.noSearchResultsViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                }

                this.progressContainer.setVisibility(View.GONE);
                this.rootViewContainer.setVisibility(View.GONE);
                this.noConnectionViewContainer.setVisibility(View.GONE);
                this.noSearchResultsViewContainer.setVisibility(View.GONE);
                if(noConnection)
                {
                    this.noConnectionViewContainer.setVisibility(View.VISIBLE);
                }
                else
                {
                    this.emptyViewContainer.setVisibility(View.VISIBLE);
                }

            }
            else if (inState == ContentState.NO_SEARCH_RESULTS)
            {
                if (inAnimate)
                {
                    this.emptyViewContainer.startAnimation(AnimationUtils.loadAnimation(
                            getActivity(), android.R.anim.fade_in));

                    if (this.currentLoadingState == ContentState.LOADING)
                    {
                        this.progressContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.LOADED)
                    {
                        this.rootViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.NO_CONNECTION)
                    {
                        this.noConnectionViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.EMPTY)
                    {
                        this.emptyViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                }

                this.progressContainer.setVisibility(View.GONE);
                this.rootViewContainer.setVisibility(View.GONE);
                this.noConnectionViewContainer.setVisibility(View.GONE);
                this.emptyViewContainer.setVisibility(View.GONE);
                this.noSearchResultsViewContainer.setVisibility(View.VISIBLE);
            }
            else if (inState == ContentState.NO_CONNECTION)
            {
                if(canShow) {
                    if (inAnimate) {
                        this.noConnectionViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_in));

                        if (this.currentLoadingState == ContentState.LOADING) {
                            this.progressContainer.startAnimation(AnimationUtils.loadAnimation(
                                    getActivity(), android.R.anim.fade_out));
                        } else if (this.currentLoadingState == ContentState.LOADED) {
                            this.rootViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                    getActivity(), android.R.anim.fade_out));
                        } else if (this.currentLoadingState == ContentState.EMPTY) {
                            this.emptyViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                    getActivity(), android.R.anim.fade_out));
                        } else if (this.currentLoadingState == ContentState.NO_SEARCH_RESULTS) {
                            this.noSearchResultsViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                    getActivity(), android.R.anim.fade_out));
                        }
                    }

                    this.progressContainer.setVisibility(View.GONE);
                    this.rootViewContainer.setVisibility(View.GONE);
                    this.emptyViewContainer.setVisibility(View.GONE);
                    this.noSearchResultsViewContainer.setVisibility(View.GONE);
                    this.noConnectionViewContainer.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                if (inAnimate)
                {
                    this.rootViewContainer.startAnimation(AnimationUtils.loadAnimation(
                            getActivity(), android.R.anim.fade_in));

                    if (this.currentLoadingState == ContentState.LOADING)
                    {
                        this.progressContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.EMPTY)
                    {
                        this.emptyViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.NO_CONNECTION)
                    {
                        this.noConnectionViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                    else if (this.currentLoadingState == ContentState.NO_SEARCH_RESULTS)
                    {
                        this.noSearchResultsViewContainer.startAnimation(AnimationUtils.loadAnimation(
                                getActivity(), android.R.anim.fade_out));
                    }
                }

                this.progressContainer.setVisibility(View.GONE);
                this.rootViewContainer.setVisibility(View.VISIBLE);
                this.emptyViewContainer.setVisibility(View.GONE);
                this.noConnectionViewContainer.setVisibility(View.GONE);
                this.noSearchResultsViewContainer.setVisibility(View.GONE);
            }

            if (!inAnimate)
            {
                this.emptyViewContainer.clearAnimation();
                this.progressContainer.clearAnimation();
                this.rootViewContainer.clearAnimation();
                this.noConnectionViewContainer.clearAnimation();
                this.noSearchResultsViewContainer.clearAnimation();
            }
        }
        this.currentLoadingState = inState;
    }


    @SuppressWarnings("ResourceType")
    private boolean ensureView()
    {

        View root = getView();

        if (root != null)
        {
            this.emptyViewContainer = root.findViewById(INTERNAL_EMPTY_CONTAINER_ID);
            this.progressContainer = root.findViewById(INTERNAL_PROGRESS_CONTAINER_ID);
            this.rootViewContainer = root.findViewById(INTERNAL_VIEW_CONTAINER_ID);
            this.noConnectionViewContainer = root.findViewById(INTERNAL_CONNECTION_CONTAINER_ID);
            this.noSearchResultsViewContainer = root.findViewById(INTERNAL_NO_SEARCH_RESULTS_CONTAINER_ID);

            return true;
        }

        return false;
    }

    @Override
    public void onDestroyView()
    {
        this.progressContainer = this.rootViewContainer = this.emptyViewContainer = this.noConnectionViewContainer = this.noSearchResultsViewContainer = null;
        super.onDestroyView();
    }

    public boolean isCanShow() {
        return canShow;
    }

    public void setCanShow(boolean canShow) {
        this.canShow = canShow;
    }


    public void setIsApiHappening(boolean value) {
        this.isApiCallHappening = value;
    }

    public enum ContentState
    {
        LOADING,
        LOADED,
        EMPTY,
        NO_SEARCH_RESULTS,
        NO_CONNECTION
    }
}