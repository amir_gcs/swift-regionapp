package com.swift.SWIFTRegional.fragments.activityfeed;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.activityfeed.FlickrThumbsActivity;
import com.swift.SWIFTRegional.activities.news.NewsDetailsActivity;
import com.swift.SWIFTRegional.adapters.activityfeed.WallCursorAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.IssuesApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.NewsApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr.FlickrAccountApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr.FlickrPhotosetApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.instagram.InstagramApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterHashTagApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterNameApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterTokenApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.youtube.YouTubeVideosApiCall;
import com.swift.SWIFTRegional.contentproviders.WallContentProvider;
import com.swift.SWIFTRegional.database.tables.WallTable;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;
import com.swift.SWIFTRegional.interfaces.SpinnerFilterFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.TwitterUtils;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.Connectivity;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.goodcoresoftware.android.common.views.Button;

import com.swift.SWIFTRegional.R;

public class WallFragment extends AbstractCardsListFragment<WallCursorAdapter> implements SpinnerFilterFragment, SearchableListInterface
{
    private int apiCallCount = 0;

    private boolean dataLoaded;
    private static SharedPreferences preferences;
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        GAHelper.trackScreen(view.getContext(), "/activity/wall");

        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);

        ((MainActivity) this.getActivity()).updateSpinnerContent();

        apiStarted=false;

        this.listView.setOnScrollListener(new ScrollListener());
        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).isWall=true;
        ((MainActivity) getActivity()).setToolbarText("");
        ((MainActivity) getActivity()).HideSpinner(true);
    }

    @Override
    public WallCursorAdapter getAdapter(Context context)
    {
        return new WallCursorAdapter(context);
    }

    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.wall, null, this);
    }

    @Override
    public void startApiCalls()
    {
        this.startLoadingView(true);
        if(preferences==null)
        {
            preferences=MainActivity.preferences;
        }

        if(VideoFragment.nextToken==null)
        {
            VideoFragment.nextToken = preferences.getString(getString(R.string.nextToken),"");


        }

        if(TwitterNameFragment.nextToken==null)
        {
            TwitterNameFragment.nextToken = preferences.getString(getString(R.string.twitterNextToken),"");

        }

        if(TwitterHashtagFragment.nextToken==null)
        {
            TwitterHashtagFragment.nextToken = preferences.getString(getString(R.string.twitterHashNextToken),"");
        }

        if(TwitterHashtagFragment.nextToken.equals("")&&TwitterNameFragment.nextToken.equals("")&&VideoFragment.nextToken.equals("")) {
            this.doApiCalls(true);
        }

    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(LoaderIDs.wall, null, this);
    }

    private boolean  apiStarted;
    private View footerView;
    private  boolean footerVisible;

    @Override
    public void onRefresh()
    {   apiCCount=0;
        VideoFragment.setNextToken("");
        TwitterHashtagFragment.setNextToken("");
        TwitterNameFragment.setNextToken("");
        this.doApiCalls(true);
    }

    private class ScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int newState){
            // your code there
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {


if(searchText!=null && !searchText.equals(""))
{
    return;
}

            //Check if the last view is visible
            if (++firstVisibleItem + visibleItemCount > totalItemCount && !apiStarted && !(VideoFragment.nextToken.equals("CantLoad") && TwitterNameFragment.nextToken.equals("CantLoad") && TwitterHashtagFragment.nextToken.equals("CantLoad"))) {
                //load more content
                int visibleCount = visibleItemCount;


                if (footerView == null)
                    footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_loader, null, false);

                if(!footerVisible)
                {
                    listView.addFooterView(footerView);
                    footerVisible = true;
                }

                callApiLazy();

            }
        }
    }

    private void callApiLazy() {

        if(preferences==null)
        {
            preferences=MainActivity.preferences;
        }

        if(VideoFragment.nextToken==null)
        {
            VideoFragment.nextToken = preferences.getString(getString(R.string.nextToken),"");
        }

        if(TwitterNameFragment.nextToken==null)
        {
            TwitterNameFragment.nextToken = preferences.getString(getString(R.string.twitterNextToken),"");

        }

        if(TwitterHashtagFragment.nextToken==null)
        {
            TwitterHashtagFragment.nextToken = preferences.getString(getString(R.string.twitterHashNextToken),"");
        }

        this.doApiCalls(true);
    }


    private  int apiCCount ;


    private void doApiCalls(boolean force)
    {

        if (super.startApiCalls(force) && !apiStarted && apiCCount<=2)
        {
            apiStarted=true;
            this.apiCallCount = 0;

            if(!VideoFragment.nextToken.equals("CantLoad")) {
                apiCCount++;
                this.doYoutubeApiCall(force);
            }

            if(!TwitterNameFragment.nextToken.equals("CantLoad"))
            {
                apiCCount++;
                this.doTwitterApiCalls(force);
            }

            if(!TwitterHashtagFragment.nextToken.equals("CantLoad"))
            {
                apiCCount++;
                this.doTwitterHasTagApiCalls(force);
            }



        }
    }

    public boolean isDoingApiCalls()
    {
        return this.apiCallCount > 0;
    }

    private void doTwitterApiCalls(boolean force)
    {
        if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getView().getContext(), UpdatePreferencesHelper.LastUpdateType.TWITTER_NAME))
        {
            this.apiCallCount++;


                final Bundle requestParamsTwitter = new Bundle();

                requestParamsTwitter.putString(TwitterNameApiCall.PARAM_SEARCH_NAME, this.getString(R.string.twitter_search_name));

                this.startApiCallForResult(Api.twitter_name, TwitterNameApiCall.class, requestParamsTwitter);

        }
    }

    private void doTwitterHasTagApiCalls(boolean force)
    {
        if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getView().getContext(), UpdatePreferencesHelper.LastUpdateType.TWITTER_NAME))
        {
            this.apiCallCount++;


            final Bundle requestParamsTwitter = new Bundle();

            requestParamsTwitter.putString(TwitterHashTagApiCall.PARAM_SEARCH_HASHTAG, this.getString(R.string.twitter_search_hashtag));

            this.startApiCallForResult(Api.twitter_hashtag, TwitterHashTagApiCall.class, requestParamsTwitter);

        }
    }

    private void doYoutubeApiCall(boolean force)
    {
        if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getView().getContext(), UpdatePreferencesHelper.LastUpdateType.VIDEO))
        {
            this.apiCallCount++;
            this.startApiCallForResult(Api.youtube, YouTubeVideosApiCall.class);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        switch (id)
        {
            case LoaderIDs.wall:

                return new CursorLoader(this.getActivity(), getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {
        return StringUtils.isEmpty(this.searchText)
                ? WallContentProvider.CONTENT_URI
                : WallContentProvider.CONTENT_URI_SEARCH.buildUpon().appendPath(this.searchText).build();
    }

    @Override
    public void onNetworkConnected() {
        super.onNetworkConnected();
        noConnection=false;
        if(this.isCanShow())
            doApiCalls(true);
    }


    @Override
    public void onNetworkDisconnected() {
        super.onNetworkDisconnected();
        this.noConnection=true;
        if(this.isCanShow())
        {
            this.changeContentState(ContentState.NO_CONNECTION);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        this.setCanShow(data.getCount()==0);
        switch (loader.getId())
        {
            case LoaderIDs.wall:

                this.adapter.swapCursor(data);

                //mark as read
                if (data.moveToFirst())
                {
                    final long date = DatabaseUtils.getLong(data, WallTable.COLUMN_DATE);

                    //if the wall is opened we mark everything as read
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.WALL, date);

                    final long twitterLastUpdate = UpdatePreferencesHelper.getLastUpdate(loader.getContext(), UpdatePreferencesHelper.LastSeenType.TWITTER);

                    if (date > twitterLastUpdate)
                    {
                        UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.TWITTER, date);
                    }

                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.NEWS, date);
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.FLICKR, date);
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.INSTAGRAM, date);
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.VIDEO, date);
                }

                this.dataLoaded = true;

                break;
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.wall:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.wall);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            Intent intent;
            if (o instanceof WallItem)
            {
                final WallItem wallItem = (WallItem) o;

                switch (wallItem.getType())
                {
                    case TWITTER_NAME:
                    case TWITTER_HASHTAG:

                        final String tweetId = wallItem.getId();
                        final String username = wallItem.getTitle();

                        GAHelper.trackEvent(this.getView().getContext(), "twitter", "open_tweet", "");

                        TwitterUtils.showTweet(this.getView().getContext(), tweetId, username);

                        break;

                    case FLICKR_PHOTOSET:

                        final String flickrPhotosetId = wallItem.getId();
                        final String flickrPhotosetTitle = wallItem.getTitle();

                        final Intent flickrIntent = FlickrThumbsActivity.getIntent(view.getContext(), flickrPhotosetId, flickrPhotosetTitle);
                        this.startActivity(flickrIntent);

                        break;

                    case INSTAGRAM_IMAGE:

                        GAHelper.trackEvent(this.getView().getContext(), "instagram", "open_instagram", "");

                        this.openMediaItem(wallItem.getLink());
                        break;

                    case INSTAGRAM_VIDEO:

                        GAHelper.trackEvent(this.getView().getContext(), "instagram", "open_instagram", "");

                        this.openMediaItem(wallItem.getLink());
                        break;

                    case VIDEO:

                        GAHelper.trackEvent(this.getView().getContext(), "video", "play", wallItem.getTitle());

                        this.openMediaItem(wallItem.getLink());
                        break;

                    case NEWS:

                        GAHelper.trackScreen(view.getContext(), "/activity/news/" + wallItem.getTitle());

                        final String newsId = wallItem.getId();

                        final Intent newsIntent = NewsDetailsActivity.createIntent(view.getContext(), newsId);
                        this.startActivity(newsIntent);

                        break;

                    case ISSUE:

                        GAHelper.trackScreen(view.getContext(), "/activity/news/" + wallItem.getTitle());

                        final String link = wallItem.getLink();

                        try
                        {
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse(link), "application/pdf");

                            this.startActivity(intent);

                        }
                        catch (ActivityNotFoundException e)
                        {
                            intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(link));

                            this.startActivity(intent);
                        }

                        break;
                }

                return;
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    private void openMediaItem(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        this.startActivity(intent);
    }




    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch (inRequestCode)
        {


            case Api.twitter_name:
            case Api.twitter_hashtag:
            case Api.youtube:
                this.setIsApiHappening(false);
                apiCCount--;
                if(apiCCount<=0)
                {
                    apiStarted=false;
                    footerVisible=false;
                    if(footerView!=null) {
                        try
                        {
                            listView.removeFooterView(footerView);
                            footerView=null;
                        }
                        catch(Exception e)
                        {
                            footerView=null;
                        }

                    }
                }
                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_FAILED)
                    {

                       // ApiErrorHelper.showError(this.getActivity());

                    }
                }

                this.apiCallCount--;

                break;
        }

        if (this.apiCallCount <= 0)
        {
            this.apiCallCount = 0;
            this.onRefreshCompleted();
        }
    }

    @Override
    public void itemSelected(Object item, int position)
    {
        // Already handled by the MainActivity
    }

    @Override
    public SpinnerItem[] getSpinnerItems()
    {
        if(shouldLoadTwitter() && shouldLoadVideo()){
            return new SpinnerItem[]{
                    new SpinnerItem(this.getString(R.string.menu_child_activity_feed_wall)),
                    new SpinnerItem(this.getString(R.string.menu_child_activity_feed_twitter)),
                    new SpinnerItem(this.getString(R.string.menu_child_activity_feed_videos))
            };
        } else if(shouldLoadTwitter()) {
            return new SpinnerItem[]{
                    new SpinnerItem(this.getString(R.string.menu_child_activity_feed_wall)),
                    new SpinnerItem(this.getString(R.string.menu_child_activity_feed_twitter))
            };
        } else if(shouldLoadVideo()) {
            return new SpinnerItem[]{
                    new SpinnerItem(this.getString(R.string.menu_child_activity_feed_wall)),
                    new SpinnerItem(this.getString(R.string.menu_child_activity_feed_videos))
            };
        }
        return new SpinnerItem[]{};
    }

    @Override
    public int getSpinnerPosition()
    {
        return 0;
    }

    private boolean shouldLoadVideo() {
        return ModulePreferenceHelper.getModuleDataAvailable(getContext(), ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_VIDEO);
    }

    private boolean shouldLoadTwitter() {
        return ModulePreferenceHelper.getModuleDataAvailable(getContext(), ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_TWITTER);
    }
}