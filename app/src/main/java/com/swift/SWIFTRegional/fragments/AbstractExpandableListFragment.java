package com.swift.SWIFTRegional.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import com.swift.SWIFTRegional.adapters.search.AbstractExpandableAdAdapter;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.Advertisement;

import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Wesley on 23/05/14.
 */
public abstract class AbstractExpandableListFragment<T extends AbstractExpandableAdAdapter> extends AbstractListFragment<ExpandableStickyListHeadersListView> implements ExpandableListView.OnChildClickListener
{
    protected T adapter;

    protected void toggleHeadersFast(int itemPosition, long headerId, Boolean isCollapsed) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = super.onCreateView(inflater, container, savedInstanceState);

        this.adapter = this.getAdapter(view.getContext());
        this.listView.setAdapter(this.adapter);
        this.listView.setOnItemClickListener(this);
        this.listView.setOnHeaderClickListener(new StickyListHeadersListView.OnHeaderClickListener()
        {
            @Override
            public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky)
            {
                SibosUtils.gcLog("in click");
                //AbstractExpandableListFragment.this.listView.setSelectionAfterHeaderView();
                //AbstractExpandableListFragment.this.listView.invalidateViews();
                AbstractExpandableListFragment.this.listView.smoothScrollBy(0,0);
                AbstractExpandableListFragment.this.listView.setSelection(0);
                toggleHeadersFast(itemPosition, headerId, AbstractExpandableListFragment.this.listView.isHeaderCollapsed(headerId));

                /*
                if (AbstractExpandableListFragment.this.listView.isHeaderCollapsed(headerId))
                {
                    // collapse other headers
                    closeHeadersExcept(headerId);

                    AbstractExpandableListFragment.this.listView.expand(headerId);
                    AbstractExpandableListFragment.this.adapter.setHeaderIdCollapsed(headerId, false);
                }
                else
                {
                    AbstractExpandableListFragment.this.listView.collapse(headerId);
                    AbstractExpandableListFragment.this.adapter.setHeaderIdCollapsed(headerId, true);
                }
                AbstractExpandableListFragment.this.listView.invalidateViews();

                // Quickfix for list getting stuck off screen when collapsing.

                */
            }
        });

        return view;
    }

    protected void closeHeadersExcept(Long headerId) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        listView.setPadding(getResources().getDimensionPixelSize(R.dimen.padding_listview), listView.getPaddingTop(), getResources().getDimensionPixelSize(R.dimen.padding_listview), listView.getPaddingBottom());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (this.adapter.getItem(position) instanceof Advertisement)
        {
            this.openAdvertisement(parent.getContext(), (Advertisement) this.adapter.getItem(position));
        }
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
    {
        final Object o = this.adapter.getItem(childPosition);

        if (o instanceof Advertisement)
        {
            this.openAdvertisement(parent.getContext(), (Advertisement) o);

            return true;
        }

        return false;
    }

    @Override
    public AdapterState isAdapterLoaded()
    {
        if (adapter != null)
        {
            if (adapter.getCount() == 0)
            {
                return AdapterState.EMPTY;
            }
            else
            {
                return AdapterState.LOADED;
            }
        }

        return AdapterState.NOT_LOADED;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {

        super.onLoadFinished(loader, data);
    }

    @Override
    public int getLayoutResourceId()
    {
        return R.layout.fragment_list_expandable;
    }

    public abstract T getAdapter(Context context);
}
