package com.swift.SWIFTRegional.fragments.activityfeed;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.IssuesApiCall;
import com.swift.SWIFTRegional.contentproviders.NewsContentProvider;
import com.swift.SWIFTRegional.database.tables.NewsTable;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.WallItem;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.news.NewsDetailsActivity;
import com.swift.SWIFTRegional.adapters.NewsCursorAdapter;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.NewsApiCall;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import com.goodcoresoftware.android.common.utils.StringUtils;

public class NewsFragment extends AbstractCardsListFragment<NewsCursorAdapter> implements AdapterView.OnItemClickListener, SearchableListInterface
{
    private int apiCallCount = 0;
    private ViewLoaderStrategyInterface viewLoader;
    private boolean dataLoaded;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach()
    {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        GAHelper.trackScreen(view.getContext(), "/activity/news");

        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);
    }

    @Override
    public void startApiCalls()
    {
        this.doApiCalls(false);
    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(LoaderIDs.news, null, this);
    }

    @Override
    public void onRefresh()
    {
        this.doApiCalls(true);
    }

    public void doApiCalls(boolean force)
    {
        if (super.startApiCalls(force))
        {
            this.doNewsApiCall(force);
            this.doIssuesApiCall(force);
        }
    }

    public void doNewsApiCall(boolean force)
    {
        if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.NEWS))
        {
            this.apiCallCount++;
            //this.startApiCallForResult(Api.news, NewsApiCall.class);
        }
    }

    public void doIssuesApiCall(boolean force)
    {
        if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.ISSUES))
        {
            this.apiCallCount++;
            //this.startApiCallForResult(Api.issues, IssuesApiCall.class);
        }
    }

    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.news, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        switch (id)
        {
            case LoaderIDs.news:

                return new CursorLoader(this.getActivity(), getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {
        if(StringUtils.isEmpty(this.searchText)) {
            return NewsContentProvider.CONTENT_URI;
        } else {
            return Uri.withAppendedPath(NewsContentProvider.CONTENT_URI_SEARCH, this.searchText);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.news:

                this.adapter.swapCursor(data);

                //mark as read
                if (data.moveToFirst())
                {
                    final long date = DatabaseUtils.getLong(data, NewsTable.COLUMN_DATE);

                    //if the wall is opened we mark everything as read
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.NEWS, date);
                }

                this.dataLoaded = true;

                break;
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.news:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.news);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof WallItem)
            {
                final WallItem wallItem = (WallItem) o;

                switch (wallItem.getType())
                {
                    case NEWS:

                        final Intent intent = NewsDetailsActivity.createIntent(parent.getContext(), String.valueOf(wallItem.getId()));
                        this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.NEWS);

                        GAHelper.trackScreen(view.getContext(), "/activity/news/" + wallItem.getTitle());

                        break;

                    case ISSUE:

                        GAHelper.trackScreen(view.getContext(), "/activity/news/" + wallItem.getTitle());

                        final String link = wallItem.getLink();

                        try
                        {
                            final Intent issueIntent = new Intent(Intent.ACTION_VIEW);
                            issueIntent.setDataAndType(Uri.parse(link), "application/pdf");

                            this.startActivity(issueIntent);

                        }
                        catch (ActivityNotFoundException e)
                        {
                            final Intent issueIntent = new Intent(Intent.ACTION_VIEW);
                            issueIntent.setData(Uri.parse(link));

                            this.startActivity(issueIntent);
                        }

                        break;
                }
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        final Long lastModified;

        switch (inRequestCode)
        {


            case Api.news:
            case Api.issues:

                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_FAILED)
                    {
                        ApiErrorHelper.showError(this.getActivity());
                    }
                }

                this.apiCallCount--;

                break;
        }

        if (this.apiCallCount <= 0)
        {
            this.apiCallCount = 0;

            this.onRefreshCompleted();
        }
    }

    @Override
    public NewsCursorAdapter getAdapter(Context context)
    {
        return new NewsCursorAdapter(context);
    }
}