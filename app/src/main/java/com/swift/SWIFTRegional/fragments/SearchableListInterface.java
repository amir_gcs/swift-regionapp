package com.swift.SWIFTRegional.fragments;


public interface SearchableListInterface
{
    // restart loaders which contain data which can be searched
    void restartSearchLoaders();
}
