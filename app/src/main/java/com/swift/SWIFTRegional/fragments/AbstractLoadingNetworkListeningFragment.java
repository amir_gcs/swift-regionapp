package com.swift.SWIFTRegional.fragments;

import android.app.Activity;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodcoresoftware.android.common.utils.Connectivity;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.receivers.NetworkStateReceiver;

/**
 * LoadingBaseFragment that listens for network connections and starts api calls + loaders when connected.
 */
public abstract class AbstractLoadingNetworkListeningFragment extends LoadingBaseFragment implements NetworkStateReceiver.OnNetworkStateChangedListener
{

    protected boolean dataLoaded = false;
    private boolean hasConnection;
    private NetworkStateReceiver networkStateReceiver;


    protected abstract void destroyLoaders();

    protected abstract void startApiCalls();

    protected abstract void startLoaders();


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        this.checkContentState();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        this.hasConnection = Connectivity.hasConnection(activity);

        if (!hasConnection)
        {
            this.networkStateReceiver = new NetworkStateReceiver();
            this.getActivity().registerReceiver(this.networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            this.networkStateReceiver.setOnNetworkStateChangedListener(this);
        }
    }

    @Override
    public void onDetach()
    {
        destroyNetworkReceiver();
        super.onDetach();
    }

    private void destroyNetworkReceiver()
    {
        if (this.networkStateReceiver != null)
        {
            this.getActivity().unregisterReceiver(this.networkStateReceiver);
            this.networkStateReceiver.setOnNetworkStateChangedListener(null);
            this.networkStateReceiver = null;
        }
    }

    @Override
    final protected View getNoConnectionView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.view_empty_no_connection, container, false);
    }

    @Override
    public void onNetworkConnected()
    {
        this.hasConnection = true;

        this.destroyLoaders();
        this.startLoaders();
        this.startApiCallsIfConnection();
        this.checkContentState();
    }

    protected void startApiCallsIfConnection()
    {
        if (this.hasConnection)
        {
            this.startApiCalls();
        }
    }


    @Override
    public void onNetworkDisconnected()
    {
        this.hasConnection = false;

    }

    /**
     * Check which view (content view, loading view, no connection view) must be displayed.
     */
    protected void checkContentState()
    {
        if (this.dataLoaded)
        {

            this.changeContentState(ContentState.LOADED);

        } else if (!this.hasConnection)
        {

            this.changeContentState(ContentState.NO_CONNECTION);

        } else
        {

            this.changeContentState(ContentState.LOADING);

        }


    }

    @Override
    protected View getNoSearchResultsView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return null;
    }
}
