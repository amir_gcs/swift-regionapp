package com.swift.SWIFTRegional.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.adapters.DrawerBaseAdapter;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

public class DrawerFragment extends Fragment implements AdapterView.OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener
{

    public interface OnDrawerClickListener
    {
        void onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem inItem);
    }

    private DrawerBaseAdapter adapter;
    private ListView listView;

    private OnDrawerClickListener onDrawerClickListener;

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        final Activity activity = this.getActivity();

        if (activity instanceof MainActivity)
        {
            this.onDrawerClickListener = ((MainActivity) activity).getHelper();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_drawer, container, false);

        this.adapter = new DrawerBaseAdapter(view.getContext());

        this.listView = (ListView) view.findViewById(R.id.listview_menu);
        this.listView.setAdapter(this.adapter);
        this.listView.setOnItemClickListener(this);
        this.listView.setItemChecked(adapter.getPosition(DrawerBaseAdapter.DrawerMenuItem.Programme), true);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        ModulePreferenceHelper.registerChangeListener(view.getContext(), this);

        this.adapter.setLoggedIn(UserPreferencesHelper.loggedIn(view.getContext()));
    }

    @Override
    public void onDestroyView()
    {
        ModulePreferenceHelper.unRegisterChangeListener(this.getView().getContext(), this);
        super.onDestroyView();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        this.onDrawerClickListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (this.onDrawerClickListener != null)
        {
            final DrawerBaseAdapter.DrawerMenuItem child = (DrawerBaseAdapter.DrawerMenuItem) this.listView.getItemAtPosition(position);
            this.onDrawerClickListener.onDrawerItemClicked(child);
        }
    }

    public void setHomeAsSelected()
    {
        this.listView.setItemChecked(adapter.getPosition(DrawerBaseAdapter.DrawerMenuItem.Programme), true);
    }

    public void selectMenuItem(DrawerBaseAdapter.DrawerMenuItem menuItem)
    {
        this.listView.setItemChecked(adapter.getPosition(menuItem), true);
    }


    public void setOnDrawerClickListener(OnDrawerClickListener onDrawerClickListener)
    {
        this.onDrawerClickListener = onDrawerClickListener;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (
        key.equals(ModulePreferenceHelper.MODULE_DATA_AVAILABLE_PROGRAMME_SESSION) ||
        key.equals(ModulePreferenceHelper.MODULE_DATA_AVAILABLE_PROGRAMME_SPEAKER) ||
        key.equals(ModulePreferenceHelper.MODULE_DATA_AVAILABLE_SOCIAL_TWITTER) ||
        key.equals(ModulePreferenceHelper.MODULE_DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG) ||
        key.equals(ModulePreferenceHelper.MODULE_DATA_AVAILABLE_SOCIAL_VIDEO) ||
        key.equals(ModulePreferenceHelper.MODULE_DATA_AVAILABLE_SPONSOR) ||
        key.equals(ModulePreferenceHelper.MODULE_DATA_AVAILABLE_PRACTICAL_INFO)
        )
        {
            this.adapter.setMenuItemsBasedOnData();
        }
    }
}
