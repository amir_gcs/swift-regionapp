package com.swift.SWIFTRegional.fragments.activityfeed;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterHashTagApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterTokenApiCall;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.views.Button;

public class TwitterHashtagFragment extends AbstractTwitterFeedFragment
{
    private int apiCallCount;

    private boolean footerVisible;
    private boolean apiStarted;

    public static String nextToken;

    private static SharedPreferences preferences;
    private static  String tokenStringValue;
    private View footerView;


    @Override
    public void onNetworkConnected() {
        super.onNetworkConnected();
        noConnection=false;
        if(this.isCanShow())
            startApiCalls();
    }


    @Override
    public void onNetworkDisconnected() {
        super.onNetworkDisconnected();
        this.noConnection=true;
        if(this.isCanShow())
        {

            this.changeContentState(ContentState.NO_CONNECTION);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiStarted=false;

        this.listView.setOnScrollListener(new ScrollListener());
        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoadingView(true);
                onRefresh();
            }
        });
    }

    public static void setNextToken(String value) {
        nextToken = value;

        if (preferences == null) {
            preferences = MainActivity.preferences;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(tokenStringValue, value);
        editor.commit();
    }



    private class ScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int newState){
            // your code there
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {




            //Check if the last view is visible
            if (++firstVisibleItem + visibleItemCount > totalItemCount && !apiStarted && !nextToken.equals("CantLoad")) {
                //load more content
                int visibleCount = visibleItemCount;


                if (footerView == null)
                    footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_loader, null, false);

                if(!footerVisible)
                {
                    listView.addFooterView(footerView);
                    footerVisible = true;
                }

                startApiCalls();

            }
        }
    }
    @Override
    public void startApiCalls() {
        if (preferences == null) {
            preferences = MainActivity.preferences;
        }
        tokenStringValue = getString(R.string.twitterHashNextToken);
        nextToken = preferences.getString(getString(R.string.twitterHashNextToken), "");

        if (!nextToken.equals("CantLoad")) {
            this.doApiCall(false);
        }
    }

    @Override
    public void onRefresh()
    {
        this.setNextToken("");
        this.doApiCall(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.doApiCall(true);
    }

    public void doApiCall(boolean force)
    {
        if (super.startApiCalls(force))
        {
            this.apiCallCount = 0;

            this.apiCallCount++;

            apiStarted=true;
            startLoadingView(true);
            this.setIsApiHappening(true);
            this.startApiCallForResult(Api.twitter_hashtag, TwitterHashTagApiCall.class);

        }
    }

    @Override
    public int getLoaderId()
    {
        return LoaderIDs.twitter_hashtag;
    }



    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch (inRequestCode)
        {
            case Api.twitter_token:

                this.apiCallCount--;

                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_SUCCESS)
                    {
                        this.startApiCalls();
                    }

                }

                break;

            case Api.twitter_hashtag:
                this.setIsApiHappening(false);
                this.apiStarted=false;
                this.apiCallCount--;
                if(nextToken.equals("CantLoad")) {
                    footerVisible=false;
                    if(footerView!=null) {
                        try
                        {
                            listView.removeFooterView(footerView);
                            footerView=null;
                        }
                        catch(Exception e)
                        {
                            footerView=null;
                        }
                    }
                }
                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_FAILED)
                    {
                        footerVisible=false;
                        if(footerView!=null) {
                            try
                            {
                                listView.removeFooterView(footerView);
                                footerView=null;
                            }
                            catch(Exception e)
                            {
                                footerView=null;
                            }
                        }
                        //ApiErrorHelper.showError(this.getActivity());
                    }
                }

                break;
        }

        if (this.apiCallCount <= 0)
        {
            this.apiCallCount = 0;
            this.onRefreshCompleted();
        }
    }

    @Override
    public WallItem.Type getTwitterType() {
        return WallItem.Type.TWITTER_HASHTAG;
    }
}
