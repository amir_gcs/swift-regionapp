package com.swift.SWIFTRegional.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.goodcoresoftware.android.common.utils.Connectivity;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.WebViewActivity;
import com.swift.SWIFTRegional.receivers.NetworkStateReceiver;
import com.swift.SWIFTRegional.utils.UriUtils;

/**
 * Created by Wesley on 2/06/14.
 */
public class WebViewFragment extends BaseFragment implements NetworkStateReceiver.OnNetworkStateChangedListener {

    static final int INTERNAL_VIEW_CONTAINER_ID = 0x00ff0003;
    static final int INTERNAL_CONNECTION_CONTAINER_ID = 0x00ff0004;

    protected WebView webView;

    public boolean hasConnection;
    private NetworkStateReceiver networkStateReceiver;
    private View webViewContainer;
    private View noConnectionViewContainer;

    @SuppressWarnings("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        FrameLayout root = new FrameLayout(container.getContext());

        FrameLayout vframe = new FrameLayout(container.getContext());
        vframe.setId(INTERNAL_VIEW_CONTAINER_ID);

        View viewContent = this.getContentView(inflater, container, savedInstanceState);
        vframe.addView(viewContent, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        root.addView(vframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        FrameLayout cframe = new FrameLayout(container.getContext());
        cframe.setVisibility(View.GONE);
        cframe.setId(INTERNAL_CONNECTION_CONTAINER_ID);

        View viewNoConnection = inflater.inflate(R.layout.view_empty_no_connection, container, false);

        // Add a blank empty view
        if (viewNoConnection == null)
            viewNoConnection = new View(getActivity());

        cframe.addView(viewNoConnection, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        root.addView(cframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        return root;
    }

    private View getContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_webview, container, false);

        this.webView = (WebView) v.findViewById(R.id.webView);

        final WebSettings webSettings = this.webView.getSettings();

        enableHTMLAppCache();

        if (this.getActivity() != null && (this.getActivity() instanceof MainActivity || this.getActivity() instanceof WebViewActivity)) {
            webSettings.setSupportZoom(true);
            webSettings.setBuiltInZoomControls(true);
        }

        this.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("/")) {
                    url = "https://www.sibos.com/" + url;
                }

                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }

                final Intent intent;

                if (UriUtils.isExternUri(url)) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    WebViewFragment.this.startActivity(intent);

                } else {
                    if (WebViewFragment.this.getActivity() instanceof WebViewActivity) {
                        return super.shouldOverrideUrlLoading(view, url);
                    }

                    int currentTheme;
                    try {
                        currentTheme = view.getContext().getPackageManager().getActivityInfo(WebViewFragment.this.getActivity().getComponentName(), 0).theme;
                    } catch (PackageManager.NameNotFoundException e) {
                        currentTheme = R.style.SibosTheme_Purple;
                    }

                    intent = WebViewActivity.getIntent(view.getContext(), url, currentTheme, 0);
                    WebViewFragment.this.startActivity(intent);

                }


                return true;
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.ensureView();
        this.setVisibilities();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        this.hasConnection = Connectivity.hasConnection(activity);


        this.networkStateReceiver = new NetworkStateReceiver();
        this.getActivity().registerReceiver(this.networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        this.networkStateReceiver.setOnNetworkStateChangedListener(this);

    }

    @Override
    public void onDetach() {
        destroyNetworkReceiver();
        super.onDetach();
    }

    private void destroyNetworkReceiver() {
        if (this.networkStateReceiver != null) {
            this.getActivity().unregisterReceiver(this.networkStateReceiver);
            this.networkStateReceiver.setOnNetworkStateChangedListener(null);
            this.networkStateReceiver = null;
        }
    }

    @Override
    public void onNetworkConnected() {
        this.hasConnection = true;
        this.setVisibilities();
    }

    @Override
    public void onNetworkDisconnected() {
        this.hasConnection = false;
    }

    @SuppressWarnings("ResourceType")
    private void ensureView() {
        View root = getView();
        if (root == null) {
            throw new IllegalStateException("Content view not yet created");
        }

        this.webViewContainer = root.findViewById(INTERNAL_VIEW_CONTAINER_ID);
        this.noConnectionViewContainer = root.findViewById(INTERNAL_CONNECTION_CONTAINER_ID);
    }

    protected void showWebView(boolean show) {
        if (show) {
            this.webViewContainer.setVisibility(View.VISIBLE);
        } else {
            this.webViewContainer.setVisibility(View.GONE);
        }

    }

    protected void setVisibilities(Boolean ignoreConnection) {
        try
        {
            if (ignoreConnection || hasConnection) {
                this.webViewContainer.setVisibility(View.VISIBLE);
                this.noConnectionViewContainer.setVisibility(View.GONE);
                this.webView.setVisibility(View.VISIBLE);
                if(!hasConnection)
                    webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

                this.webView.reload();
            } else {
                this.webViewContainer.setVisibility(View.GONE);
                this.webView.setVisibility((View.GONE));
                this.noConnectionViewContainer.setVisibility(View.VISIBLE);
            }
        }
        catch (Exception e)
        {

        }


    }

    protected void setVisibilities() {
        setVisibilities(false);
    }

    private void enableHTMLAppCache() {

        webView.getSettings().setDomStorageEnabled(true);

        // Set cache size to 8 mb by default. should be more than enough
        webView.getSettings().setAppCacheMaxSize(1024*1024*8);

        // This next one is crazy. It's the DEFAULT location for your app's cache
        // But it didn't work for me without this line
        webView.getSettings().setAppCachePath("/data/data/"+ getContext().getPackageName() +"/cache");
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAppCacheEnabled(true);

        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
    }
}
