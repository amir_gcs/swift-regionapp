package com.swift.SWIFTRegional.fragments.programme;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.speakers.SpeakerDetailsActivity;
import com.swift.SWIFTRegional.adapters.SpeakerCursorAdapter;
import com.swift.SWIFTRegional.adapters.SpeakerCursorAdapter600DP;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.views.NotLoggedInView;

import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.views.Button;
import com.goodcoresoftware.android.common.views.TextView;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.program.speaker.SpeakerApiCall;
import com.swift.SWIFTRegional.database.tables.SpeakerTable;
import com.swift.SWIFTRegional.fragments.LoginViewAbstractQuickScrollListFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.SpeakerTwoItems;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;

import static android.view.View.GONE;

public class SpeakerListFragment extends LoginViewAbstractQuickScrollListFragment<SpeakerCursorAdapter>
{

    @Override
    protected NotLoggedInView.NotLoggedColor getNotLoggedInViewColor()
    {
        return NotLoggedInView.NotLoggedColor.RED;
    }

    @Override
    public SpeakerCursorAdapter getAdapter(Context context)
    {
        return SibosUtils.isGreaterThan600DP(context)? new SpeakerCursorAdapter600DP(context, this.viewLoader) :
                new SpeakerCursorAdapter(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });
        ((TextView)(((MainActivity)getActivity()).findViewById(R.id.textView_toolbar))).setPadding(0,0,200,0);


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof Speaker || o instanceof SpeakerTwoItems)
            {
                if(!SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())) {
                    final Speaker speaker = (Speaker) o;

                    String speakerName = speaker.getFirstName()+" "+speaker.getLastName();
                    final Intent intent = SpeakerDetailsActivity.createIntent(parent.getContext(), String.valueOf(speaker.getSpeakerId()),speakerName);

                    this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.SPEAKERS);
                }

                return;
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    private Boolean apiCallingFirstTime = true;

    @Override
    protected void doApiCalls(boolean force)
    {
        if (super.startApiCalls(force) && !apiCallingFirstTime) {
            this.setIsApiHappening(true);
//            startLoadingView(true);
            this.startApiCallForResult(Api.speaker, SpeakerApiCall.class, null);
        }
        apiCallingFirstTime = false;

    }

    @Override
    public void onNetworkConnected() {
        super.onNetworkConnected();
        noConnection=false;
        if(this.isCanShow())
            doApiCalls(true);
    }


    @Override
    public void onNetworkDisconnected() {
        super.onNetworkDisconnected();
        this.noConnection=true;
        if(this.isCanShow())
        {

            this.changeContentState(ContentState.NO_CONNECTION);
        }
    }

    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.speaker, null, this);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        switch (id)
        {
            case LoaderIDs.speaker:

                return new CursorLoader(this.getActivity(), this.getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri()
    {
        switch (this.uriMode)
        {
            case ALL:
            default:
                if (StringUtils.isEmpty(this.searchText))
                {
                    return SpeakerContentProvider.CONTENT_URI;
                }
                return Uri.withAppendedPath(SpeakerContentProvider.CONTENT_URI_SEARCH, this.searchText);
            case FAVOURITES:
                return Uri.withAppendedPath(SpeakerContentProvider.CONTENT_URI_FAVORITES, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);
        }
    }

    private boolean hasData;

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.speaker:
                this.dataLoaded = true;

                this.setCanShow(data.getCount()==0);
                if(SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())){
                    this.adapter.swapCursor(getModifiedCursorForTable(data));
                }    else {
                    this.adapter.swapCursor(data);
                }

                break;
        }

        super.onLoadFinished(loader, data);


    }

    /**
     * Build a new cursor which contains two speakers in each row for the table view
     * @param cursor
     * @return
     */
    private Cursor getModifiedCursorForTable(Cursor cursor){
        MatrixCursor modifiedCursor = new MatrixCursor(SpeakerTable.getColumnNamesTwoItems());
        Integer i = 0;
        while(cursor.moveToNext()){
            Speaker speaker = new Speaker();
            speaker.constructFromCursor(cursor);

            List<Object> cursorRow = new ArrayList<Object>();
            cursorRow.add(i);
            cursorRow.addAll(addSpeakerDataToList(speaker));

            if(cursor.moveToNext()){
                Speaker speaker2 = new Speaker();
                speaker2.constructFromCursor(cursor);

                if(speaker2.getLastName().substring(0,1).equalsIgnoreCase(speaker.getLastName().substring(0, 1))){
                    cursorRow.addAll(addSpeakerDataToList(speaker2));
                    modifiedCursor.addRow(cursorRow);
                } else {
                    cursorRow.addAll(addSpeakerDataToList(null));
                    modifiedCursor.addRow(cursorRow);
                    cursor.moveToPrevious();
                }

            } else {
                cursorRow.addAll(addSpeakerDataToList(null));
                modifiedCursor.addRow(cursorRow);
            }
            i++;
        }
        return modifiedCursor;
    }

    private List<Object> addSpeakerDataToList(Speaker speaker) {
        List<Object> data = new ArrayList<>();
        if(speaker == null) {
            Integer itemCount = SpeakerTable.getColumnNames().length - 1;
            for(Integer i = 0; i < itemCount; i++){
                data.add("");
            }
        } else {
            data.add(speaker.getSpeakerId());
            data.add(speaker.getSibosKey());
            data.add(speaker.getSalutation());
            data.add(speaker.getTitle());
            data.add(speaker.getFirstName());
            data.add(speaker.getLastName());
            data.add(speaker.getEmail());
            data.add(speaker.getCompanyName());
            data.add(speaker.getBusinessFunction());
            data.add(speaker.getCountry());
            data.add(speaker.getBiography());
            data.add(speaker.getPictureUrl());
            data.add(speaker.isFavourite()? "1" : "");
        }
        return data;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.speaker:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);

                break;
        }

        super.onLoaderReset(loader);
    }


    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.speaker);
    }

    @Override
    public void restartSearchLoaders()
    {
        this.getLoaderManager().restartLoader(LoaderIDs.speaker, null, this);
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch (inRequestCode)
        {
            case Api.speaker:
                this.setIsApiHappening(false);
                this.onRefreshCompleted();

                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_FAILED)
                    {
                        //ApiErrorHelper.showError(this.getActivity());
                        //loadErrorView(true);
                    }
                }
                
                break;
        }
    }

    @Override
    public void itemSelected(Object item, int position)
    {
        super.itemSelected(item, position);

        this.selectedSpinnerPosition = position;

        if (item instanceof SpinnerItem)
        {
            String selectedTitle = ((SpinnerItem) item).getTitle();

//            if (selectedTitle.equals(getSpinnerItems()[0].getTitle()))
//            {
                this.uriMode = UriMode.ALL;
                GAHelper.trackScreen(this.getView().getContext(), "/programme/speakers");
//            }
//            else
//            {
//                this.loginCheck(LoginViewAbstractQuickScrollListFragment.UriMode.FAVOURITES, getString(R.string.favourites_speakers_not_logged_in));
//                GAHelper.trackScreen(this.getView().getContext(), "/programme/speakers/favourites");
//            }

            this.startLoaders();
        }
    }



    @Override
    public SpinnerItem[] getSpinnerItems()
    {
        return new SpinnerItem[]{
                new SpinnerItem(this.getString(R.string.speakers_filter_all))
        };
    }

}