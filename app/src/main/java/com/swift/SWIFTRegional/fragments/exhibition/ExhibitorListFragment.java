package com.swift.SWIFTRegional.fragments.exhibition;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.activities.exhibitors.ExhibitorDetailsActivity;
import com.swift.SWIFTRegional.adapters.ExhibitorCursorAdapter;
import com.swift.SWIFTRegional.adapters.ExhibitorCursorAdapter600DP;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.apicalls.program.exhibitor.ExhibitorApiCall;
import com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider;
import com.swift.SWIFTRegional.database.tables.ExhibitorTable;
import com.swift.SWIFTRegional.fragments.LoginViewAbstractQuickScrollListFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;
import com.swift.SWIFTRegional.views.NotLoggedInView;

import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

public class ExhibitorListFragment extends LoginViewAbstractQuickScrollListFragment<ExhibitorCursorAdapter>
{
    @Override
    public ExhibitorCursorAdapter getAdapter(Context context)
    {
        return SibosUtils.isGreaterThan600DP(context)? new ExhibitorCursorAdapter600DP(context, this.viewLoader)
                : new ExhibitorCursorAdapter(context);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof Exhibitor)
            {
                if(!SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())) {
                    final Intent intent = ExhibitorDetailsActivity.createIntent(parent.getContext(), (Exhibitor) o);
                    this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.EXHIBITORS);
                }
            }
            else
            {
                super.onItemClick(parent, view, position, id);
            }
        }
    }

    @Override
    protected void doApiCalls(boolean force)
    {
        if (super.startApiCalls(force) && this.getView() != null)
        {
            if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getView().getContext(), UpdatePreferencesHelper.LastUpdateType.EXHIBITORS))
            {
                if (UserPreferencesHelper.loggedIn(this.getView().getContext()))
                {
                    ApiManager.getInstance(this.getView().getContext()).getFavouriteExhibitors();
                }
                Bundle args = new Bundle();
                args.putString(ExhibitorApiCall.HEADER_DATA_TIMESTAMP, UpdatePreferencesHelper.getApiHeaderTimeStamp(this.getView().getContext(), UpdatePreferencesHelper.LastModifiedType.EXHIBITORS));
                this.startApiCallForResult(Api.exhibitor, ExhibitorApiCall.class);
            }
        }

        // The list of delegates must also be loaded so the linked representatives can be shown on the exhibitor details screen.
        if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.PARTICIPANTS))
        {
            if (UserPreferencesHelper.loggedIn(this.getActivity()))
            {
                ApiManager.getInstance(this.getActivity()).getFavouriteDelegates();
            }

            final Context context = this.getView().getContext();
            final String timeStamp = UpdatePreferencesHelper.getApiHeaderTimeStamp(this.getView().getContext(), UpdatePreferencesHelper.LastModifiedType.PARTICIPANTS);
        }
    }

    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.exhibitor, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        switch (id)
        {
            case LoaderIDs.exhibitor:
                return new CursorLoader(this.getActivity(), this.getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri()
    {
        switch (this.uriMode)
        {
            case ALL:
            default:
                if (StringUtils.isEmpty(this.searchText))
                {
                    return ExhibitorContentProvider.CONTENT_URI;
                }
                return Uri.withAppendedPath(ExhibitorContentProvider.CONTENT_URI_SEARCH, this.searchText);
            case FAVOURITES:
                return Uri.withAppendedPath(ExhibitorContentProvider.CONTENT_URI_FAVORITES, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);
            case SUGGESTED:
                return Uri.withAppendedPath(ExhibitorContentProvider.CONTENT_URI_SUGGESTED, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.exhibitor:
                this.dataLoaded = true;
                if(SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())){
                    this.adapter.swapCursor(this.getModifiedCursorForTable(data));
                } else {
                    this.adapter.swapCursor(data);
                }
                break;
        }

        super.onLoadFinished(loader, data);
    }

    /**
     * Build a new cursor which contains two speakers in each row for the table view
     * @param cursor
     * @return
     */
    private Cursor getModifiedCursorForTable(Cursor cursor){
        MatrixCursor modifiedCursor = new MatrixCursor(ExhibitorTable.getColumnNamesTwoItems());
        Integer i = 0;
        while(cursor.moveToNext()){
            Exhibitor exhibitor = new Exhibitor();
            exhibitor.constructFromCursor(cursor);

            List<Object> cursorRow = new ArrayList<Object>();
            cursorRow.add(i);
            cursorRow.addAll(addExhibitorDataToList(exhibitor));

            if(cursor.moveToNext()){
                Exhibitor exhibitor2 = new Exhibitor();
                exhibitor2.constructFromCursor(cursor);

                if(exhibitor2.getName().substring(0,1).equalsIgnoreCase(exhibitor.getName().substring(0,1))){
                    cursorRow.addAll(addExhibitorDataToList(exhibitor2));
                    modifiedCursor.addRow(cursorRow);
                } else {
                    cursorRow.addAll(addExhibitorDataToList(null));
                    modifiedCursor.addRow(cursorRow);
                    cursor.moveToPrevious();
                }

            } else {
                cursorRow.addAll(addExhibitorDataToList(null));
                modifiedCursor.addRow(cursorRow);
            }
            i++;
        }
        return modifiedCursor;
    }

    private List<Object> addExhibitorDataToList(Exhibitor exhibitor) {
        List<Object> data = new ArrayList<>();
        if(exhibitor == null) {
            Integer itemCount = ExhibitorTable.getColumnNames().length - 1;
            for(Integer i = 0; i < itemCount; i++){
                data.add("");
            }
        } else {
            data.add(exhibitor.getExhibitorId());
            data.add(exhibitor.getEventId());
            data.add(exhibitor.getCcvk());
            data.add(exhibitor.getName());
            data.add(exhibitor.getUrl());
            data.add(exhibitor.getStandNumber());
            data.add(exhibitor.getDescription());
            data.add(exhibitor.getProductsOnShow());
            data.add(exhibitor.getPictureUrl());
            data.add(exhibitor.isFavourite() ? "1" : "");
        }
        return data;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.exhibitor:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }


    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.exhibitor);
    }


    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);


        if (inRequestCode == Api.exhibitor)
        {
            if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
            {
                if (inResultCode == ApiService.RESULT_FAILED)
                {
                    ApiErrorHelper.showError(this.getActivity());
                }
            }

            this.onRefreshCompleted();
        }
    }


    @Override
    public void restartSearchLoaders()
    {
        if (isAdded())
        {
            this.getLoaderManager().restartLoader(LoaderIDs.exhibitor, null, this);
        }
    }


    @Override
    protected NotLoggedInView.NotLoggedColor getNotLoggedInViewColor()
    {
        return NotLoggedInView.NotLoggedColor.ORANGE;
    }


    @Override
    public void itemSelected(Object item, int position)
    {
        super.itemSelected(item, position);

        this.selectedSpinnerPosition = position;

        if (item instanceof SpinnerItem)
        {
            String selectedTitle = ((SpinnerItem) item).getTitle();

            if (selectedTitle.equals(getSpinnerItems()[0].getTitle()))
            {
                this.uriMode = UriMode.ALL;
                GAHelper.trackScreen(this.getView().getContext(), "/exhibition/exhibitors");
            }
            else
            {
                this.loginCheck(LoginViewAbstractQuickScrollListFragment.UriMode.FAVOURITES, getString(R.string.favourite_exhitibors_not_logged_in));
                GAHelper.trackScreen(this.getView().getContext(), "/exhibition/exhibitors/favourites");
            }

            this.startLoaders();
        }
    }

    @Override
    public SpinnerItem[] getSpinnerItems()
    {
        return new SpinnerItem[]{
                new SpinnerItem(this.getString(R.string.exhibitors_filter_all)),
                new SpinnerItem(this.getString(R.string.exhibitors_filter_favourites))
        };
    }
}
