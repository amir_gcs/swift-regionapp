package com.swift.SWIFTRegional.fragments.sponsor;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.sponsors.SponsorDetailsActivity;
import com.swift.SWIFTRegional.adapters.SponsorCursorAdapter;
import com.swift.SWIFTRegional.adapters.SponsorCursorAdapter600DP;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.program.sponsor.SponsorApiCall;
import com.swift.SWIFTRegional.contentproviders.SponsorContentProvider;
import com.swift.SWIFTRegional.database.tables.SponsorTable;
import com.swift.SWIFTRegional.fragments.LoginViewAbstractQuickScrollListFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.SponsorTwoItems;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import com.swift.SWIFTRegional.views.NotLoggedInView;

import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.views.Button;

public class SponsorListFragment extends LoginViewAbstractQuickScrollListFragment<SponsorCursorAdapter> {

    private boolean hasData;

    @Override
    public SponsorCursorAdapter getAdapter(Context context) {

        return SibosUtils.isGreaterThan600DP(context) ? new SponsorCursorAdapter600DP(context, this.viewLoader)
                : new SponsorCursorAdapter(context);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).isWall=false;
        ((MainActivity) getActivity()).HideSpinner(false);
        ((MainActivity) getActivity()).setToolbarText("Partners");

        if(MainActivity.eventChangedPartner)
        {
            doApiCalls(true);
            MainActivity.eventChangedPartner=false;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });

        view.findViewById(R.id.listView).setPadding(0, 20, 0, 20);


    }

    @Override
    public void onNetworkConnected() {
        super.onNetworkConnected();
        noConnection=false;
        if(this.isCanShow())
            doApiCalls(true);
    }


    @Override
    public void onNetworkDisconnected() {
        super.onNetworkDisconnected();
        this.noConnection=true;
        if(this.isCanShow())
        {
            this.changeContentState(ContentState.NO_CONNECTION);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Object o = this.adapter.getItem(position);

        if (o != null) {
            if (o instanceof Sponsor) {
                viewLoader.loadView(SponsorDetailsActivity.createIntent(getActivity(), (Sponsor) o), ViewLoaderStrategyInterface.ViewType.SPONSORS);

            } else {
                super.onItemClick(parent, view, position, id);
            }
        }
    }

    @Override
    protected void doApiCalls(boolean force) {
        if (super.startApiCalls(force) && this.getView() != null) {
            if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getView().getContext(), UpdatePreferencesHelper.LastUpdateType.SPONSORS)) {
                this.setIsApiHappening(true);
                startLoadingView(true);
                Bundle args = new Bundle();
                args.putString(SponsorApiCall.HEADER_DATA_TIMESTAMP, UpdatePreferencesHelper.getApiHeaderTimeStamp(this.getView().getContext(), UpdatePreferencesHelper.LastModifiedType.SPONSORS));
                this.startApiCallForResult(Api.sponsor, SponsorApiCall.class);
            }
        }


    }

    @Override
    public void startLoaders() {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.sponsor, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null) {
            return loader;
        }

        switch (id) {
            case LoaderIDs.sponsor:
                return new CursorLoader(this.getActivity(), this.getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {
        switch (this.uriMode) {
            case ALL:
            default:
                if (StringUtils.isEmpty(this.searchText)) {
                    return SponsorContentProvider.CONTENT_URI;
                }
                return Uri.withAppendedPath(SponsorContentProvider.CONTENT_URI_SEARCH, this.searchText);
            case FAVOURITES:
                return Uri.withAppendedPath(SponsorContentProvider.CONTENT_URI_FAVORITES, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);
            case SUGGESTED:
                return Uri.withAppendedPath(SponsorContentProvider.CONTENT_URI_SUGGESTED, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LoaderIDs.sponsor:
                this.dataLoaded = true;
                this.setCanShow(data.getCount() == 0);

                if (SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())) {
                    this.adapter.swapCursor(this.getModifiedCursorForTable(data));
                } else {
                    this.adapter.swapCursor(data);
                }

                break;
        }

        super.onLoadFinished(loader, data);
    }

    /**
     * Build a new cursor which contains two speakers in each row for the table view
     *
     * @param cursor
     * @return
     */
    private Cursor getModifiedCursorForTable(Cursor cursor) {
        MatrixCursor modifiedCursor = new MatrixCursor(SponsorTable.getColumnNamesTwoItems());
        Integer i = 0;
        while (cursor.moveToNext()) {
            Sponsor sponsor = new Sponsor();
            sponsor.constructFromCursor(cursor);

            List<Object> cursorRow = new ArrayList<Object>();
            cursorRow.add(i);
            cursorRow.addAll(addSponsorDataToList(sponsor));

            if (cursor.moveToNext()) {
                Sponsor sponsor2 = new Sponsor();
                sponsor2.constructFromCursor(cursor);

                if (sponsor2.getName().substring(0, 1).equalsIgnoreCase(sponsor.getName().substring(0, 1))) {
                    cursorRow.addAll(addSponsorDataToList(sponsor2));
                    modifiedCursor.addRow(cursorRow);
                } else {
                    cursorRow.addAll(addSponsorDataToList(null));
                    modifiedCursor.addRow(cursorRow);
                    cursor.moveToPrevious();
                }

            } else {
                cursorRow.addAll(addSponsorDataToList(null));
                modifiedCursor.addRow(cursorRow);
            }
            i++;
        }
        return modifiedCursor;
    }

    private List<Object> addSponsorDataToList(Sponsor sponsor) {
        List<Object> data = new ArrayList<>();
        if (sponsor == null) {
            Integer itemCount = SponsorTable.getColumnNames().length - 1;
            for (Integer i = 0; i < itemCount; i++) {
                data.add("");
            }
        } else {
            data.add(sponsor.getSponsorId());
            data.add(sponsor.getEventId());
            data.add(sponsor.getName());
            data.add(sponsor.getUrl());
            data.add(sponsor.getDescription());
            data.add(sponsor.getProductsOnShow());
            data.add(sponsor.getPictureUrl());
            data.add(null);
        }
        return data;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LoaderIDs.sponsor:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }


    @Override
    protected void destroyLoaders() {
        this.getLoaderManager().destroyLoader(LoaderIDs.sponsor);
    }


    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle) {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);


        if (inRequestCode == Api.sponsor) {
            this.setIsApiHappening(false);
            if (this.getActivity() != null && !this.isRemoving() && !this.isDetached()) {
                if (inResultCode == ApiService.RESULT_FAILED && !hasData) {
                    //ApiErrorHelper.showError(this.getActivity());
                    //show the no data view

                }
            }

            this.onRefreshCompleted();
        }
    }


    @Override
    public void restartSearchLoaders() {
        if (isAdded()) {
            this.getLoaderManager().restartLoader(LoaderIDs.sponsor, null, this);
        }
    }


    @Override
    protected NotLoggedInView.NotLoggedColor getNotLoggedInViewColor() {
        return NotLoggedInView.NotLoggedColor.ORANGE;
    }


    @Override
    public void itemSelected(Object item, int position) {
        super.itemSelected(item, position);

        this.selectedSpinnerPosition = position;

        if (item instanceof SpinnerItem) {
            String selectedTitle = ((SpinnerItem) item).getTitle();

            if (selectedTitle.equals(getSpinnerItems()[0].getTitle())) {
                this.uriMode = UriMode.ALL;
                GAHelper.trackScreen(this.getView().getContext(), "/sponsor/sponsors");
            }

            this.startLoaders();
        }
    }

    @Override
    public SpinnerItem[] getSpinnerItems() {
        return new SpinnerItem[]{
                new SpinnerItem(this.getString(R.string.sponsors_filter_all))
        };
    }
}
