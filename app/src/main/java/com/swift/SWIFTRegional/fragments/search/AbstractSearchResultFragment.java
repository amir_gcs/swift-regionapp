package com.swift.SWIFTRegional.fragments.search;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.activityfeed.FlickrThumbsActivity;
import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.activities.speakers.SpeakerDetailsActivity;
import com.swift.SWIFTRegional.activities.sponsors.SponsorDetailsActivity;
import com.swift.SWIFTRegional.adapters.AdBaseAdapter;
import com.swift.SWIFTRegional.adapters.FavoriteBaseAdapter;
import com.swift.SWIFTRegional.fragments.sponsor.SponsorDetailFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.EventTimingTwoItems;
import com.swift.SWIFTRegional.models.News;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.models.SponsorTwoItems;
import com.swift.SWIFTRegional.models.WallItem;

import com.swift.SWIFTRegional.models.flickr.FlickrPhotoset;
import com.swift.SWIFTRegional.models.instagram.Instagram;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.utils.SessionLoader;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.utils.TwitterUtils;

import java.util.ArrayList;
import java.util.List;

import com.swift.SWIFTRegional.R;


import com.swift.SWIFTRegional.activities.news.NewsDetailsActivity;
import com.swift.SWIFTRegional.adapters.search.SearchBaseAdapter;
import com.swift.SWIFTRegional.adapters.search.SearchBaseAdapter600DP;
import com.swift.SWIFTRegional.fragments.AbstractQuickScrollListFragment;
import com.swift.SWIFTRegional.models.SpeakerTwoItems;

import com.swift.SWIFTRegional.models.twitter.Tweet;
import com.swift.SWIFTRegional.models.video.Video;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.GAHelper;

import static android.view.View.GONE;

public abstract class AbstractSearchResultFragment extends AbstractQuickScrollListFragment<AdBaseAdapter> implements SessionLoader.SessionLoaderListener
{
    public static final String EXTRA_SEARCH = "search_text";

    private SessionLoader sessionLoader;

    private boolean sessionTimingsLoaded;

    private boolean speakersLoaded;
    private boolean sponsorsLoaded;
    private boolean tweetsLoaded;
    private boolean videosLoaded;

    private ViewLoaderStrategyInterface viewLoader;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach()
    {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        // Dirty hack to show the empty view when opening the screen for the first time.
        sponsorsLoaded = speakersLoaded = sessionTimingsLoaded
                = tweetsLoaded =  videosLoaded = true;
        this.checkAdapterStatus();
        sponsorsLoaded = speakersLoaded = sessionTimingsLoaded
                = tweetsLoaded = videosLoaded = false;
    }

    private SearchBaseAdapter getSearchAdapter() {
        return (SearchBaseAdapter) this.adapter;
    }

    private SearchBaseAdapter600DP getSearchAdapter600DP() {
        return (SearchBaseAdapter600DP) this.adapter;
    }

    @Override
    protected View getEmptyView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_search_empty, container, false);
    }

    @Override
    protected View getNoSearchResultsView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_search_empty, container, false);
    }

    @Override
    public void startLoaders()
    {
        if (TextUtils.isEmpty(this.searchText))
        {
            return;
        }

        Bundle args = new Bundle();
        args.putString(EXTRA_SEARCH, this.searchText);

        if (this instanceof AllSearchResultFragment)
        {
            this.getLoaderManager().restartLoader(LoaderIDs.twitter_name, args, this);
            this.getLoaderManager().restartLoader(LoaderIDs.youtube, args, this);
        }
        else
        {
            // If favorites then supply empty lists for the following items that cannot be marked as favorite

            if(SibosUtils.isGreaterThan600DP(this.getActivity().getApplicationContext())){
                getSearchAdapter600DP().setTweets(new Tweet[0]);
                getSearchAdapter600DP().setVideos(new Video[0]);
            } else {
                getSearchAdapter().setTweets(new Tweet[0]);
                getSearchAdapter().setVideos(new Video[0]);
            }

             tweetsLoaded = videosLoaded = true;
        }

        this.sessionLoader = new SessionLoader(getView().getContext(), this.getLoaderManager(), this);
        this.sessionLoader.getEventTimings(getUriMode(), "", this.searchText);
        this.getLoaderManager().restartLoader(LoaderIDs.speaker, args, this);
        this.getLoaderManager().restartLoader(LoaderIDs.sponsor, args, this);
    }

    protected abstract SessionLoader.UriMode getUriMode();

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        return null;
    }

    @Override
    public void sessionsLoaded(EventTiming[] eventTimings)
    {
        if(SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext())) {
            if(eventTimings == null){
                 getSearchAdapter600DP().setEventTimings(null);
            } else {
                List<EventTimingTwoItems> eventPairs = new ArrayList<>();
                for (Integer i = 0; i < eventTimings.length; i++) {
                    EventTimingTwoItems eventPair = new EventTimingTwoItems();
                    eventPair.event1 = eventTimings[i];
                    if (i + 1 < eventTimings.length) {
                        if (DateUtils.prettyDayString(eventTimings[i + 1].getStartDate())
                                .equals(DateUtils.prettyDayString(eventTimings[i].getStartDate()))) {
                            eventPair.event2 = eventTimings[++i];
                        }
                    }
                    eventPairs.add(eventPair);
                }
                getSearchAdapter600DP().setEventTimings(eventPairs.toArray(new EventTimingTwoItems[0]));
            }
        } else {
            getSearchAdapter().setEventTimings(eventTimings);
        }

        this.sessionTimingsLoaded = true;
        this.checkAdapterStatus();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        Boolean isTablet = SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext());

        if(isTablet){
            onLoadFinished600DP(loader, data);
        } else {
            switch (loader.getId()) {
                case LoaderIDs.session:
                    ArrayList<EventTiming> eventTimings = new ArrayList<>();
                    while (data.moveToNext()) {
                        EventTiming eventTiming = new EventTiming();
                        eventTiming.constructFromCursor(data);
                        eventTimings.add(eventTiming);
                    }
                    getSearchAdapter().setEventTimings(eventTimings.toArray(new EventTiming[eventTimings.size()]));
                    this.sessionTimingsLoaded = true;
                    break;

                case LoaderIDs.sponsor:
                    ArrayList<Sponsor> sponsors = new ArrayList<>();
                    while (data.moveToNext()) {
                        Sponsor sponsor = new Sponsor();
                        sponsor.constructFromCursor(data);
                        sponsors.add(sponsor);
                    }
                    getSearchAdapter().setSponsors(sponsors.toArray(new Sponsor[sponsors.size()]));
                    this.sponsorsLoaded = true;
                    break;

                case LoaderIDs.speaker:
                    ArrayList<Speaker> speakers = new ArrayList<>();
                    while (data.moveToNext()) {
                        Speaker speaker = new Speaker();
                        speaker.constructFromCursor(data);
                        speakers.add(speaker);
                    }
                    getSearchAdapter().setSpeakers(speakers.toArray(new Speaker[speakers.size()]));
                    this.speakersLoaded = true;
                    break;
            }
        }

        switch (loader.getId()) {

            case LoaderIDs.twitter_name:
                ArrayList<Tweet> tweets = new ArrayList<>();
                while (data.moveToNext()) {
                    Tweet tweet = new Tweet();
                    tweet.constructFromCursor(data);
                    tweets.add(tweet);
                }

                if(isTablet) {
                    getSearchAdapter600DP().setTweets(tweets.toArray(new Tweet[tweets.size()]));
                } else {
                    getSearchAdapter().setTweets(tweets.toArray(new Tweet[tweets.size()]));
                }
                this.tweetsLoaded = true;
                break;

            case LoaderIDs.youtube:
                ArrayList<Video> videos = new ArrayList<>();
                while (data.moveToNext()) {
                    Video video = new Video();
                    video.constructFromCursor(data);
                    videos.add(video);
                }

                if(isTablet) {
                    getSearchAdapter600DP().setVideos(videos.toArray(new Video[videos.size()]));
                } else {
                    getSearchAdapter().setVideos(videos.toArray(new Video[videos.size()]));
                }

                this.videosLoaded = true;
                break;
        }

        super.onLoadFinished(loader, data);
    }

    public void onLoadFinished600DP(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.session:
                List<EventTimingTwoItems> eventTimings = new ArrayList<>();
                while (data.moveToNext())
                {
                    EventTimingTwoItems etPair = new EventTimingTwoItems();

                    EventTiming e1 = new EventTiming();
                    e1.constructFromCursor(data);
                    etPair.event1 = e1;

                    if(data.moveToNext()){
                        EventTiming e2 = new EventTiming();
                        e2.constructFromCursor(data);
                        etPair.event2 = e2;
                    }

                    eventTimings.add(etPair);
                }
                getSearchAdapter600DP().setEventTimings(eventTimings.toArray(new EventTimingTwoItems[0]));
                this.sessionTimingsLoaded = true;
                break;

            case LoaderIDs.sponsor:
                List<SponsorTwoItems> sponsors = new ArrayList<>();
                while (data.moveToNext())
                {
                    SponsorTwoItems spPair = new SponsorTwoItems();
                    Sponsor sp1 = new Sponsor();
                    sp1.constructFromCursor(data);
                    spPair.sponsor1 = sp1;

                    if(data.moveToNext()) {
                        Sponsor sp2 = new Sponsor();
                        sp2.constructFromCursor(data);
                        spPair.sponsor2 = sp2;
                    }

                    sponsors.add(spPair);
                }
                getSearchAdapter600DP().setSponsors(sponsors.toArray(new SponsorTwoItems[0]));
                this.sponsorsLoaded = true;
                break;

            case LoaderIDs.speaker:
                List<SpeakerTwoItems> speakers = new ArrayList<>();
                while (data.moveToNext())
                {
                    SpeakerTwoItems spPair = new SpeakerTwoItems();
                    Speaker sp1 = new Speaker();
                    sp1.constructFromCursor(data);
                    spPair.speaker1 = sp1;

                    if(data.moveToNext()){
                        Speaker sp2 = new Speaker();
                        sp2.constructFromCursor(data);
                        spPair.speaker2 = sp2;
                    }

                    speakers.add(spPair);
                }
                getSearchAdapter600DP().setSpeakers(speakers.toArray(new SpeakerTwoItems[0]));
                this.speakersLoaded = true;
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        Boolean isTablet = SibosUtils.isGreaterThan600DP(getActivity().getApplicationContext());

        switch (loader.getId())
        {
            case LoaderIDs.speaker:
                this.speakersLoaded = false;
                if(isTablet) {
                    getSearchAdapter600DP().setSpeakers((SpeakerTwoItems[]) null);
                } else {
                    getSearchAdapter().setSpeakers(null);
                }
                break;

            case LoaderIDs.sponsor:
                this.sponsorsLoaded = false;
                if(isTablet) {
                    getSearchAdapter600DP().setSponsors((SponsorTwoItems[]) null);
                } else {
                    getSearchAdapter().setSponsors(null);
                }
                break;

            case LoaderIDs.twitter_name:
                this.tweetsLoaded = false;
                if(isTablet){
                    getSearchAdapter600DP().setTweets(null);
                } else {
                    getSearchAdapter().setTweets(null);
                }
                break;

            case LoaderIDs.youtube:
                this.videosLoaded = false;
                if(isTablet){
                    getSearchAdapter600DP().setVideos(null);
                } else {
                    getSearchAdapter().setVideos(null);
                }
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        if (speakersLoaded && sponsorsLoaded && videosLoaded && tweetsLoaded && sessionTimingsLoaded)
        {
            this.adapter.notifyDataSetChanged();
            return true;
        }

        return false;
    }

    @Override
    protected void destroyLoaders()
    {
        this.sessionLoader.destroyLoaders();
        this.getLoaderManager().destroyLoader(LoaderIDs.sponsor);
        this.getLoaderManager().destroyLoader(LoaderIDs.speaker);
        this.getLoaderManager().destroyLoader(LoaderIDs.twitter_name);
        this.getLoaderManager().destroyLoader(LoaderIDs.youtube);
    }




    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Intent intent;
        switch (this.adapter.getItemViewType(position)) {

            case FavoriteBaseAdapter.VIEWTYPE_SPEAKER:
                Speaker speaker = (Speaker) this.adapter.getItem(position);
                String speakerName = speaker.getFirstName()+" "+speaker.getLastName();
                viewLoader.loadView(SpeakerDetailsActivity.createIntent(getActivity(), String.valueOf(speaker.getSpeakerId()),speakerName), ViewLoaderStrategyInterface.ViewType.SPEAKERS);
                break;

            case FavoriteBaseAdapter.VIEWTYPE_SESSION:
                final EventTiming eventTiming = (EventTiming) this.adapter.getItem(position);
                final long sessionId = eventTiming.getEventId();
                viewLoader.loadView(SessionDetailsActivity.createIntent(getActivity(), sessionId), ViewLoaderStrategyInterface.ViewType.SESSIONS);
                break;

            case FavoriteBaseAdapter.VIEWTYPE_SPONSOR:

                Sponsor sponsor = (Sponsor) this.adapter.getItem(position);
                viewLoader.loadView(SponsorDetailsActivity.createIntent(getActivity(), sponsor),ViewLoaderStrategyInterface.ViewType.SPONSORS);
                break;

            case FavoriteBaseAdapter.VIEWTYPE_TWITTER:
                WallItem wallItem = (WallItem) this.adapter.getItem(position);
                final String tweetId = wallItem.getId();
                final String username = wallItem.getTitle();
                GAHelper.trackEvent(this.getView().getContext(), "twitter", "open_tweet", "");
                TwitterUtils.showTweet(this.getView().getContext(), tweetId, username);
                break;





            case FavoriteBaseAdapter.VIEWTYPE_VIDEO:
                wallItem = (WallItem) this.adapter.getItem(position);
                GAHelper.trackEvent(this.getView().getContext(), "video", "play", wallItem.getTitle());
                this.openMediaItem(wallItem.getLink());
                break;
        }
    }

    private void openMediaItem(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        this.startActivity(intent);
    }

    @Override
    public AdBaseAdapter getAdapter(Context context)
    {
        return (SibosUtils.isGreaterThan600DP(context)) ? new SearchBaseAdapter600DP(getActivity(), viewLoader)
                : new SearchBaseAdapter(getActivity());
    }

    @Override
    public void onRefresh()
    {

    }

    @Override
    public boolean isPullToRefreshEnabled()
    {
        return false;
    }

    public void searchTextChanged(String text)
    {
        this.searchText = text;
        startLoaders();
    }
}