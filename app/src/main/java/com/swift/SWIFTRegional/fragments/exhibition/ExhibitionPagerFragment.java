package com.swift.SWIFTRegional.fragments.exhibition;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.fragments.speakers.AbstractFilterPagerFragment;
import com.swift.SWIFTRegional.fragments.sponsor.SponsorListFragment;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.views.BlockableViewPager;

import java.util.ArrayList;

/**
 * Created by SimonRaes on 7/04/15.
 * Fragment that holds a viewpager that contains the exhibitorsList and floorPlan fragments.
 */
public class ExhibitionPagerFragment extends AbstractFilterPagerFragment
{

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        GAHelper.trackScreen(view.getContext(), "/exhibition/exhibitors");
        GAHelper.trackScreen(view.getContext(), "/sponsor/sponsors");
    }

    @Override
    protected int getLayout()
    {
        return R.layout.fragment_exhibition_viewpager;
    }

    @Override
    protected ArrayList<Fragment> getFragments()
    {
        ArrayList<Fragment> fragmentArray = new ArrayList<Fragment>();
        fragmentArray.add(new ExhibitorListFragment());
        fragmentArray.add(new SponsorListFragment());
        return fragmentArray;
    }

    @Override
    protected int getTitles()
    {
        return R.array.exhibitionTabTitles;
    }

    @Override
    protected void tabSelected(int position)
    {
        switch (position)
        {
            case 0:
                GAHelper.trackScreen(this.getView().getContext(), "/exhibition/exhibitors");
                ((BlockableViewPager)this.viewPager).setCanScroll(true);
                break;
            case 1:
                GAHelper.trackScreen(this.getView().getContext(), "/sponsor/sponsors");
                ((BlockableViewPager)this.viewPager).setCanScroll(false);
                break;
        }
    }
}
