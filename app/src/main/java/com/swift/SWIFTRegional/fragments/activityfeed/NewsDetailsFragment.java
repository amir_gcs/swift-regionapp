package com.swift.SWIFTRegional.fragments.activityfeed;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.swift.SWIFTRegional.contentproviders.NewsContentProvider;
import com.swift.SWIFTRegional.fragments.WebViewFragment;
import com.swift.SWIFTRegional.models.News;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.loaders.LoaderIDs;

@SuppressLint("ResourceType")
public class NewsDetailsFragment extends WebViewFragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    public static final String EXTRA_NEWS_ID = "news_id";

    public static final String HTML_WRAPPER = "<html>"
            + "<head>"
            + "<style type=\"text/css\">"
            + "* { margin: 0; padding: 0; } "
            + "body { font-size: 62.5%%; line-height: 1; font-family: 'Interstate-Light', Arial, sans-serif; padding: 17px 13px; background-color: %1$s; } "
            + "#container { color: #666; font-size: 1.4em; line-height: 1.6em; background-color: #FFF; box-shadow: 0 0 2px rgba(0, 0, 0, 0.2); box-sizing: border-box; padding: 20px; } "
            + "h1, h2, h3 { margin-bottom: 10px; color: #000; font-family: 'Interstate-Regular', 'Interstate-Light', Arial, sans-serif; line-height: 1.5em; } "
            + "h1 { font-size: 1.4em; color: %2$s; line-height: 1.2em; } "
            + "h2 { font-size: 1.1em; color: #555; } "
            + "h3 { font-size: 1em; color: #444; } "
            + "p { margin-bottom: 10px; } "
            + "a { color: %3$s; } "
            + "p + h1, p + h2, p + h3 { margin-top: 20px; } "
            + "img { margin: 0 5px 5px 5px; } "
            + "ol { margin: 17px 20px; } "  // top and bottom margin are 17px, left and right 20px
            + "ul { margin: 17px 20px; } "  // top and bottom margin are 17px, left and right 20px
            + "</style>"
            + "</head>"
            + "<body>"
            + "<div id=\"container\">"
            + "<h1>%4$s</h1>"
            + "<h3>%5$s</h3>"
            + "%6$s"
            + "</div>"
            + "</body>";

    private News news;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        this.setMargins();

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        final String newsId = this.getArguments() != null
                ? this.getArguments().getString(EXTRA_NEWS_ID)
                : null;

        if (!TextUtils.isEmpty(newsId))
        {
            final Bundle args = this.getArguments();
            args.putString(EXTRA_NEWS_ID, newsId);

            this.getLoaderManager().restartLoader(LoaderIDs.news, args, this);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_program, menu);

        menu.removeItem(R.id.action_favorite);
        menu.removeItem(R.id.action_unfavorite);

        final MenuItem menuItem = menu.findItem(R.id.action_share);
        menuItem.setVisible(this.news != null && !TextUtils.isEmpty(this.news.getLink()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_share:

                this.shareNews();
                return true;
        }

        return false;
    }

    private void shareNews()
    {
        final StringBuilder sb = new StringBuilder(this.news.getTitle())
                .append(" ")
                .append(this.news.getLink());

        final Intent intent = ShareCompat.IntentBuilder
                .from(this.getActivity())
                .setType("text/plain")
                .setText(sb.toString())
                .getIntent();

        this.startActivity(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        this.setMargins();
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final String newsId = args.getString(EXTRA_NEWS_ID);

        return new CursorLoader(this.getActivity(), Uri.withAppendedPath(NewsContentProvider.CONTENT_URI, "news/" + newsId), null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.news:

                if (data.moveToFirst())
                {
                    this.news = new News();
                    news.constructFromCursorForDetail(data);

                    this.getActivity().supportInvalidateOptionsMenu();

                    final String lightGray = this.getString(R.color.general_window_background_gray).replace("#ff", "#");
                    final String orange = this.getString(R.color.orange).replace("#ff", "#");

                    String originalHtml = news.getHtmlContent();
                    //fix urls
                    originalHtml = originalHtml.replace("href=\"/", "href=\"https://www.sibos.com/");

                    final String html = String.format(HTML_WRAPPER, lightGray, orange, orange, news.getTitle(), news.getDescription(), originalHtml);

                    this.webView.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);

                    // if item is a storify news item, open it in a separate browser and try to close the current fragment
                    if (this.news.isStorifyItem())
                    {
                        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.news.getLink()));
                        this.startActivity(intent);
                        if (this.getActivity() != null)
                        {
                            this.getActivity().finish();
                        }
                    }
                }

                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
    }

    private void setMargins()
    {
        Resources resources = getActivity().getResources();

        boolean is7inchTablet = resources.getBoolean(R.bool.is_7inch_tablet);
        boolean is10inchTablet = resources.getBoolean(R.bool.is_10inch_tablet);

        if (is10inchTablet || is7inchTablet)
        {
            int leftPadding = resources.getDimensionPixelOffset(R.dimen.padding_left);
            int rightPadding = resources.getDimensionPixelOffset(R.dimen.padding_right);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) this.webView.getLayoutParams();
            params.setMargins(leftPadding, 0, rightPadding, 0);
            this.webView.setLayoutParams(params);
        }
    }

    @Override
    protected void setVisibilities()
    {
        setVisibilities(true);
    }
}
