package com.swift.SWIFTRegional.fragments.activityfeed;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.activities.activityfeed.FlickrPhotosActivity;
import com.swift.SWIFTRegional.adapters.activityfeed.FlickrPhotosCursorAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.models.flickr.FlickrPhoto;
import com.swift.SWIFTRegional.receivers.NetworkStateReceiver;

import com.goodcoresoftware.android.common.utils.Connectivity;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr.FlickrPhotosApiCall;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.views.CustomGridView;

/**
 * Created by Wesley on 30/05/14.
 */
public class FlickrThumbsFragment extends AbstractFlickrPhotosetFragment implements AdapterView.OnItemClickListener, NetworkStateReceiver.OnNetworkStateChangedListener
{
    private CustomGridView gridView;
    private FlickrPhotosCursorAdapter adapter;

    private boolean hasConnection;
    private NetworkStateReceiver networkStateReceiver;

    public static Bundle createBundle(final Intent intent)
    {
        final Bundle bundle = new Bundle();
        bundle.putString(AbstractFlickrPhotosetFragment.EXTRA_PHOTOSET_ID, intent.getStringExtra(EXTRA_PHOTOSET_ID));
        bundle.putString(AbstractFlickrPhotosetFragment.EXTRA_PHOTOSET_TITLE, intent.getStringExtra(EXTRA_PHOTOSET_TITLE));
        return bundle;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.hasConnection = Connectivity.hasConnection(activity);

        if (!hasConnection)
        {
            this.networkStateReceiver = new NetworkStateReceiver();
            this.getActivity().registerReceiver(this.networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            this.networkStateReceiver.setOnNetworkStateChangedListener(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_gridview_gallery, container, false);
        View viewNoConnection = inflater.inflate(R.layout.view_empty_no_connection, container, false);

        // Add a blank empty view
        if (viewNoConnection == null)
            viewNoConnection = new View(getActivity());

        if(hasConnection) {
            this.gridView = (CustomGridView) view.findViewById(R.id.gridView);
            this.adapter = new FlickrPhotosCursorAdapter(view.getContext());
            this.gridView.setAdapter(this.adapter);
            final int columns = view.getContext().getResources().getInteger(R.integer.flickr_thumbs_columns);
            this.adapter.setColumns(columns);
        } else {
            return viewNoConnection;
        }
        
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState)
    {
        if(hasConnection) {
            super.onViewCreated(view, savedInstanceState);
            GAHelper.trackScreen(view.getContext(), "/activity/pictures/" + this.photosetTitle);
            this.gridView.setOnItemClickListener(this);
            this.doApiCall();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        final int columns = this.getView().getContext().getResources().getInteger(R.integer.flickr_thumbs_columns);
        this.gridView.setNumColumns(columns);
        this.adapter.setColumns(columns);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final FlickrPhoto photo = this.adapter.getItem(position);

        if(photo != null)
        {
            final Intent intent = FlickrPhotosActivity.getIntent(view.getContext(), this.photosetId, this.photosetTitle, position);

            this.startActivity(intent);
        }
    }

    @Override
    protected void hideLayout()
    {
        if(this.gridView != null)
            this.gridView.setVisibility(View.GONE);
    }

    @Override
    protected void showLayout()
    {
        if(this.gridView != null)
            this.gridView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void swapCursor(Cursor data)
    {
        if(this.adapter != null)
            this.adapter.swapCursor(data);
    }

    private void doApiCall()
    {
        final Bundle bundle = new Bundle();
        bundle.putString(FlickrPhotosApiCall.PARAM_PHOTOSET_ID, this.photosetId);

        this.startApiCallForResult(Api.flickr_photos, FlickrPhotosApiCall.class, bundle);
    }

    @Override
    public void onNetworkConnected()
    {
        this.hasConnection = true;
    }

    @Override
    public void onNetworkDisconnected()
    {
        this.hasConnection = false;
    }
}
