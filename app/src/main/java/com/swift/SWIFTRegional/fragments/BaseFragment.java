package com.swift.SWIFTRegional.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.swift.SWIFTRegional.activities.BaseActivity;
import com.swift.SWIFTRegional.api.apiservice.SibosApiMessenger;

import com.goodcoresoftware.android.common.framework.app.ITPBaseFragment;
import com.goodcoresoftware.android.common.http.api.ApiCall;
import com.goodcoresoftware.android.common.http.api.utils.ApiMessenger;
import com.swift.SWIFTRegional.R;

public abstract class BaseFragment extends ITPBaseFragment
        implements ApiMessenger.ApiMessengerListener, ApiMessenger.ApiMessengerWrapper
{
    private static final int USER_TYPE_CUSTOM_DIMENSION_INDEX = 1;
    private static final int BUSINESS_FOCUS_CUSTOM_DIMENSION_INDEX = 2;
    private static final int INSTITUTION_TYPE_CUSTOM_DIMENSION_INDEX = 3;

    private SibosApiMessenger apiMessenger;

    private Toast toast;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Back button fix.
//        ViewGroup parent = (ViewGroup)getActivity().findViewById(R.id.content_frame);
//        View v =  parent.findViewById(R.id.no_data_view);
//        if(v!=null)
//        parent.removeView(v);

    }

    @Override
    public void onAttach(final Activity activity)
    {
        super.onAttach(activity);

        // Workaround for an Android 4.4 bug (have not encountered it on other versions yet).
        // When using the support Toolbar on a fragment that has no OptionsMenu, no click event will be fired when tapping the up arrow.
        // The workaround is to inflate an empty menu on these devices.
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT)
        {
            setHasOptionsMenu(true);
        }

        this.apiMessenger = new SibosApiMessenger(activity, this);
        this.apiMessenger.create();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        this.apiMessenger.destroy();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        //        RefWatcher refWatcher = SibosApplication.getRefWatcher(getActivity());
        //        if (refWatcher != null)
        //        {
        //            refWatcher.watch(this);
        //        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        // Empty menu to fix up arrow bug.
        inflater.inflate(R.menu.menu_empty, menu);

    }

    @Override
    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass)
    {
        if (this.apiMessenger != null)
        {
            return this.apiMessenger.startApiCallForResult(inRequestCode, aClass);
        }
        return false;
    }

    @Override
    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass, Bundle inHttpRequestParams)
    {
        if (this.apiMessenger != null)
        {
            return this.apiMessenger.startApiCallForResult(inRequestCode, aClass, inHttpRequestParams);
        }
        return false;
    }

    @Override
    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass, Bundle inHttpRequestParams, Bundle inConstructorParams)
    {
        if (this.apiMessenger != null)
        {
            return this.apiMessenger.startApiCallForResult(inRequestCode, aClass, inHttpRequestParams, inConstructorParams);
        }
        return false;
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {

    }

    /**
     * Show toast messageThreads for a short duration.
     *
     * @param inText id of string resource to be displayed.
     */
    protected void showToast(int inText)
    {
        this.showToast(inText, Toast.LENGTH_SHORT);
    }

    /**
     * Show toast messageThreads for a defined duration.
     *
     * @param inText     id of string resource to be displayed.
     * @param inDuration length that messageThreads should be shown
     */
    protected void showToast(int inText, int inDuration)
    {
        if (this.getActivity() != null)
        {
            if (this.toast == null)
            {
                this.toast = Toast.makeText(this.getActivity(), inText, inDuration);
                this.toast.setGravity(Gravity.CENTER, 0, 0);
            }
            else
            {
                this.toast.setText(inText);
            }

            this.toast.show();
        }
    }

    public void showProgressDialog() {
        ( (BaseActivity)getActivity() ).showProgressDialog();
    }

    public void dismissProgressDialog() {

        ( (BaseActivity)getActivity() ).dismissProgressDialog();
    }
}