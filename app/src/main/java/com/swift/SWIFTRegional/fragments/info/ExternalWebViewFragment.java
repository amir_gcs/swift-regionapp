package com.swift.SWIFTRegional.fragments.info;

import android.os.Bundle;
import android.view.View;

import com.swift.SWIFTRegional.fragments.WebViewFragment;

/**
 * Created by Wesley on 17/06/14.
 */
public class ExternalWebViewFragment extends WebViewFragment
{
    public static final String EXTRA_URL = "extraUrl";

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        final String url = this.getArguments().getString(EXTRA_URL);

        if(url != null)
        {
            this.webView.loadUrl(url);
        }
    }

    public boolean onBackPressed()
    {
        if(this.webView.canGoBack())
        {
            this.webView.goBack();

            return true;
        }

        return false;
    }

    @Override
    protected void setVisibilities()
    {
        setVisibilities(true);
    }
}
