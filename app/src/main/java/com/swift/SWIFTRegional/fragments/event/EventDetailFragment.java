package com.swift.SWIFTRegional.fragments.event;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.text.TextUtilsCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.fragments.BaseFragment;
import com.swift.SWIFTRegional.models.event.Event;
import com.swift.SWIFTRegional.utils.DateUtils;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.views.TextView;

public class EventDetailFragment extends BaseFragment
{
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        ((MainActivity)getActivity()).ChangeButton(true);
        View view= inflater.inflate(R.layout.fragment_selectedevent_details,container,false);
        Event event= MainActivity.selectedEventProvider.getSelectedEvent();

        ((MainActivity)getActivity()).setToolbarText("Welcome");
        RelativeLayout mainLayout = (RelativeLayout) view.findViewById(R.id.compView_TitleNImage);
        if(event.getPictureUrl()!="")
        {
            applyBackgroundImage(mainLayout,event.getPictureUrl());
        }
        else
        {
            mainLayout.setBackgroundResource(R.color.no_image_color);
        }
        //input details and data
        TextView titleView=(TextView)mainLayout.findViewById(R.id.textView_eventTitle);
        titleView.setText(event.getTitle());
        titleView.setTextColor(Color.WHITE);



        TextView startDateView=(TextView)mainLayout.findViewById(R.id.textView_eventStartDate);

        String formatedStartDate = DateUtils.formattedDateFromString("","",event.getStartDate());
        String formatedEndDate = DateUtils.formattedDateFromString("","",event.getEndDate());
        if(formatedStartDate.equals(formatedEndDate))
        {
            startDateView.setText(formatedStartDate);
        }else
        {
            startDateView.setText(formatedStartDate +"-"+formatedEndDate);
        }

        startDateView.setTextColor(Color.WHITE);

        TextView categoryView=(TextView)mainLayout.findViewById(R.id.textView_eventCategory);
        categoryView.setText((TextUtils.isEmpty(event.getTypeName())?"":event.getTypeName()));
        categoryView.setTextColor(Color.WHITE);

        TextView locationView=(TextView)view.findViewById(R.id.textView_eventLocation);
        locationView.setText(event.getLocation());
        if(event.getLocation()== null || event.getLocation().equals(""))
        {

            ((LinearLayout) view.findViewById(R.id.location_comp)).setVisibility(View.GONE);
        }
        else
        {
            ((LinearLayout) view.findViewById(R.id.location_comp)).setVisibility(View.VISIBLE);

        }



        TextView descView=(TextView)view.findViewById(R.id.textView_eventDescription);
        descView.setText(event.getDescription());

        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return view;
    }


    private void applyBackgroundImage(final RelativeLayout linearLayout, String imageUrl)
    {
        try
        {

            Target t=  new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                             /* Save the bitmap or do something with it here */
try
{
    BitmapDrawable bitmapDrawable=new BitmapDrawable(getResources(),bitmap);
//    ColorMatrix matrix = new ColorMatrix();
//    matrix.setSaturation(0);
//
//    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
    //bitmapDrawable.setColorFilter(filter);
    //Set it in the ImageView
    linearLayout.setBackground(bitmapDrawable);

}
catch (Exception e)
{

}

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    if(placeHolderDrawable==null)
                    {

                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    if(errorDrawable==null)
                    {

                    }

                    linearLayout.setBackgroundResource(R.color.no_image_color);
                }
            };
            linearLayout.setTag(t);

            Picasso.with(getActivity())
                    .load(imageUrl)
                    .into(t);

        }
        catch(Exception e)
        {
            Log.e("imageLoad",e.toString());
        }

    }




}
