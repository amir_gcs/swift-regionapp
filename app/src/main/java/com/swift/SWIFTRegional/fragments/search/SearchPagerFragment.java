package com.swift.SWIFTRegional.fragments.search;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.search.SearchActivity;
import com.swift.SWIFTRegional.fragments.AbstractPagerFragment;

import com.goodcoresoftware.android.common.utils.SearchViewUtils;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.ToolbarUtils;

import java.util.ArrayList;


public class SearchPagerFragment extends AbstractPagerFragment implements SearchView.OnQueryTextListener
{
    private SearchView searchView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        GAHelper.trackScreen(view.getContext(), "/search/all");
        ToolbarUtils.updateToolbarColor((SearchActivity)getActivity(),R.color.region_app_backgroundcolor);
        ((SearchActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.region_app_backgroundcolor)));


    }

    @Override
    protected int getLayout()
    {
        return R.layout.fragment_search;
    }

    @Override
    protected ArrayList<Fragment> getFragments()
    {
        ArrayList<Fragment> fragmentArray = new ArrayList<Fragment>();
        fragmentArray.add(new AllSearchResultFragment());
        return fragmentArray;
    }


    @Override
    protected int getTitles()
    {
        return R.array.searchTabTitles;
    }

    @Override
    protected void tabSelected(int position)
    {
        switch (position)
        {
            case 0:
                GAHelper.trackScreen(this.getView().getContext(), "/search/all");
                break;
            case 1:
                GAHelper.trackScreen(this.getView().getContext(), "/search/favourites");
                break;
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchMenuItem = menu.findItem(R.id.home_search);
        this.searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);

        final View searchPlate = SearchViewUtils.getSearchViewEditTextFrame(MenuItemCompat.getActionView(searchMenuItem));
        if (searchPlate != null)
        {
            searchPlate.setBackgroundResource(R.drawable.textfield_searchview_holo_dark);
        }

        this.searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener()
        {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item)
            {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item)
            {
                if (SearchPagerFragment.this.getActivity() != null)
                {
                    SearchPagerFragment.this.getActivity().finish();

                    return true;
                }

                return false;
            }
        });
        MenuItemCompat.expandActionView(searchMenuItem);
    }

    @Override
    public boolean onQueryTextSubmit(String text)
    {
        this.searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String text)
    {
        // Remove "/" signs so queries with a "/" don't link to a non-existing URI.
        text = text.replace("/", " ");
        searchText(text);
        return true;
    }

    private void searchText(String text)
    {
        if (TextUtils.isEmpty(text))
        {
            return;
        }

        int fragmentCount = this.pagerAdapter.getCount();
        for (int i = 0; i < fragmentCount; i++)
        {
            if (this.pagerAdapter.getItem(i) instanceof AbstractSearchResultFragment)
            {
                AbstractSearchResultFragment fragment = (AbstractSearchResultFragment) this.pagerAdapter.getItem(i);
                fragment.searchTextChanged(text);
            }
        }
    }
}