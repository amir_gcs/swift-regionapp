package com.swift.SWIFTRegional.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.swift.SWIFTRegional.R;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Wesley on 23/05/14.
 */
public abstract class AbstractQuickScrollListFragment<T extends BaseAdapter> extends AbstractDefaultListFragment<T, StickyListHeadersListView>
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View v = super.onCreateView(inflater, container, savedInstanceState);

        this.listView.setFastScrollEnabled(true);
        this.listView.setOnItemClickListener(this);

        return v;
    }

    @Override
    public int getLayoutResourceId()
    {
        return R.layout.fragment_list_quick_scroll;
    }

}
