package com.swift.SWIFTRegional.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.swift.SWIFTRegional.R;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public abstract class AbstractDefaultListFragment<T extends BaseAdapter, S> extends AbstractListFragment<S>
{
    protected T adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View v = super.onCreateView(inflater, container, savedInstanceState);

        this.adapter = this.getAdapter(v.getContext());

        beforeSetAdapter(inflater);

        if (this.listView instanceof ListView)
        {
            ((ListView) this.listView).setAdapter(this.adapter);
        }
        else if (this.listView instanceof StickyListHeadersListView && this.adapter instanceof StickyListHeadersAdapter)
        {
            ((StickyListHeadersListView) this.listView).setAdapter((StickyListHeadersAdapter) this.adapter);
        }


        return v;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);

        if (this.shouldApplyPadding())
        {
            if (this.listView instanceof ListView)
            {
                ((ListView) listView).setPadding(
                        getResources().getDimensionPixelSize(R.dimen.padding_listview),
                        ((ListView) listView).getPaddingTop(),
                        getResources().getDimensionPixelSize(R.dimen.padding_listview),
                        ((ListView) listView).getPaddingBottom());
            }
            else if (this.listView instanceof StickyListHeadersListView && this.adapter instanceof StickyListHeadersAdapter)
            {
                ((StickyListHeadersListView) listView).setPadding(
                        getResources().getDimensionPixelSize(R.dimen.padding_listview),
                        ((StickyListHeadersListView) listView).getPaddingTop(),
                        getResources().getDimensionPixelSize(R.dimen.padding_listview),
                        ((StickyListHeadersListView) listView).getPaddingBottom());
            }
        }
    }

    protected boolean shouldApplyPadding()
    {
        return true;
    }

    /**
     * Allows to implement functionality before the adapter is set, e.g. adding header and footer views.
     */
    protected void beforeSetAdapter(LayoutInflater inflater)
    {
    }

    @Override
    public AdapterState isAdapterLoaded()
    {
        if (adapter != null)
        {
            if (adapter.getCount() == 0)
            {
                return AdapterState.EMPTY;
            }
            else
            {
                return AdapterState.LOADED;
            }
        }

        return AdapterState.NOT_LOADED;
    }

    public abstract T getAdapter(Context context);
}