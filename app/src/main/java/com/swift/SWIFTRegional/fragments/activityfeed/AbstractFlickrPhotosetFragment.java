package com.swift.SWIFTRegional.fragments.activityfeed;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ProgressBar;

import com.swift.SWIFTRegional.contentproviders.FlickrContentProvider;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.fragments.BaseFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;

/**
 * Created by Wesley on 30/05/14.
 */
public abstract class AbstractFlickrPhotosetFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    public static final String EXTRA_PHOTOSET_ID = "extraPhotosetId";
    public static final String EXTRA_PHOTOSET_TITLE = "extraPhotosetTitle";

    private ProgressBar progressBar;

    protected String photosetId;
    protected String photosetTitle;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        this.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        this.photosetId = this.getArguments().getString(EXTRA_PHOTOSET_ID);
        this.photosetTitle = this.getArguments().getString(EXTRA_PHOTOSET_TITLE);

        final Bundle bundle = new Bundle();
        bundle.putString(EXTRA_PHOTOSET_ID, this.photosetId);
        bundle.putString(EXTRA_PHOTOSET_TITLE, this.photosetTitle);

        this.getLoaderManager().restartLoader(LoaderIDs.flickr_photos, bundle, this);

        this.showProgressBar();
    }

    protected abstract void hideLayout();
    protected abstract void showLayout();

    protected abstract void swapCursor(Cursor data);

    private void showProgressBar()
    {
        this.hideLayout();

        this.progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar()
    {
        this.showLayout();

        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        switch(id)
        {
            case LoaderIDs.flickr_photos:

                final Uri uri = Uri.withAppendedPath(FlickrContentProvider.CONTENT_URI_PHOTO, args.getString(EXTRA_PHOTOSET_ID));

                return new CursorLoader(this.getView().getContext(), uri, null, null, null, null);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch(loader.getId())
        {
            case LoaderIDs.flickr_photos:

                this.swapCursor(data);

                if(data.getCount() > 0)
                {
                    this.hideProgressBar();
                }

                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch(loader.getId())
        {
            case LoaderIDs.flickr_photos:

                this.swapCursor(null);

                break;
        }
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch(inRequestCode)
        {
            case Api.flickr_photos:

                this.hideProgressBar();

                break;
        }
    }
}
