package com.swift.SWIFTRegional.fragments.speakers;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.activities.speakers.SpeakerDetailsActivity;
import com.swift.SWIFTRegional.adapters.SpeakerDetailAdapter;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.utils.PicassoUtils;

import java.util.ArrayList;
import java.util.Arrays;

import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.views.TextView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSpeakerCallback;
import com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider;
import com.swift.SWIFTRegional.contentproviders.RateContentProvider;
import com.swift.SWIFTRegional.contentproviders.SessionSpeakerContentProvider;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.contentproviders.UnsentRateContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.fragments.persons.AbstractPersonFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.Rate;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.SessionLoader;


public class SpeakerDetailsFragment extends AbstractPersonFragment<SpeakerDetailAdapter> implements SessionLoader.SessionLoaderListener
{
    public static final String EXTRA_SPEAKER_ID = "speaker_id";
    public static final String PARTICIPANT_KEY = "participant_key";
    private static final String EXTRA_SESSION_ID = "session_id";
    public static final String EXTRA_SPEAKER_NAME = "speaker_name";
    private SessionLoader sessionLoader;

    private String speakerId;
    private String speakerName;

    private boolean favoriteLoaded;
    private boolean speakerLoaded;

    //private Speaker speaker;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.speakerId = this.getArguments() != null ? this.getArguments().getString(EXTRA_SPEAKER_ID) : null;
        this.speakerName = this.getArguments() != null ? this.getArguments().getString(EXTRA_SPEAKER_NAME) : null;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.removeItem(R.id.action_share);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        GAHelper.trackScreen(view.getContext(), "/programme/speakers/" + speakerId);
        ((SpeakerDetailsActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.region_app_backgroundcolor)));

    }

    @Override
    protected int getHeaderColor()
    {
        return getActivity().getResources().getColor(R.color.region_app_backgroundcolor);
    }

    @Override
    public SpeakerDetailAdapter getAdapter(Context context)
    {
        return new SpeakerDetailAdapter(context, this, this);
    }

    @Override
    public void startApiCalls()
    {
        if (super.startApiCalls(false))
        {

            if (this.userIsLoggedIn)
            {
                // Check if any ratings were already submitted for this speaker.
                this.loadRating();
            }
        }
    }

    @Override
    public void startLoaders()
    {

        if (!TextUtils.isEmpty(this.speakerId))
        {
            final Bundle args = this.getArguments();
            args.putString(EXTRA_SPEAKER_ID, this.speakerId);

            this.getLoaderManager().restartLoader(LoaderIDs.speaker, args, this);

            if (this.userIsLoggedIn)
            {
                this.getLoaderManager().restartLoader(LoaderIDs.favorite, args, this);
                this.getLoaderManager().restartLoader(LoaderIDs.speaker_rate, args, this);
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        final String speakerId = args.getString(EXTRA_SPEAKER_ID);
        if (!TextUtils.isEmpty(speakerId) && this.getView() != null)
        {
            switch (id)
            {
                case LoaderIDs.speaker_rate:

                    return new CursorLoader(this.getView().getContext(), Uri.withAppendedPath(RateContentProvider.CONTENT_URI_SPEAKER, speakerId), null, null, null, null);

                case LoaderIDs.speaker_unsent_rate:

                    return new CursorLoader(this.getView().getContext(), Uri.withAppendedPath(UnsentRateContentProvider.CONTENT_URI_UNSENT_SPEAKER_RATE, speakerId), null, null, null, null);

                case LoaderIDs.speaker:

                    return new CursorLoader(this.getView().getContext(), Uri.withAppendedPath(SpeakerContentProvider.CONTENT_URI_SPEAKER, speakerId), null, null, null, null);

                case LoaderIDs.favorite:

                    return new CursorLoader(this.getView().getContext(), Uri.withAppendedPath(FavoriteContentProvider.CONTENT_URI_FAVORITE, FavoriteTable.FavouriteType.SPEAKER.ordinal() + "/" + speakerId), null, null, null, null);

            }
        }


        final String sessionId = args.getString(EXTRA_SESSION_ID);

        if (!TextUtils.isEmpty(speakerId))
        {
            return new CursorLoader(this.getActivity(), Uri.withAppendedPath(SessionSpeakerContentProvider.CONTENT_URI_SESSION, sessionId), null, null, null, null);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {

            case LoaderIDs.speaker:
                this.speakerLoaded = true;
                final Speaker speaker = new Speaker();
                if (data.moveToFirst())
                {
                    speaker.constructFromCursor(data);
                }
                this.speaker = speaker;
                this.adapter.setSpeaker(this.speaker);
                this.sibosKey = speaker.getSibosKey();
                this.enableMessagingButtonIfAllowed();
                this.loadSpeakerSession();
                this.loadParticipantInfo();
                this.setSpeakerHeader();
                break;

            case LoaderIDs.favorite:
                this.favoriteLoaded = true;
                this.isFavorite = data.getCount() > 0;
                this.getActivity().supportInvalidateOptionsMenu();
                break;


            case LoaderIDs.speaker_rate:
                if (data.moveToFirst())
                {
                    this.createRateAndNotify(data, false);
                }
                else
                {
                    // If there is no submitted rating, check if there are any stored ratings that haven't been submitted yet
                    final Bundle args = this.getArguments();
                    args.putString(EXTRA_SPEAKER_ID, this.speakerId);
                    this.getLoaderManager().restartLoader(LoaderIDs.speaker_unsent_rate, args, this);
                }

                break;

            case LoaderIDs.speaker_unsent_rate:
                if (data.moveToFirst())
                {
                    createRateAndNotify(data, true);
                }

                break;
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    public void sessionsLoaded(EventTiming[] eventTimings)
    {
        if (eventTimings != null)
        {
            ArrayList<EventTiming> eventTimingArrayList = new ArrayList<>(Arrays.asList(eventTimings));
            ((TextView) getActivity().findViewById(R.id.textview_speaker_details_sessionsTitle))
                    .setVisibility(View.VISIBLE);
            this.adapter.setSessions(eventTimingArrayList);
        }
        else
        {
            this.adapter.setSessions(null);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.favorite:
                this.favoriteLoaded = false;
                break;
            case LoaderIDs.speaker:
                this.speakerLoaded = false;
                this.adapter.setSpeaker(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return true;
    }

    @Override
    protected void destroyLoaders()
    {
        if (this.sessionLoader != null)
        {
            this.sessionLoader.destroyLoaders();
        }

        this.getLoaderManager().destroyLoader(LoaderIDs.favorite);
        this.getLoaderManager().destroyLoader(LoaderIDs.speaker);
        this.getLoaderManager().destroyLoader(LoaderIDs.speaker_sessions);
        this.getLoaderManager().destroyLoader(LoaderIDs.session_speakers);
        this.getLoaderManager().destroyLoader(LoaderIDs.speaker_rate);
    }

    /**
     * Loads the speaker's sessions once the speaker himself has been loaded.
     */
    private void loadSpeakerSession()
    {
        if (!this.favoriteLoaded && !this.speakerLoaded)
        {
            return;
        }

        if (this.sessionLoader == null)
        {
            this.sessionLoader = new SessionLoader(getActivity(), getLoaderManager(), this);
        }

        this.sessionLoader.getEventTimings(SessionLoader.UriMode.SPEAKER, this.speakerId, "");
    }

    /**
     * Set speaker info in the fixed header.
     */
    private void setSpeakerHeader()
    {
        /*if (delegate != null){
            this.textViewName.setText(this.delegate.getFirstName()+" "+this.delegate.getLastName());
        }*/
        this.textViewName.setText(this.getName().trim().equalsIgnoreCase("")?speakerName:this.getName());
        this.textViewBusinessFunction.setText(this.speaker.getTitle());
        this.textViewCompanyName.setText(this.speaker.getCompanyName());
        PicassoUtils.loadPersonImage(this.speaker.getPictureUrl(), this.imageViewPicture);
    }

    @Override
    protected String getName()
    {
        return this.speaker.getFirstName() + " " + this.speaker.getLastName();
    }

    private void loadParticipantInfo()
    {
        if (!this.speakerLoaded)
        {
            return;
        }

        if (!StringUtils.isEmpty(this.sibosKey) && this.userIsLoggedIn)
        {
            final Bundle args = this.getArguments();
            args.putString(PARTICIPANT_KEY, this.getSibosKey());
        }
    }


    private void loadRating()
    {
        if (!TextUtils.isEmpty(this.speakerId))
        {
            ApiManager.getInstance(this.getActivity()).getRatingSpeaker(new GetRatingSpeakerCallback(this.getActivity(), this.speakerId));
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        EventTiming eventTiming = this.adapter.getItem(position);
        if (eventTiming != null)
        {
            final Intent intent = SessionDetailsActivity.createIntent(parent.getContext(), eventTiming.getEventId());
            this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.SESSIONS);
        }
    }

    @Override
    protected void share()
    {
        Speaker speaker = this.adapter.getSpeaker();
        if (speaker != null)
        {
            GAHelper.trackEvent(this.getView().getContext(), "share", "share", "speaker_" + this.speakerId);

            StringBuilder sb = new StringBuilder();

            if (!TextUtils.isEmpty(speaker.getFirstName()))
            {
                sb.append(speaker.getFirstName()).append(" ");
            }
            if (!TextUtils.isEmpty(speaker.getLastName()))
            {
                sb.append(speaker.getLastName()).append("\n");
            }
            if (!TextUtils.isEmpty(speaker.getBiography()))
            {
                sb.append(speaker.getBiography()).append("\n");
            }

            sb.append(this.getString(R.string.share_speaker_url));
            sb.append(speaker.getSpeakerId()).append("\n");

            sb.append(this.getString(R.string.twitter_search_hashtag));

            final Intent intent = ShareCompat.IntentBuilder.from(this.getActivity()).setType("text/plain").setText(sb.toString()).getIntent();

            this.startActivity(intent);
        }
    }

    @Override
    protected String getSibosKey()
    {
        return this.sibosKey;
    }

    @Override
    protected void setAsFavourite(final boolean set)
    {
        if (!this.sendingFavourite && this.getView() != null)
        {
            GAHelper.trackEvent(this.getView().getContext(), "favourite", set ? "favourite" : "unfavourite", "speaker_" + this.speakerId);

            SpeakerDetailsFragment.this.isFavorite = set;
            SpeakerDetailsFragment.this.getActivity().supportInvalidateOptionsMenu();

            this.sendingFavourite = true;

            ApiManager.getInstance(this.getActivity()).setFavouriteSpeaker(new SetFavouriteSpeakerCallback(this.getActivity(), this.speakerId, !set)
            {
                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);

                    SpeakerDetailsFragment.this.sendingFavourite = false;
                }
            });
        }
    }

    @Override
    public void onRefresh()
    {
        // Not refreshable
    }

    @Override
    public void ratingSelected(final int rating)
    {
        ApiManager.getInstance(this.getActivity()).setRatingSpeaker(new SetRatingSpeakerCallback(this.getActivity(), this.speakerId, rating));
    }

    protected void createRateAndNotify(Cursor inData, boolean inIsUnsentRate)
    {
        final Rate rate = new Rate();
        rate.constructFromCursor(inData);
        rate.setUnsent(inIsUnsentRate);
        this.adapter.setRate(rate);
        this.adapter.notifyDataSetChanged();
    }
}

