package com.swift.SWIFTRegional.fragments.activityfeed;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.swift.SWIFTRegional.api.apicalls.activityfeed.IssuesApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr.FlickrAccountApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr.FlickrPhotosetApiCall;
import com.swift.SWIFTRegional.api.apiservice.SibosApiMessenger;

import com.goodcoresoftware.android.common.http.api.ApiCall;
import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.http.api.utils.ApiMessenger;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.NewsApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.instagram.InstagramApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterHashTagApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterNameApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterTokenApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.youtube.YouTubeVideosApiCall;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Zeeshan on 5/13/2016.
 */
public class WallApiHelper implements ApiMessenger.ApiMessengerListener {

    private static WallApiHelper instance;

    private Context context;

    private int apiCallCount = 0;

    private SibosApiMessenger apiMessenger;

    private WallApiHelper(Context context) {
        this.context = context;
        this.apiMessenger = new SibosApiMessenger(context, this);
        this.apiMessenger.create();

    }

    public static WallApiHelper getInstance(Context context) {
        if(instance == null){
            instance = new WallApiHelper(context);
        }
        return instance;
    }

    public void startApiCalls()
    {
        //dont do anything here
        //this.doApiCalls(false);
    }

    private void doApiCalls(boolean force)
    {
        if (true /*super.startApiCalls(force)*/)
        {
            this.apiCallCount = 0;

            this.doTwitterApiCalls(force);

            //this.doFlickrApiCalls(force);

            //this.doInstagramApiCall();

            this.doYoutubeApiCall(force);

            //this.doIssuesApiCall(force);

            //this.doNewsApiCall(force);
        }
    }

    private void doTwitterApiCalls(boolean force)
    {
        this.apiCallCount++;

//        if (PreferencesHelper.containsTwitterToken(context))
//        {
            final Bundle requestParamsTwitter = new Bundle();

            requestParamsTwitter.putString(TwitterNameApiCall.PARAM_SEARCH_NAME, this.getString(R.string.twitter_search_name));

            this.startApiCallForResult(Api.twitter_name, TwitterNameApiCall.class, requestParamsTwitter);
            this.startApiCallForResult(Api.twitter_hashtag, TwitterHashTagApiCall.class);
//        }
//        else
//        {
//            this.startApiCallForResult(Api.twitter_token, TwitterTokenApiCall.class);
//        }
    }

    private void doFlickrApiCalls(boolean force)
    {
        this.apiCallCount++;

        if (PreferencesHelper.containsFlickrAccountId(context))
        {
            this.startApiCallForResult(Api.flickr_photosets, FlickrPhotosetApiCall.class);
        }
        else
        {
            final Bundle params = new Bundle();
            params.putString(FlickrAccountApiCall.PARAM_USERNAME, getString(R.string.flickr_user_name));
            this.startApiCallForResult(Api.flickr_account, FlickrAccountApiCall.class, params);
        }
    }

    private void doInstagramApiCall()
    {
        //never update more than once an hour
        if (UpdatePreferencesHelper.isUpdateNeeded(context, UpdatePreferencesHelper.LastUpdateType.INSTAGRAM))
        {
            this.apiCallCount++;
            this.startApiCallForResult(Api.instagram, InstagramApiCall.class);
        }
    }

    private void doYoutubeApiCall(boolean force)
    {
        this.apiCallCount++;
        this.startApiCallForResult(Api.youtube, YouTubeVideosApiCall.class);
    }

    public void doNewsApiCall(boolean force)
    {
        this.apiCallCount++;
        this.startApiCallForResult(Api.news, NewsApiCall.class);
    }

    public void doIssuesApiCall(boolean force)
    {
        this.apiCallCount++;
        this.startApiCallForResult(Api.issues, IssuesApiCall.class);

    }

    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass, Bundle inHttpRequestParams)
    {
        if (this.apiMessenger != null)
        {
            return this.apiMessenger.startApiCallForResult(inRequestCode, aClass, inHttpRequestParams);
        }
        return false;
    }

    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass)
    {
        if (this.apiMessenger != null)
        {
            return this.apiMessenger.startApiCallForResult(inRequestCode, aClass);
        }
        return false;
    }

    public final String getString(@StringRes int resId) {
        return context.getResources().getString(resId);
    }

    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        switch (inRequestCode)
        {
            case Api.flickr_account:

                if (inResultCode == ApiService.RESULT_SUCCESS)
                {
                    this.doFlickrApiCalls(true);
                }

                this.apiCallCount--;

                break;

            case Api.twitter_token:

                if (inResultCode == ApiService.RESULT_SUCCESS)
                {
                    this.doTwitterApiCalls(true);
                }

                this.apiCallCount--;

                break;

            case Api.twitter_name:
                /*if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    this.apiCallCount--;

                    if (inResultCode == ApiService.RESULT_FAILED)
                    {
                        //access token removed, get a new one by starting the api calls again
                        ApiErrorHelper.showError(this.getActivity());

                        if (this.getView() != null && this.getView().getContext() != null)
                        {
                            final boolean hasConnection = Connectivity.hasConnection(this.getView().getContext());
                            if (hasConnection)
                            {
                                this.doTwitterApiCalls(true);
                            }
                        }
                    }
                }
                else
                {
                    this.apiCallCount--;
                }
                */
                this.apiCallCount--;
                break;

            case Api.flickr_photosets:
            case Api.instagram:
            case Api.youtube:
            case Api.news:
            case Api.issues:

                this.apiCallCount--;

                break;
        }
    }
}
