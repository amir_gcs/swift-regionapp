package com.swift.SWIFTRegional.fragments.search;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider;
import com.swift.SWIFTRegional.contentproviders.FlickrContentProvider;
import com.swift.SWIFTRegional.contentproviders.InstagramContentProvider;
import com.swift.SWIFTRegional.contentproviders.NewsContentProvider;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.contentproviders.SponsorContentProvider;
import com.swift.SWIFTRegional.contentproviders.TwitterContentProvider;
import com.swift.SWIFTRegional.contentproviders.VideoContentProvider;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.SessionLoader;

public class AllSearchResultFragment extends AbstractSearchResultFragment
{
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        String searchText = "";
        if (args != null)
        {
            searchText = args.getString(EXTRA_SEARCH);
        }

        switch (id)
        {
            case LoaderIDs.speaker:
                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(SpeakerContentProvider.CONTENT_URI_SEARCH, searchText), null, null, null, null);

            case LoaderIDs.sponsor:
                return new CursorLoader(this.getActivity(),  Uri.withAppendedPath(SponsorContentProvider.CONTENT_URI_SEARCH, searchText), null, null, null, null);

            case LoaderIDs.twitter_name:
                return new CursorLoader(this.getActivity(),
                        TwitterContentProvider.CONTENT_URI_SEARCH.buildUpon()
                            .appendPath(String.valueOf(WallItem.Type.TWITTER_NAME.ordinal()))
                            .appendPath(searchText).build()
                        , null, null, null, null);

            case LoaderIDs.youtube:
                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(VideoContentProvider.CONTENT_URI_SEARCH, searchText), null, null, null, null);
        }

        return null;
    }

    protected SessionLoader.UriMode getUriMode()
    {
        return SessionLoader.UriMode.ALL;
    }
}
