package com.swift.SWIFTRegional.fragments.event;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerTabStrip;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.fragments.speakers.AbstractFilterPagerFragment;
import com.swift.SWIFTRegional.utils.GAHelper;

import java.util.ArrayList;

public class RegionTypePagerFragment extends AbstractFilterPagerFragment
{
    public int selectedTabPosition;
    protected int selectedSpinnerPosition;
    public static String fromScreen;
    private LinearLayout tabs;


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        ((MainActivity)getActivity()).setToolbarText("SWIFT Event Selection");

        if(!MainActivity.selectedEventProvider.isEventSelected())
        {
            //((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        final View view = super.onCreateView(inflater, container, savedInstanceState);
        SetupTabStrip(view);



        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        if(savedInstanceState!=null)
        {
            fromScreen=savedInstanceState.getString("spinnerName");
        }
        ((MainActivity)getActivity()).ChangeButton(true);
        if(!MainActivity.selectedEventProvider.isEventSelected())
        {
            ((MainActivity)getActivity()).setToolbarText("SWIFT Event Selection");
            ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((MainActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.region_app_backgroundcolor)));
        }
        else
        {
            ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        GAHelper.trackScreen(this.getView().getContext(), "/mainevent/regionstypes");
    }

    @Override
    protected int getLayout()
    {
        return R.layout.fragment_eventselector_viewpager;
    }

    @Override
    protected ArrayList<Fragment> getFragments()
    {
        ArrayList<Fragment> fragmentArray = new ArrayList<Fragment>();
        fragmentArray.add(new RegionListFragment());
        fragmentArray.add(new TypeListFragment());
        return fragmentArray;
    }

    @Override
    protected int getTitles()
    {
        return R.array.regionTypeTabTitles;
    }


    private void SetupTabStrip(View view)
    {
        tabs = (LinearLayout)((PagerSlidingTabStrip) view.findViewById(R.id.tabs)).getChildAt(0);
        for(int i=0; i < tabs.getChildCount(); i++){
            TextView tv = (TextView) tabs.getChildAt(i);
            tv.setTextSize(15);
            if(i == 0){
                tv.setTextColor(getResources().getColor(R.color.white_foreground_color));
            } else {
                tv.setTextColor(getResources().getColor(R.color.region_app_backgroundcolor));
            }
        }
    }

    private void setActiveTab(int position)
    {
        for(int i=0; i < tabs.getChildCount(); i++){
            TextView tv = (TextView) tabs.getChildAt(i);
            if(i == position){
                tv.setTextColor(getResources().getColor(R.color.white_foreground_color));
            } else {
                tv.setTextColor(getResources().getColor(R.color.region_app_backgroundcolor));
            }
        }

    }


    @Override
    protected void tabSelected(int position)
    {
        switch (position)
        {
            case 0:

                GAHelper.trackScreen(this.getView().getContext(), "/mainevent/regions");
                selectedTabPosition = position;
                break;
            case 1:
                GAHelper.trackScreen(this.getView().getContext(), "/mainevent/types");
                selectedTabPosition = position;
                break;
        }

        setActiveTab(position);

    }

    public int getSelectedTabPosition(){
        return selectedTabPosition;
    }



}