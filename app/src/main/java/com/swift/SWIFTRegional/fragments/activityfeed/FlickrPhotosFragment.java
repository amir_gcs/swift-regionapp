package com.swift.SWIFTRegional.fragments.activityfeed;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.adapters.activityfeed.FlickrPhotosCursorPagerAdapter;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.views.HackyViewPager;

import com.swift.SWIFTRegional.R;

/**
 * Created by Wesley on 20/05/14.
 */
public class FlickrPhotosFragment extends AbstractFlickrPhotosetFragment
{
    public static final String EXTRA_POSITION = "position";

    private HackyViewPager viewPager;
    private FlickrPhotosCursorPagerAdapter adapter;

    private int position;

    public static Bundle createBundle(final Intent intent)
    {
        final Bundle bundle = new Bundle();
        bundle.putString(EXTRA_PHOTOSET_ID, intent.getStringExtra(EXTRA_PHOTOSET_ID));
        bundle.putString(EXTRA_PHOTOSET_TITLE, intent.getStringExtra(EXTRA_PHOTOSET_TITLE));
        bundle.putInt(FlickrPhotosFragment.EXTRA_POSITION, intent.getIntExtra(EXTRA_POSITION, -1));
        return bundle;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_viewpager_gallery, container, false);

        this.viewPager = (HackyViewPager) view.findViewById(R.id.viewPager);
        this.adapter = new FlickrPhotosCursorPagerAdapter(view.getContext());
        this.viewPager.setAdapter(this.adapter);

        this.position = this.getArguments().getInt(EXTRA_POSITION);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        GAHelper.trackScreen(view.getContext(), "/activity/pictures/" + this.getArguments().getString(EXTRA_PHOTOSET_TITLE) + "/photo");
    }

    @Override
    protected void hideLayout()
    {
        this.viewPager.setVisibility(View.GONE);
    }

    @Override
    protected void showLayout()
    {
        this.viewPager.setVisibility(View.VISIBLE);
    }

    @Override
    protected void swapCursor(Cursor data)
    {
        this.adapter.swapCursor(data);

        this.viewPager.setCurrentItem(this.position);
    }
}
