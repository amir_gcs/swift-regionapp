package com.swift.SWIFTRegional.fragments.event;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.adapters.TypeCursorAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.callbacks.GetRegionInfoCallBack;
import com.swift.SWIFTRegional.contentproviders.TypeInfoContentProvider;
import com.swift.SWIFTRegional.database.tables.TypeInfoTable;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.event.TypeInfo;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;

public class TypeListFragment  extends AbstractCardsListFragment<TypeCursorAdapter> implements AdapterView.OnItemClickListener, SearchableListInterface {
    private int apiCallCount = 0;
    private ViewLoaderStrategyInterface viewLoader;
    private boolean dataLoaded;

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_list_general;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof TypeInfo)
            {
                final TypeInfo typeInfo = (TypeInfo) o;
                UserPreferencesHelper.storeEventSelection(this.getActivity(),"", Long.toString(typeInfo.getTypeId()),"");
                if(!typeInfo.getName().equals("Show All"))
                {
                    EventListFragment.listHeader=typeInfo.getName();
                }
                else
                {
                    EventListFragment.listHeader="All Events";
                }

                ((MainActivity)getActivity()).ChangeButton(false);
                ((MainActivity)getActivity()).setToolbarText(EventListFragment.listHeader);
                ((MainActivity)getActivity()).switchFragment(EventListFragment.class,null,null,true);
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    @Override
    public void onDetach() {
        this.viewLoader = null;
        super.onDetach();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view= super.onCreateView(inflater, container, savedInstanceState);

        final ListView listView=(ListView)view.findViewById(R.id.listView);

        listView.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.type_background));

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GAHelper.trackScreen(view.getContext(), "/types/events");
        UserPreferencesHelper.storeEventSelection(this.getActivity(), "","","");
        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);
        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });
    }

    @Override
    public void startApiCalls() {
        this.doApiCalls(false);
    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(LoaderIDs.regioninfo, null, this);
    }

    @Override
    public void onRefresh() {
        this.doApiCalls(true);
    }


    protected void doApiCalls(boolean force) {
        if (super.startApiCalls(force) && this.getView() != null) {
            if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getView().getContext(), UpdatePreferencesHelper.LastUpdateType.REGIONINFO)) {
                setIsApiHappening(true);
                startLoadingView(true);
                ApiManager.getInstance(this.getActivity(),true).getRegionInfos(new GetRegionInfoCallBack(this.getActivity()) {
                    @Override
                    public void postResult(Void result) {
                        super.postResult(result);
                        onRefreshCompleted();
                    }
                });
            }
        }


    }


    @Override
    public void startLoaders() {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.regioninfo, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null) {
            return loader;
        }

        switch (id) {
            case LoaderIDs.regioninfo:

                return new CursorLoader(this.getActivity(), getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {
        if (StringUtils.isEmpty(this.searchText)) {
            return TypeInfoContentProvider.CONTENT_URI;
        } else {
            return Uri.withAppendedPath(TypeInfoContentProvider.CONTENT_URI_SEARCH, this.searchText);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LoaderIDs.regioninfo:
                this.dataLoaded = true;
                this.setCanShow(data.getCount()==0);
                    this.adapter.swapCursor(data);

                break;
        }

        super.onLoadFinished(loader, data);
    }

    private Cursor getModifiedCursorForTable(Cursor cursor) {
        MatrixCursor modifiedCursor = new MatrixCursor(TypeInfoTable.getColumnNamesTwoItems());
        Integer i = 0;
        while (cursor.moveToNext()) {
            TypeInfo typeInfo = new TypeInfo();
            typeInfo.constructFromCursor(cursor);

            List<Object> cursorRow = new ArrayList<Object>();
            cursorRow.add(i);
            cursorRow.addAll(addTypeInfoDataToList(typeInfo));

            if (cursor.moveToNext()) {
                TypeInfo typeInfo1 = new TypeInfo();
                typeInfo1.constructFromCursor(cursor);

                if (typeInfo1.getName().substring(0, 1).equalsIgnoreCase(typeInfo1.getName().substring(0, 1))) {
                    cursorRow.addAll(addTypeInfoDataToList(typeInfo1));
                    modifiedCursor.addRow(cursorRow);
                } else {
                    cursorRow.addAll(addTypeInfoDataToList(null));
                    modifiedCursor.addRow(cursorRow);
                    cursor.moveToPrevious();
                }

            } else {
                cursorRow.addAll(addTypeInfoDataToList(null));
                modifiedCursor.addRow(cursorRow);
            }
            i++;
        }
        return modifiedCursor;
    }

    private List<Object> addTypeInfoDataToList(TypeInfo typeInfo) {
        List<Object> data = new ArrayList<>();
        if (typeInfo == null) {
            Integer itemCount = TypeInfoTable.getColumnNames().length - 1;
            for (Integer i = 0; i < itemCount; i++) {
                data.add("");
            }
        } else {
            data.add(typeInfo.getTypeId());
            data.add(typeInfo.getName());
            data.add(typeInfo.getBackgroundImgURL());
        }
        return data;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LoaderIDs.news:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded() {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders() {
        this.getLoaderManager().destroyLoader(LoaderIDs.news);
    }



    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle) {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        final Long lastModified;

        switch (inRequestCode) {


            case Api.regioninfo:
                setIsApiHappening(false);
                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached()) {
                    if (inResultCode == ApiService.RESULT_FAILED) {
                        //ApiErrorHelper.showError(this.getActivity());
                    }
                }

                this.apiCallCount--;

                break;
        }

        if (this.apiCallCount <= 0) {
            this.apiCallCount = 0;

            this.onRefreshCompleted();
        }
    }

    @Override
    public TypeCursorAdapter getAdapter(Context context) {
        return new TypeCursorAdapter(context);
    }
}
