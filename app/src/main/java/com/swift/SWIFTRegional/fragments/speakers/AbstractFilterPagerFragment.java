package com.swift.SWIFTRegional.fragments.speakers;

import android.support.v4.app.Fragment;

import com.swift.SWIFTRegional.interfaces.SpinnerFilterFragment;
import com.swift.SWIFTRegional.fragments.AbstractPagerFragment;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;

/**
 * Created by SimonRaes on 20/04/15.
 * Host a viewpager with fragments and supports a filter spinner in the toolbar.
 */
public abstract class AbstractFilterPagerFragment extends AbstractPagerFragment implements SpinnerFilterFragment
{
    @Override
    public void itemSelected(Object item, int position)
    {
        Fragment activeFragment = this.pagerAdapter.getRegisteredFragment(this.viewPager.getCurrentItem());
        if (activeFragment != null && activeFragment instanceof SpinnerFilterFragment)
        {
            // Pass on the selection event to the currently visible fragment.
            ((SpinnerFilterFragment)activeFragment).itemSelected(item, position);
        }
    }

    @Override
    public int getSpinnerPosition()
    {
        Fragment activeFragment = this.pagerAdapter.getRegisteredFragment(this.viewPager.getCurrentItem());
        if (activeFragment != null && activeFragment instanceof SpinnerFilterFragment)
        {
            return ((SpinnerFilterFragment) activeFragment).getSpinnerPosition();
        }
        return 0;
    }

    @Override
    public SpinnerItem[] getSpinnerItems()
    {
        Fragment activeFragment = this.pagerAdapter.getRegisteredFragment(this.viewPager.getCurrentItem());
        if (activeFragment != null && activeFragment instanceof SpinnerFilterFragment)
        {
            return ((SpinnerFilterFragment) activeFragment).getSpinnerItems();
        }

        return null;
    }
}
