package com.swift.SWIFTRegional.fragments.event;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.adapters.RegionCursorAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.apicalls.program.event.EventApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.event.RegionTypeApiCall;
import com.swift.SWIFTRegional.api.callbacks.GetRegionInfoCallBack;
import com.swift.SWIFTRegional.contentproviders.RegionInfoContentProvider;
import com.swift.SWIFTRegional.database.tables.RegionInfoTable;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.event.RegionInfo;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;


import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;

public class RegionListFragment extends AbstractCardsListFragment<RegionCursorAdapter> implements AdapterView.OnItemClickListener, SearchableListInterface {
    private int apiCallCount = 0;
    private ViewLoaderStrategyInterface viewLoader;
    private boolean dataLoaded;


    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_list_general;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Object o = this.adapter.getItem(position);

        if (o != null) {
            if (o instanceof RegionInfo) {
                final RegionInfo regionInfo = (RegionInfo) o;
                UserPreferencesHelper.storeEventSelection(this.getActivity(), Long.toString(regionInfo.getRegionId()), "", "");
                if (!regionInfo.getName().equals("Show All")) {
                    EventListFragment.listHeader = regionInfo.getName();
                } else {
                    EventListFragment.listHeader = "All Events";
                }

                ((MainActivity)getActivity()).ChangeButton(false);
                ((MainActivity) getActivity()).setToolbarText(EventListFragment.listHeader);
                ((MainActivity) getActivity()).switchFragment(EventListFragment.class, null, null, true);

            }

        }

        super.onItemClick(parent, view, position, id);
    }

    @Override
    public void onDetach() {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = super.onCreateView(inflater, container, savedInstanceState);

        final ListView listView = (ListView) view.findViewById(R.id.listView);

        listView.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.region_background));

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GAHelper.trackScreen(view.getContext(), "/regions/allregions");
        UserPreferencesHelper.storeEventSelection(this.getActivity(), "", "", "");
        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);
        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });
    }

    @Override
    public void startApiCalls() {
        this.doApiCalls(true);
    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(LoaderIDs.regioninfo, null, this);
    }

    @Override
    public void onRefresh() {

        this.doApiCalls(true);
    }


    protected void doApiCalls(boolean force) {


        if (super.startApiCalls(force) && this.getView() != null) {
            if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getView().getContext(), UpdatePreferencesHelper.LastUpdateType.REGIONINFO)) {
                setIsApiHappening(true);
                startLoadingView(true);
                this.startApiCallForResult(Api.mainevent, EventApiCall.class, null);

                ApiManager.getInstance(this.getActivity(), true).getRegionInfos(new GetRegionInfoCallBack(this.getActivity()) {
                    @Override
                    public void postResult(Void result) {

                        super.postResult(result);
                        onRefreshCompleted();
                    }
                });
            }
        }


    }


    @Override
    public void startLoaders() {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.regioninfo, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null) {
            return loader;
        }

        switch (id) {
            case LoaderIDs.regioninfo:

                return new CursorLoader(this.getActivity(), getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {
        if (StringUtils.isEmpty(this.searchText)) {
            return RegionInfoContentProvider.CONTENT_URI;
        } else {
            return Uri.withAppendedPath(RegionInfoContentProvider.CONTENT_URI_SEARCH, this.searchText);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LoaderIDs.regioninfo:
                this.dataLoaded = true;
                this.setCanShow(data.getCount()==0);
                    this.adapter.swapCursor(data);

                break;
        }

        super.onLoadFinished(loader, data);
    }

    private Cursor getModifiedCursorForTable(Cursor cursor) {
        MatrixCursor modifiedCursor = new MatrixCursor(RegionInfoTable.getColumnNamesTwoItems());
        Integer i = 0;
        while (cursor.moveToNext()) {
            RegionInfo regionInfo = new RegionInfo();
            regionInfo.constructFromCursor(cursor);

            List<Object> cursorRow = new ArrayList<Object>();
            cursorRow.add(i);
            cursorRow.addAll(addTypeInfoDataToList(regionInfo));

            if (cursor.moveToNext()) {
                RegionInfo regionInfo1 = new RegionInfo();
                regionInfo1.constructFromCursor(cursor);

                if (regionInfo1.getName().substring(0, 1).equalsIgnoreCase(regionInfo1.getName().substring(0, 1))) {
                    cursorRow.addAll(addTypeInfoDataToList(regionInfo1));
                    modifiedCursor.addRow(cursorRow);
                } else {
                    cursorRow.addAll(addTypeInfoDataToList(null));
                    modifiedCursor.addRow(cursorRow);
                    cursor.moveToPrevious();
                }

            } else {
                cursorRow.addAll(addTypeInfoDataToList(null));
                modifiedCursor.addRow(cursorRow);
            }
            i++;
        }
        return modifiedCursor;
    }

    private List<Object> addTypeInfoDataToList(RegionInfo typeInfo) {
        List<Object> data = new ArrayList<>();
        if (typeInfo == null) {
            Integer itemCount = RegionInfoTable.getColumnNames().length - 1;
            for (Integer i = 0; i < itemCount; i++) {
                data.add("");
            }
        } else {
            data.add(typeInfo.getRegionId());
            data.add(typeInfo.getName());
            data.add(typeInfo.getBackgroundImgURL());
        }
        return data;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LoaderIDs.regioninfo:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded() {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders() {
        this.getLoaderManager().destroyLoader(LoaderIDs.regioninfo);
    }


    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle) {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        final Long lastModified;

        switch (inRequestCode) {


            case Api.regioninfo:
                setIsApiHappening(false);
                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached()) {
                    if (inResultCode == ApiService.RESULT_FAILED) {
                        //MainActivity.MakeFile("The code returned is failure"+inResultCode);
                        //ApiErrorHelper.showError(this.getActivity());
                    }
                }

                this.apiCallCount--;

                break;
        }

        if (this.apiCallCount <= 0) {
            this.apiCallCount = 0;

            this.onRefreshCompleted();
        }
    }

    @Override
    public RegionCursorAdapter getAdapter(Context context) {
       // return SibosUtils.isGreaterThan600DP(context) ? new RegionCursorAdapter600DP(context, this.viewLoader) : new RegionCursorAdapter(context);
        return  new RegionCursorAdapter(context);
    }


}
