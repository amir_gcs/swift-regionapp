package com.swift.SWIFTRegional.fragments.activityfeed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.adapters.activityfeed.FlickrCursorAdapter;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr.FlickrAccountApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr.FlickrPhotosetApiCall;
import com.swift.SWIFTRegional.contentproviders.FlickrContentProvider;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.activityfeed.FlickrThumbsActivity;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.database.tables.FlickrPhotosetsTable;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.flickr.FlickrPhotoset;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Wesley on 19/05/14.
 */
public class FlickrPhotosetsFragment extends AbstractCardsListFragment<FlickrCursorAdapter> implements AdapterView.OnItemClickListener, SearchableListInterface
{
    private int apiCallCount;
    private ViewLoaderStrategyInterface viewLoader;
    private boolean dataLoaded;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach()
    {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        GAHelper.trackScreen(view.getContext(), "/activity/pictures");

        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);
    }

    @Override
    public FlickrCursorAdapter getAdapter(Context context)
    {
        return new FlickrCursorAdapter(context);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof FlickrPhotoset)
            {
                final FlickrPhotoset flickrPhotoset = (FlickrPhotoset) o;
                final String flickrPhotosetId = flickrPhotoset.getPhotosetId();
                final String flickrPhotosetTitle = flickrPhotoset.getTitle();

                final Intent intent = FlickrThumbsActivity.getIntent(view.getContext(), flickrPhotosetId, flickrPhotosetTitle);
                this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.FLICKR_THUMBS);
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    @Override
    public void startApiCalls()
    {
        this.doApiCall(false);
    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(LoaderIDs.flickr, null, this);
    }

    private Uri getContentUri(){
        return StringUtils.isEmpty(this.searchText)
            ? FlickrContentProvider.CONTENT_URI_PHOTOSETS
            : FlickrContentProvider.CONTENT_URI_SEARCH.buildUpon().appendPath(this.searchText).build();
    }

    @Override
    public void onRefresh()
    {
        this.doApiCall(true);
    }

    public void doApiCall(boolean force)
    {
        if (super.startApiCalls(force))
        {
            if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.FLICKR))
            {
                this.apiCallCount = 0;

                this.apiCallCount++;

                if (PreferencesHelper.containsFlickrAccountId(this.getView().getContext()))
                {
                    this.startApiCallForResult(Api.flickr_photosets, FlickrPhotosetApiCall.class);
                }
                else
                {
                    final Bundle params = new Bundle();
                    params.putString(FlickrAccountApiCall.PARAM_USERNAME, getString(R.string.flickr_user_name));
                    this.startApiCallForResult(Api.flickr_account, FlickrAccountApiCall.class, params);
                }
            }
        }
    }


    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.flickr, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        switch (id)
        {
            case LoaderIDs.flickr:

                return new CursorLoader(this.getActivity(), getContentUri(), null, null, null, null);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.flickr:

                this.adapter.swapCursor(data);

                //mark as read
                if (data.moveToFirst())
                {
                    final long date = DatabaseUtils.getLong(data, FlickrPhotosetsTable.COLUMN_DATE);

                    //if the wall is opened we mark everything as read
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.FLICKR, date);
                }

                this.dataLoaded = true;

                break;
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.flickr:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.flickr);
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch (inRequestCode)
        {
            case Api.flickr_account:

                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_SUCCESS)
                    {
                        this.startApiCalls();
                    }
                    else
                    {
                        ApiErrorHelper.showError(this.getActivity());
                    }
                }

                this.apiCallCount--;

                break;

            case Api.flickr_photosets:

                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode == ApiService.RESULT_FAILED)
                    {
                        ApiErrorHelper.showError(this.getActivity());
                    }
                }

                this.apiCallCount--;

                break;
        }

        if (this.apiCallCount <= 0)
        {
            this.apiCallCount = 0;
            this.onRefreshCompleted();
        }
    }
}
