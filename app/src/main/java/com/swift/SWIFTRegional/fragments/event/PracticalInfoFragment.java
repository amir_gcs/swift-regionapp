package com.swift.SWIFTRegional.fragments.event;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.google.gson.Gson;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.practicalinfo.PracticalInfoApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.speaker.SpeakerApiCall;
import com.swift.SWIFTRegional.contentproviders.PracticalInfoContentProvider;
import com.swift.SWIFTRegional.database.tables.PracticalInfoTable;
import com.swift.SWIFTRegional.fragments.WebViewFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.event.PracticalInfo;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.utils.TabletUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PracticalInfoFragment extends WebViewFragment  implements LoaderManager.LoaderCallbacks<Cursor>
{

    static final int INTERNAL_PROGRESS_CONTAINER_ID = 0x00ff0061;
    static final int INTERNAL_NODATA_CONTAINER_ID = 0x00ff0062;
    ProgressDialog pd;
    PracticalInfo practicalInfo;
    private View progressContainer;
    private View noDataContainer;
    private boolean isLoading;

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).isWall=false;
        ((MainActivity) getActivity()).HideSpinner(false);
        ((MainActivity) getActivity()).setToolbarText("Practical Info");
        super.onAttach(this.getActivity());
    }

    @Override
    public void onNetworkDisconnected() {
        super.onNetworkDisconnected();
        startLoaders();
        this.setVisibilities();
//        if(practicalInfo==null)
//        {
//            noDataContainer.setVisibility(View.GONE);

//        }

    }

    @Override
    public void onNetworkConnected() {
        super.onNetworkConnected();
        startLoaders();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @SuppressWarnings("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        FrameLayout root = (FrameLayout) super.onCreateView(inflater,container,savedInstanceState);

        final Context context = getActivity();


        TabletUtils.setWindowBackground(getActivity());
        FrameLayout pframe = new FrameLayout(context);
        pframe.setId(INTERNAL_PROGRESS_CONTAINER_ID);
        pframe.setVisibility(View.GONE);



        ProgressBar progress = new ProgressBar(context, null,
                android.R.attr.progressBarStyleLarge);

        pframe.addView(progress, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) progress.getLayoutParams();
        params.gravity = Gravity.CENTER;
        progress.setLayoutParams(params);

        root.addView(pframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        this.progressContainer = root.findViewById(INTERNAL_PROGRESS_CONTAINER_ID);




        FrameLayout eframe = new FrameLayout(context);
        eframe.setVisibility(View.GONE);
        eframe.setId(INTERNAL_NODATA_CONTAINER_ID);

        View viewEmpty = inflater.inflate(R.layout.fragment_nodata,container,false);

        // Add a blank empty view
        if (viewEmpty == null)
        {
            viewEmpty = new View(getActivity());
        }

        //set retry button
        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) viewEmpty.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoaders();
            }
        });


        eframe.addView(viewEmpty, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        root.addView(eframe, new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        this.noDataContainer = root.findViewById(INTERNAL_NODATA_CONTAINER_ID);


        return  root;
    }

    @SuppressWarnings("ResourceType")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        startLoaders();



    }

    protected void doApiCalls()
    {
        this.startApiCallForResult(Api.practical_info, PracticalInfoApiCall.class, null);
    }

    public void startLoaders()
    {
        if(isAdded())
            this.getLoaderManager().restartLoader(LoaderIDs.practical_info, null, this);
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle) {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        if (inRequestCode == Api.practical_info) {
            if (this.getActivity() != null && !this.isRemoving() && !this.isDetached()) {
                if (inResultCode == ApiService.RESULT_FAILED) {
                    //ApiErrorHelper.showError(this.getActivity());
                }
            }
            startLoaders();
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch (id)
        {
            case LoaderIDs.practical_info:

                return new CursorLoader(this.getActivity(), PracticalInfoContentProvider.CONTENT_URI, null, null, null, null);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        switch (loader.getId())
        {
            case LoaderIDs.practical_info:

                if(data.getCount() > 0) {
                    if (data.moveToFirst()) {
                        final String practUrl = DatabaseUtils.getString(data, PracticalInfoTable.COLUMN_URL);
                        webView.loadUrl(practUrl);

                    }

                    showWebView(true);
                    noDataContainer.setVisibility(View.GONE);

                }
                else {
                    //we can show the no data screen
                    //show the no data view
                    showWebView(false);
                    if(hasConnection)
                    {
                        noDataContainer.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.getLoaderManager().restartLoader(LoaderIDs.practical_info, null, this);
    }

    @Override
    protected void setVisibilities()
    {
        setVisibilities(true);
    }
}