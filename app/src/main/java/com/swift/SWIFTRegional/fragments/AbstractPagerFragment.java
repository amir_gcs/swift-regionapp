package com.swift.SWIFTRegional.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.adapters.activityfeed.SimplePagerAdapter;

import java.util.ArrayList;

/**
 * Created by SimonRaes on 15/04/15.
 * Superclass for fragments hosting a viewpager.
 */
public abstract class AbstractPagerFragment extends BaseFragment
{
    protected SimplePagerAdapter pagerAdapter;
    protected ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(this.getLayout(), container, false);

        this.viewPager = (ViewPager) view.findViewById(R.id.viewPager);

        String[] titles = getActivity().getResources().getStringArray(this.getTitles());
        this.pagerAdapter = new SimplePagerAdapter(this.getChildFragmentManager(), this.getFragments(), titles);

        this.viewPager.setAdapter(this.pagerAdapter);
        this.viewPager.setOffscreenPageLimit(1);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        tabs.setViewPager(this.viewPager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position)
            {
                AbstractPagerFragment.this.tabSelected(position);

                if (getActivity() instanceof MainActivity)
                {
                    ((MainActivity) getActivity()).updateSpinnerContent();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

        return view;
    }

    protected abstract int getLayout();

    protected abstract ArrayList<Fragment> getFragments();

    protected abstract int getTitles();

    protected abstract void tabSelected(int position);

}
