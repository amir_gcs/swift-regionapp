package com.swift.SWIFTRegional.fragments.event;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.adapters.EventCursorAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.program.event.EventApiCall;

import com.swift.SWIFTRegional.contentproviders.EventContentProvider;
import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;
import com.swift.SWIFTRegional.contentproviders.MainEventContentProvider;

import com.swift.SWIFTRegional.contentproviders.PracticalInfoContentProvider;
import com.swift.SWIFTRegional.contentproviders.SessionSpeakerContentProvider;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.contentproviders.SponsorContentProvider;
import com.swift.SWIFTRegional.contentproviders.TwitterContentProvider;
import com.swift.SWIFTRegional.contentproviders.VideoContentProvider;
import com.swift.SWIFTRegional.contentproviders.WallContentProvider;
import com.swift.SWIFTRegional.database.tables.MainEventTable;

import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterHashtagFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterNameFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.VideoFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.WallFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.event.Event;

import com.swift.SWIFTRegional.models.event.RegionInfo;
import com.swift.SWIFTRegional.models.event.TypeInfo;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;

public class EventListFragment extends AbstractCardsListFragment<EventCursorAdapter> implements AdapterView.OnItemClickListener, SearchableListInterface {
    public static String fromScreen;
    public static String listHeader;

    private int apiCallCount = 0;
    private ViewLoaderStrategyInterface viewLoader;
    private boolean dataLoaded;


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.fragment_list_general;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {

        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        ((MainActivity) getActivity()).backStack.add("eventlist");

        View view = super.onCreateView(inflater, container, savedInstanceState);

        final ListView listView = (ListView) view.findViewById(R.id.listView);

        if (StringUtils.isEmpty(this.searchText)) {
            if (UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region") != "" && UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region") != null) {

                String regionId = UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region");
                RegionInfo region = MainActivity.selectedEventProvider.getRegionInfo(regionId);

                if(regionId.equals("-1"))
                {
                    listView.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.default_region));
                }


                if (region != null && !TextUtils.isEmpty(region.getBackgroundImgURL())) {
                    applyBackgroundImage(listView, region.getBackgroundImgURL());
                }
                else
                {
                    listView.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.default_region));
                }

            }

            if (UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type") != "" && UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type") != null) {

                //by region type
                String typeId = UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type");
                TypeInfo typeInfo = MainActivity.selectedEventProvider.getTypeInfo(typeId);

                if(typeId.equals("-1"))
                {
                    listView.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.default_type));
                }


                if (typeInfo != null && !TextUtils.isEmpty(typeInfo.getBackgroundImgURL())) {
                    applyBackgroundImage(listView, typeInfo.getBackgroundImgURL());
                }
                else
                {
                    listView.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.default_type));
                }

            }
        }

        return view;
    }

    private void applyBackgroundImage(final ListView listView, String imageUrl) {
        try {

            Target t = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                             /* Save the bitmap or do something with it here */
                    try {
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                        listView.setBackground(bitmapDrawable);

                    } catch (Exception e) {

                    }

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    if (placeHolderDrawable == null) {

                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    if (errorDrawable == null) {

                    }
                }
            };
            listView.setTag(t);

            Picasso.with(getActivity())
                    .load(imageUrl)
                    .into(t);

        } catch (Exception e) {
            Log.e("imageLoad", e.toString());
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GAHelper.trackScreen(view.getContext(), "/events/mainevents");
        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);

        if (savedInstanceState != null) {
            fromScreen = savedInstanceState.getString("spinnerName");
        }

        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Object o = this.adapter.getItem(position);

        if (o != null) {

            super.onItemClick(parent, view, position, id);
            //we save it to the db
            if (o instanceof Event) {
                Event event = (Event) o;
                event.setSelected(1);
                MainActivity.selectedEventProvider.selectEvent(event.getEventId());
                //delete all sessions and other event related stuff
                deleteOldEventInfo(view.getContext());
                //clear all tokens also
                VideoFragment.setNextToken("");
                TwitterNameFragment.setNextToken("");
                TwitterHashtagFragment.setNextToken("");
                //go to the main event fragment
                ((MainActivity) getActivity()).showEventInfo();
            }

        }
    }

    private void deleteOldEventInfo(Context context) {

        ModulePreferenceHelper.resetModuleDataLoaded(context);
        ModulePreferenceHelper.resetModuleDataAvailable(context);

        MainActivity.appContext.getContentResolver().delete(EventContentProvider.CONTENT_URI_SESSIONS, null, null);
        MainActivity.appContext.getContentResolver().delete(EventTimingContentProvider.CONTENT_URI_SESSIONS, null, null);
        MainActivity.appContext.getContentResolver().delete(SessionSpeakerContentProvider.CONTENT_URI, null, null);
        MainActivity.appContext.getContentResolver().delete(SpeakerContentProvider.CONTENT_URI, null, null);
        MainActivity.appContext.getContentResolver().delete(SponsorContentProvider.CONTENT_URI, null, null);
        MainActivity.appContext.getContentResolver().delete(PracticalInfoContentProvider.CONTENT_URI, null, null);
        MainActivity.appContext.getContentResolver().delete(VideoContentProvider.CONTENT_URI, null, null);
        MainActivity.appContext.getContentResolver().delete(TwitterContentProvider.CONTENT_URI, null, null);

        MainActivity.eventChangedPartner=true;
        ((MainActivity) getActivity()).backStack.clear();
//        ((MainActivity) getActivity()).backStack.add("programmes");
//        ((MainActivity) getActivity()).backStack.add("programmes");
        ((MainActivity) getActivity()).doSelectionApiCalls();

    }

    @Override
    public void startApiCalls() {

        this.setIsApiHappening(true);
        this.doApiCalls(false);
    }


    @Override
    public void onRefresh() {
        this.doApiCalls(true);
    }


    protected void doApiCalls(boolean force) {
        if (super.startApiCalls(force) && this.getView() != null) {
            if (force || !MainActivity.selectedEventProvider.isEventSelected()) {
                startLoadingView(true);
                Bundle args = new Bundle();
                args.putString(EventApiCall.HEADER_DATA_TIMESTAMP, UpdatePreferencesHelper.getApiHeaderTimeStamp(this.getView().getContext(), UpdatePreferencesHelper.LastModifiedType.MAINEVENTS));
                this.startApiCallForResult(Api.mainevent, EventApiCall.class);
            }
        }


    }

    @Override
    public void startLoaders() {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.mainevent, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null) {
            return loader;
        }

        MainEventContentProvider.searchIn="";

        if (StringUtils.isEmpty(this.searchText)) {

            if (UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region") != "" && UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region") != null) {
                String selectedRegion = UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region");
                if (Integer.parseInt(selectedRegion)!=-1) {
                    Uri byRegion = Uri.withAppendedPath(MainEventContentProvider.CONTENT_URI_REGION, UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region"));
                    //by region search
                    return new CursorLoader(this.getActivity(), byRegion, null, null, null, null);
                }

            }

            if (UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type") != "" && UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type") != null) {
                String selectedType = UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type");
                if (Integer.parseInt(selectedType)!=-1) {
                    //by region type
                    Uri byType = Uri.withAppendedPath(MainEventContentProvider.CONTENT_URI_TYPE, UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type"));
                    return new CursorLoader(this.getActivity(), byType, null, null, null, null);
                }
            }

        }
        else
        {
            if (UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region") != "" && UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region") != null) {
                String selectedRegion = UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_region");
                if (Integer.parseInt(selectedRegion)!=-1) {
                    MainEventContentProvider.searchIn= " AND "+MainEventTable.COLUMN_REGIONID+"="+selectedRegion;
                }
            }

            if (UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type") != "" && UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type") != null) {
                String selectedType = UserPreferencesHelper.getEventSelection(this.getActivity(), "selected_type");
                if (Integer.parseInt(selectedType)!=-1) {
                    MainEventContentProvider.searchIn= " AND "+MainEventTable.COLUMN_TYPEID+"="+selectedType;
                }
            }
        }


        switch (id) {
            case LoaderIDs.mainevent:
                return new CursorLoader(this.getActivity(), this.getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {

        if (StringUtils.isEmpty(this.searchText)) {
            return MainEventContentProvider.CONTENT_URI;
        }
        return Uri.withAppendedPath(MainEventContentProvider.CONTENT_URI_SEARCH, this.searchText);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LoaderIDs.mainevent:
                this.setCanShow(data.getCount()==0);
                this.dataLoaded = true;
                this.adapter.swapCursor(data);

                break;
        }

        super.onLoadFinished(loader, data);
    }

    /**
     * Build a new cursor which contains two speakers in each row for the table view
     *
     * @param cursor
     * @return
     */
    private Cursor getModifiedCursorForTable(Cursor cursor) {
        MatrixCursor modifiedCursor = new MatrixCursor(MainEventTable.getColumnNamesTwoItems());
        Integer i = 0;
        while (cursor.moveToNext()) {
            Event event = new Event();
            event.constructFromCursor(cursor);

            List<Object> cursorRow = new ArrayList<Object>();
            cursorRow.add(i);
            cursorRow.addAll(addEventDataToList(event));

            if (cursor.moveToNext()) {
                Event event2 = new Event();
                event2.constructFromCursor(cursor);

                if (event2.getTitle().substring(0, 1).equalsIgnoreCase(event2.getTitle().substring(0, 1))) {
                    cursorRow.addAll(addEventDataToList(event2));
                    modifiedCursor.addRow(cursorRow);
                } else {
                    cursorRow.addAll(addEventDataToList(null));
                    modifiedCursor.addRow(cursorRow);
                    cursor.moveToPrevious();
                }

            } else {
                cursorRow.addAll(addEventDataToList(null));
                modifiedCursor.addRow(cursorRow);
            }
            i++;
        }
        return modifiedCursor;
    }

    private List<Object> addEventDataToList(Event event) {
        List<Object> data = new ArrayList<>();
        if (event == null) {
            Integer itemCount = MainEventTable.getColumnNames().length - 1;
            for (Integer i = 0; i < itemCount; i++) {
                data.add("");
            }
        } else {
            data.add(event.getEventId());
            data.add(event.getTitle());
            data.add(event.getDescription());
            data.add(event.getStartDate());
            data.add(event.getEndDate());
        }
        return data;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case LoaderIDs.mainevent:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }


    @Override
    protected boolean isAllDataLoaded() {
        return this.dataLoaded;
    }


    @Override
    protected void destroyLoaders() {
        this.getLoaderManager().destroyLoader(LoaderIDs.mainevent);
    }


    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle) {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);


        if (inRequestCode == Api.mainevent) {
            this.setIsApiHappening(false);
            if (this.getActivity() != null && !this.isRemoving() && !this.isDetached()) {
                if (inResultCode == ApiService.RESULT_FAILED) {
                    //ApiErrorHelper.showError(this.getActivity());
                }
            }

            this.onRefreshCompleted();
        }
    }


    @Override
    public void restartSearchLoaders() {
        if (isAdded()) {
            this.getLoaderManager().restartLoader(LoaderIDs.mainevent, null, this);
        }
    }


    @Override
    public EventCursorAdapter getAdapter(Context context) {
        return new EventCursorAdapter(context);
    }


}
