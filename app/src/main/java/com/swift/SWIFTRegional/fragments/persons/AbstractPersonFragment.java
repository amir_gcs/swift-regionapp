package com.swift.SWIFTRegional.fragments.persons;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;


import com.swift.SWIFTRegional.fragments.AbstractDefaultListFragment;
import com.swift.SWIFTRegional.interfaces.ContactDetailListener;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.utils.AddressBookHelper;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.interfaces.RatingListener;
import com.swift.SWIFTRegional.utils.GAHelper;

public abstract class AbstractPersonFragment<T extends BaseAdapter> extends AbstractDefaultListFragment<T, ListView> implements
        AdapterView.OnItemClickListener, ContactDetailListener, RatingListener, View.OnClickListener
{
    protected String sibosKey;
    protected boolean isFavorite;
    protected Speaker speaker;
    protected ViewLoaderStrategyInterface viewLoader;

    protected View layoutContent;
    protected ImageView imageViewPicture;
    protected TextView textViewName;
    protected TextView textViewBusinessFunction;
    protected TextView textViewCompanyName;
    protected TextView textViewPersonExtra;
    protected ImageButton buttonMessage;
    protected boolean userIsLoggedIn;

    protected boolean sendingFavourite;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.userIsLoggedIn = UserPreferencesHelper.loggedIn(getActivity());
        this.setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach()
    {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        // Header views
        this.layoutContent = view.findViewById(R.id.person_details_header);
        this.layoutContent.setBackgroundColor(this.getHeaderColor());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            this.layoutContent.setElevation(getResources().getDimension(R.dimen.elevation_toolbar));
        }

        this.imageViewPicture = (ImageView) this.layoutContent.findViewById(R.id.imageview_person_profile_picture);
        this.textViewName = (TextView) this.layoutContent.findViewById(R.id.textview_person_name);
        this.textViewName.setTextColor(getResources().getColor(R.color.white));
        this.textViewPersonExtra = (TextView) this.layoutContent.findViewById(R.id.textview_person_extra);
        this.textViewPersonExtra.setTextColor(getResources().getColor(R.color.white));
        this.textViewBusinessFunction = (TextView) this.layoutContent.findViewById(R.id.textview_person_business_function);
        this.textViewBusinessFunction.setTextColor(getResources().getColor(R.color.white));
        this.textViewCompanyName = (TextView) this.layoutContent.findViewById(R.id.textview_person_company_name);
        this.textViewCompanyName.setTextColor(getResources().getColor(R.color.white));
        view.findViewById(R.id.separator).setVisibility(View.GONE);
        //this.buttonMessage = (ImageButton) view.findViewById(R.id.button_person_message);

        // Remove the views that are not required here
        this.layoutContent.findViewById(R.id.textview_person_extra).setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        this.listView.setOnItemClickListener(this);

        if (!TextUtils.isEmpty(this.getSibosKey()))
        {
            this.enableMessagingButtonIfAllowed();
        }

        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRefresh();
            }
        });
    }

    /**
     * Disables the messaging button when viewing your own profile.
     * Must be called AFTER the sibosKey has been loaded for this user.
     */
    protected void enableMessagingButtonIfAllowed()
    {
        if (this.getView() != null)
        {
            //if (!UserPreferencesHelper.getLoggedInUser(getView().getContext()).getSibosKey().equals(getSibosKey()))
           // {
                //buttonMessage.setOnClickListener(this);
                //buttonMessage.setVisibility(View.VISIBLE);
           // }
           // else
           // {
           //     buttonMessage.setVisibility(View.GONE);
           // }
        }
    }

    protected abstract int getHeaderColor();

    protected abstract void setAsFavourite(boolean set);

    protected abstract void share();

    protected abstract String getSibosKey();

    protected abstract String getName();

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_program, menu);

        if (!this.shareEnabled())
        {
            menu.removeItem(R.id.action_share);
        }

        if (this.userIsLoggedIn)
        {
            if (this.isFavorite)
            {
                menu.removeItem(R.id.action_favorite);
            }
            else
            {
                menu.removeItem(R.id.action_unfavorite);
            }
        }
        else
        {
            menu.removeItem(R.id.action_favorite);
            menu.removeItem(R.id.action_unfavorite);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_share:
                this.share();
                return true;

            case R.id.action_favorite:
                this.setAsFavourite(true);
                return true;

            case R.id.action_unfavorite:
                this.setAsFavourite(false);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean isPullToRefreshEnabled()
    {
        return false;
    }

    @Override
    protected boolean needsToolbarShadow()
    {
        return false;
    }

    public int getLayoutResourceId()
    {
        return R.layout.fragment_person_details;
    }

    protected boolean shareEnabled()
    {
        return true;

    }

    @Override
    public void ratingSelected(int inRating)
    {

    }

    @Override
    public void showTwitter()
    {

    }

    @Override
    public void showLinkedIn()
    {

    }

    @Override
    public void addToAddressBookBasicDetail() {
        AddressBookHelper.Contact contact = new AddressBookHelper.Contact();

        contact.name = this.speaker.getFirstName() + " " + this.speaker.getLastName();
        contact.jobTitle = this.speaker.getBusinessFunction();
        contact.company = this.speaker.getCompanyName();
        contact.email = this.speaker.getEmail();
        contact.bitmap = ((BitmapDrawable)imageViewPicture.getDrawable()).getBitmap();


            AddressBookHelper.Contact.Address address = new AddressBookHelper.Contact.Address();
            address.country = this.speaker.getCountry();

            contact.address = address;

        AddressBookHelper.updateContact(getActivity(), contact);
    }

    @Override
    public void addToAddressBook()
    {
        GAHelper.trackEvent(this.getView().getContext(), "participant", "add_to_contacts", "");

        AddressBookHelper.Contact contact = new AddressBookHelper.Contact();

        //AddressBookHelper.addContact(getActivity(), contact);
        AddressBookHelper.updateContact(getActivity(), contact);
    }


    @Override
    public void onClick(View view)
    {
//        switch (view.getId())
//        {
//            case R.id.button_person_message:
//                /*if (UserPreferencesHelper.getLoggedInUser(getView().getContext()).getSibosKey().equals(getSibosKey()))
//                {
//                    showToast(R.string.message_error_toast_same_speaker);
//                }*/
//                //else
//
//
//                showToast(R.string.message_error_toast_messaging);
//
//
//                break;
//        }
    }

    @Override
    public void showLoginErrorToast(){
        showToast(R.string.message_error_toast_messaging);
    }


    @Override
    protected void destroyLoaders()
    {

    }

}
