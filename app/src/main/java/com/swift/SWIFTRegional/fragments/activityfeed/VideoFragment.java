package com.swift.SWIFTRegional.fragments.activityfeed;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.facebook.stetho.common.ArrayListAccumulator;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.adapters.activityfeed.YouTubeVideoCursorAdapter;
import com.swift.SWIFTRegional.contentproviders.VideoContentProvider;
import com.swift.SWIFTRegional.fragments.SearchableListInterface;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.goodcoresoftware.android.common.views.Button;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.youtube.YouTubeVideosApiCall;
import com.swift.SWIFTRegional.database.tables.VideoTable;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.video.Video;
import com.swift.SWIFTRegional.utils.ApiErrorHelper;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import java.sql.Time;
import java.util.ArrayList;

/**
 * Created by wesley on 30/06/14.
 */
public class VideoFragment extends AbstractCardsListFragment<YouTubeVideoCursorAdapter> implements AdapterView.OnItemClickListener, SearchableListInterface
{
    private boolean dataLoaded;
    private boolean footerVisible;
    private boolean apiStarted;

    public static String nextToken;

    private static SharedPreferences preferences;
    private static  String tokenStringValue;


    private View footerView;

    public static void setNextToken(String value) {
        nextToken = value;

        if (preferences == null) {
            preferences = MainActivity.preferences;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(tokenStringValue, value);
        editor.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).isWall=true;
        ((MainActivity) getActivity()).setToolbarText("");
        ((MainActivity) getActivity()).HideSpinner(true);
        startLoadingView(true);
        startApiCalls();
    }

    private class ScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int newState){
            // your code there
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {




            //Check if the last view is visible
            if (++firstVisibleItem + visibleItemCount > totalItemCount && !apiStarted && !nextToken.equals("CantLoad")) {
                //load more content
                int visibleCount = visibleItemCount;


                    if (footerView == null)
                        footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_loader, null, false);

                if(!footerVisible)
                {
                    listView.addFooterView(footerView);
                    footerVisible = true;
                }

                startApiCalls();

            }
        }
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        apiStarted=false;
        GAHelper.trackScreen(view.getContext(), "/activity/youtube");
        this.listView.setOnScrollListener(new ScrollListener());
        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);


        com.goodcoresoftware.android.common.views.Button button = (com.goodcoresoftware.android.common.views.Button) view.findViewById(R.id.button_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoadingView(true);
                onRefresh();
            }
        });


    }

    @Override
    public YouTubeVideoCursorAdapter getAdapter(Context context)
    {
        return new YouTubeVideoCursorAdapter(context);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof Video)
            {
                final Video video = (Video) o;

                final Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData((Uri.parse(video.getLink())));

                GAHelper.trackEvent(this.getView().getContext(), "video", "play", video.getTitle());

                this.startActivity(intent);

                return;
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(LoaderIDs.youtube, null, this);
    }

    @Override
    public void startApiCalls()
    {


        //save the already saved token.
        if(preferences==null)
        {
            preferences=MainActivity.preferences;
        }
        tokenStringValue=getString(R.string.nextToken);
        nextToken= preferences.getString(getString(R.string.nextToken),"");

        if(!nextToken.equals("CantLoad")) {

            this.doApiCall(true);
        }


    }

    @Override
    public void onRefresh()
    {
        this.setNextToken("");
        this.doApiCall(true);
    }

    public void doApiCall(boolean force)
    {
        if (super.startApiCalls(force))
        {
            if (force || UpdatePreferencesHelper.isUpdateNeeded(this.getActivity(), UpdatePreferencesHelper.LastUpdateType.VIDEO))
            {

                apiStarted=true;
                this.setIsApiHappening(true);
                this.startApiCallForResult(Api.youtube, YouTubeVideosApiCall.class);
            }
        }
    }

    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(LoaderIDs.youtube, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        switch (id)
        {
            case LoaderIDs.youtube:

                return new CursorLoader(this.getActivity(), getContentUri(), null, null, null, null);
        }

        return null;
    }

    private Uri getContentUri() {
        return StringUtils.isEmpty(this.searchText)
                ? VideoContentProvider.CONTENT_URI
                : VideoContentProvider.CONTENT_URI_SEARCH.buildUpon().appendPath(this.searchText).build();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.youtube:
                this.setCanShow(data.getCount()==0);
                this.adapter.swapCursor(data);


                //mark as read

                if (data.moveToFirst())
                {
                    final long date = DatabaseUtils.getLong(data, VideoTable.COLUMN_DATE);

                    //if the wall is opened we mark everything as read
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.VIDEO, date);
                }

                this.dataLoaded = true;

                break;
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.youtube:
                this.dataLoaded = false;
                this.adapter.swapCursor(null);
                break;
        }

        super.onLoaderReset(loader);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.youtube);
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch (inRequestCode)
        {
            case Api.youtube:
                this.setIsApiHappening(false);
                apiStarted=false;
                if(nextToken.equals("CantLoad")) {
                    footerVisible=false;
                    if(footerView!=null) {
                        try
                        {
                            listView.removeFooterView(footerView);
                            footerView=null;
                        }
                        catch(Exception e)
                        {
                            footerView=null;
                        }
                    }
                }
                if (this.getActivity() != null && !this.isRemoving() && !this.isDetached())
                {
                    if (inResultCode != ApiService.RESULT_SUCCESS)
                    {
                        footerVisible=false;
                        if(footerView!=null) {
                            try
                            {
                                listView.removeFooterView(footerView);
                                footerView=null;
                            }
                            catch(Exception e)
                            {
                                footerView=null;
                            }
                        }
                        //ApiErrorHelper.showError(this.getActivity());

                    }
                }

                this.onRefreshCompleted();

                break;
        }
    }
}
