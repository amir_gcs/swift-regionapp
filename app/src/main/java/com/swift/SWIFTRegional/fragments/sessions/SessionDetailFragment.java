package com.swift.SWIFTRegional.fragments.sessions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.activities.speakers.SpeakerDetailsActivity;
import com.swift.SWIFTRegional.adapters.SessionDetailAdapter;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteExhibitorSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSessionCallback;
import com.swift.SWIFTRegional.contentproviders.RateContentProvider;
import com.swift.SWIFTRegional.contentproviders.UnsentRateContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.fragments.AbstractRateFragment;
import com.swift.SWIFTRegional.interfaces.LocateOnFloorPlanListener;
import com.swift.SWIFTRegional.interfaces.PlayYoutubeVideoListener;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.Event;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.utils.SessionLoader;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.exhibitors.ExhibitorDetailsActivity;
import com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider;
import com.swift.SWIFTRegional.interfaces.RatingListener;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.models.session.SessionSpeaker;
import com.swift.SWIFTRegional.utils.GAHelper;

public class SessionDetailFragment extends AbstractRateFragment<SessionDetailAdapter>
        implements AdapterView.OnItemClickListener,
        RatingListener, LocateOnFloorPlanListener, PlayYoutubeVideoListener, SessionLoader.SessionLoaderListener
{
    public static final String EXTRA_EVENT_TIMING_ID = "event_timing_id";
    public static final String EXTRA_SESSION_ID = "session_id";
    public static final String EXTRA_EXHIBITION_SESSION_ID = "exhibition_session_id";

    private ViewLoaderStrategyInterface viewLoader;
    private boolean isFavorite;
    private boolean isExhibitionSession;

    private EventTiming eventTiming;
    private long eventTimingId;
    private Event event;
    private String sessionId;
    private String exhibitorSessionId;
    private boolean userIsLoggedIn;

    private SessionLoader sessionLoader;

    private boolean sendingFavourite;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach()
    {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        this.listView.setOnItemClickListener(this);
        ((SessionDetailsActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.region_app_backgroundcolor)));

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);

        this.eventTimingId = getArguments().getLong(EXTRA_EVENT_TIMING_ID);
        this.userIsLoggedIn = UserPreferencesHelper.loggedIn(getActivity());
    }

    @Override
    public void startLoaders()
    {
        this.sessionLoader = new SessionLoader(getActivity(), this.getLoaderManager(), this);
        this.sessionLoader.getEventTimings(SessionLoader.UriMode.TIMING, Long.toString(this.eventTimingId), null);
    }


    @Override
    public void onRefresh()
    {
        // Not allowed here.
    }

    @Override
    public void startApiCalls()
    {
        super.startApiCalls();
    }

    @Override
    public SessionDetailAdapter getAdapter(Context context)
    {
        return new SessionDetailAdapter(context, this, this, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_program, menu);

        menu.removeItem(R.id.action_share);
        // Only show favourite button when event is session AND when user is logged in
        //if (!StringUtils.isEmpty(this.sessionId) && this.userIsLoggedIn)
        if (this.userIsLoggedIn)
        {
            if (this.isFavorite)
            {
                menu.removeItem(R.id.action_favorite);
            }
            else
            {
                menu.removeItem(R.id.action_unfavorite);
            }
        }
        else
        {
            menu.removeItem(R.id.action_favorite);
            menu.removeItem(R.id.action_unfavorite);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_share:

                this.shareSession();
                return true;

            case R.id.action_favorite:

                this.setEventAsFavourite(true);
                return true;

            case R.id.action_unfavorite:

                this.setEventAsFavourite(false);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setEventAsFavourite(final boolean set)
    {
        if (!this.sendingFavourite)
        {
            GAHelper.trackEvent(this.getView().getContext(), "favourite", set ? "favourite" : "unfavourite", "session_" + this.sessionId);

            this.sendingFavourite = true;

            SessionDetailFragment.this.isFavorite = set;
            SessionDetailFragment.this.getActivity().supportInvalidateOptionsMenu();


            if (!isExhibitionSession){
                ApiManager.getInstance(this.getActivity()).setFavouriteSession(new SetFavouriteSessionCallback(this.getActivity(), this.sessionId, !set)
                {
                    @Override
                    public void postResult(Void result)
                    {
                        super.postResult(result);

                        SessionDetailFragment.this.sendingFavourite = false;
                    }

                });
            }else{
                ApiManager.getInstance(this.getActivity()).setFavouriteExhibitorSession(new SetFavouriteExhibitorSessionCallback(this.getActivity(), ""+this.exhibitorSessionId, !set)
                {
                    @Override
                    public void postResult(Void result)
                    {
                        super.postResult(result);

                        SessionDetailFragment.this.sendingFavourite = false;
                    }

                });
            }

        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        String sessionId = null;
        String exhibitorSessionId = null;

        if (args != null && args.getString(EXTRA_SESSION_ID) != null)
        {
            sessionId = args.getString(EXTRA_SESSION_ID);
        }
        else if (args != null && args.getString(EXTRA_EXHIBITION_SESSION_ID) != null)
        {
            exhibitorSessionId = args.getString(EXTRA_EXHIBITION_SESSION_ID);
        }

        switch (id)
        {
            case LoaderIDs.favorite:

                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(FavoriteContentProvider.CONTENT_URI_FAVORITE, FavoriteTable.FavouriteType.SESSION.ordinal() + "/" + sessionId), null, null, null, null);

            case LoaderIDs.session_rate:

                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(RateContentProvider.CONTENT_URI_SESSION, this.sessionId), null, null, null, null);

            case LoaderIDs.session_unsent_rate:

                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(UnsentRateContentProvider.CONTENT_URI_UNSENT_SESSION_RATE, this.sessionId), null, null, null, null);

            case LoaderIDs.exhibitor_events:

                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(FavoriteContentProvider.CONTENT_URI_FAVORITE, FavoriteTable.FavouriteType.EXHIBITOR_SESSION.ordinal() + "/" + exhibitorSessionId), null, null, null, null);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.favorite:
            case LoaderIDs.exhibitor_events:
                this.isFavorite = data.getCount() > 0;
                this.getActivity().supportInvalidateOptionsMenu();
                break;

            case LoaderIDs.session_rate:

                if (data.moveToFirst())
                {
                    createRateAndNotify(data, false);
                }
                else
                {
                    final Bundle args = this.getArguments();
                    args.putString(EXTRA_SESSION_ID, this.sessionId);
                    this.getLoaderManager().restartLoader(LoaderIDs.session_unsent_rate, args, this);
                }
                break;

            case LoaderIDs.session_unsent_rate:

                if (data.moveToFirst())
                {
                    createRateAndNotify(data, true);
                }
                break;
        }


    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {

    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return true;
    }

    @Override
    protected void destroyLoaders()
    {
        if (this.sessionLoader != null)
        {
            this.sessionLoader.destroyLoaders();
        }
        this.getLoaderManager().destroyLoader(LoaderIDs.favorite);
        this.getLoaderManager().destroyLoader(LoaderIDs.session_rate);
        this.getLoaderManager().destroyLoader(LoaderIDs.exhibitor_events);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (position == 0)
        {
            return;
        }

        if (this.adapter.getItem(position) instanceof SessionSpeaker)
        {
            SessionSpeaker sessionSpeaker = (SessionSpeaker) this.adapter.getItem(position);

            if (sessionSpeaker != null)
            {
                String speakerName = sessionSpeaker.getSpeaker().getFirstName()+" "+sessionSpeaker.getSpeaker().getLastName();
                final Intent intent = SpeakerDetailsActivity.createIntent(parent.getContext(), String.valueOf(sessionSpeaker.getSpeaker().getSpeakerId()),speakerName);
                this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.SPEAKERS);
            }
        }
        else if (this.adapter.getItem(position) instanceof Exhibitor)
        {
            Exhibitor exhibitor = (Exhibitor) this.adapter.getItem(position);

            if (exhibitor != null)
            {
                final Intent intent = ExhibitorDetailsActivity.createIntent(parent.getContext(), exhibitor);
                this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.EXHIBITORS);
            }
        }
    }

    @Override
    public boolean isPullToRefreshEnabled()
    {
        return false;
    }

    private void shareSession()
    {
        EventTiming eventTiming = this.adapter.getEventTiming();
        Event event = eventTiming.getEvent();

        if (eventTiming != null)
        {
            GAHelper.trackEvent(this.getView().getContext(), "share", "share", "session_" + this.sessionId);

            StringBuilder sb = new StringBuilder();

            if (!TextUtils.isEmpty(event.getTitle()))
            {
                sb.append(event.getTitle()).append("\n");
            }

            if (!TextUtils.isEmpty(event.getDescription()))
            {
                sb.append(event.getDescription()).append("\n");
            }

            sb.append(this.getString(R.string.share_session_url));
            sb.append(eventTiming.getEventId()).append("\n");

            sb.append(this.getString(R.string.twitter_search_hashtag));

            final Intent intent = ShareCompat.IntentBuilder
                    .from(this.getActivity())
                    .setType("text/plain")
                    .setText(sb.toString())
                    .getIntent();

            this.startActivity(intent);
        }
    }

    @Override
    public void ratingSelected(final int rating)
    {
        ApiManager.getInstance(this.getActivity()).setRatingSession(new SetRatingSessionCallback(this.getActivity(), this.sessionId, rating));
    }

    @Override
    public void locateOnFloorPlan(String itemId, FPObjectType objectType)
    {

    }

    @Override
    public void playYoutubeVideo()
    {
        if (this.event instanceof Session)
        {
            Session session = (Session) this.event;

            final Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData((Uri.parse("http://www.youtube.com/watch?v=" + session.getYoutubeId())));

            this.startActivity(intent);
        }
    }

    @Override
    public boolean containsYoutubeVideo()
    {
        if (this.event instanceof Session)
        {
            Session session = (Session) this.event;
            return session.getYoutubeId() != null && !session.getYoutubeId().isEmpty();
        }
        return false;

    }

    private void loadRating()
    {
        if (!TextUtils.isEmpty(this.sessionId))
        {
            ApiManager.getInstance(this.getActivity()).getRatingSession(new GetRatingSessionCallback(this.getActivity(), this.sessionId)
            {

                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);
                }
            });
        }
    }


    @Override
    public void sessionsLoaded(EventTiming[] eventTimings)
    {
        if (this.getView() != null)
        {
            if (eventTimings != null && eventTimings.length > 0)
            {
                this.event = eventTimings[0].getEvent();
                this.eventTiming = eventTimings[0];

                GAHelper.trackScreen(getView().getContext(), "programme/sessions/session/" + eventTiming.getEventId());
                //Log.d("Event Timing", eventTiming.getEventId() + "  " + eventTiming.getId() + "  " + eventTiming.getEvent().getType().getType());
                //Log.d("session id", eventTiming.getEvent().getEventId()+"");
                this.adapter.setEventTiming(this.eventTiming);

                if (this.eventTiming.getEvent() instanceof Session)
                {
                    Session session = (Session) this.eventTiming.getEvent();
                    this.sessionId = "" + session.getSessionId();
                    //Log.d("Real Seesion Id", sessionId);
                    final Bundle args = this.getArguments();
                    args.putString(EXTRA_SESSION_ID, this.sessionId);

                    if (this.userIsLoggedIn)
                    {

                        this.loadRating();
                    }

                    this.isExhibitionSession = false;

                    this.getLoaderManager().restartLoader(LoaderIDs.favorite, args, this);
                    this.getLoaderManager().restartLoader(LoaderIDs.session_rate, args, this);
                }else{
                    //Start loader here for exhibition session
                    this.isExhibitionSession = true;
                    this.exhibitorSessionId = eventTiming.getEventId()+"";
                    final Bundle args = this.getArguments();
                    args.putString(EXTRA_EXHIBITION_SESSION_ID, this.exhibitorSessionId);
                    this.getLoaderManager().restartLoader(LoaderIDs.exhibitor_events, args, this);
                }
            }
        }
    }
}