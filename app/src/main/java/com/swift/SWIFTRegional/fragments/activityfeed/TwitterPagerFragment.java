package com.swift.SWIFTRegional.fragments.activityfeed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.fragments.AbstractPagerFragment;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.TwitterUtils;

import com.swift.SWIFTRegional.R;

import java.util.ArrayList;

public class TwitterPagerFragment extends AbstractPagerFragment
{
    public int selectedTabPosition;
    private LinearLayout tabs;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View view = super.onCreateView(inflater, container, savedInstanceState);
        SetupTabStrip(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        GAHelper.trackScreen(view.getContext(), "/activity/twitter/@sibos");
    }

    @Override
    protected int getLayout()
    {
        return R.layout.fragment_twitter_viewpager;
    }

    @Override
    protected ArrayList<Fragment> getFragments()
    {
        ArrayList<Fragment> fragmentArray = new ArrayList<Fragment>();
        fragmentArray.add(new TwitterNameFragment());
        fragmentArray.add(new TwitterHashtagFragment());
        return fragmentArray;
    }

    @Override
    protected int getTitles()
    {
        return R.array.twitterTabTitles;
    }

    @Override
    protected void tabSelected(int position)
    {
        selectedTabPosition = position;

        switch (position)
        {
            case 0:
                GAHelper.trackScreen(this.getView().getContext(), "/activity/twitter/@sibos");
                break;
            case 1:
                GAHelper.trackScreen(this.getView().getContext(), "/activity/twitter/#sibos");
                break;
        }

        setActiveTab(position);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).isWall=true;
        ((MainActivity) getActivity()).setToolbarText("");
        ((MainActivity) getActivity()).HideSpinner(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_twitter, menu);
    }

    private void SetupTabStrip(View view)
    {
        tabs = (LinearLayout)((PagerSlidingTabStrip) view.findViewById(R.id.tabs)).getChildAt(0);
        for(int i=0; i < tabs.getChildCount(); i++){
            TextView tv = (TextView) tabs.getChildAt(i);
            tv.setTextSize(15);
            if(i == 0){
                tv.setTextColor(getResources().getColor(R.color.white_foreground_color));
            } else {
                tv.setTextColor(getResources().getColor(R.color.region_app_backgroundcolor));
            }
        }
    }

    private void setActiveTab(int position)
    {
        for(int i=0; i < tabs.getChildCount(); i++){
            TextView tv = (TextView) tabs.getChildAt(i);
            if(i == position){
                tv.setTextColor(getResources().getColor(R.color.white_foreground_color));
            } else {
                tv.setTextColor(getResources().getColor(R.color.region_app_backgroundcolor));
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_new:

                if (this.getView() != null)
                {
                    GAHelper.trackScreen(this.getView().getContext(), "/activity/twitter/newtweet");
                    GAHelper.trackEvent(this.getView().getContext(), "twitter", "new_tweet", "post");
                }

                TwitterUtils.tweet(this.getActivity(), this.getString(R.string.twitter_search_hashtag));

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

}