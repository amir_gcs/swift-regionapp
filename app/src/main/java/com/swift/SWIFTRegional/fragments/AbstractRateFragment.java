package com.swift.SWIFTRegional.fragments;

import android.database.Cursor;
import android.widget.ListView;

import com.swift.SWIFTRegional.adapters.AbstractRateAdapter;
import com.swift.SWIFTRegional.models.Rate;

public abstract class AbstractRateFragment<T extends AbstractRateAdapter> extends AbstractDefaultListFragment<T, ListView>
{

    protected void createRateAndNotify(Cursor inData, boolean inIsUnsentRate)
    {
        final Rate rate = new Rate();
        rate.constructFromCursor(inData);
        rate.setUnsent(inIsUnsentRate);
        this.adapter.setRate(rate);
        this.adapter.notifyDataSetChanged();
    }

}
