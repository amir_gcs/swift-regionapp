package com.swift.SWIFTRegional.fragments.activityfeed;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;

import com.swift.SWIFTRegional.fragments.SearchableListInterface;

import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.adapters.activityfeed.TwitterCursorAdapter;
import com.swift.SWIFTRegional.contentproviders.TwitterContentProvider;
import com.swift.SWIFTRegional.database.tables.WallTable;
import com.swift.SWIFTRegional.fragments.AbstractCardsListFragment;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.GAHelper;
import com.swift.SWIFTRegional.utils.TwitterUtils;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Wesley on 16/05/14.
 */
public abstract class AbstractTwitterFeedFragment extends AbstractCardsListFragment<TwitterCursorAdapter> implements SearchableListInterface
{
    private boolean dataLoaded;



    @Override
    public TwitterCursorAdapter getAdapter(Context context)
    {
        return new TwitterCursorAdapter(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        this.setSwipeToRefreshColorScheme(R.array.refresh_colors_purple);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        final Object o = this.adapter.getItem(position);

        if (o != null)
        {
            if (o instanceof WallItem)
            {
                GAHelper.trackEvent(view.getContext(), "twitter", "open_tweet", "");

                final WallItem wallItem = (WallItem) o;

                final String tweetId = wallItem.getId();
                final String username = wallItem.getTitle();

                TwitterUtils.showTweet(this.getActivity(), tweetId, username);

                return;
            }
        }

        super.onItemClick(parent, view, position, id);
    }

    @Override
    public void startLoaders()
    {
        super.startLoaders();

        this.getLoaderManager().restartLoader(this.getLoaderId(), null, this);
    }

    public abstract int getLoaderId();

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        final Loader<Cursor> loader = super.onCreateLoader(id, args);

        if (loader != null)
        {
            return loader;
        }

        if (id == this.getLoaderId())
        {
            return new CursorLoader(this.getActivity(), this.getContentUri(), null, null, null, null);
        }

        return null;
    }



    @Override
    public void onLoadFinished(Loader loader, Cursor data)
    {
        if (loader.getId() == this.getLoaderId())
        {
            this.setCanShow(data.getCount()==0);
            this.adapter.swapCursor(data);

            //mark as read
            if (data.moveToFirst())
            {
                final long date = DatabaseUtils.getLong(data, WallTable.COLUMN_DATE);

                final long lastUpdate = UpdatePreferencesHelper.getLastUpdate(loader.getContext(), UpdatePreferencesHelper.LastSeenType.TWITTER);

                if (date > lastUpdate)
                {
                    UpdatePreferencesHelper.saveLastItemTimestamp(this.getView().getContext(), UpdatePreferencesHelper.LastSeenType.TWITTER, date);
                }
            }

            this.dataLoaded = true;
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        return this.dataLoaded;
    }

    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(this.getLoaderId());
    }

    @Override
    public void onLoaderReset(Loader loader)
    {
        if (loader.getId() == this.getLoaderId())
        {
            this.dataLoaded = false;
            this.adapter.swapCursor(null);
        }

        super.onLoaderReset(loader);
    }

    @Override
    public void restartSearchLoaders() {
        this.getLoaderManager().restartLoader(this.getLoaderId(), null, this);
    }

    abstract public WallItem.Type getTwitterType();

    public Uri getContentUri() {
        return StringUtils.isEmpty(this.searchText)
            ? Uri.withAppendedPath(TwitterContentProvider.CONTENT_URI_TYPE, String.valueOf(getTwitterType().ordinal()))
            : TwitterContentProvider.CONTENT_URI_SEARCH.buildUpon()
                .appendPath(String.valueOf(getTwitterType().ordinal()))
                .appendPath(this.searchText).build();
    }
}
