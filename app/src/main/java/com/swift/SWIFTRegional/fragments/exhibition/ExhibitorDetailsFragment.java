package com.swift.SWIFTRegional.fragments.exhibition;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.adapters.ExhibitorDetailBaseAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.apicalls.program.exhibitor.ExhibitorCollateralApiCall;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteExhibitorCallback;
import com.swift.SWIFTRegional.contentproviders.CategoryContentProvider;
import com.swift.SWIFTRegional.contentproviders.ExhibitorCollateralContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.fragments.AbstractDefaultListFragment;
import com.swift.SWIFTRegional.interfaces.LocateOnFloorPlanListener;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.models.exhibitor.ExhibitorCollateral;
import com.swift.SWIFTRegional.utils.SessionLoader;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import java.util.ArrayList;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;
import com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.utils.GAHelper;


public class ExhibitorDetailsFragment extends AbstractDefaultListFragment<ExhibitorDetailBaseAdapter, ListView> implements AdapterView.OnItemClickListener, LocateOnFloorPlanListener, SessionLoader.SessionLoaderListener
{
    public static final String EXTRA_EXHIBITOR = "exhibitor_id";
    public static final String EXTRA_SIBOSKEY = "sibos_key";

    private ViewLoaderStrategyInterface viewLoader;
    private boolean isFavorite;
    private boolean favoriteLoaded;
    private Exhibitor exhibitor;

    private SessionLoader sessionLoader;

    private boolean sendingFavourite;

    public static Bundle createBundle(final Intent intent)
    {
        if (!intent.hasExtra(EXTRA_EXHIBITOR))
        {
            return null;
        }

        final Bundle bundle = new Bundle();
        bundle.putString(EXTRA_EXHIBITOR, intent.getStringExtra(EXTRA_EXHIBITOR));
        return bundle;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            this.viewLoader = (ViewLoaderStrategyInterface) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement " + ViewLoaderStrategyInterface.class.getSimpleName());
        }
    }

    @Override
    public void onDetach()
    {
        this.viewLoader = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle bundle = this.getArguments();
        if(bundle != null) {
            if(bundle.getString(EXTRA_EXHIBITOR) != null) {
                String exhibitorJson = this.getArguments().getString(EXTRA_EXHIBITOR);
                this.exhibitor = new Gson().fromJson(exhibitorJson, Exhibitor.class);
            } else if(bundle.getString(EXTRA_SIBOSKEY) != null) {
                this.exhibitor = new Exhibitor();
                exhibitor.setCcvk(bundle.getString(EXTRA_SIBOSKEY));
            }
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        this.adapter.setExhibitor(this.exhibitor);
        this.listView.setOnItemClickListener(this);

        GAHelper.trackScreen(view.getContext(), "/exhibition/exhibitors/" + this.exhibitor.getExhibitorId());
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public ExhibitorDetailBaseAdapter getAdapter(Context context)
    {
        return new ExhibitorDetailBaseAdapter(context, this);
    }

    @Override
    public void startLoaders()
    {
        this.favoriteLoaded = false;

        this.getLoaderManager().restartLoader(LoaderIDs.exhibitor_sessions, null, this);

        if (UserPreferencesHelper.loggedIn(getActivity()))
        {
            this.startApiCallForCollateral(this.exhibitor.getCcvk());
            this.getLoaderManager().restartLoader(LoaderIDs.exhibitor_collaterals, null, this);
            this.getLoaderManager().restartLoader(LoaderIDs.exhibitor_representatives, null, this);
            this.getLoaderManager().restartLoader(LoaderIDs.favorite, null, this);

            this.sessionLoader = new SessionLoader(getActivity(), this.getLoaderManager(), this);
            this.sessionLoader.getEventTimings(SessionLoader.UriMode.EVENTS_FOR_EXHIBITOR, this.exhibitor.getCcvk(), "");
        }
    }


    @Override
    public void sessionsLoaded(EventTiming[] eventTimings)
    {
        this.adapter.setExhibitorEvents(eventTimings);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_program, menu);

        if (this.getView() != null)
        {
            if (UserPreferencesHelper.loggedIn(this.getView().getContext()))
            {
                if (this.isFavorite)
                {
                    menu.removeItem(R.id.action_favorite);
                }
                else
                {
                    menu.removeItem(R.id.action_unfavorite);
                }
            }
            else
            {
                menu.removeItem(R.id.action_favorite);
                menu.removeItem(R.id.action_unfavorite);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_share:

                this.shareExhibitor();
                return true;

            case R.id.action_favorite:

                this.addExhibitorToFavorites(true);

                return true;

            case R.id.action_unfavorite:

                this.addExhibitorToFavorites(false);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        switch (id)
        {
            case LoaderIDs.exhibitor_collaterals:
                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(ExhibitorCollateralContentProvider.CONTENT_URI_CCVK, this.exhibitor.getCcvk()), null, null, null, null);

            case LoaderIDs.exhibitor_representatives:
                return null;

            case LoaderIDs.favorite:
                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(FavoriteContentProvider.CONTENT_URI_FAVORITE, FavoriteTable.FavouriteType.EXHIBITOR.ordinal() + "/" + this.exhibitor.getCcvk()), null, null, null, null);

            case LoaderIDs.exhibitor_open_theatres:
                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(CategoryContentProvider.CONTENT_URI_CATEGORY_TYPE, "open_theatre"), null, null, null, null);

            case LoaderIDs.exhibitor_sessions:
                return new CursorLoader(this.getActivity(), Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_EXHIBITOR_SESSIONS, this.exhibitor.getCcvk()), null, null, null, null);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.exhibitor_collaterals:

                ArrayList<ExhibitorCollateral> collaterals = new ArrayList<ExhibitorCollateral>();
                while (data.moveToNext())
                {
                    ExhibitorCollateral collateral = new ExhibitorCollateral();
                    collateral.constructFromCursor(data);
                    collaterals.add(collateral);
                }

                this.adapter.setExhibitorCollaterals(collaterals);
                break;

            case LoaderIDs.exhibitor_representatives:
                break;

            case LoaderIDs.favorite:
                this.isFavorite = data.getCount() > 0;
                this.getActivity().supportInvalidateOptionsMenu();
                favoriteLoaded = true;
                break;

            case LoaderIDs.exhibitor_open_theatres:

                while(data.moveToNext()) {
                    Session session = new Session();
                    session.constructFromCursor(data);
                    if (session.getSessionType() == "OPEN_THEATRE") {
                        Log.v("TESt", "Test : " + session.getSessionId());
                    }
                }
                break;

            case LoaderIDs.exhibitor_sessions:

                EventTiming[] exhibitorSessions = new EventTiming[data.getCount()];

                int i = 0;
                while(data.moveToNext()){
                    EventTiming session = new EventTiming();
                    session.constructFromCursor(data);
                    ((Session) session.getEvent()).constructTagAndStreamFromCursor(SessionLoader.categories);
                    exhibitorSessions[i++] = session;
                }

                this.adapter.setExhibitorSessions(exhibitorSessions);
        }

        super.onLoadFinished(loader, data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.favorite:
                this.favoriteLoaded = false;

            case LoaderIDs.exhibitor_collaterals:
                this.adapter.setExhibitorCollaterals(null);
                break;

            case LoaderIDs.exhibitor_representatives:
                break;
        }
    }

    @Override
    public boolean isPullToRefreshEnabled()
    {
        return false;
    }

    @Override
    protected boolean isAllDataLoaded()
    {
        if (this.favoriteLoaded)
        {
            this.adapter.notifyDataSetChanged();
            return true;
        }

        return false;
    }

    @Override
    protected void destroyLoaders()
    {
        this.getLoaderManager().destroyLoader(LoaderIDs.exhibitor_collaterals);
        this.getLoaderManager().destroyLoader(LoaderIDs.exhibitor_representatives);
        this.sessionLoader.destroyLoaders();
        this.getLoaderManager().destroyLoader(LoaderIDs.favorite);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Object o = this.adapter.getItem(position);
        if (o instanceof ExhibitorCollateral)
        {
            try
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(((ExhibitorCollateral) o).getUrl()));
                getActivity().startActivity(browserIntent);
            }
            catch (ActivityNotFoundException ex)
            {
                // Thrown for invalid urls
            }
        }
        else if (o instanceof EventTiming)
        {
            final Intent intent = SessionDetailsActivity.createIntent(parent.getContext(), ((EventTiming) o).getEventId());
            this.viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.SESSIONS);
        }
    }

    private void shareExhibitor()
    {
        final Exhibitor exhibitor = this.adapter.getExhibitor();
        if (exhibitor != null)
        {
            GAHelper.trackEvent(this.getView().getContext(), "share", "share", "exhibitor_" + this.exhibitor.getExhibitorId());

            StringBuilder sb = new StringBuilder();

            if (!TextUtils.isEmpty(exhibitor.getName()))
            {
                sb.append(exhibitor.getName()).append("\n");
            }

            if (!TextUtils.isEmpty(exhibitor.getDescription()))
            {
                sb.append(exhibitor.getDescription()).append("\n");
            }

            if (!TextUtils.isEmpty(exhibitor.getUrl()))
            {
                sb.append(exhibitor.getUrl()).append("\n");
            }

            sb.append(this.getString(R.string.twitter_search_hashtag));

            final Intent intent = ShareCompat.IntentBuilder.from(this.getActivity()).setType("text/plain").setText(sb.toString()).getIntent();

            this.startActivity(intent);
        }
    }

    private void addExhibitorToFavorites(final boolean set)
    {
        if (!sendingFavourite)
        {
            GAHelper.trackEvent(this.getView().getContext(), "favourite", set ? "favourite" : "unfavourite", "exhibitor_" + this.exhibitor.getExhibitorId());

            this.sendingFavourite = true;
            ExhibitorDetailsFragment.this.isFavorite = set;
            ExhibitorDetailsFragment.this.getActivity().supportInvalidateOptionsMenu();

            ApiManager.getInstance(this.getActivity()).setFavouriteExhibitor(new SetFavouriteExhibitorCallback(this.getActivity(), this.exhibitor.getCcvk(), !set)
            {
                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);

                    ExhibitorDetailsFragment.this.sendingFavourite = false;
                }

            });
        }
    }

    private void startApiCallForCollateral(String inCcvk)
    {
        final Bundle requestParams = new Bundle();
        requestParams.putString(ExhibitorCollateralApiCall.PARAM_EXHIBITORCOLLATORAL, inCcvk);
        this.startApiCallForResult(Api.exhibitor_collateral, ExhibitorCollateralApiCall.class, null, requestParams);
    }

    @Override
    public void onRefresh()
    {

    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {

    }

    @Override
    public void locateOnFloorPlan(String itemId, FPObjectType objectType)
    {
    }

}