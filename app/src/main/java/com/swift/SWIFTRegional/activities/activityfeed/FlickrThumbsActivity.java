package com.swift.SWIFTRegional.activities.activityfeed;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.swift.SWIFTRegional.activities.AbstractToolbarActivity;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.fragments.activityfeed.FlickrPhotosFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.FlickrThumbsFragment;

/**
 * Created by Wesley on 30/05/14.
 */
public class FlickrThumbsActivity extends AbstractToolbarActivity implements ViewLoaderStrategyInterface
{
    public static final String EXTRA_PHOTOSET_ID = FlickrPhotosFragment.EXTRA_PHOTOSET_ID;
    public static final String EXTRA_PHOTOSET_TITLE = FlickrPhotosFragment.EXTRA_PHOTOSET_TITLE;

    public static final Intent getIntent(Context context, String photosetId, String photosetTitle)
    {
        final Intent intent = new Intent(context, FlickrThumbsActivity.class);

        intent.putExtra(EXTRA_PHOTOSET_ID, photosetId);
        intent.putExtra(EXTRA_PHOTOSET_TITLE, photosetTitle);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // update actionbar bg color
        this.getSupportActionBar().setTitle(R.string.photo_gallery);


        final Bundle bundle = FlickrThumbsFragment.createBundle(getIntent());
        this.switchFragment(FlickrThumbsFragment.class, R.id.content_frame, null, bundle);
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.sibos_purple;
    }

    @Override
    public void loadView(Intent intent, ViewType type)
    {
        startActivity(intent);
    }
}
