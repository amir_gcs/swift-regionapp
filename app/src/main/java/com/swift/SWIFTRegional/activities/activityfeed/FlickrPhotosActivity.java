package com.swift.SWIFTRegional.activities.activityfeed;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.BaseActivity;
import com.swift.SWIFTRegional.fragments.activityfeed.FlickrPhotosFragment;

/**
 * Created by Wesley on 20/05/14.
 */
public class FlickrPhotosActivity extends BaseActivity
{
    public static final String EXTRA_PHOTOSET_ID = FlickrPhotosFragment.EXTRA_PHOTOSET_ID;
    public static final String EXTRA_POSITION = FlickrPhotosFragment.EXTRA_POSITION;
    public static final String EXTRA_PHOTOSET_TITLE = FlickrPhotosFragment.EXTRA_PHOTOSET_TITLE;


    public static final Intent getIntent(Context context, String photosetId, String photosetTitle, int position)
    {
        final Intent intent = new Intent(context, FlickrPhotosActivity.class);

        intent.putExtra(EXTRA_PHOTOSET_ID, photosetId);
        intent.putExtra(EXTRA_POSITION, position);
        intent.putExtra(EXTRA_PHOTOSET_TITLE, photosetTitle);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            this.getWindow().setStatusBarColor(getResources().getColor(R.color.black));
        }

        final Bundle bundle = FlickrPhotosFragment.createBundle(getIntent());
        this.switchFragment(FlickrPhotosFragment.class, android.R.id.content, null, bundle);
    }
}
