package com.swift.SWIFTRegional.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.fragments.info.ExternalWebViewFragment;

/**
 * Created by Wesley on 17/06/14.
 */
public class WebViewActivity extends AbstractToolbarActivity
{
    private static final String EXTRA_URL = ExternalWebViewFragment.EXTRA_URL;
    private static final String EXTRA_THEME = "theme";
    private static final String EXTRA_TITLE = "title";

    public static Intent getIntent(Context context, String url, int theme, int title)
    {
        final Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(EXTRA_URL, url);
        intent.putExtra(EXTRA_THEME, theme);
        intent.putExtra(EXTRA_TITLE, title);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.switchFragment(ExternalWebViewFragment.class, R.id.content_frame, ExternalWebViewFragment.class.getSimpleName(), this.getIntent().getExtras());
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.sibos_purple;
    }

    @Override
    public void onBackPressed()
    {
        final ExternalWebViewFragment fragment = (ExternalWebViewFragment) this.getSupportFragmentManager().findFragmentByTag(ExternalWebViewFragment.class.getSimpleName());

        if(!fragment.onBackPressed())
        {
            super.onBackPressed();
        }
    }
}
