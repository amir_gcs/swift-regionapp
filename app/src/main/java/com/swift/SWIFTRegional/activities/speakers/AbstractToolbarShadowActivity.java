package com.swift.SWIFTRegional.activities.speakers;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.AbstractToolbarActivity;

/**
 * Created by SimonRaes on 29/04/15.
 * Used to add a shadow to activities that don't use a tab bar (since shadow is set to the tab bar there).
 */
public abstract class AbstractToolbarShadowActivity extends AbstractToolbarActivity
{
    @Override
    protected int getLayout()
    {
        return R.layout.activity_fragment_toolbar_shadow;
    }
}
