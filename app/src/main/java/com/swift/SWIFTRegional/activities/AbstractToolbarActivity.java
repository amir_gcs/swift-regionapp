package com.swift.SWIFTRegional.activities;

import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.swift.SWIFTRegional.utils.ToolbarUtils;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 3/04/15.
 * Activity that adds a Toolbar above the fragment layout.
 */
public abstract class AbstractToolbarActivity extends BaseActivity
{
    protected Toolbar toolbar;
    private TextView textViewTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(this.getLayout());

        this.toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        if(MainActivity.selectedEventProvider.isEventSelected()) {
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        this.textViewTitle = (TextView) findViewById(R.id.textView_toolbar);

        ToolbarUtils.updateToolbarColor(this, getToolBarColor());
    }

    @Override
    public void setTitle(CharSequence title)
    {
        this.textViewTitle.setText(title);
    }

    public void setToolbarTitleClickListerer(View.OnClickListener listener){
        this.textViewTitle.setOnClickListener(listener);
    }


    protected int getLayout()
    {
        return R.layout.activity_fragment_toolbar;
    }

    public abstract
    @ColorRes
    int getToolBarColor();
}
