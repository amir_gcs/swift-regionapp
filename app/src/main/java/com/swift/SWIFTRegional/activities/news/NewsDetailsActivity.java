package com.swift.SWIFTRegional.activities.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.swift.SWIFTRegional.activities.speakers.AbstractToolbarShadowActivity;
import com.swift.SWIFTRegional.fragments.activityfeed.NewsDetailsFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

import com.swift.SWIFTRegional.R;


public class NewsDetailsActivity extends AbstractToolbarShadowActivity implements ViewLoaderStrategyInterface
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.switchFragment(NewsDetailsFragment.class, R.id.content_frame, null, this.getIntent().getExtras());
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.sibos_purple;
    }

    public static Intent createIntent(final Context inContext, final String inNewsId)
    {
        final Intent intent = new Intent(inContext, NewsDetailsActivity.class);
        intent.putExtra(NewsDetailsFragment.EXTRA_NEWS_ID, inNewsId);

        return intent;
    }

    @Override
    public void loadView(Intent intent, ViewType type)
    {
        startActivity(intent);
    }
}
