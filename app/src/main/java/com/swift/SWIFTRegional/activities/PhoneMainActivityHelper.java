package com.swift.SWIFTRegional.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.swift.SWIFTRegional.adapters.DrawerBaseAdapter;
import com.swift.SWIFTRegional.fragments.DrawerFragment;
import com.swift.SWIFTRegional.interfaces.MainActivityInterface;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;

import com.swift.SWIFTRegional.R;

public class PhoneMainActivityHelper implements MainActivityInterface, DrawerFragment.OnDrawerClickListener
{
    private final MainActivity mainActivity;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private DrawerFragment menuFragment;

    public PhoneMainActivityHelper(final MainActivity inMainActivity)
    {
        this.mainActivity = inMainActivity;
    }

    public void ChangeButton(boolean value)
    {

        drawerToggle.setDrawerIndicatorEnabled(value);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        this.mainActivity.setContentView(R.layout.activity_main);

        // init drawer layout
        this.drawerLayout = (DrawerLayout) this.mainActivity.findViewById(R.id.drawer_layout);
        this.drawerToggle = new ActionBarDrawerToggle(this.mainActivity, this.drawerLayout, R.string.drawer_open, R.string.drawer_closed)
        {
            public void onDrawerClosed(View view)
            {
                super.onDrawerClosed(view);
                PhoneMainActivityHelper.this.mainActivity.supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                PhoneMainActivityHelper.this.mainActivity.supportInvalidateOptionsMenu();
            }
        };


        this.drawerLayout.setDrawerListener(this.drawerToggle);



        // add drawer fragment
        final FragmentManager fragmentManager = this.mainActivity.getSupportFragmentManager();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();

        this.menuFragment = (DrawerFragment) fragmentManager.findFragmentByTag(DrawerFragment.class.getSimpleName());
        if (this.menuFragment == null)
        {
            this.menuFragment = new DrawerFragment();
            this.menuFragment.setOnDrawerClickListener(this);
        }
        if (!this.menuFragment.isAdded())
        {
            transaction.replace(R.id.left_drawer, this.menuFragment, DrawerFragment.class.getSimpleName());
            transaction.commit();
        }

        if (PreferencesHelper.isFirstRun(this.mainActivity))
        {
            this.drawerLayout.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    PhoneMainActivityHelper.this.drawerLayout.openDrawer(Gravity.LEFT);
                }
            }, 250);

            PreferencesHelper.setFirstRun(this.mainActivity);
        }
    }

    @Override
    public void onDestroy()
    {
        this.menuFragment.setOnDrawerClickListener(null);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState)
    {
        // Sync the toggle state after onRestoreInstanceState has occurred.
        this.drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        this.drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return this.drawerToggle != null && this.drawerToggle.onOptionsItemSelected(item);
    }

    @Override
    public void loadView(Intent intent, ViewLoaderStrategyInterface.ViewType type)
    {
        this.mainActivity.startActivity(intent);
    }

    @Override
    public boolean showHomeAsUp()
    {
        return true;
    }

    @Override
    public void onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem inItem)
    {
        this.mainActivity.onDrawerItemClicked(inItem);

        // close the drawerlayout
        this.drawerLayout.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                PhoneMainActivityHelper.this.drawerLayout.closeDrawers();
            }
        }, 250);
    }

    @Override
    public void setHomeAsSelected()
    {
        this.menuFragment.setHomeAsSelected();
    }

    @Override
    public void selectMenuItem(DrawerBaseAdapter.DrawerMenuItem menuItem)
    {
        this.menuFragment.selectMenuItem(menuItem);
    }

}

