package com.swift.SWIFTRegional.activities.sponsors;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.AbstractToolbarActivity;
import com.swift.SWIFTRegional.fragments.speakers.SpeakerDetailsFragment;
import com.swift.SWIFTRegional.fragments.sponsor.SponsorDetailFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.utils.ToolbarUtils;

import com.goodcoresoftware.android.common.views.TextView;

public class SponsorDetailsActivity extends AbstractToolbarActivity implements ViewLoaderStrategyInterface
{

    private TextView textViewToolbar;
    public static Intent createIntent(final Context inContext, final Sponsor sponsor)
    {
        final Intent intent = new Intent(inContext, SponsorDetailsActivity.class);
        final Bundle args = new Bundle();
        final Gson gson = new Gson();
        args.putString("sponsorInfo",gson.toJson(sponsor));
        intent.putExtras(args);

        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home && this.showHomeAsUp())
        {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setToolbarText(String text) {
        if (text.equals("")) {
            this.textViewToolbar.setVisibility(View.GONE);
        } else {
            this.textViewToolbar.setVisibility(View.VISIBLE);
            this.textViewToolbar.setText(text);
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.textViewToolbar = (TextView) findViewById(R.id.textView_toolbar);

        this.updateWindowColoring(R.color.region_app_backgroundcolor,  R.color.general_window_background_white);
        this.switchFragment(SponsorDetailFragment.class, R.id.content_frame, SponsorDetailFragment.class.getSimpleName(), this.getIntent().getExtras());
    }

    private void updateWindowColoring(int actionBarBackgroundColor, int windowBackgroundColor) {
        this.getWindow().setBackgroundDrawableResource(windowBackgroundColor);

        ToolbarUtils.updateToolbarColor(this, actionBarBackgroundColor);
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.region_app_backgroundcolor;
    }

    @Override
    public void loadView(Intent intent, ViewType type)
    {
        startActivity(intent);
    }
}
