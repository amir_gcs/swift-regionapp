package com.swift.SWIFTRegional.activities.search;

import android.content.Intent;
import android.os.Bundle;

import com.swift.SWIFTRegional.activities.AbstractToolbarActivity;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.fragments.search.SearchPagerFragment;

public class SearchActivity extends AbstractToolbarActivity implements ViewLoaderStrategyInterface
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.switchFragment(SearchPagerFragment.class, R.id.content_frame, SearchPagerFragment.class.getSimpleName(), null);
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.theme_gray_search;
    }


    @Override
    public void loadView(Intent intent, ViewType type)
    {
        startActivity(intent);
    }
}
