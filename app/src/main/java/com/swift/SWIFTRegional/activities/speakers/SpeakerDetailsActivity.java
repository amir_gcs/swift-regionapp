package com.swift.SWIFTRegional.activities.speakers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.swift.SWIFTRegional.activities.AbstractToolbarActivity;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.fragments.speakers.SpeakerDetailsFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.utils.ToolbarUtils;

public class SpeakerDetailsActivity extends AbstractToolbarActivity implements ViewLoaderStrategyInterface
{
    public static Intent createIntent(final Context inContext, final String inSpeakerId, String sessionName)
    {
        final Intent intent = new Intent(inContext, SpeakerDetailsActivity.class);
        intent.putExtra(SpeakerDetailsFragment.EXTRA_SPEAKER_ID, inSpeakerId);
        intent.putExtra(SpeakerDetailsFragment.EXTRA_SPEAKER_NAME,sessionName);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ToolbarUtils.updateToolbarColor(this,R.color.region_app_backgroundcolor);
        this.switchFragment(SpeakerDetailsFragment.class, R.id.content_frame, SpeakerDetailsFragment.class.getSimpleName(), this.getIntent().getExtras());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home && this.showHomeAsUp())
        {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.sibos_red;
    }

    @Override
    public void loadView(Intent intent, ViewType type)
    {
        startActivity(intent);
    }
}
