package com.swift.SWIFTRegional.activities.exhibitors;

import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;

import com.google.gson.Gson;
import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.activities.speakers.AbstractToolbarShadowActivity;
import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.ExhibitorTable;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.fragments.exhibition.ExhibitorDetailsFragment;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;

public class ExhibitorDetailsActivity extends AbstractToolbarShadowActivity implements ViewLoaderStrategyInterface
{
    public static Intent createIntent(final Context inContext, final Exhibitor exhibitor)
    {
        final Gson gson = new Gson();
        final String exhibitorJson = gson.toJson(exhibitor);

        final Intent intent = new Intent(inContext, ExhibitorDetailsActivity.class);
        intent.putExtra(ExhibitorDetailsFragment.EXTRA_EXHIBITOR, exhibitorJson);

        return intent;
    }

    private static final int PINTUS_EXHIBITOR = 1;
    private static final int PINTUS_SESSION = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(getResources().getString(R.string.PISExhibitorSchemePiece), "*", PINTUS_EXHIBITOR);
        matcher.addURI("session","*", PINTUS_SESSION);

        Uri uri = intent.getData();

        Bundle bundle = this.getIntent().getExtras();

        if(uri != null) {
            if(matcher.match(uri) == PINTUS_EXHIBITOR) {
                String ccvk = uri.getLastPathSegment();
                Exhibitor e = getExhibitorFromCcvk(ccvk);
                if(e != null) {
                    if(bundle == null) {
                        bundle = new Bundle();
                    }
                    bundle.putString(ExhibitorDetailsFragment.EXTRA_EXHIBITOR, new Gson().toJson(e));
                    this.switchFragment(ExhibitorDetailsFragment.class, R.id.content_frame, ExhibitorDetailsFragment.class.getSimpleName(), bundle);
                }
            } else if(matcher.match(uri) == PINTUS_SESSION) {
                String sessionId = uri.getLastPathSegment();
                SibosUtils.gcLog("got session | id = " + sessionId);
                startActivity(SessionDetailsActivity.createIntent(this, Long.parseLong(sessionId)));
            }
        } else {
            this.switchFragment(ExhibitorDetailsFragment.class, R.id.content_frame, ExhibitorDetailsFragment.class.getSimpleName(), bundle);
        }
    }

    private Exhibitor getExhibitorFromCcvk(String ccvk) {
        final DatabaseHelper helper = new DatabaseHelper(this);
        SQLiteDatabase database = helper.getWritableDatabase();
        String q = "select * from " + ExhibitorTable.TABLE_NAME + " where " + ExhibitorTable.COLUMN_CCVK + " = \"" + ccvk + "\"";
        Cursor c = database.rawQuery( q , null);
        SibosUtils.gcLog("ex count | " + c.getCount());
        if(c.getCount() > 0) {
            c.moveToNext();
            Exhibitor e = new Exhibitor();
            e.constructFromCursor(c);
            return e;
        }
        return null;
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.sibos_orange;
    }

    @Override
    public void loadView(Intent intent, ViewType type)
    {
        startActivity(intent);
    }
}
