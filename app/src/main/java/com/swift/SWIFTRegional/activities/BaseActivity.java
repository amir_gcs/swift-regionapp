package com.swift.SWIFTRegional.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;


import com.facebook.stetho.Stetho;
import com.swift.SWIFTRegional.api.apiservice.SibosApiMessenger;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.framework.app.ITPBaseActivity;
import com.goodcoresoftware.android.common.http.api.ApiCall;
import com.goodcoresoftware.android.common.http.api.utils.ApiMessenger;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.application.SibosApplication;
import com.swift.SWIFTRegional.utils.IntentUtils;


public abstract class BaseActivity extends ITPBaseActivity implements
        ApiMessenger.ApiMessengerListener,
        ApiMessenger.ApiMessengerWrapper
{
    public static final String PARAM_UNAUTHORIZED = "param_unauthorized";

    private SibosApiMessenger apiMessenger;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        this.apiMessenger = new SibosApiMessenger(this, this);
        this.apiMessenger.create();


        this.checkOrientation();

        /*if (!QBChatService.isInitialized())
        {
            QBChatService.init(this);
        }*/

        Stetho.initializeWithDefaults(this.getApplicationContext());
    }

    private void checkOrientation()
    {
        if(!SibosUtils.isGreaterThan600DP(getApplicationContext())) {
            // Always keep in portrait mode for mobile
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        final SibosApplication application = (SibosApplication) getApplicationContext();
        application.activityStarted(this);
    }


    @Override
    protected void onStop()
    {
        super.onStop();
        final SibosApplication application = (SibosApplication) getApplicationContext();
        application.activityStopped();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home && this.showHomeAsUp())
        {
            this.onUpButtonClicked();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * What happens when the home icon was clicked. Default implementation
     * navigates to parent activity or up to home when no parent activity is
     * given
     */
    public void onUpButtonClicked()
    {

        IntentUtils.navigateUpToHome(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        if (this.getSupportActionBar() != null)
        {
            // show drawer toggle
            if (this.showHomeAsUp())
            {
                if(MainActivity.selectedEventProvider.isEventSelected()) {
                    this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                }

            }
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        this.apiMessenger.destroy();
    }

    /**
     * @return true to enable the homebutton to function as "up"
     */
    protected boolean showHomeAsUp()
    {
        return true;
    }

    @Override
    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass)
    {
        return this.apiMessenger.startApiCallForResult(inRequestCode, aClass);
    }

    @Override
    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass, Bundle inHttpRequestParams)
    {
        return this.apiMessenger.startApiCallForResult(inRequestCode, aClass, inHttpRequestParams);
    }

    @Override
    public boolean startApiCallForResult(int inRequestCode, Class<? extends ApiCall<?>> aClass, Bundle inHttpRequestParams, Bundle inConstructorParams)
    {
        return this.apiMessenger.startApiCallForResult(inRequestCode, aClass, inHttpRequestParams, inConstructorParams);
    }

    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle)
    {
        if (bundle.getBoolean(BaseActivity.PARAM_UNAUTHORIZED, false))
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.unauthorized_change_password).setNeutralButton(R.string.unauthorized_neutral_button, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                    if (BaseActivity.this instanceof MainActivity)
                    {
                        ((MainActivity) BaseActivity.this).showProgramme();
                    }
                    else
                    {
                        startActivity(new Intent(BaseActivity.this, MainActivity.class));
                    }
                }
            });
            builder.create().show();
        }
    }

    public void showProgressDialog() {
        if (null == pDialog) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please Wait");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }
        pDialog.show();
    }

    public void dismissProgressDialog() {

        if (null != pDialog) {
            pDialog.dismiss();
        }
    }
}

