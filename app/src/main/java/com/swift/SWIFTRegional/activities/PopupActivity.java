package com.swift.SWIFTRegional.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;

import com.swift.SWIFTRegional.utils.IntentUtils;

import com.swift.SWIFTRegional.R;

/**
 * Activity will appear as a popup on large screens and as a normal activities on smaller screens.<br>
 * <br>
 * Be sure to add <br>{@code android:theme="@style/SibosTheme.Orange.Popup"}<br> to the
 * AndroidManifest.xml entry for each activity that extends this class.
 */
public abstract class PopupActivity extends BaseActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (isTablet())
        {
            showAsPopup(this);
        }
    }

    @Override
    public void finish()
    {
        super.finish();

        this.overridePendingTransition(R.anim.anim_window_close_in_fade, R.anim.anim_window_close_out_fade);
    }

    @Override
    public void onUpButtonClicked()
    {
        IntentUtils.navigateUpToHome(this, true);
    }

    private boolean isTablet()
    {
        return this.getResources().getBoolean(R.bool.is_7inch_tablet) || this.getResources().getBoolean(R.bool.is_10inch_tablet);
    }

    private void showAsPopup(Activity activity)
    {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams params = activity.getWindow().getAttributes();
        params.height = (int) this.getResources().getDimension(getPhoneHeightId());
        params.width = (int) this.getResources().getDimension(getPhoneWidthId());
        params.alpha = 1.0f;
        params.dimAmount = 0.5f;
        activity.getWindow().setAttributes(params);
    }

    protected int getPhoneWidthId()
    {
        return R.dimen.popup_activity_width;
    }

    protected int getPhoneHeightId()
    {
        return R.dimen.popup_activity_height;
    }

}
