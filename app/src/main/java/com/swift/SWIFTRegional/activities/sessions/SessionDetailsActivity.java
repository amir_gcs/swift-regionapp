package com.swift.SWIFTRegional.activities.sessions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.swift.SWIFTRegional.activities.speakers.AbstractToolbarShadowActivity;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.fragments.sessions.SessionDetailFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.utils.ToolbarUtils;

public class SessionDetailsActivity extends AbstractToolbarShadowActivity implements ViewLoaderStrategyInterface
{
    public static Intent createIntent(final Context inContext, long eventTimingId)
    {
        final Intent intent = new Intent(inContext, SessionDetailsActivity.class);
        intent.putExtra(SessionDetailFragment.EXTRA_EVENT_TIMING_ID, eventTimingId);

        return  intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ToolbarUtils.updateToolbarColor(this,R.color.region_app_backgroundcolor);
        this.switchFragment(SessionDetailFragment.class, R.id.content_frame, SessionDetailFragment.class.getSimpleName(), this.getIntent().getExtras());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home && this.showHomeAsUp())
        {
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loadView(Intent intent, ViewType type)
    {
        startActivity(intent);
    }

    @Override
    public int getToolBarColor()
    {
        return R.color.sibos_orange;
    }
}
