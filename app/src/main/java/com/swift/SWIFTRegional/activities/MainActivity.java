package com.swift.SWIFTRegional.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.swift.SWIFTRegional.activities.search.SearchActivity;
import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.adapters.DrawerBaseAdapter;
import com.swift.SWIFTRegional.adapters.FilterSpinnerAdapter;
import com.swift.SWIFTRegional.api.Api;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.AdvertisementsApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.SocialInfoApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterHashTagApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter.TwitterNameApiCall;
import com.swift.SWIFTRegional.api.apicalls.activityfeed.youtube.YouTubeVideosApiCall;
import com.swift.SWIFTRegional.api.apicalls.practicalinfo.PracticalInfoApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.event.EventApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.event.RegionTypeApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.exhibitor.ExhibitorApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.session.CategoryApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.session.SessionApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.speaker.SpeakerApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.speaker.SpeakerCategoryApiCall;
import com.swift.SWIFTRegional.api.apicalls.program.sponsor.SponsorApiCall;
import com.swift.SWIFTRegional.api.callbacks.GetExhibitorEventsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetPrivateSessionsCallback;
import com.swift.SWIFTRegional.contentproviders.MainEventContentProvider;
import com.swift.SWIFTRegional.fragments.DrawerFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.FlickrPhotosetsFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.NewsFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.VideoFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.WallApiHelper;
import com.swift.SWIFTRegional.fragments.event.EventDetailFragment;
import com.swift.SWIFTRegional.fragments.event.EventListFragment;
import com.swift.SWIFTRegional.fragments.event.PracticalInfoFragment;
import com.swift.SWIFTRegional.fragments.event.RegionTypePagerFragment;

import com.swift.SWIFTRegional.fragments.programme.ProgrammePagerFragment;
import com.swift.SWIFTRegional.fragments.programme.SpeakerListFragment;
import com.swift.SWIFTRegional.fragments.sponsor.SponsorDetailFragment;
import com.swift.SWIFTRegional.fragments.sponsor.SponsorListFragment;
import com.swift.SWIFTRegional.interfaces.MainActivityInterface;
import com.swift.SWIFTRegional.interfaces.SpinnerFilterFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.utils.IntentUtils;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.utils.ToolbarUtils;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import com.goodcoresoftware.android.common.utils.Connectivity;
import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.views.TextView;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.api.callbacks.GetPasswordSaltCallback;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterPagerFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.WallFragment;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import retrofit.RetrofitError;

import static android.view.View.GONE;

public class MainActivity extends BaseActivity implements ViewLoaderStrategyInterface, SharedPreferences.OnSharedPreferenceChangeListener {
    public static final int LOGIN_REQUEST = 1;


    public static final String INTENT_ACTION_FRAGMENT = "MainActivity.intent_action_fragment";
    public static final String LOCATE_ROOM_ON_FLOORPLAN = "locate_room_on_floorplan";
    public static final String LOCATE_BEACON_ON_FLOORPLAN = "locate_beacon_on_floorplan";
    public static final String SHOW_MESSAGE_LIST = "show_message_list";
    public static final String SHOW_SESSION_DETAILS = "show_session_details";
    public static final String SHOW_MESSAGE_TAB = "show_message_tab";

    private Boolean showMessageTab = false;
    public static Context appContext = null;
    public static Boolean eventChangedPartner = false;

    public static final String INTENT_EXTRA_EVENT_ID = "EventId";

    protected MainActivityInterface mainActivityHelper;

    public boolean isWall = false;
    private boolean isHome = true;
    private boolean isActivityFirstStart = true;
    private DrawerBaseAdapter.DrawerMenuItem currentMenuItem = DrawerBaseAdapter.DrawerMenuItem.Programme;
    private int theme;
    private int filterColorSelector; // Used to set the color selector in the dropdown filter;

    private boolean isDoingSessionApiCalls; // This activity's onResume method gets called twice (because of the fragment), use this boolean to make sure the api calls only happen once.
    private boolean forceApiCalls; // Set to true if a data refresh was requested by the user (using pull to refresh)

    private Toolbar toolbar;
    private Spinner spinnerToolbar;
    private TextView textViewToolbar;
    private FilterSpinnerAdapter spinnerAdapter;

    public ArrayList<String> backStack;

    public static SharedPreferences preferences;
    public static MainEventContentProvider selectedEventProvider;

    public static Intent createIntentViewSession(Context context) {
        final Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(INTENT_ACTION_FRAGMENT, SHOW_SESSION_DETAILS);
        return intent;
    }

    public static Intent createIntentForMessageList(Context context, Bundle bundle) {
        final Intent intent = new Intent(context, MainActivity.class);
        bundle.putBoolean(SHOW_MESSAGE_LIST, true);
        intent.putExtras(bundle);
        return intent;
    }

    private boolean isBackButton;

    public void ChangeButton(boolean value) {

        ((PhoneMainActivityHelper) this.mainActivityHelper).ChangeButton(value);
        if (!value) {
            isBackButton = true;
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = ResourcesCompat.getDrawable(getResources(), R.drawable.abc_ic_ab_back_material, null);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

        } else {
            isBackButton = false;
            if (MainActivity.selectedEventProvider.isEventSelected()) {
                this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } else {
                this.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        }

    }

    public static void MakeFile(String logText, String fileName) {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(path, fileName + ".txt");

        try {
            path.mkdirs();

            OutputStream os = new FileOutputStream(file);
            os.write(logText.getBytes());
            os.close();

        } catch (IOException e) {

        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SibosUtils.gcLog("onCreate Main");

        super.onCreate(savedInstanceState);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        backStack = new ArrayList<String>();
        /*if (RootUtils.isDeviceRooted()){
            Toast.makeText(this,"This device is Rooted", Toast.LENGTH_LONG).show();
        }*/

        this.setContentView(R.layout.activity_main);
        preferences = this.getPreferences(Context.MODE_PRIVATE);
        try {
            this.theme = this.getPackageManager().getActivityInfo(this.getComponentName(), 0).theme;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(((Object) this).getClass().getSimpleName(), e.getLocalizedMessage());
        }

        appContext = this.getApplicationContext();
        // main activity helper
        this.mainActivityHelper = new PhoneMainActivityHelper(this);

        this.mainActivityHelper.onCreate(savedInstanceState);

        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(this.toolbar);
        this.getSupportActionBar().setTitle("");


        this.spinnerToolbar = (Spinner) findViewById(R.id.spinner_toolbar);
        this.textViewToolbar = (TextView) findViewById(R.id.textView_toolbar);


        if (!this.showSpecificFragment()) {
            if (!selectedEventProvider.isEventSelected()) {
                // Switch to event selection on launch

                this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.Event);
                //close the menut
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            } else if(canShowProgramme()){
                //open the programme now
                this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.Programme);
            } else if((shouldLoadTwitter() || shouldLoadVideo()) && canShowWall()){
                this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.ActivityWall);
            } else if(canShowSponsor()){
                this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.Sponsor);
            } else if(canShowPracticalInfo()){
                this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.PracticalInfo);
            } else {
                this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.SelectedEventInfo);
            }

        }


    }

    private boolean showSpecificFragment() {
        final String selectedFragment = this.getIntent().getStringExtra(INTENT_ACTION_FRAGMENT);

        if (StringUtils.isEmpty(selectedFragment)) {
            return false;
        }

        if(canShowProgramme())
        {
            if (MainActivity.SHOW_SESSION_DETAILS.equals(selectedFragment)) {
                long eventId = this.getIntent().getLongExtra(INTENT_EXTRA_EVENT_ID, -1);
                final Intent intent = SessionDetailsActivity.createIntent(this, eventId);
                this.loadView(intent, ViewLoaderStrategyInterface.ViewType.SESSIONS);

                // Also switch to fragment for back stack purposes
                this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.Programme);

                return true;
            }
        }


        return false;
    }

    public void HideSpinner(boolean show) {
        if (show) {
            spinnerToolbar.setVisibility(View.VISIBLE);
        } else {
            spinnerToolbar.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        SibosUtils.gcLog("onResume Main");

        super.onResume();

        // App might be active in the background, so check the incoming intent here too.
        this.showSpecificFragment();

        UserPreferencesHelper.registerAsListener(this, this);

        // When going from activity wall to login (sets isWall to false) and pressing back, the isWall variable will
        // still be false after arriving back at the activity wall.
        // This causes the SpinnerSelectionListener.onItemSelected to ignore the filter click event because it only works while isWall is true.
        // Fix that here by setting isWall back to true.
        if (this.currentMenuItem == DrawerBaseAdapter.DrawerMenuItem.ActivityWall) {
            this.isWall = true;
        }

        this.mainActivityHelper.selectMenuItem(currentMenuItem);

    }

    @Override
    public void onDestroy() {
        this.mainActivityHelper.onDestroy();
        UserPreferencesHelper.unregisterAsListener(this, this);

        super.onDestroy();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.mainActivityHelper.onPostCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {

        try {
            this.invalidateOptionsMenu();
            Fragment activeFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);


            if (activeFragment instanceof WallFragment || activeFragment instanceof TwitterPagerFragment || activeFragment instanceof VideoFragment) {
                this.isWall = false;
            }

            if (backStack.size() == 1 || activeFragment instanceof  ProgrammePagerFragment) {
                backStack.clear();
                this.finish();
                return;
            }

            String tag = backStack.get(backStack.size() - 2);

            if(tag.equals("selectedEvent"))
            {
                backStack.remove(backStack.size() - 1);
                onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.SelectedEventInfo);
                return;
            }

            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);

            if (fragment != null) {
                //we can show it
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, fragment, tag);
                transaction.commit();
                getSupportFragmentManager().executePendingTransactions();
                backStack.remove(backStack.size() - 1);

            }
            else
            {
                backStack.remove(backStack.size() - 1);
                onBackPressed();
            }


//            this.invalidateOptionsMenu();
//            Fragment activeFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//            int fragments = getSupportFragmentManager().getBackStackEntryCount();
//
//            if(activeFragment instanceof  WallFragment || activeFragment instanceof  TwitterPagerFragment || activeFragment instanceof VideoFragment)
//            {
//                this.isWall=false;
//            }
//
//
//            if (fragments == 1 || activeFragment instanceof  ProgrammePagerFragment) {
//                finish();
//            } else {
//                if (fragments > 1) {
//                    getSupportFragmentManager().popBackStackImmediate();
//                } else {
//                    super.onBackPressed();
//                }
//            }
        } catch (Exception e) {
            return;
        }

    }


    private void showHome() {
        this.mainActivityHelper.setHomeAsSelected();

        this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.Programme);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        this.mainActivityHelper.onConfigurationChanged(newConfig);
    }

    public void setToolbarText(String text) {
        if (text.equals("")) {
            this.textViewToolbar.setVisibility(View.GONE);
        } else {
            this.textViewToolbar.setVisibility(View.VISIBLE);
            this.textViewToolbar.setText(text);
        }
    }

    @Override
    protected boolean showHomeAsUp() {
        return this.mainActivityHelper.showHomeAsUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String title = (String) item.getTitle();

        if (title == null || (title != null && !title.equals("Search"))) {
            if (isBackButton) {
                this.onBackPressed();
                return true;
            } else {
                return this.mainActivityHelper.onOptionsItemSelected(item);
            }
        } else {
            return this.mainActivityHelper.onOptionsItemSelected(item);
        }

    }

    public void switchFragment(java.lang.Class<? extends android.support.v4.app.Fragment> classObject, String tag, android.os.Bundle bundle, boolean addToBackstack) {
        super.switchFragment(classObject, R.id.content_frame, tag, bundle, addToBackstack, 0);
    }

    public void onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem inItem) {
        Class<? extends Fragment> clazz = null;
        int actionBarBackgroundColor = 0;
        int windowBackgroundColor = 0;
        final Bundle args = new Bundle();

        this.isWall = false;
        this.isHome = false;
        boolean showDrawerShadow = false;

        // Set to true if the drawer item linked to a new activity instead of a fragment switch.
        boolean activityStarted = false;

        final boolean isUserLoggedIn = UserPreferencesHelper.loggedIn(this);
        FrameLayout leftMenu = (FrameLayout) findViewById(R.id.left_drawer);
        leftMenu.setVisibility(View.VISIBLE);
        this.textViewToolbar.setGravity(Gravity.NO_GRAVITY);

        String tag = "";
        switch (inItem) {
            case SelectedEventInfo:

                clazz = EventDetailFragment.class;
                actionBarBackgroundColor = R.color.region_app_backgroundcolor;
                windowBackgroundColor = R.color.general_window_background_white;
                this.spinnerToolbar.setVisibility(GONE);
                this.textViewToolbar.setVisibility(View.VISIBLE);
                this.textViewToolbar.setText("Welcome");
                this.theme = R.style.SibosTheme_RegApp;
                this.filterColorSelector = R.color.selector_menu_programme;
                tag = "selectedEvent";
                break;

            case PracticalInfo:
                clazz = PracticalInfoFragment.class;
                this.spinnerToolbar.setVisibility(GONE);
                this.textViewToolbar.setVisibility(View.VISIBLE);
                this.textViewToolbar.setText("Practical Info");
                ((PhoneMainActivityHelper) this.mainActivityHelper).ChangeButton(true);
                actionBarBackgroundColor = R.color.region_app_backgroundcolor;
                windowBackgroundColor = R.color.general_window_background_white;

                tag = "practical";
                this.filterColorSelector = R.color.selector_menu_programme;
                break;

            case Event:
                this.isHome = true;
                leftMenu.setVisibility(GONE);
                this.spinnerToolbar.setVisibility(GONE);
                this.textViewToolbar.setVisibility(View.VISIBLE);
                this.textViewToolbar.setText("SWIFT Event Selection");
                clazz = RegionTypePagerFragment.class;
                actionBarBackgroundColor = R.color.region_app_backgroundcolor;
                windowBackgroundColor = R.color.general_window_background_white;
                tag = "eventselector";
                this.theme = R.style.SibosTheme_RegApp;
                this.filterColorSelector = R.color.selector_menu_programme;

                break;


            case ActivityWall:

                clazz = WallFragment.class;
                actionBarBackgroundColor = R.color.region_app_backgroundcolor;
                windowBackgroundColor = R.color.general_window_background_gray;

                this.isWall = true;
                this.theme = R.style.SibosTheme_RegApp;
                this.filterColorSelector = R.color.selector_menu_activitywall;
                showDrawerShadow = true;
                tag = "social";
                break;

            case Programme:

                clazz = ProgrammePagerFragment.class;
                this.spinnerToolbar.setVisibility(View.VISIBLE);
                this.textViewToolbar.setVisibility(GONE);
                this.textViewToolbar.setText("");
                ((PhoneMainActivityHelper) this.mainActivityHelper).ChangeButton(true);
                actionBarBackgroundColor = R.color.region_app_backgroundcolor;
                windowBackgroundColor = R.color.general_window_background_white;
                tag = "programmes";
                this.theme = R.style.SibosTheme_RegApp;
                this.filterColorSelector = R.color.selector_menu_programme;

                break;

            case Sponsor:

                clazz = SponsorListFragment.class;

                this.spinnerToolbar.setVisibility(GONE);
                this.textViewToolbar.setVisibility(View.VISIBLE);
                this.textViewToolbar.setText("Partners");

                actionBarBackgroundColor = R.color.region_app_backgroundcolor;
                windowBackgroundColor = R.color.general_window_background_white;
                tag = "sponsor";
                this.theme = R.style.SibosTheme_RegApp;
                this.filterColorSelector = R.color.selector_menu_exhibition;

                break;

            case Search:

                this.mainActivityHelper.selectMenuItem(this.currentMenuItem);
                final Intent loginIntent = new Intent(this, SearchActivity.class);
                activityStarted = true;
                this.startActivity(loginIntent);

                break;

            default:

                actionBarBackgroundColor = R.color.region_app_backgroundcolor;
                windowBackgroundColor = R.color.general_window_background_gray;
                this.filterColorSelector = R.color.selector_menu_programme;

                break;
        }

        if (!activityStarted) {
            // save the currently selected menu item so it can be selected when returning from the login activity
            this.currentMenuItem = inItem;

            this.toggleToolbarShadow(showDrawerShadow);
            this.updateWindowColoring(actionBarBackgroundColor, windowBackgroundColor);

            changeFragment(clazz, tag, args, true);
        }
    }


    public void changeFragment(java.lang.Class<? extends android.support.v4.app.Fragment> classObject, String tag, android.os.Bundle bundle, boolean addToBackstack) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if(tag=="programmes")
        {
            fragment=null;
        }

        boolean instack = fragment != null;
        if (fragment == null) {
            try {
                fragment = (Fragment) classObject.newInstance();
                if (bundle != null) {
                    fragment.setArguments(bundle);
                }
            } catch (InstantiationException var9) {
                android.util.Log.e("ERROR", var9.getLocalizedMessage());
                return;
            } catch (IllegalAccessException var10) {
                android.util.Log.e("ERROR", var10.getLocalizedMessage());
                return;
            }
        } else if (fragment.getArguments() != null && bundle != null) {
            fragment.getArguments().putAll(bundle);
        }

        performNoBackStackTransaction(getSupportFragmentManager(), tag, fragment, instack);
    }


    public void performNoBackStackTransaction(FragmentManager fragmentManager, String tag, Fragment fragment, boolean alreadyPresent) {
        final int newBackStackLength = fragmentManager.getBackStackEntryCount() + 1;
        final FragmentManager fFragmentManager = fragmentManager;

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if (fragment.isAdded()) {
            transaction.show(fragment);

        } else {
            transaction.replace(R.id.content_frame, fragment, tag);
        }


        //if (!alreadyPresent && !tag.equals("selectedEvent") && !tag.equals("WallFragment") && !tag.equals("TwitterPager") && !tag.equals("VideoFrag")) {
        if (!tag.equals("WallFragment") && !tag.equals("TwitterPager") && !tag.equals("VideoFrag")) {

            if (backStack.size() == 0 || (backStack.size() >= 1 && !backStack.get(backStack.size() - 1).equals(tag))) {
                backStack.add(tag);
                transaction.addToBackStack(null);
            }
        }


        transaction.commit();
        fFragmentManager.executePendingTransactions();

//        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
//            @Override
//            public void onBackStackChanged() {
//                int nowCount = fFragmentManager.getBackStackEntryCount();
//                if (newBackStackLength != nowCount) {
//                    // we don't really care if going back or forward. we already performed the logic here.
//                    fFragmentManager.removeOnBackStackChangedListener(this);
//
//                    if ( newBackStackLength > nowCount ) { // user pressed back
//                        fFragmentManager.popBackStackImmediate();
//                    }
//                }
//            }
//        });
    }

    public void toggleToolbarShadow(boolean toggleOn) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.toolbar.setElevation(toggleOn ? getResources().getDimension(R.dimen.elevation_toolbar) : 0);
        }
    }

    private void updateWindowColoring(int actionBarBackgroundColor, int windowBackgroundColor) {
        this.getWindow().setBackgroundDrawableResource(windowBackgroundColor);

        ToolbarUtils.updateToolbarColor(this, actionBarBackgroundColor);
    }

    public void doApiCalls(boolean force) {

        if (Connectivity.hasConnection(this) && !this.isDoingSessionApiCalls) {
            this.forceApiCalls = force;

        }

        this.startApiCallForResult(Api.mainevent, EventApiCall.class, null);
    }

    public void doSelectionApiCalls() {
        showProgressDialog();
        onRegionInfoApiCallFinished();
    }


    @Override
    public void onApiCallResult(int inRequestCode, int inResultCode, Bundle bundle) {
        super.onApiCallResult(inRequestCode, inResultCode, bundle);

        switch (inRequestCode) {

            case Api.mainevent:
                this.onMainEventApiCallFinished();
                break;

            case Api.regioninfo:
                this.onRegionInfoApiCallFinished();
                break;

            case Api.category:

                this.onCategoryApiCallFinished();

                break;

            case Api.sponsor:

                this.onSponsorEventCallFinished();

                break;

            case Api.twitter_name:
            case Api.twitter_hashtag:
            case Api.social_info:
            case Api.session:
            case Api.practical_info:

                this.onCallFinished();

                break;


        }
    }

    private boolean eventSelected;

    private void onRegionInfoApiCallFinished() {

        eventSelected = selectedEventProvider.isEventSelected();
        if (eventSelected) {


            this.startApiCallForResult(Api.social_info, SocialInfoApiCall.class, null);

            this.startApiCallForResult(Api.practical_info, PracticalInfoApiCall.class, null);

            this.startApiCallForResult(Api.session, SessionApiCall.class, null);


            this.startApiCallForResult(Api.category, CategoryApiCall.class, null);


            this.startApiCallForResult(Api.speakercategory, SpeakerCategoryApiCall.class, null);
        }
    }

    private void onMainEventApiCallFinished() {

        if (forceApiCalls || UpdatePreferencesHelper.isUpdateNeeded(this, UpdatePreferencesHelper.LastUpdateType.REGIONINFO))
            this.startApiCallForResult(Api.regioninfo, RegionTypeApiCall.class, null);

    }

    private void onTwitterHashCallFinished() {
        if (allModuleLoaded())
            dismissProgressDialog();
    }

    private void onCallFinished() {
        if (allModuleLoaded())
            dismissProgressDialog();
    }

    private void onSponsorEventCallFinished() {

        if (eventSelected) {

            boolean dismissDialog = true;

            if(shouldLoadVideo()){
                dismissDialog  = false;
                this.startApiCallForResult(Api.youtube, YouTubeVideosApiCall.class);
            }

            if(shouldLoadTwitter()){
                dismissDialog  = false;
                final Bundle requestParamsTwitter = new Bundle();
                requestParamsTwitter.putString(TwitterHashTagApiCall.PARAM_SEARCH_HASHTAG, this.getString(R.string.twitter_search_hashtag));
                this.startApiCallForResult(Api.twitter_hashtag, TwitterHashTagApiCall.class, requestParamsTwitter);


                final Bundle requestParamsTwitter1 = new Bundle();
                requestParamsTwitter1.putString(TwitterNameApiCall.PARAM_SEARCH_NAME, this.getString(R.string.twitter_search_name));
                this.startApiCallForResult(Api.twitter_name, TwitterNameApiCall.class, requestParamsTwitter1);
            }

            if(allModuleLoaded() && dismissDialog){
                dismissProgressDialog();
            }

        }


    }

    private void onCategoryApiCallFinished() {
        // Start speaker category.
        if (eventSelected) {

            this.startApiCallForResult(Api.sponsor, SponsorApiCall.class, null);
        }


        if (eventSelected) {

            this.startApiCallForResult(Api.speaker, SpeakerApiCall.class, null);
        }

    }


    private void onPrivateSessionsApiCallFinished() {
        // When user has logged in, also fetch exhibitor events.

        if (UserPreferencesHelper.loggedIn(this) && (this.forceApiCalls || UpdatePreferencesHelper.isUpdateNeeded(this, UpdatePreferencesHelper.LastUpdateType.EXHIBITOR_EVENTS))) {
            ApiManager.getInstance(this).getFavouriteExhibitionSession();

            ApiManager.getInstance(this).getExhibitorEvents(new GetExhibitorEventsCallback(this) {
                @Override
                public void postResult(Void result) {
                    super.postResult(result);
                    MainActivity.this.onExhibitorEventCallFinished();
                }
            });
        } else {
            // Skip straight to the end if the exhibitor event call will be left out
            this.onExhibitorEventCallFinished();
        }
    }

    private void onExhibitorEventCallFinished() {

    }

    public boolean isDoingSessionApiCalls() {
        return this.isDoingSessionApiCalls;
    }

    @Override
    public void loadView(Intent intent, ViewType type) {
        this.mainActivityHelper.loadView(intent, type);
    }

    public int getCurrentTheme() {
        return this.theme;
    }

    public void showProgramme() {
        MainActivity.this.mainActivityHelper.selectMenuItem(DrawerBaseAdapter.DrawerMenuItem.Programme);
        MainActivity.this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.Programme);
    }

    public void showEventInfo() {
        MainActivity.this.mainActivityHelper.selectMenuItem(DrawerBaseAdapter.DrawerMenuItem.SelectedEventInfo);
        MainActivity.this.onDrawerItemClicked(DrawerBaseAdapter.DrawerMenuItem.SelectedEventInfo);
    }

    public DrawerFragment.OnDrawerClickListener getHelper() {
        if (this.mainActivityHelper instanceof DrawerFragment.OnDrawerClickListener) {
            return (DrawerFragment.OnDrawerClickListener) this.mainActivityHelper;
        }
        return null;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(UserPreferencesHelper.LOGGED_IN)) {
            if (UserPreferencesHelper.loggedIn(this)) {
                // Force a reload of the events and related data when a user logs in.
                this.doApiCalls(true);

                // Load the favourite delegates here, the others will be loaded by the normal API calls chain.
                ApiManager.getInstance(this).getFavouriteDelegates();
            }
        }
    }

    public void updateSpinnerContent() {
        Fragment activeFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        // Special case - don't make changes to the Toolbar(filter/title) while the user is browsing Activity Wall fragments.
        if (activeFragment instanceof TwitterPagerFragment || activeFragment instanceof SponsorListFragment) {
            return;
        }

        if (activeFragment instanceof ProgrammePagerFragment && ((ProgrammePagerFragment) activeFragment).selectedTabPosition == 1) {
            return;
        }

        boolean hasFilter = false;

        if (activeFragment instanceof SpinnerFilterFragment) {
            int previousPosition = ((SpinnerFilterFragment) activeFragment).getSpinnerPosition();
            this.spinnerToolbar.setVisibility(View.VISIBLE);
            this.textViewToolbar.setVisibility(GONE);

            SpinnerItem[] filterItems = ((SpinnerFilterFragment) activeFragment).getSpinnerItems();

            if (filterItems != null && filterItems.length > 0) {
                hasFilter = true;

                if (this.spinnerAdapter == null) {
                    this.spinnerAdapter = new FilterSpinnerAdapter(this, filterItems, this.filterColorSelector);
                    this.spinnerToolbar.setAdapter(spinnerAdapter);
                } else {
                    this.spinnerAdapter.setItems(filterItems, this.filterColorSelector);
                }

                if (previousPosition >= 0) {
                    this.spinnerToolbar.setSelection(previousPosition, false);
                    if (activeFragment instanceof ProgrammePagerFragment && ((ProgrammePagerFragment) activeFragment).selectedTabPosition == 0 && previousPosition == 0) {
                        this.spinnerToolbar.setSelection(1, false);
                    }
                }

                this.spinnerToolbar.setOnItemSelectedListener(new SpinnerSelectionListener());
            }
        }

        if (!hasFilter) {
            if (currentMenuItem.titleResourceId != R.string.menu_child_event) {
                try {
                    this.textViewToolbar.setText(getString(currentMenuItem.titleResourceId));
                } catch (Exception e) {

                }

            }

            this.textViewToolbar.setVisibility(View.VISIBLE);
            this.spinnerToolbar.setVisibility(GONE);
        }
    }

    /**
     * Activity wall spinner listener.
     */
    private class SpinnerSelectionListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (MainActivity.this.isWall) {
                MainActivity.this.toggleToolbarShadow(true);

                // Activity Wall needs to switch fragments, so handle that here.
                switch (i) {
                    default:
                    case 0:
                        MainActivity.this.changeFragment(WallFragment.class, "social", new Bundle(), true);
                        break;
                    case 1:
                        MainActivity.this.toggleToolbarShadow(false);
                        MainActivity.this.changeFragment(TwitterPagerFragment.class, "TwitterPager", new Bundle(), true);
                        break;
                    case 2:
                        MainActivity.this.changeFragment(VideoFragment.class, "VideoFrag", new Bundle(), true);
                        break;
                }
            } else {
                // All other filters are handled by the fragments themselves
                Fragment activeFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (activeFragment instanceof SpinnerFilterFragment) {
                    try {
                        ((SpinnerFilterFragment) activeFragment).itemSelected(MainActivity.this.spinnerAdapter.getItem(i), i);
                    } catch (ClassCastException ex) {
                        throw new ClassCastException("Fragment must implement SpinnerFilterListener interface!");
                    }
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private boolean canShowProgramme() {
        return ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SESSION) ||
                ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SPEAKER);
    }

    private boolean canShowWall() {
        return ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER) ||
                ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG) ||
                ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_VIDEO);
    }

    private boolean canShowSponsor() {
        return ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SPONSOR);
    }

    private boolean canShowPracticalInfo() {
        return ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PRACTICAL_INFO);
    }

    private boolean shouldLoadVideo() {
        return ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_VIDEO);
    }

    private boolean shouldLoadTwitter() {
        return ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_TWITTER);
    }

    private boolean allModuleLoaded() {

        boolean condition =
        ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SESSION) &&
        ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SPEAKER) &&
        ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SPONSOR) &&
        ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PRACTICAL_INFO) &&
        ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_INFO_TWITTER) &&
        ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_INFO_VIDEO);

//        return condition;
        if (condition) {

            if (ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_INFO_TWITTER) && ModulePreferenceHelper.getModuleDataAvailable(appContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_TWITTER)) {
                if (ModulePreferenceHelper.getModuleDataLoaded(appContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_TWITTER)) {
                    return true;
                } else  {
                    return false;
                }
            } else {
                return true;
            }

        } else  {
            return false;
        }
    }
}
