package com.swift.SWIFTRegional.api.apicalls.activityfeed;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.contentproviders.AdvertisementContentProvider;

import java.io.IOException;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.models.Advertisement;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 22/04/2014.
 */
public class AdvertisementsApiCall extends AbstractPrivateSibosApiCall
{
    private String uriPath;

    public AdvertisementsApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.uriPath = inContext.getString(R.string.sibos_path_advertisements);
    }

    @Override
    public String getUriPath()
    {
        return this.uriPath;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatusCode, inHeaders, inJsonReader);

        try
        {
            if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
            {
                inJsonReader.beginObject();

                if (inJsonReader.peek() == JsonToken.NAME)
                {
                    final String head = inJsonReader.nextName();
                    if (head.equals("ads"))
                    {
                        if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
                        {
                            inJsonReader.beginObject();

                            if (inJsonReader.peek() == JsonToken.NAME)
                            {
                                final String arrayHead = inJsonReader.nextName();
                                if (arrayHead.equals("item"))
                                {
                                    if (inJsonReader.peek() == JsonToken.BEGIN_ARRAY)
                                    {
                                        final Gson gson = new Gson();
                                        final Advertisement[] advertisements = gson.fromJson(inJsonReader, Advertisement[].class);

                                        final ContentValues[] contentValues = new ContentValues[advertisements.length];

                                        for (int i = 0; i < advertisements.length; i++)
                                        {
                                            contentValues[i] = advertisements[i].getContentValues();
                                        }

                                        inContext.getContentResolver().delete(AdvertisementContentProvider.CONTENT_URI, null, null);
                                        inContext.getContentResolver().bulkInsert(AdvertisementContentProvider.CONTENT_URI, contentValues);

                                        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.ADVERTISEMENTS);

                                        return;
                                    }
                                }
                                else
                                {
                                    inJsonReader.skipValue();
                                }
                            }

                            inJsonReader.endObject();
                        }
                    }
                    else
                    {
                        inJsonReader.skipValue();
                    }
                }

                inJsonReader.endObject();
            }
        }
        catch (IOException e)
        {
            Log.d(this.getClass().getSimpleName(), e.getLocalizedMessage());
        }
    }
}

