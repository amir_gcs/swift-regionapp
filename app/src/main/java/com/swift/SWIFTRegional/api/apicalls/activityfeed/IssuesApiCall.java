package com.swift.SWIFTRegional.api.apicalls.activityfeed;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.IOException;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.contentproviders.IssuesContentProvider;
import com.swift.SWIFTRegional.models.Issue;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 22/04/2014.
 */
public class IssuesApiCall extends AbstractPrivateSibosApiCall
{
    private String path;

    public IssuesApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.path = inContext.getString(R.string.sibos_path_issues);
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try
        {
            if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
            {
                inJsonReader.beginObject();

                if (inJsonReader.peek() == JsonToken.NAME)
                {
                    final String head = inJsonReader.nextName();
                    if (head.equals("issue"))
                    {
                        if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
                        {
                            inJsonReader.beginObject();

                            if (inJsonReader.peek() == JsonToken.NAME)
                            {
                                final String arrayHead = inJsonReader.nextName();
                                if (arrayHead.equals("item"))
                                {
                                    final Gson gson = new Gson();

                                    final Issue[] issues = gson.fromJson(inJsonReader, Issue[].class);
                                    final ContentValues[] contentValues = new ContentValues[issues.length];

                                    for (int i = 0; i < issues.length; i++)
                                    {
                                        contentValues[i] = issues[i].getContentValues();
                                    }

                                    inContext.getContentResolver().delete(IssuesContentProvider.CONTENT_URI, null, null);
                                    inContext.getContentResolver().bulkInsert(IssuesContentProvider.CONTENT_URI, contentValues);

                                    UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.ISSUES);

                                    return;
                                }
                            }

                            inJsonReader.endObject();
                        }
                    }
                    else
                    {
                        inJsonReader.skipValue();
                    }
                }

                inJsonReader.endObject();
            }
        }
        catch (IOException e)
        {
            Log.d(e.getClass().getSimpleName(), e.getLocalizedMessage());
        }
    }
}
