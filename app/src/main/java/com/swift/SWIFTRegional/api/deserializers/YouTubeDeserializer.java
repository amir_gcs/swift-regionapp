package com.swift.SWIFTRegional.api.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import com.swift.SWIFTRegional.models.video.Video;

/**
 * Created by wesley on 30/06/14.
 */
public class YouTubeDeserializer implements JsonDeserializer<Video>
{
    final Gson gson;

    public YouTubeDeserializer()
    {
        this.gson = new Gson();
    }

    @Override
    public Video deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException
    {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final JsonObject jsonObjectSnippet = jsonObject.getAsJsonObject("snippet");

        final Video video = this.gson.fromJson(jsonObjectSnippet, type);

        return video;
    }
}
