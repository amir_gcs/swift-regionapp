package com.swift.SWIFTRegional.api.callbacks;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.models.EventTiming;

import java.util.ArrayList;

import com.swift.SWIFTRegional.contentproviders.EventContentProvider;
import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;
import com.swift.SWIFTRegional.models.ExhibitorEvent;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import retrofit.client.Response;

public abstract class GetExhibitorEventsCallback extends AbstractSibosCallback<Void, ExhibitorEvent[]>
{

    public GetExhibitorEventsCallback(Context context)
    {
        super(context);
    }

    @Override
    public Void doInBackground(ExhibitorEvent[] exhibitorEvents, Response response)
    {
        int i = 10000;  // Set initial id at 10000 so exhibitor events cannot interfere with sessions

        ArrayList<EventTiming> timingsList = new ArrayList<>();

        for(ExhibitorEvent exhibitorEvent : exhibitorEvents)
        {
            //exhibitorEvent.setEventId(i);

            for(EventTiming eventTiming : exhibitorEvent.getEventTimings())
            {
                eventTiming.setEventId(exhibitorEvent.getEventId());
                timingsList.add(eventTiming);
            }

            i++;
        }

        // Convert events to content values

        final ContentValues[] contentValues = new ContentValues[exhibitorEvents.length];

        for (int n = 0; n < contentValues.length; n++)
        {
            contentValues[n] = exhibitorEvents[n].getContentValues();
        }

        // Convert timings to content values

        EventTiming[] timings = timingsList.toArray(new EventTiming[timingsList.size()]);

        final ContentValues[] timingContentValues = new ContentValues[timings.length];

        for (int z = 0; z < timings.length; z++)
        {
            timingContentValues[z] = timings[z].getContentValues();
        }

        this.context.getContentResolver().delete(EventContentProvider.CONTENT_URI_EXHIBITOR_EVENTS, null, null);
        this.context.getContentResolver().bulkInsert(EventContentProvider.CONTENT_URI_EXHIBITOR_EVENTS, contentValues);

        this.context.getContentResolver().delete(EventTimingContentProvider.CONTENT_URI_EXHIBITOR_EVENTS, null, null);
        this.context.getContentResolver().bulkInsert(EventTimingContentProvider.CONTENT_URI_EXHIBITOR_EVENTS, timingContentValues);

        UpdatePreferencesHelper.saveUpdateTimestamp(this.context, UpdatePreferencesHelper.LastUpdateType.EXHIBITOR_EVENTS);

        return null;
    }
}
