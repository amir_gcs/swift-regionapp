package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.RateTable;

public class GetRatingSessionCallback extends AbstractGetRateCallback
{
    public GetRatingSessionCallback(Context context, String externalId)
    {
        super(context, externalId);
    }

    @Override
    protected RateTable.RateType getRateType()
    {
        return RateTable.RateType.SESSION;
    }
}
