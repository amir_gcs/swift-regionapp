package com.swift.SWIFTRegional.api.apicalls.program.speaker;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.models.Speaker;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 1/04/2014.
 */
public class SpeakerApiCall extends AbstractPrivateSibosApiCall {
    public static final String PARAM_YEAR = "year";
    public static final String PARAM_SPEAKER_ID = "speakerId";

    private String path;
    private String year;
    private String speakerId;

    public SpeakerApiCall(Bundle inParams) {
        super(inParams);

        if (inParams.containsKey(PARAM_YEAR)) {
            this.year = inParams.getString(PARAM_YEAR);
        }
    }

    @Override
    public void onTaskPreExecute(Context inContext) {
        super.onTaskPreExecute(inContext);

        // Save the time here so a second SpeakerApiCall can not be started between this and the onTaskPostExecuteWith.
        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.SPEAKERS);

        this.baseUrl = inContext.getString(R.string.sibos_base_url);
        if(MainActivity.selectedEventProvider.isEventSelected())
        {
            long eventId= MainActivity.selectedEventProvider.getSelectedEvent().getEventId();
            this.path = inContext.getString(R.string.sibos_path_speakers).replace("[eventid]", Long.toString(eventId));
        }

    }

    @Override
    public String getUriPath() {
        return this.path;
    }

    @Override
    public void getRequestParams(Bundle inBundle) {
        super.getRequestParams(inBundle);

        if (year != null && !year.equals("")) {
            inBundle.putString(PARAM_YEAR, this.year);
        }
    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SPEAKER, true);
        // Reset the last updated time so it can be automatically updated again.
        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.SPEAKERS, System.currentTimeMillis() - 3601 * 1000);
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader) {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try {
            Gson gson = new Gson();

            final Speaker[] speakers = gson.fromJson(inJsonReader, Speaker[].class);

            final ContentValues[] contentValues = new ContentValues[speakers.length];

            for (int i = 0; i < contentValues.length; i++) {
                contentValues[i] = speakers[i].getContentValues();
            }

            UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.SPEAKERS);

            inContext.getContentResolver().delete(SpeakerContentProvider.CONTENT_URI, null, null);
            inContext.getContentResolver().bulkInsert(SpeakerContentProvider.CONTENT_URI, contentValues);

            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SPEAKER, true);

            if (speakers.length > 0){
                ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SPEAKER, true);
            } else {
                ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SPEAKER, false);
            }
        } catch (Exception e) {

            String s = "Error check";
        }

    }
}
