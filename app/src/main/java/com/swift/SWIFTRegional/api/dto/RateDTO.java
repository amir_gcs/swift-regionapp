package com.swift.SWIFTRegional.api.dto;

public class RateDTO
{
    private String externalId;
    private int rating;

    public RateDTO(String externalId, int rating)
    {
        this.externalId = externalId;
        this.rating = rating;
    }

    public String getExternalId()
    {
        return externalId;
    }

    public int getRating()
    {
        return rating;
    }
}
