package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.RateTable;

public class GetRatingSpeakerCallback extends AbstractGetRateCallback
{
    public GetRatingSpeakerCallback(Context context, String externalId)
    {
        super(context, externalId);
    }

    @Override
    protected RateTable.RateType getRateType()
    {
        return RateTable.RateType.SPEAKER;
    }
}
