package com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.contentproviders.FlickrContentProvider;
import com.swift.SWIFTRegional.models.flickr.FlickrPhoto;

import java.io.IOException;

import com.goodcoresoftware.android.common.utils.Log;

/**
 * Created by Yves on 12/03/14.
 */
public class FlickrPhotosApiCall extends BaseFlickrApiCall
{
    public static final String PARAM_EXTRAS = "extras";
    public static final String PARAM_PHOTOSET_ID = "photoset_id";

    private String photosetId;

    public FlickrPhotosApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        inBundle.putString(PARAM_METHOD, "flickr.photosets.getPhotos");
        inBundle.putString(PARAM_EXTRAS, "date_upload");
        inBundle.putString(PARAM_PER_PAGE, "1000");

        this.photosetId = inBundle.getString(PARAM_PHOTOSET_ID);
    }

    @Override
    public void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try
        {
            if (inJsonReader.peek() == JsonToken.STRING)
            {
                final String head = this.getString(inJsonReader, "");
                if ("jsonFlickrApi(".equals(head))
                {
                    if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
                    {
                        inJsonReader.beginObject();

                        if(inJsonReader.peek() == JsonToken.NAME)
                        {
                            final String key = inJsonReader.nextName();
                            if("photoset".equals(key))
                            {
                                inJsonReader.beginObject();

                                while(inJsonReader.hasNext())
                                {
                                    if (inJsonReader.peek() == JsonToken.NAME)
                                    {
                                        final String photosKey = inJsonReader.nextName();
                                        if ("photo".equals(photosKey))
                                        {
                                            final Gson gson = new Gson();

                                            FlickrPhoto[] photos = gson.fromJson(inJsonReader, FlickrPhoto[].class);
                                            final ContentValues[] cv = new ContentValues[photos.length];
                                            for(int i = 0; i < photos.length; i++)
                                            {
                                                cv[i] = photos[i].getContentValues();
                                            }

                                            final Uri uri = Uri.withAppendedPath(FlickrContentProvider.CONTENT_URI_PHOTO, this.photosetId);

                                            inContext.getContentResolver().delete(uri, null, null);
                                            inContext.getContentResolver().bulkInsert(uri, cv);

                                            return;
                                        }
                                    } else
                                    {
                                        inJsonReader.skipValue();
                                    }
                                }

                                inJsonReader.endObject();
                            }
                        }

                        inJsonReader.endObject();

                    }else
                    {
                        inJsonReader.skipValue();
                    }
                } else
                {
                    inJsonReader.skipValue();
                }
            } else
            {
                inJsonReader.skipValue();
            }
        } catch (IOException e)
        {
            Log.d(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}
