package com.swift.SWIFTRegional.api.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.swift.SWIFTRegional.models.Category;

import java.lang.reflect.Type;

/**
 * Created by Wesley on 4/06/14.
 */
public class CategoryDeserializer implements JsonDeserializer<Category>
{
    final Gson gson;
    final String categoryType;

    public CategoryDeserializer(String categoryType)
    {
        this.gson = new Gson();
        this.categoryType = categoryType;
    }

    @Override
    public Category deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException
    {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        jsonObject.addProperty("type", this.categoryType);

        final Category category = this.gson.fromJson(jsonObject, type);

        return category;
    }
}