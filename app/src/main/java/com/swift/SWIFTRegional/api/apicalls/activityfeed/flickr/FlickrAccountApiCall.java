package com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr;

import android.content.Context;
import android.os.Bundle;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.IOException;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;

/**
 * Created by Wesley on 19/05/14.
 */
public class FlickrAccountApiCall extends BaseFlickrApiCall
{
    public static final String PARAM_USERNAME = "username";

    public FlickrAccountApiCall(Bundle inBundle)
    {
        super(inBundle);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        inBundle.putString(PARAM_METHOD, "flickr.people.findByUsername");
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatusCode, inHeaders, inJsonReader);

        try
        {
            if (inJsonReader.peek() == JsonToken.STRING)
            {
                final String head = this.getString(inJsonReader, "");
                if ("jsonFlickrApi(".equals(head))
                {
                    if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
                    {
                        inJsonReader.beginObject();

                        if (inJsonReader.peek() == JsonToken.NAME)
                        {
                            final String userKey = inJsonReader.nextName();
                            if ("user".equals(userKey))
                            {
                                inJsonReader.beginObject();

                                while(inJsonReader.hasNext())
                                {
                                    if(inJsonReader.peek() == JsonToken.NAME)
                                    {
                                        final String key = inJsonReader.nextName();
                                        if("id".equals(key))
                                        {
                                            final String id = inJsonReader.nextString();

                                            PreferencesHelper.saveFlickrAccountId(inContext, id);

                                            return;
                                        }else
                                        {
                                            inJsonReader.skipValue();
                                        }
                                    }
                                }

                                inJsonReader.endObject();
                            }else
                            {
                                inJsonReader.skipValue();
                            }
                        }else
                        {
                            inJsonReader.skipValue();
                        }

                        inJsonReader.endObject();
                    }else
                    {
                        inJsonReader.skipValue();
                    }
                }
            }
        }catch(IOException e)
        {
            Log.d(this.getClass().getSimpleName(), e.getMessage());
        }
    }
}
