package com.swift.SWIFTRegional.api.apicalls.program.event;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.contentproviders.MainEventContentProvider;
import com.swift.SWIFTRegional.models.event.Event;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import java.util.ArrayList;

/**
 * Created by Inneke on 3/04/14.
 */
public class EventApiCall extends AbstractPrivateSibosApiCall
{
    public static final String HEADER_DATA_TIMESTAMP = "DATA_TIMESTAMP";

    private String path;

    private String lastModified;

    private boolean partialUpdateMode; // Set to true if a lastModified timestamp was supplied - the API will only return the items that were changed since that time

    public EventApiCall(Bundle inParams)
    {
        super(inParams);

        if (inParams.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inParams.getString(HEADER_DATA_TIMESTAMP)))
        {
            this.partialUpdateMode = true;
            this.lastModified = inParams.getString(HEADER_DATA_TIMESTAMP);
        }
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);


        this.baseUrl=inContext.getString(R.string.sibos_base_url);
        this.path = inContext.getString(R.string.sibos_path_event);

    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);
        if (!TextUtils.isEmpty(this.lastModified))
        {
            inBundle.putString("time_stamp", this.lastModified);
        }
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        if (inHeaders.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inHeaders.getString(HEADER_DATA_TIMESTAMP)))
        {
            UpdatePreferencesHelper.saveApiHeaderTimeStamp(
                    inContext,
                    UpdatePreferencesHelper.LastModifiedType.MAINEVENTS,
                    inHeaders.getString(HEADER_DATA_TIMESTAMP));
        }

        final Gson gson = new Gson();
        Event[] events = gson.fromJson(inJsonReader, Event[].class);

        final ContentValues[] contentValues = new ContentValues[events.length];
        Event selectedActivity = MainActivity.selectedEventProvider.getSelectedEvent();

        for (int i = 0; i < events.length; i++)
        {
            if(selectedActivity != null && events[i].getEventId()==selectedActivity.getEventId())
            {
                events[i].setSelected(1);
            }
            contentValues[i] = events[i].getContentValues();
        }

        if (!this.partialUpdateMode)
        {
            // Delete everything - insert everything
            //delete only if there is a existant content provider
            try
            {
                inContext.getContentResolver().delete(MainEventContentProvider.CONTENT_URI, null, null);
            }
            catch(Exception e)
            {

            }


            inContext.getContentResolver().bulkInsert(MainEventContentProvider.CONTENT_URI, contentValues);
        }
        else
        {
            // When receiving a list of updated content, only delete and insert those events.

            // Delete
            ArrayList<ContentProviderOperation> operations = new ArrayList<>();
            ContentProviderOperation operation;
            for (Event item : events)
            {

                operation = ContentProviderOperation
                        .newDelete(Uri.withAppendedPath(MainEventContentProvider.CONTENT_URI_MAINEVENT, Long.toString(item.getEventId()))).build();

                operations.add(operation);
            }
            try
            {
                inContext.getContentResolver().applyBatch(MainEventContentProvider.PROVIDER_NAME, operations);
            }
            catch (RemoteException e)
            {
                e.printStackTrace();
            }
            catch (OperationApplicationException e)
            {
                e.printStackTrace();
            }

        }

        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.MAINEVENTS);
    }
}


