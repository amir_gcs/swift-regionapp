package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;

public class GetFavouriteDelegatesCallback extends AbstractGetFavouritesCallback
{
    public GetFavouriteDelegatesCallback(Context context)
    {
        super(context);
    }

    @Override
    protected FavoriteTable.FavouriteType getType()
    {
        return FavoriteTable.FavouriteType.DELEGATE;
    }
}
