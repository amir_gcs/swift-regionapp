package com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.contentproviders.FlickrContentProvider;

import java.io.IOException;

import com.goodcoresoftware.android.common.utils.Log;

import com.swift.SWIFTRegional.models.flickr.FlickrPhotoset;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 28/04/2014.
 */
public class FlickrPhotosetApiCall extends BaseFlickrApiCall
{
    public FlickrPhotosetApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        inBundle.putString(PARAM_METHOD, "flickr.photosets.getList");
        inBundle.putString(PARAM_PER_PAGE, "1000");
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try
        {
            if (inJsonReader.peek() == JsonToken.STRING)
            {
                final String head = this.getString(inJsonReader, "");
                if ("jsonFlickrApi(".equals(head))
                {
                    if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
                    {
                        inJsonReader.beginObject();

                        if (inJsonReader.peek() == JsonToken.NAME)
                        {
                            final String photosetsKey = inJsonReader.nextName();
                            if ("photosets".equals(photosetsKey))
                            {
                                inJsonReader.beginObject();

                                while(inJsonReader.hasNext())
                                {
                                    if(inJsonReader.peek() == JsonToken.NAME)
                                    {
                                        final String photoKey = inJsonReader.nextName();
                                        if("photoset".equals(photoKey))
                                        {
                                            final Gson gson = new Gson();
                                            FlickrPhotoset[] flickrPhotoset = gson.fromJson(inJsonReader, FlickrPhotoset[].class);

                                            final ContentValues[] contentValues = new ContentValues[flickrPhotoset.length];
                                            for(int i = 0; i < flickrPhotoset.length; i++)
                                            {
                                                contentValues[i] = flickrPhotoset[i].getContentValues();
                                            }

                                            inContext.getContentResolver().delete(FlickrContentProvider.CONTENT_URI_PHOTOSETS, null, null);
                                            inContext.getContentResolver().bulkInsert(FlickrContentProvider.CONTENT_URI_PHOTOSETS, contentValues);

                                            UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.FLICKR);

                                            return;
                                        }
                                    }else
                                    {
                                        inJsonReader.skipValue();
                                    }
                                }

                                inJsonReader.endObject();
                            }
                        }
                    }else
                    {
                        inJsonReader.skipValue();
                    }
                } else
                {
                    inJsonReader.skipValue();
                }
            } else
            {
                inJsonReader.skipValue();
            }
        } catch (IOException e)
        {
            Log.d(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}
