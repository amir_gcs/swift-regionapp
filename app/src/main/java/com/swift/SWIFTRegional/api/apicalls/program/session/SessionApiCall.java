package com.swift.SWIFTRegional.api.apicalls.program.session;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.util.ArrayList;

import com.google.zxing.common.StringUtils;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.contentproviders.EventContentProvider;
import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;
import com.swift.SWIFTRegional.contentproviders.SessionSpeakerContentProvider;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.models.session.SessionSpeaker;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 2/04/2014.
 */
public class SessionApiCall extends AbstractPrivateSibosApiCall {
    private String path;

    public SessionApiCall(Bundle inParams) {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext) {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getString(R.string.sibos_base_url);

        if (MainActivity.selectedEventProvider.isEventSelected()) {
            long eventId = MainActivity.selectedEventProvider.getSelectedEvent().getEventId();
            this.path = inContext.getString(R.string.sibos_path_sessions).replace("[eventid]", Long.toString(eventId));
        }

    }

    @Override
    public String getUriPath() {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader) {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();

            final Session[] sessions = gson.fromJson(inJsonReader, Session[].class);

            ArrayList<EventTiming> timingsList = new ArrayList<>();
            ArrayList<SessionSpeaker> sessionSpeakerList = new ArrayList<>();

            int i = 1;

            for (Session session : sessions) {
                if (session.getSessionType() != "OPEN_THEATRE") {
                    // Api sometimes returns a session where this is set to PRIVATE, but sessions from this API call should always be NORMAL.
                    session.setSessionType("NORMAL");
                }
                session.setSessionId(i);
                i++;
                if (session.getEventTimings() != null) {
                    for (EventTiming eventTiming : session.getEventTimings()) {

                        //assign temporary id using int

                        eventTiming.setEventId(session.getSessionId());
                        timingsList.add(eventTiming);

                    }
                }

                if (session.getSessionSpeakers() != null) {
                    for (SessionSpeaker sessionSpeaker : session.getSessionSpeakers()) {
                        sessionSpeaker.setSessionId(session.getSessionId());
                        sessionSpeakerList.add(sessionSpeaker);
                    }
                }

            }

            // Convert events to content values

            final ContentValues[] contentValues = new ContentValues[sessions.length];

            for (int n = 0; n < contentValues.length; n++) {
                contentValues[n] = sessions[n].getContentValues();
            }

            // Convert timings to content values

            EventTiming[] timings = timingsList.toArray(new EventTiming[timingsList.size()]);

            final ContentValues[] timingContentValues = new ContentValues[timings.length];

            for (int z = 0; z < timings.length; z++) {
                timingContentValues[z] = timings[z].getContentValues();
            }

            // Convert session speakers to content values

            SessionSpeaker[] speakers = sessionSpeakerList.toArray(new SessionSpeaker[sessionSpeakerList.size()]);

            final ContentValues[] speakerContentValues = new ContentValues[speakers.length];

            for (int y = 0; y < speakers.length; y++) {
                speakerContentValues[y] = speakers[y].getContentValues();
            }

            inContext.getContentResolver().delete(EventContentProvider.CONTENT_URI_SESSIONS, null, null);
            inContext.getContentResolver().bulkInsert(EventContentProvider.CONTENT_URI_SESSIONS, contentValues);

            inContext.getContentResolver().delete(EventTimingContentProvider.CONTENT_URI_SESSIONS, null, null);
            inContext.getContentResolver().bulkInsert(EventTimingContentProvider.CONTENT_URI_SESSIONS, timingContentValues);

            inContext.getContentResolver().delete(SessionSpeakerContentProvider.CONTENT_URI, null, null);
            inContext.getContentResolver().bulkInsert(SessionSpeakerContentProvider.CONTENT_URI, speakerContentValues);

            UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.SESSIONS);

            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SESSION, true);

            if (sessions.length > 0){
                ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SESSION, true);
            } else {
                ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SESSION, false);
            }

        } catch (Exception e) {

            String s = "Error check";
        }

    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PROGRAMME_SESSION, true);
    }


}