package com.swift.SWIFTRegional.api.apiservice;

/**
 * Created by wesley on 04/07/14.
 */

import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.client.methods.HttpPost;

import java.net.URI;

@NotThreadSafe
public class HttpDeleteWithBody extends HttpPost
{
    public static final String METHOD_NAME = "DELETE";

    public String getMethod() { return METHOD_NAME; }

    public HttpDeleteWithBody(final String uri) {
        super();
        setURI(URI.create(uri));
    }

    public HttpDeleteWithBody(final URI uri) {
        super();
        setURI(uri);
    }
    public HttpDeleteWithBody() { super(); }
}
