package com.swift.SWIFTRegional.api.apicalls.profile;

import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateBase64ApiCall;

import java.util.HashMap;

import com.goodcoresoftware.android.common.utils.Base64;
import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.User;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

/**
 * Created by SimonRaes on 25/03/15.
 * Submits the user's login credentials and receives the user's info (name, etc) and tokens.
 */
public class UserApiCall extends AbstractPrivateBase64ApiCall
{
    private String path;

    public final static String USERNAME = "com.swift.SWIFTRegional.api.apicalls.profile.user";
    public final static String PASSWORD = "com.swift.SWIFTRegional.api.apicalls.profile.password";

    protected String username;
    protected String password;

    public UserApiCall(Bundle inParams)
    {
        super(inParams);

        this.username = inParams.getString(USERNAME, "");
        this.password = inParams.getString(PASSWORD, "");
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.path = inContext.getString(R.string.sibos_path_authenticate);
    }

    @Override
    public void getRequestHeaders(HashMap<String, String> inHeaders)
    {
        super.getRequestHeaders(inHeaders);

        StringBuilder sb = new StringBuilder();
        String credentials = sb.append(this.username).append(':').append(this.password).toString();
        byte[] arrayBase64 = Base64.encode(credentials.getBytes(), Base64.DEFAULT);
        sb = new StringBuilder();
        for (byte c : arrayBase64)
        {
            sb.append((char) c);
        }
        String base64 = sb.toString();
        if (base64.endsWith("\n"))
        {
            base64 = base64.substring(0, base64.length() - 1);
        }
        inHeaders.put("Authorization", "Basic " + base64);
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    public String getUri()
    {
        return this.baseUrl + this.getUriPath();
    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException)
    {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);

        UserPreferencesHelper.logout(inContext);
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try
        {
            final User user = new Gson().fromJson(inJsonReader, User.class);
            UserPreferencesHelper.storeLoggedInUserDetails(inContext, user);

            String accessToken = inHeaders.getString(UserPreferencesHelper.ACCESS_TOKEN, "");
            String refreshToken = inHeaders.getString(UserPreferencesHelper.ACCESS_REFRESH_TOKEN, "");
            String clientId = inHeaders.getString(UserPreferencesHelper.ACCESS_CLIENT_ID, "");
            String clientSecret = inHeaders.getString(UserPreferencesHelper.ACCESS_CLIENT_SECRET, "");

            UserPreferencesHelper.storeTokens(inContext, accessToken, refreshToken, clientId, clientSecret);
        }
        catch (Exception e)
        {
            Log.d(((Object) this).getClass().getSimpleName(), e.getMessage());
        }
    }
}
