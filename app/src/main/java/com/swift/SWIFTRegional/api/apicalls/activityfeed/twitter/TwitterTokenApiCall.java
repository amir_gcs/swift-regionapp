package com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import com.goodcoresoftware.android.common.http.api.JsonApiCall;
import com.goodcoresoftware.android.common.http.http.AbstractHttpClient;
import com.goodcoresoftware.android.common.http.http.exceptions.ApiCallFailedException;
import com.goodcoresoftware.android.common.http.http.utils.RawBody;
import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;

/**
 * Created by Wesley on 19/06/14.
 */
public class TwitterTokenApiCall extends JsonApiCall
{
    private String baseUrl;

    public TwitterTokenApiCall(Bundle bundle)
    {
        super(bundle);
    }

    @Override
    public String getUri()
    {
        return this.baseUrl;
    }

    @Override
    public AbstractHttpClient.Method getMethod()
    {
        return AbstractHttpClient.Method.POST;
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getString(R.string.twitter_token_url);
    }

    @Override
    public void getRequestHeaders(HashMap<String, String> inHeaders)
    {
        super.getRequestHeaders(inHeaders);

        try
        {
            final String encodedKey = URLEncoder.encode("IslKG7fh5ruRMCSpb8zVaKtJW", "UTF-8");
            final String encodedSecret = URLEncoder.encode("JqcnbVVmZjz3gCAUF8CIpCPHusYJNX4mIudwUOVWOfBI8vxade", "UTF-8");

            inHeaders.put("Authorization", "Basic " + Base64.encodeToString((encodedKey + ":" + encodedSecret).getBytes(), Base64.NO_WRAP));
            inHeaders.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        }catch(UnsupportedEncodingException e)
        {

        }
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        inBundle.putParcelable("raw", new RawBody("grant_type=client_credentials"));
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatusCode, inHeaders, inJsonReader);

        try
        {
            if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
            {
                final Gson gson = new Gson();

                final Token token = gson.fromJson(inJsonReader, Token.class);

                if("bearer".equals(token.tokenType))
                {
                    PreferencesHelper.saveTwitterToken(inContext, token.accessToken);

                    return;
                }
            }
        } catch(IOException e)
        {
            Log.e(this.getClass().getSimpleName(), e.getLocalizedMessage());
        }

        throw new ApiCallFailedException("No token found");
    }

    private static final class Token
    {
        @SerializedName("token_type")
        private String tokenType;

        @SerializedName("access_token")
        private String accessToken;
    }
}
