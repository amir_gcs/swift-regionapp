package com.swift.SWIFTRegional.api.apicalls.profile;

import android.content.Context;
import android.os.Bundle;

import com.google.gson.JsonObject;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateTokenPostApiCall;

/**
 * Created by Akhmas on 18/7/2016.
 */
public class SaveSocialSettingsCall extends AbstractPrivateTokenPostApiCall {

    private String path;

    public final static String TWITTER_ID = "twitter_id";
    public final static String LINKEDIN_URL = "linkedin_url";

    protected String twitterId;
    protected String linkedinUrl;

    public SaveSocialSettingsCall(Bundle inParams) {
        super(inParams);
        this.twitterId = inParams.getString(TWITTER_ID, "");
        this.linkedinUrl = inParams.getString(LINKEDIN_URL, "");
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.path = inContext.getString(R.string.sibos_path_post_social_links);
    }

    @Override
    public String getUriPath() {
        return this.path;
    }

    @Override
    protected String getRawData()
    {
        if (this.twitterId != null && this.linkedinUrl != null)
        {
            final JsonObject o = new JsonObject();
            o.addProperty("twitterId", this.twitterId);
            o.addProperty("linkedinUrl", this.linkedinUrl);

            return o.toString();
        }
        return "";
    }
}
