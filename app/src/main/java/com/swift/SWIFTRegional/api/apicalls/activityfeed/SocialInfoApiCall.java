package com.swift.SWIFTRegional.api.apicalls.activityfeed;

import android.content.Context;
import android.os.Bundle;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.models.SocialInfo;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;

/**
 * Created by amir.naushad on 12/13/2017.
 */

public class SocialInfoApiCall extends AbstractPrivateSibosApiCall {
    private String path;

    public SocialInfoApiCall(Bundle inParams) {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext) {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getString(R.string.sibos_base_url);

        if (MainActivity.selectedEventProvider.isEventSelected()) {
            long eventId = MainActivity.selectedEventProvider.getSelectedEvent().getEventId();
            this.path = inContext.getString(R.string.sibos_path_social_info).replace("[eventid]", Long.toString(eventId));
        }

    }

    @Override
    public String getUriPath() {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader) {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();

            final SocialInfo socialInfo = gson.fromJson(inJsonReader, SocialInfo.class);


            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_INFO_TWITTER, true);
            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_INFO_VIDEO, true);

            if (socialInfo != null){
                if(socialInfo.getTwitter() != null)
                    ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_TWITTER, true);

                if(socialInfo.getYoutube() != null)
                    ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_VIDEO, true);
            }

        } catch (Exception e) {

            String s = "Error check";
        }

    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        if(inStatusCode==404)
        {
            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_INFO_TWITTER, true);
            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_INFO_VIDEO, true);
        }
    }


}

