package com.swift.SWIFTRegional.api.apicalls.program.event;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.contentproviders.RegionInfoContentProvider;
import com.swift.SWIFTRegional.contentproviders.TypeInfoContentProvider;
import com.swift.SWIFTRegional.models.event.RegionInfo;
import com.swift.SWIFTRegional.models.event.RegionType;
import com.swift.SWIFTRegional.models.event.TypeInfo;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import java.util.ArrayList;



/**
 * Created by Inneke on 3/04/14.
 */
public class RegionTypeApiCall extends AbstractPrivateSibosApiCall {
    public static final String HEADER_DATA_TIMESTAMP = "DATA_TIMESTAMP";

    public static String urlToCall;

    private String path;

    private String lastModified;

    private boolean partialUpdateMode; // Set to true if a lastModified timestamp was supplied - the API will only return the items that were changed since that time
    private Toast toast;

    public RegionTypeApiCall(Bundle inParams) {
        super(inParams);

        if (inParams.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inParams.getString(HEADER_DATA_TIMESTAMP))) {
            this.partialUpdateMode = true;
            this.lastModified = inParams.getString(HEADER_DATA_TIMESTAMP);
        }
    }

    @Override
    public void onTaskPreExecute(Context inContext) {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getString(R.string.sibos_base_url);
        this.path = inContext.getString(R.string.sibos_path_regiontypes);



    }

    @Override
    public void getRequestParams(Bundle inBundle) {
        super.getRequestParams(inBundle);
        if (!TextUtils.isEmpty(this.lastModified)) {
            inBundle.putString("time_stamp", this.lastModified);
        }
    }

    @Override
    public String getUriPath() {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader) {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        if (inHeaders.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inHeaders.getString(HEADER_DATA_TIMESTAMP))) {
            UpdatePreferencesHelper.saveApiHeaderTimeStamp(
                    inContext,
                    UpdatePreferencesHelper.LastModifiedType.REGIONINFO,
                    inHeaders.getString(HEADER_DATA_TIMESTAMP));
        }

        final Gson gson = new Gson();
        RegionType regionandtypes = gson.fromJson(inJsonReader, RegionType.class);

        final ContentValues[] typeContentValues = new ContentValues[regionandtypes.types.length];
        final ContentValues[] regionContentValues = new ContentValues[regionandtypes.regions.length];

        for (int i = 0; i < regionandtypes.types.length; i++) {
            regionandtypes.types[i].setTypeId(Long.parseLong(regionandtypes.types[i].getId()));
            typeContentValues[i] = regionandtypes.types[i].getContentValues();
        }

        for (int i = 0; i < regionandtypes.regions.length; i++) {
            regionandtypes.regions[i].setRegionId(Long.parseLong(regionandtypes.regions[i].getId()));
            regionContentValues[i] = regionandtypes.regions[i].getContentValues();
        }

        if (!this.partialUpdateMode) {
            // Delete everything - insert everything
            //delete only if there is a existant content provider
            try {
                inContext.getContentResolver().delete(RegionInfoContentProvider.CONTENT_URI, null, null);
                inContext.getContentResolver().delete(TypeInfoContentProvider.CONTENT_URI, null, null);
            } catch (Exception e) {

            }


            inContext.getContentResolver().bulkInsert(RegionInfoContentProvider.CONTENT_URI, regionContentValues);
            inContext.getContentResolver().bulkInsert(TypeInfoContentProvider.CONTENT_URI, typeContentValues);
        } else {
            // When receiving a list of updated content, only delete and insert those events.

            // Delete Types and regions
            ArrayList<ContentProviderOperation> operations = new ArrayList<>();
            ContentProviderOperation operation;
            for (TypeInfo item : regionandtypes.types) {

                operation = ContentProviderOperation
                        .newDelete(Uri.withAppendedPath(TypeInfoContentProvider.CONTENT_URI_TYPEINFO, Long.toString(item.getTypeId()))).build();

                operations.add(operation);
            }

            try {
                inContext.getContentResolver().applyBatch(TypeInfoContentProvider.PROVIDER_NAME, operations);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }

            //empty the operations and do it for regions now
            operations.clear();

            for (RegionInfo item : regionandtypes.regions) {

                operation = ContentProviderOperation
                        .newDelete(Uri.withAppendedPath(RegionInfoContentProvider.CONTENT_URI_REGION, Long.toString(item.getRegionId()))).build();

                operations.add(operation);
            }

            try {
                inContext.getContentResolver().applyBatch(RegionInfoContentProvider.PROVIDER_NAME, operations);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }


        }

        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.REGIONINFO);
    }
}





