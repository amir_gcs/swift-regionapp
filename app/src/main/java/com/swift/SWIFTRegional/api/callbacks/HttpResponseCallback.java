package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import retrofit.client.Response;

/**
 * Created by SimonRaes on 20/08/15.
 * Empty callback that just checks if the call got a success result.
 */
public class HttpResponseCallback extends AbstractSibosCallback<Boolean, Void>
{
    public HttpResponseCallback(Context context)
    {
        super(context);
    }

    @Override
    public Boolean doInBackground(Void result, Response response)
    {
        return response.getStatus() == 200;
    }

}
