package com.swift.SWIFTRegional.api.apiservice;

import android.content.Context;
import android.os.Bundle;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.MySSLTrust;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.goodcoresoftware.android.common.http.http.AbstractHttpClient;
import com.goodcoresoftware.android.common.http.http.AbstractResponse;
import com.goodcoresoftware.android.common.http.http.impl.InputStreamHttpClient;
import com.goodcoresoftware.android.common.http.http.utils.RawBody;
import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;

/**
 * Created by wesley on 04/07/14.
 */
public class SibosApiServiceHttpClient extends InputStreamHttpClient
{
    private Charset defaultCharset;
    private HttpParams httpParams;
    private DefaultHttpClient defaultHttpClient;
    private DefaultHttpClient defaultHttpClientExternal;
    private Context inContext;

    public SibosApiServiceHttpClient(final Context inContext)
    {
        super(inContext);
        this.inContext = inContext;
        this.httpParams = createDefaultHttpParams(inContext);
        this.defaultCharset = Charset.forName("UTF-8");
    }

    @Override
    public DefaultHttpClient getDefaultHttpClient()
    {
        Log.d(this.getClass().getSimpleName(), "== DefaultHttpClient ==");

        if(this.defaultHttpClient == null) {
            this.defaultHttpClient = new DefaultHttpClient(MySSLTrust.getConnectionManager(inContext),this.httpParams);
            this.defaultHttpClient.addRequestInterceptor(createRequestInterceptor());
            this.defaultHttpClient.addResponseInterceptor(createResponseInterceptor());
        }

        defaultHttpClient.getCookieStore().clear();
        return defaultHttpClient;
    }

    public DefaultHttpClient getDefaultHttpClientForExternalCalls(){
        Log.d(this.getClass().getSimpleName(), "== External DefaultHttpClient ==");
        if(this.defaultHttpClientExternal == null){
            this.defaultHttpClientExternal = new DefaultHttpClient(this.httpParams);
            this.defaultHttpClientExternal.addRequestInterceptor(createRequestInterceptor());
            this.defaultHttpClientExternal.addResponseInterceptor(createResponseInterceptor());
        }

        defaultHttpClientExternal.getCookieStore().clear();
        return defaultHttpClientExternal;
    }

    @Override
    public AbstractResponse<InputStream> httpDeleteRequest(String inUrl, Bundle inRequestParams, HashMap<String, String> inHeaders) throws ClientProtocolException, IOException
    {
        Log.d("Public Delete Request => ", inUrl);

        /*
         * Call the API
		 */

        if (isDebuggerConnected())
        {
            Log.d("DELETE", inUrl);
        }

		/*
         * HTTP DELETE call
		 */
        final DefaultHttpClient httpClient = this.getDefaultHttpClientByUrl(inUrl);
        final HttpDeleteWithBody httpDelete = new HttpDeleteWithBody(inUrl);

        // add default headers
        final HashMap<String, String> defaultHeaders = this.getDefaultHeaders();
        for (Map.Entry<String, String> entry : defaultHeaders.entrySet())
        {
            httpDelete.addHeader(entry.getKey(), entry.getValue());
        }

        // add custom headers
        if (inHeaders != null && inHeaders.size() > 0)
        {
            for (Map.Entry<String, String> entry : inHeaders.entrySet())
            {
                httpDelete.addHeader(entry.getKey(), entry.getValue());
            }
        }

        boolean fileUpload = false;
        if (inRequestParams != null && inRequestParams.size() > 0)
        {
            final MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

            final Set<String> keys = inRequestParams.keySet();
            final TreeSet<String> sortedKeys = new TreeSet<String>(keys);

            boolean entitySet = false;
            for (String key : sortedKeys)
            {
                final Object object = inRequestParams.get(key);

                if (isDebuggerConnected())
                {
                    Log.d("POST param", key + " => " + object);
                }

                if (object instanceof File)
                {
                    multipartEntityBuilder.addPart(key, new FileBody((File) object));
                    fileUpload = true;
                } else if (object instanceof ContentBody)
                {
                    multipartEntityBuilder.addPart(key, (ContentBody) object);
                    if (object instanceof FileBody)
                    {
                        fileUpload = true;
                    }
                } else if (object instanceof RawBody)
                {
                    final RawBody rawBody = (RawBody) object;
                    final StringEntity rawEntity = new StringEntity(
                            rawBody.getRaw(),
                            rawBody.getCharset());
                    rawEntity.setContentType(rawBody.getMimeType());
                    httpDelete.setEntity(rawEntity);
                    entitySet = true;
                    break;
                } else
                {
                    multipartEntityBuilder.addPart(key, new StringBody(String.valueOf(object), "text/plain", this.defaultCharset));
                }
            }

            if (!entitySet)
            {
                httpDelete.setEntity(multipartEntityBuilder.build());
            }
        }

        this.preExecuteHook(httpClient, httpDelete);
        final AbstractResponse<InputStream> response = httpClient.execute(httpDelete, this.createResponseHandler(Method.DELETE));
        this.postExecuteHook(httpClient, response);

        if (isDebuggerConnected())
        {
            Log.d("DELETE response", response == null || response.getBody() == null ? "" : String.valueOf(response.getStatusCode()));
        }

        return response;
    }

    private DefaultHttpClient getDefaultHttpClientByUrl(String url){
        String sibosApiBase = inContext.getResources().getString(R.string.sibos_base_url);
//        if(!url.startsWith(sibosApiBase))
//        {
//            MainActivity.MakeFile(url,"testUrl");
//        }
        return this.getDefaultHttpClientForExternalCalls();
       // return url.startsWith(sibosApiBase) ? this.getDefaultHttpClient() : this.getDefaultHttpClientForExternalCalls();
    }

    @Override
    public AbstractResponse<InputStream> httpGetRequest(String inUrl, Bundle inRequestParams, HashMap<String, String> inHeaders) throws ClientProtocolException, IOException {

        Log.d("Public Get Request => ", inUrl);
        String callUrl = inUrl + this.convertRequestParamsToQueryString(inUrl, inRequestParams);
        if(isDebuggerConnected()) {
            android.util.Log.d("GET", callUrl);
        }

        // separate DefaultHttpClient for calls made to urls other than sibos. Such as twitter, flicks etc
        DefaultHttpClient httpClient = getDefaultHttpClientByUrl(inUrl);
        HttpGet httpGet = new HttpGet(callUrl);
        HashMap defaultHeaders = this.getDefaultHeaders();
        Iterator response = defaultHeaders.entrySet().iterator();
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), httpConnectionTimeoutFileUploads);
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), httpSocketTimeoutFileUploads);


        Map.Entry entry;
        while(response.hasNext()) {
            entry = (Map.Entry)response.next();
            httpGet.addHeader((String)entry.getKey(), (String)entry.getValue());
        }

        if(inHeaders != null && inHeaders.size() > 0) {
            response = inHeaders.entrySet().iterator();

            while(response.hasNext()) {
                entry = (Map.Entry)response.next();
                httpGet.addHeader((String)entry.getKey(), (String)entry.getValue());
            }
        }


        this.preExecuteHook(httpClient, httpGet);
        AbstractResponse response1 = (AbstractResponse)httpClient.execute(httpGet, this.createResponseHandler(AbstractHttpClient.Method.GET));



        this.postExecuteHook(httpClient, response1);
        if(isDebuggerConnected()) {
            android.util.Log.d("GET response", response1 != null && response1.getBody() != null ? String.valueOf(response1.getStatusCode()) : "");
        }

        return response1;
    }

    public AbstractResponse<InputStream> httpPostRequest(String inUrl, Bundle inRequestParams, HashMap<String, String> inHeaders) throws ClientProtocolException, IOException {
        Log.d("Public Post Request => ", inUrl);
        HttpPost httpPost = new HttpPost(inUrl);
        boolean fileUpload = false;
        HashMap defaultHeaders = this.getDefaultHeaders();
        Iterator httpClient = defaultHeaders.entrySet().iterator();

        Map.Entry response;
        while(httpClient.hasNext()) {
            response = (Map.Entry)httpClient.next();
            httpPost.addHeader((String)response.getKey(), (String)response.getValue());
        }

        if(inHeaders != null) {
            httpClient = inHeaders.entrySet().iterator();

            while(httpClient.hasNext()) {
                response = (Map.Entry)httpClient.next();
                httpPost.addHeader((String)response.getKey(), (String)response.getValue());
            }
        }

        if(inRequestParams != null && inRequestParams.size() > 0) {
            MultipartEntityBuilder httpClient1 = MultipartEntityBuilder.create();
            Set response2 = inRequestParams.keySet();
            TreeSet sortedKeys = new TreeSet(response2);
            boolean entitySet = false;
            Iterator i$ = sortedKeys.iterator();

            while(i$.hasNext()) {
                String key = (String)i$.next();
                Object object = inRequestParams.get(key);
                if(isDebuggerConnected()) {
                    android.util.Log.d("POST param", key + " => " + object);
                }

                if(object instanceof File) {
                    httpClient1.addPart(key, new FileBody((File)object));
                    fileUpload = true;
                } else if(object instanceof ContentBody) {
                    httpClient1.addPart(key, (ContentBody)object);
                    if(object instanceof FileBody) {
                        fileUpload = true;
                    }
                } else {
                    if(object instanceof RawBody) {
                        RawBody rawBody = (RawBody)object;
                        StringEntity rawEntity = new StringEntity(rawBody.getRaw(), rawBody.getCharset());
                        rawEntity.setContentType(rawBody.getMimeType());
                        httpPost.setEntity(rawEntity);
                        entitySet = true;
                        break;
                    }

                    httpClient1.addPart(key, new StringBody(String.valueOf(object), "text/plain", this.defaultCharset));
                }
            }

            if(!entitySet) {
                httpPost.setEntity(httpClient1.build());
            }
        }

        if(isDebuggerConnected()) {
            android.util.Log.d("POST", inUrl);
        }

        DefaultHttpClient httpClient2 = getDefaultHttpClientByUrl(inUrl);
        if(fileUpload) {
            HttpConnectionParams.setConnectionTimeout(httpClient2.getParams(), httpConnectionTimeoutFileUploads);
            HttpConnectionParams.setSoTimeout(httpClient2.getParams(), httpSocketTimeoutFileUploads);
        }

        this.preExecuteHook(httpClient2, httpPost);
        AbstractResponse response1 = (AbstractResponse)httpClient2.execute(httpPost, this.createResponseHandler(AbstractHttpClient.Method.POST));
        this.postExecuteHook(httpClient2, response1);
        if(isDebuggerConnected()) {
            android.util.Log.d("POST response", response1 != null && response1.getBody() != null ? String.valueOf(response1.getStatusCode()) : "");
        }

        return response1;
    }

    public AbstractResponse<InputStream> httpPutRequest(String inUrl, Bundle inRequestParams, HashMap<String, String> inHeaders) throws ClientProtocolException, IOException {
        Log.d("Public Put Request => ", inUrl);
        HttpPut httpPut = new HttpPut(inUrl);
        boolean fileUpload = false;
        HashMap defaultHeaders = this.getDefaultHeaders();
        Iterator httpClient = defaultHeaders.entrySet().iterator();

        Map.Entry response;
        while(httpClient.hasNext()) {
            response = (Map.Entry)httpClient.next();
            httpPut.addHeader((String)response.getKey(), (String)response.getValue());
        }

        if(inHeaders != null) {
            httpClient = inHeaders.entrySet().iterator();

            while(httpClient.hasNext()) {
                response = (Map.Entry)httpClient.next();
                httpPut.addHeader((String)response.getKey(), (String)response.getValue());
            }
        }

        if(inRequestParams != null && inRequestParams.size() > 0) {
            MultipartEntityBuilder httpClient1 = MultipartEntityBuilder.create();
            Set response2 = inRequestParams.keySet();
            TreeSet sortedKeys = new TreeSet(response2);
            boolean entitySet = false;
            Iterator i$ = sortedKeys.iterator();

            while(i$.hasNext()) {
                String key = (String)i$.next();
                Object object = inRequestParams.get(key);
                if(isDebuggerConnected()) {
                    android.util.Log.d("PUT param", key + " => " + object);
                }

                if(object instanceof File) {
                    httpClient1.addPart(key, new FileBody((File)object));
                    fileUpload = true;
                } else if(object instanceof ContentBody) {
                    httpClient1.addPart(key, (ContentBody)object);
                    if(object instanceof FileBody) {
                        fileUpload = true;
                    }
                } else {
                    if(object instanceof RawBody) {
                        RawBody rawBody = (RawBody)object;
                        StringEntity rawEntity = new StringEntity(rawBody.getRaw(), rawBody.getCharset());
                        rawEntity.setContentType(rawBody.getMimeType());
                        httpPut.setEntity(rawEntity);
                        entitySet = true;
                        break;
                    }

                    httpClient1.addPart(key, new StringBody(String.valueOf(object), "text/plain", this.defaultCharset));
                }
            }

            if(!entitySet) {
                httpPut.setEntity(httpClient1.build());
            }
        }

        if(isDebuggerConnected()) {
            android.util.Log.d("PUT", inUrl);
        }

        DefaultHttpClient httpClient2 = getDefaultHttpClientByUrl(inUrl);
        if(fileUpload) {
            HttpConnectionParams.setConnectionTimeout(httpClient2.getParams(), httpConnectionTimeoutFileUploads);
            HttpConnectionParams.setSoTimeout(httpClient2.getParams(), httpSocketTimeoutFileUploads);
        }

        this.preExecuteHook(httpClient2, httpPut);
        AbstractResponse response1 = (AbstractResponse)httpClient2.execute(httpPut, this.createResponseHandler(AbstractHttpClient.Method.POST));
        this.postExecuteHook(httpClient2, response1);
        if(isDebuggerConnected()) {
            android.util.Log.d("PUT response", response1 != null && response1.getBody() != null ? String.valueOf(response1.getStatusCode()) : "");
        }

        return response1;
    }

    public AbstractResponse<InputStream> httpHeadRequest(String inUrl, Bundle inRequestParams, HashMap<String, String> inHeaders) throws ClientProtocolException, IOException {

        Log.d("Public Head Request => ", inUrl);

        String callUrl = inUrl + this.convertRequestParamsToQueryString(inUrl, inRequestParams);
        if(isDebuggerConnected()) {
            android.util.Log.d("HEAD", callUrl);
        }

        HttpHead httpHead = new HttpHead(callUrl);
        HashMap defaultHeaders = this.getDefaultHeaders();
        Iterator httpClient = defaultHeaders.entrySet().iterator();

        Map.Entry response;
        while(httpClient.hasNext()) {
            response = (Map.Entry)httpClient.next();
            httpHead.addHeader((String)response.getKey(), (String)response.getValue());
        }

        if(inHeaders != null) {

            httpClient = inHeaders.entrySet().iterator();

            while(httpClient.hasNext()) {
                response = (Map.Entry)httpClient.next();
                httpHead.addHeader((String)response.getKey(), (String)response.getValue());
            }
        }

        DefaultHttpClient httpClient1 = getDefaultHttpClientByUrl(inUrl);
        this.preExecuteHook(httpClient1, httpHead);
        AbstractResponse response1 = (AbstractResponse)httpClient1.execute(httpHead, this.createResponseHandler(AbstractHttpClient.Method.HEAD));
        this.postExecuteHook(httpClient1, response1);
        if(isDebuggerConnected()) {
            android.util.Log.d("HEAD response", "Headers received");
        }

        return response1;
    }

}
