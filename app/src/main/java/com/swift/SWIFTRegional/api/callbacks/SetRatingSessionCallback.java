package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.RateTable;

public class SetRatingSessionCallback extends AbstractSetRatingCallback
{
    public SetRatingSessionCallback(Context context, String externalId, int rating)
    {
        super(context, externalId, rating);
    }

    @Override
    protected RateTable.RateType getType()
    {
        return RateTable.RateType.SESSION;
    }
}
