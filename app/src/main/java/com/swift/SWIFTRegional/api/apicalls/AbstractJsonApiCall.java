package com.swift.SWIFTRegional.api.apicalls;

import android.os.Bundle;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.IOException;
import java.util.HashMap;

import com.goodcoresoftware.android.common.http.api.JsonApiCall;

public abstract class AbstractJsonApiCall extends JsonApiCall
{
    protected static final String HEADER_LAST_MODIFIED = "Last-Modified";
    public static final String LAST_MODIFIED = "lastModified";
    private static final String COOKIE_HEADER = "Cookie";

    public AbstractJsonApiCall(final Bundle inBundle)
    {
        super(inBundle);
    }

    @Override
    public abstract String getUri();

    public abstract String getUriPath();

    @Override
    public void getRequestHeaders(HashMap<String, String> inHeaders)
    {
        super.getRequestHeaders(inHeaders);
        inHeaders.put(COOKIE_HEADER, "");
        inHeaders.put("Accept", "application/json;charset=UTF-8");
    }

    protected String getString(JsonReader inJsonReader, String defaultValue) throws IOException
    {
        if (inJsonReader.peek() == JsonToken.STRING)
        {
            return inJsonReader.nextString();
        }
        else
        {
            inJsonReader.skipValue();
        }

        return defaultValue;
    }
}
