package com.swift.SWIFTRegional.api.callbacks;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.models.favourites.Favourite;
import retrofit.client.Response;

public abstract class AbstractGetFavouritesCallback extends AbstractSibosCallback<Void, Favourite[]>
{
    public AbstractGetFavouritesCallback(Context context)
    {
        super(context);
    }

    protected abstract FavoriteTable.FavouriteType getType();

    @Override
    public Void doInBackground(Favourite[] result, Response response)
    {
        this.beforeLocalUpdate();

        final ContentValues[] contentValues = new ContentValues[result.length];

        for (int i = 0; i < contentValues.length; i++)
        {
            result[i].setType(getType());
            contentValues[i] = result[i].getContentValues();
        }

        // Delete all existing favourites of this type
        this.context.getContentResolver().delete(FavoriteContentProvider.CONTENT_URI, FavoriteTable.COLUMN_TYPE + "=" + this.getType().ordinal(), null);

        // Add the latest list of favourites of this type
        this.context.getContentResolver().bulkInsert(FavoriteContentProvider.CONTENT_URI, contentValues);

        this.afterLocalUpdate();

        return null;
    }

    /**
     * Gives subclasses a change to perform actions with the old data before the new data replaces it.
     */
    protected void beforeLocalUpdate()
    {
        // Nothing here
    }

    protected void afterLocalUpdate()
    {
        // Nothing, only for subclasses.
    }

}
