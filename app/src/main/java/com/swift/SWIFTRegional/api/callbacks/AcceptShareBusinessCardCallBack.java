package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.models.businessshare.BusinessCardCode;

import retrofit.client.Response;

/**
 * Created by SimonRaes on 30/04/15.
 */
public class AcceptShareBusinessCardCallBack extends AbstractSibosCallback<BusinessCardCode, BusinessCardCode>
{
    String code;

    public AcceptShareBusinessCardCallBack(Context context, String code)
    {
        super(context);
        this.code = code;
    }

    @Override
    public BusinessCardCode doInBackground(BusinessCardCode result, Response response)
    {
        return result;
    }

    public String getCode()
    {
        return this.code;
    }
}
