package com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.contentproviders.TwitterContentProvider;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterHashtagFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterNameFragment;
import com.swift.SWIFTRegional.models.WallItem;

import java.io.IOException;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 12/03/14.
 */
public class TwitterHashTagApiCall extends AbstractTwitterApiCall
{
    public static final String PARAM_SEARCH_HASHTAG = "q";

    private String path;
    private String searchHashtag;

    public TwitterHashTagApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public String getUriPath()
    {
        return path;
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        if(MainActivity.selectedEventProvider.isEventSelected())
        {
            long eventId= MainActivity.selectedEventProvider.getSelectedEvent().getEventId();
            this.path = inContext.getString(R.string.twitter_path_tweets).replace("[eventid]", Long.toString(eventId));

            if(TwitterHashtagFragment.nextToken==null)
            {
                TwitterHashtagFragment.nextToken="";
            }

            if(!TwitterHashtagFragment.nextToken.isEmpty())
            {
                this.path += TwitterHashtagFragment.nextToken;
            }
        }

        this.searchHashtag = inContext.getString(R.string.twitter_search_hashtag);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
//        super.getRequestParams(inBundle);
//
//        inBundle.putString(PARAM_SEARCH_HASHTAG, this.searchHashtag);
    }

    @Override
    public Uri getContentUri()
    {
        return Uri.withAppendedPath(TwitterContentProvider.CONTENT_URI_TYPE, String.valueOf(WallItem.Type.TWITTER_HASHTAG.ordinal()));
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        inData.putString("Api","TwitterHash");

        if(ModulePreferenceHelper.getModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_TWITTER)){
            super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);


            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG, true);
        }
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_TWITTER_HASHTAG, true);

//                        if (videos.length > 0){
        ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG, true);
//                        } else {
//                            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG, false);
//                        }


    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        if(inStatusCode==404)
        {
            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_TWITTER, true);
            TwitterHashtagFragment.setNextToken("CantLoad");
        }
    }
}
