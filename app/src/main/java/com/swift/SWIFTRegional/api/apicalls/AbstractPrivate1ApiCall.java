package com.swift.SWIFTRegional.api.apicalls;

import android.content.Context;
import android.os.Bundle;

import com.swift.SWIFTRegional.R;

/**
 * Created by Wesley on 21/05/14.
 *
 */
public abstract class AbstractPrivate1ApiCall extends AbstractPrivateBase64ApiCall
{
    public AbstractPrivate1ApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getResources().getString(R.string.sibos_base_url);
    }

    @Override
    public String getUri()
    {
        return this.baseUrl + this.getUriPath();
    }
}
