package com.swift.SWIFTRegional.api.callbacks;

import android.os.AsyncTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Wesley on 2/13/15.
 */
public abstract class BaseCallback<S, T> implements Callback<T>
{
    @Override
    public final void success(T result, Response response)
    {
        new ProcessAsyncTask<>(this).execute(new RetrofitResponse<>(result, response));
    }

    @Override
    public void failure(RetrofitError retrofitError)
    {
        this.onFailure(retrofitError);
    }

    public abstract void onFailure(RetrofitError retrofitError);

    /**
     * Processing of the API result should be done here and will be in a background thread
     * @param result
     * @param response
     * @return
     */
    public abstract S doInBackground(T result, Response response);

    /**
     * The result of method doInBackground will be given as a parameter in this method. This method is executed on the main thread.
     * Warning, the result is transferred from a background thread to the main thread!
     * @param result
     */
    public void postResult(S result)
    {

    }

    public static class ProcessAsyncTask<S, T> extends AsyncTask<RetrofitResponse<T>, Void, S>
    {
        private BaseCallback<S, T> baseCallback;

        public ProcessAsyncTask(BaseCallback<S, T> baseCallback)
        {
            this.baseCallback = baseCallback;
        }

        @SafeVarargs
        @Override
        protected final S doInBackground(RetrofitResponse<T>... params)
        {
            final RetrofitResponse<T> param = params[0];

            return baseCallback.doInBackground(param.result, param.response);
        }

        @Override
        protected void onPostExecute(S result)
        {
            baseCallback.postResult(result);
        }
    }

    public static class RetrofitResponse<T>
    {
        private T result;
        private Response response;

        public RetrofitResponse(T result, Response response)
        {
            this.result = result;
            this.response = response;
        }
    }
}
