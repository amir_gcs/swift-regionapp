package com.swift.SWIFTRegional.api;

import android.content.Context;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.swift.SWIFTRegional.api.callbacks.GetExhibitorEventsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteExhibitorsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteSpeakersCallback;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.GetRegionInfoCallBack;
import com.swift.SWIFTRegional.api.callbacks.GetRegistrationCodeCallback;
import com.swift.SWIFTRegional.api.callbacks.HttpResponseCallback;
import com.swift.SWIFTRegional.api.callbacks.PollShareBusinessCardCallBack;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteExhibitorCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSessionCallback;
import com.swift.SWIFTRegional.api.interceptors.TokenExpiredInterceptor;
import com.swift.SWIFTRegional.models.PrivacySettings;
import com.swift.SWIFTRegional.models.businessshare.BusinessCardCode;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.swift.SWIFTRegional.BuildConfig;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.callbacks.AbstractSetFavouriteCallback;
import com.swift.SWIFTRegional.api.callbacks.AcceptShareBusinessCardCallBack;
import com.swift.SWIFTRegional.api.callbacks.ChangePasswordCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteDelegatesCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteExhibitionSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteSessionsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetPasswordSaltCallback;
import com.swift.SWIFTRegional.api.callbacks.GetPrivacySettingsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetPrivateSessionsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteDelegateCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteExhibitorSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetPrivacySettingsCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSpeakerCallback;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.android.MainThreadExecutor;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class ApiManager
{
    private static ApiManager instance;

    private Context context;
    private ApiService apiService;
    private Executor mainThreadExecutor;

    private ApiManager(final Context inContext,boolean external)
    {
        super();

        if (this.mainThreadExecutor == null)
        {
            this.mainThreadExecutor = new MainThreadExecutor();
        }

        // keep a reference to the application context
        this.context = inContext.getApplicationContext();

        final GsonConverter gsonConverter = new GsonConverter(new Gson());

        final OkHttpClient okHttpClient = new OkHttpClient();
        //final OkHttpClient okHttpClient = MySSLTrust.getOKHttpClient(inContext);

        // Increase timeout to allow testing with the sometimes very slow test API
        if (BuildConfig.FLAVOR.equals("staging"))
        {
            okHttpClient.setConnectTimeout(160, TimeUnit.SECONDS);
            okHttpClient.setReadTimeout(160, TimeUnit.SECONDS);
            okHttpClient.setWriteTimeout(160, TimeUnit.SECONDS);
            okHttpClient.networkInterceptors().add(new StethoInterceptor());
        }

        final TokenExpiredInterceptor tokenExpiredInterceptor = new TokenExpiredInterceptor(this.context);
        okHttpClient.interceptors().add(tokenExpiredInterceptor);

        final OkClient okClient = new OkClient(okHttpClient);


        String baseURL = inContext.getResources().getString(R.string.sibos_base_url);

        if(external)
        {
           baseURL=inContext.getResources().getString(R.string.sibos_base_url);
        }

        // create REST adapter

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(baseURL)
                .setConverter(gsonConverter)
                .setClient(okClient)
                        // All protected API calls are done in series to make sure the access/refresh token refresh
                        // has been finished before starting the next call (which then uses the new token).
                .setExecutors(Executors.newSingleThreadExecutor(), this.mainThreadExecutor)
                .build();

        // create API service
        this.apiService = restAdapter.create(ApiService.class);
    }

    public static ApiManager getInstance(final Context inContext,boolean external)
    {
        if (instance == null)
        {
            instance = new ApiManager(inContext,external);
        }

        return instance;
    }

    public static ApiManager getInstance(final Context inContext)
    {
        if (instance == null)
        {
            instance = new ApiManager(inContext,false);
        }

        return instance;
    }

    public void getRegionInfos(GetRegionInfoCallBack callBack)
    {
        this.apiService.getRegionInfos(callBack);
    }



    public void getExhibitorEvents(GetExhibitorEventsCallback callback)
    {
        // execute http request
        this.apiService.getExhibitorEvents(callback);
    }

    public void getFavouriteDelegates()
    {
        this.apiService.getFavouriteDelegates(new GetFavouriteDelegatesCallback(this.context));
    }

    public void getFavouriteSessions()
    {
        this.apiService.getFavouriteSessions(new GetFavouriteSessionsCallback(this.context));
    }

    public void getFavouriteExhibitionSession(){
        this.apiService.getFavouriteExhibitionSessions(new GetFavouriteExhibitionSessionCallback(this.context));
    }

    public void getFavouriteSpeakers()
    {
        this.apiService.getFavouriteSpeakers(new GetFavouriteSpeakersCallback(this.context));
    }

    public void getFavouriteExhibitors()
    {
        this.apiService.getFavouriteExhibitors(new GetFavouriteExhibitorsCallback(this.context));
    }

    public void setFavouriteDelegate(SetFavouriteDelegateCallback callback)
    {
        TypedInput requestBody = constructSetFavouriteBody(callback, "sibosKey");
        this.apiService.setFavouriteDelegate(requestBody, callback.isUnflag(), callback);
    }

    public void setFavouriteSession(SetFavouriteSessionCallback callback)
    {
        TypedInput requestBody = constructSetFavouriteBody(callback, "sessionId");
        this.apiService.setFavouriteSession(requestBody, callback.isUnflag(), callback);
    }

    public void setFavouriteExhibitorSession(SetFavouriteExhibitorSessionCallback callback)
    {
        TypedInput requestBody = constructSetFavouriteBody(callback, "eventId");
        this.apiService.setFavouriteExhibitionSession(requestBody, callback.isUnflag(), callback);
    }

    public void setFavouriteExhibitor(SetFavouriteExhibitorCallback callback)
    {
        TypedInput requestBody = constructSetFavouriteBody(callback, "ccvk");
        this.apiService.setFavouriteExhibitor(requestBody, callback.isUnflag(), callback);
    }

    public void setFavouriteSpeaker(SetFavouriteSpeakerCallback callback)
    {
        TypedInput requestBody = constructSetFavouriteBody(callback, "speakerId");
        this.apiService.setFavouriteSpeaker(requestBody, callback.isUnflag(), callback);
    }

    public void getRatingSpeaker(GetRatingSpeakerCallback callback)
    {
        this.apiService.getRatingSpeaker(callback.getExternalId(), callback);
    }

    public void getRatingSession(GetRatingSessionCallback callback)
    {
        this.apiService.getRatingSession(callback.getExternalId(), callback);
    }

    public void setRatingSpeaker(SetRatingSpeakerCallback callback)
    {
        TypedInput requestBody = constructSetRatingBody(callback.getRating());
        this.apiService.setRatingSpeaker(requestBody, callback.getExternalId(), callback);
    }

    public void setRatingSession(SetRatingSessionCallback callback)
    {
        TypedInput requestBody = constructSetRatingBody(callback.getRating());
        this.apiService.setRatingSession(requestBody, callback.getExternalId(), callback);
    }

    public void changePassword(String oldPassword, String newPassword, ChangePasswordCallback callback)
    {
        HashMap<String, String> body = new HashMap<>();
        body.put("oldPassword", oldPassword);
        body.put("password", newPassword);

        this.apiService.changePassword(body, callback);
    }

    public void getRegistrationCode(GetRegistrationCodeCallback callback)
    {
        this.apiService.getRegistrationCode(callback);
    }

    public void getPrivacySettings(GetPrivacySettingsCallback callback)
    {
        this.apiService.getPrivacySettings(callback);
    }

    public void setPrivacySettings(PrivacySettings settings, SetPrivacySettingsCallback callback)
    {
        callback.setPrivacySettings(settings);

        HashMap<String, Boolean> body = new HashMap<>();
        body.put("shareCompanyAddress", settings.isCompanyAddress());
        body.put("shareEmail", settings.isEmail());
        body.put("shareMobilePhone", settings.isMobilePhone());
        body.put("shareCompanyPhone", settings.isCompanyPhone());

        this.apiService.setPrivacySettings(body, callback);
    }

    private TypedInput constructSetFavouriteBody(AbstractSetFavouriteCallback callback, String key)
    {
        String json = "[{\"" + key + "\":\"" + callback.getExternalId() + "\"}]";
        TypedInput requestBody = new TypedByteArray("application/json", json.getBytes(Charset.forName("UTF-8")));

        return requestBody;
    }

    private TypedInput constructSetRatingBody(int rating)
    {
        String json = "{\"rating\":\"" + rating + "\"}";
        TypedInput requestBody = new TypedByteArray("application/json", json.getBytes(Charset.forName("UTF-8")));

        return requestBody;
    }

    /*
     * Business card calls
     */

    public void getPasswordSalt(GetPasswordSaltCallback callback)
    {
        this.apiService.getPasswordSalt(callback);
    }

    public void initShareBusinessCard(Callback<BusinessCardCode> callback)
    {
        this.apiService.initShareBusinessCard(callback);
    }

    public void pollShareBusinessCard(PollShareBusinessCardCallBack callback)
    {
        this.apiService.pollShareBusinessCard(callback.getCode(), callback);
    }

    public void acceptShareBusinessCard(AcceptShareBusinessCardCallBack callback)
    {
        TypedInput requestBody = constructAcceptShareBusinessCard(callback.getCode());

        this.apiService.acceptShareBusinessCard(requestBody, callback);
    }

    private TypedInput constructAcceptShareBusinessCard(String code)
    {
        String json = "{\"shareBusinessCardCode\":\"" + code + "\"}";

        return new TypedByteArray("application/json", json.getBytes(Charset.forName("UTF-8")));
    }

    public void getPrivateSessions(GetPrivateSessionsCallback callback)
    {
        this.apiService.getPrivateSessions(callback);
    }

    public void validateToken(HttpResponseCallback callback)
    {
        this.apiService.validateToken(callback);
    }

    public void deAuthenticate(HttpResponseCallback callback)
    {
        this.apiService.deAuthenticate(callback);
    }

}
