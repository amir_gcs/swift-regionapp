package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import retrofit.RetrofitError;

public abstract class AbstractSibosCallback<S, T> extends BaseCallback<S, T>
{
    protected final Context context;

    public AbstractSibosCallback(Context context)
    {
        this.context = context;
    }

    @Override
    public void onFailure(RetrofitError retrofitError)
    {

    }

}
