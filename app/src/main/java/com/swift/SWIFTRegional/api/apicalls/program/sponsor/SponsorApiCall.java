package com.swift.SWIFTRegional.api.apicalls.program.sponsor;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.contentproviders.SponsorContentProvider;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import java.util.ArrayList;

/**
 * Created by Inneke on 3/04/14.
 */
public class SponsorApiCall extends AbstractPrivateSibosApiCall
{
    public static final String HEADER_DATA_TIMESTAMP = "DATA_TIMESTAMP";

    private String path;

    private String lastModified;

    private boolean partialUpdateMode; // Set to true if a lastModified timestamp was supplied - the API will only return the items that were changed since that time

    public SponsorApiCall(Bundle inParams)
    {
        super(inParams);

        if (inParams.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inParams.getString(HEADER_DATA_TIMESTAMP)))
        {
            this.partialUpdateMode = true;
            this.lastModified = inParams.getString(HEADER_DATA_TIMESTAMP);
        }
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.baseUrl=inContext.getString(R.string.sibos_base_url);

        if(MainActivity.selectedEventProvider.isEventSelected())
        {
            long eventId= MainActivity.selectedEventProvider.getSelectedEvent().getEventId();
            this.path = inContext.getString(R.string.sibos_path_sponsor).replace("[eventid]", Long.toString(eventId));
        }


    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);
        if (!TextUtils.isEmpty(this.lastModified))
        {
            inBundle.putString("time_stamp", this.lastModified);
        }
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        if (inHeaders.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inHeaders.getString(HEADER_DATA_TIMESTAMP)))
        {
            UpdatePreferencesHelper.saveApiHeaderTimeStamp(
                    inContext,
                    UpdatePreferencesHelper.LastModifiedType.SPONSORS,
                    inHeaders.getString(HEADER_DATA_TIMESTAMP));
        }

        final Gson gson = new Gson();
        Sponsor[] sponsors = gson.fromJson(inJsonReader, Sponsor[].class);

        final ContentValues[] contentValues = new ContentValues[sponsors.length];

        for (int i = 0; i < sponsors.length; i++)
        {
            sponsors[i].setSponsorId(i);
            contentValues[i] = sponsors[i].getContentValues();
        }

        if (!this.partialUpdateMode)
        {
            // Delete everything - insert everything
            inContext.getContentResolver().delete(SponsorContentProvider.CONTENT_URI, null, null);
            inContext.getContentResolver().bulkInsert(SponsorContentProvider.CONTENT_URI, contentValues);
        }
        else
        {
            // When receiving a list of updated content, only delete and insert those sponsors.

            // Delete
            ArrayList<ContentProviderOperation> operations = new ArrayList<>();
            ContentProviderOperation operation;
            for (Sponsor item : sponsors)
            {

                operation = ContentProviderOperation
                        .newDelete(Uri.withAppendedPath(SponsorContentProvider.CONTENT_URI_SPONSOR, Long.toString(item.getSponsorId()))).build();

                operations.add(operation);
            }
            try
            {
                inContext.getContentResolver().applyBatch(SponsorContentProvider.PROVIDER_NAME, operations);
            }
            catch (RemoteException e)
            {
                e.printStackTrace();
            }
            catch (OperationApplicationException e)
            {
                e.printStackTrace();
            }

        }

        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SPONSOR, true);

        if (sponsors.length > 0){
            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SPONSOR, true);
        } else {
            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SPONSOR, false);
        }

        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.SPONSORS);
    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SPONSOR, true);
    }
}


