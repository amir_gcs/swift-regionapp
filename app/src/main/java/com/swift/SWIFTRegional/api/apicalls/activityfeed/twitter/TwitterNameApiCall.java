package com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.contentproviders.TwitterContentProvider;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterHashtagFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterNameFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.VideoFragment;
import com.swift.SWIFTRegional.models.WallItem;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 12/03/14.
 */
public class TwitterNameApiCall extends AbstractTwitterApiCall
{
    public static final String PARAM_SEARCH_NAME = "screen_name";

    private String searchName;
    private String uriPath;




    public TwitterNameApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public String getUriPath()
    {
        return this.uriPath;
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        if(MainActivity.selectedEventProvider.isEventSelected() && ModulePreferenceHelper.getModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_TWITTER))
        {
            long eventId= MainActivity.selectedEventProvider.getSelectedEvent().getEventId();
            this.uriPath = inContext.getString(R.string.twitter_path_timeline).replace("[eventid]", Long.toString(eventId));

            if(TwitterNameFragment.nextToken==null)
            {
                TwitterNameFragment.nextToken="";
            }

            if(!TwitterNameFragment.nextToken.isEmpty())
            {
                uriPath += TwitterNameFragment.nextToken;
            }
        }
        this.searchName = inContext.getString(R.string.twitter_search_name);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        //super.getRequestParams(inBundle);

        //inBundle.putString(PARAM_SEARCH_NAME, this.searchName);
    }

    @Override
    public Uri getContentUri()
    {
        return Uri.withAppendedPath(TwitterContentProvider.CONTENT_URI_TYPE, String.valueOf(WallItem.Type.TWITTER_NAME.ordinal()));
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        inData.putString("Api","TwitterName");

        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.TWITTER_NAME);

        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_TWITTER, true);

        if(ModulePreferenceHelper.getModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_INFO_TWITTER)){
            super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);


//                        if (videos.length > 0){
            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER, true);
        }
//        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);
//
//        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.TWITTER_NAME);
//
//        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_TWITTER, true);
//
////                        if (videos.length > 0){
//        ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER, true);
//                        } else {
//                            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER, false);
//                        }
    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        if(inStatusCode==404)
        {
            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_TWITTER, true);
            TwitterNameFragment.setNextToken("CantLoad");
        }
    }

}
