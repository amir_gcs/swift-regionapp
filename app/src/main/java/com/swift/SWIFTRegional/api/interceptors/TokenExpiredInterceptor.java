package com.swift.SWIFTRegional.api.interceptors;

import android.content.Context;

import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import com.goodcoresoftware.android.common.utils.Base64;
import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.application.SibosApplication;
import com.swift.SWIFTRegional.models.ApiErrorCode;
import com.swift.SWIFTRegional.models.RefreshTokenResult;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

public class TokenExpiredInterceptor implements Interceptor
{
    private final Context context;
    private final Gson gson;

    public TokenExpiredInterceptor(Context context)
    {
        super();
        this.context = context.getApplicationContext();
        this.gson = new Gson();
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        // original request
        Request originalRequest = chain.request();

        // Add the authorization token to every request to a protected endpoint.
        originalRequest = originalRequest.newBuilder()
                .addHeader("Authorization", "Bearer " + UserPreferencesHelper.getAccessToken(this.context))
                .addHeader("Accept", "application/json;charset=UTF-8")
                .build();


        Response response;

        // execute original request
        response = chain.proceed(originalRequest);

        // Unauthorized request
        if (response != null && response.code() == 403 && UserPreferencesHelper.loggedIn(this.context))
        {
            final ApiErrorCode errorCode = new Gson().fromJson(response.body().string(), ApiErrorCode.class);

            // Try to refresh token
            if ("003".equals(errorCode.getCode()) || "002".equals(errorCode.getCode())) // 002 or 003 = "Expired token"
            {
                // get new token
                final String newToken = this.refreshToken(chain);

                // execute original request with new token
                response = this.executeOriginalRequestWithNewToken(chain, newToken);
            }
        }

        return response;
    }

    private Response executeOriginalRequestWithNewToken(Chain chain, String newAccessToken)
    {
        // build request
        final Request request = chain
                .request()
                .newBuilder()
                .header("Authorization", "Bearer " + newAccessToken)
                .build();

        Response response = null;

        try
        {
            response = chain.proceed(request);
        }
        catch (Exception ex)
        {
            Log.e(TokenExpiredInterceptor.class.getSimpleName(), ex.getMessage(), ex);
        }

        return response;
    }

    private String refreshToken(Chain chain)
    {
        final String baseURL = this.context.getResources().getString(R.string.sibos_base_url);
        final String refreshToken = UserPreferencesHelper.getRefreshToken(this.context);

        // make request body
        final FormEncodingBuilder formBuilder = new FormEncodingBuilder()
                .add("refresh_token", refreshToken)
                .add("grant_type", "refresh_token");

        // build request
        final Request request = new Request.Builder()
                .url(baseURL + "/sibosgateway/v1/login/token")
                .addHeader("Authorization", getTokenAuthorizationHeader())
                .post(formBuilder.build())
                .build();

        try
        {
            // execute request
            final Response response = chain.proceed(request);
            if (response != null)
            {
                if (response.code() == 200)
                {
                    final RefreshTokenResult result = this.parseLoginResult(response);

                    if (result != null)
                    {
                        final String newAccessToken = result.getAccessToken();
                        final String newRefreshToken = result.getRefreshToken();

                        UserPreferencesHelper.storeTokens(this.context,
                                newAccessToken,
                                newRefreshToken,
                                UserPreferencesHelper.getClientId(this.context),
                                UserPreferencesHelper.getClientSecret(this.context));

                        return newAccessToken;
                    }
                }
                else if (response.code() == 400)
                {
                    // Refresh token was not valid, send the user back to the login to request a new one.
                    ((SibosApplication) this.context.getApplicationContext()).goBackToLogin();
                }
            }
        }
        catch (Exception ex)
        {
            Log.e(TokenExpiredInterceptor.class.getSimpleName(), ex.getMessage(), ex);
        }

        return "";
    }

    private RefreshTokenResult parseLoginResult(final Response response)
    {
        RefreshTokenResult result = null;

        try
        {
            result = this.gson.fromJson(response.body().charStream(), RefreshTokenResult.class);
        }
        catch (Exception ex)
        {
            Log.e(TokenExpiredInterceptor.class.getSimpleName(), ex.getMessage(), ex);
        }

        return result;
    }

    private String getTokenAuthorizationHeader()
    {
        StringBuilder sb = new StringBuilder();

        String credentials = sb
                .append(UserPreferencesHelper.getClientId(this.context))
                .append(':')
                .append(UserPreferencesHelper.getClientSecret(this.context))
                .toString();

        byte[] arrayBase64 = Base64.encode(credentials.getBytes(), Base64.DEFAULT);
        sb = new StringBuilder();
        for (byte c : arrayBase64)
        {
            if (!Character.isWhitespace(c))
            {
                sb.append((char) c);
            }
        }
        String base64 = sb.toString();
        if (base64.endsWith("\n"))
        {
            base64 = base64.substring(0, base64.length() - 1);
        }

        return "Basic " + base64;
    }
}

