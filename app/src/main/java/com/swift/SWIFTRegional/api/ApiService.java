package com.swift.SWIFTRegional.api;

import com.swift.SWIFTRegional.api.callbacks.ChangePasswordCallback;
import com.swift.SWIFTRegional.api.callbacks.GetExhibitorEventsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteExhibitorsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteSpeakersCallback;
import com.swift.SWIFTRegional.api.callbacks.GetPrivateSessionsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.GetRegionInfoCallBack;
import com.swift.SWIFTRegional.api.callbacks.GetRegistrationCodeCallback;
import com.swift.SWIFTRegional.api.callbacks.HttpResponseCallback;
import com.swift.SWIFTRegional.api.callbacks.PollShareBusinessCardCallBack;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteExhibitorCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteExhibitorSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSpeakerCallback;
import com.swift.SWIFTRegional.models.businessshare.BusinessCardCode;

import java.util.HashMap;

import com.swift.SWIFTRegional.api.callbacks.AcceptShareBusinessCardCallBack;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteDelegatesCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteExhibitionSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.GetFavouriteSessionsCallback;
import com.swift.SWIFTRegional.api.callbacks.GetPasswordSaltCallback;
import com.swift.SWIFTRegional.api.callbacks.GetPrivacySettingsCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteDelegateCallback;
import com.swift.SWIFTRegional.api.callbacks.SetPrivacySettingsCallback;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedInput;

public interface ApiService
{
    //Region and Types
    @GET("/events_categories.json")
    void getRegionInfos(GetRegionInfoCallBack callback);

    // Sessions

    @GET("/sibosgateway/v1/protected/events/exhibitors")
    void getExhibitorEvents(GetExhibitorEventsCallback callback);

    @GET("/sibosgateway/v1/protected/sessions")
    void getPrivateSessions(GetPrivateSessionsCallback callback);

    // Favourites

    @GET("/sibosgateway/v1/protected/favourites/delegates")
    void getFavouriteDelegates(GetFavouriteDelegatesCallback callback);

    @GET("/sibosgateway/v1/protected/favourites/sessions")
    void getFavouriteSessions(GetFavouriteSessionsCallback callback);

    @GET("/sibosgateway/v1/protected/favourites/exhibitorEvents")
    void getFavouriteExhibitionSessions(GetFavouriteExhibitionSessionCallback callback);

    @GET("/sibosgateway/v1/protected/favourites/speakers")
    void getFavouriteSpeakers(GetFavouriteSpeakersCallback callback);

    @GET("/sibosgateway/v1/protected/favourites/exhibitors")
    void getFavouriteExhibitors(GetFavouriteExhibitorsCallback callback);

    @POST("/sibosgateway/v1/protected/favourites/delegates")
    void setFavouriteDelegate(@Body TypedInput sibosKey, @Query("unflag") boolean unflag, SetFavouriteDelegateCallback callback);

    @POST("/sibosgateway/v1/protected/favourites/sessions")
    void setFavouriteSession(@Body TypedInput sessionId, @Query("unflag") boolean unflag, SetFavouriteSessionCallback callback);

    @POST("/sibosgateway/v1/protected/favourites/exhibitorEvents")
    void setFavouriteExhibitionSession(@Body TypedInput eventId, @Query("unflag") boolean unflag, SetFavouriteExhibitorSessionCallback callback);

    @POST("/sibosgateway/v1/protected/favourites/exhibitors")
    void setFavouriteExhibitor(@Body TypedInput ccvk, @Query("unflag") boolean unflag, SetFavouriteExhibitorCallback callback);

    @POST("/sibosgateway/v1/protected/favourites/speakers")
    void setFavouriteSpeaker(@Body TypedInput speakerId, @Query("unflag") boolean unflag, SetFavouriteSpeakerCallback callback);

    // Ratings

    @GET("/sibosgateway/v1/protected/ratings/speakers/{speakerId}")
    void getRatingSpeaker(@Path("speakerId") String speakerId, GetRatingSpeakerCallback callback);

    @GET("/sibosgateway/v1/protected/ratings/sessions/{sessionId}")
    void getRatingSession(@Path("sessionId") String sessionId, GetRatingSessionCallback callback);

    @POST("/sibosgateway/v1/protected/ratings/speakers/{speakerId}")
    void setRatingSpeaker(@Body TypedInput rating, @Path("speakerId") String speakerId, SetRatingSpeakerCallback callback);

    @POST("/sibosgateway/v1/protected/ratings/sessions/{sessionId}")
    void setRatingSession(@Body TypedInput rating, @Path("sessionId") String sessionId, SetRatingSessionCallback callback);

    // Account

    @POST("/sibosgateway/v1/protected/changePassword")
    void changePassword(@Body HashMap<String, String> body, ChangePasswordCallback callback);

    @GET("/sibosgateway/v1/protected/registration/qrcode")
    void getRegistrationCode(GetRegistrationCodeCallback callback);

    @GET("/sibosgateway/v1/protected/registration/privacy")
    void getPrivacySettings(GetPrivacySettingsCallback callback);

    @POST("/sibosgateway/v1/protected/registration/privacy")
    void setPrivacySettings(@Body HashMap<String, Boolean> body, SetPrivacySettingsCallback callback);

    @GET("/sibosgateway/v1/protected/saltPassword")
    void getPasswordSalt(GetPasswordSaltCallback callback);

    @GET("/sibosgateway/v1/login/checkToken")
    void validateToken(HttpResponseCallback callback);

    @POST("/sibosgateway/v1/protected/deauthenticate")
    void deAuthenticate(HttpResponseCallback callback);

    // Business cards

    @GET("/sibosgateway/v1/protected/delegates/sharedBusinessCard")
    void initShareBusinessCard(Callback<BusinessCardCode> callback);

    @GET("/sibosgateway/v1/protected/delegates/sharedBusinessCardStatus/{uniqueKey}")
    void pollShareBusinessCard(@Path("uniqueKey") String sessionId, PollShareBusinessCardCallBack callback);

    @POST("/sibosgateway/v1/protected/delegates/sharedBusinessCard")
    void acceptShareBusinessCard(@Body TypedInput body, AcceptShareBusinessCardCallBack callback);

}
