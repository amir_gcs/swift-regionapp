package com.swift.SWIFTRegional.api.apicalls;

import android.content.Context;
import android.os.Bundle;

import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import java.util.HashMap;

import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 25/03/15.
 * Executes an API call and supplies the user's access token as a header parameter.
 */
public abstract class AbstractPrivateTokenApiCall extends AbstractPrivateSibosApiCall
{
    private String accessToken;

    public AbstractPrivateTokenApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);
        this.accessToken = UserPreferencesHelper.getAccessToken(inContext);
    }

    @Override
    public void getRequestHeaders(HashMap<String, String> inHeaders)
    {
        super.getRequestHeaders(inHeaders);

        inHeaders.put("Authorization", "Bearer " + this.accessToken);
    }

    @Override
    public String getUri()
    {
        return this.baseUrl + this.getUriPath();
    }
}
