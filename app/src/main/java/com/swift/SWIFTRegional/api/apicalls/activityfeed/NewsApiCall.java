package com.swift.SWIFTRegional.api.apicalls.activityfeed;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.contentproviders.NewsContentProvider;
import com.swift.SWIFTRegional.models.News;

import java.io.IOException;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Yves on 22/04/2014.
 */
public class NewsApiCall extends AbstractPrivateSibosApiCall
{
    private String path;

    public NewsApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.path = inContext.getString(R.string.sibos_path_news);
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try
        {
            if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
            {
                inJsonReader.beginObject();

                if (inJsonReader.peek() == JsonToken.NAME)
                {
                    final String head = inJsonReader.nextName();
                    if (head.equals("news"))
                    {
                        if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
                        {
                            inJsonReader.beginObject();

                            if (inJsonReader.peek() == JsonToken.NAME)
                            {
                                final String arrayHead = inJsonReader.nextName();
                                if (arrayHead.equals("item"))
                                {
                                    final Gson gson = new Gson();
                                    News[] news = gson.fromJson(inJsonReader, News[].class);

                                    final ContentValues[] contentValues = new ContentValues[news.length];

                                    for (int i = 0; i < news.length; i++)
                                    {
                                        contentValues[i] = news[i].getContentValues();
                                    }

                                    inContext.getContentResolver().delete(NewsContentProvider.CONTENT_URI, null, null);
                                    inContext.getContentResolver().bulkInsert(NewsContentProvider.CONTENT_URI, contentValues);

                                    UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.NEWS);

                                    return;
                                }
                                else
                                {
                                    inJsonReader.skipValue();
                                }
                            }

                            inJsonReader.endObject();
                        }
                    }
                    else
                    {
                        inJsonReader.skipValue();
                    }
                }

                inJsonReader.endObject();
            }
        }
        catch (IOException e)
        {
            Log.d(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}
