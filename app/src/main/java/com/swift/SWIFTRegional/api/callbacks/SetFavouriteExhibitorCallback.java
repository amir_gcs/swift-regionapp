package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;

public abstract class SetFavouriteExhibitorCallback extends AbstractSetFavouriteCallback
{
    public SetFavouriteExhibitorCallback(Context context, String externalId, boolean unflag)
    {
        super(context, externalId, unflag);
    }

    @Override
    protected FavoriteTable.FavouriteType getType()
    {
        return FavoriteTable.FavouriteType.EXHIBITOR;
    }
}
