package com.swift.SWIFTRegional.api.apicalls.program.exhibitor;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateTokenApiCall;
import com.swift.SWIFTRegional.contentproviders.ExhibitorCollateralContentProvider;
import com.swift.SWIFTRegional.database.tables.ExhibitorCollateralTable;
import com.swift.SWIFTRegional.models.exhibitor.ExhibitorCollateral;

/**
 * Created by Inneke on 3/04/14.
 * Download a list of collaterals for a ccvk number (exhibitor number).
 */
public class ExhibitorCollateralApiCall extends AbstractPrivateTokenApiCall
{
    public static final String PARAM_EXHIBITORCOLLATORAL = "collaterals";

    private String path;
    private String ccvk;

    public ExhibitorCollateralApiCall(Bundle inParams)
    {
        super(inParams);
        this.ccvk = inParams.getString(PARAM_EXHIBITORCOLLATORAL);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);
        this.path = inContext.getString(R.string.sibos_path_exhibitor_collateral);
    }

    @Override
    public String getUriPath()
    {
        return this.path.replace("{CCVK}", this.ccvk);
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        final List<ContentValues> contentValues = new ArrayList<ContentValues>();
        List<ExhibitorCollateral> collaterals = new ArrayList<ExhibitorCollateral>();

        try
        {
            ExhibitorCollateral[] exhibitorsCollateral = new Gson().fromJson(inJsonReader, ExhibitorCollateral[].class);
            collaterals = Arrays.asList(exhibitorsCollateral);
        }
        catch (Exception e)
        {
            Log.d("EXHIBITORCOLLATERAL API CALL", "Exception");
        }

        for (ExhibitorCollateral ec : collaterals)
        {
            ec.setCcvk(this.ccvk);
            contentValues.add(ec.getContentValues());
        }

        inContext.getContentResolver().delete(ExhibitorCollateralContentProvider.CONTENT_URI, ExhibitorCollateralTable.COLUMN_CCVK + "='" + this.ccvk + "'", null);
        inContext.getContentResolver().bulkInsert(ExhibitorCollateralContentProvider.CONTENT_URI, contentValues.toArray(new ContentValues[contentValues.size()]));
    }

}