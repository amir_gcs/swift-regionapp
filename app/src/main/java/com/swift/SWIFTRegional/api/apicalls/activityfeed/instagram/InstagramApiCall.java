package com.swift.SWIFTRegional.api.apicalls.activityfeed.instagram;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.contentproviders.InstagramContentProvider;

import java.io.IOException;

import com.goodcoresoftware.android.common.http.api.JsonApiCall;
import com.goodcoresoftware.android.common.utils.Log;

import com.swift.SWIFTRegional.models.instagram.Instagram;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by wesley on 24/06/14.
 */
public class InstagramApiCall extends JsonApiCall
{
    private static final String CLIENT_ID = "16645d898aae43ba8d0bc61f2664bc2c";
    private static final String SIBOS_USER_ID = "1156155592";

    public InstagramApiCall(Bundle inBundle)
    {
        super(inBundle);
    }

    @Override
    public String getUri() {
        return "https://api.instagram.com/v1/users/" + SIBOS_USER_ID + "/media/recent";
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        inBundle.putString("client_id", CLIENT_ID);
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatusCode, inHeaders, inJsonReader);

        try
        {
            if(inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
            {
                inJsonReader.beginObject();

                while (inJsonReader.peek() == JsonToken.NAME)
                {
                    final String name = inJsonReader.nextName();

                    if(name.equals("data"))
                    {
                        final Gson gson = new Gson();

                        final Instagram[] instagrams = gson.fromJson(inJsonReader, Instagram[].class);

                        final ContentValues[] contentValues = new ContentValues[instagrams.length];

                        for(int i = 0; i < instagrams.length; i++)
                        {
                            contentValues[i] = instagrams[i].getContentValues();
                        }

                        inContext.getContentResolver().delete(InstagramContentProvider.CONTENT_URI, null, null);
                        inContext.getContentResolver().bulkInsert(InstagramContentProvider.CONTENT_URI, contentValues);

                        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.INSTAGRAM);

                        return;
                    }else
                    {
                        inJsonReader.skipValue();
                    }
                }

                inJsonReader.endObject();
            }
        }catch(IOException e)
        {
            Log.e(InstagramApiCall.class.getSimpleName(), e.getLocalizedMessage());
        }
    }
}
