package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;

public class GetFavouriteSpeakersCallback extends AbstractGetFavouritesCallback
{
    public GetFavouriteSpeakersCallback(Context context)
    {
        super(context);
    }

    @Override
    protected FavoriteTable.FavouriteType getType()
    {
        return FavoriteTable.FavouriteType.SPEAKER;
    }
}
