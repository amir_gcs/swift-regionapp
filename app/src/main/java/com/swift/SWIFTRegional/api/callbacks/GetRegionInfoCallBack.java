package com.swift.SWIFTRegional.api.callbacks;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.contentproviders.RegionInfoContentProvider;
import com.swift.SWIFTRegional.contentproviders.TypeInfoContentProvider;
import com.swift.SWIFTRegional.models.event.RegionType;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import retrofit.client.Response;

public abstract class GetRegionInfoCallBack extends AbstractSibosCallback<Void, RegionType>
{

    public GetRegionInfoCallBack(Context context)
    {
        super(context);
    }

    @Override
    public Void doInBackground(RegionType regionType, Response response)
    {
        try
        {
            final ContentValues[] typeContentValues = new ContentValues[regionType.types.length];
            final ContentValues[] regionContentValues = new ContentValues[regionType.regions.length];

            for (int i = 0; i < regionType.types.length; i++) {
                regionType.types[i].setTypeId(Long.parseLong(regionType.types[i].getId()));
                typeContentValues[i] = regionType.types[i].getContentValues();
            }

            for (int i = 0; i < regionType.regions.length; i++) {
                regionType.regions[i].setRegionId(Long.parseLong(regionType.regions[i].getId()));
                regionContentValues[i] = regionType.regions[i].getContentValues();
            }

            // Delete everything - insert everything
            //delete only if there is a existant content provider
            try {
                this.context.getContentResolver().delete(RegionInfoContentProvider.CONTENT_URI, null, null);
                this.context.getContentResolver().delete(TypeInfoContentProvider.CONTENT_URI, null, null);
            } catch (Exception e) {

            }


            this.context.getContentResolver().bulkInsert(RegionInfoContentProvider.CONTENT_URI, regionContentValues);
            this.context.getContentResolver().bulkInsert(TypeInfoContentProvider.CONTENT_URI, typeContentValues);

            UpdatePreferencesHelper.saveUpdateTimestamp(this.context, UpdatePreferencesHelper.LastUpdateType.REGIONINFO);

        }catch(Exception e)
        {

        }

        return null;
    }
}
