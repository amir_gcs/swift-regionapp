package com.swift.SWIFTRegional.api.apiservice;

import android.content.Context;
import android.content.Intent;

import com.goodcoresoftware.android.common.http.api.utils.ApiMessenger;

/**
 * Created by wesley on 07/07/14.
 */
public class SibosApiMessenger extends ApiMessenger
{
    private Context context;

    public SibosApiMessenger(Context inContext)
    {
        super(inContext);

        this.context = inContext;
    }

    public SibosApiMessenger(Context inContext, ApiMessengerListener inApiMessengerListener)
    {
        super(inContext, inApiMessengerListener);

        this.context = inContext;
    }

    @Override
    public void create()
    {
        this.create(new Intent(this.context, SibosApiService.class));
    }
}
