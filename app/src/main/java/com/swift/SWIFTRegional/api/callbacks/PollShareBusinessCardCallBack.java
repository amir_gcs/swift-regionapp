package com.swift.SWIFTRegional.api.callbacks;

import com.swift.SWIFTRegional.models.businessshare.BusinessCardStatus;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by SimonRaes on 30/04/15.
 */
public class PollShareBusinessCardCallBack implements Callback<BusinessCardStatus>
{
    String code;

    public PollShareBusinessCardCallBack(String code)
    {
        this.code = code;
    }

    @Override
    public void success(BusinessCardStatus businessCardStatus, Response response)
    {

    }

    @Override
    public void failure(RetrofitError error)
    {

    }


    public String getCode()
    {
        return this.code;
    }
}
