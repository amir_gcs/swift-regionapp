package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.models.messaging.Salt;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import retrofit.client.Response;

public class GetPasswordSaltCallback extends AbstractSibosCallback<Void, Salt>
{
    public GetPasswordSaltCallback(Context context)
    {
        super(context);
    }

    @Override
    public void postResult(Void result) {
        super.postResult(result);
    }

    @Override
    public Void doInBackground(Salt result, Response response)
    {
        UserPreferencesHelper.storeSalt(this.context, result.getSalt());
        return null;
    }
}
