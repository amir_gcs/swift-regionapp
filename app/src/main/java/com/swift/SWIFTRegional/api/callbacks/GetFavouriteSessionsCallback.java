package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.utils.AlarmHelper;

public class GetFavouriteSessionsCallback extends AbstractGetFavouritesCallback
{
    public GetFavouriteSessionsCallback(Context context)
    {
        super(context);
    }

    @Override
    protected FavoriteTable.FavouriteType getType()
    {
        return FavoriteTable.FavouriteType.SESSION;
    }

    @Override
    protected void beforeLocalUpdate()
    {
        super.beforeLocalUpdate();
        // Need to cancel all session notifications here while we still have to them stored locally.
        // Only happens if the API call actually succeeded (so we don't erase favourites and then fail the download of new ones, leaving zero notifications).
        AlarmHelper.cancelAllSessionAlarms(context);
    }

    @Override
    protected void afterLocalUpdate()
    {
        super.afterLocalUpdate();
        // Set notifications for all new favourite sessions once they have been downloaded.
        AlarmHelper.setAllSessionAlarms(this.context);
    }

}
