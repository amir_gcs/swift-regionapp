package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.models.PrivacySettings;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import retrofit.client.Response;

public class GetPrivacySettingsCallback extends AbstractSibosCallback<Void, PrivacySettings>
{
    public GetPrivacySettingsCallback(Context context)
    {
        super(context);
    }

    @Override
    public Void doInBackground(PrivacySettings result, Response response)
    {
        UserPreferencesHelper.storePrivacySettings(this.context, result);

        return null;
    }
}
