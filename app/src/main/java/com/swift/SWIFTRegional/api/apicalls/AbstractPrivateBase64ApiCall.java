package com.swift.SWIFTRegional.api.apicalls;

import android.content.Context;
import android.os.Bundle;

import com.swift.SWIFTRegional.activities.BaseActivity;

import java.util.HashMap;

import com.goodcoresoftware.android.common.utils.Base64;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

/**
 * Created by Yves on 7/04/2014.
 * API call that includes a base64 hash of the user's username and password in the header. */
public abstract class AbstractPrivateBase64ApiCall extends AbstractPrivateSibosApiCall
{
    protected String username;
    protected String password;

    public AbstractPrivateBase64ApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);
        this.baseUrl = inContext.getResources().getString(R.string.sibos_base_url);
    }

    @Override
    public void getRequestHeaders(HashMap<String, String> inHeaders)
    {
        super.getRequestHeaders(inHeaders);

        StringBuilder sb = new StringBuilder();
        String credentials = sb.append(this.username).append(':').append(this.password).toString();
        byte[] arrayBase64 = Base64.encode(credentials.getBytes(), Base64.DEFAULT);
        sb = new StringBuilder();
        for (byte c : arrayBase64)
        {
            sb.append((char) c);
        }
        String base64 = sb.toString();
        if (base64.endsWith("\n"))
        {
            base64 = base64.substring(0, base64.length() - 1);
        }
        inHeaders.put("Authorization", "Basic " + base64);
    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException)
    {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        if (inStatusCode == 401 && UserPreferencesHelper.loggedIn(inContext))
        {
            UserPreferencesHelper.logout(inContext);
            inData.putBoolean(BaseActivity.PARAM_UNAUTHORIZED, true);
        }
    }
}
