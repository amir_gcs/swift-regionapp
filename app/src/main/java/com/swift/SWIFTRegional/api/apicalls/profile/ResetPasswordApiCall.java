package com.swift.SWIFTRegional.api.apicalls.profile;

import android.content.Context;
import android.os.Bundle;

import com.google.gson.JsonObject;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivatePostApiCall;

/*Send a request to reset the user's password. User will receive a new password by email that can then be used to log in again.
* Header contains no data, json post data contains the user's password. */
public class ResetPasswordApiCall extends AbstractPrivatePostApiCall
{
    public final static String PARAM_EMAIL = "paramEmail";

    private String email;
    private String path;

    public ResetPasswordApiCall(Bundle inParams)
    {
        super(inParams);
        email = inParams.getString(PARAM_EMAIL);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.path = inContext.getString(R.string.sibos_path_reset_password);
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    protected String getRawData()
    {
        if (this.email != null)
        {
            final JsonObject o = new JsonObject();
            o.addProperty("email", this.email);

            return o.toString();
        }
        return "";
    }
}
