package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.contentproviders.RateContentProvider;
import com.swift.SWIFTRegional.database.tables.RateTable;
import com.swift.SWIFTRegional.models.Rate;
import retrofit.client.Response;

public abstract class AbstractGetRateCallback extends AbstractSibosCallback<Void, Rate>
{
    private String externalId;

    public AbstractGetRateCallback(Context context, String externalId)
    {
        super(context);

        this.externalId = externalId;
    }

    @Override
    public Void doInBackground(Rate rate, Response response)
    {
        rate.setId(this.getExternalId());
        rate.setType(this.getRateType().ordinal());

        this.context.getContentResolver().insert(RateContentProvider.CONTENT_URI, rate.getContentValues());

        return null;
    }

    public String getExternalId()
    {
        return externalId;
    }

    abstract protected RateTable.RateType getRateType();

}
