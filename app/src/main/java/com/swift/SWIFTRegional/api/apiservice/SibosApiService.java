package com.swift.SWIFTRegional.api.apiservice;

import android.content.Context;

import java.io.InputStream;

import com.goodcoresoftware.android.common.http.api.services.ApiService;
import com.goodcoresoftware.android.common.http.http.AbstractHttpClient;

/**
 * Created by wesley on 04/07/14.
 */
public class SibosApiService extends ApiService
{
    @Override
    protected AbstractHttpClient<InputStream> createHttpClient(Context inContext)
    {
        return new SibosApiServiceHttpClient(inContext);
    }
}
