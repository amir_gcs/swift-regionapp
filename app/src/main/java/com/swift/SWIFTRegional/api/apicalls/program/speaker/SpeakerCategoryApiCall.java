package com.swift.SWIFTRegional.api.apicalls.program.speaker;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.api.deserializers.CategoryDeserializer;
import com.swift.SWIFTRegional.contentproviders.CategoryContentProvider;
import com.swift.SWIFTRegional.models.Category;
import com.swift.SWIFTRegional.models.ExhibitorEvent;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.goodcoresoftware.android.common.utils.Log;

/**
 * Created by Yves on 2/04/2014.
 */
public class SpeakerCategoryApiCall extends AbstractPrivateSibosApiCall
{
    private String path;

    public SpeakerCategoryApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);
        this.baseUrl = inContext.getString(R.string.sibos_base_url);
        this.path = inContext.getString(R.string.sibos_path_speaker_categories);
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try
        {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            ArrayList<Category> categoryList = new ArrayList<>();
            final Category[] categories = gson.fromJson(inJsonReader, Category[].class);

            categoryList.addAll(Arrays.asList(categories));
            for (Category cat:categoryList
                    ) {
                cat.setType("SPEAKER");

            }

                final ContentValues[] contentValues = new ContentValues[categoryList.size()];

                for (int i = 0; i < categoryList.size(); i++)
                {
                    contentValues[i] = categoryList.get(i).getContentValues();
                }


                inContext.getContentResolver().delete(CategoryContentProvider.CONTENT_URI, "categoryType=?", new String[]{"SPEAKER"});
                inContext.getContentResolver().bulkInsert(CategoryContentProvider.CONTENT_URI, contentValues);

                UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.SPEAKERCATEGORIES);


        }
        catch (Exception e)
        {
            Log.d(e.getClass().getSimpleName(), e.getMessage());
        }
    }
}
