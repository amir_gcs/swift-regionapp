package com.swift.SWIFTRegional.api.apicalls.practicalinfo;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.contentproviders.PracticalInfoContentProvider;
import com.swift.SWIFTRegional.models.event.PracticalInfo;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;

/**
 * Created by amir.naushad on 12/13/2017.
 */

public class PracticalInfoApiCall extends AbstractPrivateSibosApiCall {
    private String path;

    public PracticalInfoApiCall(Bundle inParams) {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext) {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getString(R.string.sibos_base_url);

        if (MainActivity.selectedEventProvider.isEventSelected()) {
            long eventId = MainActivity.selectedEventProvider.getSelectedEvent().getEventId();
            this.path = inContext.getString(R.string.sibos_path_practical_info).replace("[eventid]", Long.toString(eventId));
        }

    }

    @Override
    public String getUriPath() {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader) {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();

            final PracticalInfo practicalInfo = gson.fromJson(inJsonReader, PracticalInfo.class);

            // Convert practical info to content values

            ContentValues contentValues = new ContentValues();
            contentValues = practicalInfo.getContentValues();

            inContext.getContentResolver().delete(PracticalInfoContentProvider.CONTENT_URI, null, null);
            inContext.getContentResolver().insert(PracticalInfoContentProvider.CONTENT_URI, contentValues);


            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PRACTICAL_INFO, true);

            if (practicalInfo != null &&  practicalInfo.getPracticalInfoUrl() != null){
                ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PRACTICAL_INFO, true);
            } else {
                ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PRACTICAL_INFO, false);
            }

        } catch (Exception e) {

            String s = "Error check";
        }

    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        if(inStatusCode==404)
        {
            ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_PRACTICAL_INFO, true);
        }
    }


}
