package com.swift.SWIFTRegional.api.apicalls.program.exhibitor;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.util.ArrayList;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractPrivateSibosApiCall;
import com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by Inneke on 3/04/14.
 */
public class ExhibitorApiCall extends AbstractPrivateSibosApiCall
{
    public static final String HEADER_DATA_TIMESTAMP = "DATA_TIMESTAMP";

    private String path;

    private String lastModified;

    private boolean partialUpdateMode; // Set to true if a lastModified timestamp was supplied - the API will only return the items that were changed since that time

    public ExhibitorApiCall(Bundle inParams)
    {
        super(inParams);

        if (inParams.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inParams.getString(HEADER_DATA_TIMESTAMP)))
        {
            this.partialUpdateMode = true;
            this.lastModified = inParams.getString(HEADER_DATA_TIMESTAMP);
        }
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.baseUrl=inContext.getString(R.string.sibos_base_url);
        this.path = inContext.getString(R.string.sibos_path_exhibitor);
        //this.path = inContext.getString(R.string.sibos_path_exhibitor);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);
        if (!TextUtils.isEmpty(this.lastModified))
        {
            inBundle.putString("time_stamp", this.lastModified);
        }
    }

    @Override
    public String getUriPath()
    {
        return this.path;
    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        if (inHeaders.containsKey(HEADER_DATA_TIMESTAMP) && !TextUtils.isEmpty(inHeaders.getString(HEADER_DATA_TIMESTAMP)))
        {
            UpdatePreferencesHelper.saveApiHeaderTimeStamp(
                    inContext,
                    UpdatePreferencesHelper.LastModifiedType.EXHIBITORS,
                    inHeaders.getString(HEADER_DATA_TIMESTAMP));
        }

        final Gson gson = new Gson();
        Exhibitor[] exhibitors = gson.fromJson(inJsonReader, Exhibitor[].class);

        final ContentValues[] contentValues = new ContentValues[exhibitors.length];

        for (int i = 0; i < exhibitors.length; i++)
        {
            exhibitors[i].setExhibitorId(i);
            contentValues[i] = exhibitors[i].getContentValues();
        }

        if (!this.partialUpdateMode)
        {
            // Delete everything - insert everything
            inContext.getContentResolver().delete(ExhibitorContentProvider.CONTENT_URI, null, null);
            inContext.getContentResolver().bulkInsert(ExhibitorContentProvider.CONTENT_URI, contentValues);
        }
        else
        {
            // When receiving a list of updated content, only delete and insert those exhibitors.

            // Delete
            ArrayList<ContentProviderOperation> operations = new ArrayList<>();
            ContentProviderOperation operation;
            for (Exhibitor item : exhibitors)
            {

                operation = ContentProviderOperation
                        .newDelete(Uri.withAppendedPath(ExhibitorContentProvider.CONTENT_URI_EXHIBITOR, Long.toString(item.getExhibitorId()))).build();

                operations.add(operation);
            }
            try
            {
                inContext.getContentResolver().applyBatch(ExhibitorContentProvider.PROVIDER_NAME, operations);
            }
            catch (RemoteException e)
            {
                e.printStackTrace();
            }
            catch (OperationApplicationException e)
            {
                e.printStackTrace();
            }

        }

        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.EXHIBITORS);
    }
}


