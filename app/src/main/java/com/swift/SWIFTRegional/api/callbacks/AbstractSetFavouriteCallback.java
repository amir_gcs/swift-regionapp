package com.swift.SWIFTRegional.api.callbacks;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.broadcastreceivers.NetworkConnectionMonitor;
import com.swift.SWIFTRegional.contentproviders.PendingFavouriteContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.models.favourites.PendingFavourite;
import com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider;

import retrofit.RetrofitError;
import retrofit.client.Response;

public abstract class AbstractSetFavouriteCallback extends AbstractSibosCallback<Void, Void>
{
    protected final String externalId;
    protected final boolean unflag;

    public AbstractSetFavouriteCallback(Context context, String externalId, boolean unflag)
    {
        super(context);

        this.externalId = externalId;
        this.unflag = unflag;

        this.beforeLocalUpdate();
        this.updateLocalFavourite(this.context);
        this.afterLocalUpdate();
    }

    protected void beforeLocalUpdate()
    {
        // Nothing here, used by subclasses.
    }

    protected void afterLocalUpdate()
    {
        // Nothing here, used by subclasses.
    }

    @Override
    public Void doInBackground(Void result, Response response)
    {
        return null;
    }

    public String getExternalId()
    {
        return externalId;
    }

    public boolean isUnflag()
    {
        return unflag;
    }

    protected abstract FavoriteTable.FavouriteType getType();

    @Override
    public void onFailure(RetrofitError retrofitError)
    {
        super.onFailure(retrofitError);

        // If the API call failed, the action will be saved and re-attempted later

        PendingFavourite pendingFavourite = new PendingFavourite(this.getType(), this.externalId, this.unflag);
        ContentValues cv = pendingFavourite.getContentValues();

        this.context.getContentResolver().insert(PendingFavouriteContentProvider.CONTENT_URI, cv);

        NetworkConnectionMonitor.enable(this.context);
    }

    /**
     * Update the favourite status in the local database, regardless of the success of the API call.
     */
    private void updateLocalFavourite(Context inContext)
    {
        if (this.unflag)
        {
            // Remove favourite
            inContext.getContentResolver().delete(
                    FavoriteContentProvider.CONTENT_URI,
                    FavoriteTable.COLUMN_FAVORITE_ID + " = ? AND " + FavoriteTable.COLUMN_TYPE + " = ?",
                    new String[]{this.externalId, Integer.toString(this.getType().ordinal())});
        }
        else
        {
            // Add favourite
            final ContentValues values = new ContentValues();
            values.put(FavoriteTable.COLUMN_TYPE, this.getType().ordinal());
            values.put(FavoriteTable.COLUMN_FAVORITE_ID, this.externalId);
            inContext.getContentResolver().insert(FavoriteContentProvider.CONTENT_URI, values);
        }
    }

}
