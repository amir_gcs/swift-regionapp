package com.swift.SWIFTRegional.api.apicalls;

import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.swift.SWIFTRegional.models.ApiErrorCode;
import com.swift.SWIFTRegional.utils.InputStreamUtils;

import java.io.InputStream;
import java.util.HashMap;

import com.goodcoresoftware.android.common.http.http.exceptions.ITPResponseException;
import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 27/03/15.
 */
public abstract class AbstractPrivateSibosApiCall extends AbstractJsonApiCall
{
    protected String baseUrl;
    protected String yearString = "";
    public final static String API_ERROR_CODE = "apiErroCode";

    public AbstractPrivateSibosApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.baseUrl =  inContext.getResources().getString(R.string.sibos_base_url);

        String year = inContext.getResources().getString(R.string.sibos_api_year);

        this.yearString = (year == null || year.equals(""))? "" : "?year=" + year;


    }

    @Override
    public void getRequestHeaders(HashMap<String, String> inHeaders)
    {
        super.getRequestHeaders(inHeaders);

        inHeaders.put("Content-Type", "application/json");
    }

    @Override
    public String getUri()
    {
        return this.baseUrl + this.getUriPath() + this.yearString;
    }


    /**
     * Api will return a Json with more details in case of an (api) error.
     * Parse that error here and store it in the bundle so it can be retrieved by the receiver of the call result.
     */
    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException)
    {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);

        try
        {
            if (inException instanceof ITPResponseException)
            {
                InputStream result = (InputStream) ((ITPResponseException) inException).getBody();

                if (result != null)
                {
                    String stringResult = InputStreamUtils.streamToString(result);
                    final ApiErrorCode errorCode = new Gson().fromJson(stringResult, ApiErrorCode.class);
                    inData.putParcelable(API_ERROR_CODE, errorCode);
                }

            }
        }
        catch (ClassCastException exp)
        {
            Log.d(exp.getClass().getSimpleName(), "Exception when attempting to read API error code. ");
        }
        catch(JsonSyntaxException ex)
        {
            // API is offline and returns an HTML error instead of json.
        }

    }
}
