package com.swift.SWIFTRegional.api.callbacks;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.broadcastreceivers.NetworkConnectionMonitor;
import com.swift.SWIFTRegional.contentproviders.UnsentPrivacySettingsContentProvider;
import com.swift.SWIFTRegional.models.PrivacySettings;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class SetPrivacySettingsCallback extends AbstractSibosCallback<Void, PrivacySettings>
{
    private PrivacySettings privacySettings;

    public SetPrivacySettingsCallback(Context context)
    {
        super(context);
    }

    @Override
    public Void doInBackground(PrivacySettings result, Response response)
    {
        UserPreferencesHelper.storePrivacySettings(this.context, this.privacySettings);

        return null;
    }

    @Override
    public void onFailure(RetrofitError retrofitError)
    {
        super.onFailure(retrofitError);

        ContentValues cv = this.privacySettings.getContentValues();
        this.context.getContentResolver().insert(UnsentPrivacySettingsContentProvider.CONTENT_URI, cv);

        NetworkConnectionMonitor.enable(this.context);
    }

    public PrivacySettings getPrivacySettings()
    {
        return privacySettings;
    }

    public void setPrivacySettings(PrivacySettings privacySettings)
    {
        this.privacySettings = privacySettings;
    }
}
