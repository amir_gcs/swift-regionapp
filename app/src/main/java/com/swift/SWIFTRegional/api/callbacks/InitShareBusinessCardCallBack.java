package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.models.businessshare.BusinessCardCode;
import retrofit.client.Response;

/**
 * Created by SimonRaes on 30/04/15.
 */
public class InitShareBusinessCardCallBack extends AbstractSibosCallback<Void, BusinessCardCode>
{
    public InitShareBusinessCardCallBack(Context context)
    {
        super(context);
    }

    @Override
    public Void doInBackground(BusinessCardCode result, Response response)
    {
        String resultCode = result.getShareBusinessCardCode();
        return null;
    }
}
