package com.swift.SWIFTRegional.api.apicalls;

import android.os.Bundle;

import com.goodcoresoftware.android.common.http.http.AbstractHttpClient;
import com.goodcoresoftware.android.common.http.http.utils.RawBody;
import com.goodcoresoftware.android.common.utils.StringUtils;

/**
 * Created by SimonRaes on 27/03/15.
 * A post call that sends data, but does not send any info in the header.
 */
public abstract class AbstractPrivatePostApiCall extends AbstractPrivateSibosApiCall
{
    public final static String PARAM_KEY = "data";

    public AbstractPrivatePostApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public AbstractHttpClient.Method getMethod()
    {
        return AbstractHttpClient.Method.POST;
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        String data = getRawData();

        if (!StringUtils.isEmpty(data))
        {
            RawBody body = new RawBody(data);
            inBundle.putParcelable(PARAM_KEY, body);
        }
    }

    protected abstract String getRawData();
}
