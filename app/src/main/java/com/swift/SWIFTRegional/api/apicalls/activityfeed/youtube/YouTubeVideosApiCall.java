package com.swift.SWIFTRegional.api.apicalls.activityfeed.youtube;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.api.deserializers.YouTubeDeserializer;
import com.swift.SWIFTRegional.contentproviders.VideoContentProvider;

import java.io.IOException;

import com.goodcoresoftware.android.common.http.api.JsonApiCall;
import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.fragments.activityfeed.TwitterNameFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.VideoFragment;
import com.swift.SWIFTRegional.models.video.Video;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by wesley on 27/06/14.
 */
public class YouTubeVideosApiCall extends JsonApiCall
{
    private static final int MAX_COUNT = 50;

    private String youtubeApiKey;
    private String youtubePlayList;
    private String baseUrl;

    public YouTubeVideosApiCall(Bundle bundle)
    {
        super(bundle);
    }

    @Override
    public String getUri()
    {
        if(MainActivity.selectedEventProvider.isEventSelected()) {

            long eventId= MainActivity.selectedEventProvider.getSelectedEvent().getEventId();

//            String url = "https://api.tt.sibos.com/sibosgateway/v1/regional/social/youtube/"+ Long.toString(eventId);
            String url = this.baseUrl + Long.toString(eventId);

            if(VideoFragment.nextToken==null)
            {
                VideoFragment.nextToken="";
            }

            if(!VideoFragment.nextToken.isEmpty())
            {
                url += "?pageToken="+VideoFragment.nextToken;
            }
           return  url;


        }
        else
        {
            return "";
        }
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);
        this.baseUrl = inContext.getString(R.string.sibos_base_url_social) + inContext.getString(R.string.youtube_base_url);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {

    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatusCode, inHeaders, inJsonReader);

        try
        {
            boolean nextPagePresent = false;
            String tokenValue = "";

            if(inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
            {
                inJsonReader.beginObject();

                while (inJsonReader.peek() == JsonToken.NAME)
                {
                    final String name = inJsonReader.nextName();

                    if(name.equals("nextPageToken"))
                    {
                        nextPagePresent=true;
                        tokenValue = inJsonReader.nextString();
                        if(VideoFragment.nextToken!=tokenValue)
                        {
                            if(VideoFragment.nextToken=="")
                            {
                                inContext.getContentResolver().delete(VideoContentProvider.CONTENT_URI, null, null);
                            }
                            VideoFragment.setNextToken(tokenValue);
                        }
                        else{
                            //dont do anything becaz this is full download completed.
                            return;
                        }

                    }
                    else if(name.equals("items"))
                    {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Video.class, new YouTubeDeserializer());
                        Gson gson = gsonBuilder.create();

                        final Video[] videos = gson.fromJson(inJsonReader, Video[].class);

                        final ContentValues[] contentValues = new ContentValues[videos.length];

                        for(int i = 0; i < videos.length; i++)
                        {
                            contentValues[i] = videos[i].getContentValues();
                        }


                        inContext.getContentResolver().bulkInsert(VideoContentProvider.CONTENT_URI, contentValues);

                        UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.VIDEO);

                        ModulePreferenceHelper.saveModuleDataLoaded(inContext, ModulePreferenceHelper.ModuleDataLoaded.DATA_LOADED_SOCIAL_VIDEO, true);

//                        if (videos.length > 0){
                            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_VIDEO, true);
//                        } else {
//                            ModulePreferenceHelper.saveModuleDataAvailable(inContext, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_VIDEO, false);
//                        }

                        //set the next token


                        if(!nextPagePresent)
                        {
                            VideoFragment.setNextToken("CantLoad");
                        }


                        return;
                    }else
                    {
                        inJsonReader.skipValue();
                    }


                }

                inJsonReader.endObject();




            }
        }catch(IOException e)
        {
            Log.e(YouTubeVideosApiCall.class.getSimpleName(), e.getLocalizedMessage());
        }
    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException) {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);
        if(inStatusCode==404)
        {
            VideoFragment.setNextToken("CantLoad");
        }
    }
}
