package com.swift.SWIFTRegional.api.dto;

public class MessageDTO
{

    private String sender;
    private String recepient;
    private String body;
    private String subject;
    private long sendDate;

    public MessageDTO (String inSender, String inRecepient, String inBody, String inSubject, long inSendDate)
    {
        this.sender = inSender;
        this.recepient = inRecepient;
        this.body = inBody;
        this.subject = inSubject;
        this.sendDate = inSendDate;
    }

}
