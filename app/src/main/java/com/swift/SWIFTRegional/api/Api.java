package com.swift.SWIFTRegional.api;

/**
 * Created by Yves on 12/03/14.
 */
public interface Api
{
    int twitter_hashtag = 100;
    int twitter_name = 101;
    int twitter_token = 102;
    int twitter_hashtag_silent = 103;
    int twitter_name_silent = 104;
    int twitter_token_silent = 105;

    int flickr_account = 200;
    int flickr_photosets = 201;
    int flickr_photos = 202;
    int flickr_account_silent = 203;
    int flickr_photosets_silent = 204;

    int category = 300;
    int head_category = 301;

    int speaker = 400;
    int speakercategory=405;
    int head_speaker = 401;
    int speaker_get_favourites = 402;
    int speaker_set_favourite = 403;
    int speaker_remove_favourite = 404;

    int session = 500;
    int session_get_favourites = 501;
    int session_set_favourite = 502;
    int session_remove_favourite = 503;

    int exhibitor = 600;
    int head_exhibitor = 601;
    int exhibitor_collateral = 602;
    int exhibitor_collateral_detail = 603;
    int exhibitor_event = 604;
    int exhibitor_get_favourites = 605;
    int exhibitor_set_favourite = 606;
    int exhibitor_remove_favourite = 607;

    int issues = 700;
    int head_issues = 701;

    int news = 800;
    int head_news = 801;

    int hotel = 900;
    int head_hotel = 901;

    int advertisements = 1000;
    int head_advertisements = 1001;

    int instagram = 1101;
    int youtube = 1102;

    int user = 1200;
    int reset_password = 1201;
    int registration_code = 1202;
    int change_password = 1203;
    int set_share_settings = 1204;
    int get_share_settings = 1205;
    int deauthorize = 1206;
    int post_social_link = 1207;

    int participant = 1300;
    int head_participant = 1301;

    int message = 1400;
    int head_message = 1401;
    int message_threads = 1402;
    int head_message_threads = 1403;
    int message_send = 1404;
    int head_message_threads_notification = 1405;
    int message_threads_notification = 1406;

    int my_program = 1500;
    int head_my_program = 1501;
    int my_program_delete = 1502;
    int my_program_add = 1503;

    int rate = 1600;
    int rate_get = 1601;
    int rate_speaker = 1602;
    int rate_session = 1603;

    int share_business_card = 1700;
    int get_business_card = 1701;

    int floor_plan = 1800;
    int head_floor_plan = 1801;

    int delegate_set_favourite = 1900;
    int delegate_remove_favourite = 1901;
    int delegate_get_favourites = 1903;

    int sponsor = 2000;
    int mainevent=2001;

    int regioninfo=2002;
    int typeinfo=2003;

    int practical_info = 2100;

    int social_info = 2200;
}
