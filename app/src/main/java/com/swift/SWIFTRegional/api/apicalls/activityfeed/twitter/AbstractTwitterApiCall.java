package com.swift.SWIFTRegional.api.apicalls.activityfeed.twitter;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.swift.SWIFTRegional.api.apicalls.AbstractJsonApiCall;
import com.swift.SWIFTRegional.contentproviders.VideoContentProvider;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterHashtagFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.TwitterNameFragment;
import com.swift.SWIFTRegional.fragments.activityfeed.VideoFragment;
import com.swift.SWIFTRegional.models.twitter.ApiData;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;

import java.io.IOException;
import java.util.HashMap;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.twitter.Tweet;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by cliff on 16/05/14.
 */
public abstract class AbstractTwitterApiCall extends AbstractJsonApiCall
{
    public static final String PARAM_SEARCH_COUNT = "count";

    public static final String MAX_TWEET_COUNT = "200";

    private String baseUrl;
    private String bearerToken;


    public AbstractTwitterApiCall(final Bundle inBundle)
    {
        super(inBundle);
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getString(R.string.sibos_base_url_social) + inContext.getString(R.string.twitter_base_url);
        this.bearerToken = PreferencesHelper.getTwitterToken(inContext);
    }

    @Override
    public String getUri()
    {
       return this.baseUrl + this.getUriPath();
    }

    @Override
    public void getRequestHeaders(final HashMap<String, String> inHeaders)
    {
        super.getRequestHeaders(inHeaders);

        inHeaders.put("Authorization", "Bearer " + bearerToken);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        if(!inBundle.containsKey(PARAM_SEARCH_COUNT))
        {
            inBundle.putString(PARAM_SEARCH_COUNT, MAX_TWEET_COUNT);
        }

        inBundle.putString("exclude_replies", "true");

    }

    public abstract Uri getContentUri();

    public void descendToTweetJsonArray(JsonReader inJsonReader) throws IOException
    {

    }

    @Override
    protected void onTaskPostExecuteWithSuccess(Context inContext, Bundle inData, int inStatuscode, Bundle inHeaders, JsonReader inJsonReader)
    {
        super.onTaskPostExecuteWithSuccess(inContext, inData, inStatuscode, inHeaders, inJsonReader);

        String api = inData.getString("Api");
        try
        {
            boolean nextPagePresent = false;
            String tokenValue = "";
            this.descendToTweetJsonArray(inJsonReader);
            final Uri uri = this.getContentUri();

            if (inJsonReader.peek() == JsonToken.BEGIN_OBJECT)
            {
                final Gson gson = new Gson();

                final ApiData data = gson.fromJson(inJsonReader,ApiData.class);

                tokenValue = data.getSearch_metadata().getNext_results();

                if(api.equals("TwitterHash"))
                {
                    if(tokenValue==null)
                    {
                        tokenValue="CantLoad";

                    }
                    else
                    {
                        tokenValue=tokenValue.replace("max_id","maxId");
                    }

                    if(TwitterHashtagFragment.nextToken!=tokenValue)
                    {
                        if(TwitterHashtagFragment.nextToken=="")
                        {
                            inContext.getContentResolver().delete(uri, null, null);
                        }
                        TwitterHashtagFragment.setNextToken(tokenValue);
                    }
                    else{
                        //dont do anything becaz this is full download completed.
                        return;
                    }
                }
                else if (api.equals("TwitterName"))
                {
                    if(tokenValue==null)
                    {
                        tokenValue="CantLoad";

                    }
                    else
                    {
                        tokenValue=tokenValue.replace("max_id","maxId");
                    }

                    if(TwitterNameFragment.nextToken!=tokenValue)
                    {
                        if(TwitterNameFragment.nextToken=="")
                        {
                            inContext.getContentResolver().delete(uri, null, null);
                        }
                        TwitterNameFragment.setNextToken(tokenValue);
                    }
                    else{
                        //dont do anything becaz this is full download completed.
                        return;
                    }
                }


                final ContentValues[] contentValues = new ContentValues[data.getStatuses().length];
                for(int i = 0; i < data.getStatuses().length; i++)
                {
                    final ContentValues cv = data.getStatuses()[i].getContentValues();
                    contentValues[i] = cv;
                }




                inContext.getContentResolver().bulkInsert(uri, contentValues);

                if(api.equals("TwitterName"))
                {
                    UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.TWITTER_NAME);
                }
                else
                {
                    UpdatePreferencesHelper.saveUpdateTimestamp(inContext, UpdatePreferencesHelper.LastUpdateType.TWITTER_HASH);
                }

            }

        } catch (IOException e)
        {
            Log.d(e.getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void onTaskPostExecuteWithError(Context inContext, Bundle inData, int inStatusCode, Bundle inHeaders, Exception inException)
    {
        super.onTaskPostExecuteWithError(inContext, inData, inStatusCode, inHeaders, inException);

        if(inStatusCode == 401)
        {
            PreferencesHelper.removeTwitterToken(inContext);
        }


    }
}
