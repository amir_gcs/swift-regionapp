package com.swift.SWIFTRegional.api.apicalls;

import android.os.Bundle;

import com.goodcoresoftware.android.common.http.http.AbstractHttpClient;
import com.goodcoresoftware.android.common.http.http.utils.RawBody;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 25/03/15.
 * Executes an API POST call and supplies the user's access token as a header parameter and json data as post data.
 */
public abstract class AbstractPrivateTokenPostApiCall extends AbstractPrivateTokenApiCall
{
    public final static String PARAM_KEY = "data";

    public AbstractPrivateTokenPostApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public AbstractHttpClient.Method getMethod()
    {
        return AbstractHttpClient.Method.POST;
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        String data = getRawData();

        if (!StringUtils.isEmpty(data))
        {
            RawBody body = new RawBody(data);
            inBundle.putParcelable(PARAM_KEY, body);
        }
    }

    protected abstract String getRawData();

}
