package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.utils.AlarmHelper;

/**
 * Created by Akhmas on 18/4/2016.
 */
public class SetFavouriteExhibitorSessionCallback extends AbstractSetFavouriteCallback {


    public SetFavouriteExhibitorSessionCallback(Context context, String externalId, boolean unflag) {
        super(context, externalId, unflag);
    }

    @Override
    protected void beforeLocalUpdate()
    {
        AlarmHelper.cancelAllSessionAlarms(this.context);
    }

    @Override
    protected void afterLocalUpdate()
    {
        AlarmHelper.setAllSessionAlarms(this.context);
    }


    @Override
    protected FavoriteTable.FavouriteType getType() {
        return FavoriteTable.FavouriteType.EXHIBITOR_SESSION;
    }
}
