package com.swift.SWIFTRegional.api.callbacks;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.contentproviders.EventContentProvider;
import com.swift.SWIFTRegional.models.EventTiming;

import java.util.ArrayList;

import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import retrofit.client.Response;

public class GetPrivateSessionsCallback extends AbstractSibosCallback<Void, Session[]>
{

    public GetPrivateSessionsCallback(Context context)
    {
        super(context);
    }

    @Override
    public Void doInBackground(Session[] privateSessions, Response response)
    {
        ArrayList<EventTiming> timingsList = new ArrayList<>();

        for (Session session : privateSessions)
        {
            session.setEventId(session.getSessionId());
            // Session type might not be very reliable, make sure private sessions always have this field set to PRIVATE.
            session.setSessionType("PRIVATE");

            for (EventTiming eventTiming : session.getEventTimings())
            {
                eventTiming.setEventId(session.getSessionId());
                timingsList.add(eventTiming);
            }
        }

        // Convert sessions to content values

        final ContentValues[] contentValues = new ContentValues[privateSessions.length];

        for (int n = 0; n < contentValues.length; n++)
        {
            contentValues[n] = privateSessions[n].getContentValues();
        }

        // Convert timings to content values

        EventTiming[] timings = timingsList.toArray(new EventTiming[timingsList.size()]);

        final ContentValues[] timingContentValues = new ContentValues[timings.length];

        for (int z = 0; z < timings.length; z++)
        {
            timingContentValues[z] = timings[z].getContentValues();
        }

        this.context.getContentResolver().delete(EventContentProvider.CONTENT_URI_PRIVATE_SESSIONS, null, null);
        this.context.getContentResolver().bulkInsert(EventContentProvider.CONTENT_URI_PRIVATE_SESSIONS, contentValues);

        this.context.getContentResolver().delete(EventTimingContentProvider.CONTENT_URI_PRIVATE_SESSIONS, null, null);
        this.context.getContentResolver().bulkInsert(EventTimingContentProvider.CONTENT_URI_PRIVATE_SESSIONS, timingContentValues);

        UpdatePreferencesHelper.saveUpdateTimestamp(this.context, UpdatePreferencesHelper.LastUpdateType.PRIVATE_SESSIONS);

        return null;
    }
}
