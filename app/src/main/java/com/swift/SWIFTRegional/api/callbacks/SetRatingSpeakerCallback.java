package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.database.tables.RateTable;

public class SetRatingSpeakerCallback extends AbstractSetRatingCallback
{
    public SetRatingSpeakerCallback(Context context, String externalId, int rating)
    {
        super(context, externalId, rating);
    }

    @Override
    protected RateTable.RateType getType()
    {
        return RateTable.RateType.SPEAKER;
    }
}


