package com.swift.SWIFTRegional.api.apicalls.activityfeed.flickr;

import android.content.Context;
import android.os.Bundle;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.apicalls.AbstractJsonApiCall;
import com.swift.SWIFTRegional.utils.preferences.PreferencesHelper;

public abstract class BaseFlickrApiCall extends AbstractJsonApiCall
{
    private static final String PARAM_API_KEY = "api_key";
    private static final String PARAM_USER_ID = "user_id";
    private static final String PARAM_FORMAT = "format";

    protected static final String PARAM_METHOD = "method";
    protected static final String PARAM_PER_PAGE = "per_page";

    private String baseUrl;
    private String userId;
    private String apiKey;

    public BaseFlickrApiCall(final Bundle inBundle)
    {
        super(inBundle);
    }

    @Override
    public String getUri()
    {
        return baseUrl;
    }

    @Override
    public String getUriPath()
    {
        return "";
    }

    @Override
    public void onTaskPreExecute(Context inContext)
    {
        super.onTaskPreExecute(inContext);

        this.baseUrl = inContext.getString(R.string.flickr_base_url);
        this.apiKey = "ceb5c6f0fe970bafbe40c9888c6904e8";

        this.userId = PreferencesHelper.getFlickrAccountId(inContext);
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        inBundle.putString(PARAM_API_KEY, this.apiKey);
        inBundle.putString(PARAM_USER_ID, this.userId);
        inBundle.putString(PARAM_FORMAT, "json");
    }
}
