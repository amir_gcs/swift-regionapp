package com.swift.SWIFTRegional.api.apicalls;

import android.os.Bundle;

import com.goodcoresoftware.android.common.http.http.AbstractHttpClient;
import com.goodcoresoftware.android.common.http.http.utils.RawBody;
import com.goodcoresoftware.android.common.utils.StringUtils;

/**
 * Created by wesley on 01/07/14.
 */
public abstract class AbstractPrivate1DeleteApiCall extends AbstractPrivate1ApiCall
{
    public final static String PARAM_KEY = "data";

    public AbstractPrivate1DeleteApiCall(Bundle inParams)
    {
        super(inParams);
    }

    @Override
    public AbstractHttpClient.Method getMethod()
    {
        return AbstractHttpClient.Method.DELETE;
    }

    @Override
    public void getRequestParams(Bundle inBundle)
    {
        super.getRequestParams(inBundle);

        String data = getRawData();

        if (!StringUtils.isEmpty(data))
        {
            RawBody body = new RawBody(data);
            inBundle.putParcelable(PARAM_KEY, body);
        }
    }

    protected abstract String getRawData();
}
