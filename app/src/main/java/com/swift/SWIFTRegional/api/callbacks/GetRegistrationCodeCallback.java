package com.swift.SWIFTRegional.api.callbacks;

import android.content.Context;

import com.swift.SWIFTRegional.models.RegistrationCode;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;
import retrofit.client.Response;

public class GetRegistrationCodeCallback extends AbstractSibosCallback<Void, RegistrationCode>
{
    public GetRegistrationCodeCallback(Context context)
    {
        super(context);
    }

    @Override
    public Void doInBackground(RegistrationCode result, Response response)
    {
        UserPreferencesHelper.storeRegistrationCode(this.context, result);

        return null;
    }
}
