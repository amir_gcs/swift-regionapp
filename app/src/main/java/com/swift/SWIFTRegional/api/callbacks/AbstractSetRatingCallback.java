package com.swift.SWIFTRegional.api.callbacks;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.api.dto.RateDTO;
import com.swift.SWIFTRegional.broadcastreceivers.NetworkConnectionMonitor;
import com.swift.SWIFTRegional.contentproviders.RateContentProvider;
import com.swift.SWIFTRegional.contentproviders.UnsentRateContentProvider;
import com.swift.SWIFTRegional.database.tables.RateTable;
import com.swift.SWIFTRegional.models.Rate;
import retrofit.RetrofitError;
import retrofit.client.Response;

public abstract class AbstractSetRatingCallback extends AbstractSibosCallback<Void, Void>
{
    private final RateDTO rateDTO;
    private final String externalId;

    private final int rating;

    public AbstractSetRatingCallback(Context context, String externalId, int rating)
    {
        super(context);

        this.rateDTO = new RateDTO(externalId, rating);
        this.externalId = externalId;
        this.rating = rating;

        // Immediately store the rating locally
        this.storeLocalFavourite();
    }

    @Override
    public Void doInBackground(Void result, Response response)
    {
        return null;
    }

    @Override
    public void onFailure(RetrofitError retrofitError)
    {
        super.onFailure(retrofitError);

        // Store the unsent rating locally so it can be reattempted later.
        Rate unsentRate = new Rate(this.rateDTO, this.getType());
        ContentValues cv = unsentRate.getContentValues();

        this.context.getContentResolver().insert(UnsentRateContentProvider.CONTENT_URI, cv);

        this.storeLocalFavourite();

        NetworkConnectionMonitor.enable(context);
    }

    private void storeLocalFavourite()
    {
        // Store the rating locally.
        ContentValues cv = new ContentValues();
        cv.put(RateTable.COLUMN_ID, this.externalId);
        cv.put(RateTable.COLUMN_RATE, this.rating);
        cv.put(RateTable.COLUMN_TYPE, this.getType().ordinal());

        this.context.getContentResolver().insert(RateContentProvider.CONTENT_URI, cv);
    }

    public String getExternalId()
    {
        return externalId;
    }

    public int getRating()
    {
        return rating;
    }

    protected abstract RateTable.RateType getType();
}
