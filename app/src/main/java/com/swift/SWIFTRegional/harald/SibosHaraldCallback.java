package com.swift.SWIFTRegional.harald;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

import com.goodcoresoftware.android.common.utils.StringUtils;
import com.swift.beacons.middleware.SibosHaraldMiddleware;
import mobi.inthepocket.beacons.sdk.ITPBeaconCallback;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.User;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

public class SibosHaraldCallback implements ITPBeaconCallback, SharedPreferences.OnSharedPreferenceChangeListener
{
    public static final String PLACEHOLDER_NAME = "[NAME]";
    public static final String PLACEHOLDER_VALUE = "Delegate";

    private User user;
    private final Context context;

    public SibosHaraldCallback(Context context)
    {
        this.context = context;
        this.user = UserPreferencesHelper.getLoggedInUser(context);

        UserPreferencesHelper.registerAsListener(context, this);
    }

    @Override
    public boolean shouldPresentWebView(Context context, String url, String venueCode)
    {
        if (SibosHaraldMiddleware.isLocateMeActionUrl(url))
        {
            return false;
        }

        return true;
    }

    @Override
    public boolean shouldPresentBackgroundNotification(Context context, String message, String url, String venueCode)
    {
        if (SibosHaraldMiddleware.isLocateMeActionUrl(url))
        {
            return false;
        }

        if (!StringUtils.isEmpty(message))
        {
            try
            {
                String parsedMessage = this.parseMessage(message);

                if (!parsedMessage.equals(message) && !StringUtils.isEmpty(parsedMessage))
                {
                    // String contains placeholders, so trigger custom notification

                    this.triggerCustomNotificationWithMessage(parsedMessage);

                    return false;
                }

                return true;

            }
            catch (NullPointerException e)
            {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean shouldPresentForegroundAlert(Context context, String message, String url, String venueCode)
    {
        if (SibosHaraldMiddleware.isLocateMeActionUrl(url))
        {
            return false;
        }

        if (!StringUtils.isEmpty(message))
        {
            try
            {
                String parsedMessage = this.parseMessage(message);

                if (!parsedMessage.equals(message) && !StringUtils.isEmpty(parsedMessage))
                {
                    // String contains placeholders, so trigger custom notification

                    this.triggerCustomAlertWithMessage(parsedMessage);

                    return false;
                }

                return true;

            }
            catch (NullPointerException e)
            {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean shouldLogTriggeredAction(String uuid, int major, int minor, String venueCode)
    {
        return true;
    }

    private String parseMessage(String message)
    {
        if (message.contains(PLACEHOLDER_NAME))
        {
            if (UserPreferencesHelper.loggedIn(this.context))
            {
                message = message.replace(PLACEHOLDER_NAME, user.getFirstName());
            }
            else
            {
                message = message.replace(PLACEHOLDER_NAME, PLACEHOLDER_VALUE);
            }

        }
        return message;
    }

    private void triggerCustomNotificationWithMessage(String message)
    {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this.context);

        builder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(this.context.getResources().getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_notification)
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND);

        NotificationManager notificationManager = (NotificationManager) this.context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

    private void triggerCustomAlertWithMessage(String message)
    {
        triggerCustomNotificationWithMessage(message);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if (key.equals(UserPreferencesHelper.LOGGED_IN))
        {
            if (UserPreferencesHelper.loggedIn(this.context))
            {
                this.user = UserPreferencesHelper.getLoggedInUser(context);
            }
            else
            {
                this.user = null;
            }
        }
    }
}
