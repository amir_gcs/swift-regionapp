package com.swift.SWIFTRegional.utils;

import android.text.TextUtils;

import com.swift.SWIFTRegional.models.meetconnect.BusinessCard;

/**
 * Format addresses based upon {@code http://en.wikipedia.org/wiki/Address_(geography)#United_States}.
 */
public class AddressFormatter
{

    public static String format(BusinessCard inBusinessCard)
    {
        return format(inBusinessCard.getAddress1(), inBusinessCard.getAddress2(), inBusinessCard.getCity(), inBusinessCard.getState(), inBusinessCard.getZip(), inBusinessCard.getCountry());
    }

    public static String format(AddressBookHelper.Contact.Address inAddress)
    {
        String formattedAddress = format(inAddress.street, "", inAddress.city, "", inAddress.postalCode, inAddress.country);
        return formattedAddress.replace("\n", ", ");
    }

    private static String format(String inAddress1, String inAddress2, String inCity, String inState, String inZip, String inCountry)
    {
        StringBuilder addressSb = new StringBuilder();

        addAddressLine(addressSb, inAddress1, inAddress2);
        addAddressLine(addressSb, inCity, inState, inZip);
        addAddressLine(addressSb, inCountry);

        return removeTrailingNextline(addressSb.toString());
    }

    private static String removeTrailingNextline(String inString)
    {

        if (inString.length() > 0 && inString.charAt(inString.length() - 1) == '\n')
        {
            inString = inString.substring(0, inString.length() - 1);
        }
        return inString;
    }


    private static void addAddressLine(StringBuilder sb, String... addressElement)
    {

        StringBuilder sb2 = new StringBuilder();
        for (String element : addressElement)
        {
            if (!TextUtils.isEmpty(element))
            {
                sb2.append(element);
                sb2.append(" ");
            }
        }

        String addressLine = sb2.toString().trim();

        sb.append(addressLine);

        if (sb.length() > 0)
        {
            sb.append("\n");
        }
    }
}
