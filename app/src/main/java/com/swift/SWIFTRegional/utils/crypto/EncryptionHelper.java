package com.swift.SWIFTRegional.utils.crypto;

import android.content.Context;
import android.provider.Settings;

import java.security.MessageDigest;

public class EncryptionHelper
{

    public static String encrypt(Context inContext, String inValue, String inSalt) throws Exception
    {
        final String androidId = Settings.System.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        String keySeed = androidId + inSalt;

        return SimpleCrypto.encrypt(keySeed, inValue);
    }

    public static String decrypt(Context inContext, String inValue, String inSalt) throws Exception
    {
        final String androidId = Settings.System.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        String keySeed = androidId + inSalt;

        return SimpleCrypto.decrypt(keySeed, inValue);
    }

    public static String stringToSha256(String input)
    {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

}
