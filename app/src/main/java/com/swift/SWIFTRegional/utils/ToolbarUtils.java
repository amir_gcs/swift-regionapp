package com.swift.SWIFTRegional.utils;

import android.app.ActivityManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;

import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 6/05/15.
 */
public class ToolbarUtils
{
    /**
     * Update the toolbar, statusbar and "Recent apps" colors. Only affects Lollipop and above.
     */
    public static void updateToolbarColor(ActionBarActivity activity, int inColor)
    {
        int toolbarColor = activity.getResources().getColor(inColor);
        final ColorDrawable drawable = new ColorDrawable(toolbarColor);
        activity.getSupportActionBar().setBackgroundDrawable(drawable);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            activity.getWindow().setStatusBarColor(ColorUtils.darkenColor(toolbarColor));

            // Customize the app's card in the Android "Recent apps" menu (used to set toolbar color).
            ActivityManager.TaskDescription taskDescription = new ActivityManager.TaskDescription(
                    activity.getString(R.string.app_name),
                    BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_launcher),
                    toolbarColor);
            activity.setTaskDescription(taskDescription);
        }
    }
}
