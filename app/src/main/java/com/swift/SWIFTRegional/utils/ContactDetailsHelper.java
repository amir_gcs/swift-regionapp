package com.swift.SWIFTRegional.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;


public class ContactDetailsHelper
{
    public static void sendEmail(Context inContext, String inEmailAddress)
    {
        final Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + inEmailAddress));
        try
        {
            inContext.startActivity(Intent.createChooser(emailIntent, inContext.getResources().getString(R.string.send_mail)));
        } catch (ActivityNotFoundException e)
        {
            Log.d(ContactDetailsHelper.class.getSimpleName(), e.getMessage());
        }
    }

    public static void callNumber(Context inContext, String inPhoneNumber)
    {
        final Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + inPhoneNumber));
        try
        {
            inContext.startActivity(callIntent);
        } catch (ActivityNotFoundException e)
        {
            Log.d(ContactDetailsHelper.class.getSimpleName(), e.getMessage());
        }
    }

    public static void viewOnMap(Context inContext, String inAddress)
    {
        final String encodedAddress = Html.fromHtml(inAddress).toString();
        final Intent addressIntent = new Intent(Intent.ACTION_VIEW);
        addressIntent.setData(Uri.parse("geo:0,0?q=:" + encodedAddress));
        try
        {
            inContext.startActivity(Intent.createChooser(addressIntent, inContext.getResources().getString(R.string.view_address)));
        } catch (ActivityNotFoundException e)
        {
            Log.d(ContactDetailsHelper.class.getSimpleName(), e.getMessage());
        }
    }
}
