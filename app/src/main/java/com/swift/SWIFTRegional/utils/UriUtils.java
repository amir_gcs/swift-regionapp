package com.swift.SWIFTRegional.utils;

/**
 * Created by Wouter Pinnoo on 9/2/14.
 */
public class UriUtils
{
    public static boolean isExternUri(String uri)
    {
        return !(uri.startsWith("http://www.sibos.com") || uri.startsWith("https://www.sibos.com"))
                || uri.endsWith(".pdf");
    }
}
