package com.swift.SWIFTRegional.utils.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.swift.SWIFTRegional.R;

/**
 * Created by Wesley on 2/06/14.
 */
public class UpdatePreferencesHelper
{
    private static final String PREFERENCES_NAME = "sibosPrefsUpdates";

    public static final String LAST_SEEN_TWITTER = "lastTweetTimestamp";
    public static final String LAST_SEEN_WALL = "lastWallTimestamp";
    public static final String LAST_SEEN_NEWS = "lastNewsTimestamp";
    public static final String LAST_SEEN_FLICKR = "lastPicturesTimestamp";
    public static final String LAST_SEEN_INSTAGRAM = "lastInstagramTimestamp";
    public static final String LAST_SEEN_VIDEO = "lastVideoTimestamp";

    private static final String LAST_UPDATE_TWITTER_NAME = "lastUpdateTwitterName";
    private static final String LAST_UPDATE_NEWS = "lastUpdateNews";
    private static final String LAST_UPDATE_ISSUES = "lastUpdateIssues";
    private static final String LAST_UPDATE_FLICKR = "lastUpdateFlickr";
    private static final String LAST_UPDATE_SESSIONS = "lastUpdateSessions";
    private static final String LAST_UPDATE_CATEGORIES = "lastUpdateCategories";
    private static final String LAST_UPDATE_SPEAKERS = "lastUpdateSpeakers";
    private static final String LAST_UPDATE_EXHIBITORS = "lastUpdateExhibitors";
    private static final String LAST_UPDATE_HOTELS = "lastUpdateHotels";
    private static final String LAST_UPDATE_ADVERTISEMENTS = "lastUpdateAdvertisements";
    private static final String LAST_UPDATE_INSTAGRAM = "lastUpdateInstagram";
    private static final String LAST_UPDATE_VIDEO = "lastUpdateVideo";
    private static final String LAST_UPDATE_PARTICIPANTS = "lastUpdateParticipants";
    private static final String LAST_UPDATE_QUICKBLOXUSERS = "lastUpdateQuickBloxUsers";
    private static final String LAST_UPDATE_FlOORPLAN = "lastUpdateFloorplan";
    private static final String LAST_UPDATE_COLLATERALS = "lastUpdateCollaterals";
    private static final String LAST_UPDATE_EXHIBITOR_EVENTS = "lastUpdateExhibitorEvents";

    private static final String API_LAST_MODIFIED_EXHIBITORS = "lastModifiedExhibitors";
    private static final String API_LAST_MODIFIED_ADVERTISEMENTS = "lastModifiedAdvertisements";
    private static final String API_LAST_MODIFIED_PARTICIPANTS = "lastModifiedParticipants";
    private static final String API_LAST_MODIFIED_MESSAGE_THREADS = "lastModifiedMessageThreads";

    private static Long UPDATE_INTERVAL;
    private static Long UPDATE_INTERVAL_INSTAGRAM;

    public enum LastSeenType
    {
        WALL,
        TWITTER,
        NEWS,
        FLICKR,
        INSTAGRAM,
        VIDEO
    }

    public enum LastUpdateType
    {
        TWITTER_NAME,
        TWITTER_HASH,
        NEWS,
        ISSUES,
        FLICKR,
        SESSIONS,
        CATEGORIES,
        SPEAKERCATEGORIES,
        SPEAKERS,
        EXHIBITORS,
        SPONSORS,
        MAINEVENTS,
        HOTELS,
        ADVERTISEMENTS,
        EXHIBITOR_EVENTS,
        PRIVATE_SESSIONS,
        INSTAGRAM,
        COLLATERALS,
        PARTICIPANTS,
        VIDEO,
        FLOORPLAN,
        REGIONINFO, QUICKBLOXUSERS
    }

    public enum LastModifiedType
    {
        EXHIBITORS,
        SPONSORS,
        ADVERTISEMENTS,
        PARTICIPANTS,
        MESSAGE_THREADS, MAINEVENTS, REGIONINFO,
    }

    @SuppressLint("InlinedApi")
    public static SharedPreferences getPreferences(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD_MR1)
        {
            return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_MULTI_PROCESS);
        }
        else
        {
            return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }
    }

    public static final long getLastUpdate(Context context, LastSeenType type)
    {
        final SharedPreferences prefs = UpdatePreferencesHelper.getPreferences(context);

        final String key = UpdatePreferencesHelper.getKey(type);

        return prefs.getLong(key, 0);
    }

    public static final void saveLastItemTimestamp(Context context, LastSeenType type, long dateTime)
    {
        final String key = UpdatePreferencesHelper.getKey(type);

        if (key != null)
        {
            final SharedPreferences.Editor prefs = UpdatePreferencesHelper.getPreferences(context).edit();

            prefs.putLong(key, dateTime);

            prefs.commit();
        }
    }


    public static final boolean isUpdateNeeded(Context context, LastUpdateType lastUpdateType)
    {
        final String key = UpdatePreferencesHelper.getKey(lastUpdateType);

        if (UPDATE_INTERVAL == null)
        {
            UPDATE_INTERVAL = (long) context.getResources().getInteger(R.integer.update_interval_minutes) * 60 * 1000;
        }

        if (UPDATE_INTERVAL_INSTAGRAM == null)
        {
            UPDATE_INTERVAL_INSTAGRAM = (long) context.getResources().getInteger(R.integer.update_interval_instagram_minutes) * 60 * 1000;
        }

        final long updateInterval;
        if (lastUpdateType == LastUpdateType.INSTAGRAM)
        {
            updateInterval = UPDATE_INTERVAL_INSTAGRAM;
        }
        else
        {
            updateInterval = UPDATE_INTERVAL;
        }

        if (key != null)
        {
            final SharedPreferences prefs = UpdatePreferencesHelper.getPreferences(context);

            if (!prefs.contains(key))
            {
                return true;
            }

            final long lastUpdate = prefs.getLong(key, 0);

            return lastUpdate + updateInterval < System.currentTimeMillis();
        }

        return true;
    }

    public static final boolean isUpdateNeeded(Context context, LastModifiedType lastModifiedType, long date)
    {
        final String key = UpdatePreferencesHelper.getKey(lastModifiedType);

        if (key != null)
        {
            final SharedPreferences prefs = UpdatePreferencesHelper.getPreferences(context);

            if (!prefs.contains(key))
            {
                return true;
            }

            final long lastModified = prefs.getLong(key, 0);

            return date > lastModified;
        }

        return true;
    }

    public static final String getApiHeaderTimeStamp(Context context, LastModifiedType type)
    {
        final SharedPreferences prefs = UpdatePreferencesHelper.getPreferences(context);

        final String key = UpdatePreferencesHelper.getKey(type);

        return prefs.getString(key, "");
    }

    /**
     * Save the DATA_TIMESTAMP value that gets returned in the header of some API calls.
     */
    public static final void saveApiHeaderTimeStamp(Context context, LastModifiedType lastModifiedType, String date)
    {
        final String key = UpdatePreferencesHelper.getKey(lastModifiedType);

        if (key != null)
        {
            final SharedPreferences.Editor prefs = UpdatePreferencesHelper.getPreferences(context).edit();

            prefs.putString(key, date);

            prefs.commit();
        }
    }

    public static final void saveUpdateTimestamp(Context context, LastUpdateType lastUpdateType)
    {
        final String key = UpdatePreferencesHelper.getKey(lastUpdateType);

        if (key != null)
        {
            final SharedPreferences.Editor prefs = UpdatePreferencesHelper.getPreferences(context).edit();

            prefs.putLong(key, System.currentTimeMillis());

            prefs.commit();
        }
    }

    /**
     * Allows the saving of a custom last updated timestamp.
     */
    public static final void saveUpdateTimestamp(Context context, LastUpdateType lastUpdateType, long timeMillis)
    {
        final String key = UpdatePreferencesHelper.getKey(lastUpdateType);

        if (key != null)
        {
            final SharedPreferences.Editor prefs = UpdatePreferencesHelper.getPreferences(context).edit();

            prefs.putLong(key, timeMillis);

            prefs.commit();
        }
    }

    private static final String getKey(LastSeenType type)
    {
        final String key;

        switch (type)
        {
            case WALL:

                key = LAST_SEEN_WALL;
                break;

            case TWITTER:

                key = LAST_SEEN_TWITTER;
                break;

            case NEWS:

                key = LAST_SEEN_NEWS;
                break;

            case FLICKR:

                key = LAST_SEEN_FLICKR;
                break;

            case INSTAGRAM:

                key = LAST_SEEN_INSTAGRAM;
                break;

            case VIDEO:

                key = LAST_SEEN_VIDEO;
                break;

            default:

                key = null;
                break;
        }

        return key;
    }

    private static final String getKey(LastUpdateType lastUpdateType)
    {
        final String key;

        switch (lastUpdateType)
        {
            case TWITTER_NAME:

                key = LAST_UPDATE_TWITTER_NAME;
                break;

            case NEWS:

                key = LAST_UPDATE_NEWS;
                break;

            case ISSUES:

                key = LAST_UPDATE_ISSUES;
                break;

            case FLICKR:

                key = LAST_UPDATE_FLICKR;
                break;

            case SESSIONS:

                key = LAST_UPDATE_SESSIONS;
                break;

            case CATEGORIES:

                key = LAST_UPDATE_CATEGORIES;
                break;

            case SPEAKERS:

                key = LAST_UPDATE_SPEAKERS;
                break;

            case EXHIBITORS:

                key = LAST_UPDATE_EXHIBITORS;
                break;

            case HOTELS:

                key = LAST_UPDATE_HOTELS;
                break;

            case ADVERTISEMENTS:

                key = LAST_UPDATE_ADVERTISEMENTS;
                break;

            case EXHIBITOR_EVENTS:

                key = LAST_UPDATE_EXHIBITOR_EVENTS;
                break;

            case INSTAGRAM:

                key = LAST_UPDATE_INSTAGRAM;
                break;

            case COLLATERALS:

                key = LAST_UPDATE_COLLATERALS;
                break;

            case VIDEO:

                key = LAST_UPDATE_VIDEO;
                break;

            case PARTICIPANTS:

                key = LAST_UPDATE_PARTICIPANTS;
                break;

            case QUICKBLOXUSERS:

                key = LAST_UPDATE_QUICKBLOXUSERS;
                break;

            case FLOORPLAN:

                key = LAST_UPDATE_FlOORPLAN;
                break;

            default:

                key = null;
                break;
        }

        return key;
    }

    private static final String getKey(LastModifiedType lastModifiedType)
    {
        final String key;

        switch (lastModifiedType)
        {
            case EXHIBITORS:

                key = API_LAST_MODIFIED_EXHIBITORS;
                break;

            case ADVERTISEMENTS:

                key = API_LAST_MODIFIED_ADVERTISEMENTS;
                break;

            case PARTICIPANTS:

                key = API_LAST_MODIFIED_PARTICIPANTS;
                break;

            case MESSAGE_THREADS:

                key = API_LAST_MODIFIED_MESSAGE_THREADS;
                break;

            default:

                key = null;
                break;
        }

        return key;
    }

    public static void registerChangeListener(Context inContext, SharedPreferences.OnSharedPreferenceChangeListener inListener)
    {
        SharedPreferences preferences = getPreferences(inContext);
        preferences.registerOnSharedPreferenceChangeListener(inListener);
    }

    public static void unRegisterChangeListener(Context inContext, SharedPreferences.OnSharedPreferenceChangeListener inListener)
    {
        SharedPreferences preferences = getPreferences(inContext);
        preferences.unregisterOnSharedPreferenceChangeListener(inListener);
    }

    public static void resetSessionUpdateTimeStamps(Context inContext)
    {
        final SharedPreferences.Editor prefs = UpdatePreferencesHelper.getPreferences(inContext).edit();

        prefs.remove(LAST_UPDATE_CATEGORIES);
        prefs.remove(LAST_UPDATE_SPEAKERS);
        prefs.remove(LAST_UPDATE_SESSIONS);
        prefs.remove(LAST_UPDATE_EXHIBITOR_EVENTS);
        prefs.remove(LAST_UPDATE_EXHIBITORS);

        prefs.remove(LAST_UPDATE_PARTICIPANTS);
        prefs.remove(LAST_UPDATE_COLLATERALS);
        prefs.remove(LAST_UPDATE_FlOORPLAN);

        prefs.commit();
    }

    public static void resetUpdateTimeStamps(Context inContext)
    {
        for (LastUpdateType lastUpdateType : LastUpdateType.values())
        {
            String key = getKey(lastUpdateType);
            UpdatePreferencesHelper.getPreferences(inContext).edit().remove(key).commit();
        }

        for (LastModifiedType lastModifiedType : LastModifiedType.values())
        {
            String key = getKey(lastModifiedType);
            UpdatePreferencesHelper.getPreferences(inContext).edit().remove(key).commit();
        }
    }

    public static void resetAllTimestamps(Context context) {

        final SharedPreferences.Editor prefs = UpdatePreferencesHelper.getPreferences(context).edit();

        prefs.remove(PREFERENCES_NAME);

        prefs.remove(LAST_SEEN_TWITTER);
        prefs.remove(LAST_SEEN_WALL);
        prefs.remove(LAST_SEEN_NEWS);
        prefs.remove(LAST_SEEN_FLICKR);
        prefs.remove(LAST_SEEN_INSTAGRAM);
        prefs.remove(LAST_SEEN_VIDEO);

        prefs.remove(LAST_UPDATE_TWITTER_NAME);
        prefs.remove(LAST_UPDATE_NEWS);
        prefs.remove(LAST_UPDATE_ISSUES);
        prefs.remove(LAST_UPDATE_FLICKR);
        prefs.remove(LAST_UPDATE_SESSIONS);
        prefs.remove(LAST_UPDATE_CATEGORIES);
        prefs.remove(LAST_UPDATE_SPEAKERS);
        prefs.remove(LAST_UPDATE_EXHIBITORS);
        prefs.remove(LAST_UPDATE_HOTELS);
        prefs.remove(LAST_UPDATE_ADVERTISEMENTS);
        prefs.remove(LAST_UPDATE_INSTAGRAM);
        prefs.remove(LAST_UPDATE_VIDEO);
        prefs.remove(LAST_UPDATE_PARTICIPANTS);
        prefs.remove(LAST_UPDATE_QUICKBLOXUSERS);
        prefs.remove(LAST_UPDATE_FlOORPLAN);
        prefs.remove(LAST_UPDATE_COLLATERALS);
        prefs.remove(LAST_UPDATE_EXHIBITOR_EVENTS);

        prefs.remove(API_LAST_MODIFIED_EXHIBITORS);
        prefs.remove(API_LAST_MODIFIED_ADVERTISEMENTS);
        prefs.remove(API_LAST_MODIFIED_PARTICIPANTS);
        prefs.remove(API_LAST_MODIFIED_MESSAGE_THREADS);
    }
}
