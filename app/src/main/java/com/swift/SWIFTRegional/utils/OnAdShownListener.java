package com.swift.SWIFTRegional.utils;

import android.widget.AbsListView;

import java.util.HashSet;

import com.swift.SWIFTRegional.models.Advertisement;

public abstract class OnAdShownListener implements AbsListView.OnScrollListener
{
    private HashSet<String> trackedUrls;

    // Keep track of the item the farthest down that was already checked.
    // Only check items that are past the last checked item so no item is ever checked twice.
    private int highestCheckedPosition = -1;


    public OnAdShownListener()
    {
        this.trackedUrls = new HashSet<>();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {
        for (int i = 0; i < visibleItemCount; i++)
        {
            final int position = i + firstVisibleItem;

            if (position > this.highestCheckedPosition)
            {
                this.highestCheckedPosition = position;

                final Object o = getItem(position);

                if (o instanceof Advertisement)
                {
                    String url = ((Advertisement) o).getLink();
                    if (!this.trackedUrls.contains(url))
                    {
                        trackAdUrl(url);
                        this.trackedUrls.add(url);
                    }
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState)
    {

    }

    public abstract void trackAdUrl(String url);

    public abstract Object getItem(int position);
}