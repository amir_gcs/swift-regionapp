package com.swift.SWIFTRegional.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.swift.SWIFTRegional.contentproviders.CategoryContentProvider;
import com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider;
import com.swift.SWIFTRegional.contentproviders.SessionSpeakerContentProvider;
import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.CategoryTable;
import com.swift.SWIFTRegional.database.tables.SessionSpeakerTable;
import com.swift.SWIFTRegional.loaders.LoaderIDs;
import com.swift.SWIFTRegional.models.Category;
import com.swift.SWIFTRegional.models.EventTiming;

import java.util.ArrayList;
import java.util.HashMap;

import com.goodcoresoftware.android.common.utils.StringUtils;

import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;
import com.swift.SWIFTRegional.models.ExhibitorEvent;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.models.session.SessionSpeaker;

/**
 * Created by SimonRaes on 20/04/15.
 * Util class that loads and assembles all objects required to display a list of sessions.
 */
public class SessionLoader implements LoaderManager.LoaderCallbacks<Cursor>
{
    private static final String EXTRA_SESSION_SPEAKERS_WHERE = "speakers_where";
    private static final String EXTRA_SESSION_SPEAKERS_ARGUMENTS = "speakers_arguments";
    public static Category[] categories;
    private HashMap<String, Exhibitor> exhibitorPerCCVK;
    protected EventTiming[] eventTimings;
    private HashMap<String, ArrayList<SessionSpeaker>> sessionSpeakersSortedPerSession;  // { <SessionId: [Speaker0, ...]>, ... }

    private Context context;
    private LoaderManager loaderManager;

    private boolean eventsLoaded;
    private boolean speakerLoadingDone;
    private boolean categoriesLoaded;
    private boolean exhibitorLoadingDone;
    private boolean shouldLoadAllSpeakers;
    private boolean shouldLoadExhibitors;

    // Set to true if we are getting a list of sessions for a specific speaker.
    // Only the speakers for those sessions will be loaded from the database.
    private boolean speakerSessionsMode;


    public enum UriMode
    {
        ALL,
        SUGGESTED,
        FAVOURITES,
        CONFERENCE,
        TV,
        EXHIBITOR, // All events for all exhibitors
        TIMING,
        SPEAKER, // All sessions for the selected speaker
        STREAM,
        TAG,
        SESSIONROOM, SESSIONTYPE, SESSIONDAY,
        EVENTS_FOR_EXHIBITOR // All events for the selected exhibitor
    }

    private UriMode uriMode = UriMode.ALL;
    private String pathAppendix;
    private String searchText;

    private SessionLoaderListener listener;

    public interface SessionLoaderListener
    {
        void sessionsLoaded(EventTiming[] eventTimings);
    }

    public SessionLoader(Context context, LoaderManager loaderManager, SessionLoaderListener listener)
    {
        this.context = context;
        this.loaderManager = loaderManager;
        this.listener = listener;
    }

    public void getEventTimings(UriMode uriMode, String pathAppendix, String searchText)
    {
        this.reset();
        this.uriMode = uriMode;
        this.pathAppendix = pathAppendix;
        this.searchText = searchText;

        if (this.uriMode == UriMode.SPEAKER)
        {
            this.speakerSessionsMode = true;
        }

        loaderManager.restartLoader(LoaderIDs.category, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        switch (id)
        {
            case LoaderIDs.category:
                this.categoriesLoaded = false;
                return new CursorLoader(context, CategoryContentProvider.CONTENT_URI, null, null, null, null);

            case LoaderIDs.session_speakers:

                String whereClause = null;
                String[] arguments = null;

                if (args != null)
                {
                    whereClause = args.getString(EXTRA_SESSION_SPEAKERS_WHERE);
                    ArrayList<String> argumentsList = args.getStringArrayList(EXTRA_SESSION_SPEAKERS_ARGUMENTS);
                    arguments = argumentsList.toArray(new String[argumentsList.size()]);
                }


                final DatabaseHelper helper = new DatabaseHelper(context);
                SQLiteDatabase database = helper.getWritableDatabase();

                String updateQuery = "UPDATE " + SessionSpeakerTable.TABLE_NAME +
                        " SET " + SessionSpeakerTable.COLUMN_SPEAKER_TYPENAME +"= (SELECT "+ CategoryTable.COLUMN_NAME+
                        " FROM " + CategoryTable.TABLE_NAME +
                        " WHERE "+CategoryTable.TABLE_NAME+"."+CategoryTable.COLUMN_CATEGORY_ID  +"="+ SessionSpeakerTable.TABLE_NAME+"."+ SessionSpeakerTable.COLUMN_SPEAKER_TYPE+")" +
                        " WHERE " +
                        " EXISTS ( " +
                        " SELECT *"+
                        " FROM "+CategoryTable.TABLE_NAME+
                        " WHERE "+CategoryTable.TABLE_NAME+"."+CategoryTable.COLUMN_CATEGORY_ID+" = "+SessionSpeakerTable.TABLE_NAME+"."+SessionSpeakerTable.COLUMN_SPEAKER_TYPE+")";


                database.execSQL(updateQuery);

                return new CursorLoader(context, SessionSpeakerContentProvider.CONTENT_URI, null, whereClause, arguments, null);

            case LoaderIDs.session_timings:

                return new CursorLoader(context, this.getSessionTimingsContentUri(), null, null, null, null);

            case LoaderIDs.exhibitor:

                return new CursorLoader(context, ExhibitorContentProvider.CONTENT_URI, null, null, null, null);

        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        switch (loader.getId())
        {
            case LoaderIDs.category:

                if (data.getCount() <= 0)
                {
                    // No data available yet, abort
                    this.listener.sessionsLoaded(null);
                    break;
                }

                this.categories = new Category[data.getCount()];

                int i = 0;
                while (data.moveToNext())
                {
                    Category category = new Category();
                    category.constructFromCursor(data);
                    this.categories[i] = category;
                    i++;
                }
                this.categoriesLoaded = true;

                loaderManager.restartLoader(LoaderIDs.session_timings, null, this);
                break;

            case LoaderIDs.session_timings:

                if (data.getCount() <= 0)
                {
                    // No data available yet, abort
                    this.listener.sessionsLoaded(null);
                    break;
                }

                this.shouldLoadAllSpeakers = false;
                this.shouldLoadExhibitors = false;
                this.eventTimings = new EventTiming[data.getCount()];

                int n = 0;
                String whereClause = " IN (";
                ArrayList<String> whereArguments = null;

                while (data.moveToNext())
                {

                    EventTiming eventTiming = new EventTiming();
                    eventTiming.constructFromCursor(data);

                    if (eventTiming.getEvent() instanceof Session)
                    {
                        if (this.speakerSessionsMode)
                        {
                            if (whereArguments == null)
                            {
                                whereArguments = new ArrayList<>();
                            }
                            whereClause += "?,";
                            whereArguments.add(Long.toString(eventTiming.getEventId()));
                        }
                        else
                        {
                            this.shouldLoadAllSpeakers = true;
                        }
                    }
                    else if (eventTiming.getEvent() instanceof ExhibitorEvent)
                    {
                        this.shouldLoadExhibitors = true;
                    }

                    this.eventTimings[n] = eventTiming;
                    n++;
                }

                this.eventsLoaded = true;

                if (this.shouldLoadAllSpeakers)
                {
                    loaderManager.restartLoader(LoaderIDs.session_speakers, null, this);
                }
                else if (whereArguments != null && whereArguments.size() > 0)
                {
                    final Bundle args = new Bundle();

                    if (whereClause.endsWith(","))
                    {
                        whereClause = whereClause.substring(0, whereClause.length() - 1);
                        whereClause += ")";
                    }

                    args.putString(EXTRA_SESSION_SPEAKERS_WHERE, whereClause);
                    args.putStringArrayList(EXTRA_SESSION_SPEAKERS_ARGUMENTS, whereArguments);
                    loaderManager.restartLoader(LoaderIDs.session_speakers, args, this);
                }
                else
                {
                    this.speakerLoadingDone = true;
                }

                if (this.shouldLoadExhibitors)
                {
                    loaderManager.restartLoader(LoaderIDs.exhibitor, null, this);
                }
                else
                {
                    this.exhibitorLoadingDone = true;
                }

                if (this.speakerLoadingDone && this.exhibitorLoadingDone)
                {
                    this.mergeDataIfReady();
                }

                break;

            case LoaderIDs.session_speakers:

//                if (this.sessionSpeakersSortedPerSession == null)
//                {
                    this.sessionSpeakersSortedPerSession = new HashMap<>();
//                }

                while (data.moveToNext())
                {
                    SessionSpeaker sessionSpeaker = new SessionSpeaker();
                    sessionSpeaker.constructFromCursor(data);
                    this.registerSessionSpeaker(sessionSpeaker);
                }

                this.speakerLoadingDone = true;

                this.mergeDataIfReady();
                break;

            case LoaderIDs.exhibitor:

                this.exhibitorPerCCVK = new HashMap<>();

                while (data.moveToNext())
                {
                    Exhibitor exhibitor = new Exhibitor();
                    exhibitor.constructFromCursor(data);
                    this.exhibitorPerCCVK.put(exhibitor.getCcvk(), exhibitor);
                }

                this.exhibitorLoadingDone = true;
                this.mergeDataIfReady();
                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        switch (loader.getId())
        {
            case LoaderIDs.category:
            case LoaderIDs.session_speakers:
            case LoaderIDs.exhibitor:
            case LoaderIDs.session_timings:

                this.reset();
                //this.listener.sessionsLoaded(null);
                break;
        }
    }

    private void reset()
    {
        this.speakerSessionsMode = false;
        this.eventsLoaded = false;
        this.categoriesLoaded = false;
        this.speakerLoadingDone = false;
        this.exhibitorLoadingDone = false;

        this.shouldLoadAllSpeakers = false;
        this.shouldLoadExhibitors = false;

        //        this.categories = null;
        this.eventTimings = null;
        this.sessionSpeakersSortedPerSession = null;
        this.exhibitorPerCCVK = null;
    }

    public void destroyLoaders()
    {
        this.loaderManager.destroyLoader(LoaderIDs.category);
        this.loaderManager.destroyLoader(LoaderIDs.session_timings);
        this.loaderManager.destroyLoader(LoaderIDs.session_speakers);
        this.loaderManager.destroyLoader(LoaderIDs.exhibitor);
    }

    public void restartLoaders()
    {
        this.loaderManager.restartLoader(LoaderIDs.category, null, this);
    }

    private boolean mergeDataIfReady()
    {
        if (this.categoriesLoaded && this.eventsLoaded && this.speakerLoadingDone && this.exhibitorLoadingDone)
        {
            if (this.shouldLoadAllSpeakers && this.sessionSpeakersSortedPerSession == null)
            {
                return false;
            }

            if (this.shouldLoadExhibitors && this.exhibitorPerCCVK == null)
            {
                return false;
            }

            SessionAssembler sessionAssembler = new SessionAssembler();
            // Use an executor to allow multiple async tasks to run in parallel
            sessionAssembler.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            return true;
        }

        return false;
    }

    private void onDataLoaded()
    {
        this.listener.sessionsLoaded(this.eventTimings);
        //        SessionLoader.this.reset();
    }

    protected Uri getSessionTimingsContentUri()
    {
        switch (this.uriMode)
        {
            case ALL:
            default:
                if (StringUtils.isEmpty(this.searchText))
                {
                    return EventTimingContentProvider.CONTENT_URI;
                }
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_SEARCH, this.searchText);

            case TIMING:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_TIMING, this.pathAppendix);

            case SPEAKER:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_SPEAKER, this.pathAppendix);

            case FAVOURITES:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_FAVOURITES, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);

            case SUGGESTED:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_SUGGESTED, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);

            case CONFERENCE:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_CONFERENCE, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);

            case TV:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_TV, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);

            case EXHIBITOR:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_EXHIBITOR, StringUtils.isEmpty(this.searchText) ? "" : this.searchText);

            case EVENTS_FOR_EXHIBITOR:
                return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_CCVK, this.pathAppendix);

            case STREAM:
                if (StringUtils.isEmpty(this.searchText))
                {
                    return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_STREAM, this.pathAppendix);
                }
                else
                {
                    String stringUri = EventTimingContentProvider.CONTENT_URI_STREAM_SEARCH.toString();
                    stringUri = stringUri.replace("{0}", this.pathAppendix);
                    return Uri.withAppendedPath(Uri.parse(stringUri), this.searchText);
                }
            case TAG:
            {
                if (StringUtils.isEmpty(this.searchText))
                {
                    return Uri.withAppendedPath(EventTimingContentProvider.CONTENT_URI_TAG, this.pathAppendix);
                }
                else
                {
                    String stringUri = EventTimingContentProvider.CONTENT_URI_TAG_SEARCH.toString();
                    stringUri = stringUri.replace("{0}", this.pathAppendix);
                    return Uri.withAppendedPath(Uri.parse(stringUri), this.searchText);
                }
            }

        }

        //return null;
    }

    /**
     * Creates a hashmap of (sessions, speakers[]) pairs.
     */
    private void registerSessionSpeaker(SessionSpeaker sessionSpeaker)
    {
        String sessionId = "" + sessionSpeaker.getSessionId();

        if (this.sessionSpeakersSortedPerSession == null)
        {
            this.sessionSpeakersSortedPerSession = new HashMap<>();
        }

        ArrayList<SessionSpeaker> speakers = this.sessionSpeakersSortedPerSession.get(sessionId);

        if (speakers == null)
        {
            speakers = new ArrayList<>();
        }

        speakers.add(sessionSpeaker);

        this.sessionSpeakersSortedPerSession.put(sessionId, speakers);
    }


    /**
     * Asynctask that combines all loaded data into a list of EventTimings.
     */
    private class SessionAssembler extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected Void doInBackground(Void... voids)
        {
            if (SessionLoader.this.eventTimings != null)
            {
                for (EventTiming eventTiming : SessionLoader.this.eventTimings)
                {
                    if (eventTiming != null && eventTiming.getEvent() instanceof Session)
                    {
                        SessionLoader.this.constructSession(eventTiming);
                    }
                    else if (eventTiming != null && eventTiming.getEvent() instanceof ExhibitorEvent)
                    {
                        SessionLoader.this.constructExhibitorEvent(eventTiming);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            SessionLoader.this.onDataLoaded();
        }
    }

    private void constructSession(EventTiming eventTiming)
    {
        Session session = (Session) eventTiming.getEvent();

        if (session != null)
        {
            session.constructTagAndStreamFromCursor(this.categories);

            String sessionTimingId = "" + eventTiming.getEventId();

            if (!StringUtils.isEmpty(sessionTimingId))
            {
                if (session.getSessionSpeakers() == null)
                {
                    session.injectSessionSpeakers(this.sessionSpeakersSortedPerSession.get(sessionTimingId));
                }
            }
        }
    }

    private void constructExhibitorEvent(EventTiming eventTiming)
    {
        ExhibitorEvent exhibitorEvent = (ExhibitorEvent) eventTiming.getEvent();


        Exhibitor exhibitor = this.exhibitorPerCCVK.get(exhibitorEvent.getCcvk());

        if (exhibitor != null)
        {
            exhibitorEvent.setExhibitor(exhibitor);
        }
    }
}
