package com.swift.SWIFTRegional.utils.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import com.squareup.picasso.Picasso;
import com.swift.SWIFTRegional.models.PrivacySettings;
import com.swift.SWIFTRegional.models.User;
import com.swift.SWIFTRegional.utils.crypto.EncryptionHelper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.goodcoresoftware.android.common.utils.Log;

import com.swift.SWIFTRegional.models.RegistrationCode;

/**
 * Utility class to retrieve information about logged in user through shared preferences.
 */
public class UserPreferencesHelper
{

    private static final String USER_SALUTATION = "user_salutation";
    private static final String USER_FIRST_NAME = "user_first_name";
    private static final String USER_LAST_NAME = "user_last_name";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_COUNTRY = "user_country";
    private static final String USER_PICTURE_URL = "user_picture_url";
    private static final String USER_SIBOS_KEY = "user_sibos_key";
    private static final String USER_JOB_TITLE = "user_business_function";
    private static final String USER_COMPANY_NAME = "user_company";
    private static final String USER_ROLES = "user_roles";
    private static final String USER_RESET_PASSWORD = "user_reset_password";
    private static final String USER_LINKEDIN_URL = "user_linkedin_url";
    private static final String USER_TWITTER_URL = "user_twitter_url";


    private static final String SELECTED_REGION = "selected_region";
    private static final String SELECTED_TYPE = "selected_type";
    private static final String SELECTED_EVENT = "selected_event";




    private static final String USER_REGISTRATION_CODE = "user_registration_code";

    public static final String PRIVACY_EMAIL = "privacy_email";
    public static final String PRIVACY_COMPANY_PHONE = "privacy_company_phone";
    public static final String PRIVACY_MOBILE_PHONE = "privacy_mobile_phone";
    public static final String PRIVACY_COMPANY_ADDRESS = "privacy_company_address";

    // 2015
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String ACCESS_REFRESH_TOKEN = "ACCESS_REFRESH_TOKEN";
    public static final String ACCESS_CLIENT_SECRET = "ACCESS_CLIENT_SECRET";
    public static final String ACCESS_CLIENT_ID = "ACCESS_CLIENT_ID";

    public static final String USER_SALT = "USER_SALT";
    public static final String USER_CHAT_ID = "USER_CHAT_ID";
    public static final String HAS_UNREAD_MESSAGES = "HAS_UNREAD_MESSAGES";

    public static final String LOGGED_IN = "logged_in";
    private static final String PREFERENCES_NAME = "com.swift.SWIFTRegional.utils.preferences.user";

    @SuppressLint("InlinedApi")
    public static SharedPreferences getPreferences(Context inContext)
    {
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD_MR1)
        {
            return inContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_MULTI_PROCESS);
        }
        else
        {
            return inContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }
    }

    /**
     * Store that no user is currently logged in and clean his privacy settings.
     *
     * @param inContext {@link android.content.Context} to retrieve preferences.
     */
    public static void logout(Context inContext)
    {
        SharedPreferences.Editor editor = getPreferences(inContext).edit();
        editor.putBoolean(LOGGED_IN, false);
        editor.remove(PRIVACY_EMAIL);
        editor.remove(PRIVACY_COMPANY_PHONE);
        editor.remove(PRIVACY_MOBILE_PHONE);
        editor.remove(PRIVACY_COMPANY_ADDRESS);

        editor.remove(ACCESS_TOKEN);
        editor.remove(ACCESS_REFRESH_TOKEN);
        editor.remove(ACCESS_CLIENT_ID);
        editor.remove(ACCESS_CLIENT_SECRET);

        editor.remove(USER_JOB_TITLE);
        editor.remove(USER_REGISTRATION_CODE);
        editor.remove(USER_ROLES);
        editor.remove(USER_FIRST_NAME);
        editor.remove(USER_TWITTER_URL);
        editor.remove(USER_LINKEDIN_URL);
        editor.remove(USER_LAST_NAME);
        editor.remove(USER_PICTURE_URL);
        editor.remove(USER_COMPANY_NAME);
        editor.remove(USER_COUNTRY);
        editor.remove(USER_EMAIL);
        editor.remove(USER_CHAT_ID);
        editor.remove(USER_SIBOS_KEY);

        editor.commit();

        removeTokens(inContext);
    }


    /**
     * @param inContext {@link android.content.Context} to retrieve preferences.
     * @return true if a user is logged in, false if not.
     */
    public static boolean loggedIn(Context inContext)
    {
        SharedPreferences preferences = getPreferences(inContext);
        return preferences.getBoolean(LOGGED_IN, false);
    }

    /**
     * @param inContext {@link android.content.Context} to retrieve preferences.
     * @return {@link User} that is logged in, or an empty {@link User} object if no user is logged in.
     */
    public static User getLoggedInUser(Context inContext)
    {
        User user = new User();
        if (loggedIn(inContext))
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
            user.setSalutation(preferences.getString(USER_SALUTATION, null));
            user.setSibosKey(preferences.getString(USER_SIBOS_KEY, null));
            user.setFirstName(preferences.getString(USER_FIRST_NAME, null));
            user.setLastName(preferences.getString(USER_LAST_NAME, null));
            user.setPictureUrl(preferences.getString(USER_PICTURE_URL, null));
            user.setJobTitle(preferences.getString(USER_JOB_TITLE, null));
            user.setCompanyName(preferences.getString(USER_COMPANY_NAME, null));
            user.setCountry(preferences.getString(USER_COUNTRY, null));
            user.setEmail(preferences.getString(USER_EMAIL, null));
            user.setResetPassword(preferences.getBoolean(USER_RESET_PASSWORD, false));
            user.setTwitterId(preferences.getString(USER_TWITTER_URL,null));
            user.setLinkedinUrl(preferences.getString(USER_LINKEDIN_URL,null));

            Set<String> rolesSet = preferences.getStringSet(USER_ROLES, null);
            if (rolesSet != null)
            {
                user.setRoles(rolesSet.toArray(new String[rolesSet.size()]));
            }
        }

        return user;
    }

    /**
     * Store information about logged in user in preferences.
     *
     * @param inContext {@link android.content.Context} to retrieve preferences.
     * @param inUser    {@link User} to store.
     */
    public static void storeLoggedInUserDetails(Context inContext, User inUser)
    {
        SharedPreferences.Editor editor = getPreferences(inContext).edit();

        editor.putString(USER_SALUTATION, inUser.getSalutation());
        editor.putString(USER_FIRST_NAME, inUser.getFirstName());
        editor.putString(USER_LAST_NAME, inUser.getLastName());
        editor.putString(USER_SIBOS_KEY, inUser.getSibosKey());
        editor.putString(USER_PICTURE_URL, inUser.getPictureUrl());
        editor.putString(USER_JOB_TITLE, inUser.getJobTitle());
        editor.putString(USER_COMPANY_NAME, inUser.getCompanyName());
        editor.putString(USER_COUNTRY, inUser.getCountry());
        editor.putString(USER_EMAIL, inUser.getEmail());
        editor.putBoolean(USER_RESET_PASSWORD, inUser.isResetPassword());
        editor.putStringSet(USER_ROLES, new HashSet<String>(Arrays.asList(inUser.getRoles())));
        editor.putString(USER_LINKEDIN_URL,inUser.getLinkedinUrl());
        editor.putString(USER_TWITTER_URL,inUser.getTwitterId());
        editor.putBoolean(LOGGED_IN, true);
        editor.commit();
    }

    /**
     * Get Sibos key of currently logged in user.
     *
     * @param inContext {@link android.content.Context} to store preferences.
     * @return sibos key of currently logged in user, empty string if no user is logged in.
     */
    public static String getSibosKey(Context inContext)
    {
        if (loggedIn(inContext))
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
            return preferences.getString(USER_SIBOS_KEY, "");
        }
        else
        {
            return null;
        }
    }

    /**
     * Store registration code of currently logged in user.
     *
     * @param inContext          {@link android.content.Context} to store preferences.
     * @param inRegistrationCode {@link RegistrationCode} containing a
     *                           url to a png of the registration code
     */
    public static void storeRegistrationCode(Context inContext, RegistrationCode inRegistrationCode)
    {
        String url = inRegistrationCode.getUrl();

        Picasso.with(inContext).load(url);

        SharedPreferences.Editor editor = getPreferences(inContext).edit();
        editor.putString(USER_REGISTRATION_CODE, url);
        editor.commit();
    }

    public static void registerChangeListener(Context inContext, SharedPreferences.OnSharedPreferenceChangeListener inListener)
    {
        SharedPreferences preferences = getPreferences(inContext);
        preferences.registerOnSharedPreferenceChangeListener(inListener);
    }

    public static void unRegisterChangeListener(Context inContext, SharedPreferences.OnSharedPreferenceChangeListener inListener)
    {
        SharedPreferences preferences = getPreferences(inContext);
        preferences.unregisterOnSharedPreferenceChangeListener(inListener);
    }

    /**
     * Get URL of registration code image.
     *
     * @param inContext {@link android.content.Context} to retrieve preferences.
     * @return
     */
    public static String getRegistrationCodeUrl(Context inContext)
    {
        SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
        return preferences.getString(USER_REGISTRATION_CODE, "");
    }

    /**
     * Store privacy settings in shared preferences.
     *
     * @param inContext         {@link android.content.Context} to retrieve preferences.
     * @param inPrivacySettings
     */
    public static void storePrivacySettings(Context inContext, PrivacySettings inPrivacySettings)
    {
        SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(PRIVACY_EMAIL, inPrivacySettings.isEmail());
        editor.putBoolean(PRIVACY_COMPANY_PHONE, inPrivacySettings.isCompanyPhone());
        editor.putBoolean(PRIVACY_MOBILE_PHONE, inPrivacySettings.isMobilePhone());
        editor.putBoolean(PRIVACY_COMPANY_ADDRESS, inPrivacySettings.isCompanyAddress());
        editor.commit();
    }

    /**
     * Get {@link PrivacySettings} from shared preferences.
     *
     * @param inContext {@link android.content.Context} to retrieve preferences.
     * @return Privacy settings for current user.
     */
    public static PrivacySettings getPrivacySettings(Context inContext)
    {
        PrivacySettings privacySettings = new PrivacySettings();

        SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
        privacySettings.setSibosKey(preferences.getString(USER_SIBOS_KEY, ""));
        privacySettings.setEmail(preferences.getBoolean(PRIVACY_EMAIL, false));
        privacySettings.setCompanyPhone(preferences.getBoolean(PRIVACY_COMPANY_PHONE, false));
        privacySettings.setMobilePhone(preferences.getBoolean(PRIVACY_MOBILE_PHONE, false));
        privacySettings.setCompanyAddress(preferences.getBoolean(PRIVACY_COMPANY_ADDRESS, false));

        return privacySettings;
    }

    public static void storeTokens(Context inContext, String accessToken, String refreshToken, String clientId, String clientSecret)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
            SharedPreferences.Editor editor = preferences.edit();

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            // Store the tokens, encrypted with the device ID
            editor.putString(ACCESS_TOKEN, EncryptionHelper.encrypt(inContext, accessToken, android_id));
            editor.putString(ACCESS_REFRESH_TOKEN, EncryptionHelper.encrypt(inContext, refreshToken, android_id));
            editor.putString(ACCESS_CLIENT_ID, EncryptionHelper.encrypt(inContext, clientId, android_id));
            editor.putString(ACCESS_CLIENT_SECRET, EncryptionHelper.encrypt(inContext, clientSecret, android_id));
            editor.commit();

        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt password " + e.getMessage());
        }
    }

    public  static  String getEventSelection(Context inContext,String returnedInfo)
    {

        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);

           switch (returnedInfo)
           {
               case SELECTED_EVENT:
                  return preferences.getString(SELECTED_EVENT, "0");

               case SELECTED_REGION:
                   return preferences.getString(SELECTED_REGION, "0");

               case SELECTED_TYPE:
                   return preferences.getString(SELECTED_TYPE, "0");

               default:
                   return "";

           }




        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt accessToken " + e.getMessage());
            return null;
        }


    }


    public static  void storeEventSelection(Context inContext,String regionId,String typeId,String eventId)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
            SharedPreferences.Editor editor = preferences.edit();


                editor.putString(SELECTED_REGION, regionId);



                editor.putString(SELECTED_TYPE, typeId);



                editor.putString(SELECTED_EVENT, eventId);


            editor.commit();

        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt event selection " + e.getMessage());
        }
    }

    public static void removeTokens(Context inContext)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
            SharedPreferences.Editor editor = preferences.edit();

            // Store the tokens, encrypted with the device ID
            editor.putString(ACCESS_TOKEN, "");
            editor.putString(ACCESS_REFRESH_TOKEN, "");
            editor.putString(ACCESS_CLIENT_ID, "");
            editor.putString(ACCESS_CLIENT_SECRET, "");
            editor.putString(USER_SALT,"");
            editor.commit();

        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt password " + e.getMessage());
        }
    }

    public static String getAccessToken(Context inContext)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            return EncryptionHelper.decrypt(inContext, preferences.getString(ACCESS_TOKEN, ""), android_id);
        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt accessToken " + e.getMessage());
            return null;
        }
    }

    public static String getRefreshToken(Context inContext)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            return EncryptionHelper.decrypt(inContext, preferences.getString(ACCESS_REFRESH_TOKEN, ""), android_id);
        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt refreshToken " + e.getMessage());
            return null;
        }
    }

    public static String getClientId(Context inContext)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            return EncryptionHelper.decrypt(inContext, preferences.getString(ACCESS_CLIENT_ID, ""), android_id);
        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt clientId " + e.getMessage());
            return null;
        }
    }

    public static String getClientSecret(Context inContext)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            return EncryptionHelper.decrypt(inContext, preferences.getString(ACCESS_CLIENT_SECRET, ""), android_id);
        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt clientSecret " + e.getMessage());
            return null;
        }
    }

    public static void storeUserChatId(Context inContext, int chatId)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
            SharedPreferences.Editor editor = preferences.edit();

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            editor.putString(USER_CHAT_ID, EncryptionHelper.encrypt(inContext, Integer.toString(chatId), android_id));
            editor.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static int getUserChatId(Context inContext)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            return Integer.parseInt(EncryptionHelper.decrypt(inContext, preferences.getString(USER_CHAT_ID, ""), android_id));
        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt user chat Id " + e.getMessage());
            return 0;
        }
    }

    public static void storeSalt(Context inContext, String salt)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
            SharedPreferences.Editor editor = preferences.edit();

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            editor.putString(USER_SALT, EncryptionHelper.encrypt(inContext, salt, android_id));
            editor.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String getSalt(Context inContext)
    {
        try
        {
            SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);

            // Get the device ID
            String android_id = Settings.Secure.getString(inContext.getContentResolver(), Settings.Secure.ANDROID_ID);

            return EncryptionHelper.decrypt(inContext, preferences.getString(USER_SALT, ""), android_id);
        }
        catch (Exception e)
        {
            Log.d(UserPreferencesHelper.class.getSimpleName(), "failed to decrypt chat salt " + e.getMessage());
            return null;
        }
    }

    public static void setUnreadMessages(Context inContext, boolean hasUnreadMessages)
    {
        SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(HAS_UNREAD_MESSAGES, hasUnreadMessages);
        editor.apply();
    }

    public static boolean hasUnreadMessages(Context inContext)
    {
        SharedPreferences preferences = UserPreferencesHelper.getPreferences(inContext);
        return preferences.getBoolean(HAS_UNREAD_MESSAGES, false);
    }

    public static void registerAsListener(Context context, SharedPreferences.OnSharedPreferenceChangeListener listener)
    {
        getPreferences(context).registerOnSharedPreferenceChangeListener(listener);
    }

    public static void unregisterAsListener(Context context, SharedPreferences.OnSharedPreferenceChangeListener listener)
    {
        getPreferences(context).unregisterOnSharedPreferenceChangeListener(listener);
    }

}
