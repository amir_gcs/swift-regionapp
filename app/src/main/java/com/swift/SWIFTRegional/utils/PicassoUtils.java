package com.swift.SWIFTRegional.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Callback;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import picassoutils.CircleTransformation;
import com.swift.SWIFTRegional.BuildConfig;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 30/04/15.
 * Picasso utils that will accept all certificates while in staging mode.
 */
public class PicassoUtils
{
    public static void loadImage(String url, ImageView imageView)
    {
        if (!TextUtils.isEmpty(url))
        {
            Picasso picasso = getPicasso(imageView.getContext());
            picasso.load(url).into(imageView);
        }
        else
        {
            imageView.setImageDrawable(null);
        }
    }

    @SuppressLint("NewApi")
    public static void loadImage(String url, ImageView imageView, int placeholderId, Context context){
        if (!TextUtils.isEmpty(url))
        {
            Picasso picasso = getPicasso(imageView.getContext());
            //picasso.load(url).into(imageView);
            picasso.load(url).error(placeholderId).placeholder(placeholderId).into(imageView);
        }
        else
        {
            imageView.setImageDrawable(context.getDrawable(placeholderId));
        }
    }

    public static void loadImage(String url, ImageView imageView,Callback listener){

        if (!TextUtils.isEmpty(url)){
            Picasso picasso = getPicasso(imageView.getContext());
            picasso.load(url).into(imageView,listener);
        }else{
            imageView.setImageDrawable(null);
        }
    }


    public static void loadPersonImage(String url, ImageView imageView)
    {
        if (!TextUtils.isEmpty(url))
        {
            Picasso
                    .with(imageView.getContext())
                    .load(url)
                    .placeholder(R.drawable.placeholder_profile)
                    .error(R.drawable.placeholder_profile)
                    .transform(new CircleTransformation())
                    .into(imageView);
        }
        else
        {
            Picasso
                    .with(imageView.getContext())
                    .load(R.drawable.placeholder_profile)
                    .placeholder(R.drawable.placeholder_profile)
                    .error(R.drawable.placeholder_profile)
                    .into(imageView);
        }
    }

    /**
     * Adds the correct placeholder and circletransformation to a profile picture.
     */
    public static void unsafeLoadPersonImage(String url, ImageView imageView)
    {
        if (!TextUtils.isEmpty(url))
        {
            getPicasso(imageView.getContext())
                    .load(url)
                    .placeholder(R.drawable.placeholder_profile)
                    .error(R.drawable.placeholder_profile)
                    .transform(new CircleTransformation())
                    .into(imageView);
        }
        else
        {
            getPicasso(imageView.getContext())
                    .load(R.drawable.placeholder_profile)
                    .placeholder(R.drawable.placeholder_profile)
                    .error(R.drawable.placeholder_profile)
                    .into(imageView);
        }
    }

    private static Picasso getPicasso(Context context)
    {
        OkHttpClient okHttpClient;

        if (BuildConfig.FLAVOR.equals("staging"))
        {
            okHttpClient = OkHttpUtils.getUnsafeOkHttpClient();
        }
        else
        {
            okHttpClient = new OkHttpClient();
        }
        OkHttpDownloader downloader = new OkHttpDownloader(okHttpClient);

        return new Picasso.Builder(context).downloader(downloader).build();
    }
}
