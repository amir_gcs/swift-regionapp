package com.swift.SWIFTRegional.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.swift.SWIFTRegional.models.EventTiming;

import java.util.ArrayList;
import java.util.List;

import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;
import com.swift.SWIFTRegional.services.ShowSessionNotificationService;

public class AlarmHelper
{
    // Show notification of upcoming events 15 minutes before they start
    private static final long NOTIFICATION_TIME_OFFSET = 15L * 60L * 1000L;

    public static void setAllSessionAlarms(final Context inContext)
    {
        final Cursor cursorFavouriteSessions = getFavouriteSessions(inContext);
        final List<EventTiming> favouriteSessions = constructEventTimings(cursorFavouriteSessions);
        final List<EventTiming> futureSessions = getFutureSessions(favouriteSessions);

        for (EventTiming session : futureSessions)
        {
            int requestCode = AlarmHelper.eventIdToInt(session.getEventId());
            scheduleNotification(inContext, session, requestCode);
        }
        cursorFavouriteSessions.close();
    }

    public static void cancelAllSessionAlarms(final Context inContext)
    {
        final Cursor cursorFavouriteSessions = getFavouriteSessions(inContext);
        final List<EventTiming> favouriteSessions = constructEventTimings(cursorFavouriteSessions);
        final List<EventTiming> futureSessions = getFutureSessions(favouriteSessions);

        for (EventTiming session : futureSessions)
        {
            int requestCode = AlarmHelper.eventIdToInt(session.getEventId());
            unscheduleNotification(inContext, session, requestCode);
        }
        cursorFavouriteSessions.close();
    }

    private static Cursor getFavouriteSessions(final Context inContext)
    {
        return inContext.getContentResolver().query(EventTimingContentProvider.CONTENT_URI_FAVOURITES, null, null, null, null);
    }

    private static void scheduleNotification(final Context context, final EventTiming session, final int requestCode)
    {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent notificationPendingIntent = createPendingIntent(context, session, requestCode);

        alarmManager.set(AlarmManager.RTC_WAKEUP, session.getStartTimeMillis() - NOTIFICATION_TIME_OFFSET, notificationPendingIntent);
    }

    public static void unscheduleNotification(final Context context, final EventTiming session, final int requestCode)
    {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent notificationPendingIntent = createPendingIntent(context, session, requestCode);

        alarmManager.cancel(notificationPendingIntent);
    }

    private static PendingIntent createPendingIntent(final Context context, EventTiming session, int requestCode)
    {
        Intent notificationIntent = ShowSessionNotificationService.createIntent(context, session);
        return PendingIntent.getService(context, requestCode, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    /**
     * Converts the long session Id to an integer for use as a notification id.
     *
     * @param longEventId The event Id to convert.
     */
    public static int eventIdToInt(long longEventId)
    {
        return (int) longEventId % Integer.MAX_VALUE;
    }

    private static List<EventTiming> constructEventTimings(Cursor inCursor)
    {
        final List<EventTiming> timings = new ArrayList<>();

        while (inCursor.moveToNext())
        {
            final EventTiming eventTiming = new EventTiming();
            eventTiming.constructFromCursor(inCursor);
            timings.add(eventTiming);
        }

        return timings;
    }

    private static List<EventTiming> getFutureSessions(List<EventTiming> eventTimings)
    {
        final List<EventTiming> futureSessions = new ArrayList<>();

        for (EventTiming eventTiming : eventTimings)
        {
            if (eventTiming != null && (eventTiming.getStartTimeMillis() > System.currentTimeMillis()))
            {
                futureSessions.add(eventTiming);
            }
        }

        return futureSessions;
    }
}
