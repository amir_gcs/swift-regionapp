package com.swift.SWIFTRegional.utils;

import android.graphics.Color;

/**
 * Created by SimonRaes on 22/04/15.
 */
public class ColorUtils
{
    private static final float DEFAULT_DARKEN_PERCENTAGE = 0.8f;

    public static int darkenColor(int inColor)
    {
        return darkenColor(inColor, DEFAULT_DARKEN_PERCENTAGE);
    }

    public static int darkenColor(int inColor, float percentage)
    {
        float[] hsv = new float[3];
        Color.colorToHSV(inColor, hsv);
        hsv[2] *= percentage;
        return Color.HSVToColor(hsv);
    }
}
