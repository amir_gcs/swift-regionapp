package com.swift.SWIFTRegional.utils;

import android.content.Context;
import android.hardware.Camera;
import android.view.Surface;
import android.view.WindowManager;

/**
 * Created by SimonRaes on 4/05/15.
 */
public class CameraUtils
{

    public static void setCameraDisplayOrientation(final Context context, final Camera camera)
    {
        if (camera == null)
        {
            return;
        }

        final Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(0, info);

        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int rotation = windowManager.getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation)
        {
            case Surface.ROTATION_0:

                degrees = 0;
                break;

            case Surface.ROTATION_90:

                degrees = 90;
                break;

            case Surface.ROTATION_180:

                degrees = 180;
                break;

            case Surface.ROTATION_270:

                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        }
        else
        {
            // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        Camera.Parameters params = camera.getParameters();
        params.set("orientation", "portrait");
        params.setRotation(result);
        camera.setParameters(params);

        camera.setDisplayOrientation(result);
    }
}
