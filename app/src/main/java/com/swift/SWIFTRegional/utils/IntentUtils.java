package com.swift.SWIFTRegional.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;

public class IntentUtils
{
	/**
	 * Navigates up to the home Activity, destroying all Activities in between,
	 * or creating a new task
	 * 
	 * @param inActivity
	 */
	public static void navigateUpToHome(final Activity inActivity)
	{
		navigateUpToHome(inActivity, null);
	}

    public static void navigateUpToHome(final Activity activity, boolean fade)
    {
        navigateUpToHome(activity, null, fade);
    }
	
	/**
	 * Navigates up to the home Activity, destroying all Activities in between,
	 * or creating a new task
	 * 
	 * @param inActivity
	 * @param inBundle
	 */
	public static void navigateUpToHome(final Activity inActivity, final Bundle inBundle)
	{
		IntentUtils.navigateUpToHome(inActivity, inBundle, false);
	}

    public static void navigateUpToHome(final Activity inActivity, final Bundle inBundle, boolean fade)
    {
        if (inActivity == null)
            return;

        Intent intent = NavUtils.getParentActivityIntent(inActivity);
        if (intent == null)
        {
            intent = new Intent(inActivity, MainActivity.class);
        }

        if (inBundle != null)
        {
            intent.putExtras(inBundle);
        }

        if (!(inActivity instanceof MainActivity)
                && !NavUtils.shouldUpRecreateTask(inActivity, intent))
        {
            NavUtils.navigateUpTo(inActivity, intent);
        }
        else
        {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            inActivity.startActivity(intent);

            if (!(inActivity instanceof MainActivity))
            {
                inActivity.finish();
            }
        }

        if(!fade)
        {
            inActivity.overridePendingTransition(R.anim.anim_window_close_in, R.anim.anim_window_close_out);
        } else
        {
            inActivity.overridePendingTransition(R.anim.anim_window_close_in_fade, R.anim.anim_window_close_out_fade);
        }

    }
}
