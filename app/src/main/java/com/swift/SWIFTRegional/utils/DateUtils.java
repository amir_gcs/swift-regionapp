package com.swift.SWIFTRegional.utils;

import android.content.Context;
import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.goodcoresoftware.android.common.utils.Log;
import com.swift.SWIFTRegional.R;

/**
 * Created by Yves on 17/03/14.
 */
public class DateUtils
{
    private static final SimpleDateFormat sDateFormatTwitter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
    private static final SimpleDateFormat sDateFormatSibos = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
    private static final SimpleDateFormat sDateFormatYoutubeApi = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
    private static final SimpleDateFormat sDateFormatSibosApi = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
    private static final SimpleDateFormat sDateFormatNotification = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

    private static final long MINUTE = 60;
    private static final long HOUR = MINUTE * 60;
    private static final long DAY = HOUR * 24;
    private static final long WEEK = DAY * 7;
    private static final long MONTH = DAY * 31;
    private static final long YEAR = (long) (DAY * 365.25);

    public static String getTimeDifference(Context inContext, long inDate)
    {
        final long currentTime = System.currentTimeMillis();
        long diff = (currentTime - inDate) / 1000;

        final String unit;
        final long time;
        final boolean spacing;

        if (diff < 0)
        {
            // Things in the future
            time = 0;
            unit = inContext.getString(R.string.timeago_seconds);
            spacing = false;

        }
        else if (diff < MINUTE)
        {
            time = diff;
            unit = inContext.getString(R.string.timeago_seconds);
            spacing = false;

        }
        else if (diff < HOUR)
        {
            time = diff / MINUTE;
            unit = inContext.getString(R.string.timeago_minutes);
            spacing = false;

        }
        else if (diff < DAY)
        {
            time = diff / HOUR;
            unit = inContext.getString(R.string.timeago_hours);
            spacing = false;

        }
        else if (diff < WEEK)
        {
            time = diff / DAY;
            unit = inContext.getString(R.string.timeago_days);
            spacing = false;

        }
        else if (diff < MONTH)
        {
            time = diff / WEEK;
            unit = inContext.getString(R.string.timeago_weeks);
            spacing = false;

        }
        else if (diff < YEAR)
        {
            time = diff / MONTH;
            if (time == 1)
            {
                unit = inContext.getString(R.string.timeago_month);
            }
            else
            {
                unit = inContext.getString(R.string.timeago_months);
            }
            spacing = true;

        }
        else
        {
            time = diff / YEAR;
            if (time == 1)
            {
                unit = inContext.getString(R.string.timeago_year);
            }
            else
            {
                unit = inContext.getString(R.string.timeago_years);
            }
            spacing = true;
        }

        final StringBuilder sb = new StringBuilder(String.valueOf(time));
        if (spacing)
        {
            sb.append(" ");
        }

        sb.append(unit);

        return sb.toString();
    }

    public static long getTwitterTimestamp(String inDate)
    {
        try
        {
            final Date date = sDateFormatTwitter.parse(inDate);

            return date.getTime();

        }
        catch (ParseException e)
        {
            Log.d(DateUtils.class.getSimpleName(), e.getMessage());
        }

        return 0;
    }

    public static long getSibosTimestamp(String inDate)
    {
        try
        {
            final Date date = sDateFormatSibos.parse(inDate);

            return date.getTime();
        }
        catch (ParseException e)
        {
            Log.d(DateUtils.class.getSimpleName(), e.getMessage());
        }

        return 0;
    }


    public static String formattedDateFromString(String inputFormat, String outputFormat, String inputDate){
        if(inputFormat.equals("")){ // if inputFormat = "", set a default input format.
            inputFormat = "yyyy-MM-dd hh:mm:ss";
        }
        if(outputFormat.equals("")){
            outputFormat = "dd/MM/yyyy"; // if inputFormat = "", set a default output format.
        }
        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        // You can set a different Locale, This example set a locale of Country Mexico.
        //SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, new Locale("es", "MX"));
        //SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, new Locale("es", "MX"));

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (Exception e) {
            Log.e("formattedDateFromString", "Exception in formateDateFromstring(): " + e.getMessage());
        }
        return outputDate;

    }

    public static long getYouTubeApiTimestamp(String inDate)
    {
        try
        {
            final Date date = sDateFormatYoutubeApi.parse(inDate);

            return date.getTime();
        }
        catch (ParseException e)
        {
            Log.d(DateUtils.class.getSimpleName(), e.getMessage());
        }

        return 0;
    }

    public static Date iso8601StringToDate(String iso8601STring)
    {
        Date date = null;

        try
        {
            java.text.DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            date = df1.parse(iso8601STring);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            date = calendar.getTime();
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * @param iso8601String Example: "2012-10-29T12:00:00"
     * @return A string in the format "Monday 29 October"
     */

    public static String prettyDayString(String iso8601String)
    {
        Date date = DateUtils.iso8601StringToDate(iso8601String);
        if (date != null)
        {
            return prettyDayString(date);
        }
        else
        {
            return "";
        }
    }

    public static String iso8601StringToComparableDateString(String iso8601String){
        Date date = DateUtils.iso8601StringToDate(iso8601String);
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
            return formatter.format(date);
        }
        return "";
    }

    public static String prettyDayString(Date date)
    {
        String dateString = "";
        if (date != null)
        {
            SimpleDateFormat formatter = new SimpleDateFormat("EEEE d MMMM", Locale.ENGLISH);
            dateString = formatter.format(date);
        }

        return dateString;
    }

    public static String prettyTimeString(Context context, String iso8601String)
    {
        String time = "";
        Date result1 = DateUtils.iso8601StringToDate(iso8601String);

        if (result1 != null)
        {
            java.text.DateFormat formatter = DateFormat.getTimeFormat(context);
            time = formatter.format(result1);
        }

        return time;
    }

    public static String extractDayFromIso8601String(String iso8601String)
    {
        String day = "";
        Date result1 = DateUtils.iso8601StringToDate(iso8601String);

        if (result1 != null)
        {
            SimpleDateFormat formatter = new SimpleDateFormat("d", Locale.ENGLISH);
            day = formatter.format(result1);
        }

        return day;
    }

    public static String extractShortMonthStringFromIso8601String(String iso8601String)
    {
        String month = "";
        Date result1 = DateUtils.iso8601StringToDate(iso8601String);

        if (result1 != null)
        {
            SimpleDateFormat formatter = new SimpleDateFormat("MMM", Locale.ENGLISH);
            month = formatter.format(result1);
        }

        return month;
    }

    public static boolean iso8601StringHasStarted(String iso8601String)
    {
        Date dateObj = DateUtils.iso8601StringToDate(iso8601String);

        return dateObj != null && dateObj.before(new Date());
    }

    public static String unixTimeToPrettyTime(long longTime)
    {
        try
        {
            Date date = new Date(longTime * 1000);

            SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.ENGLISH);
            //formatter.setTimeZone(TimeZone.getTimeZone("Singapore"));

            return formatter.format(date);
        }
        catch (NumberFormatException ex)
        {
            return "";
        }
    }

    public static String unixTimeToTimeStamp(final long longTime)
    {
        return sDateFormatNotification.format(longTime);
    }
}
