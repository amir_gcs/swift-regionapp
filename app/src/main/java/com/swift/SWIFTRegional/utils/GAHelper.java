package com.swift.SWIFTRegional.utils;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.swift.SWIFTRegional.application.SibosApplication;
import com.swift.SWIFTRegional.models.User;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 4/06/15.
 */
public class GAHelper
{
    private static final int DIMENSION_ROLE = 1;
    private static final int DIMENSION_SIBOS_KEY = 2;

    private static User user;

    private static Tracker getTracker(Context context)
    {
        try
        {
            if(user == null)
            {
                user = UserPreferencesHelper.getLoggedInUser(context);
            }

            Tracker t = GoogleAnalytics.getInstance(context).newTracker(R.xml.analytics);
            t.enableAdvertisingIdCollection(true);

            if (UserPreferencesHelper.loggedIn(context))
            {
                t.set("&uid", user.getSibosKey());
            }

            UserPreferencesHelper.getLoggedInUser(context).getRoles();

            return t;
        }
        catch (Exception e)
        {
            return  null;
        }

    }

    public static void trackScreen(Context context, String screenName)
    {
        if (context != null)
        {
            final Tracker t = getTracker(context);

            if(t!=null)
            {
                // Set screen name.
                t.setScreenName(screenName);

                // Send a screen view.
                final HitBuilders.AppViewBuilder appViewBuilder = new HitBuilders.AppViewBuilder();

                if (((SibosApplication) context.getApplicationContext()).isLoggedIn())
                {
                    appViewBuilder.setCustomDimension(DIMENSION_ROLE, (user.getRoles() != null && user.getRoles().length > 0) ? user.getRoles()[0] : "");
                    appViewBuilder.setCustomDimension(DIMENSION_SIBOS_KEY, user.getSibosKey());
                }

                t.send(appViewBuilder.build());
            }

        }
    }

    public static void trackEvent(Context context, String category, String action, String label)
    {
        if (context != null)
        {
            final Tracker t = getTracker(context);
            if(t!=null) {
                final HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();

                eventBuilder.setCategory(category).setAction(action);

                if (label != null) {
                    eventBuilder.setLabel(label);
                }

                if (((SibosApplication) context.getApplicationContext()).isLoggedIn()) {
                    eventBuilder.setCustomDimension(DIMENSION_ROLE, (user.getRoles() != null && user.getRoles().length > 0) ? user.getRoles()[0] : "");
                    eventBuilder.setCustomDimension(DIMENSION_SIBOS_KEY, user.getSibosKey());
                }

                t.send(eventBuilder.build());
            }
        }
    }
}