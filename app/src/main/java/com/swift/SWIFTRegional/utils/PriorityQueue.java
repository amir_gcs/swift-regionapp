package com.swift.SWIFTRegional.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class PriorityQueue<T>
{
    private LinkedList<String> priority = new LinkedList<String>();

    private HashMap<String, ConcurrentLinkedQueue<T>> queueMap = new HashMap<String, ConcurrentLinkedQueue<T>>();

    public synchronized T poll()
    {
        if (this.queueMap.isEmpty() || this.priority.isEmpty())
        {
            return null;
        }

        // go through the queues in order of priority
        for (String queueName : this.priority)
        {
            if (!this.queueMap.containsKey(queueName))
            {
                continue;
            }

            final ConcurrentLinkedQueue<T> queue = this.queueMap.get(queueName);

            final T nextItem = queue.poll();
            if (nextItem != null)
            {
                return nextItem;
            }
        }

        return null;
    }

    public synchronized void addItem(T inItem, String inQueueName)
    {

        if (inItem == null || inQueueName == null)
        {
            return;
        }

        if (!this.queueMap.containsKey(inQueueName))
        {
            this.queueMap.put(inQueueName, new ConcurrentLinkedQueue<T>());
        }

        if (!this.alreadyInQueue(inItem, inQueueName))
        {
            this.queueMap.get(inQueueName).add(inItem);
        }

        if (!this.priority.contains(inQueueName))
        {
            this.priority.add(inQueueName);
        }
    }

    private boolean alreadyInQueue(T inItem, String inQueueName)
    {
        final ConcurrentLinkedQueue<T> queue = this.queueMap.get(inQueueName);

        return queue.contains(inItem);
    }

    protected synchronized void setPriority(String inQueueName)
    {

        this.priority.remove(inQueueName);
        if (!this.priority.contains(inQueueName))
        {
            this.priority.addFirst(inQueueName);
        }
    }

    public void clear()
    {
        this.priority.clear();
        this.queueMap.clear();
    }

    public boolean isEmpty()
    {
        return this.getSize() == 0;
    }

    public int getSize()
    {
        int size = 0;
        for (ConcurrentLinkedQueue<T> queue : queueMap.values())
        {
            size += queue.size();
        }

        return size;
    }
}
