package com.swift.SWIFTRegional.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.ContactsContract;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import com.swift.SWIFTRegional.R;

public class AddressBookHelper
{
    public static class Contact
    {
        public String name;
        public String jobTitle;
        public String company;
        public String email;
        public String phone;
        public String mobilePhone;
        public Address address;
        public Bitmap bitmap;

        public static class Address
        {
            public String postalCode;
            public String street;
            public String city;
            public String country;
        }
    }

    public static void addContact(Context inContext, Contact inContact)
    {
        if (inContext == null || inContact == null)
            throw new NullPointerException("inContext and inContact must be non-null");

        // Creates a new intent for sending to the device's contacts application
        Intent insertIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
        insertIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Sets the MIME type to the one expected by the insertion activity
        insertIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        // Sets the new contact name
        insertIntent.putExtra(ContactsContract.Intents.Insert.NAME, inContact.name);

        // Sets the new company and job title
        insertIntent.putExtra(ContactsContract.Intents.Insert.COMPANY, inContact.company);
        insertIntent.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, inContact.jobTitle);

        ArrayList<ContentValues> contactData = new ArrayList<ContentValues>();


        /*
         * Sets up the phone number data row
         */

        // Sets up the row as a ContentValues object
        ContentValues phoneRow = new ContentValues();

        // Specifies the MIME type for this data row (all data rows must be marked by their type)
        phoneRow.put(
                ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
        );

        // Adds the phone type
        phoneRow.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);

        // Adds the phone number and its type to the row
        phoneRow.put(ContactsContract.CommonDataKinds.Phone.NUMBER, inContact.phone);

        // Adds the row to the array
        contactData.add(phoneRow);

         /*
         * Sets up the phone number data row
         */

        // Sets up the row as a ContentValues object
        ContentValues mobilePhoneRow = new ContentValues();

        // Specifies the MIME type for this data row (all data rows must be marked by their type)
        mobilePhoneRow.put(
                ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
        );

        // Adds the phone type
        mobilePhoneRow.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);

        // Adds the phone number and its type to the row
        mobilePhoneRow.put(ContactsContract.CommonDataKinds.Phone.NUMBER, inContact.mobilePhone);

        // Adds the row to the array
        contactData.add(mobilePhoneRow);

        /*
         * Sets up the email data row
         */

        // Sets up the row as a ContentValues object
        ContentValues emailRow = new ContentValues();

        // Specifies the MIME type for this data row (all data rows must be marked by their type)
        emailRow.put(
                ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE
        );

        // Adds the email type
        emailRow.put(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);

        // Adds the email address and its type to the row
        emailRow.put(ContactsContract.CommonDataKinds.Email.ADDRESS, inContact.email);

        // Adds the row to the array
        contactData.add(emailRow);

        if (inContact.address != null)
        {
            insertIntent.putExtra(ContactsContract.Intents.Insert.POSTAL, AddressFormatter.format(inContact.address));
            insertIntent.putExtra(ContactsContract.Intents.Insert.POSTAL_TYPE, ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
        }


        // Adding picture in contact
        Bitmap bmImage = inContact.bitmap;
        //Bitmap bmImage = BitmapFactory.decodeResource(inContext.getResources(), R.drawable.session_detail_locate);
        //Bitmap bmImage = BitmapFactory.decodeFile(inContext.getResources(R.drawable.blue_button_background));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] photoByteArray = baos.toByteArray();

        ContentValues profilePictureValues = new ContentValues();
        //values.put(ContactsContract.Data.RAW_CONTACT_ID, contactId);
        profilePictureValues.put(ContactsContract.Data.IS_SUPER_PRIMARY, 1);
        profilePictureValues.put(ContactsContract.CommonDataKinds.Photo.PHOTO, photoByteArray);
        profilePictureValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE );

        contactData.add(profilePictureValues);


        /*
         * Adds the array to the intent's extras. It must be a parcelable object in order to
         * travel between processes. The device's contacts app expects its key to be
         * Intents.Insert.DATA
         */
        insertIntent.putParcelableArrayListExtra(ContactsContract.Intents.Insert.DATA, contactData);

        // Send out the intent to start the device's contacts app in its add contact activity.
        inContext.startActivity(insertIntent);
    }

    public static void updateContact(Context inContext, Contact inContact){

        // Creates a new Intent to insert or edit a contact
        Intent intentInsertEdit = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        // Sets the MIME type
        intentInsertEdit.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        // Add code here to insert extended data, if desired


        // Sets the new contact name
        intentInsertEdit.putExtra(ContactsContract.Intents.Insert.NAME, inContact.name);

        // Sets the new company and job title
        intentInsertEdit.putExtra(ContactsContract.Intents.Insert.COMPANY, inContact.company);
        intentInsertEdit.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, inContact.jobTitle);

        ArrayList<ContentValues> contactData = new ArrayList<ContentValues>();


        /*
         * Sets up the phone number data row
         */

        // Sets up the row as a ContentValues object
        ContentValues phoneRow = new ContentValues();

        // Specifies the MIME type for this data row (all data rows must be marked by their type)
        phoneRow.put(
                ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
        );

        // Adds the phone type
        phoneRow.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);

        // Adds the phone number and its type to the row
        phoneRow.put(ContactsContract.CommonDataKinds.Phone.NUMBER, inContact.phone);

        // Adds the row to the array
        contactData.add(phoneRow);

         /*
         * Sets up the phone number data row
         */

        // Sets up the row as a ContentValues object
        ContentValues mobilePhoneRow = new ContentValues();

        // Specifies the MIME type for this data row (all data rows must be marked by their type)
        mobilePhoneRow.put(
                ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
        );

        // Adds the phone type
        mobilePhoneRow.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);

        // Adds the phone number and its type to the row
        mobilePhoneRow.put(ContactsContract.CommonDataKinds.Phone.NUMBER, inContact.mobilePhone);

        // Adds the row to the array
        contactData.add(mobilePhoneRow);

        /*
         * Sets up the email data row
         */

        // Sets up the row as a ContentValues object
        ContentValues emailRow = new ContentValues();

        // Specifies the MIME type for this data row (all data rows must be marked by their type)
        emailRow.put(
                ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE
        );

        // Adds the email type
        emailRow.put(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);

        // Adds the email address and its type to the row
        emailRow.put(ContactsContract.CommonDataKinds.Email.ADDRESS, inContact.email);

        // Adds the row to the array
        contactData.add(emailRow);

        if (inContact.address != null)
        {
            intentInsertEdit.putExtra(ContactsContract.Intents.Insert.POSTAL, AddressFormatter.format(inContact.address));
            intentInsertEdit.putExtra(ContactsContract.Intents.Insert.POSTAL_TYPE, ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
        }


        // Adding picture in contact
        Bitmap bmImage = inContact.bitmap;
        //Bitmap bmImage = BitmapFactory.decodeResource(inContext.getResources(), R.drawable.session_detail_locate);
        //Bitmap bmImage = BitmapFactory.decodeFile(inContext.getResources(R.drawable.blue_button_background));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] photoByteArray = baos.toByteArray();

        ContentValues profilePictureValues = new ContentValues();
        //values.put(ContactsContract.Data.RAW_CONTACT_ID, contactId);
        profilePictureValues.put(ContactsContract.Data.IS_SUPER_PRIMARY, 1);
        profilePictureValues.put(ContactsContract.CommonDataKinds.Photo.PHOTO, photoByteArray);
        profilePictureValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE );

        contactData.add(profilePictureValues);

        /*
         * Adds the array to the intent's extras. It must be a parcelable object in order to
         * travel between processes. The device's contacts app expects its key to be
         * Intents.Insert.DATA
         */
        intentInsertEdit.putParcelableArrayListExtra(ContactsContract.Intents.Insert.DATA, contactData);


        // Sends the Intent with an request ID
        inContext.startActivity(intentInsertEdit);


    }

}