package com.swift.SWIFTRegional.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import com.goodcoresoftware.android.common.utils.Connectivity;
import com.swift.SWIFTRegional.R;

public class ApiErrorHelper
{

    /**
     * Shows a toast containing a error message for the user when the API call
     * has finished with error.
     *
     * If the device has no internet connection, the message will state this.
     * Otherwise the message will state that the data could not be fetched.
     * @param inContext context object
     */
    public static void showError(Context inContext)
    {

        if (Connectivity.hasConnection(inContext))
        {
            showToast(inContext, R.string.api_error, Toast.LENGTH_SHORT);

        } else
        {
            showToast(inContext, R.string.api_error_no_connection, Toast.LENGTH_SHORT);
        }
    }

    /**
     * Show toast messages for a defined duration.
     *
     * @param inContext  context object
     * @param inText     id of string resource to be displayed.
     * @param inDuration length that messageThreads should be shown
     */
    protected static void showToast(Context inContext, int inText, int inDuration)
    {
        if (inContext != null)
        {
            final Toast toast = Toast.makeText(inContext, inText, inDuration);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

}
