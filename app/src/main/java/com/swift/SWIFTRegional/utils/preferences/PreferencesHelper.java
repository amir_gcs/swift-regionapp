package com.swift.SWIFTRegional.utils.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * Created by Wesley on 19/05/14.
 */
public class PreferencesHelper
{
    private static final String PREFERENCES_NAME = "sibosPrefs";

    private static final String PREF_FIRST_RUN = "firstRun";

    private static final String PREF_TWITTER_TOKEN = "twitterToken";
    private static final String PREF_FLICKR_ACCOUNT_ID = "flickrAccountId";
    private static final String PREF_TAGS = "tags";

    @SuppressLint("InlinedApi")
    private static SharedPreferences getPreferences(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD_MR1)
        {
            return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_MULTI_PROCESS);
        }
        else
        {
            return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }
    }

    public static boolean isFirstRun(Context context)
    {
        return PreferencesHelper.getPreferences(context).getBoolean(PREF_FIRST_RUN, true);
    }

    public static void setFirstRun(Context context)
    {
        final SharedPreferences.Editor prefs = PreferencesHelper.getPreferences(context).edit();

        prefs.putBoolean(PREF_FIRST_RUN, false);

        prefs.commit();
    }

    public static final boolean containsFlickrAccountId(Context context)
    {
        return PreferencesHelper.getPreferences(context).contains(PREF_FLICKR_ACCOUNT_ID);
    }

    public static final void saveFlickrAccountId(Context context, String accountID)
    {
        final SharedPreferences.Editor prefs = PreferencesHelper.getPreferences(context).edit();

        prefs.putString(PREF_FLICKR_ACCOUNT_ID, accountID);

        prefs.commit();
    }

    public static final String getFlickrAccountId(Context context)
    {
        return PreferencesHelper.getPreferences(context).getString(PREF_FLICKR_ACCOUNT_ID, "");
    }

    public static final void saveTwitterToken(Context context, String token)
    {
        final SharedPreferences.Editor prefs = PreferencesHelper.getPreferences(context).edit();

        prefs.putString(PREF_TWITTER_TOKEN, token);

        prefs.commit();
    }

    public static final String getTwitterToken(Context context)
    {
        return PreferencesHelper.getPreferences(context).getString(PREF_TWITTER_TOKEN, "");
    }

    public static final boolean containsTwitterToken(Context context)
    {
        return PreferencesHelper.getPreferences(context).contains(PREF_TWITTER_TOKEN);
    }

    public static final void removeTwitterToken(Context context)
    {
        final SharedPreferences.Editor prefs = PreferencesHelper.getPreferences(context).edit();

        prefs.remove(PREF_TWITTER_TOKEN);

        prefs.commit();
    }

    public static void saveTags(Context context, Set<String> tags)
    {
        final SharedPreferences.Editor prefs = PreferencesHelper.getPreferences(context).edit();

        prefs.putStringSet(PREF_TAGS, tags);

        prefs.commit();
    }

    public static Set<String> getTags(Context context)
    {
        return PreferencesHelper.getPreferences(context).getStringSet(PREF_TAGS, null);
    }
}
