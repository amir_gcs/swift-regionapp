package com.swift.SWIFTRegional.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import java.net.URLEncoder;
import java.util.List;

import com.goodcoresoftware.android.common.utils.Log;

/**
 * Created by Yves on 17/03/14.
 */
public class TwitterUtils
{
    public static void showTweet(Context inContext, String inTweetId, String inTweetUsername)
    {
        if (inContext == null || inTweetId == null)
        {
            return;
        }

        // old twitter app
        Intent tweetIntent = new Intent(Intent.ACTION_VIEW);
        tweetIntent.setData(Uri.parse("http://twitter.com/itp/status/" + inTweetId));

        PackageManager packageManager = inContext.getPackageManager();
        List<ResolveInfo> intentActivities = packageManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;

        for (ResolveInfo resolveInfo : intentActivities)
        {
            if (resolveInfo.activityInfo.name.equals("com.twitter.android.TweetActivity"))
            {
                final ActivityInfo activity = resolveInfo.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                tweetIntent = new Intent(Intent.ACTION_VIEW);
                tweetIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                tweetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                tweetIntent.setComponent(name);
                tweetIntent.setData(Uri.parse("http://twitter.com/itp/status/" + inTweetId));

                inContext.startActivity(tweetIntent);
                resolved = true;

                break;
            }
        }

        // new twitter app
        tweetIntent = new Intent(Intent.ACTION_VIEW);
        tweetIntent.setData(Uri.parse("twitter://status/?status_id=" + inTweetId));

        intentActivities = packageManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        for (ResolveInfo resolveInfo : intentActivities)
        {
            if (resolveInfo.activityInfo.name.equals("com.twitter.android.TweetActivity"))
            {
                final ActivityInfo activity = resolveInfo.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                tweetIntent = new Intent(Intent.ACTION_VIEW);
                tweetIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                tweetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                tweetIntent.setComponent(name);
                tweetIntent.setData(Uri.parse("twitter://status/?status_id=" + inTweetId));

                inContext.startActivity(tweetIntent);
                resolved = true;

                break;
            }
        }

        if (!resolved)
        {
            try
            {
                String url = "https://twitter.com/" + inTweetUsername + "/statuses/" + inTweetId;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                inContext.startActivity(i);
            } catch (Exception e)
            {
                Log.d("Twitter intent", e.getMessage());
            }
        }
    }

    private static Intent getTwitterPostIntent(Context inContext)
    {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.setType("text/plain");

        final List<ResolveInfo> intentActivities = inContext.getPackageManager().queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;

        final String wantedActivityPackageName = "com.twitter.android";
        final String wantedActivityPackageNameNew = "com.twitter.applib";
        for (ResolveInfo resolveInfo : intentActivities)
        {
            if (resolveInfo.activityInfo.packageName.startsWith(wantedActivityPackageName) || resolveInfo.activityInfo.packageName.startsWith(wantedActivityPackageNameNew))
            {
                final ActivityInfo activity = resolveInfo.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                tweetIntent = new Intent(Intent.ACTION_SEND);
                tweetIntent.setComponent(name);
                tweetIntent.setType("text/plain");

                resolved = true;

                break;
            }
        }

        if (resolved)
        {
            return tweetIntent;
        }

        return null;
    }

    public static void tweet(Context inContext, String inText)
    {
        if (inContext == null || inText == null)
        {
            return;
        }

        Intent tweetIntent = getTwitterPostIntent(inContext);

        if (tweetIntent != null)
        {
            tweetIntent.putExtra(Intent.EXTRA_TEXT, inText);
            inContext.startActivity(tweetIntent);
        }
        else
        {
            try
            {
                String url = "https://twitter.com/intent/tweet?text=" + URLEncoder.encode(inText, "UTF-8");
                inContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
            catch (Exception e)
            {
                Log.e(TwitterUtils.class.getSimpleName(), e.getMessage(), e);
            }
        }
    }
}
