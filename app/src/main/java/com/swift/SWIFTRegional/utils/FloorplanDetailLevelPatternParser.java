package com.swift.SWIFTRegional.utils;

import com.qozix.tileview.detail.DetailLevelPatternParser;

public class FloorplanDetailLevelPatternParser implements DetailLevelPatternParser
{
    private int detailLevel;

    public FloorplanDetailLevelPatternParser(int detailLevel)
    {
        this.detailLevel = detailLevel;
    }

    @Override
    public String parse(String pattern, int row, int column)
    {
        return pattern.replace("%detail_level%", Integer.toString(this.detailLevel)).replace( "%col%", Integer.toString( column ) ).replace( "%row%", Integer.toString( row ) );
    }
}
