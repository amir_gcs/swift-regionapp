package com.swift.SWIFTRegional.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import java.text.Normalizer;

import com.swift.SWIFTRegional.R;

/**
 * Created by wesley on 30/06/14.
 */
public class SibosUtils
{

    private static Boolean isDimensionsInitialized = false;
    private static Boolean is7InchTablet = false;
    private static Boolean is10InchTablet = false;

    public static String getNormalizedText(String text)
    {
        return Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }



    public static boolean isGreaterThan600DP(Context context) {
        if(!isDimensionsInitialized) {
            is7InchTablet = context.getResources().getBoolean(R.bool.is_7inch_tablet);
            is10InchTablet = context.getResources().getBoolean(R.bool.is_10inch_tablet);
        }

        return is7InchTablet || is10InchTablet;
    }

    public static String getNullSafeString(String value)
    {
        if(TextUtils.isEmpty(value))
        {
            return  "";
        }
        else
        {
            return value;
        }
    }

    public static <T> int getNullSafeLength(T[] array){
        return array == null ? 0 : array.length;
    }

    public static void gcLog(String message) {
        Log.d("gclog", "gclog | " + message);
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        return width;
    }

}
