package com.swift.SWIFTRegional.utils;

/**
 * Created by SimonRaes on 4/05/15.
 */
public class QrCodeUtils
{
    // This validator is based on the assumption that all codes will follow this same pattern: 9b3722ea-3375-4dd6-98f9-9717fef8081d!
    public static boolean isQrCodeValid(String text)
    {
        final String[] stringObjects = text.split("-");

        return stringObjects.length == 5 && (stringObjects[0].length() == 8 &&
                stringObjects[1].length() == 4 &&
                stringObjects[2].length() == 4 &&
                stringObjects[3].length() == 4 &&
                stringObjects[4].length() == 12);
    }
}
