package com.swift.SWIFTRegional.utils;

import android.app.Activity;

import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 20/06/15.
 */
public class TabletUtils
{
    public static void setWindowBackground(Activity activity)
    {
        if (activity != null)
        {
            if (activity.getResources().getBoolean(R.bool.is_10inch_tablet) || activity.getResources().getBoolean(R.bool.is_7inch_tablet))
            {
                activity.getWindow().getDecorView().setBackgroundColor(activity.getResources().getColor(R.color.search_gray));
            }
        }
    }
}
