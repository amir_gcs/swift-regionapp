package com.swift.SWIFTRegional.utils.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Amir on 27-Nov-17.
 */

public class ModulePreferenceHelper {

    private static final String PREFERENCES_NAME = "PrefsModule";

    public static final String MODULE_DATA_LOADED_PROGRAMME_SESSION = "ModuleLoadedProgrammeSession";
    public static final String MODULE_DATA_LOADED_PROGRAMME_SPEAKER = "ModuleLoadedProgrammeSpeaker";
    public static final String MODULE_DATA_LOADED_SOCIAL_TWITTER = "ModuleLoadedSocialTwitter";
    public static final String MODULE_DATA_LOADED_SOCIAL_TWITTER_HASHTAG = "ModuleLoadedTwitterHashtag";
    public static final String MODULE_DATA_LOADED_SOCIAL_VIDEO = "ModuleLoadedSocialVideo";
    public static final String MODULE_DATA_LOADED_SPONSOR = "ModuleLoadedSponsor";
    public static final String MODULE_DATA_LOADED_PRACTICAL_INFO = "ModuleLoadedPracticalInfo";
    public static final String MODULE_DATA_LOADED_SOCIAL_INFO_TWITTER = "ModuleLoadedSocialInfoTwitter";
    public static final String MODULE_DATA_LOADED_SOCIAL_INFO_VIDEO = "ModuleLoadedSocialInfoVideo";

    public static final String MODULE_DATA_AVAILABLE_PROGRAMME_SESSION = "ModuleAvailableProgrammeSession";
    public static final String MODULE_DATA_AVAILABLE_PROGRAMME_SPEAKER = "ModuleAvailableLoadedProgrammeSpeaker";
    public static final String MODULE_DATA_AVAILABLE_SOCIAL_TWITTER = "ModuleAvailableSocialTwitter";
    public static final String MODULE_DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG = "ModuleAvailableSocialTwitterHashtag";
    public static final String MODULE_DATA_AVAILABLE_SOCIAL_VIDEO = "ModuleAvailableSocialVideo";
    public static final String MODULE_DATA_AVAILABLE_SPONSOR = "ModuleAvailableSponsor";
    public static final String MODULE_DATA_AVAILABLE_PRACTICAL_INFO = "ModuleAvailablePracticalInfo";
    public static final String MODULE_DATA_AVAILABLE_SOCIAL_INFO_TWITTER = "ModuleAvailableSocialInfoTwitter";
    public static final String MODULE_DATA_AVAILABLE_SOCIAL_INFO_VIDEO = "ModuleAvailableSocialInfoVideo";

    public enum ModuleDataLoaded
    {
        DATA_LOADED_PROGRAMME_SESSION,
        DATA_LOADED_PROGRAMME_SPEAKER,
        DATA_LOADED_SOCIAL_TWITTER,
        DATA_LOADED_SOCIAL_TWITTER_HASHTAG,
        DATA_LOADED_SOCIAL_VIDEO,
        DATA_LOADED_SPONSOR,
        DATA_LOADED_PRACTICAL_INFO,
        DATA_LOADED_SOCIAL_INFO_TWITTER,
        DATA_LOADED_SOCIAL_INFO_VIDEO
    }

    public enum ModuleDataAvailable
    {
        DATA_AVAILABLE_PROGRAMME_SESSION,
        DATA_AVAILABLE_PROGRAMME_SPEAKER,
        DATA_AVAILABLE_SOCIAL_TWITTER,
        DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG,
        DATA_AVAILABLE_SOCIAL_VIDEO,
        DATA_AVAILABLE_SPONSOR,
        DATA_AVAILABLE_PRACTICAL_INFO,
        DATA_AVAILABLE_SOCIAL_INFO_TWITTER,
        DATA_AVAILABLE_SOCIAL_INFO_VIDEO
    }

    @SuppressLint("InlinedApi")
    public static SharedPreferences getPreferences(Context inContext)
    {
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD_MR1)
        {
            return inContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_MULTI_PROCESS);
        }
        else
        {
            return inContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }
    }

    public static void registerChangeListener(Context inContext, SharedPreferences.OnSharedPreferenceChangeListener inListener)
    {
        SharedPreferences preferences = getPreferences(inContext);
        preferences.registerOnSharedPreferenceChangeListener(inListener);
    }

    public static void unRegisterChangeListener(Context inContext, SharedPreferences.OnSharedPreferenceChangeListener inListener)
    {
        SharedPreferences preferences = getPreferences(inContext);
        preferences.unregisterOnSharedPreferenceChangeListener(inListener);
    }

    private static final String getKey(ModuleDataLoaded moduleDataLoaded)
    {
        final String key;

        switch (moduleDataLoaded)
        {
            case DATA_LOADED_PROGRAMME_SESSION:

                key = MODULE_DATA_LOADED_PROGRAMME_SESSION;
                break;

            case DATA_LOADED_PROGRAMME_SPEAKER:

                key = MODULE_DATA_LOADED_PROGRAMME_SPEAKER;
                break;

            case DATA_LOADED_SOCIAL_TWITTER:

                key = MODULE_DATA_LOADED_SOCIAL_TWITTER;
                break;

            case DATA_LOADED_SOCIAL_TWITTER_HASHTAG:

                key = MODULE_DATA_LOADED_SOCIAL_TWITTER_HASHTAG;
                break;

            case DATA_LOADED_SOCIAL_VIDEO:

                key = MODULE_DATA_LOADED_SOCIAL_VIDEO;
                break;

            case DATA_LOADED_SPONSOR:

                key = MODULE_DATA_LOADED_SPONSOR;
                break;

            case DATA_LOADED_PRACTICAL_INFO:

                key = MODULE_DATA_LOADED_PRACTICAL_INFO;
                break;

            case DATA_LOADED_SOCIAL_INFO_TWITTER:

                key = MODULE_DATA_LOADED_SOCIAL_INFO_TWITTER;
                break;

            case DATA_LOADED_SOCIAL_INFO_VIDEO:

                key = MODULE_DATA_LOADED_SOCIAL_INFO_VIDEO;
                break;

            default:

                key = null;
                break;
        }

        return key;
    }

    private static final String getKey(ModuleDataAvailable moduleDataAvailable)
    {
        final String key;

        switch (moduleDataAvailable)
        {
            case DATA_AVAILABLE_PROGRAMME_SESSION:

                key = MODULE_DATA_AVAILABLE_PROGRAMME_SESSION;
                break;

            case DATA_AVAILABLE_PROGRAMME_SPEAKER:

                key = MODULE_DATA_AVAILABLE_PROGRAMME_SPEAKER;
                break;

            case DATA_AVAILABLE_SOCIAL_TWITTER:

                key = MODULE_DATA_AVAILABLE_SOCIAL_TWITTER;
                break;

            case DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG:

                key = MODULE_DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG;
                break;

            case DATA_AVAILABLE_SOCIAL_VIDEO:

                key = MODULE_DATA_AVAILABLE_SOCIAL_VIDEO;
                break;

            case DATA_AVAILABLE_SPONSOR:

                key = MODULE_DATA_AVAILABLE_SPONSOR;
                break;

            case DATA_AVAILABLE_PRACTICAL_INFO:

                key = MODULE_DATA_AVAILABLE_PRACTICAL_INFO;
                break;

            case DATA_AVAILABLE_SOCIAL_INFO_TWITTER:

                key = MODULE_DATA_AVAILABLE_SOCIAL_INFO_TWITTER;
                break;

            case DATA_AVAILABLE_SOCIAL_INFO_VIDEO:

                key = MODULE_DATA_AVAILABLE_SOCIAL_INFO_VIDEO;
                break;

            default:

                key = null;
                break;
        }

        return key;
    }

    public static Boolean getModuleDataLoaded(Context context, ModuleDataLoaded moduleDataLoaded)
    {
        final SharedPreferences prefs = ModulePreferenceHelper.getPreferences(context);

        final String key = ModulePreferenceHelper.getKey(moduleDataLoaded);

        return prefs.getBoolean(key, false);
    }

    public static void saveModuleDataLoaded(Context context, ModuleDataLoaded moduleDataLoaded, Boolean dateLoaded)
    {
        final String key = ModulePreferenceHelper.getKey(moduleDataLoaded);

        if (key != null)
        {
            final SharedPreferences.Editor prefs = ModulePreferenceHelper.getPreferences(context).edit();

            prefs.putBoolean(key, dateLoaded);

            prefs.apply();
        }
    }

    public static void saveModuleDataAvailable(Context context, ModuleDataAvailable moduleDataAvailable, Boolean dateAvailable)
    {
        final String key = ModulePreferenceHelper.getKey(moduleDataAvailable);

        if (key != null)
        {
            final SharedPreferences.Editor prefs = ModulePreferenceHelper.getPreferences(context).edit();

            prefs.putBoolean(key, dateAvailable);

            prefs.apply();
        }
    }

    public static Boolean getModuleDataAvailable(Context context, ModuleDataAvailable moduleDataAvailable)
    {
        final SharedPreferences prefs = ModulePreferenceHelper.getPreferences(context);

        final String key = ModulePreferenceHelper.getKey(moduleDataAvailable);

        return prefs.getBoolean(key, false);
    }

    public static void resetModuleDataLoaded(Context inContext)
    {
        final SharedPreferences.Editor prefs = ModulePreferenceHelper.getPreferences(inContext).edit();

        prefs.remove(MODULE_DATA_LOADED_PROGRAMME_SESSION);
        prefs.remove(MODULE_DATA_LOADED_PROGRAMME_SPEAKER);
        prefs.remove(MODULE_DATA_LOADED_SOCIAL_TWITTER);
        prefs.remove(MODULE_DATA_LOADED_SOCIAL_TWITTER_HASHTAG);
        prefs.remove(MODULE_DATA_LOADED_SOCIAL_VIDEO);
        prefs.remove(MODULE_DATA_LOADED_SPONSOR);
        prefs.remove(MODULE_DATA_LOADED_PRACTICAL_INFO);
        prefs.remove(MODULE_DATA_LOADED_SOCIAL_INFO_TWITTER);
        prefs.remove(MODULE_DATA_LOADED_SOCIAL_INFO_VIDEO);

        prefs.apply();
    }

    public static void resetModuleDataAvailable(Context inContext)
    {
        final SharedPreferences.Editor prefs = ModulePreferenceHelper.getPreferences(inContext).edit();

        prefs.remove(MODULE_DATA_AVAILABLE_PROGRAMME_SESSION);
        prefs.remove(MODULE_DATA_AVAILABLE_PROGRAMME_SPEAKER);
        prefs.remove(MODULE_DATA_AVAILABLE_SOCIAL_TWITTER);
        prefs.remove(MODULE_DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG);
        prefs.remove(MODULE_DATA_AVAILABLE_SOCIAL_VIDEO);
        prefs.remove(MODULE_DATA_AVAILABLE_SPONSOR);
        prefs.remove(MODULE_DATA_AVAILABLE_PRACTICAL_INFO);
        prefs.remove(MODULE_DATA_AVAILABLE_SOCIAL_INFO_TWITTER);
        prefs.remove(MODULE_DATA_AVAILABLE_SOCIAL_INFO_VIDEO);

        prefs.apply();
    }
}
