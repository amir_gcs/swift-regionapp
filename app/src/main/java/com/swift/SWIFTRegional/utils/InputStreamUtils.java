package com.swift.SWIFTRegional.utils;

/**
 * Created by SimonRaes on 27/03/15.
 */
public class InputStreamUtils
{
    public static String streamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
