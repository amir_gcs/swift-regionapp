package com.swift.SWIFTRegional.interfaces;

public interface PlayYoutubeVideoListener
{

    public void playYoutubeVideo();
    public boolean containsYoutubeVideo();

}

