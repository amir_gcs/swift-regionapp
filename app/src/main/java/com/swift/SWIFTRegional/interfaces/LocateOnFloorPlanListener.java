package com.swift.SWIFTRegional.interfaces;

public interface LocateOnFloorPlanListener
{
    public enum FPObjectType {
        EXHIBITOR,
        SESSION
    }

    public void locateOnFloorPlan(String itemId, FPObjectType objectType);
}
