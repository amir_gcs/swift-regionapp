package com.swift.SWIFTRegional.interfaces;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;

import com.swift.SWIFTRegional.adapters.DrawerBaseAdapter;

public interface MainActivityInterface
{
    void onCreate(Bundle savedInstanceState);
    void onDestroy();
    void onPostCreate(Bundle savedInstanceState);
    void onConfigurationChanged(Configuration newConfig);
    boolean onOptionsItemSelected(MenuItem item);
    void loadView(Intent intent, ViewLoaderStrategyInterface.ViewType type);
    boolean showHomeAsUp();

    /**
     * Select the activity feed wall in the drawer list.
     */
    void setHomeAsSelected();

    /**
     * Select the child item in the menu list.
     *
     * @param menuItem item in the menu that will be selected.
     */
    public void selectMenuItem(DrawerBaseAdapter.DrawerMenuItem menuItem);
}
