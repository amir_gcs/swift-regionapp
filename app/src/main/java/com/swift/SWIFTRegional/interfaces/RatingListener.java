package com.swift.SWIFTRegional.interfaces;


public interface RatingListener
{
    public void ratingSelected(int inRating);
}
