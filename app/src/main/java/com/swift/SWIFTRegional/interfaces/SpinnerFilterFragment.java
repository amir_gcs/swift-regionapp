package com.swift.SWIFTRegional.interfaces;

import com.swift.SWIFTRegional.models.spinner.SpinnerItem;

/**
 * Created by SimonRaes on 10/04/15.
 */
public interface SpinnerFilterFragment
{
    void itemSelected(Object item, int position);

    SpinnerItem[] getSpinnerItems();
    int getSpinnerPosition();

}
