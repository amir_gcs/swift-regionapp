package com.swift.SWIFTRegional.interfaces.qr;

/**
 * Created by SimonRaes on 4/05/15.
 */
public interface OnCameraDataReceivedListener
{
    void cameraNotFound();
    void onCameraStarted();
    void onCameraDataReceived(final byte[] data, final int previewWidth, final int previewHeight);
}