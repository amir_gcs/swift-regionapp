package com.swift.SWIFTRegional.interfaces;

import android.content.Intent;

public interface ViewLoaderStrategyInterface
{
    public enum ViewType
    {
        SESSIONS,
        SPONSORS,
        SPEAKERS,
        EXHIBITORS,
        NEWS,
        FLICKR_THUMBS,
        FLICKR_PHOTOS,
        PARTICIPANTS,
        COLLATERAL,
        MESSAGES,
        FLOORPLAN
    }

    void loadView(Intent intent, ViewType type);
}
