package com.swift.SWIFTRegional.interfaces;

/**
 * Created by wesley on 03/07/14.
 */
public interface ContactDetailListener
{
    void addToAddressBook();
    void showTwitter();
    void showLinkedIn();
    void addToAddressBookBasicDetail();
    void showLoginErrorToast();
}
