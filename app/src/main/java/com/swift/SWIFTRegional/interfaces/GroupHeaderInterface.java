package com.swift.SWIFTRegional.interfaces;

/**
 * Created by Wesley on 18/06/14.
 */
public interface GroupHeaderInterface extends Comparable<GroupHeaderInterface>
{
    public String getTitle();
    public String getKey();
}
