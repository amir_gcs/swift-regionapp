package com.swift.SWIFTRegional.interfaces;

import com.swift.SWIFTRegional.models.Advertisement;

/**
 * Created by Wesley on 17/06/14.
 */
public interface AdAdapterInterface
{
    public void setAdvertisements(Advertisement[] advertisements);
}
