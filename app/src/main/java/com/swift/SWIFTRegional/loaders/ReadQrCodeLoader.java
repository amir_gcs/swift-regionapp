package com.swift.SWIFTRegional.loaders;

import android.content.Context;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import com.goodcoresoftware.android.common.framework.content.ITPLoader;

/**
 * Created by SimonRaes on 4/05/15.
 */
public class ReadQrCodeLoader extends ITPLoader<ReadQrCodeLoader.LoaderResult>
{
    private byte[] data;
    private int previewWidth;
    private int previewHeight;

    private QRCodeReader qrCodeReader;

    public ReadQrCodeLoader(final Context context, final byte[] inData, final int inPreviewWidth, final int inPreviewHeight)
    {
        super(context);

        this.data = inData;
        this.previewWidth = inPreviewWidth;
        this.previewHeight = inPreviewHeight;

        this.qrCodeReader = new QRCodeReader();
    }

    @Override
    public LoaderResult loadInBackground()
    {
        // Search QR in all image along, not only in Framing Rect as original code done
        // method from CameraManager class
        final PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource
                (
                        this.data,
                        this.previewWidth,
                        this.previewHeight,
                        0,
                        0,
                        this.previewWidth,
                        this.previewHeight,
                        false
                );

        final HybridBinarizer hybBin = new HybridBinarizer(source);
        final BinaryBitmap bitmap = new BinaryBitmap(hybBin);

        LoaderResult loaderResult = new LoaderResult();

        try
        {
            final Result result = this.qrCodeReader.decode(bitmap);

            // Notify We're found a QRCode
            loaderResult.qrCodeData = result.getText();
        }
        catch (ChecksumException e)
        {
            // QR code invalid: error correction fails
            loaderResult = null;
        }
        catch (NotFoundException e)
        {
            // Notify QR not found
            loaderResult.exception = e;
        }
        catch (FormatException e)
        {
            // QR code cannot be decoded
            loaderResult = null;
        }
        finally
        {
            this.qrCodeReader.reset();
        }

        return loaderResult;
    }

    public static class LoaderResult
    {
        private String qrCodeData;
        private NotFoundException exception;

        public final String getQrCodeData()
        {
            return this.qrCodeData;
        }

        public final NotFoundException getNotFoundException()
        {
            return this.exception;
        }
    }
}
