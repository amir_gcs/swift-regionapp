package com.swift.SWIFTRegional.loaders;

public interface LoaderIDs
{
    int twitter = 1;
    int twitter_name = 2;
    int twitter_hashtag = 3;

    int flickr = 100;
    int flickr_photos = 101;

    int wall = 200;

    int speaker = 300;
    int speaker_sessions = 301;
    int speaker_rate = 302;
    int speaker_unsent_rate = 303;

    int session = 400;
    int session_speakers = 401;
    int tags = 402;
    int session_rate = 403;
    int session_unsent_rate = 404;
    int session_timings = 405;

    int exhibitor = 500;
    int exhibitor_collaterals = 501;
    int exhibitor_representatives = 502;
    int exhibitor_open_theatres = 503;
    int exhibitor_events = 504;
    int exhibitor_sessions = 505;

    int favorite = 600;

    int issue = 800;

    int advertisement = 900;

    int news = 1000;

    int category = 1100;
    int streams = 1101;

    int instagram = 1300;

    int youtube = 1600;

    int message_threads = 1700;
    int message_thread = 1701;
    int messages = 1702;

    int floors = 1800;
    int mappings = 1801;
    int floor_plan_tile = 1802;
    int floor_plan_beacons = 1803;

    int read_qr_code = 2000;

    int unread_messages = 2100;

    int sponsor = 2200;
    int mainevent=2300;
    int regioninfo=2400;

    int practical_info = 2500;

}
