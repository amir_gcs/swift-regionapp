package com.swift.SWIFTRegional.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import com.swift.SWIFTRegional.api.apiservice.SibosApiMessenger;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteExhibitorCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSessionCallback;
import com.swift.SWIFTRegional.contentproviders.PendingFavouriteContentProvider;
import com.swift.SWIFTRegional.models.PrivacySettings;
import com.swift.SWIFTRegional.models.favourites.PendingFavourite;

import com.goodcoresoftware.android.common.http.api.utils.ApiMessenger;
import com.swift.SWIFTRegional.api.ApiManager;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.GetRatingSpeakerCallback;
import com.swift.SWIFTRegional.api.callbacks.SetFavouriteSessionCallback;
import com.swift.SWIFTRegional.api.callbacks.SetPrivacySettingsCallback;
import com.swift.SWIFTRegional.api.callbacks.SetRatingSpeakerCallback;
import com.swift.SWIFTRegional.broadcastreceivers.NetworkConnectionMonitor;
import com.swift.SWIFTRegional.contentproviders.UnsentPrivacySettingsContentProvider;
import com.swift.SWIFTRegional.contentproviders.UnsentRateContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.models.Rate;

import static com.swift.SWIFTRegional.database.tables.RateTable.RateType.SESSION;
import static com.swift.SWIFTRegional.database.tables.RateTable.RateType.SPEAKER;

/**
 * Execute API calls that failed previously.
 */
public class SyncService extends IntentService implements ApiMessenger.ApiMessengerListener
{

    private SibosApiMessenger apiMessenger;

    @Override
    public void onCreate()
    {
        super.onCreate();
        this.apiMessenger = new SibosApiMessenger(this, this);
        this.apiMessenger.create();
    }

    @Override
    public void onDestroy()
    {
        if (this.apiMessenger != null)
        {
            this.apiMessenger.destroy();
        }
        super.onDestroy();
    }

    public SyncService()
    {
        super("SyncService");
    }

    public static Intent createIntent(Context inContext)
    {
        return new Intent(inContext, SyncService.class);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Cursor c = this.getContentResolver().query(UnsentRateContentProvider.CONTENT_URI, null, null, null, null);

        while (c.moveToNext())
        {
            final Rate unsentRate = new Rate();
            unsentRate.constructFromCursor(c);
            sendRating(unsentRate);
        }

        c = this.getContentResolver().query(UnsentPrivacySettingsContentProvider.CONTENT_URI, null, null, null, null);

        while (c.moveToNext())
        {
            final PrivacySettings unsentPrivacySettings = new PrivacySettings();
            unsentPrivacySettings.constructFromCursor(c);
            sendPrivacySettings(unsentPrivacySettings);
        }

        c = this.getContentResolver().query(PendingFavouriteContentProvider.CONTENT_URI, null, null, null, null);

        while (c.moveToNext())
        {
            final PendingFavourite favourite = new PendingFavourite();
            favourite.constructFromCursor(c);
            sendFavourite(favourite);
        }

        NetworkConnectionMonitor.disable(this);
    }


    /**
     * Remove unsentRate from the unsentRates table and attempt to send the rating.
     *
     * @param unsentRate The rating to send.
     */
    private void sendRating(final Rate unsentRate)
    {
        if (unsentRate.getType() == SESSION.ordinal())
        {
            this.getContentResolver().delete(Uri.withAppendedPath(UnsentRateContentProvider.CONTENT_URI_UNSENT_SESSION_RATE, unsentRate.getId()), null, null);

            ApiManager.getInstance(this).setRatingSession(new SetRatingSessionCallback(this, unsentRate.getId(), unsentRate.getRating())
            {
                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);

                    ApiManager.getInstance(SyncService.this).getRatingSession(new GetRatingSessionCallback(SyncService.this, unsentRate.getId()));
                }
            });
        }
        else if (unsentRate.getType() == SPEAKER.ordinal())
        {
            this.getContentResolver().delete(Uri.withAppendedPath(UnsentRateContentProvider.CONTENT_URI_UNSENT_SPEAKER_RATE, unsentRate.getId()), null, null);

            ApiManager.getInstance(this).setRatingSpeaker(new SetRatingSpeakerCallback(this, unsentRate.getId(), unsentRate.getRating())
            {
                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);

                    ApiManager.getInstance(SyncService.this).getRatingSpeaker(new GetRatingSpeakerCallback(SyncService.this, unsentRate.getId())
                    {
                    });
                }
            });
        }
    }

    private void sendFavourite(PendingFavourite favourite)
    {
        if (favourite.getTypeOrdinal() == FavoriteTable.FavouriteType.SPEAKER.ordinal())
        {
            this.getContentResolver().delete(Uri.withAppendedPath(PendingFavouriteContentProvider.CONTENT_URI_UNSENT_SPEAKER_FAVOURITE, favourite.getId()), null, null);

            ApiManager.getInstance(this).setFavouriteSpeaker(new SetFavouriteSpeakerCallback(this, favourite.getId(), favourite.isUnFlag())
            {
                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);
                    ApiManager.getInstance(SyncService.this).getFavouriteSpeakers();
                }
            });
        }
        else if (favourite.getTypeOrdinal() == FavoriteTable.FavouriteType.SESSION.ordinal())
        {
            this.getContentResolver().delete(Uri.withAppendedPath(PendingFavouriteContentProvider.CONTENT_URI_UNSENT_SESSION_FAVOURITE, favourite.getId()), null, null);

            ApiManager.getInstance(this).setFavouriteSession(new SetFavouriteSessionCallback(this, favourite.getId(), favourite.isUnFlag())
            {
                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);
                    ApiManager.getInstance(SyncService.this).getFavouriteSessions();
                }
            });
        }
        else if (favourite.getTypeOrdinal() == FavoriteTable.FavouriteType.EXHIBITOR.ordinal())
        {
            this.getContentResolver().delete(Uri.withAppendedPath(PendingFavouriteContentProvider.CONTENT_URI_UNSENT_EXHIBITOR_FAVOURITE, favourite.getId()), null, null);

            ApiManager.getInstance(this).setFavouriteExhibitor(new SetFavouriteExhibitorCallback(this, favourite.getId(), favourite.isUnFlag())
            {
                @Override
                public void postResult(Void result)
                {
                    super.postResult(result);
                    ApiManager.getInstance(SyncService.this).getFavouriteExhibitors();
                }
            });
        }
    }

    private void sendPrivacySettings(PrivacySettings inPrivacySettings)
    {
        this.getContentResolver().delete(UnsentPrivacySettingsContentProvider.CONTENT_URI, null, null);

        ApiManager.getInstance(this).setPrivacySettings(inPrivacySettings, new SetPrivacySettingsCallback(this));
    }

    @Override
    public void onApiCallResult(int i, int i1, Bundle bundle)
    {

    }
}
