package com.swift.SWIFTRegional.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.swift.SWIFTRegional.utils.AlarmHelper;

/**
 * Called by the BootReceiver when (re)starting te device.
 */
public class ScheduleSessionNotificationsService extends IntentService
{
    public ScheduleSessionNotificationsService()
    {
        super("SessionsNotificationService");
    }

    public static Intent createIntent(Context inContext)
    {
        return new Intent(inContext, ScheduleSessionNotificationsService.class);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        AlarmHelper.cancelAllSessionAlarms(this);
        AlarmHelper.setAllSessionAlarms(this);
    }

}
