package com.swift.SWIFTRegional.services;


import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.swift.SWIFTRegional.models.Event;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.utils.AlarmHelper;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Show a notification that a session is about to start.
 */
public class ShowSessionNotificationService extends IntentService
{
    private static final String EXTRA_START_TIME_UTC_MILLIS = "start_time";
    private static final String EXTRA_SESSION_TITLE = "session_title";
    private static final String EXTRA_ROOM_NAME = "room_name";
    private static final String EXTRA_SESSION_ID = "session_id";

    public static Intent createIntent(Context inContext, EventTiming inEventTiming)
    {
        final Intent intent = ShowSessionNotificationService.createIntent(inContext);

        intent.putExtra(EXTRA_SESSION_ID, inEventTiming.getEventId());
        intent.putExtra(EXTRA_START_TIME_UTC_MILLIS, inEventTiming.getStartTimeMillis());
        intent.putExtra(EXTRA_SESSION_TITLE, inEventTiming.getEvent().getTitle());
        intent.putExtra(EXTRA_ROOM_NAME, inEventTiming.getRoomName());

        return intent;
    }

    public static Intent createIntent(Context inContext)
    {
        return new Intent(inContext, ShowSessionNotificationService.class);
    }

    public ShowSessionNotificationService()
    {
        super("ShowSessionNotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        final EventTiming eventTiming = this.constructSessionTimingFromIntent(intent);
        this.showNotification(eventTiming);
    }

    private EventTiming constructSessionTimingFromIntent(Intent intent)
    {
        final long startTimeMillis = intent.getLongExtra(EXTRA_START_TIME_UTC_MILLIS, 0L);
        final String sessionTitle = intent.getStringExtra(EXTRA_SESSION_TITLE);
        final String roomName = intent.getStringExtra(EXTRA_ROOM_NAME);
        final long sessionId = intent.getLongExtra(EXTRA_SESSION_ID, 0L);

        final EventTiming eventTiming = new EventTiming();
        final Event event = new Event();
        event.setTitle(sessionTitle);

        eventTiming.setStartTimeMillis(startTimeMillis);
        eventTiming.setEvent(event);
        eventTiming.setRoomName(roomName);
        eventTiming.setEventId(sessionId);

        return eventTiming;
    }

    private void showNotification(EventTiming inEventTiming)
    {
        final String startTime = DateUtils.unixTimeToTimeStamp(inEventTiming.getStartTimeMillis());

        final int eventId = AlarmHelper.eventIdToInt(inEventTiming.getEventId());

        Intent sessionDetailsIntent = MainActivity.createIntentViewSession(this);
        sessionDetailsIntent.putExtra(MainActivity.INTENT_EXTRA_EVENT_ID, inEventTiming.getEventId());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(sessionDetailsIntent);

        PendingIntent sessionDetailPendingIntent = stackBuilder.getPendingIntent(eventId, 0);

        final String title = this.getResources().getString(R.string.notification_session_title) + " " + startTime;
        final Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_icon_session)
                        .setContentTitle(title)
                        .setContentText(inEventTiming.getEvent().getTitle())
                        .setSound(notificationSound);

        final NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(title);
        inboxStyle.addLine(inEventTiming.getEvent().getTitle());
        inboxStyle.addLine(inEventTiming.getRoomName());

        mBuilder.setStyle(inboxStyle);
        mBuilder.setContentIntent(sessionDetailPendingIntent);
        mBuilder.setAutoCancel(true);

        final NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(eventId, mBuilder.build());
    }


}
