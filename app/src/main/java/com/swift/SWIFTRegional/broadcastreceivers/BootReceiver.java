package com.swift.SWIFTRegional.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.swift.SWIFTRegional.services.ScheduleSessionNotificationsService;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

public class BootReceiver extends BroadcastReceiver
{

    @Override
    public void onReceive(Context inContext, Intent inIntent)
    {
        if (inIntent.getAction().equals("android.intent.action.BOOT_COMPLETED") && UserPreferencesHelper.loggedIn(inContext))
        {
            Intent startServiceIntent = new Intent(inContext, ScheduleSessionNotificationsService.class);
            inContext.startService(startServiceIntent);
        }
    }
}
