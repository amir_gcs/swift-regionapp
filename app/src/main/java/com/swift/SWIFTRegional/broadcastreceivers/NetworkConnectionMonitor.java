package com.swift.SWIFTRegional.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.swift.SWIFTRegional.services.SyncService;

import com.goodcoresoftware.android.common.utils.Connectivity;

/**
 * Listen for changes in network connection.
 */
public class NetworkConnectionMonitor extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (Connectivity.hasConnection(context))
        {
            context.startService(SyncService.createIntent(context));
        }
    }

    public static void enable(Context inContext)
    {
        ComponentName receiver = new ComponentName(inContext, NetworkConnectionMonitor.class);
        PackageManager pm = inContext.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public static void disable(Context inContext)
    {
        ComponentName receiver = new ComponentName(inContext, NetworkConnectionMonitor.class);
        PackageManager pm = inContext.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
