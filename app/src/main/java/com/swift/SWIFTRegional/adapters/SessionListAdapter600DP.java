package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.swift.SWIFTRegional.fragments.programme.SessionListFragment;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.viewhelper.SessionViewHelper600DP;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.EventTimingTwoItems;
import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Created by SimonRaes on 07/07/15.
 * Display a collapsible list of sessions, grouped on date (day).
 */
public class SessionListAdapter600DP extends SessionListAdapter
{
    private int paddingBottom;
    private ViewLoaderStrategyInterface viewLoader;

    public SessionListAdapter600DP(Context inContext, ViewLoaderStrategyInterface viewLoader)
    {
        super(inContext);
        this.paddingBottom = (int) context.getResources().getDimension(R.dimen.padding_session_card);
        this.viewLoader = viewLoader;
    }

    @Override
    public Integer getChildrenForItem(EventTiming item){
        return ((EventTimingTwoItems) item).hasSecondEvent ? 2 : 1;
    }

    // Dirty Hack
    @Override
    public Boolean isSessionListAdapterOverridden() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Object previousItem = this.getItem(position - 1);
        Object currentItem = this.getItem(position);
        Object nextItem = this.getItem(position + 1);

        if (currentItem instanceof EventTimingTwoItems)
        {
            EventTimingTwoItems currentEvent = (EventTimingTwoItems) currentItem;

            // Check if the date label should be shown (only if previous event is on a different day)
            boolean showDateLabel = position == 0;
            if (position > 0 && previousItem instanceof EventTimingTwoItems)
            {
                showDateLabel = !(DateUtils.prettyDayString(((EventTimingTwoItems) previousItem).event1.getStartDate())
                        .equals(DateUtils.prettyDayString(currentEvent.event1.getStartDate())));
            }

            convertView = SessionViewHelper600DP.createSessionView(convertView, parent, this.viewLoader, currentEvent.event1, currentEvent.event2, false, showDateLabel);
        }
        else
        {
            // Advertisement
            convertView = super.getView(position, convertView, parent);
        }

        // Add extra bottom padding to last child in group (if the next child has a different date or if there is no next child)
        boolean isLastChild = false;
        if (nextItem == null)
        {
            isLastChild = true;
        }
        else if (nextItem instanceof EventTimingTwoItems) // won't need extra padding if next item is an ad
        {
            if (currentItem instanceof EventTimingTwoItems)
            {
                isLastChild = !(DateUtils.prettyDayString(((EventTimingTwoItems) currentItem).event1.getStartDate())
                        .equals(DateUtils.prettyDayString(((EventTimingTwoItems) nextItem).event1.getStartDate())));
            }
            else
            {
                if (previousItem != null && previousItem instanceof EventTiming)
                {
                    isLastChild = !(DateUtils.prettyDayString(((EventTimingTwoItems) previousItem).event1.getStartDate())
                            .equals(DateUtils.prettyDayString(((EventTimingTwoItems) nextItem).event1.getStartDate())));
                }
            }
        }

        convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), isLastChild ? this.paddingBottom : 0);

        return convertView;
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup)
    {
        if (this.getCount() > 0)
        {
            Object item = getItem(i);

            if (!(item instanceof EventTiming))
            {
                item = getItem(i - 1);
            }

            String startDate = DateUtils.prettyDayString(((EventTimingTwoItems) item).event1.getStartDate());

            if (view == null)
            {
                view = this.inflater.inflate(R.layout.listitem_session_groupheader, viewGroup, false);

                final HeaderViewHolder holder = new HeaderViewHolder();
                holder.textViewHeaderTitle = (TextView) view.findViewById(R.id.textView_sessionHeaderTitle);
                holder.textViewHeaderNumber = (TextView) view.findViewById(R.id.textView_sessionHeaderNumber);
                holder.imageViewCollapseIcon = (ImageView) view.findViewById(R.id.imageView_collapseIcon);

                view.setTag(holder);
            }

            final HeaderViewHolder holder = (HeaderViewHolder) view.getTag();

            long headerId = getHeaderId(i);

            if(SessionListFragment.selectedMode.equals("DAY"))
            {
                holder.textViewHeaderTitle.setText(startDate);

            }else if(SessionListFragment.selectedMode.equals("TYPE"))
            {
                holder.textViewHeaderTitle.setText(((EventTimingTwoItems) item).event1.getEvent().getType().getSessionName());
            }

            holder.textViewHeaderNumber.setText(SessionListFragment.headerCount.get(headerId) + " sessions");

            boolean collapsed = (this.headerStates == null || !this.headerStates.containsKey(headerId) || this.headerStates.get(headerId).isCollapsed());
            holder.imageViewCollapseIcon.setImageDrawable(ContextCompat.getDrawable(this.context, collapsed ? R.drawable.ic_colapse : R.drawable.ic_expand));

            return view;
        }
        else
        {
            return null;
        }
    }

    @Override
    public long getHeaderId(int i)
    {
        Object item = getItem(i);
        if (!(item instanceof EventTimingTwoItems))
        {
            // Advertisements don't have a date to generate a header Id from, get the exhibitor event that preceded them instead.
            item = getItem(i - 1);
        }
        return item != null ? getHeaderId((EventTimingTwoItems) item) : -1;
    }

    @Override
    protected long getHeaderId(EventTiming item)
    {
        if(SessionListFragment.selectedMode.equals("DAY"))
        {
            return Long.parseLong(DateUtils.iso8601StringToComparableDateString(((EventTimingTwoItems) item).event1.getStartDate()));
        }
        else if(SessionListFragment.selectedMode.equals("ROOM"))
        {
            return  (long)((EventTimingTwoItems) item).event1.getRoomName().hashCode();
        }
        else if(SessionListFragment.selectedMode.equals("TYPE"))
        {
            return  (long)((EventTimingTwoItems) item).event1.getEvent().getType().getSessionId().hashCode();
        }

        return 1;

    }

    @Override
    public long getItemId(int position)
    {
        return (long) position;
    }

    private static class HeaderViewHolder
    {
        TextView textViewHeaderTitle;
        TextView textViewHeaderNumber;
        ImageView imageViewCollapseIcon;
    }
}
