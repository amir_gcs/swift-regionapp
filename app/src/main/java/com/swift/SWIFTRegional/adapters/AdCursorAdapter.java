package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.swift.SWIFTRegional.interfaces.AdAdapterInterface;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.Advertisement;
import com.swift.SWIFTRegional.views.RatioImageView;

public abstract class AdCursorAdapter extends CursorAdapter implements AdAdapterInterface
{
    public static final float AD_RATIO = 24 / 64f;

    protected LayoutInflater inflater;

    private Advertisement[] advertisements;

    private final int adInterval;

    private int adLayoutId;

    public AdCursorAdapter(Context inContext)
    {
        super(inContext, null, 0);

        this.inflater = LayoutInflater.from(inContext);

        this.adInterval = inContext.getResources().getInteger(R.integer.ad_interval);

        this.adLayoutId = R.layout.listitem_ad;
    }

    @Override
    public Object getItem(int position)
    {
        this.getCursor().moveToPosition(this.getModifiedPosition(position));

        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return null;
        }

        if (position > 0 && position % adInterval == adInterval - 1)
        {
            return this.advertisements[(position / adInterval) % advertisements.length];
        }

        return null;
    }

    @Override
    public int getCount()
    {
        int count = this.getCursor() == null ? 0 : this.getCursor().getCount();

        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return count;
        }

        count += count / (this.adInterval - 1);

        return count;
    }

    public boolean isAdvertisement(int position)
    {
        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return false;
        }

        return position > 0 && position % adInterval == adInterval - 1;
    }

    public int getModifiedPosition(int position)
    {
        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return position;
        }

        return position - position / adInterval;
    }

    @Override
    public void setAdvertisements(Advertisement[] advertisements)
    {
        //disable ads for regional app
        this.advertisements = null;
        //this.advertisements = advertisements;

        //this.notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount()
    {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object o = this.getItem(position);

        if (o instanceof Advertisement)
        {
            if (convertView == null)
            {
                convertView = this.inflater.inflate(adLayoutId, parent, false);

                final AdViewHolder adHolder = new AdViewHolder();

                adHolder.imageView = (RatioImageView) convertView.findViewById(R.id.imageView);
                adHolder.imageView.setRatio(AD_RATIO);


                convertView.setTag(adHolder);
            }

            final Advertisement ad = (Advertisement) o;

            final AdViewHolder adHolder = (AdViewHolder) convertView.getTag();

            if (!TextUtils.isEmpty(ad.getPictureUrlBig()))
            {
                Picasso.with(parent.getContext())
                        .load(ad.getPictureUrlBig())
                        .into(adHolder.imageView);
            }

            if(SibosUtils.isGreaterThan600DP(mContext)) {
                int screenWidth = SibosUtils.getScreenWidth(mContext);
                int padding = screenWidth / 6;
                convertView.setPadding(padding, adHolder.imageView.getPaddingTop(), padding, adHolder.imageView.getPaddingBottom());
            }

            return convertView;
        }

        return null;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    public void setAdLayoutId(int layoutId)
    {
        this.adLayoutId = layoutId;
    }

    private static class AdViewHolder
    {
        public RatioImageView imageView;
    }
}
