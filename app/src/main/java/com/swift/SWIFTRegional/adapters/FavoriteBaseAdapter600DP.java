package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.SponsorTwoItems;
import com.swift.SWIFTRegional.models.exhibitor.ExhibitorTwoItems;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.viewhelper.SessionViewHelper600DP;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.EventTimingTwoItems;
import com.swift.SWIFTRegional.models.SpeakerTwoItems;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.viewhelper.ExhibitorViewHelper600DP;
import com.swift.SWIFTRegional.viewhelper.SpeakerViewHelper600DP;
import com.swift.SWIFTRegional.viewhelper.SponsorViewHelper600DP;


public class FavoriteBaseAdapter600DP extends FavoriteBaseAdapter
{
    private EventTimingTwoItems[] eventTimings;
    private ExhibitorTwoItems[] exhibitors;
    private SpeakerTwoItems[] speakers;
    private SponsorTwoItems[] sponsors;


    protected int paddingCard;

    ViewLoaderStrategyInterface viewLoader;

    @Override
    protected Object[] getLists(){
        return new Object[]{speakers, eventTimings, sponsors
                , tweets, videos};
    }

    public FavoriteBaseAdapter600DP(Context inContext, ViewLoaderStrategyInterface viewLoader)
    {
        super(inContext);
        this.viewLoader = viewLoader;
    }

    @Override
    public Boolean isOverridden() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        switch (getItemViewType(position))
        {
            case VIEWTYPE_SESSION:

                EventTimingTwoItems et2 = (EventTimingTwoItems) getItem(position);
                convertView = SessionViewHelper600DP.createSessionView(convertView, parent, viewLoader, et2.event1, et2.event2, true, this.sessionShouldShowDate(position));
                convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), isLastSession(position) ? this.paddingCard : 0);
                break;

            case VIEWTYPE_SPONSOR:

                SponsorTwoItems spx2 = (SponsorTwoItems) getItem(position);
                convertView = SponsorViewHelper600DP.createSpeakerView(convertView, viewLoader,parent, spx2.sponsor1, spx2.sponsor2);
                break;

            case VIEWTYPE_SPEAKER:
                SpeakerTwoItems sp2 = (SpeakerTwoItems) getItem(position);
                convertView = SpeakerViewHelper600DP.createSpeakerView(convertView, viewLoader, parent, sp2.speaker1, sp2.speaker2);
                break;



            default:
                convertView = super.getView(position, convertView, parent);
                break;
        }

        return convertView;
    }

    private boolean isLastSession(int position)
    {
        return eventTimings.length - 1 == getAdjustedSessionPosition(position);
    }

    private boolean sessionShouldShowDate(int position)
    {
        boolean shouldShowDate = true;
        if (this.eventTimings.length > 1 && this.getAdjustedSessionPosition(position) > 0)
        {
            int previousTimingPositionOffset = this.getItem(position - 1) instanceof EventTiming ? 1 : 2;
            String previousDate = DateUtils.prettyDayString(((EventTimingTwoItems) this.getItem(position - previousTimingPositionOffset)).event1.getStartDate());
            String currentDate = DateUtils.prettyDayString(((EventTimingTwoItems) this.getItem(position)).event1.getStartDate());
            if (previousDate.equals(currentDate))
            {
                shouldShowDate = false;
            }
        }
        return shouldShowDate;
    }

    private int getAdjustedSessionPosition(int position)
    {
        position = this.getModifiedPosition(position);

        if (this.speakers != null && speakers.length > 0)
        {
            // Speakers
            position -= speakers.length;
        }

        return position;
    }

    public void setEventTimings(EventTimingTwoItems[] eventTimings)
    {
        this.eventTimings = eventTimings;
    }

    public void setExhibitors(ExhibitorTwoItems[] exhibitors)
    {
        this.exhibitors = exhibitors;
    }

    public void setSpeakers(SpeakerTwoItems[] speakers)
    {
        this.speakers = speakers;
    }

    public void setSponsors(SponsorTwoItems[] sponsors) {
        this.sponsors = sponsors;
    }
}