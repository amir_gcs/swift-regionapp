package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.models.event.Event;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.preferences.ModulePreferenceHelper;

import java.util.EnumSet;

public class DrawerBaseAdapter extends BaseAdapter
{
    private static final int VIEWTYPE_CHILD = 1;

    private static boolean sLoggedIn = false;

    private EnumSet<DrawerMenuItem> drawerMenuItems = EnumSet.allOf(DrawerMenuItem.class);
    public void setLoggedIn(boolean loggedInStatus)
    {
        sLoggedIn = loggedInStatus;
        this.notifyDataSetChanged();
    }

    private boolean canShowProgramme() {
        return ModulePreferenceHelper.getModuleDataAvailable(context, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SESSION) ||
               ModulePreferenceHelper.getModuleDataAvailable(context, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PROGRAMME_SPEAKER);
    }

    private boolean canShowWall() {
        return ModulePreferenceHelper.getModuleDataAvailable(context, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER) ||
               ModulePreferenceHelper.getModuleDataAvailable(context, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_TWITTER_HASHTAG) ||
               ModulePreferenceHelper.getModuleDataAvailable(context, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SOCIAL_VIDEO);
    }

    private boolean canShowSponsor() {
        return ModulePreferenceHelper.getModuleDataAvailable(context, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_SPONSOR);
    }

    private boolean canShowPracticalInfo() {
        return ModulePreferenceHelper.getModuleDataAvailable(context, ModulePreferenceHelper.ModuleDataAvailable.DATA_AVAILABLE_PRACTICAL_INFO);
    }

    public void setMenuItemsBasedOnData(){

        if (!MainActivity.selectedEventProvider.isEventSelected()){
            drawerMenuItems.remove(DrawerMenuItem.Programme);
        } else if (canShowProgramme()){
            drawerMenuItems.add(DrawerMenuItem.Programme);
        } else {
            drawerMenuItems.remove(DrawerMenuItem.Programme);
        }

        if (!MainActivity.selectedEventProvider.isEventSelected()){
            drawerMenuItems.remove(DrawerMenuItem.ActivityWall);
        } else if (canShowWall()){
            drawerMenuItems.add(DrawerMenuItem.ActivityWall);
        } else {
            drawerMenuItems.remove(DrawerMenuItem.ActivityWall);
        }

        if (!MainActivity.selectedEventProvider.isEventSelected()){
            drawerMenuItems.remove(DrawerMenuItem.Sponsor);
        } else if (canShowSponsor()){
            drawerMenuItems.add(DrawerMenuItem.Sponsor);
        } else {
            drawerMenuItems.remove(DrawerMenuItem.Sponsor);
        }

        if (!MainActivity.selectedEventProvider.isEventSelected()){
            drawerMenuItems.remove(DrawerMenuItem.PracticalInfo);
        } else if (canShowPracticalInfo()){
            drawerMenuItems.add(DrawerMenuItem.PracticalInfo);
        } else {
            drawerMenuItems.remove(DrawerMenuItem.PracticalInfo);
        }

        this.notifyDataSetChanged();
    }

    public enum DrawerMenuItem
    {

        SelectedEventInfo(),
        Programme(R.string.menu_child_programme, R.drawable.icon_programme),
        ActivityWall(R.string.menu_child_activity_wall, R.drawable.icon_wall),
        Sponsor(R.string.menu_child_exhibition, R.drawable.icon_sponsor),
        PracticalInfo(R.string.menu_child_practicalinfo, R.drawable.icon_practical),
        Search(R.string.menu_child_search, R.drawable.ic_action_ic_search),
        Event(R.string.menu_child_event, R.drawable.icon_event);



        public int titleResourceId;
        public int iconImageResourceId;

        DrawerMenuItem(final int titleResourceId, final int iconImageResourceId)
        {
            this.titleResourceId = titleResourceId;
            this.iconImageResourceId = iconImageResourceId;
        }

        DrawerMenuItem()
        {

        }
    }

    private final Context context;
    private final LayoutInflater inflater;

    public DrawerBaseAdapter(Context context)
    {
        super();

        this.context = context;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setMenuItemsBasedOnData();
    }

    @Override
    public int getCount()
    {
        return drawerMenuItems.size();
//        return DrawerMenuItem.values().length;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public Object getItem(int position)
    {
        int i = 0;

        for (DrawerMenuItem drawerMenuItem : drawerMenuItems)
        {

            if (position == i)
            {
                return drawerMenuItem;
            }

            i++;
        }

        return null;
    }

    /**
     * Determine position of a childItem in the list.
     *
     * @param childItem
     * @return position of childItem
     */
    public int getPosition(DrawerMenuItem childItem)
    {

        int i = 0;

        for (DrawerMenuItem drawerMenuItem : drawerMenuItems)
        {
            if (drawerMenuItem.equals(childItem))
            {
                return i;
            }

            i++;
        }

        return -1;
    }

    @Override
    public int getViewTypeCount()
    {
        return 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        return VIEWTYPE_CHILD;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

            if(position==0)
            {
                convertView=this.newChildEventView(parent);
            }
            else
            {

                    convertView = this.newChildView(parent);


            }



        this.bindChildView(position, convertView);

        return convertView;
    }

    private View newChildView(ViewGroup parent)
    {
        final View view = inflater.inflate(R.layout.listitem_drawer, parent, false);

        final ChildViewHolder holder = new ChildViewHolder();

        holder.layoutContent = (RelativeLayout) view.findViewById(R.id.layout_content);
        holder.textViewTitle = (TextView) view.findViewById(R.id.textView_menuitem_title);
        holder.imageViewIcon = (ImageView) view.findViewById(R.id.imageView_menuitem_icon);

        view.setTag(holder);

        return view;
    }

    private View newChildEventView(ViewGroup parent)
    {
        final View view = inflater.inflate(R.layout.listitem_selectedevent, parent, false);

        final SelectedEventViewHolder holder = new SelectedEventViewHolder();

        holder.layoutContent = (LinearLayout) view.findViewById(R.id.layout_content);
        holder.textViewTitle = (TextView) view.findViewById(R.id.textView_menuitem_eventtitle);
        holder.textViewCategory = (TextView) view.findViewById(R.id.textView_menuitem_eventcategory);
        holder.textViewStartDate = (TextView) view.findViewById(R.id.textView_menuitem_eventstartdate);
        holder.textViewLocation = (TextView) view.findViewById(R.id.textView_menuitem_eventlocation);


        view.setTag(holder);

        return view;
    }

    private void bindChildView(int position, View convertView)
    {
        final DrawerMenuItem drawerMenuItem = (DrawerMenuItem) this.getItem(position);

        if(drawerMenuItem.titleResourceId==0)
        {
            //get the selected event information from the mainactivity
            Event event= MainActivity.selectedEventProvider.getSelectedEvent();

            if(event!=null)
            {
                final SelectedEventViewHolder holder = (SelectedEventViewHolder) convertView.getTag();
                holder.textViewTitle.setText(event.getTitle());
                holder.textViewCategory.setText(event.getTypeName());
                holder.textViewStartDate.setText(DateUtils.formattedDateFromString("","",event.getStartDate()));
                holder.textViewLocation.setText(event.getLocation());
            }




        }
        else {


            final String childText = this.context.getString(drawerMenuItem.titleResourceId);

            final ChildViewHolder holder = (ChildViewHolder) convertView.getTag();

            holder.imageViewIcon.setImageDrawable(ContextCompat.getDrawable(context, drawerMenuItem.iconImageResourceId));
            holder.textViewTitle.setText(childText);

            switch (drawerMenuItem) {
                case Programme:
                    holder.textViewTitle.setTextColor(convertView.getResources().getColorStateList(R.color.white_foreground_color));
                    break;
                case ActivityWall:
                    holder.textViewTitle.setTextColor(convertView.getResources().getColorStateList(R.color.white_foreground_color));
                    break;
                case Sponsor:
                    holder.textViewTitle.setTextColor(convertView.getResources().getColorStateList(R.color.white_foreground_color));
                    break;
                case Search:
                    // No selected state for search because it launches in a new activity
                    holder.textViewTitle.setTextColor(convertView.getResources().getColor(R.color.white));
                    break;
                case SelectedEventInfo:
                case  Event:
                    // No selected state for search because it launches in a new activity
                    holder.textViewTitle.setTextColor(convertView.getResources().getColor(R.color.white));
                    break;
            }
        }
    }

    @Override
    public boolean areAllItemsEnabled()
    {
        return false;
    }

    @Override
    public boolean isEnabled(int position)
    {
        final int viewType = this.getItemViewType(position);
        return viewType == VIEWTYPE_CHILD;
    }

    private static class ChildViewHolder
    {
        RelativeLayout layoutContent;
        TextView textViewTitle;
        ImageView imageViewIcon;
    }


    private static class SelectedEventViewHolder
    {
        LinearLayout layoutContent;
        TextView textViewTitle;
        TextView textViewCategory;
        TextView textViewStartDate;
        TextView textViewLocation;

    }
}