package com.swift.SWIFTRegional.adapters;

import android.content.Context;

import com.swift.SWIFTRegional.adapters.activityfeed.WallCursorAdapter;
import com.swift.SWIFTRegional.models.News;

public class NewsCursorAdapter extends WallCursorAdapter
{
    public NewsCursorAdapter(Context inContext)
    {
        super(inContext);
    }

    @Override
    public Object getItem(int position)
    {
        if(this.isAdvertisement(position))
        {
            final Object o = super.getItem(position);

            return o;
        }

        this.getCursor().moveToPosition(this.getModifiedPosition(position));

        final News news = new News();
        news.constructFromCursor(this.getCursor());

        return news;
    }
}
