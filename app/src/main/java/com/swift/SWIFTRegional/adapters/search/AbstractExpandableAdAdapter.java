package com.swift.SWIFTRegional.adapters.search;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;
import com.swift.SWIFTRegional.models.HeaderState;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.adapters.AdCursorAdapter;
import com.swift.SWIFTRegional.interfaces.AdAdapterInterface;
import com.swift.SWIFTRegional.models.Advertisement;
import com.swift.SWIFTRegional.views.RatioImageView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by SimonRaes on 07/07/15.
 * Adapter for a list of grouped items with sticky and collapsible group headers.
 * Must be subclassed to supply the views for the items and headers.
 */
public abstract class AbstractExpandableAdAdapter<T> extends BaseAdapter implements AdAdapterInterface, StickyListHeadersAdapter
{
    protected final Context context;
    protected final LayoutInflater inflater;
    protected T[] items;
    private Advertisement[] advertisements;
    private int count = -1;

    // Map of <headerId, headerState (= child count and collapsed state)>
    protected HashMap<Long, HeaderState> headerStates;

    // Map of <Index of first item of group, list of all items in that group>
    protected SparseArray<ArrayList<T>> sparseArray;

    private final int adInterval;
    private final int paddingTop;

    // Override this if item has more than one children
    public Integer getChildrenForItem(T item){
        return 1;
    }

    public AbstractExpandableAdAdapter(Context context)
    {
        this.context = context;
        this.inflater = LayoutInflater.from(this.context);

        this.adInterval = context.getResources().getInteger(R.integer.ad_interval);
        this.paddingTop = (int) context.getResources().getDimension(R.dimen.padding_session_card);
    }

    public void setItems(T[] items)
    {
        this.items = items;
        this.count = -1;

        int lastInsertedIndex = -1;
        int adsInPreviousGroups = 0;

        if (items != null && items.length > 0)
        {
            this.sparseArray = new SparseArray<>();
            this.headerStates = new LinkedHashMap<>();

            // Create a map of <HeaderId - HeaderState>
            for (int i = 0; i < this.items.length; i++)
            {
                final T item = this.items[i];
                final long headerId = this.getHeaderId(item);

                if (!this.headerStates.containsKey(headerId))
                {
                    //check if a previous group exists
                    if (this.sparseArray.get(lastInsertedIndex) != null && this.sparseArray.get(lastInsertedIndex).size() > 0)
                    {
                        if (this.sparseArray.get(lastInsertedIndex).size() < this.adInterval)
                        {
                            // Previous group was very short, contains exactly 1 ad
                            adsInPreviousGroups += 1;
                        }
                        else
                        {
                            // Previous group might contain more than one ad, calculate:
                            adsInPreviousGroups += this.sparseArray.get(lastInsertedIndex).size() / (this.adInterval - 1);
                        }
                    }

                    HeaderState headerState = new HeaderState(false, 1);

                    headerState.setChildCount(headerState.getChildCount() + (getChildrenForItem(item) - 1));

                    this.headerStates.put(headerId, headerState);

                    ArrayList<T> sparseArrayItems = new ArrayList<>();
                    sparseArrayItems.add(item);
                    this.sparseArray.put(i + adsInPreviousGroups, sparseArrayItems);
                    lastInsertedIndex = i + adsInPreviousGroups;
                }
                else
                {
                    HeaderState state = this.headerStates.get(headerId);
                    state.setChildCount(state.getChildCount() + getChildrenForItem(item));
                    this.headerStates.put(headerId, state);

                    this.sparseArray.get(lastInsertedIndex).add(item);
                }
            }

            this.calculateCount();

        }

        this.notifyDataSetChanged();
    }


    /**
     * Returns the adjusted position of this item, after taking inserted advertisements into account.
     *
     * @return the new position after advertisements have been inserted into the list.
     */
    public int getAdjustedChildPosition(int groupPosition, int childPosition)
    {
        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return childPosition;
        }

        final int groupCount = this.sparseArray.get(this.sparseArray.keyAt(groupPosition)).size();

        if (childPosition < this.adInterval)
        {
            return childPosition;
        }

        if (groupCount < this.adInterval)
        {
            return childPosition - 1;
        }
        else
        {
            return childPosition - childPosition / this.adInterval;
        }
    }

    /**
     * @param position the position of the item before advertisements have been added.
     * @return the group in which this item will be placed.
     */
    private int groupPositionForPosition(int position)
    {
        for (int i = 0; i < this.sparseArray.size(); i++)
        {
            if (i == this.sparseArray.size() - 1)
            {
                // we reached the final part of the loop, position is definitely in the last group
                return i;
            }
            else if (position < sparseArray.keyAt(i + 1))
            {
                // found the group
                return i;
            }
        }
        return -1;
    }

    private int childPositionForPosition(int position, int groupPosition)
    {
        if (groupPosition > 0)
        {
            return position - sparseArray.keyAt(groupPosition);
        }
        else
        {
            return position;
        }
    }

    public void setAdvertisements(Advertisement[] advertisements)
    {
        // disable ads for regional app
        this.advertisements = null;
    }

    public Long[] getHeaderIds()
    {
        final Long[] headers = new Long[this.headerStates.size()];

        int i = 0;
        for (Map.Entry<Long, HeaderState> entry : this.headerStates.entrySet())
        {
            headers[i] = entry.getKey();
            i++;
        }

        return headers;
    }


    public void setHeaderIdCollapsed(long headerId, boolean collapsed)
    {
        final HeaderState state = this.headerStates.get(headerId);
        state.setCollapsed(collapsed);
        this.headerStates.put(headerId, state);
    }

    protected abstract long
    getHeaderId(T item);

    @Override
    public int getCount()
    {
        if (this.items == null)
        {
            return 0;
        }
        else if (this.advertisements == null || this.advertisements.length == 0)
        {
            return this.items.length;
        }
        else
        {
            return this.count;
        }
    }

    /**
     * Calculates the total number of items in the list.
     */
    private void calculateCount()
    {
        // Calculate the total number of items
        final int lastGroupStartPosition = this.sparseArray.keyAt(this.sparseArray.size() - 1);
        final int lastGroupItemCount = this.sparseArray.get(lastGroupStartPosition).size();
        int totalItemsCount = lastGroupStartPosition + lastGroupItemCount;

        // Adjust for advertisements that will be inserted (startPosition has already been adjusted)
        if (lastGroupItemCount > this.adInterval)
        {
            totalItemsCount += lastGroupItemCount / (this.adInterval - 1);
        }
        else
        {
            totalItemsCount++;
        }

        this.count = totalItemsCount;
    }

    @Override
    public Object getItem(int position)
    {
        if (position < 0)
        {
            return null;
        }
        else if (this.advertisements == null || this.advertisements.length == 0)
        {
            return position < items.length ? this.items[position] : null;
        }

        int groupPosition = this.groupPositionForPosition(position);
        int childPosition = this.childPositionForPosition(position, groupPosition);

        // Last year's version of the app used an expandable adapter with a child and group position for each element.
        // This new ListView uses a single list of items. In order to keep the behaviour exactly the same,
        // positions are converted to group and child position so the old code can be reused.

        int childCount = 0;

        if (childPosition > 0)
        {
            childCount = this.sparseArray.get(this.sparseArray.keyAt(groupPosition)).size();

            int x = 0;

            for (int i = 0; i <= groupPosition; i++)
            {
                final int count = this.sparseArray.get(this.sparseArray.keyAt(i)).size();

                if (i != groupPosition)
                {
                    x += Math.max(1, count / adInterval);
                }
                else
                {
                    x += (childPosition - 1) / adInterval;
                }
            }

            if (childPosition % this.adInterval == this.adInterval - 1)
            {
                return this.advertisements[x % this.advertisements.length];
            }
            else if (childCount < this.adInterval && childPosition == childCount)
            {
                return this.advertisements[x % this.advertisements.length];
            }
        }

        final int adjustedChildPosition = this.getAdjustedChildPosition(groupPosition, childPosition);
        if (adjustedChildPosition > 0 && adjustedChildPosition >= childCount)
        {
            return null;
        }
 
        return this.sparseArray.get(this.sparseArray.keyAt(groupPosition)).get(adjustedChildPosition);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null || !(convertView.getTag() instanceof AdViewHolder))
        {
            convertView = this.newView(parent);
        }
        this.bindView(position, convertView);

        return convertView;
    }

    public View newView(ViewGroup parent)
    {
        final View view = this.inflater.inflate(R.layout.listitem_ad, parent, false);

        final AdViewHolder adHolder = new AdViewHolder();

        adHolder.imageView = (RatioImageView) view.findViewById(R.id.imageView);
        adHolder.imageView.setRatio(AdCursorAdapter.AD_RATIO);



        view.setBackgroundColor(adHolder.imageView.getContext().getResources().getColor(R.color.search_gray));

        view.setTag(adHolder);

        return view;
    }

    public void bindView(int childPosition, View view)
    {
        final Object o = this.getItem(childPosition);

        if (o instanceof Advertisement)
        {
            final Advertisement ad = (Advertisement) o;

            final AdViewHolder adHolder = (AdViewHolder) view.getTag();

            if(SibosUtils.isGreaterThan600DP(context)) {
                int screenWidth = SibosUtils.getScreenWidth(context);
                int padding = screenWidth / 6;
                view.setPadding(padding, this.paddingTop, padding, adHolder.imageView.getPaddingBottom());
            } else {
                view.setPadding(0, this.paddingTop, 0, adHolder.imageView.getPaddingBottom());
            }

            if (ad.getPictureUrlBig() != null)
            {
                Picasso.with(context)
                        .load(ad.getPictureUrlBig())
                        .into(adHolder.imageView);
            }
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    private static class AdViewHolder
    {
        public RatioImageView imageView;
    }

}
