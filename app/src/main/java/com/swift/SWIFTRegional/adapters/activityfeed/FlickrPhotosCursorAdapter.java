package com.swift.SWIFTRegional.adapters.activityfeed;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.swift.SWIFTRegional.adapters.AdCursorAdapter;
import com.swift.SWIFTRegional.views.CustomImageView;

import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.flickr.FlickrPhoto;

/**
 * Created by Wesley on 30/05/14.
 */
public class FlickrPhotosCursorAdapter extends AdCursorAdapter
{
    private int columns;

    public FlickrPhotosCursorAdapter(Context context)
    {
        super(context);
        setAdLayoutId(R.layout.listitem_ad_cards);
    }

    @Override
    public FlickrPhoto getItem(int position)
    {
        if (this.getCursor().moveToPosition(position))
        {
            final FlickrPhoto photo = new FlickrPhoto();
            photo.constructFromCursor(this.getCursor());

            return photo;
        }

        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = this.inflater.inflate(R.layout.griditem_imageview, parent, false);
        }

        final int thumbSize = this.getThumbSize(parent, this.columns);

        convertView.setLayoutParams(new GridView.LayoutParams(thumbSize, thumbSize));

        convertView.requestLayout();

        final FlickrPhoto photo = this.getItem(position);

        if(thumbSize > 0 && (!photo.getPictureUrlSmall().equals(((CustomImageView)convertView).getPictureUrl()) || thumbSize != ((CustomImageView) convertView).getThumbSize()))
        {
            if(thumbSize > 0)
            {
                Picasso.with(convertView.getContext())
                        .load(photo.getPictureUrlSmall())
                        .resize(thumbSize, thumbSize)
                        .centerCrop()
                        .into((ImageView) convertView);

                ((CustomImageView) convertView).setPictureUrl(photo.getPictureUrlSmall());
                ((CustomImageView) convertView).setThumbSize(thumbSize);
            }
        }

        return convertView;
    }

    public int getThumbSize(final View view, int columns)
    {
        final int width = view.getMeasuredWidth();

        final int horizontalSpacing = view.getContext().getResources().getDimensionPixelSize(R.dimen.padding_xsmall);

        return Math.max(0, (width - view.getPaddingLeft() - view.getPaddingRight() - (horizontalSpacing * (columns - 1))) / columns);
    }

    public void setColumns(int columns)
    {
        this.columns = columns;

        this.notifyDataSetChanged();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }
}
