package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.event.RegionInfo;
import com.swift.SWIFTRegional.viewhelper.RegionViewHelper;

public class RegionCursorAdapter extends AdCursorAdapter implements ListAdapter
{
    private static final int VIEW_TYPE_REGION = 1;

    private int paddingAd;

    public RegionCursorAdapter(Context inContext)
    {
        super(inContext);
        this.paddingAd = (int) inContext.getResources().getDimension(R.dimen.padding_medium);

    }
    public Boolean isOverridden() {
        return false;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_REGION;
        } else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(isOverridden()) {
            return super.getView(position, convertView, parent);
        }

        final Object o = this.getItem(position);

        if (o instanceof RegionInfo)
        {

            convertView = RegionViewHelper.createRegionInfoListView(convertView,parent, (RegionInfo) getItem(position));
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
            convertView.setPadding(convertView.getPaddingLeft(), this.paddingAd, convertView.getPaddingRight(), this.paddingAd);
        }

        return convertView;
    }


    public void setSeparatorEnabled(View view, int position)
    {
        final View separator = view.findViewById(R.id.separator);
        if(separator != null)
        {
            final boolean showSeparator;

            showSeparator=false;

            separator.setVisibility(showSeparator ? View.VISIBLE : View.INVISIBLE);
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        if(isOverridden()) {
            return super.getItem(position);
        }

        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final RegionInfo regionInfo  = new RegionInfo();
        regionInfo.constructFromCursor(this.getCursor());

        return regionInfo;
    }





}
