package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.fragments.programme.SessionListFragment;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.viewhelper.SessionViewHelper;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by khuru on 31-Jan-2017.
 */

public class RSessionListAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private EventTiming[] eventTimings;
    private String mode;
    private LayoutInflater inflater;

    public RSessionListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        //countries = context.getResources().getStringArray(R.array.countries);
    }

    @Override
    public int getCount() {
        return eventTimings.length;
    }

    @Override
    public Object getItem(int position) {
        return eventTimings[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Object previousItem = null;
        for (int i = position - 1; i >=0; i-- ){
            if(this.getItem(i) instanceof EventTiming){
                previousItem = this.getItem(i);
                break;
            }
        }


        Object currentItem = this.getItem(position);


        if (currentItem instanceof EventTiming)
        {
            EventTiming currentEvent = (EventTiming) currentItem;

            // Check if the date label should be shown (only if previous event is on a different day)
            boolean showDateLabel = position == 0;

            if (position > 0 && previousItem != null)
            {
                if(mode.equals("DAY"))
                {
                    showDateLabel = !(DateUtils.prettyDayString(((EventTiming) previousItem).getStartDate())
                            .equals(DateUtils.prettyDayString(currentEvent.getStartDate())));
                }
                else if(mode.equals("ROOM"))
                {
                    //different room or different date
                    showDateLabel = !(DateUtils.prettyDayString(((EventTiming) previousItem).getStartDate())
                            .equals(DateUtils.prettyDayString(currentEvent.getStartDate()))) || !(((EventTiming) previousItem).getRoomName().equals(currentEvent.getRoomName()));

                }
                else if(mode.equals("TYPE"))
                {
                    //different type or different date
                    showDateLabel = !(DateUtils.prettyDayString(((EventTiming) previousItem).getStartDate())
                            .equals(DateUtils.prettyDayString(currentEvent.getStartDate()))) || !(((EventTiming) previousItem).getEvent().getType().getSessionName().equals(currentEvent.getEvent().getType().getSessionName()));

                }
            }



            convertView = SessionViewHelper.createSessionView(convertView, parent, currentEvent, showDateLabel);
        }

        return convertView;
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        if (this.getCount() > 0)
        {
            Object item = getItem(i);
            if (!(item instanceof EventTiming))
            {
                item = getItem(i - 1);
            }

            String startDate = DateUtils.prettyDayString(((EventTiming) item).getStartDate());

            if (view == null)
            {
                view = this.inflater.inflate(R.layout.listitem_session_groupheader, viewGroup, false);

                final HeaderViewHolder holder = new HeaderViewHolder();
                holder.textViewHeaderTitle = (com.goodcoresoftware.android.common.views.TextView) view.findViewById(R.id.textView_sessionHeaderTitle);
                holder.textViewHeaderNumber = (com.goodcoresoftware.android.common.views.TextView) view.findViewById(R.id.textView_sessionHeaderNumber);
                holder.imageViewCollapseIcon = (ImageView) view.findViewById(R.id.imageView_collapseIcon);

                view.setTag(holder);
            }

            final HeaderViewHolder holder = (HeaderViewHolder) view.getTag();

            long headerId = getHeaderId(i);

            if(mode.equals("DAY"))
            {
                holder.textViewHeaderTitle.setText(startDate);
            }
            else if(mode.equals("ROOM"))
            {
                holder.textViewHeaderTitle.setText(((EventTiming) item).getRoomName());
            }else if(mode.equals("TYPE"))
            {
                holder.textViewHeaderTitle.setText(((EventTiming) item).getEvent().getType().getSessionName());
            }

            holder.textViewHeaderNumber.setText(SessionListFragment.headerCount.get(headerId) + " sessions");

            //boolean collapsed = (this.headerStates == null || !this.headerStates.containsKey(headerId) || this.headerStates.get(headerId).isCollapsed());
            //holder.imageViewCollapseIcon.setImageDrawable(ContextCompat.getDrawable(this.context, collapsed ? R.drawable.ic_colapse : R.drawable.ic_expand));


            return view;
        }
        else
        {
            return null;
        }
    }

    @Override
    public long getHeaderId(int i) {
        //return the first character of the country as ID because this is what headers are based upon
        Object item = getItem(i);
        if (!(item instanceof EventTiming))
        {
            // Advertisements don't have a date to generate a header Id from, get the exhibitor event that preceded them instead.
            item = getItem(i - 1);
        }
        return item != null ? getHeaderId((EventTiming) item) : -1;
    }


    protected long getHeaderId(EventTiming item)
    {

        if(mode.equals("DAY"))
        {
            return Long.parseLong(DateUtils.iso8601StringToComparableDateString(item.getStartDate()));
        }
        else if(mode.equals("ROOM"))
        {
            return  (long)item.getRoomName().hashCode();
        }
        else if(mode.equals("TYPE"))
        {
            return  (long)item.getEvent().getType().getSessionId().hashCode();
        }

        return 1;
    }

    public void setEventTimings(EventTiming[] eventTimings) {
        this.eventTimings = eventTimings;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    private static class HeaderViewHolder
    {
        com.goodcoresoftware.android.common.views.TextView textViewHeaderTitle;
        com.goodcoresoftware.android.common.views.TextView textViewHeaderNumber;
        ImageView imageViewCollapseIcon;
    }

}