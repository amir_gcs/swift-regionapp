package com.swift.SWIFTRegional.adapters.search;

import android.content.Context;

import com.swift.SWIFTRegional.adapters.FavoriteBaseAdapter600DP;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

public class SearchBaseAdapter600DP extends FavoriteBaseAdapter600DP
{
    public SearchBaseAdapter600DP(Context inContext, ViewLoaderStrategyInterface viewLoader)
    {
        super(inContext, viewLoader);
    }
}
