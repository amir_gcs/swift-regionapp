package com.swift.SWIFTRegional.adapters.activityfeed;

import android.content.Context;

import com.swift.SWIFTRegional.models.instagram.Instagram;

/**
 * Created by wesley on 24/06/14.
 */
public class InstagramCursorAdapter extends WallCursorAdapter
{
    public InstagramCursorAdapter(Context context)
    {
        super(context);
    }

    @Override
    public Object getItem(int position)
    {
        if(this.isAdvertisement(position))
        {
            final Object o = super.getItem(position);

            return o;
        }

        this.getCursor().moveToPosition(this.getModifiedPosition(position));

        final Instagram instagram = new Instagram();
        instagram.constructFromCursor(this.getCursor());

        return instagram;
    }
}
