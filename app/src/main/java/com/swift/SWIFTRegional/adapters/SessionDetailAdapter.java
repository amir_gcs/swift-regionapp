package com.swift.SWIFTRegional.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.provider.CalendarContract;
import android.text.Html;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.swift.SWIFTRegional.activities.BaseActivity;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.interfaces.LocateOnFloorPlanListener;
import com.swift.SWIFTRegional.interfaces.PlayYoutubeVideoListener;
import com.swift.SWIFTRegional.models.Event;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.models.session.Stream;
import com.swift.SWIFTRegional.utils.PicassoUtils;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;
import com.swift.SWIFTRegional.viewhelper.TitleViewHelper;
import com.swift.SWIFTRegional.views.IconButton;
import com.swift.SWIFTRegional.views.ReadMoreView;
import com.wefika.flowlayout.FlowLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.interfaces.RatingListener;
import com.swift.SWIFTRegional.models.ExhibitorEvent;
import com.swift.SWIFTRegional.models.Rate;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.models.session.SessionSpeaker;
import com.swift.SWIFTRegional.models.session.Tag;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.viewhelper.ExhibitorViewHelper;

public class SessionDetailAdapter extends AbstractRateAdapter
{
    public static final int VIEWTYPE_SPEAKER = 0;
    public static final int VIEWTYPE_SPEAKERS_HEADER = 1;
    public static final int VIEWTYPE_HEADER = 2;
    public static final int VIEWTYPE_TAG = 3;
    public static final int VIEWTYPE_RATE = 4;
    public static final int VIEWTYPE_SUMMARY = 5;
    public static final int VIEWTYPE_EXHIBITOR = 6;
    public static final int VIEWTYPE_EXHIBITOR_TITLE = 7;
    public static final int VIEWTYPE_ADD_TO_CALENDAR = 8;
    public static final int VIEWTYPE_MAX_COUNT = 9;
    private final int paddingSmall;
    private EventTiming eventTiming;
    private Session session;
    private ExhibitorEvent exhibitorEvent;
    private Context context;
    private boolean userIsLoggedIn;
    private RatingListener ratingListener;
    private LocateOnFloorPlanListener locateOnFloorPlanListener;
    private PlayYoutubeVideoListener playYoutubeVideoListener;

    public SessionDetailAdapter(Context context,
                                RatingListener inRatingListener, LocateOnFloorPlanListener inLocateOnFloorPlanListener, PlayYoutubeVideoListener inPlayYoutubeVideoListener)
    {
        super(context);
        this.context = context;

        this.ratingListener = inRatingListener;
        this.locateOnFloorPlanListener = inLocateOnFloorPlanListener;
        this.playYoutubeVideoListener = inPlayYoutubeVideoListener;
        this.userIsLoggedIn = UserPreferencesHelper.loggedIn(context);
        this.paddingSmall = context.getResources().getDimensionPixelSize(R.dimen.padding_small);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public int getViewTypeCount()
    {
        return VIEWTYPE_MAX_COUNT;
    }

    @Override
    public int getCount()
    {
        if (this.eventTiming == null)
        {
            return 0;
        }

        int rows = super.getCount();

        // Session header view
        rows++;

        // Summary view
        rows++;

        if (this.session != null)
        {
            // Only show rating view when user is logged in and event is a session
            if (this.userIsLoggedIn)
            {
                rows++;
            }

            if (this.session.getTags() != null && this.session.getTags().length > 0)
            {
                rows++;
            }

            int numberOfSpeakers = (this.session.getSessionSpeakers() == null ? 0 : this.session.getSessionSpeakers().length);

            if (numberOfSpeakers > 0)
            {
                rows += numberOfSpeakers;

                // Speakers header
                rows++;
            }
        }
        else if (this.exhibitorEvent != null)
        {
            // exhibitor title
            rows++;

            // exhibitor
            rows++;
        }

        // Add to Calendar button
        rows++;

        return rows;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == 0)
        {
            return VIEWTYPE_HEADER;
        }

        if (this.session != null)
        {
            if (this.userIsLoggedIn)
            {
                if (position == 1)
                {
                    return VIEWTYPE_RATE;
                }
                else if (position == 2)
                {
                    return VIEWTYPE_SUMMARY;
                }
                else if(position == getCount() - 1){
                    return VIEWTYPE_ADD_TO_CALENDAR;
                }
                else
                {
                    return this.getItemForSession(3, position);
                }
            }
            else
            {
                if (position == 1)
                {
                    return VIEWTYPE_SUMMARY;
                }
                else if(position == getCount() - 1)
                {
                    return VIEWTYPE_ADD_TO_CALENDAR;
                }
                else
                {
                    return this.getItemForSession(2, position);
                }
            }
        }
        else if (this.exhibitorEvent != null)
        {
            if (position == 1)
            {
                return VIEWTYPE_SUMMARY;
            }
            else if (position == 2)
            {
                return VIEWTYPE_EXHIBITOR_TITLE;
            }
            else if(position == getCount() - 1){
                return VIEWTYPE_ADD_TO_CALENDAR;
            }
            else
            {
                return VIEWTYPE_EXHIBITOR;
            }
        }
        else
        {
            if (position == 1)
            {
                return VIEWTYPE_SUMMARY;
            }
            else if(position == getCount() - 1){
                return VIEWTYPE_ADD_TO_CALENDAR;
            }
        }

        return -1;
    }

    private int getItemForSession(int startPosition, int currentPosition)
    {
        if (this.session != null)
        {
            int numberOfSpeakers =  this.session.getSessionSpeakers() == null ? 0
                                        : this.session.getSessionSpeakers().length;

            //perform a test on session speakers to see if there is valid data





            if (currentPosition == startPosition)
            {
                if (numberOfSpeakers == 0)
                {
                    return VIEWTYPE_TAG;
                }
                else
                {
                    return VIEWTYPE_SPEAKERS_HEADER;
                }
            }
            else if (currentPosition == startPosition + numberOfSpeakers + 1)
            {
                return VIEWTYPE_TAG;
            }
            else
            {
                return VIEWTYPE_SPEAKER;
            }
        }

        return -1;
    }

    @Override
    public Object getItem(int position)
    {
        final int type = this.getItemViewType(position);

        if (type == VIEWTYPE_SPEAKER)
        {
            if (this.session != null)
            {
                final SessionSpeaker[] speakers = this.session.getSessionSpeakers();

                position -= (this.userIsLoggedIn ? 4 : 3); // Substract header(+rating)+summary+speakers_header

                if (speakers.length > position)
                {
                    return speakers[position];
                }
            }
        }
        else if (type == VIEWTYPE_EXHIBITOR)
        {
            return this.exhibitorEvent.getExhibitor();
        }
        else
        {
            return this.eventTiming;
        }

        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object object = this.getItem(position);
        switch (getItemViewType(position))
        {
            case VIEWTYPE_HEADER:

                if (convertView == null)
                {
                    convertView = this.newHeaderView(parent);
                }

                this.bindHeaderView(convertView, (EventTiming) object);
                break;

            case VIEWTYPE_SUMMARY:

                if (convertView == null)
                {
                    convertView = this.newSummaryView(parent);
                }

                this.bindSummaryView(convertView, (EventTiming) object);
                break;

            case VIEWTYPE_RATE:

                if (convertView == null)
                {
                    convertView = this.newRateView(parent);
                }

                this.bindRateView(convertView, (EventTiming) object);
                break;

            case VIEWTYPE_TAG:

                if (convertView == null)
                {
                    convertView = this.newTagView(parent);
                }

                this.bindTagView(convertView);
                break;

            case VIEWTYPE_SPEAKERS_HEADER:

                if (convertView == null)
                {
                    convertView = this.newSpeakersHeaderView(parent);
                }

                break;

            case VIEWTYPE_SPEAKER:

                if (convertView == null)
                {
                    convertView = this.newSpeakerView(parent);
                }

                this.bindSpeakerView(convertView, (SessionSpeaker) object, position);
                break;

            case VIEWTYPE_EXHIBITOR:

                convertView = ExhibitorViewHelper.createExhibitorListView(convertView, parent, this.exhibitorEvent.getExhibitor(), true);

                break;

            case VIEWTYPE_EXHIBITOR_TITLE:

                String title = this.context.getResources().getText(R.string.exhibitor_title).toString();

                convertView = TitleViewHelper.createTitleListView(convertView, parent, title);

                break;

            case VIEWTYPE_ADD_TO_CALENDAR:

                if(convertView == null) {
                    convertView = this.newAddToCalendarView(parent);
                }

                this.bindAddToCalendarView(convertView);

                break;

            default:

                break;
        }

        return convertView;
    }

    private View newTagView(ViewGroup parent)
    {
        final View v = this.inflater.inflate(R.layout.listitem_session_details_tags, parent, false);

        final TagViewHolder holder = new TagViewHolder();
        holder.flowLayoutTags = (FlowLayout) v.findViewById(R.id.flowLayoutTags);

        v.setTag(holder);
        return v;
    }

    private View newRateView(ViewGroup parent)
    {
        final View v = this.inflater.inflate(R.layout.listitem_rate, parent, false);
        final SessionRateViewHolder holder = new SessionRateViewHolder();

        holder.ratingBarSession = (RatingBar) v.findViewById(R.id.ratingBar);
        holder.textViewRatingTitle = (TextView) v.findViewById(R.id.textView_ratingTitle);

        v.setTag(holder);
        return v;
    }

    private View newHeaderView(ViewGroup parent)
    {
        final View v = this.inflater.inflate(R.layout.listitem_session_details_header, parent, false);

        final SessionViewHolder holder = new SessionViewHolder();
        holder.textViewTitle = (TextView) v.findViewById(R.id.textView_summaryTitle);
        holder.textViewType = (TextView) v.findViewById(R.id.textView_sessionType);
        holder.textViewRoom = (TextView) v.findViewById(R.id.textView_sessionRoom);
        holder.textViewDay = (TextView) v.findViewById(R.id.textview_sessionDay);
        holder.textViewMonth = (TextView) v.findViewById(R.id.textview_sessionMonth);
        holder.textViewTime = (TextView) v.findViewById(R.id.textview_session_time);
        //holder.buttonFloorPlan = (IconButton) v.findViewById(R.id.button_locateOnFloorPlan);
        holder.buttonPlayYoutubeVideo = (Button) v.findViewById(R.id.button_youtube);
        holder.textViewStreams = (TextView) v.findViewById(R.id.textView_streams);

//        holder.buttonFloorPlan.setOnClickListener(new IconButton.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                String itemId = SessionDetailAdapter.this.exhibitorEvent == null ?
//                        Long.toString(session.getSessionId()) : exhibitorEvent.getCcvk();
//                SessionDetailAdapter.this.locateOnFloorPlanListener.locateOnFloorPlan(itemId
//                        , exhibitorEvent == null ? LocateOnFloorPlanListener.FPObjectType.SESSION : LocateOnFloorPlanListener.FPObjectType.EXHIBITOR);
//            }
//        });

        holder.buttonPlayYoutubeVideo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SessionDetailAdapter.this.playYoutubeVideoListener.playYoutubeVideo();
            }
        });

        v.setTag(holder);

        return v;
    }

    private View newSummaryView(ViewGroup parent)
    {
        final View v = inflater.inflate(R.layout.listitem_session_details_summary, parent, false);

        final SessionSummaryViewHolder holder = new SessionSummaryViewHolder();

        holder.readmoreViewSummary = (ReadMoreView) v.findViewById(R.id.readmoreView_session_details_summary);

        v.setTag(holder);

        return v;
    }

    private View newSpeakersHeaderView(ViewGroup parent)
    {
        final View v = inflater.inflate(R.layout.listitem_session_details_speakers_header, parent, false);

        return v;
    }

    private View newSpeakerView(ViewGroup parent)
    {
        final View v = inflater.inflate(R.layout.listitem_person, parent, false);

        v.findViewById(R.id.textview_person_extra).setVisibility(View.GONE);

        final SpeakerViewHolder holder = new SpeakerViewHolder();

        holder.layoutContent = v.findViewById(R.id.layout_content);

        holder.imageViewPicture = (ImageView) v.findViewById(R.id.imageview_person_profile_picture);
        holder.textViewName = (TextView) v.findViewById(R.id.textview_person_name);
        holder.textViewBusinessFunction = (TextView) v.findViewById(R.id.textview_person_business_function);
        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textview_person_company_name);
        holder.viewSpeakerSeparator = v.findViewById(R.id.separator);
        holder.imageViewModerator = (ImageView) v.findViewById(R.id.imageview_person_moderator);

        v.setTag(holder);

        return v;
    }

    private View newAddToCalendarView(ViewGroup parent)
    {
        final View v =  inflater.inflate(R.layout.listitem_session_add_to_calendar_footer, parent, false);
        final AddToCalendarHolder holder = new AddToCalendarHolder();
        holder.addToCalendarButton = (IconButton) v.findViewById(R.id.button_session_add__to_calendar);
        v.setTag(holder);
        return v;
    }

    private void bindAddToCalendarView(View view) {
        final AddToCalendarHolder holder = (AddToCalendarHolder) view.getTag();
        holder.addToCalendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCalendar();
            }
        });
    }

    public void addToCalendar() {

        // Date format for ISO-8601
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        final String location = MainActivity.selectedEventProvider.getSelectedEvent().getLocation();
        boolean foundTimeZone = false;
        if(location== null || location.equals(""))
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.timezone_notfound).setNeutralButton(R.string.message_delete_conversation_confirm, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();

                }
            });
            builder.create().show();
        }
        else
        {
            String[] timeZones = TimeZone.getAvailableIDs();

            for (String timeZone : timeZones) {
                String tz= timeZone.toLowerCase().replace("/","")
                        .replace("_","");
                String loc = location.toLowerCase().replace("/","")
                        .replace(" ","");
                if(tz.contains(loc))
                {
                    df.setTimeZone(TimeZone.getTimeZone(timeZone));
                    foundTimeZone=true;
                    break;
                }

            }

        }


        if(!foundTimeZone)
        {
            // Setting the time zone to Geneva where event will take place
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.timezone_notfound).setNeutralButton(R.string.message_delete_conversation_confirm, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                    try {

                        Date beginTime = df.parse(eventTiming.getStartDate());
                        Date endTime = df.parse(eventTiming.getEndDate());

                        Intent intent = new Intent(Intent.ACTION_INSERT)
                                .setData(CalendarContract.Events.CONTENT_URI)
                                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTime())
                                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTime())
                                .putExtra(CalendarContract.Events.HAS_ALARM,true)
                                .putExtra(CalendarContract.Reminders.MINUTES,15)
                                .putExtra(CalendarContract.Events.TITLE, eventTiming.getEvent().getTitle())
                                .putExtra(CalendarContract.Events.DESCRIPTION, Html.fromHtml(eventTiming.getEvent().getDescription()).toString())
                                .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

                        context.startActivity(intent);
                    } catch (ParseException pe) {
                        // Log parse error
                        return;
                    }
                }
            });
            builder.create().show();
        }
        else {
            try {

                Date beginTime = df.parse(eventTiming.getStartDate());
                Date endTime = df.parse(eventTiming.getEndDate());

                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTime())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTime())
                        .putExtra(CalendarContract.Events.TITLE, eventTiming.getEvent().getTitle())
                        .putExtra(CalendarContract.Events.HAS_ALARM,true)
                        .putExtra(CalendarContract.Reminders.MINUTES,15)
                        .putExtra(CalendarContract.Events.DESCRIPTION, Html.fromHtml(eventTiming.getEvent().getDescription()).toString())
                        .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                        .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

                context.startActivity(intent);
            } catch (ParseException pe) {
                // Log parse error
                return;
            }
        }



    }

    private void bindTagView(View view)
    {
        final TagViewHolder holder = (TagViewHolder) view.getTag();
        holder.flowLayoutTags.removeAllViews();

        if (this.session != null)
        {
            for (Tag tag : this.session.getTags())
            {
                TextView textView = new TextView(this.context);
                FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, this.paddingSmall, this.paddingSmall);
                textView.setTextAppearance(this.context, R.style.Sibos_TextView_Session_Details_TagName);
                textView.setLayoutParams(params);
                textView.setText(tag.getName());
                textView.setBackgroundResource(R.drawable.tag_background);
                textView.setCustomTypeFace(view.getContext().getResources().getString(R.string.typeface_light), 0);
                holder.flowLayoutTags.addView(textView);
            }
        }
    }

    private void bindSummaryView(View view, EventTiming inEventTiming)
    {
        final SessionSummaryViewHolder holder = (SessionSummaryViewHolder) view.getTag();

        final Event event = inEventTiming.getEvent();

        holder.readmoreViewSummary.setText(Html.fromHtml(event.getDescription()));
    }

    private void bindRateView(View view, EventTiming inEventTiming)
    {
        final SessionRateViewHolder holder = (SessionRateViewHolder) view.getTag();

        // rating
        holder.textViewRatingTitle.setText(this.context.getString(R.string.session_rate));
        holder.ratingBarSession.setRating(this.rate == null ? 0 : this.rate.getRating());

        // Prevent giving a rating when the session has not yet started

        if (DateUtils.iso8601StringHasStarted(inEventTiming.getStartDate()))
        {
            holder.ratingBarSession.setEnabled(true);

            holder.ratingBarSession.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener()
            {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
                {
                    if (fromUser)
                    {
                        if (SessionDetailAdapter.this.rate == null)
                        {
                            SessionDetailAdapter.this.rate = new Rate();
                        }
                        SessionDetailAdapter.this.rate.setRating((int) rating);

                        SessionDetailAdapter.this.ratingListener.ratingSelected((int) rating);
                    }
                }
            });
        }
        else
        {
            holder.ratingBarSession.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    SessionDetailAdapter.this.detector.onTouchEvent(event);
                    return true;
                }
            });
        }
    }


    GestureDetector detector = new GestureDetector(this.context, new GestureTap());

    class GestureTap extends GestureDetector.SimpleOnGestureListener
    {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e)
        {
            Toast.makeText(SessionDetailAdapter.this.context, R.string.session_rating_notstarted, Toast.LENGTH_SHORT).show();

            return true;
        }
    }

    private void bindHeaderView(View view, EventTiming inEventTiming)
    {
        final SessionViewHolder holder = (SessionViewHolder) view.getTag();

        final Event event = inEventTiming.getEvent();

        String startTime = DateUtils.prettyTimeString(context, eventTiming.getStartDate());
        String endTime = DateUtils.prettyTimeString(context, eventTiming.getEndDate());

        holder.textViewDay.setText(DateUtils.extractDayFromIso8601String(eventTiming.getStartDate()));
        holder.textViewMonth.setText(DateUtils.extractShortMonthStringFromIso8601String(eventTiming.getStartDate()));

        holder.textViewTime.setText(startTime + " - " + endTime);
        holder.textViewTitle.setText(event.getTitle());
        holder.textViewRoom.setText(eventTiming.getRoomName());
        holder.textViewType.setText(event.getSessionType());

        //holder.buttonFloorPlan.setText(inEventTiming.getRoomName());

        if (!this.playYoutubeVideoListener.containsYoutubeVideo())
        {
            holder.buttonPlayYoutubeVideo.setVisibility(View.GONE);
        }
        else
        {
            holder.buttonPlayYoutubeVideo.setVisibility(View.VISIBLE);
        }

        //Add streams
        Stream[] sessionStreams = new Stream[0];
        if (session != null)
        {
            sessionStreams = session.getStreams();
        }
        if (sessionStreams != null && sessionStreams.length > 0)
        {
            String separator = ", ";
            StringBuilder displayStreamsStr = new StringBuilder();
            for (Stream sessionStream : sessionStreams) {
                if (sessionStream.getName() != null && sessionStream.getName() != "") {
                    displayStreamsStr.append(sessionStream.getName());
                    displayStreamsStr.append(separator);
                }
            }
            String streamsStr = "";
            if (displayStreamsStr.length() != 0) {
                streamsStr = displayStreamsStr.toString().replaceAll(separator + "$", "");
            }
            holder.textViewStreams.setText(streamsStr);
            holder.textViewStreams.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.textViewStreams.setVisibility(View.GONE);
        }
    }

    private void bindSpeakerView(View view, SessionSpeaker sessionSpeaker, int position)
    {
        position -= 4;

        final SpeakerViewHolder holder = (SpeakerViewHolder) view.getTag();

        final Speaker speaker = sessionSpeaker.getSpeaker();
        if (sessionSpeaker.isModerator()){
            holder.imageViewModerator.setVisibility(View.VISIBLE);
        }else{
            holder.imageViewModerator.setVisibility(View.GONE);
        }

        holder.textViewName.setText(speaker.getFirstName() + " " + speaker.getLastName());
        holder.textViewBusinessFunction.setText(speaker.getTitle());
        holder.textViewCompanyName.setText(sessionSpeaker.getSpeakerTypeName());

        PicassoUtils.loadPersonImage(speaker.getPictureUrl(), holder.imageViewPicture);

        holder.textViewBusinessFunction.setVisibility(TextUtils.isEmpty(speaker.getTitle()) ? View.GONE : View.VISIBLE);
        holder.textViewCompanyName.setVisibility(TextUtils.isEmpty(sessionSpeaker.getSpeakerTypeName()) ? View.GONE : View.VISIBLE);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.viewSpeakerSeparator.getLayoutParams();
        int paddingLeft = (isLastSessionSpeaker(position) ? 0 : this.context.getResources().getDimensionPixelSize(R.dimen.padding_left));
        int paddingTop = (isLastSessionSpeaker(position) ? this.context.getResources().getDimensionPixelSize(R.dimen.padding_small) : 0);
        params.setMargins(paddingLeft, paddingTop, params.rightMargin, params.bottomMargin);
        holder.viewSpeakerSeparator.setLayoutParams(params);
    }

    private boolean isLastSessionSpeaker(int position)
    {
        SessionSpeaker[] sessionSpeakers = this.session.getSessionSpeakers();

        return sessionSpeakers.length - 1 == position;
    }

    @Override
    public boolean isEnabled(int position)
    {
        return getItemViewType(position) == VIEWTYPE_SPEAKER || getItemViewType(position) == VIEWTYPE_EXHIBITOR;
    }

    public EventTiming getEventTiming()
    {
        return this.eventTiming;
    }

    public void setEventTiming(EventTiming inEventTiming)
    {
        this.eventTiming = inEventTiming;

        if (this.eventTiming != null)
        {
            if (this.getEventTiming().getEvent() instanceof Session)
            {
                this.session = (Session) this.eventTiming.getEvent();
            }
            else if (this.getEventTiming().getEvent() instanceof ExhibitorEvent)
            {
                this.exhibitorEvent = (ExhibitorEvent) this.eventTiming.getEvent();
            }
        }

        this.notifyDataSetChanged();
    }

    public void setRate(Rate rate)
    {
        this.rate = rate;
    }

    private static class TagViewHolder
    {
        FlowLayout flowLayoutTags;
    }

    private static class SpeakerViewHolder
    {
        View layoutContent;
        ImageView imageViewPicture;
        TextView textViewName;
        TextView textViewBusinessFunction;
        TextView textViewCompanyName;
        View viewSpeakerSeparator;
        ImageView imageViewModerator;
    }

    private static class SessionViewHolder
    {
        TextView textViewTitle;
        TextView textViewDay;
        TextView textViewMonth;
        TextView textViewTime;
        TextView textViewStreams;
        TextView textViewType;
        TextView textViewRoom;
        IconButton buttonFloorPlan;
        Button buttonPlayYoutubeVideo;
    }

    private static class SessionSummaryViewHolder
    {
        ReadMoreView readmoreViewSummary;
    }

    private static class SessionRateViewHolder
    {
        TextView textViewRatingTitle;
        RatingBar ratingBarSession;
    }

    private static class AddToCalendarHolder
    {
        IconButton addToCalendarButton;
    }

}
