package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.viewhelper.ExhibitorViewHelper;

public class ExhibitorCursorAdapter extends StickyCursorAdapter
{
    private static final int VIEW_TYPE_EXHIBITOR = 0;

    private int paddingAd;

    public ExhibitorCursorAdapter(Context inContext)
    {
        super(inContext);
        this.paddingAd = (int) context.getResources().getDimension(R.dimen.padding_medium);

    }

    public Boolean isOverridden() {
        return false;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_EXHIBITOR;
        } else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(isOverridden()) {
            return super.getView(position, convertView, parent);
        }

        final Object o = this.getItem(position);

        if (o instanceof Exhibitor)
        {
            convertView = ExhibitorViewHelper.createExhibitorListView(convertView,parent, (Exhibitor) getItem(position));
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
            convertView.setPadding(convertView.getPaddingLeft(), this.paddingAd, convertView.getPaddingRight(), this.paddingAd);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        if(isOverridden()) {
            return super.getItem(position);
        }

        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final Exhibitor exhibitor = new Exhibitor();
        exhibitor.constructFromCursor(this.getCursor());

        return exhibitor;
    }

    @Override
    public CharSequence getHeaderText(int position)
    {
        final Object o = this.getItem(position);

        final CharSequence header;
        if(!(o instanceof Exhibitor))
        {
            final Exhibitor exhibitor = (Exhibitor) this.getItem(position - 1);
            header = exhibitor.getName();
        }else
        {
            header = ((Exhibitor)o).getName();
        }

        if(header.length()>0)
        {
            return header.subSequence(0, 1);
        }
        else
        {
            return  " ";
        }

    }
}
