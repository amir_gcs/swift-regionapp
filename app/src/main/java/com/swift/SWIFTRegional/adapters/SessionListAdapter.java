package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.swift.SWIFTRegional.adapters.search.AbstractExpandableAdAdapter;
import com.swift.SWIFTRegional.fragments.programme.SessionListFragment;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.viewhelper.SessionViewHelper;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Created by SimonRaes on 07/07/15.
 * Display a collapsible list of sessions, grouped on date (day)/room and type.
 */
public class SessionListAdapter extends AbstractExpandableAdAdapter<EventTiming>
{
    private int paddingBottom;

    public SessionListAdapter(Context inContext)
    {
        super(inContext);
        this.paddingBottom = (int) context.getResources().getDimension(R.dimen.padding_session_card);
    }

    // A very dirty hack
    public Boolean isSessionListAdapterOverridden() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // dirty hack
        if(isSessionListAdapterOverridden()){
            convertView = super.getView(position, convertView, parent);
            return convertView;
        }

        //find previous item
        Object previousItem = null;
        for (int i = position - 1; i >=0; i-- ){
            if(this.getItem(i) instanceof EventTiming){
                previousItem = this.getItem(i);
                break;
            }
        }

        this.getItem(position - 1);
        Object currentItem = this.getItem(position);
        Object nextItem = this.getItem(position + 1);

        if (currentItem instanceof EventTiming)
        {
            EventTiming currentEvent = (EventTiming) currentItem;

            // Check if the date label should be shown (only if previous event is on a different day)
            boolean showDateLabel = position == 0;

            if (position > 0 && previousItem != null)
            {
                if(SessionListFragment.selectedMode.equals("DAY"))
                {
                    showDateLabel = !(DateUtils.prettyDayString(((EventTiming) previousItem).getStartDate())
                            .equals(DateUtils.prettyDayString(currentEvent.getStartDate())));
                }
                else if(SessionListFragment.selectedMode.equals("ROOM"))
                {
                    //different room or different date
                    showDateLabel = !(DateUtils.prettyDayString(((EventTiming) previousItem).getStartDate())
                            .equals(DateUtils.prettyDayString(currentEvent.getStartDate()))) || !(((EventTiming) previousItem).getRoomName().equals(currentEvent.getRoomName()));

                }
                else if(SessionListFragment.selectedMode.equals("TYPE"))
                {
                    //different type or different date
                    showDateLabel = !(DateUtils.prettyDayString(((EventTiming) previousItem).getStartDate())
                            .equals(DateUtils.prettyDayString(currentEvent.getStartDate()))) || !(((EventTiming) previousItem).getEvent().getType().getSessionName().equals(currentEvent.getEvent().getType().getSessionName()));

                }
            }



            convertView = SessionViewHelper.createSessionView(convertView, parent, currentEvent, showDateLabel);
        }
        else
        {
            // Advertisement
            convertView = super.getView(position, convertView, parent);
        }

        // Add extra bottom padding to last child in group (if the next child has a different date or if there is no next child)
        boolean isLastChild = false;
        if (nextItem == null)
        {
            isLastChild = true;
        }
        else if (nextItem instanceof EventTiming) // won't need extra padding if next item is an ad
        {
            if (currentItem instanceof EventTiming)
            {
                isLastChild = !(DateUtils.prettyDayString(((EventTiming) currentItem).getStartDate())
                        .equals(DateUtils.prettyDayString(((EventTiming) nextItem).getStartDate())));
            }
            else
            {
                if (previousItem != null && previousItem instanceof EventTiming)
                {
                    isLastChild = !(DateUtils.prettyDayString(((EventTiming) previousItem).getStartDate())
                            .equals(DateUtils.prettyDayString(((EventTiming) nextItem).getStartDate())));
                }
            }
        }

        convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), isLastChild ? this.paddingBottom : 0);

        return convertView;
    }

    private long currentRenderedId;
    
    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup)
    {
        if (this.getCount() > 0)
        {
            Object item = getItem(i);
            if (!(item instanceof EventTiming))
            {
                item = getItem(i - 1);
            }

            String startDate = DateUtils.prettyDayString(((EventTiming) item).getStartDate());

            if (view == null)
            {
                view = this.inflater.inflate(R.layout.listitem_session_groupheader, viewGroup, false);

                final HeaderViewHolder holder = new HeaderViewHolder();
                holder.textViewHeaderTitle = (TextView) view.findViewById(R.id.textView_sessionHeaderTitle);
                holder.textViewHeaderNumber = (TextView) view.findViewById(R.id.textView_sessionHeaderNumber);
                holder.imageViewCollapseIcon = (ImageView) view.findViewById(R.id.imageView_collapseIcon);

                view.setTag(holder);
            }

            final HeaderViewHolder holder = (HeaderViewHolder) view.getTag();

            long headerId = getHeaderId(i);

            if(SessionListFragment.selectedMode.equals("DAY"))
            {
                holder.textViewHeaderTitle.setText(startDate);
            }
            else if(SessionListFragment.selectedMode.equals("ROOM"))
            {
                holder.textViewHeaderTitle.setText(((EventTiming) item).getRoomName());
            }else if(SessionListFragment.selectedMode.equals("TYPE"))
            {
                holder.textViewHeaderTitle.setText(((EventTiming) item).getEvent().getType().getSessionName());
            }

            holder.textViewHeaderNumber.setText(SessionListFragment.headerCount.get(headerId) + " sessions");

            boolean collapsed = (this.headerStates == null || !this.headerStates.containsKey(headerId) || this.headerStates.get(headerId).isCollapsed());
            holder.imageViewCollapseIcon.setImageDrawable(ContextCompat.getDrawable(this.context, collapsed ? R.drawable.ic_colapse : R.drawable.ic_expand));


            return view;
        }
        else
        {
            return null;
        }
    }

    @Override
    public long getHeaderId(int i)
    {
        Object item = getItem(i);
        if (!(item instanceof EventTiming))
        {
            // Advertisements don't have a date to generate a header Id from, get the exhibitor event that preceded them instead.
            item = getItem(i - 1);
        }
        return item != null ? getHeaderId((EventTiming) item) : -1;
    }

    @Override
    protected long getHeaderId(EventTiming item)
    {

        if(SessionListFragment.selectedMode.equals("DAY"))
        {
            return Long.parseLong(DateUtils.iso8601StringToComparableDateString(item.getStartDate()));
        }
        else if(SessionListFragment.selectedMode.equals("ROOM"))
        {
            return  (long)item.getRoomName().hashCode();
        }
        else if(SessionListFragment.selectedMode.equals("TYPE"))
        {
            return  (long)item.getEvent().getType().getSessionId().hashCode();
        }

        return 1;
    }

    @Override
    public long getItemId(int position)
    {
        return (long) position;
    }

    private static class HeaderViewHolder
    {
        TextView textViewHeaderTitle;
        TextView textViewHeaderNumber;
        ImageView imageViewCollapseIcon;
    }
}
