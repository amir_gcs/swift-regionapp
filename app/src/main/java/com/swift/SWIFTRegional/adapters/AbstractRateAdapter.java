package com.swift.SWIFTRegional.adapters;

import android.content.Context;

import com.swift.SWIFTRegional.models.Rate;

public abstract class AbstractRateAdapter extends AdCursorAdapter
{

    protected Rate rate;

    public AbstractRateAdapter (Context inContext)
    {
        super(inContext);
    }

    public Rate getRate()
    {
        return this.rate;
    }

    public void setRate(Rate inRate)
    {
        this.rate = inRate;
    }

}
