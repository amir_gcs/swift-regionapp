package com.swift.SWIFTRegional.adapters.activityfeed;

import android.content.Context;

import com.swift.SWIFTRegional.models.flickr.FlickrPhotoset;

/**
 * Created by Wesley on 19/05/14.
 */
public class FlickrCursorAdapter extends WallCursorAdapter
{
    public FlickrCursorAdapter(Context context)
    {
        super(context);
    }

    @Override
    public Object getItem(int position)
    {
        if(this.isAdvertisement(position))
        {
            final Object o = super.getItem(position);

            return o;
        }

        this.getCursor().moveToPosition(this.getModifiedPosition(position));

        final FlickrPhotoset flickrPhotoset = new FlickrPhotoset();
        flickrPhotoset.constructFromCursor(this.getCursor());

        return flickrPhotoset;
    }
}
