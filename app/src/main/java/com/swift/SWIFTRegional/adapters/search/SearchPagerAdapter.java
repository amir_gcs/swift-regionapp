//package mobi.inthepocket.sibos.adapters.search;
//
//import android.content.Context;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//
//import mobi.inthepocket.sibos.R;
//import mobi.inthepocket.sibos.fragments.search.AbstractSearchResultFragment;
//import mobi.inthepocket.sibos.fragments.search.AllSearchResultFragment;
//import mobi.inthepocket.sibos.fragments.search.FavoriteSearchResultFragment;
//
//
//public class SearchPagerAdapter extends FragmentPagerAdapter
//{
//    private Context context;
//
//    private AllSearchResultFragment allSearchResultFragment;
//    private FavoriteSearchResultFragment favoriteSearchResultFragment;
//
//    public SearchPagerAdapter(FragmentManager fm, Context inContext)
//    {
//        super(fm);
//        this.context = inContext;
//
//        this.allSearchResultFragment = new AllSearchResultFragment();
//        this.favoriteSearchResultFragment = new FavoriteSearchResultFragment();
//    }
//
//    @Override
//    public AbstractSearchResultFragment getItem(int position)
//    {
//        if(position == 0)
//            return allSearchResultFragment;
//        else
//            return favoriteSearchResultFragment;
//    }
//
//    @Override
//    public int getCount()
//    {
//        return 2;
//    }
//
//    @Override
//    public CharSequence getPageTitle(int position)
//    {
//        if(position == 0)
//            return context.getResources().getString(R.string.search_all);
//        else
//            return context.getResources().getString(R.string.search_favorites);
//    }
//}