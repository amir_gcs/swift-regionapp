package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.models.exhibitor.ExhibitorTwoItems;
import com.swift.SWIFTRegional.viewhelper.ExhibitorViewHelper600DP;

import com.swift.SWIFTRegional.R;

public class ExhibitorCursorAdapter600DP extends ExhibitorCursorAdapter
{
    private static final int VIEW_TYPE_EXHIBITOR = 0;

    private int paddingAd;

    private ViewLoaderStrategyInterface viewLoader;

    public ExhibitorCursorAdapter600DP(Context inContext, ViewLoaderStrategyInterface viewLoader)
    {
        super(inContext);
        this.viewLoader = viewLoader;
    }

    @Override
    public Boolean isOverridden() {
        return true;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_EXHIBITOR;
        } else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object o = this.getItem(position);

        if (o instanceof ExhibitorTwoItems)
        {
            convertView = ExhibitorViewHelper600DP.createExhibitorListView(convertView, this.viewLoader, parent, ((ExhibitorTwoItems) o).exhibitor1, ((ExhibitorTwoItems) o).exhibitor2);
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
            convertView.setPadding(convertView.getPaddingLeft(), this.paddingAd, convertView.getPaddingRight(), this.paddingAd);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final ExhibitorTwoItems exhibitorTwoItems = new ExhibitorTwoItems();
        exhibitorTwoItems.constructFromCursor(this.getCursor());

        return exhibitorTwoItems;
    }

    @Override
    public CharSequence getHeaderText(int position)
    {
        final Object o = this.getItem(position);

        final CharSequence header;
        if(!(o instanceof ExhibitorTwoItems))
        {
            final Exhibitor exhibitor = ((ExhibitorTwoItems) this.getItem(position - 1)).exhibitor1;
            header = exhibitor.getName();
        }else
        {
            header = ((ExhibitorTwoItems)o).exhibitor1.getName();
        }

        return header.subSequence(0, 1);
    }
}
