package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.interfaces.AdAdapterInterface;
import com.swift.SWIFTRegional.models.Advertisement;
import com.swift.SWIFTRegional.views.RatioImageView;

/**
 * Created by Wesley on 19/06/14.
 */
public abstract class AdBaseAdapter extends BaseAdapter implements AdAdapterInterface
{
    protected Context context;
    protected LayoutInflater inflater;

    private int adInterval;

    private Advertisement[] advertisements;

    public AdBaseAdapter(Context context)
    {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.adInterval = context.getResources().getInteger(R.integer.ad_interval);
    }

    @Override
    public Object getItem(int position)
    {
        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return null;
        }

        if (this.isAdvertisement(position))
        {
            return this.advertisements[(position / adInterval) % advertisements.length];
        }

        return null;
    }

    public boolean isAdvertisement(int position)
    {
        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return false;
        }

        return position > 0 && ((position % adInterval == adInterval - 1) || (position < this.adInterval - 1 && position == this.getCount() - 1));
    }

    public abstract int getItemCount();

    @Override
    public int getCount()
    {
        int count = this.getItemCount();

        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return count;
        }

        if (count > 0 && count < this.adInterval - 1)
        {
            return count + 1;
        }

        count += count / (this.adInterval - 1);

        return count;
    }

    @Override
    public void setAdvertisements(Advertisement[] advertisements)
    {
        this.advertisements = advertisements;

        this.notifyDataSetChanged();
    }

    public int getModifiedPosition(int position)
    {
        if (this.advertisements == null || this.advertisements.length == 0)
        {
            return position;
        }

        return position - position / adInterval;
    }

    public View newView(ViewGroup parent)
    {
        final View view = this.inflater.inflate(R.layout.listitem_ad, parent, false);

        final AdViewHolder adHolder = new AdViewHolder();

        adHolder.imageView = (RatioImageView) view.findViewById(R.id.imageView);
        adHolder.imageView.setRatio(AdCursorAdapter.AD_RATIO);

        view.setTag(adHolder);

        return view;
    }

    public void bindView(View view, int position)
    {
        final Object o = this.getItem(position);

        if (o instanceof Advertisement)
        {
            final Advertisement ad = (Advertisement) o;

            final AdViewHolder adHolder = (AdViewHolder) view.getTag();

            if (!TextUtils.isEmpty(ad.getPictureUrlBig()))
            {
                Picasso.with(context)
                        .load(ad.getPictureUrlBig())
                        .into(adHolder.imageView);
            }

            if(SibosUtils.isGreaterThan600DP(context)) {
                int screenWidth = SibosUtils.getScreenWidth(context);
                int padding = screenWidth / 6;
                view.setPadding(padding, adHolder.imageView.getPaddingTop(), padding, adHolder.imageView.getPaddingBottom());
            }
        }
    }

    private static class AdViewHolder
    {
        public RatioImageView imageView;
    }
}
