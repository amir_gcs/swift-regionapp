package com.swift.SWIFTRegional.adapters.activityfeed;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Simple adapter for a viewpager.
 */
public class SimplePagerAdapter extends FragmentStatePagerAdapter
{
    private ArrayList<Fragment> fragments;
    private String[] titles;
    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();


    public SimplePagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments, String[] titles)
    {
        super(fm);

        this.fragments = fragments;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int index)
    {
        return fragments.get(index);
    }

    @Override
    public int getCount()
    {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return titles[position];
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position)
    {
        return registeredFragments.get(position);
    }
}
