package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.event.Event;
import com.swift.SWIFTRegional.viewhelper.EventViewHelper;

public class EventCursorAdapter extends StickyCursorAdapter
{
    private static final int VIEW_TYPE_EVENT = 1;

    private int paddingAd;

    public EventCursorAdapter(Context inContext)
    {
        super(inContext);
        this.paddingAd = (int) context.getResources().getDimension(R.dimen.padding_medium);

    }

    public Boolean isOverridden() {
        return false;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_EVENT;
        } else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(isOverridden()) {
            return super.getView(position, convertView, parent);
        }

        final Object o = this.getItem(position);

        if (o instanceof Event)
        {
            convertView = EventViewHelper.createEventListView(convertView,parent, (Event) getItem(position));
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
            convertView.setPadding(convertView.getPaddingLeft(), this.paddingAd, convertView.getPaddingRight(), this.paddingAd);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        if(isOverridden()) {
            return super.getItem(position);
        }

        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final Event event = new Event();
        event.constructFromCursor(this.getCursor());

        return event;
    }

    @Override
    public CharSequence getHeaderText(int position)
    {
        final Object o = this.getItem(position);

        final CharSequence header;
        if(!(o instanceof Event))
        {
            final Event event = (Event) this.getItem(position - 1);
            header = event.getTitle();
        }else
        {
            header = ((Event)o).getTitle();
        }

        if(header.length()>0)
        {
            return header.subSequence(0, 1);
        }
        else
        {
            return  " ";
        }

    }
}
