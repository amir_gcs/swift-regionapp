package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.SpeakerTwoItems;
import com.swift.SWIFTRegional.viewhelper.SpeakerViewHelper600DP;

public class SpeakerCursorAdapter600DP extends SpeakerCursorAdapter
{
    private static final int VIEW_TYPE_SPEAKER = 0;

    private ViewLoaderStrategyInterface viewLoader;

    public SpeakerCursorAdapter600DP(Context inContext, ViewLoaderStrategyInterface viewLoader)
    {
        super(inContext);
        this.viewLoader = viewLoader;
    }

    @Override
    public Boolean isOverridden(){
        return true;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_SPEAKER;
        }
        else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object o = this.getItem(position);

        if (o instanceof SpeakerTwoItems)
        {
            convertView = SpeakerViewHelper600DP.createSpeakerView(convertView, this.viewLoader, parent, ((SpeakerTwoItems) o).speaker1
                    , ((SpeakerTwoItems) o).speaker2);
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final SpeakerTwoItems speakers = new SpeakerTwoItems();
        speakers.constructFromCursor(this.getCursor());

        return speakers;
    }

    @Override
    public CharSequence getHeaderText(int position)
    {
        final Object o = this.getItem(position);

        final CharSequence header;
        if (!(o instanceof SpeakerTwoItems))
        {
            final Speaker speaker = ((SpeakerTwoItems) this.getItem(position - 1)).speaker1;
            header = speaker.getLastName();
        }
        else
        {
            header = ((SpeakerTwoItems) o).speaker1.getLastName();
        }

        return header.subSequence(0, 1);
    }
}
