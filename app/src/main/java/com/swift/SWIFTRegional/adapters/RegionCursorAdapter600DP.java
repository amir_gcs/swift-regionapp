package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.event.RegionTwoItem;
import com.swift.SWIFTRegional.viewhelper.RegionInfoViewHelper600DP;

public class RegionCursorAdapter600DP extends RegionCursorAdapter
{
    private static final int VIEW_TYPE_REGION = 0;

    private ViewLoaderStrategyInterface viewLoader;

    public RegionCursorAdapter600DP(Context inContext, ViewLoaderStrategyInterface viewLoader)
    {
        super(inContext);
        this.viewLoader = viewLoader;
    }

    @Override
    public Boolean isOverridden(){
        return true;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_REGION;
        }
        else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object o = this.getItem(position);

        if (o instanceof RegionTwoItem)
        {
            convertView = RegionInfoViewHelper600DP.createRegionView(convertView, this.viewLoader, parent, ((RegionTwoItem) o).regionInfo1
                    , ((RegionTwoItem) o).regionInfo2);
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final RegionTwoItem regions  = new RegionTwoItem();
        regions.constructFromCursor(this.getCursor());

        return regions;
    }


}
