package com.swift.SWIFTRegional.adapters.activityfeed;

import android.content.Context;

import com.swift.SWIFTRegional.models.twitter.Tweet;

/**
 * Created by Wesley on 19/05/14.
 */
public class TwitterCursorAdapter extends WallCursorAdapter
{
    public TwitterCursorAdapter(Context context)
    {
        super(context);
    }

    @Override
    public Object getItem(int position)
    {
        if(this.isAdvertisement(position))
        {
            final Object o = super.getItem(position);

            return o;
        }

        this.getCursor().moveToPosition(this.getModifiedPosition(position));

        final Tweet tweet = new Tweet();
        tweet.constructFromCursor(this.getCursor());

        return tweet;
    }
}
