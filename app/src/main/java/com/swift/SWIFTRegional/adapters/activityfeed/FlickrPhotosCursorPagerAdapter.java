package com.swift.SWIFTRegional.adapters.activityfeed;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.goodcoresoftware.android.common.views.utils.RecycleBin;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.flickr.FlickrPhoto;
import uk.co.senab.photoview.PhotoView;

/**
 * Created by Wesley on 20/05/14.
 */
public class FlickrPhotosCursorPagerAdapter extends PagerAdapter
{
    private Cursor cursor;

    public FlickrPhotosCursorPagerAdapter(Context inContext)
    {
    }

    public Cursor getCursor()
    {
        return this.cursor;
    }

    public void swapCursor(Cursor inCursor)
    {
        this.cursor = inCursor;

        if (this.cursor != null)
        {
            this.cursor.moveToFirst();
            this.notifyDataSetChanged();
        }
    }

    public FlickrPhoto getItem(Cursor cursor)
    {
        final FlickrPhoto photo = new FlickrPhoto();
        photo.constructFromCursor(cursor);

        return photo;
    }

    @Override
    public int getCount()
    {
        return this.cursor == null ? 0 : this.cursor.getCount();
    }

    public View getView(ViewGroup container, Cursor cursor, int position)
    {
        FlickrPhotoView view = (FlickrPhotoView) RecycleBin.get(FlickrPhotoView.class);
        if (view == null)
        {
            view = new FlickrPhotoView(container.getContext());
        }

        cursor.moveToPosition(position);
        final FlickrPhoto photo = this.getItem(cursor);

        @SuppressLint("WrongViewCast")
        final PhotoView imageView = (PhotoView) view.findViewById(R.id.imageView_photo);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        imageView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        Picasso.with(view.getContext())
                .load(photo.getPictureUrlLarge())
                .into(imageView, new Callback()
                {
                    @Override
                    public void onSuccess()
                    {
                        imageView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError()
                    {
                    }
                });

        return view;
    }

    @Override
    public final Object instantiateItem(ViewGroup container, int position)
    {
        final View view = getView(container, this.cursor, position);

        container.addView(view);

        return view;
    }

    @Override
    public boolean isViewFromObject(View inView, Object inObject)
    {
        return inView == inObject;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        final View view = (View) object;

        container.removeView(view);

        this.destroyView(view);
    }

    public void destroyView(View view)
    {
        RecycleBin.add(view);
    }

    public class FlickrPhotoView extends RelativeLayout
    {
        public FlickrPhotoView(Context context)
        {
            this(context, null);
        }

        public FlickrPhotoView(Context context, AttributeSet attrs)
        {
            super(context, attrs);

            LayoutInflater.from(context).inflate(R.layout.pager_item_photo, this);
        }
    }
}
