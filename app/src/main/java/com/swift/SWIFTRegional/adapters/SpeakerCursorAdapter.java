package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.viewhelper.SpeakerViewHelper;

public class SpeakerCursorAdapter extends StickyCursorAdapter
{
    private static final int VIEW_TYPE_SPEAKER = 0;

    public SpeakerCursorAdapter(Context inContext)
    {
        super(inContext);
    }

    public Boolean isOverridden() {
        return false;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_SPEAKER;
        }
        else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(isOverridden()){
            return super.getView(position, convertView, parent);
        }

        final Object o = this.getItem(position);

        if (o instanceof Speaker)
        {
            convertView = SpeakerViewHelper.createSpeakerView(convertView, parent, (Speaker) o);
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        if(isOverridden()){
            return super.getItem(position);
        }

        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final Speaker speaker = new Speaker();
        speaker.constructFromCursor(this.getCursor());

        return speaker;
    }

    @Override
    public CharSequence getHeaderText(int position)
    {
        final Object o = this.getItem(position);

        final CharSequence header;
        if (!(o instanceof Speaker))
        {
            final Speaker speaker = (Speaker) this.getItem(position - 1);
            header = speaker.getLastName();
        }
        else
        {
            header = ((Speaker) o).getLastName();
        }

        if(header.length()>0)
        {
            return header.subSequence(0, 1);
        }
        else
        {
            //last name in mock service needs lastname
            return  " ";
        }

    }
}
