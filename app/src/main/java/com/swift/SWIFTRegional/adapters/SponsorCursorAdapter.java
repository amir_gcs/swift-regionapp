package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.viewhelper.SponsorViewHelper;

public class SponsorCursorAdapter extends StickyCursorAdapter
{
    private static final int VIEW_TYPE_SPONSOR = 1;

    private int paddingAd;

    public SponsorCursorAdapter(Context inContext)
    {
        super(inContext);
        this.paddingAd = (int) context.getResources().getDimension(R.dimen.padding_medium);

    }

    public Boolean isOverridden() {
        return false;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_SPONSOR;
        } else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(isOverridden()) {
            return super.getView(position, convertView, parent);
        }

        final Object o = this.getItem(position);

        if (o instanceof Sponsor)
        {
            convertView = SponsorViewHelper.createSponsorListView(convertView,parent, (Sponsor) getItem(position));
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
            convertView.setPadding(convertView.getPaddingLeft(), this.paddingAd, convertView.getPaddingRight(), this.paddingAd);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        if(isOverridden()) {
            return super.getItem(position);
        }

        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final Sponsor sponsor = new Sponsor();
        sponsor.constructFromCursor(this.getCursor());

        return sponsor;
    }

    @Override
    public CharSequence getHeaderText(int position)
    {
        final Object o = this.getItem(position);

        final CharSequence header;
        if(!(o instanceof Sponsor))
        {
            final Sponsor sponsor = (Sponsor) this.getItem(position - 1);
            header = sponsor.getName();
        }else
        {
            header = ((Sponsor)o).getName();
        }

        if(header.length()>0)
        {
            return header.subSequence(0, 1);
        }
        else
        {
            return  " ";
        }

    }
}
