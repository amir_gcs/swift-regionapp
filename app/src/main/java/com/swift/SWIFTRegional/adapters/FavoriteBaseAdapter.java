package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.News;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.models.flickr.FlickrPhotoset;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.viewhelper.SessionViewHelper;
import com.swift.SWIFTRegional.viewhelper.SponsorViewHelper;
import com.swift.SWIFTRegional.viewhelper.WallViewHelper;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.models.instagram.Instagram;
import com.swift.SWIFTRegional.models.twitter.Tweet;
import com.swift.SWIFTRegional.models.video.Video;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.viewhelper.ExhibitorViewHelper;
import com.swift.SWIFTRegional.viewhelper.SpeakerViewHelper;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class FavoriteBaseAdapter extends AdBaseAdapter implements StickyListHeadersAdapter
{
    public static final int VIEWTYPE_SPEAKER = 0;
    public static final int VIEWTYPE_SESSION = 1;
    public static final int VIEWTYPE_SPONSOR = 2;
    public static final int VIEWTYPE_TWITTER = 3;
    public static final int VIEWTYPE_VIDEO = 4;

    public static final int VIEWTYPE_MAX_COUNT = 5;

    private EventTiming[] eventTimings;
    private Sponsor[] sponsors;
    private Speaker[] speakers;

    protected Tweet[] tweets;
    protected News[] newsList;
    protected FlickrPhotoset[] photosets;
    protected Instagram[] instagramSets;
    protected Video[] videos;

    protected String[] headers;

    protected int paddingCard;

    protected Object[] getLists(){
        return new Object[]{speakers, eventTimings, sponsors
                , tweets, videos};
    }

    public FavoriteBaseAdapter(Context inContext)
    {
        super(inContext);

        this.headers = new String[VIEWTYPE_MAX_COUNT];
        this.headers[0] = this.context.getResources().getString(R.string.favorite_speakers);
        this.headers[1] = this.context.getResources().getString(R.string.favorite_sessions);
        this.headers[2] = this.context.getResources().getString(R.string.favorite_sponsors);
        this.headers[3] = this.context.getResources().getString(R.string.favorite_twitter);
        this.headers[4] = this.context.getResources().getString(R.string.favorite_videos);

        this.paddingCard = (int) context.getResources().getDimension(R.dimen.padding_session_card);

    }

    public Boolean isOverridden() {
        return false;
    }

    @Override
    public int getItemCount()
    {
        Integer totalCount = 0;
        for(Integer i = 0; i < getLists().length; i++) {
            totalCount += SibosUtils.getNullSafeLength((Object[]) getLists()[i]);
        }

        return totalCount;
    }


    @Override
    public Object getItem(int position)
    {
        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final int itemViewType = this.getItemViewType(position);

        position = this.getModifiedPosition(position);

        Integer totalCount = 0;
        for(Integer i = 0; i < getLists().length; i++) {
            if(itemViewType == i) {
                Object[] arr = (Object[]) getLists()[i];
                return arr != null && arr.length > 0 ? arr[position - totalCount] : null;
            }
            totalCount += SibosUtils.getNullSafeLength((Object[])getLists()[i]);
        }

        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (this.isAdvertisement(position))
        {
            return this.getViewTypeCount() - 1;
        }

        position = this.getModifiedPosition(position);

        Integer totalCount = 0;
        for(Integer i = 0; i < getLists().length; i++) {
            totalCount += SibosUtils.getNullSafeLength((Object[])getLists()[i]);
            if(totalCount - position > 0 ) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int getViewTypeCount()
    {
        return VIEWTYPE_MAX_COUNT + super.getViewTypeCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        switch (getItemViewType(position))
        {
            case VIEWTYPE_SESSION:

                convertView = SessionViewHelper.createSessionView(convertView, parent, (EventTiming) getItem(position), this.sessionShouldShowDate(position));
                convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), isLastSession(position) ? this.paddingCard : 0);

                break;

            case VIEWTYPE_SPEAKER:

                convertView = SpeakerViewHelper.createSpeakerView(convertView, parent, (Speaker) getItem(position));

                break;

            case VIEWTYPE_SPONSOR:

                convertView = SponsorViewHelper.createSponsorListView(convertView, parent, (Sponsor) getItem(position));

                break;

            case VIEWTYPE_TWITTER:

                if (convertView == null)
                {
                    convertView = WallViewHelper.newTwitterView(parent, context);
                }

                WallViewHelper.bindTwitterView(convertView, convertView.getContext(), (WallItem) getItem(position));

                break;



            case VIEWTYPE_VIDEO:

                if(convertView == null) {
                    convertView = WallViewHelper.newVideoView(parent, context);
                }

                WallViewHelper.bindVideoView(convertView,convertView.getContext(), (WallItem) getItem(position));

                break;

            default:
                if (convertView == null)
                {
                    convertView = super.newView(parent);
                }

                super.bindView(convertView, position);

                if (position - 1 >= 0 && position + 1 < this.getCount())
                {
                    Object previousListItem = getItem(position - 1);
                    Object nextListItem = getItem(position + 1);

                    boolean surroundedByEvents = ((previousListItem != null && previousListItem instanceof EventTiming) &&
                            (nextListItem != null && nextListItem instanceof EventTiming));

                    convertView.setPadding(convertView.getPaddingLeft(), surroundedByEvents ? this.paddingCard : 0, convertView.getPaddingRight(), convertView.getPaddingBottom());
                }

                break;
        }

        return convertView;
    }

    private boolean isLastSession(int position)
    {
        return eventTimings.length - 1 == getAdjustedSessionPosition(position);
    }

    private boolean sessionShouldShowDate(int position)
    {
        boolean shouldShowDate = true;
        if (this.eventTimings.length > 1 && this.getAdjustedSessionPosition(position) > 0)
        {
            int previousTimingPositionOffset = this.getItem(position - 1) instanceof EventTiming ? 1 : 2;
            String previousDate = DateUtils.prettyDayString(((EventTiming) this.getItem(position - previousTimingPositionOffset)).getStartDate());
            String currentDate = DateUtils.prettyDayString(((EventTiming) this.getItem(position)).getStartDate());
            if (previousDate.equals(currentDate))
            {
                shouldShowDate = false;
            }
        }
        return shouldShowDate;
    }

    private int getAdjustedSessionPosition(int position)
    {
        position = this.getModifiedPosition(position);

        if (this.speakers != null && speakers.length > 0)
        {
            // Speakers
            position -= speakers.length;
        }

        return position;
    }

    @Override
    public View getHeaderView(int position, View view, ViewGroup viewGroup)
    {
        int currentOrientation = this.context.getResources().getConfiguration().orientation;

        int viewOrientation = -1;
        if (view != null)
        {
            viewOrientation = view.getTag() != null ? (Integer) view.getTag() : -1;
        }

        if (view == null || viewOrientation != currentOrientation)
        {
            view = this.inflater.inflate(R.layout.listitem_sticky_header, viewGroup, false);
        }

        final TextView headerTextView = (TextView) view;

        headerTextView.setText(getHeaderText(position));

        return view;
    }

    @Override
    public long getHeaderId(int position)
    {
        return this.getHeaderText(position).hashCode();
    }

    protected String getHeaderText(int position)
    {
        int itemViewType = this.getItemViewType(position);

        //get item view type for ad by looking at the view type of the previous item
        if (itemViewType == this.getViewTypeCount() - 1)
        {
            itemViewType = this.getItemViewType(position - 1);
        }

        if(itemViewType >= 0 && itemViewType < VIEWTYPE_MAX_COUNT) {
            return this.headers[itemViewType];
        }

        return "";
    }

    public void setEventTimings(EventTiming[] eventTimings)
    {
        this.eventTimings = eventTimings;
    }

    public void setSponsors(Sponsor[] sponsors)
    {
        this.sponsors = sponsors;
    }

    public void setSpeakers(Speaker[] speakers)
    {
        this.speakers = speakers;
    }

    public void setTweets(Tweet[] tweets) { this.tweets = tweets; }

    public void setNews(News[] newsList) { this.newsList = newsList; }

    public void setPhotosets(FlickrPhotoset[] photosets) { this.photosets = photosets; }

    public void setInstagramSets(Instagram[] instagramSets) { this.instagramSets = instagramSets;}

    public void setVideos(Video[] videos) { this.videos = videos;}
}