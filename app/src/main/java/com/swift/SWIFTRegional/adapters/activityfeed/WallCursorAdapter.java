package com.swift.SWIFTRegional.adapters.activityfeed;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.models.WallItem;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.adapters.AdCursorAdapter;
import com.swift.SWIFTRegional.viewhelper.WallViewHelper;

public class WallCursorAdapter extends AdCursorAdapter
{
   public enum ViewType
    {
        TWITTER,
        FLICKR,
        NEWS,
        ISSUES,
        INSTAGRAM,
        VIDEO
    }

    public WallCursorAdapter(Context context)
    {
        super(context);

        WallViewHelper.paddingSmall = context.getResources().getDimensionPixelSize(R.dimen.padding_small);
        WallViewHelper.paddingMedium = context.getResources().getDimensionPixelSize(R.dimen.padding_medium);

        setAdLayoutId(R.layout.listitem_ad_cards);
    }

    @Override
    public Object getItem(int position)
    {
        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final WallItem wallItem = new WallItem();
        wallItem.constructFromCursor(this.getCursor());

        return wallItem;
    }

    @Override
    public int getItemViewType(int position)
    {
        final Object object = this.getItem(position);

        if (object instanceof WallItem)
        {
            final WallItem wallItem = (WallItem) object;

            switch (wallItem.getType())
            {
                case TWITTER_NAME:
                case TWITTER_HASHTAG:
                    return ViewType.TWITTER.ordinal();
                case FLICKR_PHOTOSET:
                    return ViewType.FLICKR.ordinal();
                case NEWS:
                    return ViewType.NEWS.ordinal();
                case ISSUE:
                    return ViewType.ISSUES.ordinal();
                case INSTAGRAM_IMAGE:
                case INSTAGRAM_VIDEO:
                    return ViewType.INSTAGRAM.ordinal();
                case VIDEO:
                    return ViewType.VIDEO.ordinal();
            }
        }
        else
        {
            return this.getViewTypeCount() - 1;
        }

        return -1;
    }

    @Override
    public int getViewTypeCount()
    {
        return ViewType.values().length + super.getViewTypeCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object o = this.getItem(position);

        if (o instanceof WallItem)
        {
            final int itemViewType = this.getItemViewType(position);

            final ViewType viewType = ViewType.values()[itemViewType];

            final WallItem wallItem = (WallItem) o;

            switch (viewType)
            {
                case TWITTER:
                    if (convertView == null)
                    {
                        convertView = WallViewHelper.newTwitterView(parent, mContext);
                    }

                    WallViewHelper.bindTwitterView(convertView, convertView.getContext(), wallItem);

                    break;

                case FLICKR:
                    if (convertView == null)
                    {
                        convertView = WallViewHelper.newFlickrView(parent, mContext);
                    }

                    WallViewHelper.bindFlickrView(convertView, convertView.getContext(), wallItem);

                    break;
                case NEWS:
                    if (convertView == null)
                    {
                        convertView = WallViewHelper.newNewsView(parent, mContext);
                    }

                    WallViewHelper.bindNewsView(convertView, convertView.getContext(), wallItem);

                    break;
                case ISSUES:
                    if (convertView == null)
                    {
                        convertView = WallViewHelper.newIssuesView(parent, mContext);
                    }

                    WallViewHelper.bindIssueView(convertView, convertView.getContext(), wallItem);

                    break;
                case INSTAGRAM:
                    if (convertView == null)
                    {
                        convertView = WallViewHelper.newInstagramView(parent, mContext);
                    }

                    WallViewHelper.bindInstagramView(convertView, convertView.getContext(), wallItem);

                    break;

                case VIDEO:
                    if (convertView == null)
                    {
                        convertView = WallViewHelper.newVideoView(parent, mContext);
                    }

                    WallViewHelper.bindVideoView(convertView, convertView.getContext(), wallItem);

                    break;
            }
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }



    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
    }




}

