package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.models.SpeakerTwoItems;
import com.swift.SWIFTRegional.models.SponsorTwoItems;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.viewhelper.SpeakerViewHelper600DP;
import com.swift.SWIFTRegional.viewhelper.SponsorViewHelper600DP;

public class SponsorCursorAdapter600DP extends SponsorCursorAdapter
{
    private static final int VIEW_TYPE_SPONSOR = 0;

    private ViewLoaderStrategyInterface viewLoader;

    public SponsorCursorAdapter600DP(Context inContext, ViewLoaderStrategyInterface viewLoader)
    {
        super(inContext);
        this.viewLoader = viewLoader;
    }

    @Override
    public Boolean isOverridden(){
        return true;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (!this.isAdvertisement(position))
        {
            return VIEW_TYPE_SPONSOR;
        }
        else
        {
            return this.getViewTypeCount() - 1;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return super.getViewTypeCount() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object o = this.getItem(position);

        if (o instanceof SponsorTwoItems)
        {
            convertView = SponsorViewHelper600DP.createSpeakerView(convertView, this.viewLoader, parent, ((SponsorTwoItems) o).sponsor1
                    , ((SponsorTwoItems) o).sponsor2);
            this.setSeparatorEnabled(convertView, position);
        }
        else
        {
            convertView = super.getView(position, convertView, parent);
        }

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {

    }

    @Override
    public Object getItem(int position)
    {
        final Object o = super.getItem(position);

        if (o != null)
        {
            return o;
        }

        final SponsorTwoItems sponsors = new SponsorTwoItems();
        sponsors.constructFromCursor(this.getCursor());


        return sponsors;
    }

    @Override
    public CharSequence getHeaderText(int position)
    {
        final Object o = this.getItem(position);

        final CharSequence header;
        if (!(o instanceof SponsorTwoItems))
        {
            final Sponsor speaker = ((SponsorTwoItems) this.getItem(position - 1)).sponsor1;
            header = speaker.getName();
        }
        else
        {
            header = ((SponsorTwoItems) o).sponsor1.getName();
        }

        if(header.length()>0)
        {
            return header.subSequence(0, 1);
        }
        else
        {
            return  " ";
        }
    }
}
