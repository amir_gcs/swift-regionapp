package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Wesley on 26/05/14.
 */
public abstract class StickyCursorAdapter extends AdCursorAdapter implements StickyListHeadersAdapter
{
    protected Context context;

    public StickyCursorAdapter(Context context)
    {
        super(context);

        this.context = context;
   }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        return super.getView(position, convertView, parent);
    }

    @Override
    public View getHeaderView(int position, View view, ViewGroup viewGroup)
    {
        int currentOrientation = this.context.getResources().getConfiguration().orientation;

        int viewOrientation = -1;
        if(view != null)
        {
            viewOrientation = view.getTag() != null ? (Integer) view.getTag() : -1;
        }

        if(view == null || viewOrientation != currentOrientation)
        {
            view = this.inflater.inflate(R.layout.listitem_sticky_header, viewGroup, false);
        }

        final TextView headerTextView = (TextView) view;
        headerTextView.setText(this.getHeaderText(position).toString().toUpperCase());

        headerTextView.setTag(currentOrientation);

        return view;
    }

    public void setSeparatorEnabled(View view, int position)
    {
        final View separator = view.findViewById(R.id.separator);
        if(separator != null)
        {
            final boolean showSeparator;

            if(this.getModifiedPosition(position) < this.getCursor().getCount() - 1)
            {
                final long currentHeaderId = this.getHeaderId(position);
                final long nextHeaderId = this.getHeaderId(position + 1);
                showSeparator = !this.isAdvertisement(position + 1) && currentHeaderId == nextHeaderId;

            }else
            {
                showSeparator = false;
            }

            separator.setVisibility(showSeparator ? View.VISIBLE : View.INVISIBLE);
        }
    }

    @Override
    public long getHeaderId(int position)
    {
        return this.getHeaderText(position).toString().toUpperCase().charAt(0);
    }

    public abstract CharSequence getHeaderText(int position);
}
