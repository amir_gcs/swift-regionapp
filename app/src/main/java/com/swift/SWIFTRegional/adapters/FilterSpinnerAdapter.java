package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.spinner.SpinnerItem;

/**
 * Created by SimonRaes on 9/04/15.
 * Adapter that populates the dropdown spinner in the toolbar.
 */
public class FilterSpinnerAdapter extends BaseAdapter
{
    public static final int VIEWTYPE_DEFAULT = 0;
    public static final int VIEWTYPE_TITLE = 1;

    private LayoutInflater inflater;

    private Context context;

    private SpinnerItem[] listItems;
    private int colorStateList;

    public FilterSpinnerAdapter(Context context, SpinnerItem[] items, int colorSelector)
    {
        super();
        this.context = context;
        this.inflater = LayoutInflater.from(context);

        this.listItems = items;
        this.colorStateList = colorSelector;
    }

    public void setItems(final SpinnerItem[] items, final int colorselector)
    {
        this.listItems = items;
        this.colorStateList = colorselector;
        this.notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount()
    {
        // MUST be one - spinner does not support more
        return 1;
    }

    @Override
    public int getCount()
    {
        int count = 0;

        for (SpinnerItem item : listItems)
        {
            count++;

            if (item.getChildItems() != null && item.getChildItems().length > 0)
            {
                count += item.getChildItems().length;
            }
        }

        return count;
    }

    @Override
    public int getItemViewType(int position)
    {
        int i = 0;

        for (SpinnerItem item : listItems)
        {
            if (item.getChildItems() != null && item.getChildItems().length > 0)
            {
                // title
                if (i == position)
                {
                    return VIEWTYPE_TITLE;
                }
                else if (position > i && position <= i + item.getChildItems().length)
                {
                    // requested item is part of the children of this node
                    return VIEWTYPE_DEFAULT;
                }
                else
                {
                    // item is not in this list of children
                    i += item.getChildItems().length;
                }
            }
            else
            {
                // has no child nodes, is a normal item
                if (i == position)
                {
                    return VIEWTYPE_DEFAULT;
                }
            }
            i++;
        }

        // Shouldn't be possible to reach this
        return VIEWTYPE_TITLE;
    }

    @Override
    public Object getItem(int position)
    {
        int i = 0;

        for (SpinnerItem item : listItems)
        {
            if (i == position)
            {
                return item;
            }
            else if (item.getChildItems() != null && item.getChildItems().length > 0)
            {
                if (position > i && position <= i + item.getChildItems().length)
                {
                    // requested item is a child of this node
                    return item.getChildItems()[position - i - 1];
                }

                // item is not in this list of children
                i += item.getChildItems().length;
            }
            i++;
        }

        return null;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }


    @Override
    public boolean isEnabled(int position)
    {
        return getItemViewType(position) != VIEWTYPE_TITLE;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null || !(convertView.getTag() instanceof SelectionViewHolder))
        {
            convertView = newSelectionViewHolder(parent);
        }
        this.bindSelectionViewHolder(convertView, position);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        int viewType = getItemViewType(position);
        switch (viewType)
        {
            default:
            case VIEWTYPE_DEFAULT:

                if (convertView == null || !(convertView.getTag() instanceof ChildViewHolder))
                {
                    convertView = newChildView(parent);
                }
                this.bindChildView(convertView, position);

                break;

            case VIEWTYPE_TITLE:

                if (convertView == null || !(convertView.getTag() instanceof TitleViewHolder))
                {
                    convertView = newTitleView(parent);
                }
                this.bindTitleView(convertView, position);
                break;

        }
        return convertView;
    }


    private static class SelectionViewHolder
    {
        TextView textView;
    }

    private View newSelectionViewHolder(ViewGroup parent)
    {
        final View view = this.inflater.inflate(R.layout.spinneritem_selection, parent, false);

        final SelectionViewHolder holder = new SelectionViewHolder();

        holder.textView = (TextView) view.findViewById(R.id.textView_spinneritem_dropdown);

        view.setTag(holder);

        return view;
    }

    private void bindSelectionViewHolder(View view, int position)
    {
        SpinnerItem item = (SpinnerItem) getItem(position);

        final SelectionViewHolder viewHolder = (SelectionViewHolder) view.getTag();
        viewHolder.textView.setText(item.getTitle());
    }



    private View newTitleView(ViewGroup parent)
    {
        final View view = this.inflater.inflate(R.layout.spinneritem_title, parent, false);

        final TitleViewHolder holder = new TitleViewHolder();

        holder.layoutItem = (LinearLayout) view.findViewById(R.id.layout_spinner_item_title);
        holder.textView = (TextView) view.findViewById(R.id.textView_spinner_title);

        view.setTag(holder);

        return view;
    }

    private void bindTitleView(View view, int position)
    {
        SpinnerItem item = (SpinnerItem) getItem(position);

        final TitleViewHolder viewHolder = (TitleViewHolder) view.getTag();

        this.setBackgroundForPosition(viewHolder.layoutItem, position);

        viewHolder.textView.setText(item.getTitle());
    }

    private View newChildView(ViewGroup parent)
    {
        final View view = this.inflater.inflate(R.layout.spinneritem_program_child, parent, false);

        final ChildViewHolder holder = new ChildViewHolder();

        holder.layoutItem = (LinearLayout) view.findViewById(R.id.layout_spinner_item_child);
        holder.textView = (TextView) view.findViewById(R.id.textView_spinneritem_dropdown);
        holder.imageView = (ImageView) view.findViewById(R.id.imageView_spinner);
        holder.separator = view.findViewById(R.id.spinner_item_divider);

        view.setTag(holder);

        return view;
    }

    private void bindChildView(View view, int position)
    {
        SpinnerItem item = (SpinnerItem) getItem(position);

        final ChildViewHolder viewHolder = (ChildViewHolder) view.getTag();
        viewHolder.textView.setText(item.getTitle());

        viewHolder.textView.setTextColor(this.context.getResources().getColorStateList(colorStateList));

        this.setBackgroundForPosition(viewHolder.layoutItem, position);

        if (item.getDrawable() > 0)
        {
            viewHolder.imageView.setVisibility(View.VISIBLE);
            viewHolder.imageView.setImageResource(item.getDrawable());
        }
        else
        {
            viewHolder.imageView.setVisibility(View.GONE);
        }

        viewHolder.separator.setVisibility(position == getCount() - 1 ? View.GONE :
                item.isDivider() ? View.VISIBLE : View.GONE);
    }

    private void setBackgroundForPosition(View layoutItem, int position)
    {
        if(position == 0)
        {
            layoutItem.setBackgroundResource(R.drawable.filter_item_top_background);
        }
        else if(position == getCount() - 1)
        {
            layoutItem.setBackgroundResource(R.drawable.filter_item_bottom_background);
        }
        else
        {
            layoutItem.setBackgroundColor(layoutItem.getContext().getResources().getColor(R.color.white));
        }
    }

    private static class TitleViewHolder
    {
        LinearLayout layoutItem;
        TextView textView;
    }

    private static class ChildViewHolder
    {
        LinearLayout layoutItem;
        TextView textView;
        ImageView imageView;
        View separator;
    }
}
