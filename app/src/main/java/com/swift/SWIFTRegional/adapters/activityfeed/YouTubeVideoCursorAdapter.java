package com.swift.SWIFTRegional.adapters.activityfeed;

import android.content.Context;

import com.swift.SWIFTRegional.fragments.activityfeed.VideoFragment;
import com.swift.SWIFTRegional.models.video.Video;

/**
 * Created by wesley on 30/06/14.
 */
public class YouTubeVideoCursorAdapter extends WallCursorAdapter
{
    public YouTubeVideoCursorAdapter(Context context)
    {
        super(context);
    }

    @Override
    public Object getItem(int position)
    {
        if(this.isAdvertisement(position))
        {
            final Object o = super.getItem(position);

            return o;
        }

        this.getCursor().moveToPosition(this.getModifiedPosition(position));

        final Video video = new Video();
        video.constructFromCursor(this.getCursor());
        return video;
    }
}
