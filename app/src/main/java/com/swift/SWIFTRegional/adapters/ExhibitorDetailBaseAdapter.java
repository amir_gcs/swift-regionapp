package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.swift.SWIFTRegional.interfaces.LocateOnFloorPlanListener;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.exhibitor.ExhibitorCollateral;
import com.swift.SWIFTRegional.utils.PicassoUtils;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;
import com.swift.SWIFTRegional.viewhelper.SessionViewHelper;
import com.swift.SWIFTRegional.views.NotLoggedInView;
import com.swift.SWIFTRegional.views.ReadMoreView;

import java.util.ArrayList;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;
import com.swift.SWIFTRegional.utils.DateUtils;

public class ExhibitorDetailBaseAdapter extends BaseAdapter
{
    public static final int VIEWTYPE_EXHIBITOR_GENERAL = 0;
    public static final int VIEWTYPE_EXHIBITOR_COMPANY = 1;
    public static final int VIEWTYPE_EXHIBITOR_PRODUCTS = 2;
    public static final int VIEWTYPE_EXHIBITOR_COLLATERAL = 3;
    public static final int VIEWTYPE_EXHIBITOR_COLLATERAL_TITLE = 4;
    public static final int VIEWTYPE_EXHIBITOR_REPRESENTATIVE = 5;
    public static final int VIEWTYPE_EXHIBITOR_REPRESENTATIVE_TITLE = 6;
    public static final int VIEWTYPE_EXHIBITOR_EVENT = 7;
    public static final int VIEWTYPE_EXHIBITOR_EVENT_TITLE = 8;
    public static final int VIEWTYPE_EMPTY = 9;
    public static final int VIEWTYPE_NOT_LOGGED_IN = 10;
    public static final int VIEWTYPE_ERROR_MESSAGE = 11;
    public static final int VIEWTYPE_MAX_COUNT = 14;
    public static final int VIEWTYPE_EXHIBITOR_SESSION = 12;
    public static final int VIEWTYPE_EXHIBITOR_SESSION_TITLE = 13;


    private Exhibitor exhibitor;
    private ArrayList<ExhibitorCollateral> exhibitorCollaterals;
    private EventTiming[] exhibitorEvents;

    private EventTiming[] exhibitorSessions;

    private Context context;
    private LayoutInflater inflater;

    private LocateOnFloorPlanListener locateOnFloorPlanListener;

    private final int paddingBottom;
    private final int paddingSession;

    public ExhibitorDetailBaseAdapter(Context inContext, LocateOnFloorPlanListener inLocateOnFloorPlanListener)
    {
        this.context = inContext;
        this.inflater = LayoutInflater.from(inContext);
        this.locateOnFloorPlanListener = inLocateOnFloorPlanListener;

        this.paddingBottom = inContext.getResources().getDimensionPixelSize(R.dimen.padding_xxlarge);
        this.paddingSession = inContext.getResources().getDimensionPixelSize(R.dimen.padding_session_last_card_in_details);
    }

    @Override
    public int getViewTypeCount()
    {
        return VIEWTYPE_MAX_COUNT;
    }

    @Override
    public int getCount()
    {
        if (this.exhibitor == null)
        {
            return 0;
        }

        int rows = 1;
        if (!TextUtils.isEmpty(this.exhibitor.getDescription()))
        {
            rows++;
        }

        if (!TextUtils.isEmpty(this.exhibitor.getProductsOnShow()))
        {
            rows++;
        }

        if(this.exhibitorSessions != null && this.exhibitorSessions.length > 0) {
            rows += this.exhibitorSessions.length + 1;
        }

        if (this.exhibitorCollaterals != null && this.exhibitorCollaterals.size() > 0)
        {
            rows += this.exhibitorCollaterals.size() + 1;
        }else if (this.exhibitorCollaterals == null || this.exhibitorCollaterals.size() == 0)
        {
            rows += 1 + 1;
        }

        if (this.exhibitorEvents != null && this.exhibitorEvents.length > 0)
        {
            rows += this.exhibitorEvents.length + 1;
        }else if (this.exhibitorEvents == null || this.exhibitorEvents.length == 0)
        {
            rows += 1 + 1;
        }

        return rows;
    }

    private int getCountWithHeader(int count, boolean alwaysShow){
        return count + (count > 0 ? 1 : (alwaysShow ? 2 : 0));
    }

    @Override
    public int getItemViewType(int position)
    {
        int headerRowsCount = 1 + (TextUtils.isEmpty(this.exhibitor.getDescription()) ? 0 : 1) + (TextUtils.isEmpty(this.exhibitor.getProductsOnShow()) ? 0 : 1);
        int sessionsCount = this.exhibitorSessions != null ? this.exhibitorSessions.length : 0;
        int eventsCount = this.exhibitorEvents != null ? this.exhibitorEvents.length : 0;
        int collateralsCount = this.exhibitorCollaterals != null ? this.exhibitorCollaterals.size() : 0;

        if (position == 0)
        {
            // Company name, logo
            return VIEWTYPE_EXHIBITOR_GENERAL;
        }
        else
        {
            if (position == 1)
            {
                if (!TextUtils.isEmpty(this.exhibitor.getDescription()))
                {
                    // Company details text
                    return VIEWTYPE_EXHIBITOR_COMPANY;
                }
                else if (!TextUtils.isEmpty(this.exhibitor.getProductsOnShow()))
                {
                    // Company products text
                    return VIEWTYPE_EXHIBITOR_PRODUCTS;
                }
            }

            if (position == 2)
            {
                if (!TextUtils.isEmpty(this.exhibitor.getProductsOnShow()))
                {
                    return VIEWTYPE_EXHIBITOR_PRODUCTS;
                }
            }

            if (sessionsCount > 0) {
                if(position == headerRowsCount){
                    return VIEWTYPE_EXHIBITOR_SESSION_TITLE;
                }
                else if (position > headerRowsCount && position < headerRowsCount + getCountWithHeader(sessionsCount, false)) {
                    return VIEWTYPE_EXHIBITOR_SESSION;
                }
            }

            if (eventsCount == 0){

                if (position == headerRowsCount + getCountWithHeader(sessionsCount, false))
                {
                    return VIEWTYPE_EXHIBITOR_EVENT_TITLE;
                }
                else if (position > headerRowsCount && position < headerRowsCount + 1 + 1 + getCountWithHeader(sessionsCount, false))
                {
                    //Return Login View Type here if logged out otherwise return error message view
                    if (!UserPreferencesHelper.loggedIn(context))
                    {
                        return VIEWTYPE_NOT_LOGGED_IN;
                    }else{
                        return VIEWTYPE_ERROR_MESSAGE;
                    }
                }
            }else{
                if (eventsCount > 0)
                {
                    if (position == headerRowsCount + getCountWithHeader(sessionsCount, false))
                    {
                        return VIEWTYPE_EXHIBITOR_EVENT_TITLE;
                    }
                    else if (position > headerRowsCount
                            && position < headerRowsCount + getCountWithHeader(eventsCount, true) + getCountWithHeader(sessionsCount, false))
                    {
                        return VIEWTYPE_EXHIBITOR_EVENT;
                    }
                }
            }

            if (collateralsCount == 0){
                if (position == headerRowsCount + getCountWithHeader(sessionsCount, false) + getCountWithHeader(eventsCount, true))
                {
                    return VIEWTYPE_EXHIBITOR_COLLATERAL_TITLE;
                }
                else if (position > headerRowsCount + getCountWithHeader(sessionsCount, false) + getCountWithHeader(eventsCount, true)
                        && position < headerRowsCount + getCountWithHeader(sessionsCount, false) + getCountWithHeader(eventsCount, true) + 1 + 1)
                {
                    //return view for login or error message
                    if (!UserPreferencesHelper.loggedIn(context))
                    {
                        return VIEWTYPE_NOT_LOGGED_IN;
                    }else{
                        return VIEWTYPE_ERROR_MESSAGE;
                    }
                }
            }else{
                if (collateralsCount > 0)
                {
                    if (position == headerRowsCount  + getCountWithHeader(sessionsCount, false) + getCountWithHeader(eventsCount, true))
                    {
                        return VIEWTYPE_EXHIBITOR_COLLATERAL_TITLE;
                    }
                    else if (position > headerRowsCount + getCountWithHeader(sessionsCount, false) + getCountWithHeader(eventsCount, true)
                            && position < headerRowsCount +  + getCountWithHeader(sessionsCount, false) + getCountWithHeader(eventsCount, true)
                                + getCountWithHeader(collateralsCount, true))
                    {
                        return VIEWTYPE_EXHIBITOR_COLLATERAL;
                    }
                }
            }


        }

        return VIEWTYPE_EMPTY;
    }

    @Override
    public Object getItem(int position)
    {
        final int viewType = getItemViewType(position);

        if (viewType == VIEWTYPE_EXHIBITOR_COLLATERAL)
        {
            return this.exhibitorCollaterals.get(position);
        }
        else if (viewType == VIEWTYPE_EXHIBITOR_REPRESENTATIVE)
        {
            position = representativeIndexForPosition(position);
        }
        else if (viewType == VIEWTYPE_EXHIBITOR_EVENT)
        {
            position = eventIndexForPosition(position);
            return this.exhibitorEvents[position];
        }
        else if (viewType == VIEWTYPE_EXHIBITOR_SESSION)
        {
            position = sessionsIndexForPosition(position);
            return this.exhibitorSessions[position];
        }

        return this.exhibitor;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        switch (getItemViewType(position))
        {
            case VIEWTYPE_EXHIBITOR_GENERAL:

                if (convertView == null)
                {
                    convertView = this.newGeneralView(parent);
                }

                this.bindGeneralView(convertView);
                break;

            case VIEWTYPE_EXHIBITOR_COMPANY:

                if (convertView == null)
                {
                    convertView = this.newCompanyView(parent);
                }

                this.bindCompanyView(convertView);
                break;

            case VIEWTYPE_EXHIBITOR_PRODUCTS:

                if (convertView == null)
                {
                    convertView = this.newProductsView(parent);
                }

                this.bindProductsView(convertView);
                break;

            case VIEWTYPE_EXHIBITOR_COLLATERAL:

                if (convertView == null)
                {
                    convertView = this.newCollateralView(parent);
                }

                //this.bindCollateralView(convertView, collateralIndexForPosition(position));
                break;

            case VIEWTYPE_EXHIBITOR_REPRESENTATIVE:

                if (convertView == null)
                {
                    convertView = this.newRepresentativeView(parent);
                }

                this.bindRepresentativeView(convertView, representativeIndexForPosition(position));
                break;

            case VIEWTYPE_EXHIBITOR_EVENT_TITLE:

                if (convertView == null)
                {
                    convertView = this.newTitleView(parent);
                }

                this.bindTitleView(convertView, this.context.getResources().getString(R.string.exhibitor_events));
                break;

            case VIEWTYPE_EXHIBITOR_EVENT:

                convertView = SessionViewHelper.createSessionView(convertView, parent, (EventTiming) getItem(position), this.eventShouldShowDate(position));

                // Extra padding for last session
                if (eventIndexForPosition(position) == 0)
                {
                    convertView.setPadding(
                            convertView.getPaddingLeft(),
                            this.paddingSession,
                            convertView.getPaddingRight(),
                            convertView.getPaddingBottom());
                }


                convertView.setPadding(
                        convertView.getPaddingLeft(),
                        convertView.getPaddingTop(),
                        convertView.getPaddingRight(),
                        //convertView.getPaddingBottom());
                        this.paddingSession);

                break;

            case VIEWTYPE_EXHIBITOR_COLLATERAL_TITLE:

                if (convertView == null)
                {
                    convertView = this.newTitleView(parent);
                }

                this.bindTitleView(convertView, this.context.getResources().getString(R.string.exhibitor_collaterals));
                break;

            case VIEWTYPE_EXHIBITOR_REPRESENTATIVE_TITLE:

                if (convertView == null)
                {
                    convertView = this.newTitleView(parent);
                }

                this.bindTitleView(convertView, this.context.getResources().getString(R.string.exhibitor_representatives));
                break;

            case VIEWTYPE_EMPTY:

                convertView = new View(this.context);

                break;

            case VIEWTYPE_NOT_LOGGED_IN:

                if (convertView == null)
                {
                    convertView = this.newErrorView(parent);
                    //convertView = this.newLoginView(parent);
                }

                this.bindErrorView(convertView, "Please login to view this section.");
                break;

            case VIEWTYPE_ERROR_MESSAGE:

                if (convertView == null)
                {
                    convertView = this.newErrorView(parent);
                }

                this.bindErrorView(convertView, context.getResources().getString(R.string.no_details_error));
                break;

            case VIEWTYPE_EXHIBITOR_SESSION_TITLE:

                if (convertView == null)
                {
                    convertView = this.newTitleView(parent);
                }

                this.bindTitleView(convertView, this.context.getResources().getString(R.string.exhibitor_sessions));
                break;

            case VIEWTYPE_EXHIBITOR_SESSION:

                convertView = SessionViewHelper.createSessionView(convertView, parent, (EventTiming) getItem(position), sessionShouldShowDate(position));

                // Extra padding for last session
                if (sessionsIndexForPosition(position) == 0)
                {
                    convertView.setPadding(
                            convertView.getPaddingLeft(),
                            this.paddingSession,
                            convertView.getPaddingRight(),
                            convertView.getPaddingBottom());
                }


                convertView.setPadding(
                        convertView.getPaddingLeft(),
                        convertView.getPaddingTop(),
                        convertView.getPaddingRight(),
                        //convertView.getPaddingBottom());
                        this.paddingSession);

                break;

            default:
                break;
        }

        if (getItemViewType(position) != VIEWTYPE_EXHIBITOR_EVENT)
        {
            // set extra bottom padding if this is the last item in the list, no bottom padding otherwise
            if (position == this.getCount() - 1 && position > 0)
            {
                convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), this.paddingBottom);
            }
            else
            {
                convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), 0);
            }
        }

        return convertView;
    }


    /**
     * Determines if the selected session should show a date label.
     * If the previous session in the list has the same date, the new item does not have to display its date.
     */
    private boolean eventShouldShowDate(int position)
    {
        boolean shouldShowDate = true;
        if (this.exhibitorEvents.length > 1 && eventIndexForPosition(position) > 0)
        {
            String previousDate = DateUtils.prettyDayString(exhibitorEvents[ eventIndexForPosition(position) - 1].getStartDate());
            String currentDate = DateUtils.prettyDayString(((EventTiming) this.getItem(position)).getStartDate());
            if (previousDate.equals(currentDate))
            {
                shouldShowDate = false;
            }
        }
        return shouldShowDate;
    }

    private boolean sessionShouldShowDate(int position)
    {
        boolean shouldShowDate = true;
        if (this.exhibitorSessions.length > 1 && sessionsIndexForPosition(position) > 0)
        {
            String previousDate = DateUtils.prettyDayString(exhibitorSessions[sessionsIndexForPosition(position) - 1].getStartDate());
            String currentDate = DateUtils.prettyDayString(((EventTiming) this.getItem(position)).getStartDate());
            if (previousDate.equals(currentDate))
            {
                shouldShowDate = false;
            }
        }
        return shouldShowDate;
    }

    private int getAdjustedSessionPosition(int position)
    {
        if (this.exhibitor != null)
        {
            // Exhibitor info header
            position--;

            if (!TextUtils.isEmpty(this.exhibitor.getDescription()))
            {
                position--;
            }

            if (!TextUtils.isEmpty(this.exhibitor.getProductsOnShow()))
            {
                position--;
            }

            // Title
            position--;
        }

        return position;
    }

    @Override
    public boolean isEnabled(int position)
    {
        int viewType = this.getItemViewType(position);

        return viewType == VIEWTYPE_EXHIBITOR_COLLATERAL
                || viewType == VIEWTYPE_EXHIBITOR_REPRESENTATIVE
                || viewType == VIEWTYPE_EXHIBITOR_EVENT
                || viewType == VIEWTYPE_EXHIBITOR_SESSION;
    }

    private View newGeneralView(ViewGroup parent)
    {
        final ExhibitorGeneralViewHolder holder = new ExhibitorGeneralViewHolder();
        final View v = this.inflater.inflate(R.layout.listitem_exhibitor_general, parent, false);

        holder.imageViewLogo = (ImageView) v.findViewById(R.id.imageView_exhibitorDetailLogo);
        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textView_exhibitorDetailCompanyName);
        holder.buttonLocateOnFloorPlan = (LinearLayout) v.findViewById(R.id.button_locateOnFloorPlan);
        holder.textLocateOnFloorPlan = (TextView) v.findViewById(R.id.text_locateOnFloorPlan);

        holder.buttonLocateOnFloorPlan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ExhibitorDetailBaseAdapter.this.locateOnFloorPlanListener.locateOnFloorPlan(null,null);
            }
        });

        v.setTag(holder);
        return v;
    }

    private View newCompanyView(ViewGroup parent)
    {
        final ExhibitorCompanyViewHolder holder = new ExhibitorCompanyViewHolder();
        final View v = inflater.inflate(R.layout.listitem_exhibitor_company, parent, false);

        holder.textViewCompanyDescription = (ReadMoreView) v.findViewById(R.id.textView_exhibitor_company);

        v.setTag(holder);

        return v;
    }

    private View newProductsView(ViewGroup parent)
    {
        final ExhibitorProductsViewHolder holder = new ExhibitorProductsViewHolder();
        final View v = inflater.inflate(R.layout.listitem_exhibitor_products, parent, false);

        holder.textViewProductsDescription = (ReadMoreView) v.findViewById(R.id.textView_exhibitor_products);

        v.setTag(holder);

        return v;
    }

    private View newCollateralView(ViewGroup parent)
    {
        final ExhibitorCollateralViewHolder holder = new ExhibitorCollateralViewHolder();
        final View v = inflater.inflate(R.layout.listitem_exhibitor_collateral, parent, false);

        holder.textViewCollateral = (TextView) v.findViewById(R.id.textview_collateral);
        holder.textViewCollateral.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        v.setTag(holder);

        return v;
    }

    private View newRepresentativeView(ViewGroup parent)
    {
        final ExhibitorRepresentativeViewHolder holder = new ExhibitorRepresentativeViewHolder();

        final View v = inflater.inflate(R.layout.listitem_person, parent, false);

        v.findViewById(R.id.textview_person_extra).setVisibility(View.GONE);
        holder.imageViewPicture = (ImageView) v.findViewById(R.id.imageview_person_profile_picture);
        holder.imageViewFavourite = (ImageView) v.findViewById(R.id.imageview_person_favourite);
        holder.textViewName = (TextView) v.findViewById(R.id.textview_person_name);
        holder.textViewBusinessFunction = (TextView) v.findViewById(R.id.textview_person_business_function);
        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textview_person_company_name);
        holder.separator = v.findViewById(R.id.separator);

        v.setTag(holder);

        return v;
    }

    private View newTitleView(ViewGroup parent)
    {
        final TitleViewHolder holder = new TitleViewHolder();
        final View v = inflater.inflate(R.layout.listitem_title, parent, false);

        holder.textViewTitle = (TextView) v.findViewById(R.id.textView_title);

        v.setTag(holder);

        return v;
    }

    private View newLoginView(ViewGroup parent){
        final LoginViewHolder holder = new LoginViewHolder();
        final View v = inflater.inflate(R.layout.listitem_login_view, parent, false);

        holder.notLoggedInView = (NotLoggedInView) v.findViewById(R.id.notLoggedInView);
        holder.notLoggedInView.setImageViewHeaderVisibility(false);

        v.setTag(holder);

        return v;
    }

    private View newErrorView(ViewGroup parent){
        final ErrorViewHolder holder = new ErrorViewHolder();
        final View v = inflater.inflate(R.layout.listitem_error_message, parent, false);

        holder.textViewErrorMessage = (TextView) v.findViewById(R.id.textView_error_message);

        v.setTag(holder);

        return v;
    }

    private void bindGeneralView(View view)
    {
        final ExhibitorGeneralViewHolder holder = (ExhibitorGeneralViewHolder) view.getTag();

        PicassoUtils.loadImage(this.exhibitor.getPictureUrl(), holder.imageViewLogo,R.drawable.placeholder_profile_photo,context);

        holder.textViewCompanyName.setText(this.exhibitor.getName());
        //holder.textViewStand.setText(this.exhibitor.getStandNumber());
        holder.textLocateOnFloorPlan.setText(this.exhibitor.getStandNumber());
    }

    private void bindCompanyView(View view)
    {
        final ExhibitorCompanyViewHolder holder = (ExhibitorCompanyViewHolder) view.getTag();
        holder.textViewCompanyDescription.setText(Html.fromHtml(this.exhibitor.getDescription()));
    }

    private void bindProductsView(View view)
    {
        final ExhibitorProductsViewHolder holder = (ExhibitorProductsViewHolder) view.getTag();
        holder.textViewProductsDescription.setText(Html.fromHtml(this.exhibitor.getProductsOnShow()));
    }

    private void bindCollateralView(View view, int position)
    {
        final ExhibitorCollateralViewHolder holder = (ExhibitorCollateralViewHolder) view.getTag();

        final ExhibitorCollateral collateral = this.exhibitorCollaterals.get(position);

        holder.textViewCollateral.setText(collateral.getTitle());
    }

    private void bindRepresentativeView(View view, int position)
    {

    }

    private void bindTitleView(View view, String title)
    {
        final TitleViewHolder holder = (TitleViewHolder) view.getTag();
        holder.textViewTitle.setText(title);
    }

    private void bindLoginView(View view){
        final LoginViewHolder holder = (LoginViewHolder) view.getTag();
        holder.notLoggedInView.setText("Please login to view this section.");
    }

    private void bindErrorView(View view, String message){
        final ErrorViewHolder holder = (ErrorViewHolder) view.getTag();
        holder.textViewErrorMessage.setText(message+"");
    }

    private int sessionsIndexForPosition(int position)
    {
        // General row
        position = position - 1;

        // Company description row
        if (!TextUtils.isEmpty(this.exhibitor.getDescription()))
        {
            position = position - 1;
        }

        // Company product rows
        if (!TextUtils.isEmpty(this.exhibitor.getProductsOnShow()))
        {
            position = position - 1;
        }

        if(this.exhibitorSessions != null && this.exhibitorSessions.length > 0 ) {
            //minus title row
            position = position - 1;
        }

        return position;
    }

    private int eventIndexForPosition(int position)
    {
        position = sessionsIndexForPosition(position);

        if (this.exhibitorSessions != null && this.exhibitorSessions.length > 0)
        {
            position = position - this.exhibitorSessions.length;
        }

        if(this.exhibitorEvents != null && this.exhibitorEvents.length > 0 ) {
            position = position - 1;
        }

        return position;
    }

    private int representativeIndexForPosition(int position)
    {
        position = eventIndexForPosition(position);

        if (this.exhibitorEvents != null && this.exhibitorEvents.length > 0)
        {
            position = position - this.exhibitorEvents.length;
        }
        else if (this.exhibitorEvents == null || this.exhibitorEvents.length == 0)
        {
            position = position - 2;
        }

        return position;
    }

    public void setExhibitor(Exhibitor exhibitor)
    {
        this.exhibitor = exhibitor;
    }

    public Exhibitor getExhibitor()
    {
        return this.exhibitor;
    }

    public void setExhibitorCollaterals(ArrayList<ExhibitorCollateral> exhibitorCollaterals)
    {
        this.exhibitorCollaterals = exhibitorCollaterals;
        this.notifyDataSetChanged();
    }

    public void setExhibitorEvents(EventTiming[] events)
    {
        this.exhibitorEvents = events;
        this.notifyDataSetChanged();
    }

    public void setExhibitorSessions(EventTiming[] sessions) {
        this.exhibitorSessions = sessions;
        this.notifyDataSetChanged();
    }

    private static class ExhibitorGeneralViewHolder
    {
        ImageView imageViewLogo;
        TextView textViewCompanyName;
        LinearLayout buttonLocateOnFloorPlan;
        TextView textLocateOnFloorPlan;
    }

    private static class ExhibitorCompanyViewHolder
    {
        ReadMoreView textViewCompanyDescription;
    }

    private static class ExhibitorProductsViewHolder
    {
        ReadMoreView textViewProductsDescription;
    }

    private static class ExhibitorCollateralViewHolder
    {
        TextView textViewCollateral;
    }

    private static class ExhibitorRepresentativeViewHolder
    {
        View separator;
        ImageView imageViewPicture;
        ImageView imageViewFavourite;
        TextView textViewName;
        TextView textViewBusinessFunction;
        TextView textViewCompanyName;
    }

    private static class TitleViewHolder
    {
        TextView textViewTitle;
    }

    private static class LoginViewHolder
    {
        NotLoggedInView notLoggedInView;
    }

    private static class ErrorViewHolder
    {
        TextView textViewErrorMessage;
    }
}