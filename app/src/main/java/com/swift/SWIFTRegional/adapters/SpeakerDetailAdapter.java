package com.swift.SWIFTRegional.adapters;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;

import com.swift.SWIFTRegional.models.Speaker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.interfaces.ContactDetailListener;
import com.swift.SWIFTRegional.interfaces.RatingListener;
import com.swift.SWIFTRegional.models.ContactInformation;
import com.swift.SWIFTRegional.models.EventTiming;
import com.swift.SWIFTRegional.models.Rate;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;
import com.swift.SWIFTRegional.viewhelper.ContactViewHelper;
import com.swift.SWIFTRegional.viewhelper.SessionViewHelper;
import com.swift.SWIFTRegional.viewhelper.TitleViewHelper;
import com.swift.SWIFTRegional.views.IconButton;
import com.swift.SWIFTRegional.views.ReadMoreView;

public class SpeakerDetailAdapter extends AbstractRateAdapter
{
    public static final int VIEWTYPE_SPEAKER_TOP = 0;
    public static final int VIEWTYPE_SESSION = 1;
    public static final int VIEWTYPE_ATTENDANCE = 2;
    public static final int VIEWTYPE_CONTACT_TITLE = 3;
    public static final int VIEWTYPE_CONTACT = 4;
    public static final int VIEWTYPE_CONTACT_FOOTER = 5;
    public static final int VIEWTYPE_MAX_COUNT = 6;
    private Speaker speaker;

    private ContactInformation contactInformation;
    private Context context;

    private final int paddingBottom;
    private final int paddingSession;

    private RatingListener ratingListener;
    private ContactDetailListener listener;

    private boolean userIsLoggedIn;

    private List<EventTiming> sessions;

    public SpeakerDetailAdapter(Context context, ContactDetailListener listener, RatingListener ratingListener)
    {
        super(context);

        this.context = context;
        this.listener = listener;
        this.ratingListener = ratingListener;

        this.paddingBottom = this.context.getResources().getDimensionPixelSize(R.dimen.padding_xxlarge);
        this.paddingSession = this.context.getResources().getDimensionPixelSize(R.dimen.padding_session_last_card_in_details);

        this.userIsLoggedIn = UserPreferencesHelper.loggedIn(context);
    }

    @Override
    public int getViewTypeCount()
    {
        return VIEWTYPE_MAX_COUNT;
    }

    private int getSessionsCount()
    {
        return this.sessions == null ? 0 : this.sessions.size();
    }

    @Override
    public int getCount()
    {
        // session rows
        int rows = this.getSessionsCount();

        // speaker details
        if (this.speaker != null)
        {
            // Add 1 row for the header (rating, summary and "Sessions" title)
            rows++;
        }

        // "Add to contacts" button
        rows++;

        return rows;
    }

    @Override
    public int getItemViewType(int position)
    {
        int numberOfSessions = this.getSessionsCount();

        int numberOfContactItems = 0;
        if (this.contactInformation != null)
        {
            numberOfContactItems = this.contactInformation.size();
        }

        if (position == 0)
        {
            // First item: speaker info (rating bar, summary and header for sessions)
            return VIEWTYPE_SPEAKER_TOP;
        }
        else if ((position == numberOfSessions + numberOfContactItems + 3))
        {
            // Last item: social info + add to contacts
            return VIEWTYPE_CONTACT_FOOTER;
        }else if (position == numberOfSessions + numberOfContactItems + 1){
            return VIEWTYPE_CONTACT_FOOTER;
        }
        else if (position <= numberOfSessions)
        {
            // Sessions
            return VIEWTYPE_SESSION;
        }
        else
        {
            // Contact options (phone number, company address, etc)
            return VIEWTYPE_CONTACT;
        }
    }


    @Override
    public EventTiming getItem(int position)
    {
        int type = this.getItemViewType(position);
        if (type == VIEWTYPE_SESSION)
        {
            position = getAdjustedPosition(position);

            if (this.sessions != null)
            {
                return sessions.get(position);
            }
        }
        else if (type == VIEWTYPE_CONTACT)
        {

        }

        return null;
    }

    private int getAdjustedPosition(int position)
    {
        if (this.speaker != null)
        {
            position--;
        }
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Object object = this.getItem(position);
        switch (getItemViewType(position))
        {
            case VIEWTYPE_SPEAKER_TOP:

                if (convertView == null || !(convertView.getTag() instanceof SpeakerDetailsTopViewHolder))
                {
                    convertView = this.newSpeakerTopView(parent);
                }
                this.bindSpeakerTopView(convertView);
                break;

            case VIEWTYPE_SESSION:

                convertView = SessionViewHelper.createSessionView(convertView, parent, (EventTiming) object, this.sessionShouldShowDate(position));

                // Extra padding for last session
                convertView.setPadding(
                        convertView.getPaddingLeft(),
                        convertView.getPaddingTop(),
                        convertView.getPaddingRight(),
                        this.paddingSession);

                break;

            case VIEWTYPE_ATTENDANCE:

                break;

            case VIEWTYPE_CONTACT_TITLE:

                convertView = TitleViewHelper.createTitleListView(convertView, parent, this.context.getResources().getString(R.string.contact_title));

                break;

            case VIEWTYPE_CONTACT:

                position -= (3 + this.getSessionsCount());
                ContactInformation.ContactInformationItem contactInformationItem = this.contactInformation.get(position);

                boolean isLastItem = position == this.contactInformation.size() - 1;

                convertView = ContactViewHelper.createContactView(convertView, parent, contactInformationItem, ContactViewHelper.ColorMode.RED, isLastItem);

                // add extra padding to bottom of last contact item
                if (isLastItem)
                {
                    convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), this.context.getResources().getDimensionPixelOffset(R.dimen.padding_large));
                }

                break;

            case VIEWTYPE_CONTACT_FOOTER:

                if (convertView == null)
                {
                    convertView = this.newContactFooterView(parent);
                }
                this.bindContactFooterView(convertView);
                break;
        }

        // set extra bottom padding if this is the last item in the list, no bottom padding otherwise
        if (getItemViewType(position) != VIEWTYPE_SESSION)
        {
            if (position == this.getCount() - 1 && position > 0)
            {
                convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), this.paddingBottom);
            }
            else
            {
                convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), 0);
            }
        }

        return convertView;
    }


    /**
     * Determines if the selected session should show a date label.
     * If the previous session in the list has the same date, the new item does not have to display its date.
     */
    private boolean sessionShouldShowDate(int position)
    {
        boolean shouldShowDate = true;
        if (this.sessions.size() > 1 && this.getAdjustedPosition(position) > 0)
        {
            String previousDate = DateUtils.prettyDayString(sessions.get(this.getAdjustedPosition(position) - 1).getStartDate());
            String currentDate = DateUtils.prettyDayString(this.getItem(position).getStartDate());
            if (previousDate.equals(currentDate))
            {
                shouldShowDate = false;
            }
        }
        return shouldShowDate;
    }


    // ViewHolder creation

    public View newSpeakerTopView(ViewGroup parent)
    {
        final View v = this.inflater.inflate(R.layout.listitem_speaker_top, parent, false);

        final SpeakerDetailsTopViewHolder viewHolder = new SpeakerDetailsTopViewHolder();
        viewHolder.readmoreViewBiography = (ReadMoreView) v.findViewById(R.id.readmoreView_speaker_details_biography);
        viewHolder.textViewAbout = (TextView) v.findViewById(R.id.textview_speaker_details_aboutTitle);
        viewHolder.textViewRatingTitle = (TextView) v.findViewById(R.id.textView_ratingTitle);
        viewHolder.ratingBarSpeaker = (RatingBar) v.findViewById(R.id.ratingBar);
        viewHolder.ratingDivider = v.findViewById(R.id.divider_speaker_rating_summary);

        v.setTag(viewHolder);

        return v;
    }


    public View newContactFooterView(ViewGroup parent)
    {
        final View view = this.inflater.inflate(R.layout.listitem_speaker_contact_footer, parent, false);
        final ContactFooterViewHolder holder = new ContactFooterViewHolder();

        holder.buttonTwitter = (ImageButton) view.findViewById(R.id.imageButton_speaker_contact_twitter);
        holder.buttonLinkedIn = (ImageButton) view.findViewById(R.id.imageButton_speaker_contact_linkedin);
        holder.textViewSocialHeader = (TextView) view.findViewById(R.id.textView_speaker_social);
        holder.buttonAddToContacts = (IconButton) view.findViewById(R.id.button_speaker_contact_add);

        view.setTag(holder);

        return view;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return null;
    }


    // View binding

    public void bindSpeakerTopView(View view)
    {
        final SpeakerDetailsTopViewHolder viewHolder = (SpeakerDetailsTopViewHolder) view.getTag();

        String bio = this.speaker == null ? null : this.speaker.getBiography();
        if (TextUtils.isEmpty(bio))
        {
            viewHolder.readmoreViewBiography.setVisibility(View.GONE);
            viewHolder.ratingDivider.setVisibility(View.GONE);
            viewHolder.textViewAbout.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.readmoreViewBiography.setVisibility(View.VISIBLE);
            viewHolder.textViewAbout.setVisibility(View.VISIBLE);
            viewHolder.ratingDivider.setVisibility(View.VISIBLE);
            viewHolder.readmoreViewBiography.setText(this.speaker.getBiography());
        }


        // rating
        viewHolder.textViewRatingTitle.setText(this.context.getString(R.string.speaker_rate));
        if (!userIsLoggedIn) // do not show rating bar when user is not logged in
        {
            viewHolder.textViewRatingTitle.setVisibility(View.GONE);
            viewHolder.ratingBarSpeaker.setVisibility(View.GONE);
            viewHolder.ratingDivider.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.ratingBarSpeaker.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener()
            {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser)
                {
                    if (fromUser)
                    {
                        if (SpeakerDetailAdapter.this.rate == null)
                        {
                            SpeakerDetailAdapter.this.rate = new Rate();
                        }

                        SpeakerDetailAdapter.this.rate.setRating((int) rating);

                        SpeakerDetailAdapter.this.ratingListener.ratingSelected((int) rating);
                    }
                }
            });

            viewHolder.ratingBarSpeaker.setRating(this.rate == null ? 0 : this.rate.getRating());
        }
    }

    private void bindContactFooterView(View convertView)
    {
        ContactFooterViewHolder holder = (ContactFooterViewHolder) convertView.getTag();
        holder.buttonTwitter.setVisibility(View.GONE);
        holder.buttonLinkedIn.setVisibility(View.GONE);
        holder.textViewSocialHeader.setVisibility(View.GONE);
        holder.buttonAddToContacts.setVisibility(View.GONE);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
    }

    @Override
    public boolean isEnabled(int position)
    {
        return getItemViewType(position) == VIEWTYPE_SESSION;
    }

    public void setRate(Rate rate)
    {
        this.rate = rate;
    }

    public void setSpeaker(Speaker speaker)
    {
        this.speaker = speaker;
    }

    public Speaker getSpeaker()
    {
        return this.speaker;
    }

    public void setSessions(Collection<EventTiming> values)
    {
        if (values != null)
        {
            this.sessions = new ArrayList<>(values);
        }
        else
        {
            this.sessions = null;

        }
        this.notifyDataSetChanged();
    }

    private static class SpeakerDetailsTopViewHolder
    {
        TextView textViewAbout;
        ReadMoreView readmoreViewBiography;
        TextView textViewRatingTitle;
        RatingBar ratingBarSpeaker;
        View ratingDivider;
    }

    private static class ContactFooterViewHolder
    {
        IconButton buttonAddToContacts;
        TextView textViewSocialHeader;
        ImageButton buttonTwitter;
        ImageButton buttonLinkedIn;
    }

}
