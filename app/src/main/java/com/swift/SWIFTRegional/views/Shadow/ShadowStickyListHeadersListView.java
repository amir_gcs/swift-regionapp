package com.swift.SWIFTRegional.views.Shadow;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by SimonRaes on 3/06/15.
 * Adds a shadow to the left and right of the listview.
 */
public class ShadowStickyListHeadersListView extends StickyListHeadersListView
{
    private ShadowViewHelper shadowHelper;

    public ShadowStickyListHeadersListView(Context context)
    {
        this(context, null);
    }

    public ShadowStickyListHeadersListView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public ShadowStickyListHeadersListView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        this.setWillNotDraw(false);
        this.shadowHelper = new ShadowViewHelper(this.getContext(), attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.shadowHelper.measure(getPaddingLeft(), getPaddingRight(), getMeasuredWidth(), this.getMeasuredHeight());
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        this.shadowHelper.draw(canvas);
        super.onDraw(canvas);
    }

}
