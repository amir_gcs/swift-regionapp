package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;

import com.goodcoresoftware.android.common.views.TextView;

/**
 * Created by iker on 28/05/14.
 */
public class LayoutedTextView extends TextView
{
    private boolean showGradient = true;
    private LinearGradient textGradient;

    public LayoutedTextView(Context context)
    {
        super(context);
    }

    public LayoutedTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public LayoutedTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public interface OnLayoutListener
    {
        void onLayouted(TextView view);
    }

    private OnLayoutListener mOnLayoutListener;

    public void setOnLayoutListener(OnLayoutListener listener)
    {
        mOnLayoutListener = listener;
    }

    public void toggleGradient(boolean showGradient)
    {
        this.showGradient = showGradient;
        this.invalidate();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);

        if (this.textGradient == null)
        {
            this.initGradient(left, top, right, bottom);
        }

        if (this.mOnLayoutListener != null)
        {
            this.mOnLayoutListener.onLayouted(this);
        }
    }

    @Override
    public void draw(Canvas canvas)
    {
        if (this.textGradient != null && this.showGradient)
        {
            // Applies a gradient shader to the text to make it fade out near the bottom
            Shader textShader = this.textGradient;
            this.getPaint().setShader(textShader);
        }
        else
        {
            this.getPaint().setShader(null);
        }

        super.draw(canvas);

    }

    private void initGradient(int left, int top, int right, int bottom)
    {
        this.textGradient = new LinearGradient((left + right) / 2, (top + bottom) * (2 / 3), (left + right) / 2, bottom, new int[]{Color.BLACK, Color.WHITE}, null, Shader.TileMode.CLAMP);
    }
}
