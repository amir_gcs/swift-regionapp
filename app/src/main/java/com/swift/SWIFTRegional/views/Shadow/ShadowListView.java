package com.swift.SWIFTRegional.views.Shadow;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by SimonRaes on 3/06/15.
 * Adds a shadow to the left and right of the listview.
 */
public class ShadowListView extends ListView
{
    private ShadowViewHelper shadowHelper;

    public ShadowListView(Context context)
    {
        this(context, null);
    }

    public ShadowListView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public ShadowListView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        this.setWillNotDraw(false);
        this.shadowHelper = new ShadowViewHelper(this.getContext(), attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.shadowHelper.measure(getPaddingLeft(), getPaddingRight(), getMeasuredWidth(), this.getMeasuredHeight());
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        this.shadowHelper.draw(canvas);
    }
}
