package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.TextureView;

import com.google.zxing.client.android.camera.open.CameraManager;
import com.swift.SWIFTRegional.utils.CameraUtils;

import java.io.IOException;

import com.swift.SWIFTRegional.interfaces.qr.OnCameraDataReceivedListener;

/**
 * Created by SimonRaes on 4/05/15.
 * Code taken from the Kluwer-incident-android app.
 */
public class QrReaderView extends TextureView implements TextureView.SurfaceTextureListener, Camera.PreviewCallback
{
    private static final String TAG = QrReaderView.class.getName();

    private OnCameraDataReceivedListener cameraDataReceivedListener;

    private long lastPreviewTimestamp;
    private int previewWidth;
    private int previewHeight;
    private CameraManager cameraManager;

    private boolean checkedForCamera;

    public QrReaderView(Context context)
    {
        this(context, null);
    }

    public QrReaderView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        if (!this.isInEditMode())
        {
            this.init();
        }

    }

    public void setOnCameraDataReceivedListener(OnCameraDataReceivedListener cameraDataReceivedListener)
    {
        this.cameraDataReceivedListener = cameraDataReceivedListener;
    }

    private void init()
    {
        if (checkCameraHardware(getContext()))
        {
            this.cameraManager = new CameraManager(getContext());

            this.setSurfaceTextureListener(this);
        }
        else
        {
            if (!this.checkedForCamera && this.cameraDataReceivedListener != null)
            {
                this.cameraDataReceivedListener.cameraNotFound();
                this.checkedForCamera = true;
            }
        }
    }

    @Override
    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();

        if (this.cameraManager.isOpen())
        {
            this.cameraManager.closeDriver();
        }
    }

    /**
     * @author Wesley: Makes a square texture view
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        //be as big as possible to fit on screen
        int size = Math.min(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));

        this.setMeasuredDimension(size, size);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        if (changed)
        {
            //be as wide as possible and make it square
            this.layout(left, top, left + this.getMeasuredWidth(), top + this.getMeasuredHeight());
        }
    }

    private void openCamera(SurfaceTexture surface)
    {
        new OpenCameraTask(this.getContext(), this.cameraManager, surface, this.getWidth(), this.getHeight()).execute();
    }

    public void startCamera()
    {
        this.openCamera(this.getSurfaceTexture());
    }

    public void stopCamera()
    {
        if (this.cameraManager != null)
        {

            if (this.cameraManager.getCamera() != null)
            {
                this.cameraManager.getCamera().setPreviewCallback(null);
            }

            this.cameraManager.stopPreview();

            if (this.cameraManager.isOpen())
            {
                this.cameraManager.closeDriver();
            }
        }
    }

    // Called when camera take a frame
    @Override
    public void onPreviewFrame(byte[] data, Camera camera)
    {
        final long now = System.currentTimeMillis();

        if (this.cameraDataReceivedListener != null && now - this.lastPreviewTimestamp > 1000L)
        {
            this.lastPreviewTimestamp = now;
            this.cameraDataReceivedListener.onCameraDataReceived(data, this.previewWidth, this.previewHeight);
        }
    }

    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware(Context context)
    {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            // this device has a camera
            return true;
        }
        else if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT))
        {
            // this device has a front camera
            return true;
        }
        else if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
        {
            // this device has any camera
            return true;
        }
        else
        {
            // no camera on this device
            return false;
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
    {
        new OpenCameraTask(this.getContext(), this.cameraManager, surface, width, height).execute();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
    {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface)
    {
        this.stopCamera();

        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface)
    {
        if (this.cameraManager.getPreviewSize() != null)
        {
            this.previewWidth = this.cameraManager.getPreviewSize().x;
            this.previewHeight = this.cameraManager.getPreviewSize().y;

            if (this.cameraManager.isOpen())
            {
                this.cameraManager.getCamera().setPreviewCallback(this);
            }
        }
    }

    public void updateOrientation(Context context)
    {
        if(this.cameraManager != null)
        {
            CameraUtils.setCameraDisplayOrientation(context, this.cameraManager.getCamera());
        }
    }

    private class OpenCameraTask extends AsyncTask<Void, Void, Boolean>
    {
        private Context context;
        private CameraManager cameraManager;
        private SurfaceTexture surface;
        private int width;
        private int height;

        public OpenCameraTask(final Context inContext, final CameraManager cameraManager, final SurfaceTexture surface, final int width, final int height)
        {
            this.context = inContext;
            this.cameraManager = cameraManager;
            this.surface = surface;
            this.width = width;
            this.height = height;
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            try
            {
                if (!this.cameraManager.isOpen())
                {
                    // Indicate camera, our View dimensions
                    this.cameraManager.openDriver(this.surface, this.width, this.height);
                }

                return true;
            }
            catch (IOException e)
            {
                // failed to open camera driver
                //Log.w(TAG, "Can not openDriver: " + e.getMessage());
                this.cameraManager.closeDriver();

                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success)
        {
            super.onPostExecute(success);

            if (success)
            {
                try
                {
                    QrReaderView.this.updateOrientation(this.context);
                    this.cameraManager.startPreview();
                    QrReaderView.this.cameraDataReceivedListener.onCameraStarted();
                }
                catch (Exception e)
                {
                    this.cameraManager.closeDriver();
                }
            }
        }
    }
}