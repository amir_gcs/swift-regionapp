package com.swift.SWIFTRegional.views.Shadow;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;

import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 3/06/15.
 */
public class ShadowViewHelper
{
    private final int DEFAULT_SHADOW_WIDTH = 10;

    private Paint backgroundPaint, shadowPaintLeft, shadowPaintRight;

    private int backgroundPaintColor;
    private int shadowColor;
    private int shadowSize;

    private int measuredHeight;
    private int measuredWidth = -1;
    private int paddingLeft;
    private int paddingRight;

    public ShadowViewHelper(Context context, AttributeSet attrs)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ListViewShadow);

        this.backgroundPaintColor = a.getColor(R.styleable.ListViewShadow_itemsBackgroundFill, 0);
        if (this.backgroundPaintColor != 0)
        {
            this.backgroundPaint = new Paint();
            this.backgroundPaint.setColor(this.backgroundPaintColor);
        }

        this.shadowPaintLeft = new Paint();
        this.shadowPaintRight = new Paint();

        this.shadowColor = a.getColor(R.styleable.ListViewShadow_shadowColor, context.getResources().getColor(R.color.text_lightest_gray));
        this.shadowSize = a.getDimensionPixelSize(R.styleable.ListViewShadow_shadowSize, DEFAULT_SHADOW_WIDTH);

        a.recycle();
    }

    public void measure(int paddingLeft, int paddingRight, int measuredWidth, int measuredHeight)
    {
        this.paddingLeft = paddingLeft;
        this.paddingRight = paddingRight;

        if (measuredWidth != this.measuredWidth)
        {
            this.measuredHeight = measuredHeight;
            this.measuredWidth = measuredWidth;

            initShaders();
        }
    }

    public void initShaders()
    {
        Shader shader = new LinearGradient(paddingLeft - this.shadowSize,
                0,
                paddingLeft,
                0,
                Color.TRANSPARENT, this.shadowColor, Shader.TileMode.CLAMP);
        shadowPaintLeft.setShader(shader);

        Shader shaderRight = new LinearGradient(
                measuredWidth - paddingRight,
                0,
                measuredWidth - paddingRight + this.shadowSize,
                0,
                this.shadowColor, Color.TRANSPARENT, Shader.TileMode.CLAMP);
        shadowPaintRight.setShader(shaderRight);
    }

    public void draw(Canvas canvas)
    {
        canvas.drawRect(this.paddingLeft - this.shadowSize,
                0,
                this.paddingLeft,
                this.measuredHeight,
                shadowPaintLeft);
        if (backgroundPaint != null)
        {
            canvas.drawRect(this.paddingLeft,
                    0,
                    this.measuredWidth - this.paddingRight,
                    this.measuredHeight, this.backgroundPaint);
        }
        canvas.drawRect(this.measuredWidth - this.paddingRight,
                0,
                this.measuredWidth - this.paddingRight + this.shadowSize,
                this.measuredHeight,
                shadowPaintRight);
    }
}
