package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Yves on 18/03/14.
 */
public class RatioImageView extends ImageView
{
    private float ratio;

    public RatioImageView(Context context)
    {
        this(context, null);
    }

    public RatioImageView(Context context, AttributeSet attributeSet)
    {
        this(context, attributeSet, 0);
    }

    public RatioImageView(Context context, AttributeSet attributeSet, int style)
    {
        super(context, attributeSet, style);

        this.ratio = 9f / 16f;
    }

    public void setRatio(float ratio)
    {
        this.ratio = ratio;

        this.requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        final int width = MeasureSpec.getSize(widthMeasureSpec) + getPaddingLeft()+ getPaddingRight();
        final int height = (int) (width * this.ratio) + getPaddingTop();

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
