package com.swift.SWIFTRegional.views.Shadow;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by SimonRaes on 3/06/15.
 */
public class ShadowLinearLayout extends LinearLayout
{
    private ShadowViewHelper shadowHelper;

    public ShadowLinearLayout(Context context)
    {
        this(context, null);
    }

    public ShadowLinearLayout(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public ShadowLinearLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        this.setWillNotDraw(false);
        this.shadowHelper = new ShadowViewHelper(this.getContext(), attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.shadowHelper.measure(getPaddingLeft(), getPaddingRight(), getMeasuredWidth(), this.getMeasuredHeight());
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        this.shadowHelper.draw(canvas);
        super.onDraw(canvas);
    }
}
