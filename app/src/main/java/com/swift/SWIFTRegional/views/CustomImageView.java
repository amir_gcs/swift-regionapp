package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Wesley on 5/06/14.
 */
public class CustomImageView extends ImageView
{
    private int thumbSize;
    private String pictureUrl;

    public CustomImageView(Context context)
    {
        this(context, null);
    }

    public CustomImageView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public int getThumbSize()
    {
        return thumbSize;
    }

    public void setThumbSize(int thumbSize)
    {
        this.thumbSize = thumbSize;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl)
    {
        this.pictureUrl = pictureUrl;
    }
}

