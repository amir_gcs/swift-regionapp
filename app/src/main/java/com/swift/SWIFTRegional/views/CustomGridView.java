package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by Wesley on 5/06/14.
 */
public class CustomGridView extends GridView
{
    public CustomGridView(Context context)
    {
        super(context);
    }

    public CustomGridView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    /*@Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        super.onLayout(changed, l, t, r, b);

        if(changed)
        {
            this.post(new Runnable()
            {
                @Override
                public void run()
                {
                    if (CustomGridView.this.getAdapter() != null && CustomGridView.this.getAdapter() instanceof CursorAdapter)
                    {
                        ((CursorAdapter) CustomGridView.this.getAdapter()).notifyDataSetChanged();
                    }
                }
            });
        }
    }*/

   /* @Override
    protected void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);

        if(this.getAdapter() != null && this.getAdapter() instanceof CursorAdapter)
        {
            ((CursorAdapter)this.getAdapter()).notifyDataSetChanged();
        }
    }*/
}
