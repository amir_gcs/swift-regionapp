package com.swift.SWIFTRegional.views.Shadow;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Created by SimonRaes on 3/06/15.
 */
public class ShadowScrollView extends ScrollView
{
    private ShadowViewHelper shadowHelper;

    public ShadowScrollView(Context context)
    {
        this(context, null);
    }

    public ShadowScrollView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public ShadowScrollView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        this.setWillNotDraw(false);
        this.shadowHelper = new ShadowViewHelper(this.getContext(), attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.shadowHelper.measure(getPaddingLeft(), getPaddingRight(), getMeasuredWidth(), this.getMeasuredHeight());
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        this.shadowHelper.draw(canvas);
    }
}
