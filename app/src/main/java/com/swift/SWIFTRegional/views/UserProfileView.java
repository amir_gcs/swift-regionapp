package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.swift.SWIFTRegional.models.User;
import com.swift.SWIFTRegional.utils.PicassoUtils;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

public class UserProfileView extends RelativeLayout
{
    private User user;

    private TextView textViewCountry;
    private TextView textViewName;
    private TextView textViewBusinessFunction;
    private TextView textViewCompanyName;
    private ImageView imageViewProfilePicture;
    private TextView regHelpText;

    public UserProfileView(Context context)
    {
        this(context, null);
    }

    public UserProfileView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public UserProfileView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs);
        init();
    }

    private void init()
    {
        LayoutInflater.from(this.getContext()).inflate(R.layout.view_userprofile, this);

        this.textViewCountry = (TextView) this.findViewById(R.id.textView_country);
        this.imageViewProfilePicture = (ImageView) this.findViewById(R.id.imageView_profile_picture);
        this.textViewName = (TextView) this.findViewById(R.id.textView_name);
        this.regHelpText = (TextView) this.findViewById(R.id.textView_helptext);
        this.textViewBusinessFunction = (TextView) this.findViewById(R.id.textView_business_function);
        this.textViewCompanyName = (TextView) this.findViewById(R.id.textView_company_name);
    }

    public void setUser(User user)
    {
        this.user = user;
        this.updateUserInfo();
    }

    private void updateUserInfo()
    {
        if (this.user != null)
        {
            this.textViewName.setText(this.user.getFirstName() + " " + this.user.getLastName());
            this.textViewBusinessFunction.setText(this.user.getJobTitle());
            this.textViewCompanyName.setText(this.user.getCompanyName());
            this.textViewCountry.setText(this.user.getCountry());

            PicassoUtils.loadPersonImage(this.user.getPictureUrl(), imageViewProfilePicture);
        }
    }

    public void setHelpTextVisible() {
        regHelpText.setVisibility(VISIBLE);
        textViewBusinessFunction.setVisibility(GONE);
        textViewCompanyName.setVisibility(GONE);
        textViewCountry.setVisibility(GONE);
    }
}
