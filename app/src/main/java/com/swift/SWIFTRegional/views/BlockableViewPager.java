package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by SimonRaes on 11/06/15.
 * A custom viewPager that accepts a boolean to enable/disable the swiping between fragments.
 * Used to allow panning of the floorplan without the ViewPager switching page.
 */
public class BlockableViewPager extends ViewPager
{
    private boolean canScroll = true;

    public BlockableViewPager(Context context)
    {
        super(context);
    }

    public BlockableViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y)
    {
        if (!this.canScroll)
        {
            // Return true if a CHILD of the viewpager can scroll. Will block switching between pager fragments by swiping.
            return true;
        }

        return super.canScroll(v, checkV, dx, x, y);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.canScroll) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.canScroll) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setCanScroll(boolean canScroll)
    {
        this.canScroll = canScroll;
    }
}
