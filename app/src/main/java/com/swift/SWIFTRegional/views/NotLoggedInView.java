package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 4/06/15.
 * A view displaying a message indicating login is required to access the content.
 * Has a button that links to the login screen. 
 */
public class NotLoggedInView extends LinearLayout implements View.OnClickListener
{
    private TextView textViewMessage;
    private Button button;
    private ImageView imageViewHeader;

    private LoggedInViewLoginListener listener;

    public interface LoggedInViewLoginListener
    {
        void logInClicked();
    }

    public enum NotLoggedColor
    {
        RED,
        ORANGE,
        GREEN
    }

    public NotLoggedInView(Context context)
    {
        this(context, null);
    }

    public NotLoggedInView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public NotLoggedInView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_not_logged_in, this, true);

        this.textViewMessage = (TextView) findViewById(R.id.textView_notLogged);
        this.imageViewHeader = (ImageView) findViewById(R.id.imageView);
        this.button = (Button) findViewById(R.id.button_login);
        this.button.setOnClickListener(this);

        this.setOrientation(VERTICAL);
        this.setGravity(Gravity.CENTER);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NotLoggedInView);
        this.setText(a.getString(R.styleable.NotLoggedInView_notLoggedText));
        a.recycle();
    }

    public void setColor(NotLoggedColor color)
    {
        switch (color)
        {
            case RED:
                this.button.setTextColor(getResources().getColor(R.color.sibos_red));
                this.button.setBackgroundResource(R.drawable.red_button_background);
                this.imageViewHeader.setImageResource(R.drawable.login_ic_pink);
                break;

            case ORANGE:
                this.button.setTextColor(getResources().getColor(R.color.sibos_orange));
                this.button.setBackgroundResource(R.drawable.orange_button_background);
                this.imageViewHeader.setImageResource(R.drawable.login_ic_orange);
                break;

            case GREEN:
                this.button.setTextColor(getResources().getColor(R.color.sibos_green));
                this.button.setBackgroundResource(R.drawable.green_button_background);
                this.imageViewHeader.setImageResource(R.drawable.login_ic_green);
                break;
        }
    }

    public void setImageViewHeaderVisibility(boolean visible){
        if (visible){
            imageViewHeader.setVisibility(View.VISIBLE);
        }else{
            imageViewHeader.setVisibility(View.GONE);
        }
    }

    public void setText(String text)
    {
        this.textViewMessage.setText(text);
    }

    public void setListener(LoggedInViewLoginListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.button_login)
        {
            if (this.listener != null)
            {
                this.listener.logInClicked();
            }
        }
    }
}
