package com.swift.SWIFTRegional.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

public class IconButton extends com.goodcoresoftware.android.common.views.Button
{
    protected int drawableWidth;
    protected int iconPadding;

    Rect bounds;

    public IconButton(Context context)
    {
        this(context, null);
    }

    public IconButton(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public IconButton(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs);

        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        // Slight contortion to prevent allocating in onLayout
        if (null == bounds) {
            bounds = new Rect();
        }

        iconPadding = (int)context.getResources().getDimension(R.dimen.padding_medium);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        Paint textPaint = getPaint();
        String text = getText().toString();
        textPaint.getTextBounds(text, 0, text.length(), bounds);

        int textWidth = bounds.width();
        int contentWidth = drawableWidth + iconPadding + textWidth;

        int contentLeft = (int) ((getWidth() / 2.0) - (contentWidth / 2.0));
        setCompoundDrawablePadding(-contentLeft + iconPadding);
        setPadding(contentLeft, 0, 0, 0);
    }

    @Override
    public void setCompoundDrawablesWithIntrinsicBounds(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        super.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);

        if (null != left)
        {
            drawableWidth = left.getIntrinsicWidth();
        }

        requestLayout();
    }

}
