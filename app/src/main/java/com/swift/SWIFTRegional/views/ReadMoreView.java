package com.swift.SWIFTRegional.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.goodcoresoftware.android.common.views.Button;
import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

public class ReadMoreView extends FrameLayout
{
    private static final int DEFAULT_MAXIMUM_LINES = 10;

    private String defaultButtonText;

    private LayoutedTextView textViewText;
    private Button buttonReadMore;

    private int maximumLines;
    private int calculatedMaximumLines;
    private String buttonText;

    private boolean expanded = false;

    public ReadMoreView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.defaultButtonText = getResources().getString(R.string.read_more);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ReadMoreView);
        this.maximumLines = a.getInt(R.styleable.ReadMoreView_maxLines, DEFAULT_MAXIMUM_LINES);
        String text = a.getString(R.styleable.ReadMoreView_text);
        this.buttonText = a.getString(R.styleable.ReadMoreView_buttonText);
        final float lineSpacingMultiplier = a.getFloat(R.styleable.ReadMoreView_lineSpacingMultiplier, 1);
        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.textview_readmore, this, true);

        this.textViewText = (LayoutedTextView) view.findViewById(R.id.textView_text);
        this.textViewText.setLineSpacing(0, lineSpacingMultiplier);

        this.buttonReadMore = (Button) view.findViewById(R.id.button_readMore);


        this.setText(text);
        this.buttonReadMore.setText(TextUtils.isEmpty(this.buttonText) ? defaultButtonText : this.buttonText);
        this.textViewText.setMaxLines(this.maximumLines);

        this.textViewText.setOnLayoutListener(new LayoutedTextView.OnLayoutListener()
        {
            @Override
            public void onLayouted(TextView view)
            {
                ReadMoreView.this.calculatedMaximumLines = view.getLineCount();
                if (ReadMoreView.this.calculatedMaximumLines <= ReadMoreView.this.maximumLines)
                {
                    ReadMoreView.this.textViewText.toggleGradient(false);
                    ReadMoreView.this.buttonReadMore.setVisibility(View.GONE);
                }
            }
        });

        this.buttonReadMore.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!ReadMoreView.this.expanded)
                {
                    ReadMoreView.this.clickedExpand();
                }
            }
        });
    }

    public void setText(CharSequence text)
    {
        this.textViewText.setText(text);
    }

    private void clickedExpand()
    {
        try
        {
            this.textViewText.toggleGradient(false);

            this.buttonReadMore.setVisibility(View.GONE);
            ObjectAnimator animation = ObjectAnimator.ofInt(
                    this.textViewText,
                    "maxLines",
                    1000);
            animation.setDuration(2000);
            animation.start();
        }
        catch (Exception e)
        {
            this.buttonReadMore.setVisibility(View.GONE);
            this.textViewText.setMaxLines(1000);
        }

    }
}