package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.UnsentPrivacySettingsTable;

public class PrivacySettings
{
    @SerializedName("sibosKey")
    private String sibosKey;
    @SerializedName("shareEmail")
    private boolean email;
    @SerializedName("shareCompanyPhone")
    private boolean companyPhone;
    @SerializedName("shareMobilePhone")
    private boolean mobilePhone;
    @SerializedName("shareCompanyAddress")
    private boolean companyAddress;

    public PrivacySettings()
    {
        this(false, false, false, false);
    }
    public PrivacySettings(boolean inEmail, boolean inCompanyPhone, boolean inMobilePhone, boolean inCompanyAddress)
    {
        this("", inEmail, inCompanyPhone, inMobilePhone, inCompanyAddress);
    }

    public PrivacySettings(String inSibosKey, boolean inEmail, boolean inCompanyPhone, boolean inMobilePhone, boolean inCompanyAddress)
    {
        this.sibosKey = inSibosKey;
        this.email = inEmail;
        this.companyPhone = inCompanyPhone;
        this.mobilePhone = inMobilePhone;
        this.companyAddress = inCompanyAddress;
    }


    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(UnsentPrivacySettingsTable.COLUMN_SIBOS_KEY, this.sibosKey);
        cv.put(UnsentPrivacySettingsTable.COLUMN_EMAIL, this.email);
        cv.put(UnsentPrivacySettingsTable.COLUMN_COMPANY_PHONE, this.companyPhone);
        cv.put(UnsentPrivacySettingsTable.COLUMN_MOBILE_PHONE, this.mobilePhone);
        cv.put(UnsentPrivacySettingsTable.COLUMN_COMPANY_ADDRESS, this.companyAddress);

        return cv;
    }

    public void constructFromCursor(Cursor inCursor)
    {
        this.sibosKey = DatabaseUtils.getString(inCursor, UnsentPrivacySettingsTable.COLUMN_SIBOS_KEY, UnsentPrivacySettingsTable.TABLE_NAME);
        this.email = DatabaseUtils.getBoolean(inCursor, UnsentPrivacySettingsTable.COLUMN_EMAIL, UnsentPrivacySettingsTable.TABLE_NAME);
        this.companyPhone = DatabaseUtils.getBoolean(inCursor, UnsentPrivacySettingsTable.COLUMN_COMPANY_PHONE, UnsentPrivacySettingsTable.TABLE_NAME);
        this.mobilePhone = DatabaseUtils.getBoolean(inCursor, UnsentPrivacySettingsTable.COLUMN_MOBILE_PHONE, UnsentPrivacySettingsTable.TABLE_NAME);
        this.companyAddress = DatabaseUtils.getBoolean(inCursor, UnsentPrivacySettingsTable.COLUMN_COMPANY_ADDRESS, UnsentPrivacySettingsTable.TABLE_NAME);
    }


    public boolean isEmail()
    {
        return email;
    }

    public void setEmail(boolean email)
    {
        this.email = email;
    }

    public boolean isCompanyPhone()
    {
        return companyPhone;
    }

    public void setCompanyPhone(boolean companyPhone)
    {
        this.companyPhone = companyPhone;
    }

    public boolean isMobilePhone()
    {
        return mobilePhone;
    }

    public void setMobilePhone(boolean mobilePhone)
    {
        this.mobilePhone = mobilePhone;
    }

    public boolean isCompanyAddress()
    {
        return companyAddress;
    }

    public void setCompanyAddress(boolean companyAddress)
    {
        this.companyAddress = companyAddress;
    }

    public String getSibosKey()
    {
        return sibosKey;
    }

    public void setSibosKey(String sibosKey)
    {
        this.sibosKey = sibosKey;
    }

    public String toJson()
    {
        // new Gson().toJson() is not used to generate the json, as the API does not expect the
        // valid JSON representation of this object, but a variant

        final JsonObject o = new JsonObject();

        o.addProperty("shareCompanyAddress", this.companyAddress ? "true" : "false");
        o.addProperty("shareEmail", this.email ? "true" : "false");
        o.addProperty("shareMobilePhone", this.mobilePhone ? "true" : "false");
        o.addProperty("shareCompanyPhone", this.companyPhone ? "true" : "false");

        return o.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrivacySettings that = (PrivacySettings) o;

        if (companyAddress != that.companyAddress) return false;
        if (companyPhone != that.companyPhone) return false;
        if (email != that.email) return false;
        if (mobilePhone != that.mobilePhone) return false;
        if (sibosKey != null ? !sibosKey.equals(that.sibosKey) : that.sibosKey != null)
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = sibosKey != null ? sibosKey.hashCode() : 0;
        result = 31 * result + (email ? 1 : 0);
        result = 31 * result + (companyPhone ? 1 : 0);
        result = 31 * result + (mobilePhone ? 1 : 0);
        result = 31 * result + (companyAddress ? 1 : 0);
        return result;
    }
}
