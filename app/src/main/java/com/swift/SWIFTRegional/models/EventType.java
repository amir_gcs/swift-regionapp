package com.swift.SWIFTRegional.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventType extends Category
{
    @Expose
    @SerializedName("typeId")
    protected String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    private String sessionName;

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }
}
