package com.swift.SWIFTRegional.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SimonRaes on 27/03/15.
 * Some API calls return a json with details when an error occurs.
 */
public class ApiErrorCode implements Parcelable
{
    @SerializedName("code")
    public String code;
    @SerializedName("detail")
    public String detail;
    @SerializedName("correlationId")
    public String correlationId;

    public ApiErrorCode(){

    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }

    public String getCorrelationId()
    {
        return correlationId;
    }

    public void setCorrelationId(String correlationId)
    {
        this.correlationId = correlationId;
    }


    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.code);
        dest.writeString(this.detail);
        dest.writeString(this.correlationId);
    }

    private ApiErrorCode(Parcel in)
    {
        this.code = in.readString();
        this.detail = in.readString();
        this.correlationId = in.readString();
    }

    public static final Parcelable.Creator<ApiErrorCode> CREATOR = new Parcelable.Creator<ApiErrorCode>()
    {
        public ApiErrorCode createFromParcel(Parcel source)
        {
            return new ApiErrorCode(source);
        }

        public ApiErrorCode[] newArray(int size)
        {
            return new ApiErrorCode[size];
        }
    };

    @Override
    public String toString() {
        return "ApiErrorCode{" +
                "code='" + code + '\'' +
                ", detail='" + detail + '\'' +
                ", correlationId='" + correlationId + '\'' +
                '}';
    }
}
