package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.goodcoresoftware.android.common.utils.database.AbstractDatabaseObject;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.WallTable;

/**
 * Created by Wesley on 16/05/14.
 */
public class WallItem extends AbstractDatabaseObject
{
    public enum Type
    {
        TWITTER_NAME,
        TWITTER_HASHTAG,
        FLICKR_PHOTOSET,
        NEWS,
        ISSUE,
        INSTAGRAM_IMAGE,
        INSTAGRAM_VIDEO,
        VIDEO
    }

    protected transient String id;

    protected transient String title;
    protected transient String description;

    protected transient long date;

    protected transient String pictureUrl;

    protected transient String link;

    protected transient Type type;

    public String getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDescription()
    {
        return description;
    }

    public long getDate()
    {
        return date;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public String getLink()
    {
        return link;
    }

    public Type getType()
    {
        return type;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getString(cursor, WallTable.COLUMN_ID, WallTable.TABLE_NAME);

        this.title = DatabaseUtils.getString(cursor, WallTable.COLUMN_TITLE, WallTable.TABLE_NAME);
        this.description = DatabaseUtils.getString(cursor, WallTable.COLUMN_DESCRIPTION, WallTable.TABLE_NAME);

        this.date = DatabaseUtils.getLong(cursor, WallTable.COLUMN_DATE, WallTable.TABLE_NAME);

        this.pictureUrl = DatabaseUtils.getString(cursor, WallTable.COLUMN_PICTURE_URL, WallTable.TABLE_NAME);
        this.link = DatabaseUtils.getString(cursor, WallTable.COLUMN_LINK, WallTable.TABLE_NAME);

        this.type = Type.values()[DatabaseUtils.getInt(cursor, WallTable.COLUMN_TYPE, WallTable.TABLE_NAME)];
    }

    @Override
    public ContentValues getContentValues()
    {
        return null;
    }
}
