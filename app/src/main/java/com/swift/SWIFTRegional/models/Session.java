package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.EventTable;
import com.swift.SWIFTRegional.models.session.Stream;

import java.util.ArrayList;
import java.util.Arrays;

import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

import com.swift.SWIFTRegional.models.session.SessionSpeaker;
import com.swift.SWIFTRegional.models.session.Tag;

public class Session extends Event
{



    @Expose
    @SerializedName("sessionId")
    private long sessionId;

    @Expose
    private SessionSpeaker[] sessionSpeakers;

    @Expose
    private String[] exhibitorCcvks;

    @Expose
    private String youtubeId;

    @Expose
    private Stream[] streams;

    @Expose
    private Tag[] tags;

    private String[] streamIds;
    private String[] tagIds;

    public long getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(long sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getYoutubeId()
    {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId)
    {
        this.youtubeId = youtubeId;
    }

    public Stream[] getStreams()
    {
        return this.streams;
    }

    public void setStreams(Stream[] streams)
    {
        this.streams = streams;
    }

    public Tag[] getTags()
    {
        return tags;
    }

    public void setTags(Tag[] tags)
    {
        this.tags = tags;
    }

    public SessionSpeaker[] getSessionSpeakers()
    {

        if(sessionSpeakers!=null)
        {
            ArrayList<SessionSpeaker> returnList = new ArrayList<SessionSpeaker>();

            for (SessionSpeaker sessionSpeaker : sessionSpeakers) {

                if(sessionSpeaker.getSpeakerId()!="")
                {
                    returnList.add(sessionSpeaker);
                }

            }

            return  returnList.toArray(new SessionSpeaker[0]);
        }

        return  sessionSpeakers;
    }

    public void setSessionSpeakers(SessionSpeaker[] sessionSpeakers)
    {
        this.sessionSpeakers = sessionSpeakers;
    }

    public String[] getExhibitorCcvks() {
        return exhibitorCcvks;
    }

    public void setExhibitorCcvks(String[] exhibitorCcvks) {
        this.exhibitorCcvks = exhibitorCcvks;
    }

    public void constructFromCursor(Cursor cursor)
    {
        super.constructFromCursor(cursor);

        this.sessionId = DatabaseUtils.getLong(cursor, EventTable.COLUMN_SESSION_ID);
        this.youtubeId = DatabaseUtils.getString(cursor, EventTable.COLUMN_YOUTUBE_ID);

        String stringStreams = DatabaseUtils.getString(cursor, EventTable.COLUMN_STREAMS);
        this.streamIds = stringStreams.split(",");

        String stringTags = DatabaseUtils.getString(cursor, EventTable.COLUMN_TAGS);
        this.tagIds = stringTags.split(",");
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = super.getContentValues();

        cv.put(EventTable.COLUMN_SESSION_ID, this.getSessionId());
        cv.put(EventTable.COLUMN_YOUTUBE_ID, this.getYoutubeId());


        cv.put(EventTable.COLUMN_EXHIBITOR_CCVK, getExhibitorCcvksString());


        return cv;
    }

    private String getExhibitorCcvksString() {

        if(getExhibitorCcvks() == null || getExhibitorCcvks().length == 0){
            return "";
        }

        StringBuilder ccvkStringBuilder = new StringBuilder("#");
        for(String ccvk : getExhibitorCcvks()) {
            ccvkStringBuilder.append(ccvk).append("#");
        }

        return ccvkStringBuilder.toString();
    }

    public void constructTagAndStreamFromCursor(Category[] inCategories)
    {
        ArrayList<Category> tags = new ArrayList<Category>();
        ArrayList<Category> streams = new ArrayList<Category>();

        for (String streamId : this.streamIds)
        {
            for (Category category : inCategories)
            {
                if (category != null && category.getId() != null && category.getType() != null && category.getId().equals(streamId) && category.getType().equals("streams"))
                {
                    final Stream stream = new Stream();
                    stream.constructFromCategory(category);

                    streams.add(stream);
                    break;
                }
            }
        }

        for (String tagId : this.tagIds)
        {
            for (Category category : inCategories)
            {
                if (category != null && category.getId() != null && category.getType() != null && category.getId().equals(tagId) && category.getType().equals("tags"))
                {
                    final Tag tag = new Tag();
                    tag.constructFromCategory(category);

                    tags.add(tag);
                    break;
                }
            }
        }

        this.streams = streams.toArray(new Stream[streams.size()]);
        this.tags = tags.toArray(new Tag[tags.size()]);
    }

    public void injectSessionSpeakers(ArrayList<SessionSpeaker> sessionSpeakersList)
    {
        if (sessionSpeakersList != null)
        {
            this.setSessionSpeakers(sessionSpeakersList.toArray(new SessionSpeaker[sessionSpeakersList.size()]));
        }
    }

    private String[] extractCategoryIds(Category[] categories)
    {
        String[] categoryIds = new String[categories.length];

        for (int i = 0; i < categories.length; i++)
        {
            Category category = categories[i];

            if (category.getId() != null)
            {
                categoryIds[i] = category.getId();
            }
        }

        return categoryIds;
    }
}