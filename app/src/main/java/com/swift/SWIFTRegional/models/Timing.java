package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.EventTimingTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

public class Timing
{
    @Expose
    private String timingId;
    @Expose
    private long sessionId;
    @Expose
    @SerializedName("room_code")
    private String roomCode;
    @Expose
    @SerializedName("room")
    private String roomName;
    @Expose
    @SerializedName("sortdate")
    private long sortDate;
    @Expose
    @SerializedName("displaydate")
    private String displayDate;
    @Expose
    @SerializedName("starttime")
    private String startTime;
    @Expose
    @SerializedName("endtime")
    private String endTime;

    private transient long startTimeMillis;

    public String getTimingId()
    {
        return this.timingId;
    }

    public void setTimingId(String timingId)
    {
        this.timingId = timingId;
    }

    public long getSessionId()
    {
        return this.sessionId;
    }

    public void setSessionId(long sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getRoomCode()
    {
        return roomCode;
    }

    public void setRoomCode(String roomCode)
    {
        this.roomCode = roomCode;
    }

    public String getRoomName()
    {
        return this.roomName;
    }

    public void setRoomName(String roomName)
    {
        this.roomName = roomName;
    }

    public Long getSortDate()
    {
        return this.sortDate;
    }

    public void setSortDate(long sortDate)
    {
        this.sortDate = sortDate;
        this.startTimeMillis = this.sortDateToStartTimeMillis();
    }

    public String getDisplayDate()
    {
        return this.displayDate;
    }

    public void setDisplayDate(String displayDate)
    {
        this.displayDate = displayDate;
    }

    public String getStartTime()
    {
        return this.startTime;
    }

    public long getStartTimeMillis()
    {
        if (this.startTimeMillis == 0)
        {
            this.startTimeMillis = this.sortDateToStartTimeMillis();
        }
        return this.startTimeMillis;
    }

    public void setStartTimeMillis(long startTimeMillis)
    {
        this.startTimeMillis = startTimeMillis;
        this.sortDate = this.startTimeMillisToSortDate();
    }


    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }

    public String getEndTime()
    {
        return endTime;
    }

    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }

    public long sortDateToStartTimeMillis()
    {
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmm", Locale.US);
        try
        {
            final Date date = df.parse(Long.toString(this.sortDate));
            return date.getTime();
        } catch (ParseException e)
        {
            Log.d(this.getClass().getSimpleName(), "Could not parse date " + this.sortDate);
        }
        return 0;
    }

    private long startTimeMillisToSortDate()
    {
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmm", Locale.US);
        final Date date = new Date(this.startTimeMillis);

        return Long.valueOf(df.format(date));

    }

    public boolean hasStarted()
    {
        final long startTimeMillis = this.getStartTimeMillis();
        final long currentTimeMillis = System.currentTimeMillis();

        return startTimeMillis < currentTimeMillis;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.roomName = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_ROOM_NAME);
        this.roomCode = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_ROOM_CODE);
        //this.sortDate = DatabaseUtils.getLong(cursor, TimingTable.COLUMN_SORT_DATE, TimingTable.TABLE_NAME);
        //this.displayDate = DatabaseUtils.getString(cursor, TimingTable.COLUMN_DISPLAY_DATE, TimingTable.TABLE_NAME);
        this.startTime = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_START_TIME);
        this.endTime = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_END_TIME);
        this.sessionId = DatabaseUtils.getLong(cursor, EventTimingTable.COLUMN_EVENT_ID);
        this.timingId = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_ID);
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();
        cv.put(EventTimingTable.COLUMN_ID, this.timingId);
        cv.put(EventTimingTable.COLUMN_ROOM_NAME, this.roomName);
        cv.put(EventTimingTable.COLUMN_ROOM_CODE, this.roomCode);
        //cv.put(TimingTable.COLUMN_SORT_DATE, this.sortDate);
        //cv.put(TimingTable.COLUMN_DISPLAY_DATE, this.displayDate);
        cv.put(EventTimingTable.COLUMN_START_TIME, this.startTime);
        cv.put(EventTimingTable.COLUMN_END_TIME, this.endTime);
        cv.put(EventTimingTable.COLUMN_EVENT_ID, this.sessionId);

        cv.put(EventTimingTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(this.roomName));

        return cv;
    }

}
