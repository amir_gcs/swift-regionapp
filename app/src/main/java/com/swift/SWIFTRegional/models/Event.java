package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.CategoryTable;
import com.swift.SWIFTRegional.database.tables.EventTable;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

public class Event
{
    @SerializedName("eventId")
    protected long mainEventId;


    @Expose
    protected String title;

    @Expose
    protected String description;


    protected String sessionTypeText;

    @Expose
    @SerializedName("sessionType")
    protected EventType type;

    @SerializedName("timings")
    protected EventTiming[] eventTimings;

    public long getEventId()
    {
        return mainEventId;
    }

    public void setEventId(long id)
    {
        this.mainEventId = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getSessionType()
    {
        return sessionTypeText;
    }

    public void setSessionType(String sessionType)
    {
        this.sessionTypeText = sessionType;
    }

    public EventType getType()
    {
        return type;
    }

    public void setType(EventType type)
    {
        this.type = type;
    }

    public EventTiming[] getEventTimings()
    {
        return eventTimings;
    }

    public void setEventTimings(EventTiming[] eventTimings)
    {
        this.eventTimings = eventTimings;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.mainEventId = DatabaseUtils.getLong(cursor, EventTable.COLUMN_EVENT_ID);
        this.title = DatabaseUtils.getString(cursor, EventTable.COLUMN_TITLE);
        this.description = DatabaseUtils.getString(cursor, EventTable.COLUMN_DESCRIPTION);

        this.sessionTypeText = DatabaseUtils.getString(cursor, EventTable.COLUMN_SESSION_TYPE);
        this.type = new EventType();
        this.type.setSessionId(DatabaseUtils.getString(cursor, EventTable.COLUMN_TYPEID));
        this.type.setSessionName(DatabaseUtils.getString(cursor, CategoryTable.COLUMN_NAME));
        this.type.constructFromCursor(cursor);
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(EventTable.COLUMN_EVENT_ID, getEventId());
        cv.put(EventTable.COLUMN_TITLE, this.getTitle());
        cv.put(EventTable.COLUMN_DESCRIPTION, this.getDescription());
        cv.put(EventTable.COLUMN_SESSION_TYPE, this.getSessionType());
        cv.put(EventTable.COLUMN_TYPE, this.getType().getName());
        cv.put(EventTable.COLUMN_TYPEID,this.getType().getSessionId());

        String eventTimingNormalizedStr = "";
        for(EventTiming et : eventTimings){
             String dateStart = DateUtils.formattedDateFromString("yyyy-MM-dd'T'hh:mm:ss","",et.getStartDate());
                eventTimingNormalizedStr+= dateStart+ " " +DateUtils.formattedDateFromString("yyyy-MM-dd'T'hh:mm:ss","",et.getEndDate()) ;
        }
        cv.put(EventTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(SibosUtils.getNullSafeString(this.title)+" "+SibosUtils.getNullSafeString(this.getType().getName())+" "+eventTimingNormalizedStr));

        return cv;
    }

}
