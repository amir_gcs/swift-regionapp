package com.swift.SWIFTRegional.models.favourites;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Inneke on 5/05/2014.
 * Used for speaker, session and exhibitor favourites.
 */
public class Favourite
{
    protected String id;
    // The different favourite API calls each use their own key
    private String speakerId;
    private String sessionId;
    private String ccvk;
    private String sibosKey;
    private String eventId;

    private int type;

    public Favourite()
    {
    }

    public int getTypeOrdinal()
    {
        return type;
    }

    public void setType(FavoriteTable.FavouriteType type)
    {
        this.type = type.ordinal();
    }

    public String getId()
    {
        if (TextUtils.isEmpty(this.id))
        {
            if (!TextUtils.isEmpty(this.speakerId))
            {
                this.id = this.speakerId;
            }
            else if (!TextUtils.isEmpty(this.sessionId))
            {
                this.id = this.sessionId;
            }
            else if (!TextUtils.isEmpty(this.ccvk))
            {
                this.id = this.ccvk;
            }
            else if (!TextUtils.isEmpty(this.sibosKey))
            {
                this.id = this.sibosKey;
            }
            else if (!TextUtils.isEmpty(this.eventId))
            {
                this.id = this.eventId;
            }
            else
            {
                this.id = "";
            }
        }

        return this.id;
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(FavoriteTable.COLUMN_TYPE, this.getTypeOrdinal());
        cv.put(FavoriteTable.COLUMN_FAVORITE_ID, this.getId());

        return cv;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.type = DatabaseUtils.getInt(cursor, FavoriteTable.COLUMN_TYPE);
        this.id = DatabaseUtils.getString(cursor, FavoriteTable.COLUMN_FAVORITE_ID);
    }

}
