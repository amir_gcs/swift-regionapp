package com.swift.SWIFTRegional.models.flickr;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.FlickrPhotosetsTable;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Yves on 12/03/14.
 */
public class FlickrPhotoset extends WallItem
{
    @SerializedName("primary")
    private String coverPhotoId;

    @SerializedName("id")
    private String photosetId;

    @SerializedName("title")
    private FlickrText flickrTitle;

    @SerializedName("description")
    private FlickrText flickrDescription;

    @SerializedName("date_update")
    protected long dateUpdate;

    private String farm;
    private String secret;
    private String server;

    private transient String description;

    public String getPhotosetId()
    {
        return this.photosetId;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        this.photosetId = DatabaseUtils.getString(cursor, FlickrPhotosetsTable.COLUMN_ID, FlickrPhotosetsTable.TABLE_NAME);

        this.title = DatabaseUtils.getString(cursor, FlickrPhotosetsTable.COLUMN_TITLE, FlickrPhotosetsTable.TABLE_NAME);
        this.description = DatabaseUtils.getString(cursor, FlickrPhotosetsTable.COLUMN_DESCRIPTION, FlickrPhotosetsTable.TABLE_NAME);

        this.date = DatabaseUtils.getLong(cursor, FlickrPhotosetsTable.COLUMN_DATE, FlickrPhotosetsTable.TABLE_NAME);

        this.pictureUrl = DatabaseUtils.getString(cursor, FlickrPhotosetsTable.COLUMN_PICTURE_URL, FlickrPhotosetsTable.TABLE_NAME);

        this.type = Type.values()[DatabaseUtils.getInt(cursor, FlickrPhotosetsTable.COLUMN_TYPE, FlickrPhotosetsTable.TABLE_NAME)];
    }

    @Override
    public ContentValues getContentValues()
    {
        this.postProcessJson();

        final ContentValues contentValues = new ContentValues();

        contentValues.put(FlickrPhotosetsTable.COLUMN_ID, this.photosetId);
        contentValues.put(FlickrPhotosetsTable.COLUMN_TITLE, this.flickrTitle.getText());
        contentValues.put(FlickrPhotosetsTable.COLUMN_DESCRIPTION, this.flickrDescription.getText());
        contentValues.put(FlickrPhotosetsTable.COLUMN_DATE, this.dateUpdate);
        contentValues.put(FlickrPhotosetsTable.COLUMN_PICTURE_URL, this.pictureUrl);
        contentValues.put(FlickrPhotosetsTable.COLUMN_SEARCH_NORMALIZED,
                SibosUtils.getNormalizedText(this.flickrTitle.getText() + " " + this.flickrDescription.getText()));

        return contentValues;
    }

    public void postProcessJson()
    {
        this.dateUpdate = this.dateUpdate * 1000; // put the date in milliseconds

        //TODO get the size based on the size of the screen
        this.pictureUrl = String.format("https://farm%1$s.static.flickr.com/%2$s/%3$s_%4$s_%5$s.jpg", this.farm, this.server, this.coverPhotoId, this.secret, "b");
    }
}
