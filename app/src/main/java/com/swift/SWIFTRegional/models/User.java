package com.swift.SWIFTRegional.models;

public class User
{
    private String salutation = "";
    private String firstName = "";
    private String lastName = "";
    private String email = "";
    private String sibosKey = "";
    private String country = "";
    private String pictureUrl = "";
    private String jobTitle = "";
    private String companyName = "";
    private boolean resetPassword = false;
    private String[] roles;
    private String twitterId = "";
    private String linkedinUrl = "";

    public User()
    {
    }

    public String getSalutation()
    {
        return salutation;
    }

    public void setSalutation(String salutation)
    {
        this.salutation = salutation;
    }

    public String getSibosKey()
    {
        return sibosKey;
    }

    public void setSibosKey(String sibosKey)
    {
        this.sibosKey = sibosKey;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl)
    {
        this.pictureUrl = pictureUrl;
    }

    public String getJobTitle()
    {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public boolean isResetPassword()
    {
        return resetPassword;
    }

    public void setResetPassword(boolean resetPassword)
    {
        this.resetPassword = resetPassword;
    }

    public String[] getRoles()
    {
        return roles;
    }

    public void setRoles(String[] roles)
    {
        this.roles = roles;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getLinkedinUrl() {
        return linkedinUrl;
    }

    public void setLinkedinUrl(String linkedinUrl) {
        this.linkedinUrl = linkedinUrl;
    }
}
