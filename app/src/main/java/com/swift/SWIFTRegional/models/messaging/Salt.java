package com.swift.SWIFTRegional.models.messaging;

/**
 * Created by SimonRaes on 8/05/15.
 * Salt for the encryption used when generating the QuickBlox messaging password.
 */
public class Salt
{
    private String salt;

    public Salt()
    {
    }

    public String getSalt()
    {
        return salt;
    }
}
