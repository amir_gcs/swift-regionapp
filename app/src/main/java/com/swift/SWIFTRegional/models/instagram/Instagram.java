package com.swift.SWIFTRegional.models.instagram;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.InstagramTable;

/**
 * Created by wesley on 24/06/14.
 */
public class Instagram extends WallItem
{
    @SerializedName("id")
    private String instagramId;

    @SerializedName("link")
    private String instagramLink;

    @SerializedName("created_time")
    protected long dateUpdate;

    private InstagramImages images;
    private InstagramCaption caption;

    @SerializedName("type")
    private MediaType mediaType;

    public enum MediaType
    {
        image,
        video
    }

    private static final class InstagramImages
    {
        @SerializedName("standard_resolution")
        private InstagramImage normal;
    }

    private static final class InstagramImage
    {
        private String url;
    }

    private static final class InstagramCaption
    {
        private String text;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getString(cursor, InstagramTable.COLUMN_ID, InstagramTable.TABLE_NAME);

        this.description = DatabaseUtils.getString(cursor, InstagramTable.COLUMN_DESCRIPTION, InstagramTable.TABLE_NAME);

        this.date = DatabaseUtils.getLong(cursor, InstagramTable.COLUMN_DATE, InstagramTable.TABLE_NAME);

        this.pictureUrl = DatabaseUtils.getString(cursor, InstagramTable.COLUMN_PICTURE_URL, InstagramTable.TABLE_NAME);
        this.link = DatabaseUtils.getString(cursor, InstagramTable.COLUMN_LINK, InstagramTable.TABLE_NAME);

        this.type = Type.values()[DatabaseUtils.getInt(cursor, InstagramTable.COLUMN_TYPE, InstagramTable.TABLE_NAME)];
    }

    @Override
    public ContentValues getContentValues()
    {
        this.postProcessJson();

        final ContentValues contentValues = new ContentValues();

        contentValues.put(InstagramTable.COLUMN_ID, this.instagramId);
        contentValues.put(InstagramTable.COLUMN_LINK, this.instagramLink);
        contentValues.put(InstagramTable.COLUMN_DESCRIPTION, this.caption.text);
        contentValues.put(InstagramTable.COLUMN_DATE, this.dateUpdate);
        contentValues.put(InstagramTable.COLUMN_PICTURE_URL, this.images.normal.url);
        contentValues.put(InstagramTable.COLUMN_TYPE, this.mediaType.ordinal());
        contentValues.put(InstagramTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(this.caption.text));

        return contentValues;
    }

    public void postProcessJson()
    {
        this.dateUpdate = this.dateUpdate * 1000; // put the date in milliseconds
    }
}
