package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.swift.SWIFTRegional.api.dto.RateDTO;
import com.swift.SWIFTRegional.database.tables.RateTable;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

public class Rate
{
    private String id;
    private int type;
    private int rating;

    private transient boolean unsent;

    public Rate(RateDTO rateDTO, RateTable.RateType type)
    {
        this.id = rateDTO.getExternalId();
        this.rating = rateDTO.getRating();
        this.type = type.ordinal();
    }

    public Rate()
    {
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getString(cursor, RateTable.COLUMN_ID, RateTable.TABLE_NAME);
        this.type = DatabaseUtils.getInt(cursor, RateTable.COLUMN_TYPE, RateTable.TABLE_NAME);
        this.rating = DatabaseUtils.getInt(cursor, RateTable.COLUMN_RATE, RateTable.TABLE_NAME);
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(RateTable.COLUMN_ID, this.id);
        cv.put(RateTable.COLUMN_TYPE, this.type);
        cv.put(RateTable.COLUMN_RATE, this.rating);

        return cv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isUnsent()
    {
        return unsent;
    }

    public void setUnsent(boolean unsent)
    {
        this.unsent = unsent;
    }
}
