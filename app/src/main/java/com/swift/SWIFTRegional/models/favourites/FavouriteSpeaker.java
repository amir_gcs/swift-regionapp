//package mobi.inthepocket.sibos.models.favourites;
//
//import android.content.ContentValues;
//import android.database.Cursor;
//
//import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
//import mobi.inthepocket.sibos.database.tables.FavoriteTable;
//
///**
// * Created by SimonRaes on 3/04/15.
// */
//public class FavouriteSpeaker extends Favourite
//{
//    private String speakerId;
//
//
////    public FavouriteSpeaker(String speakerId)
////    {
////        this.speakerId = speakerId;
////    }
//
//
//    public void setSessionId(String speakerId)
//    {
//        this.speakerId = speakerId;
//    }
//
//    @Override
//    public String getSessionId()
//    {
//        return speakerId;
//    }
//
//    public ContentValues getContentValues()
//    {
//        final ContentValues cv = new ContentValues();
//
//        cv.put(FavoriteTable.COLUMN_TYPE, FavoriteTable.FavouriteType.SPEAKER.ordinal());
//        cv.put(FavoriteTable.COLUMN_FAVORITE_ID, this.speakerId);
//
//        return cv;
//    }
//
//    public void constructFromCursor(Cursor cursor)
//    {
//        this.type = FavoriteTable.FavouriteType.SPEAKER;
//        this.speakerId = DatabaseUtils.getString(cursor, FavoriteTable.COLUMN_FAVORITE_ID);
//    }
//}
