package com.swift.SWIFTRegional.models.event;

import android.database.Cursor;

/**
 * Created by khuru on 06-Jan-2017.
 */

public class RegionTwoItem {
    public RegionInfo regionInfo1;
    public RegionInfo regionInfo2;
    public void constructFromCursor(Cursor cursor) {
        regionInfo1 = new RegionInfo();
        regionInfo1.constructFromCursor(cursor);
        regionInfo2 = new RegionInfo();
        regionInfo2.constructEvent2FromCursor(cursor);
    }
}
