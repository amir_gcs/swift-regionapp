package com.swift.SWIFTRegional.models.exhibitor;

import android.database.Cursor;

/**
 * Created by zesshan on 11/04/16.
 */
public class ExhibitorTwoItems {

    public Exhibitor exhibitor1;
    public Exhibitor exhibitor2;

    public void constructFromCursor(Cursor cursor) {
        exhibitor1 = new Exhibitor();
        exhibitor1.constructFromCursor(cursor);
        exhibitor2 = new Exhibitor();
        exhibitor2.constructExhibitor2FromCursor(cursor);
        exhibitor2 = exhibitor2.getExhibitorId() == -1 ? null : exhibitor2;
    }
}
