package com.swift.SWIFTRegional.models.twitter;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.TweetsTable;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Yves on 12/03/14.
 */
public class Tweet extends WallItem
{
    @SerializedName("id_str")
    protected String id;

    @SerializedName("created_at")
    private String dateString;

    @SerializedName("text")
    protected String description;

    private TwitterUser user;

    private TwitterEntity entities;

    private transient long date;

    public String getId()
    {
        return id;
    }

    public long getDate()
    {
        return date;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getString(cursor, TweetsTable.COLUMN_ID, TweetsTable.TABLE_NAME);

        this.title = DatabaseUtils.getString(cursor, TweetsTable.COLUMN_TITLE, TweetsTable.TABLE_NAME);
        this.description = DatabaseUtils.getString(cursor, TweetsTable.COLUMN_DESCRIPTION, TweetsTable.TABLE_NAME);

        this.date = DatabaseUtils.getLong(cursor, TweetsTable.COLUMN_DATE, TweetsTable.TABLE_NAME);

        this.pictureUrl = DatabaseUtils.getString(cursor, TweetsTable.COLUMN_PICTURE_URL, TweetsTable.TABLE_NAME);

        this.type = Type.values()[DatabaseUtils.getInt(cursor, TweetsTable.COLUMN_TYPE, TweetsTable.TABLE_NAME)];
    }

    public ContentValues getContentValues()
    {
        this.postProcessJson();

        final ContentValues contentValues = new ContentValues();

        contentValues.put(TweetsTable.COLUMN_ID, this.id);
        contentValues.put(TweetsTable.COLUMN_DATE, this.date);
        contentValues.put(TweetsTable.COLUMN_DESCRIPTION, this.description);
        contentValues.put(TweetsTable.COLUMN_TITLE, this.user.getScreenName());

        String name = "@"+this.user.getScreenName();

        contentValues.put(TweetsTable.COLUMN_SEARCH_NORMALIZED, name + SibosUtils.getNormalizedText(" " + this.description));

        if(this.entities != null && this.entities.getMedia() != null && this.entities.getMedia().length > 0)
        {
            contentValues.put(TweetsTable.COLUMN_PICTURE_URL, this.entities.getMedia()[0].getMediaUrl());
        }else
        {
            contentValues.put(TweetsTable.COLUMN_PICTURE_URL, "");
        }


        return contentValues;
    }

    private void postProcessJson()
    {
        this.date = DateUtils.getTwitterTimestamp(this.dateString);
    }
}
