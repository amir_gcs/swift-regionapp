package com.swift.SWIFTRegional.models;

/**
 * Created by zesshan on 06/04/16.
 */
public class EventTimingTwoItems extends EventTiming {
    public String testStr = "yay! my rocket science worked";

    public EventTiming event1;
    public EventTiming event2;

    public Boolean hasSecondEvent = false;
}
