package com.swift.SWIFTRegional.models.exhibitor;

import android.content.ContentValues;
import android.database.Cursor;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.ExhibitorCollateralTable;

public class ExhibitorCollateral
{
    private String id;
    private int sequence;
    private String ccvk;
    private String title;
    private String description;
    private String url;

    public ExhibitorCollateral()
    {
        super();
    }

    public ExhibitorCollateral(String id, String ccvk, int sequence, String title, String description, String url)
    {
        this.id = id;
        this.ccvk = ccvk;
        this.sequence = sequence;
        this.title = title;
        this.description = description;
        this.url = url;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getCcvk()
    {
        return ccvk;
    }

    public void setCcvk(String ccvk)
    {
        this.ccvk = ccvk;
    }

    public int getSequence()
    {
        return sequence;
    }

    public void setSequence(int sequence)
    {
        this.sequence = sequence;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getString(cursor, ExhibitorCollateralTable.COLUMN_ID);
        this.ccvk = DatabaseUtils.getString(cursor, ExhibitorCollateralTable.COLUMN_CCVK);
        this.sequence = DatabaseUtils.getInt(cursor, ExhibitorCollateralTable.COLUMN_SEQUENCE);
        this.title = DatabaseUtils.getString(cursor, ExhibitorCollateralTable.COLUMN_TITLE);
        this.description = DatabaseUtils.getString(cursor, ExhibitorCollateralTable.COLUMN_DESCRIPTION);
        this.url = DatabaseUtils.getString(cursor, ExhibitorCollateralTable.COLUMN_URL);
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();
        cv.put(ExhibitorCollateralTable.COLUMN_ID, this.id);
        cv.put(ExhibitorCollateralTable.COLUMN_CCVK, this.ccvk);
        cv.put(ExhibitorCollateralTable.COLUMN_SEQUENCE, this.sequence);
        cv.put(ExhibitorCollateralTable.COLUMN_TITLE, this.title);
        cv.put(ExhibitorCollateralTable.COLUMN_DESCRIPTION, this.description);
        cv.put(ExhibitorCollateralTable.COLUMN_URL, this.url);

        return cv;
    }
}
