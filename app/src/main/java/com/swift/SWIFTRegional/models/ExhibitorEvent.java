package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.swift.SWIFTRegional.database.tables.EventTable;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.contentproviders.EventTimingContentProvider;

public class ExhibitorEvent extends Event
{
    private String ccvk;

    private Exhibitor exhibitor;

    public static final EventType TYPE = new EventType();

    static {
        TYPE.setId(EventTimingContentProvider.CATEGORY_ID_EXHIBITOR);
        TYPE.setName("Exhibitor event");
        TYPE.setType("types");
    }

    public ExhibitorEvent()
    {
        super();

        this.type = TYPE;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        super.constructFromCursor(cursor);
        this.ccvk = DatabaseUtils.getString(cursor, EventTable.COLUMN_CCVK);
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = super.getContentValues();

        cv.put(EventTable.COLUMN_CCVK, this.getCcvk());

        return cv;
    }

    public String getCcvk()
    {
        return ccvk;
    }

    public void setCcvk(String ccvk)
    {
        this.ccvk = ccvk;
    }

    public Exhibitor getExhibitor()
    {
        return exhibitor;
    }

    public void setExhibitor(Exhibitor exhibitor)
    {
        this.exhibitor = exhibitor;
    }
}
