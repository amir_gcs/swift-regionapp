package com.swift.SWIFTRegional.models;

/**
 * Created by SimonRaes on 19/08/15.
 * Keeps track of some info related to the groups in collapsible listviews.
 */
public class HeaderState
{
    private int childCount;
    private boolean collapsed;

    public HeaderState(boolean collapsed, int childCount)
    {
        this.collapsed = collapsed;
        this.childCount = childCount;
    }

    public int getChildCount()
    {
        return childCount;
    }

    public void setChildCount(int childCount)
    {
        this.childCount = childCount;
    }

    public boolean isCollapsed()
    {
        return collapsed;
    }

    public void setCollapsed(boolean collapsed)
    {
        this.collapsed = collapsed;
    }
}
