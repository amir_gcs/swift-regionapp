package com.swift.SWIFTRegional.models.event;

import android.content.ContentValues;
import android.database.Cursor;

import com.swift.SWIFTRegional.database.tables.RegionInfoTable;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Inneke on 3/04/14.
 */
public class RegionInfo
{
    private long regionId;
    private String name;
    private String backgroundImgURL;
    private long eventCount;
    private String id;

    public String getId()
    {
        return this.id;
    }

    public void setRegionId(long regionId)
    {

            this.regionId = regionId;

    }

    public long getRegionId()
    {
        return regionId;
    }

    public String getName()
    {
        if(name.equals("AAShow All"))
        {
            return "Show All";
        }
        return name;
    }




    public void constructEvent2FromCursor(Cursor cursor) {
        constructFromCursor(cursor, true);
    }

    public void constructFromCursor(Cursor cursor){
        constructFromCursor(cursor, false);
    }

    public void constructFromCursor(Cursor cursor, Boolean isSecondEvent)
    {
        String postfix = isSecondEvent? "2" : "";
        this.regionId = DatabaseUtils.getInt(cursor, RegionInfoTable.COLUMN_ID + postfix);
        this.name = DatabaseUtils.getString(cursor, RegionInfoTable.COLUMN_NAME + postfix);
        this.backgroundImgURL=DatabaseUtils.getString(cursor, RegionInfoTable.COLUMN_PICTUREURL + postfix);
        this.eventCount = DatabaseUtils.getInt(cursor,RegionInfoTable.COLUMN_EVENTCOUNT+postfix);

    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();
        cv.put(RegionInfoTable.COLUMN_ID, this.regionId);
        cv.put(RegionInfoTable.COLUMN_NAME, this.name);
        cv.put(RegionInfoTable.COLUMN_PICTUREURL,this.backgroundImgURL);

        return cv;
    }

    @Override
    public String toString() {
        return "RegionInfo{" +
                "regionid=" + regionId +
                ", name='" + name + '\'' +
                ", backgroundImgURL='" + backgroundImgURL + '\'' +
                '}';
    }


    public String getBackgroundImgURL() {
        return backgroundImgURL;
    }

    public long getEventCount() {
        return eventCount;
    }

    public void setEventCount(long eventCount) {
        this.eventCount = eventCount;
    }
}
