package com.swift.SWIFTRegional.models.spinner;

import android.support.annotation.DrawableRes;

/**
 * Created by SimonRaes on 10/04/15.
 * Item to be displayed in the filter spinners in the toolbar.
 * Will be turned into a title if it contains any child items.
 */
public class SpinnerItem
{
    private String title;
    private String value;
    private int drawable;
    private SpinnerItem[] childItems;
    private boolean divider;
    private SpinnerItemType type;

    public SpinnerItem(String title)
    {
        this(title, 0);
    }

    public SpinnerItem(String title, String value)
    {
        this(title, 0);
        this.value = value;
    }

    public SpinnerItem(String title, String value, SpinnerItemType type) {
        this(title, value);
        this.type = type;
    }


    public SpinnerItem(String title, boolean hasDivider)
    {
        this(title, 0);
        this.divider = hasDivider;
    }


    public SpinnerItem(String title, @DrawableRes int drawable)
    {
        this.title = title;
        this.drawable = drawable;
    }

    public SpinnerItem(String title, @DrawableRes int drawable, SpinnerItemType type) {
        this(title, drawable);
        this.type = type;
    }

    public SpinnerItem(String title, SpinnerItem[] childItems)
    {
        this.title = title;
        this.childItems = childItems;
        if (this.childItems != null && this.childItems.length > 0)
        {
            this.childItems[this.childItems.length - 1].divider = true;
        }
    }

    public String getTitle()
    {
        return title;
    }

    public String getValue()
    {
        return value;
    }

    public SpinnerItemType getType(){
        return type;
    }

    public int getDrawable()
    {
        return drawable;
    }

    public SpinnerItem[] getChildItems()
    {
        return childItems;
    }

    public boolean isDivider()
    {
        return divider;
    }

    public enum SpinnerItemType {
        SessionFull,
        SessionTV,
        SessionExhibitor,
        SessionFavourite,
        Stream,
        Tag,
        SessionDay,
        SessionRoom,
        SessionType
    }
}

