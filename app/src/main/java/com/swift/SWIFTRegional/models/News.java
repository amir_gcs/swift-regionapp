package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.NewsTable;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.SibosUtils;

/**
 * Created by Inneke on 9/05/2014.
 */
public class News extends WallItem
{
    private static final String STORIFY_URL_BEGIN = "//storify";
    @SerializedName("title")
    protected String title;

    @SerializedName("slug")
    protected String description;

    @SerializedName("date")
    private String dateString;

    @SerializedName("html_content")
    private String htmlContent;

    @SerializedName("picture")
    protected String pictureUrl;

    @SerializedName("feature")
    private String featureString;
    private String category;

    private String link = "";

    private transient boolean feature;
    private transient long date;

    @Override
    public String getId()
    {
        return this.id;
    }

    @Override
    public String getTitle()
    {
        return this.title;
    }

    @Override
    public String getDescription()
    {
        return this.description;
    }

    public String getHtmlContent()
    {
        return htmlContent;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public boolean isFeature()
    {
        return feature;
    }

    public String getCategory()
    {
        return category;
    }

    public long getDate()
    {
        return date;
    }

    public String getLink()
    {
        return link;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.constructFromCursor(cursor, NewsTable.TABLE_NAME);
    }

    public void constructFromCursorForDetail(Cursor cursor)
    {
        final String projectionId = "newsDetail";

        this.constructFromCursor(cursor, projectionId);

        this.link = DatabaseUtils.getString(cursor, NewsTable.COLUMN_LINK, projectionId);

        this.category = DatabaseUtils.getString(cursor, NewsTable.COLUMN_CATEGORY, projectionId);
    }

    private void constructFromCursor(Cursor cursor, String projectionId)
    {
        this.id = DatabaseUtils.getString(cursor, NewsTable.COLUMN_ID, projectionId);

        this.title = DatabaseUtils.getString(cursor, NewsTable.COLUMN_TITLE, projectionId);
        this.description = DatabaseUtils.getString(cursor, NewsTable.COLUMN_DESCRIPTION, projectionId);

        this.date = DatabaseUtils.getLong(cursor, NewsTable.COLUMN_DATE, projectionId);

        this.type = Type.values()[DatabaseUtils.getInt(cursor, NewsTable.COLUMN_TYPE, projectionId)];

        this.link = DatabaseUtils.getString(cursor, NewsTable.COLUMN_LINK, projectionId);

        this.htmlContent = DatabaseUtils.getString(cursor, NewsTable.COLUMN_HTML_CONTENT, projectionId);
    }

    public ContentValues getContentValues()
    {
        this.postProcessJson();

        final ContentValues cv = new ContentValues();

        cv.put(NewsTable.COLUMN_HTML_CONTENT, this.htmlContent);
        cv.put(NewsTable.COLUMN_DESCRIPTION, this.description);
        cv.put(NewsTable.COLUMN_PICTURE_URL, this.pictureUrl);
        cv.put(NewsTable.COLUMN_FEATURE, this.feature);
        cv.put(NewsTable.COLUMN_CATEGORY, this.category);
        cv.put(NewsTable.COLUMN_TITLE, this.title);
        cv.put(NewsTable.COLUMN_DATE, this.date);
        cv.put(NewsTable.COLUMN_LINK, this.link);

        cv.put(NewsTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(this.title + " " + this.description));

        return cv;
    }

    private void postProcessJson()
    {
        this.feature = "yes".equals(this.featureString);
        this.date = DateUtils.getSibosTimestamp(this.dateString);
    }

    public boolean isStorifyItem()
    {
        return !TextUtils.isEmpty(this.htmlContent) && this.htmlContent.contains(STORIFY_URL_BEGIN);
    }

}
