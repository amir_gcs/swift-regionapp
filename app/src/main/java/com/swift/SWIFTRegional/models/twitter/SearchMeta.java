package com.swift.SWIFTRegional.models.twitter;

/**
 * Created by khurum ahmed khan on 28/2/2017.
 */

public class SearchMeta {

    private String next_results;

    public String getNext_results() {
        return next_results;
    }

    public void setNext_results(String next_results) {
        this.next_results = next_results;
    }
}
