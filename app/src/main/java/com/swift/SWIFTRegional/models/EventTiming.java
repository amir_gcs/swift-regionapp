package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.swift.SWIFTRegional.fragments.programme.SessionListFragment;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.CategoryTable;
import com.swift.SWIFTRegional.database.tables.EventTimingTable;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.utils.DateUtils;

public class EventTiming implements Comparable<EventTiming>
{
    protected long id;
    protected long eventId;

    private String roomName;

    private String roomCode;

    private String startDate;

    private String endDate;

    private Boolean rehearsal;

    private String date;

    @Expose
    private Event event;

    private boolean isFavourite;

    public long startTimeMillis;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public long getEventId()
    {
        return eventId;
    }

    public void setEventId(long eventId)
    {
        this.eventId = eventId;
    }

    public String getRoomCode()
    {
        return roomCode;
    }

    public void setRoomCode(String roomCode)
    {
        this.roomCode = roomCode;
    }

    public String getRoomName()
    {
        return roomName;
    }

    public void setRoomName(String roomName)
    {
        this.roomName = roomName;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public String getStartDateOnlyDate() {
        return startDate.substring(0, 10).replaceAll("-","");
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public Boolean isRehearsal()
    {
        if(rehearsal==null)
        {
            rehearsal=false;
            return false;
        }
        return rehearsal;
    }

    public void setRehearsal(Boolean rehearsal)
    {
        this.rehearsal = rehearsal;
    }

    public Event getEvent()
    {
        return event;
    }

    public void setEvent(Event event)
    {
        this.event = event;
    }

    public void setStartTimeMillis(long startTimeMillis)
    {
        this.startTimeMillis = startTimeMillis;
    }

    public long getStartTimeMillis()
    {
        return startTimeMillis;
    }

    public boolean isFavourite()
    {
        return isFavourite;

    }

    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getLong(cursor, EventTimingTable.COLUMN_ID);
        this.roomName = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_ROOM_NAME);
        this.roomCode = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_ROOM_CODE);
        this.startDate = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_START_TIME);
        this.endDate = DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_END_TIME);
        this.eventId = DatabaseUtils.getLong(cursor, EventTimingTable.COLUMN_EVENT_ID);
        this.rehearsal = DatabaseUtils.getBoolean(cursor, EventTimingTable.COLUMN_IS_REHEARSAL);
        this.date =  DatabaseUtils.getString(cursor, EventTimingTable.COLUMN_DATE);
        this.startTimeMillis = DateUtils.iso8601StringToDate(this.startDate).getTime();

        String typeId = DatabaseUtils.getString(cursor, CategoryTable.COLUMN_CATEGORY_ID);

        // Check if the join with the favourites table resulted in a match
        if (cursor.getColumnIndex(FavoriteTable.COLUMN_FAVORITE_ID) >= 0)
        {
            String favouriteId = DatabaseUtils.getString(cursor, FavoriteTable.COLUMN_FAVORITE_ID);
            this.isFavourite = !TextUtils.isEmpty(favouriteId);
        }

        if (typeId.equals(ExhibitorEvent.TYPE.getId()) )
        {
            ExhibitorEvent exhibitorEvent = new ExhibitorEvent();
            exhibitorEvent.constructFromCursor(cursor);

            this.setEvent(exhibitorEvent);
        }
        else
        {
            Session session = new Session();
            session.constructFromCursor(cursor);

            this.setEvent(session);
        }
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(EventTimingTable.COLUMN_ID, this.id);
        cv.put(EventTimingTable.COLUMN_ROOM_NAME, SibosUtils.getNullSafeString(this.roomName));
        cv.put(EventTimingTable.COLUMN_ROOM_CODE, SibosUtils.getNullSafeString(this.roomCode));
        cv.put(EventTimingTable.COLUMN_START_TIME, this.startDate);
        cv.put(EventTimingTable.COLUMN_DATE, this.date);
        cv.put(EventTimingTable.COLUMN_END_TIME, this.endDate);
        cv.put(EventTimingTable.COLUMN_EVENT_ID, this.eventId);
        cv.put(EventTimingTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText( SibosUtils.getNullSafeString(this.roomName)));
        isRehearsal();
        cv.put(EventTimingTable.COLUMN_IS_REHEARSAL, this.rehearsal);

        return cv;
    }

    @Override
    public int compareTo(@NonNull EventTiming another)
    {
        try
        {
            if(SessionListFragment.selectedMode=="DAY")
            {
                return this.getStartDate().compareTo(another.getStartDate());

            }
            else
            {
                return this.getEvent().getType().getName().compareTo(another.getEvent().getType().getName());
            }
        }
        catch(Exception e)
        {
            return  0;
        }



    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
