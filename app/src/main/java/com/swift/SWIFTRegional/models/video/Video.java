package com.swift.SWIFTRegional.models.video;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.VideoTable;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Created by wesley on 30/06/14.
 */
public class Video extends WallItem
{
    private ResourceId resourceId;

    @SerializedName("title")
    private String videoTitle;

    @SerializedName("publishedAt")
    private String youtubeDateUpdate;

    private Thumbnails thumbnails;

    private transient String videoLink;
    private transient long dateUpdate;

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getString(cursor, VideoTable.COLUMN_ID, VideoTable.TABLE_NAME);

        this.title = DatabaseUtils.getString(cursor, VideoTable.COLUMN_TITLE, VideoTable.TABLE_NAME);

        this.date = DatabaseUtils.getLong(cursor, VideoTable.COLUMN_DATE, VideoTable.TABLE_NAME);

        this.pictureUrl = DatabaseUtils.getString(cursor, VideoTable.COLUMN_PICTURE_URL, VideoTable.TABLE_NAME);
        this.link = DatabaseUtils.getString(cursor, VideoTable.COLUMN_LINK, VideoTable.TABLE_NAME);

        this.type = WallItem.Type.values()[DatabaseUtils.getInt(cursor, VideoTable.COLUMN_TYPE, VideoTable.TABLE_NAME)];
    }

    @Override
    public ContentValues getContentValues()
    {
        this.postProcessJson();

        final ContentValues contentValues = new ContentValues();

        contentValues.put(VideoTable.COLUMN_ID, this.resourceId.videoId);
        contentValues.put(VideoTable.COLUMN_LINK, this.videoLink);
        contentValues.put(VideoTable.COLUMN_TITLE, this.videoTitle);
        contentValues.put(VideoTable.COLUMN_DATE, this.dateUpdate);

        contentValues.put(VideoTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(videoTitle));

        try
        {
            contentValues.put(VideoTable.COLUMN_PICTURE_URL, this.thumbnails.maxres.url);
        } catch (NullPointerException e)
        {
            contentValues.put(VideoTable.COLUMN_PICTURE_URL, "");
        }

        return contentValues;
    }

    public void postProcessJson()
    {
        this.videoLink = "https://www.youtube.com/watch?v=" + this.resourceId.videoId;
        this.dateUpdate = DateUtils.getYouTubeApiTimestamp(this.youtubeDateUpdate);
    }

    private static class ResourceId
    {
        private String videoId;
    }

    private static class Thumbnails
    {
        private Image maxres;

        private static class Image
        {
            private String url;
        }
    }
}
