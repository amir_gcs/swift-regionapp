package com.swift.SWIFTRegional.models.exhibitor;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.ExhibitorTable;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Inneke on 3/04/14.
 */
public class Exhibitor
{
    private long exhibitorId;

    private String ccvk;
    private long eventId;
    private String name;
    @SerializedName("websiteUrl")
    private String url;
    private String standNumber;
    private String description;
    private String productsOnShow;
    private String pictureUrl;

    private boolean isFavourite; // Extra field used to show favourite status in the list view

    public void setExhibitorId(long exhibitorId)
    {
        this.exhibitorId = exhibitorId;
    }

    public long getExhibitorId()
    {
        return exhibitorId;
    }

    public void setEventId(long eventId)
    {
        this.eventId = eventId;
    }

    public long getEventId()
    {
        return eventId;
    }

    public String getCcvk()
    {
        return ccvk;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public String getDescription()
    {
        return description;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public String getStandNumber()
    {
        return standNumber;
    }

    public String getProductsOnShow()
    {
        return productsOnShow;
    }

    public boolean isFavourite()
    {
        return isFavourite;
    }

    public void setIsFavourite(boolean isFavourite)
    {
        this.isFavourite = isFavourite;
    }

    public void constructExhibitor2FromCursor(Cursor cursor) {
        constructFromCursor(cursor, true);
    }

    public void constructFromCursor(Cursor cursor){
        constructFromCursor(cursor, false);
    }

    public void constructFromCursor(Cursor cursor, Boolean isSecondExhibitor)
    {
        String postfix = isSecondExhibitor? "2" : "";
        this.exhibitorId = DatabaseUtils.getInt(cursor, ExhibitorTable.COLUMN_ID + postfix);
        this.eventId=DatabaseUtils.getInt(cursor, ExhibitorTable.COLUMN_EVENTID + postfix);
        this.ccvk = DatabaseUtils.getString(cursor, ExhibitorTable.COLUMN_CCVK + postfix);
        this.name = DatabaseUtils.getString(cursor, ExhibitorTable.COLUMN_COMPANY_NAME + postfix);
        this.url = DatabaseUtils.getString(cursor, ExhibitorTable.COLUMN_WEBSITE_ADDRESS + postfix);
        this.standNumber = DatabaseUtils.getString(cursor, ExhibitorTable.COLUMN_STAND_NUMBER + postfix);
        this.description = DatabaseUtils.getString(cursor, ExhibitorTable.COLUMN_DESCRIPTION + postfix);
        this.productsOnShow = DatabaseUtils.getString(cursor, ExhibitorTable.COLUMN_PRODUCTS_ON_SHOW + postfix);
        this.pictureUrl = DatabaseUtils.getString(cursor, ExhibitorTable.COLUMN_COMPANY_LOGO_URL + postfix);

        // Check if the join with the favourites table resulted in a match
        if (cursor.getColumnIndex(FavoriteTable.COLUMN_FAVORITE_ID + postfix) >= 0)
        {
            String favouriteId = DatabaseUtils.getString(cursor, FavoriteTable.COLUMN_FAVORITE_ID + postfix);
            this.isFavourite = !TextUtils.isEmpty(favouriteId);
        }
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(ExhibitorTable.COLUMN_ID, this.exhibitorId);
        cv.put(ExhibitorTable.COLUMN_EVENTID,this.eventId);
        cv.put(ExhibitorTable.COLUMN_CCVK, this.ccvk);
        cv.put(ExhibitorTable.COLUMN_COMPANY_NAME, this.name);
        cv.put(ExhibitorTable.COLUMN_WEBSITE_ADDRESS, this.url);
        cv.put(ExhibitorTable.COLUMN_STAND_NUMBER, this.standNumber);
        cv.put(ExhibitorTable.COLUMN_DESCRIPTION, this.description);
        cv.put(ExhibitorTable.COLUMN_PRODUCTS_ON_SHOW, this.productsOnShow);
        cv.put(ExhibitorTable.COLUMN_COMPANY_LOGO_URL, this.pictureUrl);

        cv.put(ExhibitorTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(this.name+" "+this.standNumber));

        return cv;
    }

    @Override
    public String toString() {
        return "Exhibitor{" +
                "exhibitorId=" + exhibitorId +
                ", eventid='" + eventId + '\'' +
                ", ccvk='" + ccvk + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", standNumber='" + standNumber + '\'' +
                ", description='" + description + '\'' +
                ", productsOnShow='" + productsOnShow + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", isFavourite=" + isFavourite +
                '}';
    }

    public void setCcvk(String ccvk) {
        this.ccvk = ccvk;
    }
}
