package com.swift.SWIFTRegional.models;

import com.google.gson.annotations.SerializedName;

public class RefreshTokenResult
{
    @SerializedName(value = "tokenKey")
    private String accessToken;

    @SerializedName(value = "refreshToken")
    private String refreshToken;

    public String getAccessToken()
    {
        return accessToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }
}
