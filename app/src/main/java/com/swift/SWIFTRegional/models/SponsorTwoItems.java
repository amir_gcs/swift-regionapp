package com.swift.SWIFTRegional.models;

import android.database.Cursor;

import com.swift.SWIFTRegional.models.sponsor.Sponsor;

/**
 * Created by zesshan on 11/04/16.
 */
public class SponsorTwoItems {
    public Sponsor sponsor1;
    public Sponsor sponsor2;

    public void constructFromCursor(Cursor cursor) {
        sponsor1 = new Sponsor();
        sponsor1.constructFromCursor(cursor);
        sponsor2 = new Sponsor();
        sponsor2.constructSponsor2FromCursor(cursor);
    }
}
