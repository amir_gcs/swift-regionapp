package com.swift.SWIFTRegional.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amir.naushad on 12/13/2017.
 */

public class SocialInfo {

    @Expose
    @SerializedName("eventId")
    private long eventId;

    @Expose
    @SerializedName("twitter")
    private  String twitter;

    @Expose
    @SerializedName("youtube")
    private  String youtube;

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }
}
