package com.swift.SWIFTRegional.models.event;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.database.tables.MainEventTable;
import com.swift.SWIFTRegional.database.tables.SelectEventTable;
import com.swift.SWIFTRegional.utils.DateUtils;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Inneke on 3/04/14.
 */
public class Event
{
    private long eventId;
    private long regionId;
    private long typeId;
    private String title;
    private String description;
    private String pictureUrl;
    private String startDate;
    private String endDate;
    private String location;
    private long selected;
    private String regionName;
    private String typeName;

    public void setEventId(long eventId)
    {
        this.eventId = eventId;
    }

    public long getEventId()
    {
        return eventId;
    }

    public long getSelected()
    {
        return selected;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDescription()
    {
        return description;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public  String getStartDate(){return  startDate;}

    public String getEndDate(){return  endDate;}

    public  String getLocation(){return location;}


    public void constructEvent2FromCursor(Cursor cursor) {
        constructFromCursor(cursor, true);
    }

    public void constructFromCursor(Cursor cursor){
        constructFromCursor(cursor, false);
    }

    public void constructFromCursor(Cursor cursor, Boolean isSecondEvent)
    {
        String postfix = isSecondEvent? "2" : "";
        this.regionId = DatabaseUtils.getInt(cursor, MainEventTable.COLUMN_REGIONID + postfix);
        this.typeId = DatabaseUtils.getInt(cursor, MainEventTable.COLUMN_TYPEID + postfix);
        this.eventId=DatabaseUtils.getInt(cursor, MainEventTable.COLUMN_ID + postfix);
        this.title = DatabaseUtils.getString(cursor, MainEventTable.COLUMN_TITLE + postfix);
        this.startDate = DatabaseUtils.getString(cursor, MainEventTable.COLUMN_STARTDATE + postfix);
        this.endDate = DatabaseUtils.getString(cursor, MainEventTable.COLUMN_ENDDATE + postfix);
        this.description = DatabaseUtils.getString(cursor, MainEventTable.COLUMN_DESCRIPTION + postfix);
        this.location = DatabaseUtils.getString(cursor, MainEventTable.COLUMN_LOCATION + postfix);
        this.selected = DatabaseUtils.getInt(cursor, MainEventTable.COLUMN_SELECTED + postfix);
        this.pictureUrl = DatabaseUtils.getString(cursor, MainEventTable.COLUMN_PICTUREURL + postfix);

    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        MainActivity.selectedEventProvider.fillEventInfo(this);

        cv.put(MainEventTable.COLUMN_ID, this.eventId);
        cv.put(MainEventTable.COLUMN_TYPEID,this.typeId);
        cv.put(MainEventTable.COLUMN_REGIONID, this.regionId);
        cv.put(MainEventTable.COLUMN_TITLE, this.title);
        cv.put(MainEventTable.COLUMN_STARTDATE, this.startDate);
        cv.put(MainEventTable.COLUMN_DESCRIPTION, this.description);
        cv.put(MainEventTable.COLUMN_LOCATION, this.location);
        cv.put(MainEventTable.COLUMN_ENDDATE, this.endDate);
        cv.put(MainEventTable.COLUMN_SELECTED, this.selected);
        cv.put(MainEventTable.COLUMN_PICTUREURL, this.pictureUrl);
        cv.put(MainEventTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(this.title+" "+this.description+" "+this.regionName+" "+this.typeName+" "+this.getLocation() + " "
               + DateUtils.formattedDateFromString("","",this.startDate)));

        return cv;
    }

    public  ContentValues getSelectedEventContentValues()
    {
        final ContentValues cv = new ContentValues();
        cv.put(SelectEventTable.COLUMN_ID,this.eventId);
        cv.put(SelectEventTable.COLUMN_SELECTEDDATE,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

        return  cv;
    }

    @Override
    public String toString() {
        return "MainEvent{" +
                "eventid=" + eventId +
                ", typeid='" + typeId + '\'' +
                ", regionid='" + regionId + '\'' +
                ", title='" + title + '\'' +
                ", location='" + location + '\'' +
                ", selected='" + selected + '\'' +
                ", description='" + description + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", pictureURL='" + pictureUrl + '\'' +
                '}';
    }


    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getTypeName() {

        return (TextUtils.isEmpty(this.regionName) ? "" : this.regionName)+(TextUtils.isEmpty(this.typeName)?"":"/"+this.typeName);
    }

    public long getRegionId() {
        return regionId;
    }
    
    public long getTypeId(){
        return  typeId;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
