package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.TvClipTable;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Yves on 9/05/2014.
 */
public class TvClip
{
    private String tagline;
    @SerializedName("image")
    private String imageUrl;
    private String category;
    private String tags;
    @SerializedName("hi_res")
    private boolean hiRes;
    @SerializedName("youtube")
    private String youtubeId;
    private String title;

    private Long date;

    public TvClip()
    {

    }

    public TvClip(String tagline, String imageUrl, String category, String tags, boolean hiRes, String youtubeId, String title, long date)
    {
        this.setTagline(tagline);
        this.setImageUrl(imageUrl);
        this.setCategory(category);
        this.setTags(tags);
        this.setHiRes(hiRes);
        this.setYoutubeId(youtubeId);
        this.setTitle(title);
        this.setDate(date);
    }

    public String getTagline()
    {
        return tagline;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public String getCategory()
    {
        return category;
    }

    public String getTags()
    {
        return tags;
    }

    public boolean isHiRes()
    {
        return hiRes;
    }

    public String getYoutubeId()
    {
        return youtubeId;
    }

    public String getTitle()
    {
        return title;
    }

    public Long getDate()
    {
        return date;
    }

    public void setTagline(String tagline)
    {
        this.tagline = tagline;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public void setTags(String tags)
    {
        this.tags = tags;
    }

    public void setHiRes(boolean hiRes)
    {
        this.hiRes = hiRes;
    }

    public void setYoutubeId(String youtubeId)
    {
        this.youtubeId = youtubeId;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setDate(Long date)
    {
        this.date = date;
    }

    public ContentValues getContentValues()
    {
        ContentValues cv = new ContentValues();
        cv.put(TvClipTable.COLUMN_TAGLINE, this.getTagline());
        cv.put(TvClipTable.COLUMN_IMAGE_URL, this.getImageUrl());
        cv.put(TvClipTable.COLUMN_CATEGORY, this.getCategory());
        cv.put(TvClipTable.COLUMN_TAGS, this.getTags());
        cv.put(TvClipTable.COLUMN_HI_RES, this.isHiRes());
        cv.put(TvClipTable.COLUMN_YOUTUBE, this.getYoutubeId());
        cv.put(TvClipTable.COLUMN_TITLE, this.getTitle());
        cv.put(TvClipTable.COLUMN_DATE, this.getDate());

        return cv;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.setTagline(DatabaseUtils.getString(cursor, TvClipTable.COLUMN_TAGLINE));
        this.setImageUrl(DatabaseUtils.getString(cursor, TvClipTable.COLUMN_IMAGE_URL));
        this.setCategory(DatabaseUtils.getString(cursor, TvClipTable.COLUMN_CATEGORY));
        this.setTags(DatabaseUtils.getString(cursor, TvClipTable.COLUMN_TAGS));
        this.setHiRes(DatabaseUtils.getBoolean(cursor, TvClipTable.COLUMN_HI_RES));
        this.setYoutubeId(DatabaseUtils.getString(cursor, TvClipTable.COLUMN_YOUTUBE));
        this.setTitle(DatabaseUtils.getString(cursor, TvClipTable.COLUMN_TITLE));
        this.setDate(DatabaseUtils.getLong(cursor, TvClipTable.COLUMN_DATE));
    }




}
