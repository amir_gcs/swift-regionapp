package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.CategoryTable;

/**
 * Created by Yves on 2/04/2014.
 */
public class Category
{

    protected String id;
    protected String name;
    protected String type;
    private int count; // Value read from database - used to display number of sessions next to filter option

    public String getId()
    {
        return id;
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getCount()
    {
        return count;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.id = DatabaseUtils.getString(cursor, CategoryTable.COLUMN_CATEGORY_ID);
        this.name = DatabaseUtils.getString(cursor, CategoryTable.COLUMN_NAME);
        this.type = DatabaseUtils.getString(cursor, CategoryTable.COLUMN_TYPE);
        this.count = DatabaseUtils.getInt(cursor, CategoryTable.COLUMN_COUNT);
    }

    public void constructFromCategory(Category category)
    {
        this.id = category.getId();
        this.name = category.getName();
        this.type = category.getType();
        this.count = category.getCount();
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(CategoryTable.COLUMN_CATEGORY_ID, this.id);
        cv.put(CategoryTable.COLUMN_NAME, this.name);
        cv.put(CategoryTable.COLUMN_TYPE, this.type);

        return cv;
    }
}
