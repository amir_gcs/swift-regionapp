package com.swift.SWIFTRegional.models.session;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.SessionSpeakerTable;
import com.swift.SWIFTRegional.models.Speaker;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

public class SessionSpeaker
{
    @Expose
    private long sessionId;

    @Expose
    private String speakerId;

    @Expose
    @SerializedName("moderator")
    private boolean isModerator;

    @Expose
    private Speaker speaker;

    private String[] speakerTypes;

    private String speakerTypeName;

    public long getSessionId()
    {
        return sessionId;
    }

    public String getSpeakerId()
    {
        return speakerId;
    }

    public void setSessionId(long sessionId)
    {
        this.sessionId = sessionId;
    }

    public boolean isModerator()
    {
        return this.isModerator;
    }

    public void setIsModerator(boolean isModerator)
    {
        this.isModerator = isModerator;
    }

    public Speaker getSpeaker()
    {
        return speaker;
    }

    public void setSpeaker(Speaker speaker)
    {
        this.speaker = speaker;
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();
        cv.put(SessionSpeakerTable.COLUMN_SESSION_ID, this.sessionId);
        cv.put(SessionSpeakerTable.COLUMN_SPEAKER_ID, this.speakerId);
        cv.put(SessionSpeakerTable.COLUMN_SPEAKER_TYPE, this.getSpeakerTypes()!=null && this.getSpeakerTypes().length > 0 ? this.getSpeakerTypes()[0] : null);
        cv.put(SessionSpeakerTable.COLUMN_SPEAKER_TYPENAME, "");
        cv.put(SessionSpeakerTable.COLUMN_IS_MODERATOR, this.isModerator);

        return cv;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.sessionId = DatabaseUtils.getLong(cursor, SessionSpeakerTable.COLUMN_SESSION_ID, SessionSpeakerTable.TABLE_NAME);
        this.speakerId = DatabaseUtils.getString(cursor, SessionSpeakerTable.COLUMN_SPEAKER_ID, SessionSpeakerTable.TABLE_NAME);
        this.isModerator = DatabaseUtils.getBoolean(cursor, SessionSpeakerTable.COLUMN_IS_MODERATOR, SessionSpeakerTable.TABLE_NAME);

        String[] types = {DatabaseUtils.getString(cursor, SessionSpeakerTable.COLUMN_SPEAKER_TYPE, SessionSpeakerTable.TABLE_NAME)};
        this.setSpeakerTypes(types);
        speakerTypeName = DatabaseUtils.getString(cursor, SessionSpeakerTable.COLUMN_SPEAKER_TYPENAME, SessionSpeakerTable.TABLE_NAME);
        Speaker speaker = new Speaker();
        speaker.constructFromCursor(cursor);

        this.setSpeaker(speaker);
    }

    public String[] getSpeakerTypes() {
        return speakerTypes;
    }

    public void setSpeakerTypes(String[] speakerTypes) {
        this.speakerTypes = speakerTypes;
    }

    public String getSpeakerTypeName() {
        return speakerTypeName;
    }

    public void setSpeakerTypeName(String speakerTypeName) {
        this.speakerTypeName = speakerTypeName;
    }
}
