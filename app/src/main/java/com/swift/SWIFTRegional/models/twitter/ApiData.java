package com.swift.SWIFTRegional.models.twitter;

/**
 * Created by khuru on 07-Feb-2017.
 */

public class ApiData {

    private Tweet[] statuses;

    private SearchMeta search_metadata;

    public Tweet[] getStatuses() {
        return statuses;
    }

    public void setStatuses(Tweet[] statuses) {
        this.statuses = statuses;
    }

    public SearchMeta getSearch_metadata() {
        return search_metadata;
    }

    public void setSearch_metadata(SearchMeta search_metadata) {
        this.search_metadata = search_metadata;
    }
}
