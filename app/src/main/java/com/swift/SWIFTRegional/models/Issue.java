package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.IssuesTable;
import com.swift.SWIFTRegional.database.tables.NewsTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Created by Inneke on 8/05/2014.
 */
public class Issue extends WallItem
{
    protected String title;

    @SerializedName("desc")
    protected String description;

    @SerializedName("picture")
    protected String pictureUrl;

    private String link;

    @SerializedName("date")
    private String dateString;

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public String getDescription()
    {
        return description;
    }

    @Override
    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public String getLink()
    {
        return link;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.title = DatabaseUtils.getString(cursor, IssuesTable.COLUMN_TITLE, IssuesTable.TABLE_NAME);

        this.date = DatabaseUtils.getLong(cursor, IssuesTable.COLUMN_DATE, IssuesTable.TABLE_NAME);
        this.pictureUrl = DatabaseUtils.getString(cursor, IssuesTable.COLUMN_PICTURE_URL, IssuesTable.TABLE_NAME);
        this.description = DatabaseUtils.getString(cursor, IssuesTable.COLUMN_DESCRIPTION, IssuesTable.TABLE_NAME);

        this.type = Type.values()[DatabaseUtils.getInt(cursor, IssuesTable.COLUMN_TYPE, IssuesTable.TABLE_NAME)];

        this.link = DatabaseUtils.getString(cursor, IssuesTable.COLUMN_LINK_URL, IssuesTable.TABLE_NAME);
    }

    public ContentValues getContentValues()
    {
        this.postProcessJson();

        final ContentValues cv = new ContentValues();

        cv.put(IssuesTable.COLUMN_TITLE, this.title);
        cv.put(IssuesTable.COLUMN_DESCRIPTION, this.description);
        cv.put(IssuesTable.COLUMN_DATE, this.date);
        cv.put(IssuesTable.COLUMN_PICTURE_URL, this.pictureUrl);
        cv.put(IssuesTable.COLUMN_LINK_URL, this.link);

        cv.put(NewsTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(this.title + " " + this.description));

        return cv;
    }

    private void postProcessJson()
    {
        this.date = DateUtils.getSibosTimestamp(this.dateString);
    }
}
