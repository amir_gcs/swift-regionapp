package com.swift.SWIFTRegional.models;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import java.util.TreeMap;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.utils.ContactDetailsHelper;

public class ContactInformation extends TreeMap<ContactInformation.ContactInformationType, ContactInformation.ContactInformationItem>
{

    public enum ContactInformationType
    {
        EMAIL(R.drawable.speaker_detail_ic_mail, R.drawable.participant_detail_ic_mail, 0),
        MOBILE_PHONE(R.drawable.speaker_detail_ic_phone, R.drawable.participant_detail_ic_phone, 1),
        COMPANY_PHONE(R.drawable.speaker_detail_ic_phone, R.drawable.participant_detail_ic_phone, 2),
        ADDRESS(R.drawable.session_detail_locate, R.drawable.detail_speaker_icon_locate_green, 3);

        int imageResourceIdRed;
        int imageResourceIdGreen;

        ContactInformationType(int inImageResourceIdRed, int inImageResourceIdGreen, int inOrder)
        {
            this.imageResourceIdRed = inImageResourceIdRed;
            this.imageResourceIdGreen = inImageResourceIdGreen;
        }

        public int getImageResourceIdRed()
        {
            return imageResourceIdRed;
        }

        public int getImageResourceIdGreen()
        {
            return imageResourceIdGreen;
        }
    }

    public ContactInformationItem put(ContactInformationType inType, String inValue)
    {
        if (!(TextUtils.isEmpty(inValue)))
        {
            final ContactInformationItem item = new ContactInformationItem(inType, inValue);
            return this.put(inType, item);
        }

        return null;
    }

    public ContactInformationItem get(int inPosition)
    {
        return (ContactInformationItem) this.values().toArray()[inPosition];
    }

    public class ContactInformationItem
    {
        private ContactInformationType type;
        private String text;

        public ContactInformationItem(ContactInformationType inType, String inText)
        {
            this.type = inType;
            this.text = inText;
        }

        public ContactInformationType getType()
        {
            return type;
        }

        public String getText()
        {
            return text;
        }

        public View.OnClickListener getClickListener(final Context inContext)
        {
            switch (this.type)
            {
                case EMAIL:
                    return new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            ContactDetailsHelper.sendEmail(inContext, ContactInformationItem.this.text);
                        }
                    };

                case MOBILE_PHONE:
                case COMPANY_PHONE:
                    return new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            ContactDetailsHelper.callNumber(inContext, ContactInformationItem.this.text);
                        }
                    };

                case ADDRESS:
                    return new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            //ContactDetailsHelper.viewOnMap(inContext, ContactInformationItem.this.text);
                        }
                    };

                default:
                    return null;
            }
        }
    }

}