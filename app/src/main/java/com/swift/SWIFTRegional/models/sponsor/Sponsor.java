package com.swift.SWIFTRegional.models.sponsor;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.SponsorTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Inneke on 3/04/14.
 */
public class Sponsor
{
    private long sponsorId;
    private long eventId;
    private String name;
    @SerializedName("websiteUrl")
    private String url;
    private String description;
    private String productsOnShow;
    private String pictureUrl;


    public void setSponsorId(long sponsorId)
    {
        this.sponsorId = sponsorId;
    }

    public long getSponsorId()
    {
        return sponsorId;
    }

    public void setEventId(long eventId)
    {
        this.eventId = eventId;
    }

    public long getEventId()
    {
        return eventId;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        return url;
    }

    public String getDescription()
    {
        return description;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }



    public String getProductsOnShow()
    {
        return productsOnShow;
    }





    public void constructSponsor2FromCursor(Cursor cursor) {
        constructFromCursor(cursor, true);
    }

    public void constructFromCursor(Cursor cursor){
        constructFromCursor(cursor, false);
    }

    public void constructFromCursor(Cursor cursor, Boolean isSecondExhibitor)
    {
        String postfix = isSecondExhibitor? "2" : "";
        this.sponsorId = DatabaseUtils.getInt(cursor, SponsorTable.COLUMN_ID + postfix);
        this.eventId=DatabaseUtils.getInt(cursor, SponsorTable.COLUMN_EVENTID + postfix);
        this.name = DatabaseUtils.getString(cursor, SponsorTable.COLUMN_COMPANY_NAME + postfix);
        this.url = DatabaseUtils.getString(cursor, SponsorTable.COLUMN_WEBSITE_ADDRESS + postfix);
        this.description = DatabaseUtils.getString(cursor, SponsorTable.COLUMN_DESCRIPTION + postfix);
        this.productsOnShow = DatabaseUtils.getString(cursor, SponsorTable.COLUMN_PRODUCTS_ON_SHOW + postfix);
        this.pictureUrl = DatabaseUtils.getString(cursor, SponsorTable.COLUMN_COMPANY_LOGO_URL + postfix);

    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(SponsorTable.COLUMN_ID, this.sponsorId);
        cv.put(SponsorTable.COLUMN_EVENTID,this.eventId);
        cv.put(SponsorTable.COLUMN_COMPANY_NAME, this.name);
        cv.put(SponsorTable.COLUMN_WEBSITE_ADDRESS, this.url);
        cv.put(SponsorTable.COLUMN_DESCRIPTION, this.description);
        cv.put(SponsorTable.COLUMN_PRODUCTS_ON_SHOW, this.productsOnShow);
        cv.put(SponsorTable.COLUMN_COMPANY_LOGO_URL, this.pictureUrl);
        cv.put(SponsorTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(this.name+" "+this.description));

        return cv;
    }

    @Override
    public String toString() {
        return "Sponsor{" +
                "sponsorId=" + sponsorId +
                ", eventid='" + eventId + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", productsOnShow='" + productsOnShow + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                '}';
    }

}
