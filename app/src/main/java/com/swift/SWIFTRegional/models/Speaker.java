package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.database.tables.SpeakerTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

/**
 * Created by Yves on 1/04/2014.
 */
public class Speaker
{
    private String speakerId;
    private String sibosKey;
    private String salutation;
    private String title;
    private String firstName;
    private String lastName;
    private String email;

    @SerializedName("company")
    private String companyName;
    private String businessFunction;
    private String country;

    @SerializedName("summary")
    private String biography;
    private String pictureUrl; // No longer returned by the API, is now set by a join with the delegates table (delegates are protected, so only when signed in).

    private boolean isFavourite; // Extra field used to show favourite status in the list view

    public String getSpeakerId()
    {
        return speakerId;
    }

    public String getSalutation()
    {
        return salutation;
    }

    public String getTitle()
    {
        return title;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public String getBusinessFunction()
    {
        return businessFunction;
    }

    public String getCountry()
    {
        return country;
    }

    public String getBiography()
    {
        return biography;
    }

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public boolean isFavourite()
    {
        return isFavourite;
    }

    public void setIsFavourite(boolean isfavourite)
    {
        this.isFavourite = isfavourite;
    }

    public String getSibosKey()
    {
        return sibosKey;
    }

    public void constructSpeaker2FromCursor(Cursor cursor){
        constructFromCursor(cursor, true);
    }

    public void constructFromCursor(Cursor cursor){
        constructFromCursor(cursor, false);
    }

    public void constructFromCursor(Cursor cursor, Boolean isSecondSpeaker)
    {
        String postfix = isSecondSpeaker? "2" : "";
        this.speakerId = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_SPEAKER_ID + postfix);
        this.sibosKey = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_SIBOSKEY + postfix);
        this.salutation = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_SALUTATION + postfix);
        this.title = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_TITLE + postfix);
        this.firstName = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_FIRST_NAME + postfix);
        this.lastName = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_LAST_NAME + postfix);
        this.email = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_EMAIL + postfix);
        this.companyName = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_COMPANY_NAME + postfix);
        this.businessFunction = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_BUSINESS_FUNCTION + postfix);
        this.country = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_COUNTRY + postfix);
        this.biography = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_BIOGRAPHY + postfix);
        if(this.pictureUrl == null || this.pictureUrl.equalsIgnoreCase("")){
            this.pictureUrl = DatabaseUtils.getString(cursor, SpeakerTable.COLUMN_PICTURE_URL + postfix);
        }

        // Check if the join with the favourites table resulted in a match
        if (cursor.getColumnIndex(FavoriteTable.COLUMN_FAVORITE_ID + postfix) >= 0)
        {
            String favouriteId = DatabaseUtils.getString(cursor, FavoriteTable.COLUMN_FAVORITE_ID + postfix, SpeakerTable.TABLE_NAME);
            this.isFavourite = !TextUtils.isEmpty(favouriteId);
            if(!isFavourite()) {
                favouriteId = DatabaseUtils.getString(cursor, FavoriteTable.COLUMN_FAVORITE_ID + postfix);
                this.isFavourite = !TextUtils.isEmpty(favouriteId);
            }
        }
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();
        cv.put(SpeakerTable.COLUMN_SPEAKER_ID, this.speakerId);
        cv.put(SpeakerTable.COLUMN_SIBOSKEY, this.sibosKey);
        cv.put(SpeakerTable.COLUMN_TITLE, SibosUtils.getNullSafeString(this.title));
        cv.put(SpeakerTable.COLUMN_SALUTATION, SibosUtils.getNullSafeString(this.salutation));
        cv.put(SpeakerTable.COLUMN_FIRST_NAME, SibosUtils.getNullSafeString(this.firstName));
        cv.put(SpeakerTable.COLUMN_LAST_NAME, SibosUtils.getNullSafeString(this.lastName));
        cv.put(SpeakerTable.COLUMN_EMAIL, SibosUtils.getNullSafeString(this.email));
        cv.put(SpeakerTable.COLUMN_COMPANY_NAME, SibosUtils.getNullSafeString(this.companyName));
        cv.put(SpeakerTable.COLUMN_BUSINESS_FUNCTION, SibosUtils.getNullSafeString(this.businessFunction));
        cv.put(SpeakerTable.COLUMN_COUNTRY, SibosUtils.getNullSafeString(this.country));
        cv.put(SpeakerTable.COLUMN_BIOGRAPHY, SibosUtils.getNullSafeString(this.biography));
        cv.put(SpeakerTable.COLUMN_PICTURE_URL, SibosUtils.getNullSafeString(this.pictureUrl)); // Will be empty

        cv.put(SpeakerTable.COLUMN_SEARCH_NORMALIZED, SibosUtils.getNormalizedText(SibosUtils.getNullSafeString(this.lastName) + " " + SibosUtils.getNullSafeString(this.firstName) + " " + SibosUtils.getNullSafeString(this.companyName) + " "+ SibosUtils.getNullSafeString(this.title) + " " + SibosUtils.getNullSafeString(this.businessFunction)));

        return cv;
    }

    @Override
    public String toString() {
        return "Speaker{" +
                "speakerId='" + speakerId + '\'' +
                ", sibosKey='" + sibosKey + '\'' +
                ", salutation='" + salutation + '\'' +
                ", title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", companyName='" + companyName + '\'' +
                ", businessFunction='" + businessFunction + '\'' +
                ", country='" + country + '\'' +
                ", biography='" + biography + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", isFavourite=" + isFavourite +
                '}';
    }
}
