package com.swift.SWIFTRegional.models.twitter;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Wesley on 30/05/14.
 */
public class TwitterMedia
{
    @SerializedName("media_url")
    private String mediaUrl;

    public String getMediaUrl()
    {
        return mediaUrl;
    }
}
