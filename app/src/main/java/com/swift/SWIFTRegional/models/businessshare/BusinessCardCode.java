package com.swift.SWIFTRegional.models.businessshare;

/**
 * Created by SimonRaes on 4/05/15.
 */
public class BusinessCardCode
{
    private String shareBusinessCardCode;

    public BusinessCardCode()
    {
    }

    public String getShareBusinessCardCode()
    {
        return shareBusinessCardCode;
    }
}
