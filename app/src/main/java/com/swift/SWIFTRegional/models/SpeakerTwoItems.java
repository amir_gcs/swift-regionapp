package com.swift.SWIFTRegional.models;

import android.database.Cursor;

/**
 * Created by zesshan on 11/04/16.
 */
public class SpeakerTwoItems {
    public Speaker speaker1;
    public Speaker speaker2;
    public void constructFromCursor(Cursor cursor) {
        speaker1 = new Speaker();
        speaker1.constructFromCursor(cursor);
        speaker2 = new Speaker();
        speaker2.constructSpeaker2FromCursor(cursor);
    }
}
