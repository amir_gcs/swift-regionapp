package com.swift.SWIFTRegional.models.businessshare;

/**
 * Created by SimonRaes on 6/05/15.
 */
public class BusinessCardStatus
{
    private String shareBusinessCardStatus;

    public BusinessCardStatus()
    {
    }

    public String getShareBusinessCardStatus()
    {
        return shareBusinessCardStatus;
    }
}
