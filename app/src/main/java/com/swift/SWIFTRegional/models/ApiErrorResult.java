package com.swift.SWIFTRegional.models;

/**
 * Created by SimonRaes on 26/03/15.
 * Holds the data that is returned by the API when an error occurs (like invalid/expired token)
 */
public class ApiErrorResult
{
    int code;
    String detail;
    String correlationId;

    public ApiErrorResult(){

    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }

    public String getCorrelationId()
    {
        return correlationId;
    }

    public void setCorrelationId(String correlationId)
    {
        this.correlationId = correlationId;
    }
}
