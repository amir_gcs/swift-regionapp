package com.swift.SWIFTRegional.models.event;

import android.content.ContentValues;
import android.database.Cursor;

import com.swift.SWIFTRegional.database.tables.RegionInfoTable;
import com.swift.SWIFTRegional.database.tables.TypeInfoTable;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Inneke on 3/04/14.
 */
public class TypeInfo
{
    private long typeId;
    private String name;
    private String id;
    private long eventCount;
    private String backgroundImgURL;

    public String getId()
    {
        return this.id;
    }

    public void setTypeId(long typeId)
    {
        this.typeId = typeId;
    }

    public long getTypeId()
    {
        return typeId;
    }

    public String getName()
    {
        if(name.equals("AAShow All"))
        {
            return "Show All";
        }
        return name;
    }




    public void constructEvent2FromCursor(Cursor cursor) {
        constructFromCursor(cursor, true);
    }

    public void constructFromCursor(Cursor cursor){
        constructFromCursor(cursor, false);
    }

    public void constructFromCursor(Cursor cursor, Boolean isSecondEvent)
    {
        String postfix = isSecondEvent? "2" : "";
        this.typeId = DatabaseUtils.getInt(cursor, TypeInfoTable.COLUMN_ID + postfix);
        this.name = DatabaseUtils.getString(cursor, TypeInfoTable.COLUMN_NAME + postfix);
        this.backgroundImgURL=DatabaseUtils.getString(cursor, TypeInfoTable.COLUMN_PICTUREURL + postfix);
        this.eventCount = DatabaseUtils.getInt(cursor, RegionInfoTable.COLUMN_EVENTCOUNT + postfix);
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();
        cv.put(TypeInfoTable.COLUMN_ID, this.typeId);
        cv.put(TypeInfoTable.COLUMN_NAME, this.name);
        cv.put(TypeInfoTable.COLUMN_PICTUREURL,this.backgroundImgURL);

        return cv;
    }

    @Override
    public String toString() {
        return "TypeInfo{" +
                "typeid=" + typeId +
                ", name='" + name + '\'' +
                ", backgroundImgURL='" + backgroundImgURL + '\'' +
                '}';
    }


    public String getBackgroundImgURL() {
        return backgroundImgURL;
    }

    public long getEventCount() {
        return eventCount;
    }

    public void setEventCount(long eventCount) {
        this.eventCount = eventCount;
    }
}
