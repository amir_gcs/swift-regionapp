package com.swift.SWIFTRegional.models.flickr;

import android.content.ContentValues;
import android.database.Cursor;

import com.swift.SWIFTRegional.database.tables.FlickrPhotosTable;

import com.goodcoresoftware.android.common.utils.database.AbstractDatabaseObject;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Wesley on 20/05/14.
 */
public class FlickrPhoto extends AbstractDatabaseObject
{
    private String id;

    private String secret;
    private String server;
    private String farm;

    private transient String pictureUrlLarge;
    private transient String pictureUrlSmall;

    public String getPictureUrlLarge()
    {
        return pictureUrlLarge;
    }

    public String getPictureUrlSmall()
    {
        return pictureUrlSmall;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        this.pictureUrlLarge = DatabaseUtils.getString(cursor, FlickrPhotosTable.COLUMN_PICTURE_URL_LARGE, FlickrPhotosTable.TABLE_NAME);
        this.pictureUrlSmall = DatabaseUtils.getString(cursor, FlickrPhotosTable.COLUMN_PICTURE_URL_SMALL, FlickrPhotosTable.TABLE_NAME);
    }

    @Override
    public ContentValues getContentValues()
    {
        this.postProcessJson();

        final ContentValues cv = new ContentValues();

        cv.put(FlickrPhotosTable.COLUMN_PICTURE_URL_LARGE, this.pictureUrlLarge);
        cv.put(FlickrPhotosTable.COLUMN_PICTURE_URL_SMALL, this.pictureUrlSmall);

        return cv;
    }

    public void postProcessJson()
    {
        //TODO get the size based on the size of the screen
        this.pictureUrlLarge = String.format("https://farm%1$s.static.flickr.com/%2$s/%3$s_%4$s_%5$s.jpg", this.farm, this.server, this.id, this.secret, "b");
        this.pictureUrlSmall = String.format("https://farm%1$s.static.flickr.com/%2$s/%3$s_%4$s_%5$s.jpg", this.farm, this.server, this.id, this.secret, "b");
    }
}
