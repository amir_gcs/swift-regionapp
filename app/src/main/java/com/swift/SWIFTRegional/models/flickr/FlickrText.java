package com.swift.SWIFTRegional.models.flickr;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Wesley on 19/05/14.
 */
public class FlickrText
{
    @SerializedName("_content")
    private String text;

    public String getText()
    {
        return text;
    }
}
