package com.swift.SWIFTRegional.models.favourites;

import android.content.ContentValues;
import android.database.Cursor;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.database.tables.PendingFavouriteTable;

/**
 * Created by SimonRaes on 5/04/15.
 * Adds the extra unFlag field so the action (either add or remove) can be stored in the database.
 */
public class PendingFavourite extends Favourite
{
    // true = remove from favourites - false = add to favourites
    private boolean unFlag;

    public PendingFavourite()
    {
    }

    public PendingFavourite(FavoriteTable.FavouriteType type, String id, boolean unFlag)
    {
        this.setType(type);
        this.id = id;
        this.unFlag = unFlag;
    }

    public boolean isUnFlag()
    {
        return unFlag;
    }

    public void setUnFlag(boolean unFlag)
    {
        this.unFlag = unFlag;
    }

    @Override
    public ContentValues getContentValues()
    {
        ContentValues cvs =  super.getContentValues();

        cvs.put(PendingFavouriteTable.COLUMN_UNFLAG, this.unFlag);

        return cvs;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        super.constructFromCursor(cursor);
        this.unFlag = DatabaseUtils.getBoolean(cursor, PendingFavouriteTable.COLUMN_UNFLAG);
    }
}
