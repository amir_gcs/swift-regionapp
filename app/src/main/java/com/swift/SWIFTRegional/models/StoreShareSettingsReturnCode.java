package com.swift.SWIFTRegional.models;

import com.google.gson.annotations.SerializedName;

public class StoreShareSettingsReturnCode
{

    public final static int FAILURE = -1;

    @SerializedName("sibosKey")
    private String sibosKey;
    @SerializedName("returnCode")
    private int returnCode;

    public String getSibosKey()
    {
        return sibosKey;
    }

    public void setSibosKey(String sibosKey)
    {
        this.sibosKey = sibosKey;
    }

    public int getReturnCode()
    {
        return returnCode;
    }

    public void setReturnCode(int returnCode)
    {
        this.returnCode = returnCode;
    }
}
