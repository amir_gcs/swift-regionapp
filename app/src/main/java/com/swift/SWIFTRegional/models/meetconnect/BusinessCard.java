package com.swift.SWIFTRegional.models.meetconnect;

import android.content.ContentValues;
import android.os.Bundle;
import android.text.TextUtils;

import com.swift.SWIFTRegional.utils.AddressFormatter;

public class BusinessCard
{

    private final static String EMAIL = "email";
    private final static String COMPANY_PHONE = "companyPhone";
    private final static String MOBILE_PHONE = "mobilePhone";
    private final static String ADDRESS_1 = "address1";
    private final static String ADDRESS_2 = "address2";
    private final static String ZIP = "zip";
    private final static String CITY = "city";
    private final static String STATE = "state";
    private final static String COUNTRY = "country";

    private String email;
    private String phone;
    private String mobile;
    private String address1;
    private String address2;
    private String zip;
    private String city;
    private String state;
    private String country;
    private transient String formattedAddress;

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getFormattedAddress()
    {
        if (TextUtils.isEmpty(this.formattedAddress))
        {
            this.formattedAddress = AddressFormatter.format(this);
        }
        return this.formattedAddress;
    }

    public static BusinessCard getFromBundle(Bundle inBundle)
    {

        BusinessCard businessCard = new BusinessCard();

        businessCard.mobile = inBundle.getString(MOBILE_PHONE, null);
        businessCard.phone = inBundle.getString(COMPANY_PHONE, null);
        businessCard.email = inBundle.getString(EMAIL, null);
        businessCard.address1 = inBundle.getString(ADDRESS_1, null);
        businessCard.address2 = inBundle.getString(ADDRESS_2, null);
        businessCard.zip = inBundle.getString(ZIP, null);
        businessCard.city = inBundle.getString(CITY, null);
        businessCard.state = inBundle.getString(STATE, null);
        businessCard.country = inBundle.getString(COUNTRY, null);
        businessCard.formattedAddress = AddressFormatter.format(businessCard);

        return businessCard;
    }

    public void putInBundle(Bundle inBundle)
    {

        if (!TextUtils.isEmpty(this.email))
        {
            inBundle.putString(EMAIL, this.email);
        }

        if (!TextUtils.isEmpty(this.phone))
        {
            inBundle.putString(COMPANY_PHONE, this.phone);
        }

        if (!TextUtils.isEmpty(this.mobile))
        {
            inBundle.putString(MOBILE_PHONE, this.mobile);
        }

        if (!TextUtils.isEmpty(this.address1))
        {
            inBundle.putString(ADDRESS_1, this.address1);
        }

        if (!TextUtils.isEmpty(this.address2))
        {
            inBundle.putString(ADDRESS_2, this.address2);
        }

        if (!TextUtils.isEmpty(this.city))
        {
            inBundle.putString(CITY, this.city);
        }

        if (!TextUtils.isEmpty(this.state))
        {
            inBundle.putString(STATE, this.state);
        }

        if (!TextUtils.isEmpty(this.zip))
        {
            inBundle.putString(ZIP, this.zip);
        }

        if (!TextUtils.isEmpty(this.country))
        {
            inBundle.putString(COUNTRY, this.country);
        }
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();
        return cv;
    }

    public boolean isNotEmpty()
    {
        return (!TextUtils.isEmpty(this.email) ||
                !TextUtils.isEmpty(this.phone) ||
                !TextUtils.isEmpty(this.mobile) ||
                !TextUtils.isEmpty(this.address1) ||
                !TextUtils.isEmpty(this.address2) ||
                !TextUtils.isEmpty(this.city) ||
                !TextUtils.isEmpty(this.state) ||
                !TextUtils.isEmpty(this.zip) ||
                !TextUtils.isEmpty(this.country));
    }
}
