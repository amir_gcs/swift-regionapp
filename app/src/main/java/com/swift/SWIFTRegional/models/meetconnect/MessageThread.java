package com.swift.SWIFTRegional.models.meetconnect;


public class MessageThread
{
    // the "opponent" is the person you are chatting with
    private String dialogId;
    private int opponentId;

    private transient int unreadCount;
    private transient long lastMessageDate;

    private transient String opponentName;
    private transient String opponentFunction;
    private transient String opponentCompany;
    private transient String opponentPictureUrl;

    private boolean isFavouriteDelegate;
    private String sibosKey;

    public MessageThread()
    {
    }

    public String getDialogId()
    {
        return dialogId;
    }

    /**
     * Returns the Quickblox Id of the opponent of this conversation.
     */
    public int getOpponentId()
    {
        return opponentId;
    }

    public long getLastMessageDate()
    {
        return lastMessageDate;
    }

    public String getOpponentName()

    {
        return opponentName;
    }

    public String getOpponentFunction()
    {
        return opponentFunction;
    }

    public String getOpponentCompany()
    {
        return opponentCompany;
    }

    public String getOpponentPictureUrl()
    {
        return opponentPictureUrl;
    }

    public int getUnreadCount()
    {
        return unreadCount;
    }

    public void setUnreadCount(int inUnreadCount)
    {
        this.unreadCount = inUnreadCount;
    }

    public boolean isFavouriteDelegate()
    {
        return isFavouriteDelegate;
    }

    public void setDialogId(String dialogId)
    {
        this.dialogId = dialogId;
    }

    public void setOpponentId(int opponentId)
    {
        this.opponentId = opponentId;
    }

    public void setLastMessageDate(long lastMessageDate)
    {
        this.lastMessageDate = lastMessageDate;
    }

    public void setOpponentName(String opponentName)
    {
        this.opponentName = opponentName;
    }

    public void setOpponentFunction(String opponentFunction)
    {
        this.opponentFunction = opponentFunction;
    }

    public void setOpponentCompany(String opponentCompany)
    {
        this.opponentCompany = opponentCompany;
    }

    public void setOpponentPictureUrl(String opponentPictureUrl)
    {
        this.opponentPictureUrl = opponentPictureUrl;
    }

    public void setIsFavouriteDelegate(boolean isFavouriteDelegate)
    {
        this.isFavouriteDelegate = isFavouriteDelegate;
    }

    public String getSibosKey() {
        return sibosKey;
    }

    public void setSibosKey(String sibosKey) {
        this.sibosKey = sibosKey;
    }
}
