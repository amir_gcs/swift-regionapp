package com.swift.SWIFTRegional.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.AdvertisementTable;

import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Inneke on 9/05/2014.
 */
public class Advertisement
{
    @SerializedName("picture_high")
    private String pictureUrlBig;

    @SerializedName("picture")
    private String pictureUrlSmall;

    private String link;

    public String getPictureUrlBig()
    {
        return pictureUrlBig;
    }

    public String getPictureUrlSmal()
    {
        return pictureUrlSmall;
    }

    public String getLink()
    {
        return link;
    }

    public void constructFromCursor(Cursor cursor)
    {
        this.pictureUrlBig = DatabaseUtils.getString(cursor, AdvertisementTable.COLUMN_PICTURE_URL_BIG, AdvertisementTable.TABLE_NAME);
        this.pictureUrlSmall = DatabaseUtils.getString(cursor, AdvertisementTable.COLUMN_PICTURE_URL_SMALL, AdvertisementTable.TABLE_NAME);
        this.link = DatabaseUtils.getString(cursor, AdvertisementTable.COLUMN_LINK, AdvertisementTable.TABLE_NAME);
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(AdvertisementTable.COLUMN_PICTURE_URL_BIG, this.pictureUrlBig);
        cv.put(AdvertisementTable.COLUMN_PICTURE_URL_SMALL, this.pictureUrlSmall);
        cv.put(AdvertisementTable.COLUMN_LINK, this.link);

        return cv;
    }
}
