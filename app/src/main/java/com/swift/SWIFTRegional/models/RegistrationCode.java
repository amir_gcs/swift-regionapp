package com.swift.SWIFTRegional.models;

/**
 * Created by tomitp on 23/06/14.
 */
public class RegistrationCode
{

    private String url = "";
    private String returnCode = "";

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getReturnCode()
    {
        return returnCode;
    }

    public void setReturnCode(String returnCode)
    {
        this.returnCode = returnCode;
    }
}
