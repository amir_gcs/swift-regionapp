package com.swift.SWIFTRegional.models.event;

import android.content.ContentValues;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.PracticalInfoTable;

/**
 * Created by khuru on 27-Jan-2017.
 * Modified by Amir Naushad.
 */

public class PracticalInfo {

    @Expose
    @SerializedName("eventId")
    private long eventId;

    @Expose
    @SerializedName("practicalInfoUrl")
    private  String practicalInfoUrl;


    public String getPracticalInfoUrl() {
        if(practicalInfoUrl.contains("http") && !practicalInfoUrl.contains("https"))
            practicalInfoUrl = practicalInfoUrl.replace("http","https");
        return practicalInfoUrl;
    }

    public void setPracticalInfoUrl(String practicalInfoUrl) {
        this.practicalInfoUrl = practicalInfoUrl;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public ContentValues getContentValues()
    {
        final ContentValues cv = new ContentValues();

        cv.put(PracticalInfoTable.COLUMN_EVENT_ID, this.getEventId());
        cv.put(PracticalInfoTable.COLUMN_URL, this.getPracticalInfoUrl());

        return cv;
    }
}
