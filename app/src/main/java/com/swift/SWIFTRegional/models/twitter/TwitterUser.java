package com.swift.SWIFTRegional.models.twitter;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;
import com.swift.SWIFTRegional.database.tables.TweetsTable;

import com.goodcoresoftware.android.common.utils.database.AbstractDatabaseObject;
import com.goodcoresoftware.android.common.utils.database.DatabaseUtils;

/**
 * Created by Wesley on 16/05/14.
 */
public class TwitterUser extends AbstractDatabaseObject
{
    @SerializedName("screen_name")
    private String screenName;

    @SerializedName("profile_image_url")
    private String profilePictureUrl;

    public String getScreenName()
    {
        return screenName;
    }

    public String getProfilePictureUrl()
    {
        return profilePictureUrl;
    }

    @Override
    public void constructFromCursor(Cursor cursor)
    {
        this.screenName = DatabaseUtils.getString(cursor, TweetsTable.COLUMN_TITLE, TweetsTable.TABLE_NAME);
        this.profilePictureUrl = DatabaseUtils.getString(cursor, TweetsTable.COLUMN_PICTURE_URL, TweetsTable.TABLE_NAME);
    }

    @Override
    public ContentValues getContentValues()
    {
        return null;
    }
}
