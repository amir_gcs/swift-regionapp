package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.models.event.Event;
import com.swift.SWIFTRegional.utils.DateUtils;

import com.goodcoresoftware.android.common.views.TextView;

/**
 * Created by SimonRaes on 20/04/15.
 */
public class EventViewHelper
{
    /**
     * Create, bind and return a new sponsor item for use in a list.
     */
    public static View createEventListView(View convertView, ViewGroup parent, Event object)
    {
        return createEventListView(convertView, parent, object, false);
    }

    /**
     * Create, bind and return a new sponsor item for use in a list.
     *
     * @param detailPaddingMode Set this to true to remove the padding from the textView in this listitem.
     */
    public static View createEventListView(View convertView, ViewGroup parent, Event object, boolean detailPaddingMode)
    {
        if (convertView == null || !(convertView.getTag() instanceof EventViewHolder))
        {
            convertView = newEventView(parent, parent.getContext());
        }
        bindEventView(convertView, object, detailPaddingMode);

        return convertView;
    }

    private static View newEventView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_event, parent, false);

        final EventViewHolder holder = new EventViewHolder();

        holder.textViewDashBetweenDates =  (android.widget.TextView) v.findViewById(R.id.dashBetweenDates);
        holder.textViewTitle = (TextView) v.findViewById(R.id.textView_eventTitle);
        holder.textViewStartDate = (TextView) v.findViewById(R.id.textView_eventStartDate);
        holder.textViewEndDate = (TextView) v.findViewById(R.id.textView_eventEndDate);
        holder.textViewCategory = (TextView) v.findViewById(R.id.textView_eventCategory);
        holder.textViewLocation = (TextView) v.findViewById(R.id.textView_eventLocation);
        v.setTag(holder);

        return v;
    }


    private static void bindEventView(View view, Event event, boolean detailPaddingMode)
    {
        if (event != null)
        {
            final EventViewHolder holder = (EventViewHolder) view.getTag();

            holder.textViewTitle.setText(event.getTitle());
            String formatedStartDate = DateUtils.formattedDateFromString("","",event.getStartDate());
            String formatedEndDate = DateUtils.formattedDateFromString("","",event.getEndDate());

            holder.textViewStartDate.setText(formatedStartDate);
            if(!formatedStartDate.equals(formatedEndDate))
            {
                holder.textViewEndDate.setText(formatedEndDate);
                holder.textViewDashBetweenDates.setVisibility(View.VISIBLE);
                holder.textViewEndDate.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.textViewEndDate.setVisibility(View.GONE);
                holder.textViewDashBetweenDates.setVisibility(View.GONE);
            }

            if(event.getLocation()==null || event.getLocation().equals(""))
            {
                holder.textViewLocation.setVisibility(View.GONE);
            }
            else
            {
                holder.textViewLocation.setText(event.getLocation());
            }


            //call the method to fill the information of the event
            MainActivity.selectedEventProvider.fillEventInfo(event);

            holder.textViewCategory.setText(event.getTypeName());




        }
    }


    private static class EventViewHolder
    {
        android.widget.TextView textViewDashBetweenDates;
        TextView textViewCategory;
        TextView textViewLocation;
        TextView textViewTitle;
        TextView textViewStartDate;
        TextView textViewEndDate;
    }
}
