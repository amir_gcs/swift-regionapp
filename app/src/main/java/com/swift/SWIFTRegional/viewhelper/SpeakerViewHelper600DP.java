package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.swift.SWIFTRegional.activities.speakers.SpeakerDetailsActivity;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.utils.PicassoUtils;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

/**
 * Created by zesshan on 11/04/16.
 */
public class SpeakerViewHelper600DP {

    public static ViewLoaderStrategyInterface viewLoader;

    public static View createSpeakerView(View convertView, ViewLoaderStrategyInterface vLoader, ViewGroup parent, Speaker object, Speaker object2)
    {
        if(viewLoader == null){
            viewLoader = vLoader;
        }

        if (convertView == null || !(convertView.getTag() instanceof SpeakerViewHolder))
        {
            convertView = newSpeakerView(parent, parent.getContext());
        }

        final SpeakerViewHolder holder = (SpeakerViewHolder) convertView.getTag();

        bindSpeakerView(convertView, holder.layoutSpeaker, object, holder.imageViewPicture, holder.imageViewFavourite, holder.textViewName,
                holder.textViewBusinessFunction, holder.textViewCompanyName, holder.textViewExtra);

        if(object2 == null || object2.getSpeakerId().equals("")) {
            holder.layoutSpeaker2.setVisibility(View.INVISIBLE);
        } else {
            bindSpeakerView(convertView, holder.layoutSpeaker2, object2, holder.imageViewPicture2, holder.imageViewFavourite2, holder.textViewName2,
                    holder.textViewBusinessFunction2, holder.textViewCompanyName2, holder.textViewExtra2);
            holder.layoutSpeaker2.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private static View newSpeakerView(ViewGroup parent, Context context)
    {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_person_tablet, parent, false);

        final SpeakerViewHolder holder = new SpeakerViewHolder();

        holder.layoutSpeaker = (RelativeLayout) v.findViewById(R.id.layout_person);
        holder.imageViewPicture = (ImageView) v.findViewById(R.id.imageview_person_profile_picture);
        holder.imageViewFavourite = (ImageView) v.findViewById(R.id.imageview_person_favourite);
        holder.textViewName = (TextView) v.findViewById(R.id.textview_person_name);
        holder.textViewBusinessFunction = (TextView) v.findViewById(R.id.textview_person_business_function);
        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textview_person_company_name);
        holder.textViewExtra = (TextView) v.findViewById(R.id.textview_person_extra);
        holder.viewSeparator = v.findViewById(R.id.separator);

        holder.layoutSpeaker2 = (RelativeLayout) v.findViewById(R.id.layout_person2);
        holder.imageViewPicture2 = (ImageView) v.findViewById(R.id.imageview_person_profile_picture2);
        holder.imageViewFavourite2 = (ImageView) v.findViewById(R.id.imageview_person_favourite2);
        holder.textViewName2 = (TextView) v.findViewById(R.id.textview_person_name2);
        holder.textViewBusinessFunction2 = (TextView) v.findViewById(R.id.textview_person_business_function2);
        holder.textViewCompanyName2 = (TextView) v.findViewById(R.id.textview_person_company_name2);
        holder.textViewExtra2 = (TextView) v.findViewById(R.id.textview_person_extra2);

        v.setTag(holder);

        return v;
    }

    private static void bindSpeakerView(View view, RelativeLayout layoutSpeaker, final Speaker speaker, ImageView imageViewPicture,
                            ImageView imageViewFavourite, TextView textViewName, TextView textViewBusinessFunction,
                            TextView textViewCompanyName, TextView textViewExtra)
    {


        textViewName.setText(speaker.getFirstName() + " " + speaker.getLastName());
        textViewBusinessFunction.setText(speaker.getTitle());
        textViewCompanyName.setText(speaker.getCompanyName());

        PicassoUtils.loadPersonImage(speaker.getPictureUrl(), imageViewPicture);

        imageViewFavourite.setVisibility(speaker.isFavourite() ? View.VISIBLE : View.GONE);
        textViewExtra.setVisibility(View.GONE);

        layoutSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String speakerName = speaker.getFirstName()+" "+speaker.getLastName();
                final Intent intent = SpeakerDetailsActivity.createIntent(view.getContext(), String.valueOf(speaker.getSpeakerId()),speakerName);
                viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.SPEAKERS);
            }
        });
    }

    private static class SpeakerViewHolder
    {
        RelativeLayout layoutSpeaker;
        ImageView imageViewPicture;
        ImageView imageViewFavourite;
        TextView textViewName;
        TextView textViewBusinessFunction;
        TextView textViewCompanyName;
        TextView textViewExtra;
        View viewSeparator;

        RelativeLayout layoutSpeaker2;
        ImageView imageViewPicture2;
        ImageView imageViewFavourite2;
        TextView textViewName2;
        TextView textViewBusinessFunction2;
        TextView textViewCompanyName2;
        TextView textViewExtra2;
        View viewSeparator2;
    }
}
