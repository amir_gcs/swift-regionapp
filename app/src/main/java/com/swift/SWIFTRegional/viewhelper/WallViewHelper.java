package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.swift.SWIFTRegional.models.WallItem;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Created by Zeeshan on 5/4/2016.
 */
public class WallViewHelper {

    public static int paddingSmall;
    public static int paddingMedium;

    public static View newTwitterView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.listitem_tweet, parent, false);

        final TweetViewHolder tweetHolder = new TweetViewHolder();

        tweetHolder.textViewUser = (TextView) view.findViewById(R.id.textView_user);
        tweetHolder.textViewTweet = (TextView) view.findViewById(R.id.textView_text);
        tweetHolder.imageViewPhoto = (ImageView) view.findViewById(R.id.imageView_photo);
        tweetHolder.textViewTweetDate = (TextView) view.findViewById(R.id.textView_date);

        view.setTag(tweetHolder);

        return view;
    }

    public static View newFlickrView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.listitem_flickr, parent, false);

        final FlickrViewHolder flickrHolder = new FlickrViewHolder();

        flickrHolder.imageViewPhoto = (ImageView) view.findViewById(R.id.imageView_photo);
        flickrHolder.textViewDescription = (TextView) view.findViewById(R.id.textView_description);
        flickrHolder.textViewDate = (TextView) view.findViewById(R.id.textView_date);

        view.setTag(flickrHolder);

        return view;
    }

    public static View newNewsView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.listitem_news, parent, false);

        final NewsViewHolder newsHolder = new NewsViewHolder();

        newsHolder.textViewTitle = (TextView) view.findViewById(R.id.textView_title);
        newsHolder.textViewDate = (TextView) view.findViewById(R.id.textView_date);
        newsHolder.textViewDescription = (TextView) view.findViewById(R.id.textView_slug);

        view.setTag(newsHolder);

        return view;
    }

    public static View newIssuesView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.listitem_issue, parent, false);

        final IssueViewHolder issueHolder = new IssueViewHolder();

        issueHolder.textViewTitle = (TextView) view.findViewById(R.id.textView_title);
        issueHolder.textViewDate = (TextView) view.findViewById(R.id.textView_date);
        issueHolder.textViewDescription = (TextView) view.findViewById(R.id.textView_description);

        view.setTag(issueHolder);

        return view;
    }

    public static View newInstagramView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.listitem_instagram, parent, false);

        final InstagramViewHolder instagramHolder = new InstagramViewHolder();

        instagramHolder.textViewDate = (TextView) view.findViewById(R.id.textView_date);
        instagramHolder.textViewDescription = (TextView) view.findViewById(R.id.textView_description);
        instagramHolder.imageViewPhoto = (ImageView) view.findViewById(R.id.imageView_photo);
        instagramHolder.imageViewPlay = (ImageView) view.findViewById(R.id.imageView_play);

        view.setTag(instagramHolder);

        return view;
    }

    public static View newVideoView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.listitem_video, parent, false);

        final VideoViewHolder videoHolder = new VideoViewHolder();

        videoHolder.textViewDate = (TextView) view.findViewById(R.id.textView_date);
        videoHolder.textViewTitle = (TextView) view.findViewById(R.id.textView_title);
        videoHolder.imageViewPhoto = (ImageView) view.findViewById(R.id.imageView_photo);
        videoHolder.imageViewPlay = (ImageView) view.findViewById(R.id.imageView_play);

        view.setTag(videoHolder);

        return view;
    }

    public static void bindTwitterView(View view, Context context, WallItem wallItem)
    {
        final TweetViewHolder holder = (TweetViewHolder) view.getTag();

        holder.textViewUser.setText("@" + wallItem.getTitle());
        holder.textViewTweet.setText(Html.fromHtml(wallItem.getDescription()));
        holder.textViewTweetDate.setText(DateUtils.getTimeDifference(context, wallItem.getDate()));

        holder.imageViewPhoto.setBackgroundColor(context.getResources().getColor(R.color.picture_placeholder));

        if (!TextUtils.isEmpty(wallItem.getPictureUrl()))
        {
            Picasso.with(context).load(wallItem.getPictureUrl()).into(holder.imageViewPhoto);
            holder.imageViewPhoto.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.imageViewPhoto.setVisibility(View.GONE);
            holder.imageViewPhoto.setImageDrawable(null);
        }

        holder.textViewTweet.setPadding(holder.textViewTweet.getPaddingLeft(), TextUtils.isEmpty(wallItem.getPictureUrl()) ? paddingSmall : paddingMedium, holder.textViewTweet.getPaddingRight(), holder.textViewTweet.getPaddingBottom());
    }

    public static void bindFlickrView(View view, Context context, WallItem wallItem)
    {
        final FlickrViewHolder holder = (FlickrViewHolder) view.getTag();

        holder.imageViewPhoto.setBackgroundColor(context.getResources().getColor(R.color.picture_placeholder));

        if (!TextUtils.isEmpty(wallItem.getPictureUrl()))
        {
            Picasso.with(context).load(wallItem.getPictureUrl()).into(holder.imageViewPhoto);
        }
        else
        {
            holder.imageViewPhoto.setImageDrawable(null);
        }

        holder.textViewDescription.setText(wallItem.getDescription());
        holder.textViewDate.setText(DateUtils.getTimeDifference(context, wallItem.getDate()));
    }

    public static void bindNewsView(View view, Context context, WallItem wallItem)
    {
        final NewsViewHolder holder = (NewsViewHolder) view.getTag();

        holder.textViewTitle.setText(wallItem.getTitle());
        holder.textViewDescription.setText(Html.fromHtml(wallItem.getDescription()));

        holder.textViewDescription.setVisibility(TextUtils.isEmpty(wallItem.getDescription()) ? View.GONE : View.VISIBLE);

        holder.textViewDate.setText(DateUtils.getTimeDifference(context, wallItem.getDate()));
    }

    public static void bindIssueView(View view, Context context, WallItem wallItem)
    {
        final IssueViewHolder holder = (IssueViewHolder) view.getTag();

        holder.textViewTitle.setText(wallItem.getTitle());
        holder.textViewTitle.setVisibility(TextUtils.isEmpty(wallItem.getTitle()) ? View.INVISIBLE : View.VISIBLE);

        holder.textViewDescription.setText(Html.fromHtml(wallItem.getDescription()));
        holder.textViewDate.setText(DateUtils.getTimeDifference(context, wallItem.getDate()));
    }

    public static void bindInstagramView(View view, Context context, WallItem wallItem)
    {
        final InstagramViewHolder holder = (InstagramViewHolder) view.getTag();

        holder.imageViewPhoto.setBackgroundColor(context.getResources().getColor(R.color.picture_placeholder));

        if (!TextUtils.isEmpty(wallItem.getPictureUrl()))
        {
            Picasso.with(context).load(wallItem.getPictureUrl()).into(holder.imageViewPhoto);
        }
        else
        {
            holder.imageViewPhoto.setImageDrawable(null);
        }

        holder.textViewDescription.setText(wallItem.getDescription());
        holder.textViewDate.setText(DateUtils.getTimeDifference(context, wallItem.getDate()));

        holder.imageViewPlay.setVisibility(wallItem.getType() == WallItem.Type.INSTAGRAM_VIDEO ? View.VISIBLE : View.GONE);
    }

    public static void bindVideoView(View view, Context context, WallItem wallItem)
    {
        final VideoViewHolder holder = (VideoViewHolder) view.getTag();

        holder.imageViewPhoto.setBackgroundColor(context.getResources().getColor(R.color.picture_placeholder));

        if (!TextUtils.isEmpty(wallItem.getPictureUrl()))
        {
            Picasso.with(context).load(wallItem.getPictureUrl()).into(holder.imageViewPhoto);
        }
        else
        {
            holder.imageViewPhoto.setImageDrawable(null);
        }

        holder.textViewTitle.setText(wallItem.getTitle());
        holder.textViewDate.setText(DateUtils.getTimeDifference(context, wallItem.getDate()));
    }

    public static class TweetViewHolder
    {
        public TextView textViewUser;
        public ImageView imageViewPhoto;
        public TextView textViewTweet;
        public TextView textViewTweetDate;
    }

    public static class FlickrViewHolder
    {
        public ImageView imageViewPhoto;
        public TextView textViewDescription;
        public TextView textViewDate;
    }

    public static class NewsViewHolder
    {
        public TextView textViewTitle;
        public TextView textViewDate;
        public TextView textViewDescription;
    }

    public static class IssueViewHolder
    {
        public TextView textViewTitle;
        public TextView textViewDate;
        public TextView textViewDescription;
    }

    public static class InstagramViewHolder
    {
        public TextView textViewDate;
        public TextView textViewDescription;
        public ImageView imageViewPhoto;
        public ImageView imageViewPlay;
    }

    public static class VideoViewHolder
    {
        public TextView textViewDate;
        public TextView textViewTitle;
        public ImageView imageViewPhoto;
        public ImageView imageViewPlay;
    }
}
