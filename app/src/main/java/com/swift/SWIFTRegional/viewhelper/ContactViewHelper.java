package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.swift.SWIFTRegional.models.ContactInformation;

import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 27/04/15.
 */
public class ContactViewHelper
{
    public enum ColorMode
    {
        RED,
        GREEN
    }

    private static int paddingLeft;

    public static View createContactView(View convertView, ViewGroup parent, ContactInformation.ContactInformationItem contactInformationItem, ColorMode colorMode, boolean isLastItem)
    {
        if (convertView == null || !(convertView.getTag() instanceof ContactViewHolder))
        {
            convertView = newContactView(parent, parent.getContext());
        }
        bindContactView(convertView, contactInformationItem, colorMode, isLastItem);

        return convertView;
    }

    private static View newContactView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.listitem_person_contact, parent, false);

        paddingLeft = context.getResources().getDimensionPixelSize(R.dimen.padding_medium_large);

        final ContactViewHolder holder = new ContactViewHolder();

        holder.layoutContent = view.findViewById(R.id.layout_content);
        holder.buttonContact = (Button) view.findViewById(R.id.button_speaker_contact);
        holder.textAddress = (TextView) view.findViewById(R.id.address_person_contact);
        holder.dividerPerson = view.findViewById(R.id.divider_person_contact);

        view.setTag(holder);

        return view;
    }


    private static void bindContactView(View convertView, ContactInformation.ContactInformationItem contactInformationItem, ColorMode colorMode, boolean isLastItem)
    {
        ContactViewHolder holder = (ContactViewHolder) convertView.getTag();

        if (!contactInformationItem.getType().equals(ContactInformation.ContactInformationType.ADDRESS)) {
            holder.textAddress.setVisibility(View.GONE);
            holder.buttonContact.setText(contactInformationItem.getText());

            final View.OnClickListener clickListener = contactInformationItem.getClickListener(convertView.getContext());
            if (clickListener != null) {
                holder.buttonContact.setOnClickListener(clickListener);
            }

                holder.buttonContact.setTextColor(
                        colorMode == ColorMode.RED
                                ? convertView.getContext().getResources().getColor(R.color.sibos_red)
                                : convertView.getContext().getResources().getColor(R.color.sibos_green));

            if (contactInformationItem.getType().getImageResourceIdRed() != -1) {
                holder.buttonContact.setCompoundDrawablesWithIntrinsicBounds(colorMode == ColorMode.RED
                        ? contactInformationItem.getType().getImageResourceIdRed()
                        : contactInformationItem.getType().getImageResourceIdGreen()
                        , 0, 0, 0);
                holder.buttonContact.setCompoundDrawablePadding((int) convertView.getContext().getResources().getDimension(R.dimen.padding_medium));
            }
        } else {
            holder.buttonContact.setVisibility(View.GONE);
            holder.textAddress.setText(contactInformationItem.getText());
            holder.textAddress.setTextColor(
                    colorMode == ColorMode.RED
                            ? convertView.getContext().getResources().getColor(R.color.sibos_red)
                            : convertView.getContext().getResources().getColor(R.color.gray));


            if (contactInformationItem.getType().getImageResourceIdRed() != -1) {
                holder.textAddress.setCompoundDrawablesWithIntrinsicBounds(colorMode == ColorMode.RED
                        ? contactInformationItem.getType().getImageResourceIdRed()
                        : contactInformationItem.getType().getImageResourceIdGreen()
                        , 0, 0, 0);
                holder.textAddress.setCompoundDrawablePadding((int) convertView.getContext().getResources().getDimension(R.dimen.padding_medium));
            }
        }

        holder.dividerPerson.setPadding(isLastItem ? 0 : paddingLeft, holder.dividerPerson.getPaddingTop(), holder.dividerPerson.getPaddingRight(), holder.dividerPerson.getPaddingBottom());
    }

    private static class ContactViewHolder
    {
        View layoutContent;
        Button buttonContact;
        TextView textAddress;
        View dividerPerson;
    }
}
