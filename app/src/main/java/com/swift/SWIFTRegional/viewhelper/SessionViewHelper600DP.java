package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.swift.SWIFTRegional.activities.sessions.SessionDetailsActivity;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.EventTiming;

import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.ExhibitorEvent;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.models.session.Stream;
import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Created by SimonRaes on 14/04/15.
 * Inflates and binds the cards used to display sessions.
 */
public class SessionViewHelper600DP
{
    private static final String TYPE_PRIVATE = "PRIVATE";
    private static ViewLoaderStrategyInterface viewLoader;

    public static View createSessionView(View convertView, ViewGroup parent, ViewLoaderStrategyInterface vLoader, EventTiming object, EventTiming object2, boolean enableDate, boolean showDate)
    {
        if(viewLoader == null){
            viewLoader = vLoader;
        }

        if (convertView == null || !(convertView.getTag() instanceof SessionViewHolder))
        {
            convertView = newSessionView(parent, parent.getContext());
        }
        bindSessionView(convertView, object, object2, enableDate , showDate);

        return convertView;
    }

    private static View newSessionView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_session_tablet, parent, false);

        final SessionViewHolder holder = new SessionViewHolder();

        holder.textViewTitle = (TextView) v.findViewById(R.id.textview_session_title);
        holder.textViewRoom = (TextView) v.findViewById(R.id.textview_session_room);
        holder.textViewDay = (TextView) v.findViewById(R.id.textview_sessionDay);
        holder.textViewMonth = (TextView) v.findViewById(R.id.textview_sessionMonth);
        holder.textViewTime = (TextView) v.findViewById(R.id.textview_session_time);
        holder.textViewStreams = (TextView) v.findViewById(R.id.textview_session_streams);
        holder.dateContainer = v.findViewById(R.id.layout_dateContainer);
        holder.layoutContent = v.findViewById(R.id.listitem_session_layout);
        holder.imageViewStatusOne = (ImageView) v.findViewById(R.id.imageview_status_one);
        holder.imageViewStatusTwo = (ImageView) v.findViewById(R.id.imageview_status_two);
        holder.eventContainer = v.findViewById(R.id.layout_event_container);


        holder.event2Container = v.findViewById(R.id.layout_event2_container);
        holder.textViewTitle2 = (TextView) v.findViewById(R.id.textview_session_title2);
        holder.textViewRoom2 = (TextView) v.findViewById(R.id.textview_session_room2);
        holder.textViewTime2 = (TextView) v.findViewById(R.id.textview_session_time2);
        holder.textViewStreams2 = (TextView) v.findViewById(R.id.textview_session_streams2);
        holder.imageViewStatusOne2 = (ImageView) v.findViewById(R.id.imageview_status_one2);
        holder.imageViewStatusTwo2 = (ImageView) v.findViewById(R.id.imageview_status_two2);

        v.setPadding(context.getResources().getDimensionPixelSize(R.dimen.padding_sides), v.getPaddingTop(), context.getResources().getDimensionPixelSize(R.dimen.padding_sides), v.getPaddingBottom());

        v.setTag(holder);

        return v;
    }

    private static void bindSessionView(View view, EventTiming eventTiming, EventTiming eventTiming2, boolean enableDate, boolean showDate)
    {
        final SessionViewHolder holder = (SessionViewHolder) view.getTag();

        if(enableDate) {
            holder.textViewDay.setText(DateUtils.extractDayFromIso8601String(eventTiming.getStartDate()));
            holder.textViewMonth.setText(DateUtils.extractShortMonthStringFromIso8601String(eventTiming.getStartDate()));
            holder.dateContainer.setVisibility(showDate ? View.VISIBLE : View.INVISIBLE);
        }

        bindEvent(view.getContext(), eventTiming, holder.textViewTitle, holder.textViewRoom, holder.textViewTime, holder.textViewStreams
                , holder.imageViewStatusOne, holder.imageViewStatusTwo, holder.eventContainer);

        if(eventTiming2 == null) {
            holder.event2Container.setVisibility(View.INVISIBLE);
        } else {
            holder.event2Container.setVisibility(View.VISIBLE);
            bindEvent(view.getContext(), eventTiming2, holder.textViewTitle2, holder.textViewRoom2, holder.textViewTime2, holder.textViewStreams2
                    , holder.imageViewStatusOne2, holder.imageViewStatusTwo2, holder.event2Container);
        }
    }

    private static void bindEvent(final Context context, final EventTiming eventTiming, TextView textViewTitle, TextView textViewRoom
            , TextView textViewTime, TextView textViewStreams, ImageView imageViewStatusOne, ImageView imageViewStatusTwo, View eventContainer) {

        textViewTitle.setText(eventTiming.getEvent().getTitle());
        textViewRoom.setText(eventTiming.getRoomName());

        String startTime = DateUtils.prettyTimeString(context, eventTiming.getStartDate());
        String endTime = DateUtils.prettyTimeString(context, eventTiming.getEndDate());

        textViewTime.setText(startTime + " - " + endTime);

        if (eventTiming.getEvent() instanceof Session)
        {
            Session session = (Session) eventTiming.getEvent();

            Stream [] sessionStreams = session.getStreams();

            if (sessionStreams != null && sessionStreams.length > 0)
            {
                String separator = ", ";
                StringBuilder displayStreamsStr = new StringBuilder();
                for (Stream sessionStream : sessionStreams) {
                    if (sessionStream.getName() != null && sessionStream.getName() != "") {
                        displayStreamsStr.append(sessionStream.getName());
                        displayStreamsStr.append(separator);
                    }
                }

                String streamsStr = "";
                if (displayStreamsStr.length() != 0) {
                    streamsStr = displayStreamsStr.toString().replaceAll(separator + "$", "");
                }

                textViewStreams.setText(streamsStr);
                textViewStreams.setVisibility(View.VISIBLE);

            }
            else
            {
                textViewStreams.setVisibility(View.GONE);
            }
        }
        else if (eventTiming.getEvent() instanceof ExhibitorEvent)
        {
            ExhibitorEvent exhibitorEvent = (ExhibitorEvent) eventTiming.getEvent();

            if (exhibitorEvent.getExhibitor() !=null && !StringUtils.isEmpty(exhibitorEvent.getExhibitor().getName()))
            {
                textViewStreams.setText(exhibitorEvent.getExhibitor().getName());
                textViewStreams.setVisibility(View.VISIBLE);
            }
            else
            {
                textViewStreams.setVisibility(View.GONE);
            }
        }
        else
        {
            textViewStreams.setVisibility(View.GONE);
        }

        boolean isPrivate = eventTiming.getEvent().getSessionType().equals(TYPE_PRIVATE);

        if (eventTiming.isFavourite())
        {
            imageViewStatusOne.setImageDrawable(ContextCompat.getDrawable(imageViewStatusOne.getContext(), R.drawable.ic_favorite));
            imageViewStatusOne.setVisibility(View.VISIBLE);

            if (isPrivate)
            {
                imageViewStatusTwo.setImageDrawable(ContextCompat.getDrawable(imageViewStatusOne.getContext(), R.drawable.ic_sessions_private));
                imageViewStatusTwo.setVisibility(View.VISIBLE);

            }
            else
            {
                imageViewStatusTwo.setVisibility(View.GONE);
            }
        }
        else
        {
            if (isPrivate)
            {
                imageViewStatusOne.setImageDrawable(ContextCompat.getDrawable(imageViewStatusOne.getContext(), R.drawable.ic_sessions_private));
                imageViewStatusOne.setVisibility(View.VISIBLE);
            }
            else
            {
                imageViewStatusOne.setVisibility(View.GONE);
            }
            imageViewStatusTwo.setVisibility(View.GONE);
        }

        eventContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = SessionDetailsActivity.createIntent(context, eventTiming.getEventId());
                viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.SESSIONS);
            }
        });
    }

    private static class SessionViewHolder
    {
        View layoutContent;
        View dateContainer;
        View eventContainer;
        View event2Container;

        TextView textViewTitle;
        TextView textViewRoom;
        TextView textViewDay;
        TextView textViewMonth;
        TextView textViewTime;
        TextView textViewStreams;

        ImageView imageViewStatusOne;
        ImageView imageViewStatusTwo;

        TextView textViewTitle2;
        TextView textViewRoom2;
        TextView textViewTime2;
        TextView textViewStreams2;

        ImageView imageViewStatusOne2;
        ImageView imageViewStatusTwo2;
    }
}
