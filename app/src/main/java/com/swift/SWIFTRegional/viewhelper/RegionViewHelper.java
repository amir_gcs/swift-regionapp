package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.event.RegionInfo;

import com.goodcoresoftware.android.common.views.TextView;

/**
 * Created by SimonRaes on 20/04/15.
 */
public class RegionViewHelper
{
    /**
     * Create, bind and return a new sponsor item for use in a list.
     */
    public static View createRegionInfoListView(View convertView, ViewGroup parent, RegionInfo object)
    {
        return createRegionInfoListView(convertView, parent, object, false);
    }

    /**
     * Create, bind and return a new sponsor item for use in a list.
     *
     * @param detailPaddingMode Set this to true to remove the padding from the textView in this listitem.
     */
    public static View createRegionInfoListView(View convertView, ViewGroup parent, RegionInfo object, boolean detailPaddingMode)
    {
        if (convertView == null || !(convertView.getTag() instanceof RegionInfoViewHolder))
        {
            convertView = newRegionView(parent, parent.getContext());
        }
        bindRegionView(convertView, object, detailPaddingMode);

        return convertView;
    }

    private static View newRegionView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);

        final View v = inflater.inflate(R.layout.listitem_regioninfo,parent, false);

        final RegionInfoViewHolder holder = new RegionInfoViewHolder();

        holder.textViewTitle = (TextView) v.findViewById(R.id.textView_regioninfoname);
        holder.textViewEventCount = (TextView) v.findViewById(R.id.textView_eventCount);

        v.setTag(holder);

        return v;
    }


    private static void bindRegionView(View view, RegionInfo regionInfo, boolean detailPaddingMode)
    {
        if (regionInfo != null)
        {
            final RegionInfoViewHolder holder = (RegionInfoViewHolder) view.getTag();
            String name=regionInfo.getName();

            holder.textViewTitle.setText(name);

            //applyBackgroundImage(holder.textViewTitle,regionInfo.getBackgroundImgURL());
            if(regionInfo.getName().equals("Show All"))
            {
                holder.textViewEventCount.setText("");
            }
            else
            {
                holder.textViewEventCount.setText(regionInfo.getEventCount()+" events");
            }

            holder.textViewTitle.setTextColor(Color.parseColor("#574E4C"));
        }
    }




    private static void applyBackgroundImage(final TextView textView, String imageUrl)
    {
        try
        {

            Target t=  new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                             /* Save the bitmap or do something with it here */
                    try
                    {
                        BitmapDrawable bitmapDrawable=new BitmapDrawable(textView.getResources(),bitmap);
                        //Set it in the ImageView
                        textView.setBackground(bitmapDrawable);

                    }
                    catch (Exception e)
                    {

                    }

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    if(placeHolderDrawable==null)
                    {

                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    if(errorDrawable==null)
                    {

                    }
                }
            };
            textView.setTag(t);

            Picasso.with(textView.getContext())
                    .load(imageUrl)
                    .into(t);

        }
        catch(Exception e)
        {
            Log.e("imageLoad",e.toString());
        }

    }



    private static class RegionInfoViewHolder
    {
        TextView textViewTitle;
        TextView textViewEventCount;
    }
}
