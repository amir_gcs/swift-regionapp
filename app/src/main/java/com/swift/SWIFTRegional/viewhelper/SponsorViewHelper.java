package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;

import com.goodcoresoftware.android.common.views.TextView;

/**
 * Created by SimonRaes on 20/04/15.
 */
public class SponsorViewHelper
{
    /**
     * Create, bind and return a new sponsor item for use in a list.
     */
    public static View createSponsorListView(View convertView, ViewGroup parent, Sponsor object)
    {
        return createSponsorListView(convertView, parent, object, false);
    }

    /**
     * Create, bind and return a new sponsor item for use in a list.
     *
     * @param detailPaddingMode Set this to true to remove the padding from the textView in this listitem.
     */
    public static View createSponsorListView(View convertView, ViewGroup parent, Sponsor object, boolean detailPaddingMode)
    {
        if (convertView == null || !(convertView.getTag() instanceof SponsorViewHolder))
        {
            convertView = newSponsorView(parent, parent.getContext());
        }
        bindSponsorView(convertView, object, detailPaddingMode);

        return convertView;
    }

    private static View newSponsorView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_sponsor, parent, false);

        final SponsorViewHolder holder = new SponsorViewHolder();

        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textView_sponsorCompanyName);
        holder.textViewStand = (TextView) v.findViewById(R.id.textView_sponsorStand);
        holder.imageViewFavourite = (ImageView) v.findViewById(R.id.imageView_sponsor_favourite);

        v.setTag(holder);

        return v;
    }


    private static void bindSponsorView(View view, Sponsor sponsor, boolean detailPaddingMode)
    {
        if (sponsor != null)
        {
            final SponsorViewHolder holder = (SponsorViewHolder) view.getTag();

            holder.textViewCompanyName.setText(sponsor.getName());
            holder.textViewStand.setText(sponsor.getPictureUrl());
            if (detailPaddingMode)
            {
                holder.textViewCompanyName.setPadding(
                        view.getContext().getResources().getDimensionPixelSize(R.dimen.padding_medium_large),
                        holder.textViewCompanyName.getPaddingTop(),
                        holder.textViewCompanyName.getPaddingRight(),
                        holder.textViewCompanyName.getPaddingBottom());

                holder.textViewStand.setPadding(
                        view.getContext().getResources().getDimensionPixelSize(R.dimen.padding_medium_large),
                        holder.textViewStand.getPaddingTop(),
                        holder.textViewStand.getPaddingRight(),
                        holder.textViewStand.getPaddingBottom());
            }

            holder.imageViewFavourite.setVisibility(View.GONE);
            holder.textViewStand.setVisibility(View.GONE);
        }
    }


    private static class SponsorViewHolder
    {
        TextView textViewCompanyName;
        TextView textViewStand;
        ImageView imageViewFavourite;
    }
}
