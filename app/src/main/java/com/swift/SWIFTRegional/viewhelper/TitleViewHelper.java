package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

public class TitleViewHelper
{
    public static View createTitleListView(View convertView, ViewGroup parent, String title)
    {
        if (convertView == null || !(convertView.getTag() instanceof TitleViewHolder))
        {
            convertView = newTitleView(parent, parent.getContext());
        }
        bindTitleView(convertView, title);

        return convertView;
    }

    private static View newTitleView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_title, parent, false);

        final TitleViewHolder holder = new TitleViewHolder();

        holder.textViewTitle = (TextView) v.findViewById(R.id.textView_title);

        v.setTag(holder);

        return v;
    }


    private static void bindTitleView(View view, String title)
    {
        final TitleViewHolder holder = (TitleViewHolder) view.getTag();

        holder.textViewTitle.setText(title);
    }

    private static class TitleViewHolder
    {
        TextView textViewTitle;
    }
}

