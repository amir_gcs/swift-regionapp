package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.activities.speakers.SpeakerDetailsActivity;
import com.swift.SWIFTRegional.activities.sponsors.SponsorDetailsActivity;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.models.sponsor.Sponsor;
import com.swift.SWIFTRegional.utils.PicassoUtils;

import com.goodcoresoftware.android.common.views.TextView;

/**
 * Created by zesshan on 11/04/16.
 */
public class SponsorViewHelper600DP {

    public static ViewLoaderStrategyInterface viewLoader;

    public static View createSpeakerView(View convertView, ViewLoaderStrategyInterface vLoader, ViewGroup parent, Sponsor object, Sponsor object2)
    {
        if(viewLoader == null){
            viewLoader = vLoader;
        }

        if (convertView == null || !(convertView.getTag() instanceof SponsorViewHolder))
        {
            convertView = newSpeakerView(parent, parent.getContext());
        }

        final SponsorViewHolder holder = (SponsorViewHolder) convertView.getTag();

        bindSponsorView(convertView, holder.layoutSponsor, object,  holder.textViewCompanyName, holder.textViewStand);

        if(object2 == null || object2.getSponsorId()==-1) {
            holder.layoutSponsor2.setVisibility(View.INVISIBLE);
        } else {
            bindSponsorView(convertView, holder.layoutSponsor2, object2,  holder.textViewCompanyName2, holder.textViewStand2);
            holder.layoutSponsor2.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private static View newSpeakerView(ViewGroup parent, Context context)
    {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_sponsor_tablet, parent, false);

        final SponsorViewHolder holder = new SponsorViewHolder();

        holder.layoutSponsor = (RelativeLayout) v.findViewById(R.id.layout_sponsor);
        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textView_sponsorCompanyName);
        holder.textViewStand = (TextView) v.findViewById(R.id.textView_sponsorStand);


        holder.layoutSponsor2 = (RelativeLayout) v.findViewById(R.id.layout_sponsor2);
        holder.textViewCompanyName2 = (TextView) v.findViewById(R.id.textView_sponsorCompanyName2);
        holder.textViewStand2 = (TextView) v.findViewById(R.id.textView_sponsorStand2);

        v.setTag(holder);

        return v;
    }

    private static void bindSponsorView(View view, RelativeLayout layoutSponsor, final Sponsor sponsor,
                            TextView textViewCompanyName, TextView textViewStand)
    {

        if(sponsor !=null)
        {
            textViewCompanyName.setText(sponsor.getName());
            textViewStand.setText(sponsor.getPictureUrl());

        }

        layoutSponsor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewLoader.loadView(SponsorDetailsActivity.createIntent(view.getContext(), sponsor),ViewLoaderStrategyInterface.ViewType.SPONSORS);

            }
        });
    }

    private static class SponsorViewHolder
    {
        RelativeLayout layoutSponsor;
        TextView textViewCompanyName;
        TextView textViewStand;
        ImageView imageViewFavourite;
        View viewSeparator;

        RelativeLayout layoutSponsor2;
        TextView textViewCompanyName2;
        TextView textViewStand2;
        ImageView imageViewFavourite2;
        View viewSeparator2;
    }
}
