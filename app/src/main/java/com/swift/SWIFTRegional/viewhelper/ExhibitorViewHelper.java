package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 20/04/15.
 */
public class ExhibitorViewHelper
{
    /**
     * Create, bind and return a new exhibitor item for use in a list.
     */
    public static View createExhibitorListView(View convertView, ViewGroup parent, Exhibitor object)
    {
        return createExhibitorListView(convertView, parent, object, false);
    }

    /**
     * Create, bind and return a new exhibitor item for use in a list.
     *
     * @param detailPaddingMode Set this to true to remove the padding from the textView in this listitem.
     */
    public static View createExhibitorListView(View convertView, ViewGroup parent, Exhibitor object, boolean detailPaddingMode)
    {
        if (convertView == null || !(convertView.getTag() instanceof ExhibitorViewHolder))
        {
            convertView = newExhibitorView(parent, parent.getContext());
        }
        bindExhibitorView(convertView, object, detailPaddingMode);

        return convertView;
    }

    private static View newExhibitorView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_exhibitor, parent, false);

        final ExhibitorViewHolder holder = new ExhibitorViewHolder();

        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textView_exhibitorCompanyName);
        holder.textViewStand = (TextView) v.findViewById(R.id.textView_exhibitorStand);
        holder.imageViewFavourite = (ImageView) v.findViewById(R.id.imageView_exhibitor_favourite);

        v.setTag(holder);

        return v;
    }


    private static void bindExhibitorView(View view, Exhibitor exhibitor, boolean detailPaddingMode)
    {
        if (exhibitor != null)
        {
            final ExhibitorViewHolder holder = (ExhibitorViewHolder) view.getTag();

            holder.textViewCompanyName.setText(exhibitor.getName());
            holder.textViewStand.setText(exhibitor.getStandNumber());
            if (detailPaddingMode)
            {
                holder.textViewCompanyName.setPadding(
                        view.getContext().getResources().getDimensionPixelSize(R.dimen.padding_medium_large),
                        holder.textViewCompanyName.getPaddingTop(),
                        holder.textViewCompanyName.getPaddingRight(),
                        holder.textViewCompanyName.getPaddingBottom());

                holder.textViewStand.setPadding(
                        view.getContext().getResources().getDimensionPixelSize(R.dimen.padding_medium_large),
                        holder.textViewStand.getPaddingTop(),
                        holder.textViewStand.getPaddingRight(),
                        holder.textViewStand.getPaddingBottom());
            }

            holder.imageViewFavourite.setVisibility(exhibitor.isFavourite() ? View.VISIBLE : View.GONE);
        }
    }


    private static class ExhibitorViewHolder
    {
        TextView textViewCompanyName;
        TextView textViewStand;
        ImageView imageViewFavourite;
    }
}
