package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.swift.SWIFTRegional.models.Speaker;
import com.swift.SWIFTRegional.utils.PicassoUtils;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 20/04/15.
 */
public class SpeakerViewHelper
{

    public static View createSpeakerView(View convertView, ViewGroup parent, Speaker object)
    {
        if (convertView == null || !(convertView.getTag() instanceof SpeakerViewHolder))
        {
            convertView = newSpeakerView(parent, parent.getContext());
        }
        bindSpeakerView(convertView, object);

        return convertView;
    }

    private static View newSpeakerView(ViewGroup parent, Context context)
    {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_person, parent, false);

        final SpeakerViewHolder holder = new SpeakerViewHolder();

        holder.imageViewPicture = (ImageView) v.findViewById(R.id.imageview_person_profile_picture);
        holder.imageViewFavourite = (ImageView) v.findViewById(R.id.imageview_person_favourite);
        holder.textViewName = (TextView) v.findViewById(R.id.textview_person_name);
        holder.textViewBusinessFunction = (TextView) v.findViewById(R.id.textview_person_business_function);
        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textview_person_company_name);
        holder.textViewExtra = (TextView) v.findViewById(R.id.textview_person_extra);
        holder.viewSeparator = v.findViewById(R.id.separator);

        v.setTag(holder);

        return v;
    }


    private static void bindSpeakerView(View view, Speaker speaker)
    {
        final SpeakerViewHolder holder = (SpeakerViewHolder) view.getTag();

        holder.textViewName.setText(speaker.getFirstName() + " " + speaker.getLastName());
        holder.textViewBusinessFunction.setText(speaker.getTitle());
        holder.textViewCompanyName.setText(speaker.getCompanyName());

        PicassoUtils.loadPersonImage(speaker.getPictureUrl(), holder.imageViewPicture);

//        holder.imageViewFavourite.setVisibility(speaker.isFavourite() ? View.VISIBLE : View.GONE);
//        holder.textViewExtra.setVisibility(View.GONE);
    }


    private static class SpeakerViewHolder
    {
        ImageView imageViewPicture;
        ImageView imageViewFavourite;
        TextView textViewName;
        TextView textViewBusinessFunction;
        TextView textViewCompanyName;
        TextView textViewExtra;
        View viewSeparator;
    }
}
