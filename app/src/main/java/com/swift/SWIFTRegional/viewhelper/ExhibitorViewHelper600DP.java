package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.activities.exhibitors.ExhibitorDetailsActivity;
import com.swift.SWIFTRegional.models.exhibitor.Exhibitor;

/**
 * Created by SimonRaes on 20/04/15.
 */
public class ExhibitorViewHelper600DP
{
    public static ViewLoaderStrategyInterface viewLoader;

    public static View createExhibitorListView(View convertView, ViewLoaderStrategyInterface vLoader, ViewGroup parent, Exhibitor exhibitor, Exhibitor exhibitor2)
    {
        if(viewLoader == null){
            viewLoader = vLoader;
        }

        if (convertView == null || !(convertView.getTag() instanceof ExhibitorViewHolder))
        {
            convertView = newExhibitorView(parent, parent.getContext());
        }

        final ExhibitorViewHolder holder = (ExhibitorViewHolder) convertView.getTag();

        bindExhibitorView(convertView, exhibitor, holder.textViewCompanyName, holder.textViewStand, holder.imageViewFavourite, holder.exhibitorContainer);

        if(exhibitor2 == null){
            holder.exhibitor2Container.setVisibility(View.INVISIBLE);
        } else {
            holder.exhibitor2Container.setVisibility(View.VISIBLE);
            bindExhibitorView(convertView, exhibitor2, holder.textViewCompanyName2, holder.textViewStand2, holder.imageViewFavourite2, holder.exhibitor2Container);
        }

        return convertView;
    }

    private static View newExhibitorView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_exhibitor_tablet, parent, false);

        final ExhibitorViewHolder holder = new ExhibitorViewHolder();

        holder.textViewStand = (TextView) v.findViewById(R.id.textView_exhibitorStand);
        holder.textViewCompanyName = (TextView) v.findViewById(R.id.textView_exhibitorCompanyName);
        holder.imageViewFavourite = (ImageView) v.findViewById(R.id.imageView_exhibitor_favourite);
        holder.exhibitorContainer = (RelativeLayout) v.findViewById(R.id.exhibitor_container);

        holder.textViewStand2 = (TextView) v.findViewById(R.id.textView_exhibitorStand2);
        holder.textViewCompanyName2 = (TextView) v.findViewById(R.id.textView_exhibitorCompanyName2);
        holder.imageViewFavourite2 = (ImageView) v.findViewById(R.id.imageView_exhibitor_favourite2);
        holder.exhibitor2Container = (RelativeLayout) v.findViewById(R.id.exhibitor2_container);

        v.setTag(holder);

        return v;
    }


    private static void bindExhibitorView(View view, final Exhibitor exhibitor, TextView textViewCompanyName, TextView textViewStand, ImageView imageViewFavourite, RelativeLayout exhibitorContainer)
    {
        if (exhibitor != null)
        {
            textViewCompanyName.setText(exhibitor.getName());
            textViewStand.setText(exhibitor.getStandNumber());
            imageViewFavourite.setVisibility(exhibitor.isFavourite() ? View.VISIBLE : View.GONE);

            exhibitorContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(exhibitor.getExhibitorId() != -1) {
                        final Intent intent = ExhibitorDetailsActivity.createIntent(view.getContext(), exhibitor);
                        viewLoader.loadView(intent, ViewLoaderStrategyInterface.ViewType.EXHIBITORS);
                    }
                }
            });
        } else {
            exhibitorContainer.setOnClickListener(null);
        }
    }


    private static class ExhibitorViewHolder
    {
        TextView textViewCompanyName;
        TextView textViewStand;
        ImageView imageViewFavourite;
        RelativeLayout exhibitorContainer;

        TextView textViewCompanyName2;
        TextView textViewStand2;
        ImageView imageViewFavourite2;
        RelativeLayout exhibitor2Container;
    }
}
