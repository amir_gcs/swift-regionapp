package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.swift.SWIFTRegional.models.EventTiming;

import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

import com.swift.SWIFTRegional.models.ExhibitorEvent;
import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.models.session.Stream;
import com.swift.SWIFTRegional.utils.DateUtils;

/**
 * Created by SimonRaes on 14/04/15.
 * Inflates and binds the cards used to display sessions.
 */
public class SessionViewHelper
{
    private static final String TYPE_PRIVATE = "PRIVATE";

    public static View createSessionView(View convertView, ViewGroup parent, EventTiming object, boolean showTitle)
    {
        if (convertView == null || !(convertView.getTag() instanceof SessionViewHolder))
        {
            convertView = newSessionView(parent, parent.getContext());
        }
        bindSessionView(convertView, object, showTitle);

        return convertView;
    }

    private static View newSessionView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_session, parent, false);

        final SessionViewHolder holder = new SessionViewHolder();

        holder.textViewTitle = (TextView) v.findViewById(R.id.textview_session_title);
        holder.textViewRoom = (TextView) v.findViewById(R.id.textview_session_room);
        holder.textViewDay = (TextView) v.findViewById(R.id.textview_sessionDay);
        holder.textViewMonth = (TextView) v.findViewById(R.id.textview_sessionMonth);
        holder.textViewTime = (TextView) v.findViewById(R.id.textview_session_time);
        holder.textViewStreams = (TextView) v.findViewById(R.id.textview_session_streams);
        holder.dateContainer = v.findViewById(R.id.layout_dateContainer);
        holder.layoutContent = v.findViewById(R.id.listitem_session_layout);
        holder.imageViewStatusOne = (ImageView) v.findViewById(R.id.imageview_status_one);
        holder.imageViewStatusTwo = (ImageView) v.findViewById(R.id.imageview_status_two);

        v.setPadding(context.getResources().getDimensionPixelSize(R.dimen.padding_sides), v.getPaddingTop(), context.getResources().getDimensionPixelSize(R.dimen.padding_sides), v.getPaddingBottom());

        v.setTag(holder);

        return v;
    }

    private static void bindSessionView(View view, EventTiming eventTiming, boolean showDate)
    {
        final SessionViewHolder holder = (SessionViewHolder) view.getTag();

        holder.textViewTitle.setText(eventTiming.getEvent().getTitle());
        holder.textViewRoom.setText(eventTiming.getRoomName());

        String startTime = DateUtils.prettyTimeString(view.getContext(), eventTiming.getStartDate());
        String endTime = DateUtils.prettyTimeString(view.getContext(), eventTiming.getEndDate());
        holder.textViewDay.setText(DateUtils.extractDayFromIso8601String(eventTiming.getStartDate()));
        holder.textViewMonth.setText(DateUtils.extractShortMonthStringFromIso8601String(eventTiming.getStartDate()));

        holder.textViewTime.setText(startTime + " - " + endTime);


        if (eventTiming.getEvent() instanceof Session)
        {
            Session session = (Session) eventTiming.getEvent();

            Stream [] sessionStreams = session.getStreams();

            if (sessionStreams != null && sessionStreams.length > 0)
            {
                String separator = ", ";
                StringBuilder displayStreamsStr = new StringBuilder();
                for (Stream sessionStream : sessionStreams) {
                    if (sessionStream.getName() != null && sessionStream.getName() != "") {
                        displayStreamsStr.append(sessionStream.getName());
                        displayStreamsStr.append(separator);
                    }
                }

                String streamsStr = "";
                if (displayStreamsStr.length() != 0) {
                    streamsStr = displayStreamsStr.toString().replaceAll(separator + "$", "");
                }

                holder.textViewStreams.setText(streamsStr);
                holder.textViewStreams.setVisibility(View.VISIBLE);

            }
            else
            {
                holder.textViewStreams.setVisibility(View.GONE);
            }
        }
        else if (eventTiming.getEvent() instanceof ExhibitorEvent)
        {
            ExhibitorEvent exhibitorEvent = (ExhibitorEvent) eventTiming.getEvent();

            if (exhibitorEvent.getExhibitor() !=null && !StringUtils.isEmpty(exhibitorEvent.getExhibitor().getName()))
            {
                holder.textViewStreams.setText(exhibitorEvent.getExhibitor().getName());
                holder.textViewStreams.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.textViewStreams.setVisibility(View.GONE);
            }
        }
        else
        {
            holder.textViewStreams.setVisibility(View.GONE);
        }

        boolean isPrivate = eventTiming.getEvent().getSessionType().equals(TYPE_PRIVATE);

        if (eventTiming.isFavourite())
        {
            holder.imageViewStatusOne.setImageDrawable(ContextCompat.getDrawable(holder.imageViewStatusOne.getContext(), R.drawable.ic_favorite));
            holder.imageViewStatusOne.setVisibility(View.VISIBLE);

            if (isPrivate)
            {
                holder.imageViewStatusTwo.setImageDrawable(ContextCompat.getDrawable(holder.imageViewStatusOne.getContext(), R.drawable.ic_sessions_private));
                holder.imageViewStatusTwo.setVisibility(View.VISIBLE);

            }
            else
            {
                holder.imageViewStatusTwo.setVisibility(View.GONE);
            }
        }
        else
        {
            if (isPrivate)
            {
                holder.imageViewStatusOne.setImageDrawable(ContextCompat.getDrawable(holder.imageViewStatusOne.getContext(), R.drawable.ic_sessions_private));
                holder.imageViewStatusOne.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.imageViewStatusOne.setVisibility(View.GONE);
            }
            holder.imageViewStatusTwo.setVisibility(View.GONE);
        }

        holder.dateContainer.setVisibility(showDate ? View.VISIBLE : View.INVISIBLE);
    }

    private static class SessionViewHolder
    {
        View layoutContent;
        View dateContainer;

        TextView textViewTitle;
        TextView textViewRoom;
        TextView textViewDay;
        TextView textViewMonth;
        TextView textViewTime;
        TextView textViewStreams;

        ImageView imageViewStatusOne;
        ImageView imageViewStatusTwo;
    }

}
