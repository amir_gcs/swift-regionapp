package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodcoresoftware.android.common.views.TextView;
import com.swift.SWIFTRegional.R;

/**
 * Created by SimonRaes on 27/04/15.
 */
public class AttendanceViewHelper
{

    private static View newAttendanceView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_participant_attendance, parent, false);

        final AttendanceViewHolder holder = new AttendanceViewHolder();

        holder.textViewAttending = (TextView) v.findViewById(R.id.textView_participant_attending);
        holder.textViewAttendingOn = (TextView) v.findViewById(R.id.textView_participant_attending_on);

        v.setTag(holder);

        return v;
    }

    private static class AttendanceViewHolder
    {
        TextView textViewAttending;
        TextView textViewAttendingOn;
    }
}
