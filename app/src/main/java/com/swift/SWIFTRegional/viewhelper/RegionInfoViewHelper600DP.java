package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.interfaces.ViewLoaderStrategyInterface;
import com.swift.SWIFTRegional.models.event.RegionInfo;

import com.goodcoresoftware.android.common.views.TextView;

/**
 * Created by zesshan on 11/04/16.
 */
public class RegionInfoViewHelper600DP {

    public static ViewLoaderStrategyInterface viewLoader;

    public static View createRegionView(View convertView, ViewLoaderStrategyInterface vLoader, ViewGroup parent, RegionInfo object, RegionInfo object2)
    {
        if(viewLoader == null){
            viewLoader = vLoader;
        }

        if (convertView == null || !(convertView.getTag() instanceof RegionViewHolder))
        {
            convertView = newSpeakerView(parent, parent.getContext());
        }

        final RegionViewHolder holder = (RegionViewHolder) convertView.getTag();

        bindRegionView(convertView, holder.layoutRegion, object, holder.textViewTitle, holder.textViewEventCount);

        if(object2 == null) {
            holder.layoutRegion2.setVisibility(View.INVISIBLE);
        } else {
            bindRegionView(convertView, holder.layoutRegion2, object2, holder.textViewTitle2, holder.textViewEventCount2);
            holder.layoutRegion2.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private static View newSpeakerView(ViewGroup parent, Context context)
    {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_regioninfo, parent, false);

        final RegionViewHolder holder = new RegionViewHolder();

        holder.layoutRegion = (RelativeLayout) v.findViewById(R.id.layout_content_region_info);
        holder.textViewTitle = (TextView) v.findViewById(R.id.textView_regioninfoname);
        holder.textViewEventCount = (TextView) v.findViewById(R.id.textView_eventCount);

//        holder.layoutRegion2 = (RelativeLayout) v.findViewById(R.id.layout_content_region_info2);
//        holder.textViewTitle2 = (TextView) v.findViewById(R.id.textView_regioninfoname2);
//        holder.textViewEventCount2 = (TextView) v.findViewById(R.id.textView_eventCount2);


        v.setTag(holder);

        return v;
    }

    private static void bindRegionView(View view, RelativeLayout layoutRegion, final RegionInfo regionInfo, TextView textViewTitle,
                                       TextView textViewEventCount)
    {
        textViewTitle.setText(regionInfo.getName());
        textViewEventCount.setText(regionInfo.getEventCount()+" events");
        textViewTitle.setTextColor(Color.parseColor("#574E4C"));
        layoutRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //show events logic
            }
        });


    }


    private static class RegionViewHolder
    {
        RelativeLayout layoutRegion;
        TextView textViewTitle;
        TextView textViewEventCount;


        RelativeLayout layoutRegion2;
        TextView textViewTitle2;
        TextView textViewEventCount2;
    }


}
