package com.swift.SWIFTRegional.viewhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.models.event.TypeInfo;

import com.goodcoresoftware.android.common.views.TextView;

/**
 * Created by SimonRaes on 20/04/15.
 */
public class TypeViewHelper
{
    /**
     * Create, bind and return a new sponsor item for use in a list.
     */
    public static View createTypeInfoListView(View convertView, ViewGroup parent, TypeInfo object)
    {
        return createTypeInfoListView(convertView, parent, object, false);
    }

    /**
     * Create, bind and return a new sponsor item for use in a list.
     *
     * @param detailPaddingMode Set this to true to remove the padding from the textView in this listitem.
     */
    public static View createTypeInfoListView(View convertView, ViewGroup parent, TypeInfo object, boolean detailPaddingMode)
    {
        if (convertView == null || !(convertView.getTag() instanceof TypeInfoViewHolder))
        {
            convertView = newTypeInfoView(parent, parent.getContext());
        }
        bindTypeInfoView(convertView, object, detailPaddingMode);

        return convertView;
    }

    private static View newTypeInfoView(ViewGroup parent, Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View v = inflater.inflate(R.layout.listitem_typeinfo, parent, false);

        final TypeInfoViewHolder holder = new TypeInfoViewHolder();

        holder.textViewTitle = (TextView) v.findViewById(R.id.textView_typeinfoname);
        holder.textViewEventCount = (TextView) v.findViewById(R.id.textView_eventCount);

        v.setTag(holder);

        return v;
    }


    private static void bindTypeInfoView(View view, TypeInfo TypeInfo, boolean detailPaddingMode)
    {
        if (TypeInfo != null)
        {
            final TypeInfoViewHolder holder = (TypeInfoViewHolder) view.getTag();

            holder.textViewTitle.setText(TypeInfo.getName());
            if(TypeInfo.getName().equals("Show All"))
            {
                holder.textViewEventCount.setText("");
            }
            else
            {
                holder.textViewEventCount.setText(TypeInfo.getEventCount()+" events");
            }

        }
    }


    private static class TypeInfoViewHolder
    {
        TextView textViewTitle;
        TextView textViewEventCount;
    }
}
