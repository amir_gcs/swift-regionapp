package com.swift.SWIFTRegional.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
//import com.facebook.stetho.Stetho;
import com.facebook.stetho.Stetho;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.swift.SWIFTRegional.api.callbacks.HttpResponseCallback;
import com.swift.SWIFTRegional.harald.SibosHaraldCallback;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

import java.lang.ref.WeakReference;

import io.fabric.sdk.android.Fabric;
import com.goodcoresoftware.android.common.utils.DeviceUtils;
import com.goodcoresoftware.android.common.utils.Log;
import com.swift.beacons.middleware.SibosHaraldMiddleware;
import mobi.inthepocket.beacons.sdk.ITPBeacon;
import com.swift.SWIFTRegional.BuildConfig;
import com.swift.SWIFTRegional.R;
import com.swift.SWIFTRegional.api.ApiManager;

public class SibosApplication extends Application implements SharedPreferences.OnSharedPreferenceChangeListener
{
    private int adCycle;

    private int activityCount;

    private boolean loggedIn;
    private WeakReference<Activity> activeActivity;

    @Override
    public void onCreate()
    {
        MultiDex.install(this);

        super.onCreate();

        this.loggedIn = UserPreferencesHelper.loggedIn(this);

        Log.initSafeLogging(BuildConfig.LOGS_ENABLED);

        if (BuildConfig.DEBUG)
        {
            // Stetho
            Stetho.initialize(Stetho
                    .newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build());
        }

        if (BuildConfig.REPORT_CRASHES)
        {
            Fabric.with(this, new Crashlytics());
        }

        DeviceUtils.disableHardwareMenuButton(this);

        // Parse
//        final String parseApplicationId = this.getResources().getString(R.string.parse_application_id);
//        final String parseClientKey = this.getResources().getString(R.string.parse_client_key);
//
//
//        Parse.initialize(this, parseApplicationId, parseClientKey);
//
//        ParsePush.subscribeInBackground("", new SaveCallback()
//        {
//            @Override
//            public void done(ParseException e)
//            {
//                if (e == null)
//                {
//                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
//                }
//                else
//                {
//                    Log.e("com.parse.push", "failed to subscribe for push", e);
//                }
//            }
//        });

        final String haraldAppKey = this.getResources().getString(R.string.harald_app_key);

        final SibosHaraldCallback callback = new SibosHaraldCallback(this);

        ITPBeacon.getConfiguration()
                .setAppKey(haraldAppKey)
                .setDefaultNotificationIcon(R.drawable.ic_notification)
                .setCallbacks(callback)
                .init(this);

        // Start Harald SDK
        ITPBeacon.getConfiguration().start(this);

        UserPreferencesHelper.registerAsListener(this, this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public int getNextAdCycle()
    {
        final int cycle = this.adCycle;

        this.adCycle++;

        return cycle;
    }

    // Methods that keep track of the number of active activities.
    // Logs out of QuickBlox when the app goes to the background and logs in when it comes back to the foreground.
    public void activityStarted(final Activity activity)
    {
        this.activeActivity = new WeakReference<>(activity);

        // Code to be executed when bringing the app to the foreground / first launching it.
        if (this.activityCount == 0)
        {
            if (UserPreferencesHelper.loggedIn(this))
            {
                // Check if the access token is still valid.
                // The interceptor attached to every RetroFit request will take care of requesting a new access token if necessary.
                // If the refresh token itself is expired, the interceptor will log out the user and send him back to the login screen.
                ApiManager.getInstance(this).validateToken(new HttpResponseCallback(this));
            }

            if (SibosHaraldMiddleware.isLocateMeBeaconDetected())
            {
                Log.d("Harald", "Show alert from BaseFragment");

            }
        }

        this.activityCount++;
    }

    public void activityStopped()
    {
        this.activityCount--;

        if (this.activityCount == 0)
        {
            this.activeActivity = null;
        }
    }

    public boolean isLoggedIn()
    {
        return this.loggedIn;
    }

    public void goBackToLogin()
    {
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s)
    {
        if (s.equals(UserPreferencesHelper.LOGGED_IN))
        {
            this.loggedIn = UserPreferencesHelper.loggedIn(this);
        }
    }
}