package com.swift.SWIFTRegional.tasks.favorites;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.lang.ref.WeakReference;

import com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider;
import com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider;
import com.swift.SWIFTRegional.contentproviders.EventContentProvider;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;

/**
 * Created by cliff on 14/05/14.
 */
public class RemoveItemFromFavoritesTask extends AsyncTask<Void, Void, Boolean>
{
    private final WeakReference<Context> contextRef;
    private final WeakReference<OnRemoveItemFromFavoritesListener> listenerRef;
    private final int type;
    private final String itemId;

    public RemoveItemFromFavoritesTask(final Context inContext, final int inType, final String inItemId, final OnRemoveItemFromFavoritesListener inListener)
    {
        super();

        this.contextRef = new WeakReference<Context>(inContext);
        this.listenerRef = new WeakReference<OnRemoveItemFromFavoritesListener>(inListener);
        this.type = inType;
        this.itemId = inItemId;
    }

    @Override
    protected Boolean doInBackground(Void... params)
    {
        final Context context = this.contextRef.get();
        if (context != null && this.type > 0 && !TextUtils.isEmpty(this.itemId))
        {
            final int count = context.getContentResolver().delete
            (
                FavoriteContentProvider.CONTENT_URI,
                FavoriteTable.COLUMN_FAVORITE_ID + " = '" + itemId + "' AND " + FavoriteTable.COLUMN_TYPE + " = " + this.type,
                null
            );

            if(count > 0)
            {
                if(this.type == FavoriteTable.FavouriteType.SPEAKER.ordinal())
                {
                    context.getContentResolver().notifyChange(SpeakerContentProvider.CONTENT_URI, null);
                }
                else if(this.type == FavoriteTable.FavouriteType.SESSION.ordinal())
                {
                    context.getContentResolver().notifyChange(EventContentProvider.CONTENT_URI, null);
                }
                else
                {
                    context.getContentResolver().notifyChange(ExhibitorContentProvider.CONTENT_URI, null);
                }

            }

            return count > 0;
        }

        return Boolean.FALSE;
    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        super.onPostExecute(result);

        if (this.listenerRef.get() != null)
        {
            this.listenerRef.get().onRemoveItemFromFavoritesFinished(this.type, this.itemId, result);
        }
    }

    public interface OnRemoveItemFromFavoritesListener
    {
        void onRemoveItemFromFavoritesFinished(int inType, String inItemId, boolean inSuccess);
    }
}
