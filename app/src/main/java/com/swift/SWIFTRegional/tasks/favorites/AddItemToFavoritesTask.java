package com.swift.SWIFTRegional.tasks.favorites;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.swift.SWIFTRegional.contentproviders.EventContentProvider;
import com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider;
import com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;

import java.lang.ref.WeakReference;

import com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider;

/**
 * Created by cliff on 14/05/14.
 */
public class AddItemToFavoritesTask extends AsyncTask<Void, Void, Boolean>
{
    private final WeakReference<Context> contextRef;
    private final WeakReference<OnAddItemToFavoritesListener> listenerRef;
    private final int type;
    private final String itemId;

    public AddItemToFavoritesTask(final Context inContext, final int inType, final String inItemId, final OnAddItemToFavoritesListener inListener)
    {
        super();

        this.contextRef = new WeakReference<Context>(inContext);
        this.listenerRef = new WeakReference<OnAddItemToFavoritesListener>(inListener);
        this.type = inType;
        this.itemId = inItemId;
    }

    @Override
    protected Boolean doInBackground(Void... params)
    {
        final Context context = this.contextRef.get();
        if (context != null && this.type > 0 && !TextUtils.isEmpty(this.itemId))
        {
            final ContentValues values = new ContentValues();
            values.put(FavoriteTable.COLUMN_TYPE, this.type);
            values.put(FavoriteTable.COLUMN_FAVORITE_ID, this.itemId);
            final Uri uri = context.getContentResolver().insert(FavoriteContentProvider.CONTENT_URI, values);

            if(uri != null)
            {
                if(this.type == FavoriteTable.FavouriteType.SPEAKER.ordinal())
                {
                    context.getContentResolver().notifyChange(SpeakerContentProvider.CONTENT_URI, null);
                }
                else if(this.type == FavoriteTable.FavouriteType.SESSION.ordinal())
                {
                    context.getContentResolver().notifyChange(EventContentProvider.CONTENT_URI, null);
                }
                else
                {
                    context.getContentResolver().notifyChange(ExhibitorContentProvider.CONTENT_URI, null);
                }
            }

            return uri != null;
        }

        return Boolean.FALSE;
    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        super.onPostExecute(result);

        if (this.listenerRef.get() != null)
        {
            this.listenerRef.get().onAddItemToFavoritesFinished(this.type, this.itemId, result);
        }
    }

    public interface OnAddItemToFavoritesListener
    {
        void onAddItemToFavoritesFinished(int inType, String inItemId, boolean inSuccess);
    }
}
