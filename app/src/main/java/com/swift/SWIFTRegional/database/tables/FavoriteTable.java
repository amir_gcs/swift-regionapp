package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class FavoriteTable
{
    public static final String TABLE_NAME = "favorites";
    public static final String TABLE_PREFIX = "f";

    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_FAVORITE_ID = "favorite_id";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_TYPE + " numeric, "
            + COLUMN_FAVORITE_ID + " text"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }

    public enum FavouriteType
    {
        SPEAKER,
        EXHIBITOR,
        SESSION,
        DELEGATE,
        EXHIBITOR_SESSION
    }
}
