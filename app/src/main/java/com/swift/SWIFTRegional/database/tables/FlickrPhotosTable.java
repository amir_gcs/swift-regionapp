package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Wesley on 20/05/14.
 */
public class FlickrPhotosTable
{
    public static final String TABLE_NAME = "flickrPhotos";

    public static final String COLUMN_PHOTOSET_ID = "photosetId";
    public static final String COLUMN_PICTURE_URL_LARGE = "pictureUrlLarge";
    public static final String COLUMN_PICTURE_URL_SMALL = "pictureUrlSmall";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_PHOTOSET_ID + " text, "
            + COLUMN_PICTURE_URL_LARGE + " text, "
            + COLUMN_PICTURE_URL_SMALL + " text "
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
