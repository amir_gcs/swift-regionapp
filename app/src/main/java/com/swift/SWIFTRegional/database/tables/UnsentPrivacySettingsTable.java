package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Holds privacy settings to be resent when connection is restored.
 */
public class UnsentPrivacySettingsTable
{


    public static final String TABLE_NAME = "unsentPrivacySettings";

    public static final String COLUMN_SIBOS_KEY = "siboskey";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_COMPANY_PHONE = "telephone";
    public static final String COLUMN_MOBILE_PHONE = "mobile";
    public static final String COLUMN_COMPANY_ADDRESS = "address";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "create table IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_SIBOS_KEY + " text, "
            + COLUMN_EMAIL + " bool, "
            + COLUMN_COMPANY_PHONE + " bool, "
            + COLUMN_MOBILE_PHONE + " bool, "
            + COLUMN_COMPANY_ADDRESS + " bool"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
