package com.swift.SWIFTRegional.database;

import android.content.ContentValues;
import android.content.Context;

import com.swift.SWIFTRegional.models.Event;
import com.swift.SWIFTRegional.models.EventTiming;

import java.util.ArrayList;
import java.util.Arrays;

import com.swift.SWIFTRegional.models.Session;
import com.swift.SWIFTRegional.models.session.SessionSpeaker;

/**
 * The helper class is used to collect all events (both sessions and exhibitor events) and perform a bulk insert operation.
 * This helper was necessary because we had to generate a unique ID for each event,
 * so therefore it was important to first collect them all and subsequently perform the bulk insert.
 *
 * The unique ID is required to link an EventTiming to an event.
 * For instance, the Exhibitor events have no notion of a unique identifier, so therefore this ID is used to link to them.
 *
 */

public class EventDatabaseHelper
{
    static private ArrayList<Event> eventList = new ArrayList<>();

    static public void addEvents(Event[] events)
    {
        eventList.addAll(Arrays.asList(events));
    }

    static public void execute(Context context)
    {
        int i = 0;

        Event[] events = eventList.toArray(new Event[eventList.size()]);
        ArrayList<EventTiming> timingsList = new ArrayList<>();
        ArrayList<SessionSpeaker> sessionSpeakerList = new ArrayList<>();

        for(Event event : events)
        {
            event.setEventId(i);

            for(EventTiming eventTiming : event.getEventTimings())
            {
                eventTiming.setEventId(i);
                timingsList.add(eventTiming);
            }

            // In case of sessions, also collect speakers

            if(event instanceof Session)
            {
                Session session = (Session) event;

                for(SessionSpeaker sessionSpeaker : session.getSessionSpeakers())
                {
                    sessionSpeaker.setSessionId(i);
                    sessionSpeakerList.add(sessionSpeaker);
                }
            }

            i++;
        }

        // Convert events to content values

        final ContentValues[] contentValues = new ContentValues[events.length];

        for (int n = 0; n < contentValues.length; n++)
        {
            contentValues[n] = events[n].getContentValues();
        }

        // Convert timings to content values

        EventTiming[] timings = timingsList.toArray(new EventTiming[timingsList.size()]);

        final ContentValues[] timingContentValues = new ContentValues[timings.length];

        for (int z = 0; z < timings.length; z++)
        {
            timingContentValues[z] = timings[z].getContentValues();
        }

        // Convert session speakers to content values

        SessionSpeaker[] speakers = sessionSpeakerList.toArray(new SessionSpeaker[sessionSpeakerList.size()]);

        final ContentValues[] speakerContentValues = new ContentValues[speakers.length];

        for (int y = 0; y < speakers.length; y++)
        {
            speakerContentValues[y] = speakers[y].getContentValues();
        }

        /*context.getContentResolver().delete(EventContentProvider.CONTENT_URI, null, null);
        context.getContentResolver().bulkInsert(EventContentProvider.CONTENT_URI, contentValues);

        context.getContentResolver().delete(EventTimingContentProvider.CONTENT_URI, null, null);
        context.getContentResolver().bulkInsert(EventTimingContentProvider.CONTENT_URI, timingContentValues);

        context.getContentResolver().delete(SessionSpeakerContentProvider.CONTENT_URI, null, null);
        context.getContentResolver().bulkInsert(SessionSpeakerContentProvider.CONTENT_URI, speakerContentValues);


        UpdatePreferencesHelper.saveUpdateTimestamp(context, UpdatePreferencesHelper.LastUpdateType.SESSIONS);  */


        eventList.clear();
    }


}
