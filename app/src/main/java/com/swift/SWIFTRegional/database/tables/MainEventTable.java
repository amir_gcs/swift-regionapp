package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by khuru on 17-Oct-2016.
 */

public class MainEventTable {


    public static final String TABLE_NAME = "mainEvents";
    public static final String TABLE_INDEX = "idxEvents";
    public static final String TABLE_PREFIX = "mevt";

    public static final String COLUMN_ID = "event_id";
    public static final String COLUMN_REGIONID = "region_id";
    public static final String COLUMN_TYPEID = "type_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_STARTDATE = "startdate";
    public static final String COLUMN_ENDDATE = "enddate";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_PICTUREURL = "pictureurl";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_SELECTED = "selected";

    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_ID + " numeric unique, "
            + COLUMN_REGIONID + " numeric, "
            + COLUMN_TYPEID + " numeric, "
            + COLUMN_TITLE + " text, "
            + COLUMN_PICTUREURL + " text, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_LOCATION + " text, "
            + COLUMN_STARTDATE + " date, "
            + COLUMN_ENDDATE + " date, "
            + COLUMN_SELECTED + " numeric, "
            + COLUMN_SEARCH_NORMALIZED + " text"
            + ");";

    private static final String CREATE_INDEX = "CREATE INDEX IF NOT EXISTS " + TABLE_INDEX
            + " ON " + TABLE_NAME
            + " ("
            + COLUMN_SEARCH_NORMALIZED
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
        onCreateIndex(db);
    }

    public static void onCreateIndex(SQLiteDatabase db)
    {
        db.execSQL(CREATE_INDEX);
    }

    public static String[] getColumnNames(){
        return new String[]{
                "_id",
                COLUMN_ID,
                COLUMN_REGIONID,
                COLUMN_TYPEID,
                COLUMN_TITLE,
                COLUMN_PICTUREURL,
                COLUMN_DESCRIPTION,
                COLUMN_LOCATION,
                COLUMN_STARTDATE,
                COLUMN_ENDDATE,
                COLUMN_SELECTED
        };
    }

    /**
     * Returns column names to construct cursor containing rows with two Sponsors each
     * @return
     */
    public static String[] getColumnNamesTwoItems(){
        String[] columnNames = getColumnNames();
        String[] columnNames2 = new String[columnNames.length*2-1];
        for(Integer i=0; i < columnNames2.length; i++) {
            if (i < columnNames.length) {
                columnNames2[i] = columnNames[i];
            } else {
                columnNames2[i] = columnNames[(i + 1) % columnNames.length] + "2";
            }
        }
        return columnNames2;
    }
}
