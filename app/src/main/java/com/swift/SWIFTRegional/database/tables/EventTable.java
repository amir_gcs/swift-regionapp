package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class EventTable
{
    public static final String TABLE_NAME = "events";
    public static final String TABLE_INDEX = "idxEvents";
    public static final String TABLE_PREFIX = "e";

    public static final String COLUMN__ID = "__id";
    public static final String COLUMN_SESSION_TYPE = "session_type";
    public static final String COLUMN_EVENT_TYPE_ID = "event_type";
    public static final String COLUMN_EVENT_ID = "event_id";
    public static final String COLUMN_SESSION_ID = "session_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TYPEID = "typeid";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_STREAMS = "streams";
    public static final String COLUMN_TAGS = "tags";
    public static final String COLUMN_YOUTUBE_ID = "youtube_id";
    public static final String COLUMN_CCVK = "ccvk";
    public static final String COLUMN_EXHIBITOR_CCVK = "exhibitorCcvk";

    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";

    public static final String COLUMN_SPEAKERINFO = "speakerinfo";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + COLUMN__ID + " integer primary key autoincrement, "
            + COLUMN_EVENT_ID + " integer, "
            + COLUMN_SESSION_TYPE + " text, "
            + COLUMN_EVENT_TYPE_ID + " integer, "
            + COLUMN_SESSION_ID + " numeric unique, "
            + COLUMN_TITLE + " text, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_TYPEID + " text, "
            + COLUMN_TYPE + " text, "
            + COLUMN_STREAMS + " text, "
            + COLUMN_TAGS + " text, "
            + COLUMN_YOUTUBE_ID + " text, "
            + COLUMN_CCVK + " text, "
            + COLUMN_SEARCH_NORMALIZED + " text, "
            + COLUMN_EXHIBITOR_CCVK + " text"
            + ");";

    private static final String CREATE_INDEX = "CREATE INDEX IF NOT EXISTS " + TABLE_INDEX
            + " ON " + TABLE_NAME
            + " ("
            + COLUMN_SEARCH_NORMALIZED
            + ");";


    public static void onUpdate(SQLiteDatabase db)
    {
        onCreate(db);
    }

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
        onCreateIndex(db);
    }

    public static void onCreateIndex(SQLiteDatabase db)
    {
        db.execSQL(CREATE_INDEX);
    }
}
