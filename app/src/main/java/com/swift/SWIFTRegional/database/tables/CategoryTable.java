package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class CategoryTable
{
    public static final String TABLE_NAME = "categories";
    public static final String TABLE_PREFIX = "cat";

    public static final String COLUMN__ID = "_id";
    public static final String COLUMN_CATEGORY_ID = "categoryId";
    public static final String COLUMN_NAME = "categoryName";
    public static final String COLUMN_TYPE =  "categoryType";

    // Used with a db query to build session list filter
    public static final String COLUMN_COUNT = "count(*)";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + COLUMN__ID + " integer primary key autoincrement, "
            + COLUMN_CATEGORY_ID + " text,"
            + COLUMN_NAME + " text, "
            + COLUMN_TYPE + " text"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }

}
