package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class SpeakerTable
{
    public static final String TABLE_NAME = "speakers";
    public static final String TABLE_INDEX = "idxSpeakers";
    public static final String TABLE_PREFIX = "speak";

    public static final String COLUMN_SPEAKER_ID = "speaker_id";
    public static final String COLUMN_SIBOSKEY = "sibos_key";
    public static final String COLUMN_SALUTATION = "salutation";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_COMPANY_NAME = "company_name";
    public static final String COLUMN_BUSINESS_FUNCTION = "business_function";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_BIOGRAPHY = "biography";
    public static final String COLUMN_PICTURE_URL = "picture_url";

    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";

    public static final String[] SpeakerProjection = {
            TABLE_PREFIX + "." + "_id",
            TABLE_PREFIX + "." + COLUMN_SPEAKER_ID,
            TABLE_PREFIX + "." + COLUMN_SIBOSKEY,
            TABLE_PREFIX + "." + COLUMN_SALUTATION,
            TABLE_PREFIX + "." + COLUMN_TITLE,
            TABLE_PREFIX + "." + COLUMN_FIRST_NAME,
            TABLE_PREFIX + "." + COLUMN_LAST_NAME,
            TABLE_PREFIX + "." + COLUMN_EMAIL,
            TABLE_PREFIX + "." + COLUMN_COMPANY_NAME,
            TABLE_PREFIX + "." + COLUMN_BUSINESS_FUNCTION,
            TABLE_PREFIX + "." + COLUMN_COUNTRY,
            TABLE_PREFIX + "." + COLUMN_BIOGRAPHY,
            TABLE_PREFIX + "." + COLUMN_PICTURE_URL,
            TABLE_PREFIX + "." + COLUMN_SEARCH_NORMALIZED
    };

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_SPEAKER_ID + " text, "
            + COLUMN_SIBOSKEY + " text, "
            + COLUMN_SALUTATION + " text, "
            + COLUMN_TITLE + " text, "
            + COLUMN_FIRST_NAME + " text, "
            + COLUMN_LAST_NAME + " text, "
            + COLUMN_EMAIL + " text, "
            + COLUMN_COMPANY_NAME + " text, "
            + COLUMN_BUSINESS_FUNCTION + " text, "
            + COLUMN_COUNTRY + " text, "
            + COLUMN_BIOGRAPHY + " text, "
            + COLUMN_PICTURE_URL + " text, "
            + COLUMN_SEARCH_NORMALIZED + " text"
            + ");";


    private static final String CREATE_INDEX = "CREATE INDEX IF NOT EXISTS " + TABLE_INDEX
            + " ON " + TABLE_NAME
            + " ("
            + COLUMN_SEARCH_NORMALIZED
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
        onCreateIndex(db);
    }

    public static void onCreateIndex(SQLiteDatabase db)
    {
        db.execSQL(CREATE_INDEX);
    }

    public static String[] getColumnNames() {
        return new String[] {
                "_id",
                COLUMN_SPEAKER_ID,
                COLUMN_SIBOSKEY,
                COLUMN_SALUTATION,
                COLUMN_TITLE,
                COLUMN_FIRST_NAME,
                COLUMN_LAST_NAME,
                COLUMN_EMAIL,
                COLUMN_COMPANY_NAME,
                COLUMN_BUSINESS_FUNCTION,
                COLUMN_COUNTRY,
                COLUMN_BIOGRAPHY,
                COLUMN_PICTURE_URL,
                FavoriteTable.COLUMN_FAVORITE_ID
        };
    }

    /**
     * Returns column names to construct cursor containing rows with two speakers each
     * @return
     */
    public static String[] getColumnNamesTwoItems(){
        String[] columnNames = getColumnNames();
        String[] columnNames2 = new String[columnNames.length*2-1];
        for(Integer i=0; i < columnNames2.length; i++) {
            if (i < columnNames.length) {
                columnNames2[i] = columnNames[i];
            } else {
                columnNames2[i] = columnNames[(i + 1) % columnNames.length] + "2";
            }
        }
        return columnNames2;
    }

}
