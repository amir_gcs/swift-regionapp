package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Holds ratings that failed to sent, so they can be resent when connection is restored.
 */
public class UnsentRateTable extends RateTable
{
    public static final String TABLE_NAME = "unsentRates";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "create table IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_ID + " text unique, "
            + COLUMN_TYPE + " numeric, "
            + COLUMN_RATE + " numeric"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
