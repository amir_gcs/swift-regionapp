package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by khuru on 17-Oct-2016.
 */

public class SponsorTable {


    public static final String TABLE_NAME = "sponsors";
    public static final String TABLE_INDEX = "idxSponsors";
    public static final String TABLE_PREFIX = "spo";

    public static final String COLUMN_ID = "sponsor_id";
    public static final String COLUMN_EVENTID = "event_id";
    public static final String COLUMN_COMPANY_NAME = "company_name";
    public static final String COLUMN_WEBSITE_ADDRESS = "website_address";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_PRODUCTS_ON_SHOW = "products_on_show";
    public static final String COLUMN_COMPANY_LOGO_URL = "company_logo_url";

    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_ID + " numeric unique, "
            + COLUMN_EVENTID + " numeric, "
            + COLUMN_COMPANY_NAME + " text, "
            + COLUMN_WEBSITE_ADDRESS + " text, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_PRODUCTS_ON_SHOW + " text, "
            + COLUMN_COMPANY_LOGO_URL + " text, "
            + COLUMN_SEARCH_NORMALIZED + " text"
            + ");";

    private static final String CREATE_INDEX = "CREATE INDEX IF NOT EXISTS " + TABLE_INDEX
            + " ON " + TABLE_NAME
            + " ("
            + COLUMN_SEARCH_NORMALIZED
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
        onCreateIndex(db);
    }

    public static void onCreateIndex(SQLiteDatabase db)
    {
        db.execSQL(CREATE_INDEX);
    }

    public static String[] getColumnNames(){
        return new String[]{
                "_id",
                COLUMN_ID,
                COLUMN_EVENTID,
                COLUMN_COMPANY_NAME,
                COLUMN_WEBSITE_ADDRESS,
                COLUMN_DESCRIPTION,
                COLUMN_PRODUCTS_ON_SHOW,
                COLUMN_COMPANY_LOGO_URL,
                FavoriteTable.COLUMN_FAVORITE_ID
        };
    }

    /**
     * Returns column names to construct cursor containing rows with two Sponsors each
     * @return
     */
    public static String[] getColumnNamesTwoItems(){
        String[] columnNames = getColumnNames();
        String[] columnNames2 = new String[columnNames.length*2-1];
        for(Integer i=0; i < columnNames2.length; i++) {
            if (i < columnNames.length) {
                columnNames2[i] = columnNames[i];
            } else {
                columnNames2[i] = columnNames[(i + 1) % columnNames.length] + "2";
            }
        }
        return columnNames2;
    }
}
