package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

import com.swift.SWIFTRegional.models.WallItem;

/**
 * Created by wesley on 30/06/14.
 */
public class VideoTable extends WallItem
{
    public static final String TABLE_NAME = "video";

    public static final String COLUMN_ID = WallTable.COLUMN_ID;
    public static final String COLUMN_DATE = WallTable.COLUMN_DATE;
    public static final String COLUMN_LINK = WallTable.COLUMN_LINK;
    public static final String COLUMN_TITLE = WallTable.COLUMN_TITLE;
    public static final String COLUMN_PICTURE_URL = WallTable.COLUMN_PICTURE_URL;

    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";

    public static final String COLUMN_TYPE = WallTable.COLUMN_TYPE;

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_ID + " text unique, "
            + COLUMN_DATE + " numeric, "
            + COLUMN_LINK + " text, "
            + COLUMN_TITLE + " text, "
            + COLUMN_PICTURE_URL + " text, "
            + COLUMN_TYPE + " integer, "
            + COLUMN_SEARCH_NORMALIZED + " text "
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
