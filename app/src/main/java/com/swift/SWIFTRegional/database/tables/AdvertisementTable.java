package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class AdvertisementTable
{
    public static final String TABLE_NAME = "advertisements";

    public static final String COLUMN__ID = "_id";
    public static final String COLUMN_PICTURE_URL_SMALL = "picture_url_small";
    public static final String COLUMN_PICTURE_URL_BIG = "picture_url_big";
    public static final String COLUMN_LINK = "link";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "create table IF NOT EXISTS " + TABLE_NAME
            + " ("
            + COLUMN__ID + " integer primary key autoincrement, "
            + COLUMN_PICTURE_URL_SMALL + " text, "
            + COLUMN_PICTURE_URL_BIG + " text, "
            + COLUMN_LINK + " text"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
