package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class SessionSpeakerTable
{
    public static final String TABLE_NAME = "sessionSpeakers";
    public static final String TABLE_PREFIX = "spe";

    public static final String COLUMN_SESSION_ID = "session_id";
    public static final String COLUMN_SPEAKER_ID = "speaker_id";
    public static final String COLUMN_SPEAKER_TYPE = "speaker_type";
    public static final String COLUMN_SPEAKER_TYPENAME = "speaker_type_name";
    public static final String COLUMN_IS_MODERATOR = "is_moderator";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_SESSION_ID + " numeric, "
            + COLUMN_SPEAKER_ID + " text, "
            + COLUMN_SPEAKER_TYPE + " text, "
            + COLUMN_SPEAKER_TYPENAME + " text, "
            + COLUMN_IS_MODERATOR + " bool"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
