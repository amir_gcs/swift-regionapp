package com.swift.SWIFTRegional.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.swift.SWIFTRegional.database.tables.EventTable;
import com.swift.SWIFTRegional.database.tables.MainEventTable;
import com.swift.SWIFTRegional.database.tables.PracticalInfoTable;
import com.swift.SWIFTRegional.database.tables.RegionInfoTable;
import com.swift.SWIFTRegional.database.tables.SelectEventTable;
import com.swift.SWIFTRegional.database.tables.SponsorTable;
import com.swift.SWIFTRegional.database.tables.TweetsTable;
import com.swift.SWIFTRegional.database.tables.TypeInfoTable;
import com.swift.SWIFTRegional.utils.SibosUtils;
import com.swift.SWIFTRegional.database.tables.AdvertisementTable;
import com.swift.SWIFTRegional.database.tables.CategoryTable;
import com.swift.SWIFTRegional.database.tables.EventTimingTable;
import com.swift.SWIFTRegional.database.tables.ExhibitorCollateralTable;
import com.swift.SWIFTRegional.database.tables.ExhibitorTable;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.database.tables.FlickrPhotosTable;
import com.swift.SWIFTRegional.database.tables.FlickrPhotosetsTable;
import com.swift.SWIFTRegional.database.tables.InstagramTable;
import com.swift.SWIFTRegional.database.tables.IssuesTable;
import com.swift.SWIFTRegional.database.tables.NewsTable;
import com.swift.SWIFTRegional.database.tables.PendingFavouriteTable;
import com.swift.SWIFTRegional.database.tables.RateTable;
import com.swift.SWIFTRegional.database.tables.SessionSpeakerTable;
import com.swift.SWIFTRegional.database.tables.SpeakerTable;
import com.swift.SWIFTRegional.database.tables.UnsentPrivacySettingsTable;
import com.swift.SWIFTRegional.database.tables.UnsentRateTable;
import com.swift.SWIFTRegional.database.tables.VideoTable;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;
import com.swift.SWIFTRegional.utils.preferences.UserPreferencesHelper;

public class DatabaseHelper extends SQLiteOpenHelper
{
    public static final String DATABASE_NAME = "sibos";
    public static final int DATABASE_VERSION = 10;
    private final Context context;

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        createAllTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

        SibosUtils.gcLog("version | oldVersion = " + oldVersion + " | newVersion = " + newVersion);

//        if (oldVersion < 7)
//        {
//            // Drop and recreate the entire database
//            this.onCreate(db);
//        }
//        else if (oldVersion < 8)
//        {
//            EventTable.onUpdate(db);
//        }
//        else if(oldVersion < 10)
//        {
//            createAllTables(db);
//            UpdatePreferencesHelper.resetAllTimestamps(context);
//        }
//
//        UpdatePreferencesHelper.resetSessionUpdateTimeStamps(this.context);
//
//        if(oldVersion < 8) {
//            UpdatePreferencesHelper.resetSessionUpdateTimeStamps(this.context);
//            UserPreferencesHelper.logout(this.context);
//        }

        if(oldVersion < DATABASE_VERSION) {
            createAllTables(db);
            UpdatePreferencesHelper.resetAllTimestamps(context);
            UpdatePreferencesHelper.resetSessionUpdateTimeStamps(this.context);
            UserPreferencesHelper.logout(this.context);
        }
    }

    private void createAllTables(SQLiteDatabase db) {
        FlickrPhotosetsTable.onCreate(db);
        FlickrPhotosTable.onCreate(db);
        TweetsTable.onCreate(db);
        EventTimingTable.onCreate(db);
        SpeakerTable.onCreate(db);
        EventTable.onCreate(db);
        SessionSpeakerTable.onCreate(db);
        CategoryTable.onCreate(db);
        ExhibitorTable.onCreate(db);
        FavoriteTable.onCreate(db);
        IssuesTable.onCreate(db);
        AdvertisementTable.onCreate(db);
        NewsTable.onCreate(db);
        SponsorTable.onCreate(db);
        InstagramTable.onCreate(db);
        VideoTable.onCreate(db);
        ExhibitorCollateralTable.onCreate(db);
        RateTable.onCreate(db);
        UnsentRateTable.onCreate(db);
        UnsentPrivacySettingsTable.onCreate(db);
        MainEventTable.onCreate(db);
        SelectEventTable.onCreate(db);
        RegionInfoTable.onCreate(db);
        TypeInfoTable.onCreate(db);
        PendingFavouriteTable.onCreate(db);
        PracticalInfoTable.onCreate(db);
    }
}
