package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Holds favourites that failed to submit, so they can be resent when connection is restored.
 */
public class PendingFavouriteTable extends FavoriteTable
{
    public static final String TABLE_NAME = "pendingFavourites";

    public static final String COLUMN_UNFLAG = "unFlag";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "create table IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_TYPE + " numeric, "
            + COLUMN_FAVORITE_ID + " numeric, "
            + COLUMN_UNFLAG + " boolean"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
