package com.swift.SWIFTRegional.database.tables;

/**
 * Created by Wesley on 30/05/14.
 */
public class WallTable
{
    public static final String TABLE_NAME = "wall";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";

    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_PICTURE_URL = "pictureUrl";

    public static final String COLUMN_LINK = "link";

    public static final String COLUMN_TYPE = "type";

    // Only used in search
    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";
}
