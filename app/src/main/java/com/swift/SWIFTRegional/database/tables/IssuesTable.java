package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class IssuesTable
{
    public static final String TABLE_NAME = "issues";

    public static final String COLUMN_ID = WallTable.COLUMN_ID;
    public static final String COLUMN_TITLE = WallTable.COLUMN_TITLE;
    public static final String COLUMN_PICTURE_URL = WallTable.COLUMN_PICTURE_URL;
    public static final String COLUMN_DESCRIPTION = WallTable.COLUMN_DESCRIPTION;
    public static final String COLUMN_DATE = WallTable.COLUMN_DATE;
    public static final String COLUMN_TYPE = WallTable.COLUMN_TYPE;
    public static final String COLUMN_LINK_URL = WallTable.COLUMN_LINK;

    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_ID + " text, "
            + COLUMN_TITLE + " text, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_DATE + " numeric, "
            + COLUMN_PICTURE_URL + " text, "
            + COLUMN_TYPE + " numeric, "
            + COLUMN_LINK_URL + " text, "
            + COLUMN_SEARCH_NORMALIZED + " text"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
