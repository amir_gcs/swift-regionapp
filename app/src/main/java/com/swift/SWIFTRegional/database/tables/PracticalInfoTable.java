package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by amir.naushad on 12/13/2017.
 */

public class PracticalInfoTable {
    public static final String TABLE_NAME = "practicalInfo";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_EVENT_ID = "event_id";
    public static final String COLUMN_URL = "url";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "create table IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_ID + " text unique, "
            + COLUMN_EVENT_ID + " numeric, "
            + COLUMN_URL + " text"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
