package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by khuru on 17-Oct-2016.
 */

public class RegionInfoTable {


    public static final String TABLE_NAME = "regionInfo";
    public static final String TABLE_INDEX = "idxRegions";
    public static final String TABLE_PREFIX = "rgs";

    public static final String COLUMN_ID = "region_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_EVENTCOUNT = "eventCount";
    public static final String COLUMN_PICTUREURL = "pictureUrl";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + "_id integer primary key autoincrement, "
            + COLUMN_ID + " numeric unique, "
            + COLUMN_NAME + " text ,"
            + COLUMN_PICTUREURL + " text "
            + ");";

    private static final String CREATE_INDEX = "CREATE INDEX IF NOT EXISTS " + TABLE_INDEX
            + " ON " + TABLE_NAME
            + " ("
            + COLUMN_NAME
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
        onCreateIndex(db);
    }

    public static void onCreateIndex(SQLiteDatabase db)
    {
        db.execSQL(CREATE_INDEX);
    }

    public static String[] getColumnNames(){
        return new String[]{
                "_id",
                COLUMN_ID,
                COLUMN_NAME,
                COLUMN_PICTUREURL
        };
    }

    /**
     * Returns column names to construct cursor containing rows with two Sponsors each
     * @return
     */
    public static String[] getColumnNamesTwoItems(){
        String[] columnNames = getColumnNames();
        String[] columnNames2 = new String[columnNames.length*2-1];
        for(Integer i=0; i < columnNames2.length; i++) {
            if (i < columnNames.length) {
                columnNames2[i] = columnNames[i];
            } else {
                columnNames2[i] = columnNames[(i + 1) % columnNames.length] + "2";
            }
        }
        return columnNames2;
    }
}
