package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class ExhibitorCollateralTable
{
    public static final String TABLE_NAME = "exhibitors_collateral";

    public static final String COLUMN__ID = "_id";
    public static final String COLUMN_ID = "collateral_id";
    public static final String COLUMN_CCVK = "ccvk";
    public static final String COLUMN_SEQUENCE = "sequence";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_URL = "url";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + COLUMN__ID + " integer primary key autoincrement, "
            + COLUMN_ID + " text, "
            + COLUMN_CCVK + " text, "
            + COLUMN_SEQUENCE + " numeric, "
            + COLUMN_TITLE + " text, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_URL + " text"
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
    }
}
