package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class EventTimingTable
{
    public static final String TABLE_NAME = "eventTimings";
    public static final String TABLE_INDEX = "idxTimings";
    public static final String TABLE_PREFIX = "t";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_EVENT_ID = "eventId";
    public static final String COLUMN_EVENT_TYPE = "eventType";
    public static final String COLUMN_ROOM_NAME = "room";
    public static final String COLUMN_ROOM_CODE = "room_code";
    public static final String COLUMN_START_TIME = "start_time";
    public static final String COLUMN_END_TIME = "end_time";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_IS_REHEARSAL = "is_rehearsal";

    public static final String COLUMN_SEARCH_NORMALIZED = "searchNormalized";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + " ("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_EVENT_ID + " integer, "
            + COLUMN_EVENT_TYPE + " integer, "
            + COLUMN_ROOM_NAME + " text, "
            + COLUMN_ROOM_CODE + " text, "
            + COLUMN_START_TIME + " text, "
            + COLUMN_END_TIME + " text, "
            + COLUMN_DATE + " text, "
            + COLUMN_IS_REHEARSAL + " numeric, "
            + COLUMN_SEARCH_NORMALIZED + " text"
            + ");";

    private static final String CREATE_INDEX = "CREATE INDEX IF NOT EXISTS " + TABLE_INDEX
            + " ON " + TABLE_NAME
            + " ("
            + COLUMN_SEARCH_NORMALIZED
            + ");";

    public static void onUpdate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
        onCreateIndex(db);
    }

    public static void onCreateIndex(SQLiteDatabase db)
    {
        db.execSQL(CREATE_INDEX);
    }
}
