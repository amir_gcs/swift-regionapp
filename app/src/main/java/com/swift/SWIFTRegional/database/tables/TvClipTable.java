package com.swift.SWIFTRegional.database.tables;

import android.database.sqlite.SQLiteDatabase;

public class TvClipTable
{
    public static final String TABLE_NAME = "tvclips";

    public static final String COLUMN__ID = "_id";
    public static final String COLUMN_TAGLINE = "tagline";
    public static final String COLUMN_IMAGE_URL = "image";
    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_TAGS = "tags";
    public static final String COLUMN_HI_RES = "hiRes";
    public static final String COLUMN_YOUTUBE = "youtubeId";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DATE = "date";

    private static final String CREATE_TABLE = "create table IF NOT EXISTS " + TABLE_NAME
            + " (" + COLUMN__ID + " integer primary key autoincrement, "
            + COLUMN_TAGLINE + " text, "
            + COLUMN_IMAGE_URL + " text, "
            + COLUMN_CATEGORY + " text, "
            + COLUMN_TAGS + " text, "
            + COLUMN_HI_RES + " bool, "
            + COLUMN_YOUTUBE + " text, "
            + COLUMN_TITLE + " text, "
            + COLUMN_DATE + " date "
            + ");";

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_TABLE);
    }
}
