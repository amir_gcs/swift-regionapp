package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;
import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.PracticalInfoTable;
/**
 * Created by amir.naushad on 12/13/2017.
 */

public class PracticalInfoContentProvider extends ContentProvider{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.PracticalInfoContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_EVENT = Uri.parse("content://" + PROVIDER_NAME + "/event");

    private static final int EVENT = 1;
    private static final int EVENT_ID = 2;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, EVENT);
        uriMatcher.addURI(PROVIDER_NAME, "event/*", EVENT_ID);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case EVENT:
            
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case EVENT_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(PracticalInfoTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case EVENT:

                return this.database.delete(PracticalInfoTable.TABLE_NAME, selection, null);

            case EVENT_ID:

                final String eventId = uri.getLastPathSegment();

                return this.database.delete(PracticalInfoTable.TABLE_NAME, PracticalInfoTable.COLUMN_EVENT_ID + " = ?", new String[]{eventId});

        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;
        String query = "";
        String searchText = null;

        switch (uriMatcher.match(uri))
        {

            case EVENT:

                query = this.getSelectFrom();

                cursor = this.database.rawQuery(query, null);


                break;

            case EVENT_ID:

                final String eventId = uri.getLastPathSegment();

                query = this.getSelectFrom();
                query += " Where "+PracticalInfoTable.COLUMN_EVENT_ID +"="+eventId;

                cursor = this.database.rawQuery(query, null);

                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    private String getSelectFrom()
    {
        return "SELECT "
                + " * "
                + " FROM " + PracticalInfoTable.TABLE_NAME;
    }
}
