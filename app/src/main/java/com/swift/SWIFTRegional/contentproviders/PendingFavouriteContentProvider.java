package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.database.tables.PendingFavouriteTable;

/**
 * Created by SimonRaes on 3/04/15.
 * Stores favourite actions that couldn't be submitted to the API.
 */
public class PendingFavouriteContentProvider extends ContentProvider
{

    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.PendingFavouriteContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_UNSENT_SPEAKER_FAVOURITE = Uri.parse("content://" + PROVIDER_NAME + "/speaker");
    public static final Uri CONTENT_URI_UNSENT_EXHIBITOR_FAVOURITE = Uri.parse("content://" + PROVIDER_NAME + "/exhibitor");
    public static final Uri CONTENT_URI_UNSENT_SESSION_FAVOURITE = Uri.parse("content://" + PROVIDER_NAME + "/session");

    private static final int PENDING_FAVOURITES = 1;
    private static final int PENDING_SPEAKER_FAVOURITE = 2;
    private static final int PENDING_EXHIBITOR_FAVOURITE = 3;
    private static final int PENDING_SESSION_FAVOURITE = 4;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, PENDING_FAVOURITES);
        uriMatcher.addURI(PROVIDER_NAME, "speaker/*", PENDING_SPEAKER_FAVOURITE);
        uriMatcher.addURI(PROVIDER_NAME, "exhibitor/*", PENDING_EXHIBITOR_FAVOURITE);
        uriMatcher.addURI(PROVIDER_NAME, "session/*", PENDING_SESSION_FAVOURITE);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case PENDING_FAVOURITES:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case PENDING_SPEAKER_FAVOURITE:
            case PENDING_EXHIBITOR_FAVOURITE:
            case PENDING_SESSION_FAVOURITE:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings)
    {
        // Not needed
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        switch (uriMatcher.match(uri))
        {
            case PENDING_FAVOURITES:
            {
                // delete record if it exists
                final String faveId = values.getAsString(PendingFavouriteTable.COLUMN_FAVORITE_ID).toString();
                final String type = values.getAsString(FavoriteTable.COLUMN_TYPE);

                this.delete(Uri.withAppendedPath(uri, faveId), PendingFavouriteTable.COLUMN_TYPE + " = ?", new String[]{type});

                // Add record
                long rowID = this.database.replace(PendingFavouriteTable.TABLE_NAME, null, values);

                // If record is added successfully
                if (rowID > 0)
                {
                    final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
                    this.getContext().getContentResolver().notifyChange(_uri, null);



                    return _uri;
                }
                break;
            }
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String query;
        Cursor cursor;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(PendingFavouriteTable.TABLE_NAME);

        switch (uriMatcher.match(uri))
        {
            case PENDING_FAVOURITES:
                // Don't have to add anything to the querybuilder to select everything
                break;

            case PENDING_SESSION_FAVOURITE:

                qb.appendWhere(PendingFavouriteTable.COLUMN_FAVORITE_ID + " = ? AND " + PendingFavouriteTable.COLUMN_TYPE + "=" + PendingFavouriteTable.FavouriteType.SESSION.ordinal());
                selectionArgs = new String[]{uri.getLastPathSegment()};
                break;

            case PENDING_EXHIBITOR_FAVOURITE:

                qb.appendWhere(PendingFavouriteTable.COLUMN_FAVORITE_ID + " = ? AND " + PendingFavouriteTable.COLUMN_TYPE + "=" + PendingFavouriteTable.FavouriteType.EXHIBITOR.ordinal());
                selectionArgs = new String[]{uri.getLastPathSegment()};
                break;

            case PENDING_SPEAKER_FAVOURITE:

                qb.appendWhere(PendingFavouriteTable.COLUMN_FAVORITE_ID + " = ? AND " + PendingFavouriteTable.COLUMN_TYPE + "=" + PendingFavouriteTable.FavouriteType.SPEAKER.ordinal());
                selectionArgs = new String[]{uri.getLastPathSegment()};
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case PENDING_SPEAKER_FAVOURITE:

                final String speakerFavouriteId = uri.getLastPathSegment();

                return this.database.delete(PendingFavouriteTable.TABLE_NAME,
                        PendingFavouriteTable.COLUMN_FAVORITE_ID + " = ? AND " +
                                PendingFavouriteTable.COLUMN_TYPE + " = " + FavoriteTable.FavouriteType.SPEAKER.ordinal(),
                        new String[]{speakerFavouriteId}
                );

            case PENDING_EXHIBITOR_FAVOURITE:

                final String exhibitorFavouriteId = uri.getLastPathSegment();

                return this.database.delete(PendingFavouriteTable.TABLE_NAME,
                        PendingFavouriteTable.COLUMN_FAVORITE_ID + " = ? AND " +
                                PendingFavouriteTable.COLUMN_TYPE + " = " + FavoriteTable.FavouriteType.EXHIBITOR.ordinal(),
                        new String[]{exhibitorFavouriteId}
                );

            case PENDING_SESSION_FAVOURITE:

                final String sessionFavouriteId = uri.getLastPathSegment();

                return this.database.delete(PendingFavouriteTable.TABLE_NAME,
                        PendingFavouriteTable.COLUMN_FAVORITE_ID + " = ? AND " +
                                PendingFavouriteTable.COLUMN_TYPE + " = " + FavoriteTable.FavouriteType.SESSION.ordinal(),
                        new String[]{sessionFavouriteId}
                );

            case PENDING_FAVOURITES:

                return this.database.delete(PendingFavouriteTable.TABLE_NAME, selection, null);
        }

        return 0;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        throw new IllegalArgumentException("Unsupported URI: " + uri);
    }


}
