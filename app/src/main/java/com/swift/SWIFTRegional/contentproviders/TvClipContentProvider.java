package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.TvClipTable;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.IssuesTable;

public class TvClipContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.TvClipContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_CLIP = Uri.parse("content://" + PROVIDER_NAME + "/clip");

    private static final int TV_CLIPS = 1;
    private static final int TV_CLIP_ID = 2;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, TV_CLIPS);
        uriMatcher.addURI(PROVIDER_NAME, "clip/*", TV_CLIP_ID);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case TV_CLIPS:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case TV_CLIP_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(TvClipTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case TV_CLIPS:

                return this.database.delete(TvClipTable.TABLE_NAME, selection, null);

            case TV_CLIP_ID:

                final String whereClause = TvClipTable.COLUMN__ID + " = '" + uri.getLastPathSegment() + "'";
                return this.database.delete(TvClipTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TvClipTable.TABLE_NAME);

        switch (uriMatcher.match(uri))
        {
            case TV_CLIPS:

                break;

            case TV_CLIP_ID:

                qb.appendWhere(TvClipTable.COLUMN__ID + " = '" + uri.getLastPathSegment() + "'");
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (TextUtils.isEmpty(sortOrder))
        {
            sortOrder = "LOWER(" + IssuesTable.COLUMN_DATE + ") DESC";
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case TV_CLIPS:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, TvClipTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(TvClipTable.COLUMN_TAGLINE, contentValues.getAsString(TvClipTable.COLUMN_TAGLINE));
                        inserter.bind(TvClipTable.COLUMN_IMAGE_URL, contentValues.getAsString(TvClipTable.COLUMN_IMAGE_URL));
                        inserter.bind(TvClipTable.COLUMN_CATEGORY, contentValues.getAsString(TvClipTable.COLUMN_CATEGORY));
                        inserter.bind(TvClipTable.COLUMN_TAGS, contentValues.getAsString(TvClipTable.COLUMN_TAGS));
                        inserter.bind(TvClipTable.COLUMN_HI_RES, contentValues.getAsBoolean(TvClipTable.COLUMN_HI_RES));
                        inserter.bind(TvClipTable.COLUMN_YOUTUBE, contentValues.getAsString(TvClipTable.COLUMN_YOUTUBE));
                        inserter.bind(TvClipTable.COLUMN_TITLE, contentValues.getAsString(TvClipTable.COLUMN_TITLE));
                        inserter.bind(TvClipTable.COLUMN_DATE, contentValues.getAsLong(TvClipTable.COLUMN_DATE));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("TvClipContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
