package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.ExhibitorTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.util.ArrayList;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;

public class ExhibitorContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.ExhibitorContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_EXHIBITOR = Uri.parse("content://" + PROVIDER_NAME + "/exhibitor");
    public static final Uri CONTENT_URI_FAVORITES = Uri.parse("content://" + PROVIDER_NAME + "/favorites");
    public static final Uri CONTENT_URI_SUGGESTED = Uri.parse("content://" + PROVIDER_NAME + "/suggested");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    private static final int EXHIBITORS = 1;
    private static final int EXHIBITOR_ID = 2;
    private static final int FAVORITES = 3;
    private static final int FAVORITES_SEARCH = 4;
    private static final int SUGGESTED = 5;
    private static final int SUGGESTED_SEARCH = 6;
    private static final int SEARCH = 7;


    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, EXHIBITORS);
        uriMatcher.addURI(PROVIDER_NAME, "exhibitor/*", EXHIBITOR_ID);
        uriMatcher.addURI(PROVIDER_NAME, "favorites", FAVORITES);
        uriMatcher.addURI(PROVIDER_NAME, "favorites/*", FAVORITES_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "suggested", SUGGESTED);
        uriMatcher.addURI(PROVIDER_NAME, "suggested/*", SUGGESTED_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case EXHIBITORS:
            case SEARCH:
            case FAVORITES_SEARCH:
            case FAVORITES:
            case SUGGESTED:
            case SUGGESTED_SEARCH:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case EXHIBITOR_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(ExhibitorTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case EXHIBITORS:

                return this.database.delete(ExhibitorTable.TABLE_NAME, selection, null);

            case EXHIBITOR_ID:

                String whereClause = ExhibitorTable.COLUMN_ID + "=" + uri.getLastPathSegment();
                return this.database.delete(ExhibitorTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String query;
        Cursor cursor;
        String searchText = null;

        switch (uriMatcher.match(uri))
        {
            case SEARCH:

                query = this.getBaseQuery()
                        + " WHERE "
                        + ExhibitorTable.TABLE_PREFIX + "." + ExhibitorTable.COLUMN_SEARCH_NORMALIZED
                        + " LIKE ?";

                selectionArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");
                for (int i = 0; i < selectionArgs.length; i++)
                {
                    if (i >= 1)
                    {
                        query += " AND " + ExhibitorTable.TABLE_PREFIX + "." + ExhibitorTable.COLUMN_SEARCH_NORMALIZED + " LIKE ? ";
                    }
                    selectionArgs[i] = "%" + selectionArgs[i] + "%";
                }

                cursor = this.database.rawQuery(query, selectionArgs);

                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case FAVORITES_SEARCH:
                searchText = uri.getLastPathSegment();

                // Fall through

            case FAVORITES:

                query = "SELECT * FROM "
                        + ExhibitorTable.TABLE_NAME + " " + ExhibitorTable.TABLE_PREFIX
                        + " JOIN "
                        + FavoriteTable.TABLE_NAME + " " + FavoriteTable.TABLE_PREFIX
                        + " ON "
                        + ExhibitorTable.TABLE_PREFIX + "." + ExhibitorTable.COLUMN_CCVK
                        + " = "
                        + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_FAVORITE_ID
                        + " WHERE " + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_TYPE
                        + "=?";

                ArrayList<String> args = new ArrayList<String>();
                args.add(String.valueOf(FavoriteTable.FavouriteType.EXHIBITOR.ordinal()));

                if (!TextUtils.isEmpty(searchText))
                {
                    selectionArgs = SibosUtils.getNormalizedText(searchText).split(" ");
                    for (String selectionArg : selectionArgs)
                    {
                        query += " AND " + ExhibitorTable.TABLE_PREFIX + "." + ExhibitorTable.COLUMN_SEARCH_NORMALIZED + " LIKE ? ";
                        args.add("%" + selectionArg + "%");
                    }
                }

                query += " ORDER BY " + ExhibitorTable.TABLE_PREFIX + "." + ExhibitorTable.COLUMN_COMPANY_NAME + " COLLATE NOCASE ASC";

                cursor = this.database.rawQuery(query, args.toArray(new String[args.size()]));
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case EXHIBITORS:
                qb.setTables(ExhibitorTable.TABLE_NAME);

                query = this.getBaseQuery();

                query += " ORDER BY " + ExhibitorTable.TABLE_PREFIX + "." + ExhibitorTable.COLUMN_COMPANY_NAME + " COLLATE NOCASE ASC";


                ArrayList<String> argsa = new ArrayList<String>();

                cursor = this.database.rawQuery(query, argsa.toArray(new String[argsa.size()]));
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case EXHIBITOR_ID:
                qb.setTables(ExhibitorTable.TABLE_NAME);
                qb.appendWhere(ExhibitorTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (TextUtils.isEmpty(sortOrder))
        {
            sortOrder = "UPPER(" + ExhibitorTable.COLUMN_COMPANY_NAME + ") ASC";
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    /**
     * Returns a query that will join with the favourites table to include the extra isFavourite field in the result.
     */
    private String getBaseQuery()
    {
        return "SELECT * FROM "
                + ExhibitorTable.TABLE_NAME + " " + ExhibitorTable.TABLE_PREFIX
                + " LEFT JOIN "
                + " ( SELECT * FROM " + FavoriteTable.TABLE_NAME + " " + FavoriteTable.TABLE_PREFIX
                + " WHERE " + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_TYPE + " = " + FavoriteTable.FavouriteType.EXHIBITOR.ordinal() + ") "
                + " ON "
                + ExhibitorTable.TABLE_PREFIX + "." + ExhibitorTable.COLUMN_CCVK
                + " = "
                + FavoriteTable.COLUMN_FAVORITE_ID;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case EXHIBITORS:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, ExhibitorTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(ExhibitorTable.COLUMN_ID, contentValues.getAsLong(ExhibitorTable.COLUMN_ID));
                        inserter.bind(ExhibitorTable.COLUMN_CCVK, contentValues.getAsString(ExhibitorTable.COLUMN_CCVK));
                        inserter.bind(ExhibitorTable.COLUMN_COMPANY_NAME, contentValues.getAsString(ExhibitorTable.COLUMN_COMPANY_NAME));
                        inserter.bind(ExhibitorTable.COLUMN_WEBSITE_ADDRESS, contentValues.getAsString(ExhibitorTable.COLUMN_WEBSITE_ADDRESS));
                        inserter.bind(ExhibitorTable.COLUMN_STAND_NUMBER, contentValues.getAsString(ExhibitorTable.COLUMN_STAND_NUMBER));
                        inserter.bind(ExhibitorTable.COLUMN_DESCRIPTION, contentValues.getAsString(ExhibitorTable.COLUMN_DESCRIPTION));
                        inserter.bind(ExhibitorTable.COLUMN_PRODUCTS_ON_SHOW, contentValues.getAsString(ExhibitorTable.COLUMN_PRODUCTS_ON_SHOW));
                        inserter.bind(ExhibitorTable.COLUMN_COMPANY_LOGO_URL, contentValues.getAsString(ExhibitorTable.COLUMN_COMPANY_LOGO_URL));

                        inserter.bind(ExhibitorTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(ExhibitorTable.COLUMN_SEARCH_NORMALIZED));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("ExhibitorContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(ExhibitorContentProvider.CONTENT_URI, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
