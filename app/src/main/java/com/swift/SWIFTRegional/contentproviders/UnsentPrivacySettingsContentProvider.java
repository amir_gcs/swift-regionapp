package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.UnsentPrivacySettingsTable;

public class UnsentPrivacySettingsContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.UnsentPrivacySettingsContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");

    private static final int UNSENT_PRIVACY_SETTINGS = 1;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, UNSENT_PRIVACY_SETTINGS);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case UNSENT_PRIVACY_SETTINGS:
                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {

        switch (uriMatcher.match(uri))
        {
            case UNSENT_PRIVACY_SETTINGS:
            {

                // delete record if it exists
                this.delete(uri, null, null);

                // Add record
                long rowID = this.database.replace(UnsentPrivacySettingsTable.TABLE_NAME, null, values);

                // If record is added successfully
                if (rowID > 0)
                {
                    final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
                    this.getContext().getContentResolver().notifyChange(_uri, null);

                    return _uri;
                }
                break;
            }
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case UNSENT_PRIVACY_SETTINGS:
                return this.database.delete(UnsentPrivacySettingsTable.TABLE_NAME, selection, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String query;
        Cursor cursor;
        switch (uriMatcher.match(uri))
        {
            case UNSENT_PRIVACY_SETTINGS:
                query = "SELECT * FROM " + UnsentPrivacySettingsTable.TABLE_NAME;
                cursor = this.database.rawQuery(query, null);
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }
                return cursor;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        throw new IllegalArgumentException("Unsupported URI: " + uri);
    }
}
