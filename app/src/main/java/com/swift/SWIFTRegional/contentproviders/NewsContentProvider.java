package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.IssuesTable;
import com.swift.SWIFTRegional.database.tables.NewsTable;
import com.swift.SWIFTRegional.database.tables.WallTable;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

public class NewsContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.NewsContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_UPDATES = Uri.parse("content://" + PROVIDER_NAME + "/updates");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    private static final int NEWS = 1;
    private static final int NEWS_ID = 2;
    private static final int NEWS_UPDATES = 3;
    private static final int SEARCH = 4;
    private static final int SEARCH_QUERY = 5;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, NEWS);
        uriMatcher.addURI(PROVIDER_NAME, "news/*", NEWS_ID);
        uriMatcher.addURI(PROVIDER_NAME, "updates", NEWS_UPDATES);
        uriMatcher.addURI(PROVIDER_NAME, "search", SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH_QUERY);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case SEARCH_QUERY:
            case NEWS:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case NEWS_ID:
            case NEWS_UPDATES:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(NewsTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case NEWS:

                return this.database.delete(NewsTable.TABLE_NAME, selection, null);

            case NEWS_ID:

                final String whereClause = "_id='" + uri.getLastPathSegment() + "'";
                return this.database.delete(NewsTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    private List<String> getSeachParams(StringBuilder query, Uri uri, final String SEARCH_COLUMN) {
        List<String> params = new ArrayList<>();
        query.append(" WHERE ").append(SEARCH_COLUMN).append(" LIKE ? ");
        String[] searchArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");
        for (int i = 0; i < searchArgs.length; i++) {
            if (i >= 1) {
                query.append(" AND ").append(SEARCH_COLUMN).append(" LIKE ? ");
            }
            params.add("%" + searchArgs[i] + "%");
        }
        return params;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor = null;

        Integer matchedURI = uriMatcher.match(uri);

        switch (matchedURI)
        {
            case SEARCH_QUERY:
            case NEWS:
            case NEWS_UPDATES:

                final StringBuilder query = new StringBuilder("SELECT ")
                    .append("_id, ")
                    .append(NewsTable.COLUMN_TYPE)
                    .append(", ")
                    .append(NewsTable.COLUMN_ID)
                    .append(", ")
                    .append(NewsTable.COLUMN_DATE)
                    .append(", ")
                    .append(NewsTable.COLUMN_DESCRIPTION)
                    .append(", null")
                    .append(" AS ")
                    .append(WallTable.COLUMN_LINK)
                    .append(", ")
                    .append(NewsTable.COLUMN_TITLE)
                    .append(" FROM ")
                    .append(NewsTable.TABLE_NAME);


                List<String> searchParams = new ArrayList<>();
                if(matchedURI == SEARCH_QUERY) {
                    searchParams.addAll(getSeachParams(query, uri, NewsTable.COLUMN_SEARCH_NORMALIZED));
                }

                query.append(" UNION ALL ")
                    .append("SELECT ")
                    .append("_id, ")
                    .append(IssuesTable.COLUMN_TYPE)
                    .append(", ")
                    .append(IssuesTable.COLUMN_ID)
                    .append(", ")
                    .append(IssuesTable.COLUMN_DATE)
                    .append(", ")
                    .append(IssuesTable.COLUMN_DESCRIPTION)
                    .append(", ")
                    .append(IssuesTable.COLUMN_LINK_URL)
                    .append(", ")
                    .append(IssuesTable.COLUMN_TITLE)
                    .append(" FROM ")
                    .append(IssuesTable.TABLE_NAME);


                if(matchedURI == SEARCH_QUERY) {
                    searchParams.addAll(getSeachParams(query, uri, IssuesTable.COLUMN_SEARCH_NORMALIZED));
                }


                switch(uriMatcher.match(uri))
                {
                    case NEWS:
                        query.append(" ORDER BY " + WallTable.COLUMN_DATE + " DESC");
                        cursor = this.database.rawQuery(query.toString(), null);

                        break;

                    case NEWS_UPDATES:
                        final StringBuilder outerQuery = new StringBuilder("SELECT count(1) AS count FROM (")
                                .append(query)
                                .append(" ) WHERE ")
                                .append(WallTable.COLUMN_DATE)
                                .append(" > ?");

                        final long lastUpdate = UpdatePreferencesHelper.getLastUpdate(this.getContext(), UpdatePreferencesHelper.LastSeenType.NEWS);
                        cursor = this.database.rawQuery(outerQuery.toString(), new String[] { String.valueOf(lastUpdate) });

                        break;

                    case SEARCH_QUERY:
                        cursor = this.database.rawQuery(query.toString(), searchParams.toArray(new String[0]));
                        break;
                }

                break;

            case NEWS_ID:

                final String newsId = uri.getLastPathSegment();

                cursor = this.database.query(NewsTable.TABLE_NAME, projection, NewsTable.COLUMN_ID + " = ?", new String[] { newsId }, null, null, null);


                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if(cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }


        return cursor;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case NEWS:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, NewsTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(NewsTable.COLUMN_ID, String.valueOf(i));
                        inserter.bind(NewsTable.COLUMN_HTML_CONTENT, contentValues.getAsString(NewsTable.COLUMN_HTML_CONTENT));
                        inserter.bind(NewsTable.COLUMN_DESCRIPTION, contentValues.getAsString(NewsTable.COLUMN_DESCRIPTION));
                        inserter.bind(NewsTable.COLUMN_PICTURE_URL, contentValues.getAsString(NewsTable.COLUMN_PICTURE_URL));
                        inserter.bind(NewsTable.COLUMN_FEATURE, contentValues.getAsBoolean(NewsTable.COLUMN_FEATURE));
                        inserter.bind(NewsTable.COLUMN_CATEGORY, contentValues.getAsString(NewsTable.COLUMN_CATEGORY));
                        inserter.bind(NewsTable.COLUMN_TITLE, contentValues.getAsString(NewsTable.COLUMN_TITLE));
                        inserter.bind(NewsTable.COLUMN_DATE, contentValues.getAsLong(NewsTable.COLUMN_DATE));
                        inserter.bind(NewsTable.COLUMN_LINK, contentValues.getAsString(NewsTable.COLUMN_LINK));
                        inserter.bind(NewsTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(NewsTable.COLUMN_SEARCH_NORMALIZED));

                        inserter.bind(NewsTable.COLUMN_TYPE, WallItem.Type.NEWS.ordinal());

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d(this.getClass().getSimpleName(), e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);

                    this.getContext().getContentResolver().notifyChange(NewsContentProvider.CONTENT_URI, null);

                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI_UPDATES, null);
                    this.getContext().getContentResolver().notifyChange(NewsContentProvider.CONTENT_URI_UPDATES, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
