package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;
import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.VideoTable;
import com.swift.SWIFTRegional.fragments.activityfeed.VideoFragment;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.models.video.Video;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by wesley on 30/06/14.
 */
public class VideoContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.VideoContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");

    public static final Uri CONTENT_URI_UPDATES = Uri.parse("content://" + PROVIDER_NAME + "/updates/");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    private static final int VIDEOS = 1;
    private static final int VIDEOS_UPDATES = 2;
    private static final int SEARCH_QUERY = 3;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, VIDEOS);
        uriMatcher.addURI(PROVIDER_NAME, "updates", VIDEOS_UPDATES);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH_QUERY);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case VIDEOS:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case VIDEOS_UPDATES:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;

        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case VIDEOS:

                return this.database.delete(VideoTable.TABLE_NAME, null, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;
        switch (uriMatcher.match(uri))
        {
            case VIDEOS:

                cursor = this.database.query(VideoTable.TABLE_NAME, null, null, null, null, null, VideoTable.COLUMN_DATE + " DESC");

                break;

            case VIDEOS_UPDATES:

                final long lastUpdate = UpdatePreferencesHelper.getLastUpdate(this.getContext(), UpdatePreferencesHelper.LastSeenType.VIDEO);

                cursor = this.database.query(VideoTable.TABLE_NAME, new String[]{"count(1) as count"}, VideoTable.COLUMN_DATE + " > ?", new String[]{String.valueOf(lastUpdate)}, null, null, null);

                break;

            case SEARCH_QUERY:

                String[] searchParams = uri.getLastPathSegment().split(" ");

                StringBuilder query = new StringBuilder("Select * from ")
                    .append(VideoTable.TABLE_NAME)
                    .append(" where ").append(VideoTable.COLUMN_SEARCH_NORMALIZED)
                    .append(" like ?");

                for (Integer i = 0;  i< searchParams.length; i++) {
                    if(i >= 1) {
                        query.append(" and ").append(VideoTable.COLUMN_SEARCH_NORMALIZED)
                                .append(" like ?");
                    }
                    searchParams[i] = "%" + searchParams[i] + "%";
                }

                cursor = database.rawQuery(query.toString(), searchParams);

                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (cursor != null)
        {
            cursor.setNotificationUri(this.getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }


    public static boolean CheckIsDataAlreadyInDBorNot(SQLiteDatabase sqldb,String TableName,
                                                      String dbfield, String fieldValue) {

        String Query = "Select * from " + TableName + " where " + dbfield + " = '" + fieldValue+"'";
        Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;

    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        int insertedRows = 0;
        ITPQueryHelper inserter;

        switch (uriMatcher.match(uri))
        {
            case VIDEOS:

                inserter = new ITPQueryHelper(this.database, VideoTable.TABLE_NAME);


                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        if(!CheckIsDataAlreadyInDBorNot(this.database,VideoTable.TABLE_NAME,VideoTable.COLUMN_ID,contentValues.getAsString(VideoTable.COLUMN_ID)))
                        {
                            inserter.prepareForInsert();
                            inserter.bind(VideoTable.COLUMN_ID, contentValues.getAsString(VideoTable.COLUMN_ID));
                            inserter.bind(VideoTable.COLUMN_LINK, contentValues.getAsString(VideoTable.COLUMN_LINK));
                            inserter.bind(VideoTable.COLUMN_TITLE, contentValues.getAsString(VideoTable.COLUMN_TITLE));
                            inserter.bind(VideoTable.COLUMN_DATE, contentValues.getAsString(VideoTable.COLUMN_DATE));
                            inserter.bind(VideoTable.COLUMN_PICTURE_URL, contentValues.getAsString(VideoTable.COLUMN_PICTURE_URL));
                            inserter.bind(VideoTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(VideoTable.COLUMN_SEARCH_NORMALIZED));

                            inserter.bind(VideoTable.COLUMN_TYPE, WallItem.Type.VIDEO.ordinal());


                            long rowId = inserter.execute();

                            if (rowId != -1)
                            {
                                insertedRows++;
                            }
                        }


                    }

                    this.database.setTransactionSuccessful();
                } catch (Exception e)
                {
                    Log.d(this.getClass().getSimpleName(), e.getMessage());

                } finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);

                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI, null);
                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI_UPDATES, null);

                    this.getContext().getContentResolver().notifyChange(VideoContentProvider.CONTENT_URI_UPDATES, null);
                }



                return insertedRows;



            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        return null;
    }
}
