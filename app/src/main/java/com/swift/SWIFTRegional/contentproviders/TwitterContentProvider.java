package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.TweetsTable;

import java.util.ArrayList;
import java.util.List;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.VideoTable;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

import static com.swift.SWIFTRegional.contentproviders.VideoContentProvider.CheckIsDataAlreadyInDBorNot;

public class TwitterContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.TwitterContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_TYPE = Uri.parse("content://" + PROVIDER_NAME + "/type/");
    public static final Uri CONTENT_URI_UPDATES = Uri.parse("content://" + PROVIDER_NAME + "/updates/");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search/");

    private static final int TWEETS = 1;
    private static final int TWEET = 2;
    private static final int TWEETS_TYPE = 3;
    private static final int TWEETS_UPDATES = 4;
    private static final int TWEETS_SEARCH = 5;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, TWEETS);
        uriMatcher.addURI(PROVIDER_NAME, "#", TWEET);
        uriMatcher.addURI(PROVIDER_NAME, "type/#", TWEETS_TYPE);
        uriMatcher.addURI(PROVIDER_NAME, "updates", TWEETS_UPDATES);
        uriMatcher.addURI(PROVIDER_NAME, "search/#/*", TWEETS_SEARCH);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);

        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case TWEETS:
            case TWEETS_TYPE:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case TWEET:
            case TWEETS_UPDATES:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        return null;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case TWEETS:

                return this.database.delete(TweetsTable.TABLE_NAME,null,null);
        }

        return 0;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;

        switch (uriMatcher.match(uri))
        {
            case TWEET:

                final String id = uri.getLastPathSegment();
                cursor = this.database.query(TweetsTable.TABLE_NAME, null, TweetsTable.COLUMN_ID + " = ?", new String[] { id }, null, null, null);

                break;

            case TWEETS_TYPE:

                final String type = uri.getLastPathSegment();
                cursor = this.database.query(TweetsTable.TABLE_NAME, null, TweetsTable.COLUMN_TYPE + " = ?", new String[] { type }, null, null, TweetsTable.COLUMN_DATE + " DESC");

                break;

            case TWEETS_UPDATES:

                final long lastUpdate = UpdatePreferencesHelper.getLastUpdate(this.getContext(), UpdatePreferencesHelper.LastSeenType.TWITTER);

                cursor = this.database.query(TweetsTable.TABLE_NAME, new String[] { "COUNT(1) AS count" }, TweetsTable.COLUMN_DATE + " > ?", new String [] { String.valueOf(lastUpdate) } , null, null, null);

                break;

            case TWEETS_SEARCH:
                List<String> segments = uri.getPathSegments();
                if(segments.size() >= 3){
                    String tweetType = segments.get(1);
                    String searchText = segments.get(2);

                    StringBuilder query = new StringBuilder("SELECT ")
                            .append("_id, ")
                            .append(TweetsTable.COLUMN_ID)
                            .append(", ")
                            .append(TweetsTable.COLUMN_DATE)
                            .append(", ")
                            .append(TweetsTable.COLUMN_DESCRIPTION)
                            .append(", ")
                            .append(TweetsTable.COLUMN_PICTURE_URL)
                            .append(", ")
                            .append(TweetsTable.COLUMN_TYPE)
                            .append(", ")
                            .append(TweetsTable.COLUMN_TITLE)
                            .append(" FROM ")
                            .append(TweetsTable.TABLE_NAME)
                            .append(" where ")
                            .append(TweetsTable.COLUMN_TYPE)
                            .append(" = ? ");


                    List<String> params = new ArrayList<>();
                    query.append(" and ").append(TweetsTable.COLUMN_SEARCH_NORMALIZED).append(" LIKE ? ");
                    params.add(tweetType);
                    String[] searchArgs = searchText.split(" ");
                    for (int i = 0; i < searchArgs.length; i++) {
                        if (i >= 1) {
                            query.append(" AND ").append(TweetsTable.COLUMN_SEARCH_NORMALIZED).append(" LIKE ? ");
                        }
                        params.add("%" + searchArgs[i] + "%");
                    }

                    cursor = this.database.rawQuery(query.toString(), params.toArray(new String[0]));

                } else {
                    cursor = null;
                }
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if(cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case TWEETS_TYPE:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, TweetsTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];
                        if(!CheckIsDataAlreadyInDBorNot(this.database, TweetsTable.TABLE_NAME,TweetsTable.COLUMN_ID,contentValues.getAsString(TweetsTable.COLUMN_ID))) {

                            inserter.prepareForInsert();
                            inserter.bind(TweetsTable.COLUMN_ID, contentValues.getAsLong(TweetsTable.COLUMN_ID));
                            inserter.bind(TweetsTable.COLUMN_DATE, contentValues.getAsString(TweetsTable.COLUMN_DATE));
                            inserter.bind(TweetsTable.COLUMN_DESCRIPTION, contentValues.getAsString(TweetsTable.COLUMN_DESCRIPTION));
                            inserter.bind(TweetsTable.COLUMN_TITLE, contentValues.getAsString(TweetsTable.COLUMN_TITLE));
                            inserter.bind(TweetsTable.COLUMN_PICTURE_URL, contentValues.getAsString(TweetsTable.COLUMN_PICTURE_URL));
                            inserter.bind(TweetsTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(TweetsTable.COLUMN_SEARCH_NORMALIZED));

                            inserter.bind(TweetsTable.COLUMN_TYPE, Integer.parseInt(uri.getLastPathSegment()));

                            long rowId = inserter.execute();

                            if (rowId != -1) {
                                insertedRows++;
                            }
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d(e.getClass().getSimpleName(), e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);

                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI, null);

                    if(Integer.parseInt(uri.getLastPathSegment()) == WallItem.Type.TWITTER_NAME.ordinal())
                    {
                        this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI_UPDATES, null);
                    }

                    final Uri updateUri = Uri.withAppendedPath(TwitterContentProvider.CONTENT_URI_UPDATES, uri.getLastPathSegment());

                    this.getContext().getContentResolver().notifyChange(updateUri, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
