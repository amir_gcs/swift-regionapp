package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.models.WallItem;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.IssuesTable;

public class IssuesContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.IssuesContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");

    private static final int ISSUES = 1;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, ISSUES);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case ISSUES:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case ISSUES:

                return this.database.delete(IssuesTable.TABLE_NAME, selection, selectionArgs);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;

        switch (uriMatcher.match(uri))
        {
            case ISSUES:

                cursor = this.database.query(IssuesTable.TABLE_NAME, projection, selection, selectionArgs, null, null, IssuesTable.COLUMN_DATE + " DESC");

                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case ISSUES:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, IssuesTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();

                        inserter.bind(IssuesTable.COLUMN_ID, String.valueOf(i));
                        inserter.bind(IssuesTable.COLUMN_TITLE, contentValues.getAsString(IssuesTable.COLUMN_TITLE));
                        inserter.bind(IssuesTable.COLUMN_DESCRIPTION, contentValues.getAsString(IssuesTable.COLUMN_DESCRIPTION));
                        inserter.bind(IssuesTable.COLUMN_DATE, contentValues.getAsLong(IssuesTable.COLUMN_DATE));
                        inserter.bind(IssuesTable.COLUMN_PICTURE_URL, contentValues.getAsString(IssuesTable.COLUMN_PICTURE_URL));
                        inserter.bind(IssuesTable.COLUMN_LINK_URL, contentValues.getAsString(IssuesTable.COLUMN_LINK_URL));
                        inserter.bind(IssuesTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(IssuesTable.COLUMN_SEARCH_NORMALIZED));

                        inserter.bind(IssuesTable.COLUMN_TYPE, WallItem.Type.ISSUE.ordinal());

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                } catch (Exception e)
                {
                    Log.d("IssueContentProvider", e.getMessage());
                } finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);

                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI, null);

                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI_UPDATES, null);
                    this.getContext().getContentResolver().notifyChange(NewsContentProvider.CONTENT_URI_UPDATES, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
