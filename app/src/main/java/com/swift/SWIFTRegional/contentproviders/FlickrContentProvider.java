package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;
import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.FlickrPhotosTable;
import com.swift.SWIFTRegional.database.tables.FlickrPhotosetsTable;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

public class FlickrContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.FlickrContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_PHOTOSETS = Uri.parse("content://" + PROVIDER_NAME + "/photoset");
    public static final Uri CONTENT_URI_PHOTO = Uri.parse("content://" + PROVIDER_NAME + "/photos");

    public static final Uri CONTENT_URI_UPDATES = Uri.parse("content://" + PROVIDER_NAME + "/photoset/updates/");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    private static final int PHOTOSETS = 1;
    private static final int PHOTOSET = 2;
    private static final int PHOTOS = 3;
    private static final int PHOTOSETS_UPDATES = 4;
    private static final int QUERY_SEARCH = 5;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "photoset", PHOTOSETS);
        uriMatcher.addURI(PROVIDER_NAME, "photoset/updates", PHOTOSETS_UPDATES);
        uriMatcher.addURI(PROVIDER_NAME, "photoset/*", PHOTOSET);
        uriMatcher.addURI(PROVIDER_NAME, "photos/*", PHOTOS);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", QUERY_SEARCH);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case PHOTOSETS:
            case PHOTOS:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case PHOTOSET:
            case PHOTOSETS_UPDATES:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;

        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case PHOTOSETS:

                return this.database.delete(FlickrPhotosetsTable.TABLE_NAME, null, null);

            case PHOTOS:

                final String photosetId = uri.getLastPathSegment();
                return this.database.delete(FlickrPhotosTable.TABLE_NAME, FlickrPhotosTable.COLUMN_PHOTOSET_ID + " = ?", new String[] { photosetId });
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;
        switch (uriMatcher.match(uri))
        {
            case PHOTOSETS:

                cursor = this.database.query(FlickrPhotosetsTable.TABLE_NAME, null, null, null, null, null, FlickrPhotosetsTable.COLUMN_DATE + " DESC");

                break;

            case PHOTOSETS_UPDATES:

                final long lastUpdate = UpdatePreferencesHelper.getLastUpdate(this.getContext(), UpdatePreferencesHelper.LastSeenType.FLICKR);

                cursor = this.database.query(FlickrPhotosetsTable.TABLE_NAME, new String[] { "count(1) as count" }, FlickrPhotosetsTable.COLUMN_DATE + " > ?", new String[] { String.valueOf(lastUpdate) }, null, null, null);

                break;

            case PHOTOS:

                final String photosetId = uri.getLastPathSegment();
                cursor = this.database.query(FlickrPhotosTable.TABLE_NAME, null, FlickrPhotosTable.COLUMN_PHOTOSET_ID + " = ?", new String[] { photosetId }, null, null, null);

                break;

            case QUERY_SEARCH:

                String[] searchParams = uri.getLastPathSegment().split(" ");

                StringBuilder query = new StringBuilder("Select * from ")
                    .append(FlickrPhotosetsTable.TABLE_NAME)
                    .append(" where ").append(FlickrPhotosetsTable.COLUMN_SEARCH_NORMALIZED)
                    .append(" like ?");

                for (Integer i = 0;  i< searchParams.length; i++) {
                    if(i >= 1) {
                        query.append(" and ").append(FlickrPhotosetsTable.COLUMN_SEARCH_NORMALIZED)
                                .append(" like ?");
                    }
                    searchParams[i] = "%" + searchParams[i] + "%";
                }

                cursor = database.rawQuery(query.toString(), searchParams);
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if(cursor != null)
        {
            cursor.setNotificationUri(this.getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        int insertedRows = 0;
        ITPQueryHelper inserter;

        switch (uriMatcher.match(uri))
        {
            case PHOTOSETS:

                inserter = new ITPQueryHelper(this.database, FlickrPhotosetsTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(FlickrPhotosetsTable.COLUMN_ID, contentValues.getAsString(FlickrPhotosetsTable.COLUMN_ID));
                        inserter.bind(FlickrPhotosetsTable.COLUMN_TITLE, contentValues.getAsString(FlickrPhotosetsTable.COLUMN_TITLE));
                        inserter.bind(FlickrPhotosetsTable.COLUMN_DESCRIPTION, contentValues.getAsString(FlickrPhotosetsTable.COLUMN_DESCRIPTION));
                        inserter.bind(FlickrPhotosetsTable.COLUMN_DATE, contentValues.getAsString(FlickrPhotosetsTable.COLUMN_DATE));
                        inserter.bind(FlickrPhotosetsTable.COLUMN_PICTURE_URL, contentValues.getAsString(FlickrPhotosetsTable.COLUMN_PICTURE_URL));
                        inserter.bind(FlickrPhotosetsTable.COLUMN_SEARCH_NORMALIZED,
                                contentValues.getAsString(FlickrPhotosetsTable.COLUMN_SEARCH_NORMALIZED));
                        inserter.bind(FlickrPhotosetsTable.COLUMN_TYPE, WallItem.Type.FLICKR_PHOTOSET.ordinal());

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                } catch (Exception e)
                {
                    Log.d(this.getClass().getSimpleName(), e.getMessage());
                } finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);

                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI, null);
                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI_UPDATES, null);

                    this.getContext().getContentResolver().notifyChange(FlickrContentProvider.CONTENT_URI_UPDATES, null);
                }

                return insertedRows;

            case PHOTOS:

                inserter = new ITPQueryHelper(this.database, FlickrPhotosTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();

                        final String photosetId = uri.getLastPathSegment();
                        inserter.bind(FlickrPhotosTable.COLUMN_PHOTOSET_ID, photosetId);
                        inserter.bind(FlickrPhotosTable.COLUMN_PICTURE_URL_LARGE, contentValues.getAsString(FlickrPhotosTable.COLUMN_PICTURE_URL_LARGE));
                        inserter.bind(FlickrPhotosTable.COLUMN_PICTURE_URL_SMALL, contentValues.getAsString(FlickrPhotosTable.COLUMN_PICTURE_URL_SMALL));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                } catch (Exception e)
                {
                    Log.d(this.getClass().getSimpleName(), e.getMessage());
                } finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        return null;
    }
}
