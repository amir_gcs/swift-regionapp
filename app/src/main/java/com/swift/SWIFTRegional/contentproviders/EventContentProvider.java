package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.EventTable;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

public class EventContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.SessionContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_SESSIONS = Uri.parse("content://" + PROVIDER_NAME + "/sessions");
    public static final Uri CONTENT_URI_EXHIBITOR_EVENTS = Uri.parse("content://" + PROVIDER_NAME + "/exhibitor_events");
    public static final Uri CONTENT_URI_PRIVATE_SESSIONS = Uri.parse("content://" + PROVIDER_NAME + "/private_sessions");

    private static final int SESSIONS = 1;
    private static final int EXHIBITOR_EVENTS = 2;
    private static final int PRIVATE_SESSIONS = 3;

    private static final int EVENT_TYPE_SESSIONS = 0;
    private static final int EVENT_TYPE_EXHIBITOR_EVENT = 1;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "sessions", SESSIONS);
        uriMatcher.addURI(PROVIDER_NAME, "exhibitor_events", EXHIBITOR_EVENTS);
        uriMatcher.addURI(PROVIDER_NAME, "private_sessions", PRIVATE_SESSIONS);

    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case SESSIONS:
            case EXHIBITOR_EVENTS:
            case PRIVATE_SESSIONS:
                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(EventTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case SESSIONS:

                final String sessionWhereClause = EventTable.COLUMN_EVENT_TYPE_ID + "=" + EVENT_TYPE_SESSIONS;

                return this.database.delete(EventTable.TABLE_NAME, sessionWhereClause, null);

            case EXHIBITOR_EVENTS:

                final String exhibitorEventWhereClause = EventTable.COLUMN_EVENT_TYPE_ID + "=" + EVENT_TYPE_EXHIBITOR_EVENT;

                return this.database.delete(EventTable.TABLE_NAME, exhibitorEventWhereClause, null);

            case PRIVATE_SESSIONS:

                final String privateEventWhereClause = EventTable.COLUMN_SESSION_TYPE + "=" + "'PRIVATE'";

                return this.database.delete(EventTable.TABLE_NAME, privateEventWhereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        // Querying of events/sessions is done using the EventTimingContentProvider
        return null;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case SESSIONS:
            case PRIVATE_SESSIONS:

                return this.insert(uri, EVENT_TYPE_SESSIONS, values);

            case EXHIBITOR_EVENTS:

                return this.insert(uri, EVENT_TYPE_EXHIBITOR_EVENT, values);

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    private int insert(Uri uri, int eventType, ContentValues[] values)
    {
        int insertedRows = 0;
        ITPQueryHelper inserter = new ITPQueryHelper(this.database, EventTable.TABLE_NAME);


        this.database.beginTransaction();
        try
        {
            ContentValues contentValues;

            for (int i = 0; i < values.length; i++)
            {
                contentValues = values[i];

                inserter.prepareForInsert();

                inserter.bind(EventTable.COLUMN_SESSION_TYPE, contentValues.getAsString(EventTable.COLUMN_SESSION_TYPE));
                inserter.bind(EventTable.COLUMN_EVENT_ID, contentValues.getAsString(EventTable.COLUMN_EVENT_ID));
                inserter.bind(EventTable.COLUMN_TITLE, contentValues.getAsString(EventTable.COLUMN_TITLE));
                inserter.bind(EventTable.COLUMN_DESCRIPTION, contentValues.getAsString(EventTable.COLUMN_DESCRIPTION));
                inserter.bind(EventTable.COLUMN_TYPEID, contentValues.getAsString(EventTable.COLUMN_TYPEID));
                inserter.bind(EventTable.COLUMN_TYPE, contentValues.getAsString(EventTable.COLUMN_TYPE));
                inserter.bind(EventTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(EventTable.COLUMN_SEARCH_NORMALIZED));
                inserter.bind(EventTable.COLUMN_EVENT_TYPE_ID, eventType);

                if(eventType == EVENT_TYPE_SESSIONS)
                {
                    inserter.bind(EventTable.COLUMN_SESSION_ID, contentValues.getAsString(EventTable.COLUMN_SESSION_ID));
                    inserter.bind(EventTable.COLUMN_STREAMS, contentValues.getAsString(EventTable.COLUMN_STREAMS));
                    inserter.bind(EventTable.COLUMN_TAGS, contentValues.getAsString(EventTable.COLUMN_TAGS));
                    inserter.bind(EventTable.COLUMN_YOUTUBE_ID, contentValues.getAsString(EventTable.COLUMN_YOUTUBE_ID));
                    inserter.bind(EventTable.COLUMN_EXHIBITOR_CCVK, contentValues.getAsString(EventTable.COLUMN_EXHIBITOR_CCVK));
                }
                else if(eventType == EVENT_TYPE_EXHIBITOR_EVENT)
                {
                    inserter.bind(EventTable.COLUMN_CCVK, contentValues.getAsString(EventTable.COLUMN_CCVK));
                }

                long rowId = inserter.execute();

                if (rowId != -1)
                {
                    insertedRows++;
                }
            }
            this.database.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.d("SpeakerContentProvider", e.getMessage());
        }
        finally
        {
            this.database.endTransaction();
            inserter.close();
        }

        if (insertedRows > 0)
        {
            this.getContext().getContentResolver().notifyChange(uri, null);
            this.getContext().getContentResolver().notifyChange(EventContentProvider.CONTENT_URI, null);
        }

        return insertedRows;
    }
}
