package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.CategoryTable;
import com.swift.SWIFTRegional.database.tables.EventTable;
import com.swift.SWIFTRegional.database.tables.EventTimingTable;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.database.tables.SessionSpeakerTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.util.List;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;


public class EventTimingContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.TimingContentProvider";

    /**
     * Filter values.
     * These values determine which sessions/events will be shown when selecting a programme filter.
     */


    // Exhibitor events.
    // These events do not have a category ID in the api, so a fixed type_id is assigned to every exhibitor event when it is saved to the database.
    // The same value is then used to retrieve these exhibitor events again. (See CATEGORY_IDS_EXHIBITOR).
    // This value can be changed if it conflicts with a real type id from the API.
    public static final String CATEGORY_ID_EXHIBITOR = "999";

    // Two categories included in the Exhibitor Events filter
    public static final String CATEGORY_ID_OPENTHEATRE = "134";
    public static final String CATEGORY_ID_SWIFTINSTITUTE = "137";

    // Conference programme
    public static final String CATEGORY_ID_CONFERENCE = "126";
    // Tv programmes/sessions
    public static final String CATEGORY_ID_SIBOSTV = "131";
    public static final String CATEGORY_ID_TV = "132";

    // Ids for the "Conference Programmes" filter.
    private static final String[] CATEGORY_IDS_CONFERENCE = new String[]{CATEGORY_ID_CONFERENCE};
    // Ids for the "TV Programmes" filter.
    private static final String[] CATEGORY_IDS_TV = new String[]{CATEGORY_ID_SIBOSTV, CATEGORY_ID_TV};
    // Ids for the "Exhibitor Events" filter.
    // This filter will only show exhibitor events if the user is logged in and will always show the two others.
    private static final String[] CATEGORY_IDS_EXHIBITOR = new String[]{CATEGORY_ID_EXHIBITOR, CATEGORY_ID_OPENTHEATRE, CATEGORY_ID_SWIFTINSTITUTE};

    // Streams directly use the title and ID from the API to create a filter and filter the content.

    /**
     * End of filter values
     */


    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_TIMING = Uri.parse("content://" + PROVIDER_NAME + "/timing");
    public static final Uri CONTENT_URI_FAVOURITES = Uri.parse("content://" + PROVIDER_NAME + "/favourites");
    public static final Uri CONTENT_URI_SUGGESTED = Uri.parse("content://" + PROVIDER_NAME + "/suggested");

    public static final Uri CONTENT_URI_CONFERENCE = Uri.parse("content://" + PROVIDER_NAME + "/conference");
    public static final Uri CONTENT_URI_TV = Uri.parse("content://" + PROVIDER_NAME + "/tv");

    public static final Uri CONTENT_URI_SPEAKER = Uri.parse("content://" + PROVIDER_NAME + "/speaker");

    public static final Uri CONTENT_URI_EXHIBITOR = Uri.parse("content://" + PROVIDER_NAME + "/exhibitor");

    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    public static final Uri CONTENT_URI_CCVK = Uri.parse("content://" + PROVIDER_NAME + "/ccvk");

    public static final Uri CONTENT_URI_STREAM = Uri.parse("content://" + PROVIDER_NAME + "/stream");

    public static final Uri CONTENT_URI_TAG = Uri.parse("content://" + PROVIDER_NAME + "/tag");
    // Don't use this Uri directly, convert it to a string and insert your parameters
    public static final Uri CONTENT_URI_STREAM_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/stream/{0}/search");

    public static final Uri CONTENT_URI_TAG_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/tag/{0}/search");

    public static final Uri CONTENT_URI_SESSIONS = Uri.parse("content://" + PROVIDER_NAME + "/sessions");
    public static final Uri CONTENT_URI_EXHIBITOR_EVENTS = Uri.parse("content://" + PROVIDER_NAME + "/exhibitor_events");
    public static final Uri CONTENT_URI_PRIVATE_SESSIONS = Uri.parse("content://" + PROVIDER_NAME + "/private_sessions");

    public static final Uri CONTENT_URI_EXHIBITOR_SESSIONS = Uri.parse("content://" + PROVIDER_NAME + "/exhibitor_sessions");

    private static final int EVENT_TYPE_SESSION = 0;
    private static final int EVENT_TYPE_EXHIBITOR_EVENT = 1;
    private static final int EVENT_TYPE_PRIVATE_SESSION = 2;

    private static final int TIMINGS = 1;
    private static final int TIMING_ID = 2;
    private static final int FAVOURITES = 3;
    private static final int FAVOURITES_SEARCH = 4;
    private static final int SUGGESTED = 5;
    private static final int SUGGESTED_SEARCH = 6;

    private static final int SESSIONS_CONFERENCE = 7;
    private static final int CONFERENCE_SEARCH = 8;

    private static final int SESSIONS_TV = 9;
    private static final int SESSIONS_TV_SEARCH = 10;

    private static final int SESSIONS_EXHIBITOR = 11;
    private static final int SESSIONS_EXHIBITOR_SEARCH = 12;

    private static final int SEARCH_QUERY = 13;

    private static final int EXHIBITOR_EVENT = 14;
    private static final int STREAM = 15;
    private static final int STREAM_SEARCH = 16;

    private static final int SPEAKER = 17;
    private static final int SESSIONS = 18;
    private static final int EXHIBITOR_EVENTS = 19;
    private static final int PRIVATE_SESSIONS = 20;

    private static final int TAG = 21;
    private static final int TAG_SEARCH = 22;

    private static final int EXHIBITOR_SESSIONS = 23;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, TIMINGS);
        uriMatcher.addURI(PROVIDER_NAME, "timing/#", TIMING_ID);
        uriMatcher.addURI(PROVIDER_NAME, "favourites", FAVOURITES);
        uriMatcher.addURI(PROVIDER_NAME, "favourites/*", FAVOURITES_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "suggested", SUGGESTED);
        uriMatcher.addURI(PROVIDER_NAME, "suggested/*", SUGGESTED_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "conference", SESSIONS_CONFERENCE);
        uriMatcher.addURI(PROVIDER_NAME, "conference/*", CONFERENCE_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "tv", SESSIONS_TV);
        uriMatcher.addURI(PROVIDER_NAME, "tv/*", SESSIONS_TV_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "exhibitor", SESSIONS_EXHIBITOR);
        uriMatcher.addURI(PROVIDER_NAME, "exhibitor/*", SESSIONS_EXHIBITOR_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "stream/#", STREAM);
        uriMatcher.addURI(PROVIDER_NAME, "stream/#/search/*", STREAM_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "tag/#", TAG);
        uriMatcher.addURI(PROVIDER_NAME, "tag/#/search/*", TAG_SEARCH);

        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH_QUERY);
        uriMatcher.addURI(PROVIDER_NAME, "speaker/*", SPEAKER);

        uriMatcher.addURI(PROVIDER_NAME, "ccvk/*", EXHIBITOR_EVENT);

        uriMatcher.addURI(PROVIDER_NAME, "exhibitor_events", EXHIBITOR_EVENTS);
        uriMatcher.addURI(PROVIDER_NAME, "sessions", SESSIONS);
        uriMatcher.addURI(PROVIDER_NAME, "private_sessions", PRIVATE_SESSIONS);

        uriMatcher.addURI(PROVIDER_NAME, "exhibitor_sessions/*", EXHIBITOR_SESSIONS);

    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case TIMINGS:
            case FAVOURITES:
            case FAVOURITES_SEARCH:
            case SUGGESTED:
            case SUGGESTED_SEARCH:
            case SESSIONS_CONFERENCE:
            case SESSIONS_TV:
            case SPEAKER:
            case SESSIONS_EXHIBITOR:
            case EXHIBITOR_EVENTS:
            case PRIVATE_SESSIONS:
            case SESSIONS:
            case STREAM:
            case TAG:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case TIMING_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(EventTimingTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case TIMINGS:
            case SESSIONS_TV:

                return this.database.delete(EventTimingTable.TABLE_NAME, selection, null);

            case EXHIBITOR_EVENTS:

                final String exhibitorEventWhereClause = EventTimingTable.COLUMN_EVENT_TYPE + "=" + EVENT_TYPE_EXHIBITOR_EVENT;
                return this.database.delete(EventTimingTable.TABLE_NAME, exhibitorEventWhereClause, null);

            case SESSIONS:

                final String sessionWhereClause = EventTimingTable.COLUMN_EVENT_TYPE + "=" + EVENT_TYPE_SESSION;
                return this.database.delete(EventTimingTable.TABLE_NAME, sessionWhereClause, null);

            case PRIVATE_SESSIONS:

                final String privateSessionWhereClause = EventTimingTable.COLUMN_EVENT_TYPE + "=" + EVENT_TYPE_PRIVATE_SESSION;
                return this.database.delete(EventTimingTable.TABLE_NAME, privateSessionWhereClause, null);

            case TIMING_ID:

                final String whereClause = EventTimingTable.COLUMN_EVENT_ID + "='" + uri.getLastPathSegment() + "' ";
                return this.database.delete(EventTimingTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;
        String fullQuery = "";
        String whereOptions = "";
        String joinOptions = "";
        switch (uriMatcher.match(uri))
        {


            case FAVOURITES_SEARCH:

                whereOptions = this.createRemainingSearchOptionsFromUriPath(uri);

                // Fall through

            case FAVOURITES:

                String favoWhereOptions = " WHERE " + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_FAVORITE_ID + " <> 0";

                fullQuery = constructMainQuery(favoWhereOptions + whereOptions, joinOptions);

                break;

            case SPEAKER:
                String speakerId = uri.getLastPathSegment();

                whereOptions = " WHERE " + SessionSpeakerTable.COLUMN_SPEAKER_ID + " = " + speakerId;
                joinOptions = " LEFT JOIN " + SessionSpeakerTable.TABLE_NAME + " " + SessionSpeakerTable.TABLE_PREFIX
                        + " ON "
                        + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_EVENT_ID
                        + "="
                        + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SESSION_ID;

                fullQuery = constructMainQuery(whereOptions, joinOptions);

                break;

            case SEARCH_QUERY:

                this.database.execSQL("update events set searchNormalized=(select categoryname from categories where categoryid=events.typeid limit 1)||\" \"||searchNormalized where searchNormalized not like (select categoryname from categories where categoryid=events.typeid limit 1)||\"%\"");
                String[] searchArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");

                if (searchArgs.length > 0)
                {
                    whereOptions = " WHERE " + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_SEARCH_NORMALIZED + " LIKE '%" + searchArgs[0] + "%' ";
                    whereOptions += createRemainingSearchOptionsFromUriPath(uri);
                }

                // Fall through

            case TIMINGS:
                fullQuery = this.constructMainQuery(whereOptions, joinOptions);
                break;

            case CONFERENCE_SEARCH:

                whereOptions = this.createRemainingSearchOptionsFromUriPath(uri);

                // Fall through

            case SESSIONS_CONFERENCE:

                // Conference filter also includes the private sessions.
                String conferenceOptions = this.createWhereClauseForFilter(CATEGORY_IDS_CONFERENCE, new String[]{"PRIVATE"});

                fullQuery = this.constructMainQuery(conferenceOptions + whereOptions, joinOptions);
                break;

            case SESSIONS_TV_SEARCH:

                whereOptions = this.createRemainingSearchOptionsFromUriPath(uri);

                // Fall through

            case SESSIONS_TV:

                String tvOptions = this.createWhereClauseForFilter(CATEGORY_IDS_TV);

                fullQuery = this.constructMainQuery(tvOptions + whereOptions, joinOptions);
                break;

            case SESSIONS_EXHIBITOR_SEARCH:

                whereOptions = this.createRemainingSearchOptionsFromUriPath(uri);

                // Fall through

            case SESSIONS_EXHIBITOR:
                String exhibitorOptions = this.createWhereClauseForFilter(CATEGORY_IDS_EXHIBITOR);

                fullQuery = this.constructMainQuery(exhibitorOptions + whereOptions, joinOptions);
                break;

            case EXHIBITOR_EVENT:
                String exhibitorEventOptions = " WHERE "
                        + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_CCVK
                        + "="
                        + "'" + uri.getLastPathSegment() + "'";

                fullQuery = this.constructMainQuery(exhibitorEventOptions, joinOptions);
                break;

            case TIMING_ID:

                String options = " WHERE "
                        + "(" + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_EVENT_ID
                        + "="
                        + "" + uri.getLastPathSegment() + ")";

                fullQuery = this.constructMainQuery(options, joinOptions);

                break;

            case STREAM_SEARCH:

                // Get the searchcategory from the Uri, it's not the last path segment so retrieve it manually
                List<String> pathSegments = uri.getPathSegments();
                String category = pathSegments.get(pathSegments.size() - 3);
                String streamWhereOptions = " WHERE "
                        + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_STREAMS
                        + " LIKE '%" + category + "%'";

                whereOptions = this.createRemainingSearchOptionsFromUriPath(uri);

                fullQuery = this.constructMainQuery(streamWhereOptions + whereOptions, null);

                break;

            case STREAM:

                String streamOptions = " WHERE "
                        + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_STREAMS
                        + " LIKE '%" + uri.getLastPathSegment() + "%'";

                fullQuery = this.constructMainQuery(streamOptions, null);
                break;

            case TAG_SEARCH:

                // Get the searchcategory from the Uri, it's not the last path segment so retrieve it manually
                List<String> tagPathSegments = uri.getPathSegments();
                String tags = tagPathSegments.get(tagPathSegments.size() - 3);
                Log.d("path segment",tags+"");
                String tagWhereOptions = " WHERE "
                        + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TAGS
                        + " LIKE '%" + tags + "%'";

                whereOptions = this.createRemainingSearchOptionsFromUriPath(uri);

                fullQuery = this.constructMainQuery(tagWhereOptions + whereOptions, null);
                break;

            case TAG:

                String tagOptions = " WHERE "
                        + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TAGS
                        + " LIKE '%" + uri.getLastPathSegment() + "%'";

                fullQuery = this.constructMainQuery(tagOptions, null);
                break;

            case EXHIBITOR_SESSIONS:

                StringBuilder whereClause = new StringBuilder(" WHERE ")
                        .append(EventTable.TABLE_PREFIX).append(".").append(EventTable.COLUMN_EXHIBITOR_CCVK)
                        .append(" LIKE '%").append(uri.getLastPathSegment()).append("%'");

                fullQuery = this.constructMainQuery(whereClause.toString(), null);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);

        }

        cursor = this.database.rawQuery(fullQuery, null);

        if (cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    /**
     * Creates a query that will return sessions that match any of the supplied type ids.
     */
    private String createWhereClauseForFilter(String[] ids)
    {
        return this.createWhereClauseForFilter(ids, null);
    }

    /**
     * Creates a query that will return sessions that match any of the supplied type ids or any of the session types.
     *
     * @param ids   The type id the sessions must belong to. Must only belong to one.
     * @param types The session type the session must be. Must only match one. Optional.
     */
    private String createWhereClauseForFilter(String[] ids, String[] types)
    {
        String whereClause = " WHERE (";

        for (int i = 0; i < ids.length; i++)
        {
            whereClause += EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TYPE
                    + " = "
                    + "'" + ids[i] + "'";

            if (i < ids.length - 1)
            {
                whereClause += " OR ";
            }
        }

        if (types != null && types.length > 0)
        {
            whereClause += " OR ";

            for (int i = 0; i < types.length; i++)
            {
                whereClause += EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_SESSION_TYPE
                        + " = "
                        + "'" + types[i] + "'";

                if (i < types.length - 1)
                {
                    whereClause += " OR ";
                }
            }
        }
        whereClause += ")";

        return whereClause;
    }

    /**
     * Create additional search options if there are more than one.
     * The first one needs to include the WHERE keyword (but not always) and is therefore done outside of this method.
     */
    private String createRemainingSearchOptionsFromUriPath(Uri uri)
    {
        String[] searchArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");

        String searchOptions = "";

        for (int i = 0; i < searchArgs.length; i++)
        {
            searchOptions += " AND " + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_SEARCH_NORMALIZED + " LIKE '%" + searchArgs[i] + "%' ";
        }

        return searchOptions;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case SESSIONS:

                return insert(uri, EVENT_TYPE_SESSION, values);

            case EXHIBITOR_EVENTS:

                return insert(uri, EVENT_TYPE_EXHIBITOR_EVENT, values);

            case PRIVATE_SESSIONS:

                return insert(uri, EVENT_TYPE_PRIVATE_SESSION, values);

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    private int insert(Uri uri, int eventType, ContentValues[] values)
    {
        int insertedRows = 0;
        ITPQueryHelper inserter = new ITPQueryHelper(this.database, EventTimingTable.TABLE_NAME);

        this.database.beginTransaction();
        try
        {
            ContentValues contentValues;

            for (int i = 0; i < values.length; i++)
            {
                contentValues = values[i];

                inserter.prepareForInsert();
                inserter.bind(EventTimingTable.COLUMN_ROOM_NAME, contentValues.getAsString(EventTimingTable.COLUMN_ROOM_NAME));
                inserter.bind(EventTimingTable.COLUMN_ROOM_CODE, contentValues.getAsString(EventTimingTable.COLUMN_ROOM_CODE));
                inserter.bind(EventTimingTable.COLUMN_START_TIME, contentValues.getAsString(EventTimingTable.COLUMN_START_TIME));
                inserter.bind(EventTimingTable.COLUMN_DATE, contentValues.getAsString(EventTimingTable.COLUMN_DATE));
                inserter.bind(EventTimingTable.COLUMN_END_TIME, contentValues.getAsString(EventTimingTable.COLUMN_END_TIME));
                inserter.bind(EventTimingTable.COLUMN_EVENT_ID, contentValues.getAsString(EventTimingTable.COLUMN_EVENT_ID));
                inserter.bind(EventTimingTable.COLUMN_EVENT_TYPE, eventType);
                inserter.bind(EventTimingTable.COLUMN_IS_REHEARSAL, contentValues.getAsBoolean(EventTimingTable.COLUMN_IS_REHEARSAL));

                long rowId = inserter.execute();

                if (rowId != -1)
                {
                    insertedRows++;
                }
            }

            this.database.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.d("TimingContentProvider", e.getMessage());
        }
        finally
        {
            this.database.endTransaction();
            inserter.close();
        }

        if (insertedRows > 0)
        {
            this.getContext().getContentResolver().notifyChange(uri, null);
            this.getContext().getContentResolver().notifyChange(EventTimingContentProvider.CONTENT_URI, null);
        }

        return insertedRows;
    }

    private String constructMainQuery(String whereOptions, String joinOptions)
    {
        String query = "SELECT " + EventTimingTable.TABLE_PREFIX + "._id, "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_ROOM_NAME + ", "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_ROOM_CODE + ", "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_START_TIME + ", "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_DATE + ", "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_END_TIME + ", "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_EVENT_ID + ", "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_EVENT_TYPE + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TITLE + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_DESCRIPTION + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TYPE + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TYPEID + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_YOUTUBE_ID + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_STREAMS + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_EVENT_ID + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TAGS + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_SESSION_TYPE + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_SESSION_ID + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN__ID + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_CCVK + ", "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_EVENT_TYPE_ID + ", "
                + CategoryTable.TABLE_PREFIX + "." + CategoryTable.COLUMN_NAME + ", "
                + CategoryTable.TABLE_PREFIX + "." + CategoryTable.COLUMN_TYPE + ", "
                + CategoryTable.TABLE_PREFIX + "." + CategoryTable.COLUMN_CATEGORY_ID + ", "
                + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_FAVORITE_ID;

        query = query
                + " FROM " + EventTimingTable.TABLE_NAME + " " + EventTimingTable.TABLE_PREFIX

                // Join events
                + " LEFT JOIN " + EventTable.TABLE_NAME + " " + EventTable.TABLE_PREFIX
                + " ON "
                + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_EVENT_ID
                + "="
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_SESSION_ID

                // Join Session Types
                + " LEFT JOIN " + CategoryTable.TABLE_NAME + " " + CategoryTable.TABLE_PREFIX
                + " ON "
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TYPEID
                + "="
                + CategoryTable.TABLE_PREFIX + "." + CategoryTable.COLUMN_CATEGORY_ID
                + " and "
                + CategoryTable.TABLE_PREFIX  + "." + CategoryTable.COLUMN_TYPE
                + " = 'SESSION' "

                // Join favourites table
                + " LEFT OUTER JOIN "
                + FavoriteTable.TABLE_NAME + " " + FavoriteTable.TABLE_PREFIX
                + " ON ("
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_SESSION_ID
                + " = "
                + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_FAVORITE_ID
                + " AND "
                + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_TYPE
                + " = "
                + FavoriteTable.FavouriteType.SESSION.ordinal()
                + ") OR ("
                + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_EVENT_ID
                + " = "
                + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_FAVORITE_ID
                + " AND "
                + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_TYPE
                + " = "
                + FavoriteTable.FavouriteType.EXHIBITOR_SESSION.ordinal()
                + ")";


        if (!TextUtils.isEmpty(joinOptions))
        {
            query += joinOptions;
        }

        if (!TextUtils.isEmpty(whereOptions))
        {
            query += whereOptions;
            // Never show rehearsals
            query += " AND " + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_IS_REHEARSAL + " = 0";
        }
        else
        {
            // Never show rehearsals
            query += " WHERE " + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_IS_REHEARSAL + " = 0";
        }

        query = query
                + " ORDER BY " + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_START_TIME
                + " , " + EventTimingTable.TABLE_PREFIX + "." + EventTimingTable.COLUMN_END_TIME
                + " ," + EventTable.TABLE_PREFIX + "." + EventTable.COLUMN_TITLE;

        return query;
    }

}
