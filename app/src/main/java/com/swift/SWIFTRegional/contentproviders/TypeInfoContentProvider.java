package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.MainEventTable;
import com.swift.SWIFTRegional.database.tables.RegionInfoTable;
import com.swift.SWIFTRegional.database.tables.TypeInfoTable;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.util.ArrayList;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

public class TypeInfoContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.TypeInfoContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_TYPEINFO = Uri.parse("content://" + PROVIDER_NAME + "/types");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    private static final int TYPES = 1;
    private static final int TYPE_ID = 2;
    private static final int SEARCH = 3;


    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, TYPES);
        uriMatcher.addURI(PROVIDER_NAME, "types/*", TYPE_ID);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case TYPES:
            case SEARCH:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case TYPE_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(TypeInfoTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case TYPES:

                return this.database.delete(TypeInfoTable.TABLE_NAME, selection, null);

            case TYPE_ID:

                String whereClause = TypeInfoTable.COLUMN_ID + "=" + uri.getLastPathSegment();
                return this.database.delete(TypeInfoTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String query;
        Cursor cursor;
        String searchText = null;

        switch (uriMatcher.match(uri))
        {
            case SEARCH:

                query = this.getBaseQuery()
                        + " WHERE "
                        + TypeInfoTable.COLUMN_NAME
                        + " LIKE ?";

                selectionArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");
                for (int i = 0; i < selectionArgs.length; i++)
                {
                    if (i >= 1)
                    {
                        query += " AND " + TypeInfoTable.COLUMN_NAME + " LIKE ? ";
                    }
                    selectionArgs[i] = "%" + selectionArgs[i] + "%";
                }

                cursor = this.database.rawQuery(query, selectionArgs);

                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;


            case TYPES:
                qb.setTables(TypeInfoTable.TABLE_NAME);
                Cursor mCount=this.database.rawQuery("select COUNT(*) from "+TypeInfoTable.TABLE_NAME,null);
                mCount.moveToFirst();
                int count= mCount.getInt(0);
                mCount.close();

                query = "";
                if(count>0) {
                    query = "select 0 as _id,-1 as type_id,'AAShow All' as name, 'https://static.pexels.com/photos/4097/city-cars-traffic-lights.jpeg' as pictureUrl,(select count(*) from " + MainEventTable.TABLE_NAME + ") as " + RegionInfoTable.COLUMN_EVENTCOUNT + " union ";
                }
                    query += this.getBaseQuery();

                query += " ORDER BY " + TypeInfoTable.COLUMN_NAME + " COLLATE NOCASE ASC";


                ArrayList<String> argsa = new ArrayList<String>();

                cursor = this.database.rawQuery(query, argsa.toArray(new String[argsa.size()]));
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case TYPE_ID:
                qb.setTables(TypeInfoTable.TABLE_NAME);
                qb.appendWhere(TypeInfoTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (TextUtils.isEmpty(sortOrder))
        {
            sortOrder = "UPPER(" + TypeInfoTable.COLUMN_NAME + ") ASC";
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    /**
     * Returns a query that will join with the favourites table to include the extra isFavourite field in the result.
     */
    private String getBaseQuery()
    {
        return "SELECT * ,(SELECT COUNT(*) FROM "+MainEventTable.TABLE_NAME+
               " WHERE "+ MainEventTable.TABLE_NAME+"."+ MainEventTable.COLUMN_TYPEID +"="+TypeInfoTable.TABLE_NAME+"."+ TypeInfoTable.COLUMN_ID+" ) as "+RegionInfoTable.COLUMN_EVENTCOUNT+
               " FROM " + TypeInfoTable.TABLE_NAME;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case TYPES:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, TypeInfoTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(TypeInfoTable.COLUMN_ID, contentValues.getAsLong(TypeInfoTable.COLUMN_ID));
                        inserter.bind(TypeInfoTable.COLUMN_NAME, contentValues.getAsString(TypeInfoTable.COLUMN_NAME));
                        inserter.bind(TypeInfoTable.COLUMN_PICTUREURL, contentValues.getAsString(TypeInfoTable.COLUMN_PICTUREURL));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("TypeInfoContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(TypeInfoContentProvider.CONTENT_URI, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
