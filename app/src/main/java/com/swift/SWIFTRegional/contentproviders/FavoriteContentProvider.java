package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.FavoriteTable;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

public class FavoriteContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.FavoriteContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_FAVORITE = Uri.parse("content://" + PROVIDER_NAME + "/favorite");

    private static final int FAVORITES = 1;
    private static final int FAVORITE = 2;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, FAVORITES);
        uriMatcher.addURI(PROVIDER_NAME, "favorite/#/*", FAVORITE);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case FAVORITES:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case FAVORITE:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(FavoriteTable.TABLE_NAME, null, values);
        final String type = values.getAsString(FavoriteTable.COLUMN_TYPE);

        this.notifyUri(type);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);


            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case FAVORITES:

                if (selectionArgs != null && selectionArgs.length > 1)
                {
                    this.notifyUri(selectionArgs[1]);
                }

                return this.database.delete(FavoriteTable.TABLE_NAME, selection, selectionArgs);
        }

        return 0;
    }

    // Notify the lists that one of their items changed favourite status.
    private void notifyUri(String favouriteType)
    {
        int intType = -1;
        try
        {
            intType = Integer.parseInt(favouriteType);
        }
        catch (Exception e)
        {
            Log.e("", "Invalid favourite type value.");
        }

        if (intType == FavoriteTable.FavouriteType.SPEAKER.ordinal())
        {
            this.getContext().getContentResolver().notifyChange(SpeakerContentProvider.CONTENT_URI, null);
        }
        else if (intType == FavoriteTable.FavouriteType.EXHIBITOR.ordinal())
        {
            this.getContext().getContentResolver().notifyChange(ExhibitorContentProvider.CONTENT_URI, null);
        }
        else if (intType == FavoriteTable.FavouriteType.DELEGATE.ordinal())
        {

        }
        else if (intType == FavoriteTable.FavouriteType.SESSION.ordinal() || intType == FavoriteTable.FavouriteType.EXHIBITOR_SESSION.ordinal())
        {
            // We notify the CATEGORY content provider here since that is the first one started in the SessionLoader.
            // Reloading categories will restart the entire SessionLoader chain of loaders.
            this.getContext().getContentResolver().notifyChange(CategoryContentProvider.CONTENT_URI, null);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String query;
        Cursor cursor;
        switch (uriMatcher.match(uri))
        {
            case FAVORITES:
                query = "SELECT * FROM " + FavoriteTable.TABLE_NAME;
                cursor = this.database.rawQuery(query, null);
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }
                return cursor;

            case FAVORITE:
                query = "SELECT * FROM " + FavoriteTable.TABLE_NAME + " WHERE " + FavoriteTable.COLUMN_TYPE + "=? AND " + FavoriteTable.COLUMN_FAVORITE_ID + "=?";
                final String favoriteType = uri.getPathSegments().get(1);
                final String favoriteId = uri.getPathSegments().get(2);
                cursor = this.database.rawQuery(query, new String[]{favoriteType, favoriteId});
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }
                return cursor;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public int bulkInsert(Uri uri, @NonNull ContentValues[] values)
    {
        int favoType = -1;

        switch (uriMatcher.match(uri))
        {
            case FAVORITES:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, FavoriteTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (ContentValues value : values)
                    {
                        contentValues = value;

                        inserter.prepareForInsert();
                        inserter.bind(FavoriteTable.COLUMN_TYPE, contentValues.getAsString(FavoriteTable.COLUMN_TYPE));
                        inserter.bind(FavoriteTable.COLUMN_FAVORITE_ID, contentValues.getAsString(FavoriteTable.COLUMN_FAVORITE_ID));

                        favoType = contentValues.getAsInteger(FavoriteTable.COLUMN_TYPE);

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("FavoriteContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(FavoriteContentProvider.CONTENT_URI, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
