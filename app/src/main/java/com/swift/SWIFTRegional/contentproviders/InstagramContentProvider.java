package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.InstagramTable;
import com.swift.SWIFTRegional.models.WallItem;
import com.swift.SWIFTRegional.models.instagram.Instagram;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

/**
 * Created by wesley on 24/06/14.
 */
public class InstagramContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.InstagramContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    public static final Uri CONTENT_URI_UPDATES = Uri.parse("content://" + PROVIDER_NAME + "/updates/");

    private static final int INSTAGRAMS = 1;
    private static final int INSTAGRAMS_UPDATES = 2;
    private static final int SEARCH_QUERY = 3;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, INSTAGRAMS);
        uriMatcher.addURI(PROVIDER_NAME, "updates", INSTAGRAMS_UPDATES);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH_QUERY);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case INSTAGRAMS:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case INSTAGRAMS_UPDATES:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;

        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case INSTAGRAMS:

                return this.database.delete(InstagramTable.TABLE_NAME, null, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;
        switch (uriMatcher.match(uri))
        {
            case INSTAGRAMS:

                cursor = this.database.query(InstagramTable.TABLE_NAME, null, null, null, null, null, InstagramTable.COLUMN_DATE + " DESC");

                break;

            case INSTAGRAMS_UPDATES:

                final long lastUpdate = UpdatePreferencesHelper.getLastUpdate(this.getContext(), UpdatePreferencesHelper.LastSeenType.INSTAGRAM);

                cursor = this.database.query(InstagramTable.TABLE_NAME, new String[] { "count(1) as count" }, InstagramTable.COLUMN_DATE + " > ?", new String[] { String.valueOf(lastUpdate) }, null, null, null);

                break;
            case SEARCH_QUERY:

                String[] searchParams = uri.getLastPathSegment().split(" ");

                StringBuilder query = new StringBuilder("Select * from ")
                    .append(InstagramTable.TABLE_NAME)
                    .append(" where ").append(InstagramTable.COLUMN_SEARCH_NORMALIZED)
                    .append(" like ?");

                for (Integer i = 0;  i< searchParams.length; i++) {
                    if(i >= 1) {
                        query.append(" and ").append(InstagramTable.COLUMN_SEARCH_NORMALIZED)
                                .append(" like ?");
                    }
                    searchParams[i] = "%" + searchParams[i] + "%";
                }

                cursor = database.rawQuery(query.toString(), searchParams);

                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if(cursor != null)
        {
            cursor.setNotificationUri(this.getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        int insertedRows = 0;
        ITPQueryHelper inserter;

        switch (uriMatcher.match(uri))
        {
            case INSTAGRAMS:

                inserter = new ITPQueryHelper(this.database, InstagramTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(InstagramTable.COLUMN_ID, contentValues.getAsString(InstagramTable.COLUMN_ID));
                        inserter.bind(InstagramTable.COLUMN_LINK, contentValues.getAsString(InstagramTable.COLUMN_LINK));
                        inserter.bind(InstagramTable.COLUMN_DESCRIPTION, contentValues.getAsString(InstagramTable.COLUMN_DESCRIPTION));
                        inserter.bind(InstagramTable.COLUMN_DATE, contentValues.getAsString(InstagramTable.COLUMN_DATE));
                        inserter.bind(InstagramTable.COLUMN_PICTURE_URL, contentValues.getAsString(InstagramTable.COLUMN_PICTURE_URL));
                        inserter.bind(InstagramTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(InstagramTable.COLUMN_SEARCH_NORMALIZED));

                        final Instagram.MediaType mediaType = Instagram.MediaType.values()[contentValues.getAsInteger(InstagramTable.COLUMN_TYPE)];
                        switch(mediaType)
                        {
                            case image:

                                inserter.bind(InstagramTable.COLUMN_TYPE, WallItem.Type.INSTAGRAM_IMAGE.ordinal());

                                break;
                            case video:

                                inserter.bind(InstagramTable.COLUMN_TYPE, WallItem.Type.INSTAGRAM_VIDEO.ordinal());

                                break;
                        }

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                } catch (Exception e)
                {
                    Log.d(this.getClass().getSimpleName(), e.getMessage());
                } finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);

                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI, null);
                    this.getContext().getContentResolver().notifyChange(WallContentProvider.CONTENT_URI_UPDATES, null);

                    this.getContext().getContentResolver().notifyChange(InstagramContentProvider.CONTENT_URI_UPDATES, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        return null;
    }
}
