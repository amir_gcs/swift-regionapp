package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.FlickrPhotosetsTable;
import com.swift.SWIFTRegional.database.tables.NewsTable;
import com.swift.SWIFTRegional.database.tables.TweetsTable;
import com.swift.SWIFTRegional.database.tables.VideoTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.swift.SWIFTRegional.database.tables.InstagramTable;
import com.swift.SWIFTRegional.database.tables.IssuesTable;
import com.swift.SWIFTRegional.database.tables.WallTable;
import com.swift.SWIFTRegional.models.twitter.Tweet;
import com.swift.SWIFTRegional.utils.preferences.UpdatePreferencesHelper;

public class WallContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.WallContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_UPDATES = Uri.parse("content://" + PROVIDER_NAME + "/updates/");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    private static final int WALL = 0;
    private static final int WALL_UPDATES = 1;
    private static final int SEARCH_QUERY = 2;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, WALL);
        uriMatcher.addURI(PROVIDER_NAME, "updates", WALL_UPDATES);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH_QUERY);
    }

    private SQLiteDatabase database;


    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case WALL:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case WALL_UPDATES:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        return null;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        return super.bulkInsert(uri, values);
    }

    private String getSearchQuery(String[] keywords,Boolean withWhere){
        StringBuilder query = new StringBuilder(withWhere? " where " : " and ")
                .append(WallTable.COLUMN_SEARCH_NORMALIZED)
                .append(" like ?");

        for (Integer i = 0;  i< keywords.length; i++) {
            if(i >= 1) {
                query.append(" and ").append(WallTable.COLUMN_SEARCH_NORMALIZED)
                        .append(" like ?");
            }
            keywords[i] = "%" + keywords[i] + "%";
        }

        return query.toString();
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        final Boolean isSearch = uriMatcher.match(uri) == SEARCH_QUERY;

        final StringBuilder query = new StringBuilder("");

        String searchQueryWithWhere = "";
        String searchQueryWithAnd = "";
        String[] keywords = new String[0];

        if(isSearch){
            keywords = uri.getLastPathSegment().split(" ");
            searchQueryWithWhere = getSearchQuery(keywords, true);
            searchQueryWithAnd = getSearchQuery(keywords, false);
        }

        query.append(" Select ")
                .append("_id, ")
                .append(TweetsTable.COLUMN_TYPE)
                .append(" AS ")
                .append(WallTable.COLUMN_TYPE)
                .append(", ")
                .append(TweetsTable.COLUMN_ID)
                .append(" AS ")
                .append(WallTable.COLUMN_ID)
                .append(", ")
                .append(TweetsTable.COLUMN_DATE)
                .append(" AS ")
                .append(WallTable.COLUMN_DATE)
                .append(", ")
                .append(TweetsTable.COLUMN_DESCRIPTION)
                .append(" AS ")
                .append(WallTable.COLUMN_DESCRIPTION)
                .append(", ")
                .append(TweetsTable.COLUMN_PICTURE_URL)
                .append(" AS ")
                .append(WallTable.COLUMN_PICTURE_URL)
                .append(", ")
                .append(TweetsTable.COLUMN_TITLE)
                .append(" AS ")
                .append(WallTable.COLUMN_TITLE)
                .append(", null AS ")
                .append(WallTable.COLUMN_LINK)
                .append(", ")
                .append(WallTable.COLUMN_SEARCH_NORMALIZED)
                .append(" AS ")
                .append(WallTable.COLUMN_SEARCH_NORMALIZED)
                .append(" FROM ")
                .append(TweetsTable.TABLE_NAME)
//                .append(" WHERE ")
//                .append(TweetsTable.COLUMN_TYPE)
//                .append(" = ? ")
//                .append(isSearch? searchQueryWithAnd : "")
                .append(isSearch? searchQueryWithWhere : "")

            .append(" UNION ALL ")

                .append("SELECT ")
                .append("_id, ")
                .append(VideoTable.COLUMN_TYPE)
                .append(", ")
                .append(VideoTable.COLUMN_ID)
                .append(", ")
                .append(VideoTable.COLUMN_DATE)
                .append(", null, ")
                .append(VideoTable.COLUMN_PICTURE_URL)
                .append(", ")
                .append(VideoTable.COLUMN_TITLE)
                .append(", ")
                .append(VideoTable.COLUMN_LINK)
                .append(", ")
                .append(WallTable.COLUMN_SEARCH_NORMALIZED)
                .append(" FROM ")
                .append(VideoTable.TABLE_NAME)
                .append(isSearch? searchQueryWithWhere : "");

        Cursor cursor = null;
        switch (uriMatcher.match(uri))
        {
            case WALL:

                query.append(" ORDER BY " + TweetsTable.COLUMN_DATE + " DESC");

//                cursor = this.database.rawQuery(query.toString(), new String[] { String.valueOf(Tweet.Type.TWITTER_NAME.ordinal()) });
                cursor = this.database.rawQuery(query.toString(), new String[] {});

                break;

            case WALL_UPDATES:

                final StringBuilder outerQuery = new StringBuilder("SELECT count(1) AS count FROM (")
                                                              .append(query)
                                                              .append(" ) WHERE ")
                                                              .append(WallTable.COLUMN_DATE)
                                                              .append(" > ?");

                final long lastUpdate = UpdatePreferencesHelper.getLastUpdate(this.getContext(), UpdatePreferencesHelper.LastSeenType.WALL);

                cursor = this.database.rawQuery(outerQuery.toString(), new String[] { String.valueOf(lastUpdate) });

                break;

            case SEARCH_QUERY:
                List<String> searchParams = new ArrayList<>();
//                searchParams.add(String.valueOf(Tweet.Type.TWITTER_NAME.ordinal()));
                for(Integer i = 0; i < 2; i++) {
                    searchParams.addAll(Arrays.asList(keywords));
                }

                cursor = database.rawQuery(query.toString(), searchParams.toArray(new String[0]));

                break;
        }

        if(cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }
}
