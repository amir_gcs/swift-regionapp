package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.CategoryTable;
import com.swift.SWIFTRegional.database.tables.SessionSpeakerTable;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.SpeakerTable;
import com.swift.SWIFTRegional.models.session.SessionSpeaker;

public class SessionSpeakerContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.SessionSpeakerContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_SESSION = Uri.parse("content://" + PROVIDER_NAME + "/session");

    private static final int SPEAKERS_IN_SESSION = 1;
    private static final int SPEAKERS_IN_SESSION_ID = 2;



    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, SPEAKERS_IN_SESSION);
        uriMatcher.addURI(PROVIDER_NAME, "session/#", SPEAKERS_IN_SESSION_ID);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case SPEAKERS_IN_SESSION:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case SPEAKERS_IN_SESSION_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(SessionSpeakerTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case SPEAKERS_IN_SESSION:

                return this.database.delete(SessionSpeakerTable.TABLE_NAME, selection, null);

            case SPEAKERS_IN_SESSION_ID:

                final String whereClause = SessionSpeakerTable.COLUMN_SESSION_ID + "='" + uri.getLastPathSegment() + "'";
                return this.database.delete(SessionSpeakerTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;



        String query = "SELECT " + SessionSpeakerTable.TABLE_PREFIX + "._id, "
                + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SPEAKER_ID + ", "
                + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SESSION_ID + ", "
                + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SPEAKER_TYPE + ", "
                + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SPEAKER_TYPENAME + ", "
                + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_IS_MODERATOR + ", "
                + SpeakerTable.TABLE_PREFIX + ".*";

        switch (uriMatcher.match(uri))
        {

            case SPEAKERS_IN_SESSION:
                query = query
                        // Append this to the where clause to include speaker pictures on a session detail view.
                        + " FROM " + SessionSpeakerTable.TABLE_NAME + " " + SessionSpeakerTable.TABLE_PREFIX

                        // Join Speakers
                        + " LEFT JOIN " + SpeakerTable.TABLE_NAME + " " + SpeakerTable.TABLE_PREFIX
                        + " ON "
                        + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SPEAKER_ID
                        + "="
                        + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_SPEAKER_ID

                        // Join delegates to ge the picture url.
                        + " order by " + SpeakerTable.COLUMN_LAST_NAME;
                break;

            case SPEAKERS_IN_SESSION_ID:

                query = query
                        // Append this to the where clause to include speaker pictures on a session detail view.
                        + " FROM " + SessionSpeakerTable.TABLE_NAME + " " + SessionSpeakerTable.TABLE_PREFIX

                        // Join Speakers
                        + " LEFT JOIN " + SpeakerTable.TABLE_NAME + " " + SpeakerTable.TABLE_PREFIX
                        + " ON "
                        + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SPEAKER_ID
                        + "="
                        + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_SPEAKER_ID

                        // For the requested session
                        + " WHERE " + SessionSpeakerTable.TABLE_PREFIX + "." + SessionSpeakerTable.COLUMN_SESSION_ID
                        + " = " + uri.getLastPathSegment();
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);

        }

        cursor = this.database.rawQuery(query, null);

        if (cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }



    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case SPEAKERS_IN_SESSION:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, SessionSpeakerTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(SessionSpeakerTable.COLUMN_SPEAKER_ID, contentValues.getAsString(SessionSpeakerTable.COLUMN_SPEAKER_ID));
                        inserter.bind(SessionSpeakerTable.COLUMN_SESSION_ID, contentValues.getAsString(SessionSpeakerTable.COLUMN_SESSION_ID));
                        inserter.bind(SessionSpeakerTable.COLUMN_SPEAKER_TYPE, contentValues.getAsString(SessionSpeakerTable.COLUMN_SPEAKER_TYPE));
                        inserter.bind(SessionSpeakerTable.COLUMN_SPEAKER_TYPENAME, contentValues.getAsString(SessionSpeakerTable.COLUMN_SPEAKER_TYPENAME));
                        inserter.bind(SessionSpeakerTable.COLUMN_IS_MODERATOR, contentValues.getAsBoolean(SessionSpeakerTable.COLUMN_IS_MODERATOR));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();



                }
                catch (Exception e)
                {
                    Log.d("SpeakersInSessionContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(SessionSpeakerContentProvider.CONTENT_URI, null);

                }




                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
