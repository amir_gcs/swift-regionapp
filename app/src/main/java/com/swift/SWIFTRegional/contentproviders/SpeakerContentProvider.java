package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.util.ArrayList;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.FavoriteTable;
import com.swift.SWIFTRegional.database.tables.SpeakerTable;

public class SpeakerContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.SpeakerContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_SPEAKER = Uri.parse("content://" + PROVIDER_NAME + "/speaker");
    public static final Uri CONTENT_URI_FAVORITES = Uri.parse("content://" + PROVIDER_NAME + "/favorites");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");

    private static final int SPEAKERS = 1;
    private static final int SPEAKER_ID = 2;
    private static final int FAVORITES = 3;
    private static final int FAVORITES_SEARCH = 4;
    private static final int SEARCH = 5;
    private static final int SEARCH_QUERY = 6;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, SPEAKERS);
        uriMatcher.addURI(PROVIDER_NAME, "speaker/*", SPEAKER_ID);
        uriMatcher.addURI(PROVIDER_NAME, "favorites", FAVORITES);
        uriMatcher.addURI(PROVIDER_NAME, "favorites/*", FAVORITES_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "search", SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH_QUERY);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case SPEAKERS:
            case FAVORITES:
            case FAVORITES_SEARCH:
            case SEARCH:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case SPEAKER_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(SpeakerTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case SPEAKERS:

                return this.database.delete(SpeakerTable.TABLE_NAME, selection, null);

            case SPEAKER_ID:

                final String speakerId = uri.getLastPathSegment();

                return this.database.delete(SpeakerTable.TABLE_NAME, SpeakerTable.COLUMN_SPEAKER_ID + " = ?", new String[]{speakerId});

        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor cursor;
        String query = "";
        String searchText = null;

        switch (uriMatcher.match(uri))
        {
            case SEARCH_QUERY:

                query = this.getSelectFrom()
                        + this.getFavouritesOuterJoin()
                        + " WHERE " + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_SEARCH_NORMALIZED + " LIKE ? ";

                selectionArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");
                for (int i = 0; i < selectionArgs.length; i++)
                {
                    if (i >= 1)
                    {
                        query += " AND " + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_SEARCH_NORMALIZED + " LIKE ? ";
                    }
                    selectionArgs[i] = "%" + selectionArgs[i] + "%";
                }

                query += " ORDER BY UPPER(" + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_LAST_NAME + ") ASC";

                cursor = this.database.rawQuery(query, selectionArgs);
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case FAVORITES_SEARCH:
                searchText = uri.getLastPathSegment();

                // Fall through.

            case FAVORITES:
                query = this.getSelectFrom()
                        + " JOIN "
                        + FavoriteTable.TABLE_NAME + " " + FavoriteTable.TABLE_PREFIX
                        + " ON "
                        + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_SPEAKER_ID
                        + "="
                        + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_FAVORITE_ID
                        + " WHERE " + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_TYPE
                        + "=?";

                ArrayList<String> args = new ArrayList<String>();
                args.add(String.valueOf(FavoriteTable.FavouriteType.SPEAKER.ordinal()));

                if (!TextUtils.isEmpty(searchText))
                {
                    selectionArgs = SibosUtils.getNormalizedText(searchText).split(" ");
                    for (int i = 0; i < selectionArgs.length; i++)
                    {
                        query += " AND " + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_SEARCH_NORMALIZED + " LIKE ? ";
                        args.add("%" + selectionArgs[i] + "%");
                    }
                }

                query += " ORDER BY UPPER(" + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_LAST_NAME + ") ASC";

                cursor = this.database.rawQuery(query, args.toArray(new String[args.size()]));
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case SEARCH:
            case SPEAKERS:

                // Select all speakers and include an extra field favourite_id if the speaker is a favourite
                query = this.getSelectFrom()
                        + this.getFavouritesOuterJoin();

                query += " ORDER BY UPPER(" + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_LAST_NAME + ") ASC";

                cursor = this.database.rawQuery(query, null);


                break;

            case SPEAKER_ID:

                final String speakerId = uri.getLastPathSegment();

                query = this.getSelectFrom()+ this.getFavouritesOuterJoin();
                query += " Where "+SpeakerTable.TABLE_PREFIX+"."+SpeakerTable.COLUMN_SPEAKER_ID +"="+speakerId;

                cursor = this.database.rawQuery(query, null);

                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (cursor != null)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    private String getSelectFrom()
    {
        return "SELECT "
                + SpeakerTable.TABLE_PREFIX + ".*,"
                + FavoriteTable.TABLE_PREFIX + ".*,"
                + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_PICTURE_URL + " as " + SpeakerTable.COLUMN_PICTURE_URL + "_sp "
                + " FROM " + SpeakerTable.TABLE_NAME + " " + SpeakerTable.TABLE_PREFIX;
    }

    // Join with favourites table to include an optional favouriteId field with every speaker record
    private String getFavouritesOuterJoin()
    {
        return " LEFT OUTER JOIN "
                + FavoriteTable.TABLE_NAME + " " + FavoriteTable.TABLE_PREFIX
                + " ON ("
                + SpeakerTable.TABLE_PREFIX + "." + SpeakerTable.COLUMN_SPEAKER_ID
                + " = "
                + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_FAVORITE_ID
                + " AND "
                + FavoriteTable.TABLE_PREFIX + "." + FavoriteTable.COLUMN_TYPE
                + " = "
                + FavoriteTable.FavouriteType.SPEAKER.ordinal()
                + ")";
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case SPEAKERS:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, SpeakerTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(SpeakerTable.COLUMN_SPEAKER_ID, contentValues.getAsString(SpeakerTable.COLUMN_SPEAKER_ID));
                        inserter.bind(SpeakerTable.COLUMN_SIBOSKEY, contentValues.getAsString(SpeakerTable.COLUMN_SIBOSKEY));
                        inserter.bind(SpeakerTable.COLUMN_SALUTATION, contentValues.getAsString(SpeakerTable.COLUMN_SALUTATION));
                        inserter.bind(SpeakerTable.COLUMN_TITLE, contentValues.getAsString(SpeakerTable.COLUMN_TITLE));
                        inserter.bind(SpeakerTable.COLUMN_FIRST_NAME, contentValues.getAsString(SpeakerTable.COLUMN_FIRST_NAME));
                        inserter.bind(SpeakerTable.COLUMN_LAST_NAME, contentValues.getAsString(SpeakerTable.COLUMN_LAST_NAME));
                        inserter.bind(SpeakerTable.COLUMN_EMAIL, contentValues.getAsString(SpeakerTable.COLUMN_EMAIL));
                        inserter.bind(SpeakerTable.COLUMN_COMPANY_NAME, contentValues.getAsString(SpeakerTable.COLUMN_COMPANY_NAME));
                        inserter.bind(SpeakerTable.COLUMN_BUSINESS_FUNCTION, contentValues.getAsString(SpeakerTable.COLUMN_BUSINESS_FUNCTION));
                        inserter.bind(SpeakerTable.COLUMN_COUNTRY, contentValues.getAsString(SpeakerTable.COLUMN_COUNTRY));
                        inserter.bind(SpeakerTable.COLUMN_BIOGRAPHY, contentValues.getAsString(SpeakerTable.COLUMN_BIOGRAPHY));
                        inserter.bind(SpeakerTable.COLUMN_PICTURE_URL, contentValues.getAsString(SpeakerTable.COLUMN_PICTURE_URL));

                        inserter.bind(SpeakerTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(SpeakerTable.COLUMN_SEARCH_NORMALIZED));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("SpeakerContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(SpeakerContentProvider.CONTENT_URI, null);

                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
