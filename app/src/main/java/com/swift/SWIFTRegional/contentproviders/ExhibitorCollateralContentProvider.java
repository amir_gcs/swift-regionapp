package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.DatabaseHelper;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.ExhibitorCollateralTable;

public class ExhibitorCollateralContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.ExhibitorCollateralContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_EXHIBITOR_COLLATERAL = Uri.parse("content://" + PROVIDER_NAME + "/exhibitorcollateral");
    public static final Uri CONTENT_URI_CCVK = Uri.parse("content://" + PROVIDER_NAME + "/ccvk");

    private static final int EXHIBITORSCOLLATERAL = 1;
    private static final int EXHIBITORCOLLATERAL_ID = 2;
    private static final int CCVK = 3;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, EXHIBITORSCOLLATERAL);
        uriMatcher.addURI(PROVIDER_NAME, "exhibitorcollateral/*", EXHIBITORCOLLATERAL_ID);
        uriMatcher.addURI(PROVIDER_NAME, "ccvk/*", CCVK);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case EXHIBITORSCOLLATERAL:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case CCVK:
            case EXHIBITORCOLLATERAL_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        final long rowID = this.database.replace(ExhibitorCollateralTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case EXHIBITORSCOLLATERAL:

                return this.database.delete(ExhibitorCollateralTable.TABLE_NAME, selection, null);

            case EXHIBITORCOLLATERAL_ID:

                final String whereClause = ExhibitorCollateralTable.COLUMN_ID + "='" + uri.getLastPathSegment() + "'";
                return this.database.delete(ExhibitorCollateralTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(ExhibitorCollateralTable.TABLE_NAME);

        switch (uriMatcher.match(uri))
        {
            case EXHIBITORSCOLLATERAL:

                break;

            case EXHIBITORCOLLATERAL_ID:

                qb.appendWhere(ExhibitorCollateralTable.COLUMN_ID + "='" + uri.getLastPathSegment() + "'");
                break;

            case CCVK:

                qb.appendWhere(ExhibitorCollateralTable.COLUMN_CCVK + "='" + uri.getLastPathSegment() + "'");
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (TextUtils.isEmpty(sortOrder))
        {
            sortOrder = ExhibitorCollateralTable.COLUMN_TITLE + " ASC";
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case EXHIBITORSCOLLATERAL:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, ExhibitorCollateralTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(ExhibitorCollateralTable.COLUMN_ID, contentValues.getAsString(ExhibitorCollateralTable.COLUMN_ID));
                        inserter.bind(ExhibitorCollateralTable.COLUMN_CCVK, contentValues.getAsString(ExhibitorCollateralTable.COLUMN_CCVK));
                        inserter.bind(ExhibitorCollateralTable.COLUMN_SEQUENCE, contentValues.getAsString(ExhibitorCollateralTable.COLUMN_SEQUENCE));
                        inserter.bind(ExhibitorCollateralTable.COLUMN_TITLE, contentValues.getAsString(ExhibitorCollateralTable.COLUMN_TITLE));
                        inserter.bind(ExhibitorCollateralTable.COLUMN_DESCRIPTION, contentValues.getAsString(ExhibitorCollateralTable.COLUMN_DESCRIPTION));
                        inserter.bind(ExhibitorCollateralTable.COLUMN_URL, contentValues.getAsString(ExhibitorCollateralTable.COLUMN_URL));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }
                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("ExhibitorCollateralContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(ExhibitorCollateralContentProvider.CONTENT_URI, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}