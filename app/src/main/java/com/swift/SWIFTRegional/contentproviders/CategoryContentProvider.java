package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.CategoryTable;
import com.swift.SWIFTRegional.database.tables.EventTable;
import com.swift.SWIFTRegional.database.tables.EventTimingTable;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;
import com.swift.SWIFTRegional.R;

public class CategoryContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.CategoryContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_CATEGORY_TYPE = Uri.parse("content://" + PROVIDER_NAME + "/type");

    private static final int CATEGORIES = 1;
    private static final int CATEGORY_ID = 2;
    private static final int CATEGORY_TYPE = 3;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, CATEGORIES);
        uriMatcher.addURI(PROVIDER_NAME, "category/#", CATEGORY_ID);
        uriMatcher.addURI(PROVIDER_NAME, "type/*", CATEGORY_TYPE);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case CATEGORIES:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;
            case CATEGORY_ID:
                return "vnd.android.cursor.item/" + PROVIDER_NAME;
            case CATEGORY_TYPE:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(CategoryTable.TABLE_NAME, null, values);
        // If record is added successfully
        if (rowID > 0)
        {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        String whereClause;
        switch (uriMatcher.match(uri))
        {
            case CATEGORIES:

                int res = this.database.delete(CategoryTable.TABLE_NAME, selection, selectionArgs);
                return res;
            case CATEGORY_ID:
                whereClause = CategoryTable.COLUMN_CATEGORY_ID + "='" + uri.getLastPathSegment() + "'";
                res = this.database.delete(CategoryTable.TABLE_NAME, whereClause, null);
                return res;
            case CATEGORY_TYPE:
                whereClause = CategoryTable.COLUMN_TYPE + "='" + uri.getLastPathSegment() + "'";
                res = this.database.delete(CategoryTable.TABLE_NAME, whereClause, null);
                return res;

        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String groupBy = null;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(CategoryTable.TABLE_NAME);

        switch (uriMatcher.match(uri))
        {
            case CATEGORIES:
                break;
            case CATEGORY_ID:
                qb.appendWhere(CategoryTable.COLUMN_CATEGORY_ID + "=" + uri.getPathSegments().get(1));
                break;
            case CATEGORY_TYPE:
                // Returns a list of categories matching the type and includes an extra "count(*)"
                // field indicating how many sessions fall under each category

                String columnName = uri.getLastPathSegment().equalsIgnoreCase("streams")
                        ? EventTable.COLUMN_STREAMS : EventTable.COLUMN_TAGS;


                qb.setTables(CategoryTable.TABLE_NAME +
                            " left join " + EventTable.TABLE_NAME + " on " +
                            EventTable.TABLE_NAME + "." + columnName +
                            " like '%' || " +
                            CategoryTable.TABLE_NAME + "." + CategoryTable.COLUMN_CATEGORY_ID +
                            " || '%' " + " join " + EventTimingTable.TABLE_NAME +
                            " on " + EventTimingTable.TABLE_NAME + "." + EventTimingTable.COLUMN_EVENT_ID +
                            " = " + EventTable.TABLE_NAME + "." + EventTable.COLUMN_EVENT_ID +
                            " and " + EventTimingTable.COLUMN_IS_REHEARSAL + " = 0 " );

                projection = new String[]{"categories.*", "count(*)"};
                groupBy = "categories.categoryId";
                selection = CategoryTable.COLUMN_TYPE + "='" + uri.getLastPathSegment() + "'" +
                        " AND " + EventTable.TABLE_NAME + "." + EventTable.COLUMN_TITLE +
                        " <> ''";

                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (sortOrder == null)
        {
            sortOrder = CategoryTable.COLUMN_NAME;
        }

        Cursor c = qb.query(this.database, projection, selection, selectionArgs, groupBy, null, sortOrder);
        // register to watch a content URI for changes
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case CATEGORIES:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, CategoryTable.TABLE_NAME);

                this.database.beginTransaction();

                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        String categoryType = contentValues.getAsString(CategoryTable.COLUMN_TYPE);
                        String categoryName = contentValues.getAsString(CategoryTable.COLUMN_NAME);

                        // dont add to streams table if stream is not configured to be displayed
                        if(categoryType.equalsIgnoreCase("streams") && !getStreamsToDisplay().contains("#" + categoryName + "#")){
                            continue;
                        }

                        inserter.prepareForInsert();
                        inserter.bind(CategoryTable.COLUMN_CATEGORY_ID, contentValues.getAsString(CategoryTable.COLUMN_CATEGORY_ID));
                        inserter.bind(CategoryTable.COLUMN_NAME, contentValues.getAsString(CategoryTable.COLUMN_NAME));

                        inserter.bind(CategoryTable.COLUMN_TYPE, contentValues.getAsString(CategoryTable.COLUMN_TYPE));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }
                    this.database.setTransactionSuccessful();

                }
                catch (Exception e)
                {
                    Log.d("CategoryContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(CategoryContentProvider.CONTENT_URI, null);
                }

                return insertedRows;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    private String streamsToDisplay;

    public String getStreamsToDisplay(){
        if(streamsToDisplay == null || streamsToDisplay.isEmpty()) {
            StringBuilder streamStr = new StringBuilder("#");
            String[] streams = this.getContext().getResources().getStringArray(R.array.streams);
            for (String stream : streams) {
                streamStr.append(stream).append("#");
            }
            streamsToDisplay = streamStr.toString();
        }
        return streamsToDisplay;
    }
}
