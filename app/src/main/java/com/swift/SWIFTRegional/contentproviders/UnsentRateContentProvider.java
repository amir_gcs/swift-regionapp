package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.RateTable;
import com.swift.SWIFTRegional.database.tables.UnsentRateTable;

public class UnsentRateContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.UnsentRateContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_UNSENT_SPEAKER_RATE = Uri.parse("content://" + PROVIDER_NAME + "/speaker");
    public static final Uri CONTENT_URI_UNSENT_SESSION_RATE = Uri.parse("content://" + PROVIDER_NAME + "/session");

    private static final int UNSENT_RATES = 1;
    private static final int UNSENT_SPEAKER_RATE = 2;
    private static final int UNSENT_SESSION_RATE = 3;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, UNSENT_RATES);
        uriMatcher.addURI(PROVIDER_NAME, "speaker/*", UNSENT_SPEAKER_RATE);
        uriMatcher.addURI(PROVIDER_NAME, "session/*", UNSENT_SESSION_RATE);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case UNSENT_RATES:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case UNSENT_SPEAKER_RATE:
            case UNSENT_SESSION_RATE:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {

        switch (uriMatcher.match(uri))
        {
            case UNSENT_RATES:
            {

                // delete record if it exists
                final String rateId = values.getAsInteger(UnsentRateTable.COLUMN_ID).toString();
                this.delete(Uri.withAppendedPath(uri, rateId), null, null);

                // Add record
                long rowID = this.database.replace(UnsentRateTable.TABLE_NAME, null, values);

                // If record is added successfully
                if (rowID > 0)
                {
                    final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
                    this.getContext().getContentResolver().notifyChange(_uri, null);

                    return _uri;
                }
                break;
            }
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case UNSENT_SPEAKER_RATE:

                final String speakerRateId = uri.getLastPathSegment();

                return this.database.delete(UnsentRateTable.TABLE_NAME,
                        UnsentRateTable.COLUMN_ID + " = ? AND " +
                                UnsentRateTable.COLUMN_TYPE + " = " + RateTable.RateType.SPEAKER.ordinal(),
                        new String[]{speakerRateId}
                );

            case UNSENT_SESSION_RATE:

                final String sessionRateId = uri.getLastPathSegment();

                return this.database.delete(UnsentRateTable.TABLE_NAME,
                        UnsentRateTable.COLUMN_ID + " = ? AND " +
                                UnsentRateTable.COLUMN_TYPE + " = " + RateTable.RateType.SESSION.ordinal(),
                        new String[]{sessionRateId}
                );

            case UNSENT_RATES:

                return this.database.delete(UnsentRateTable.TABLE_NAME, selection, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String query;
        Cursor cursor;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(UnsentRateTable.TABLE_NAME);

        switch (uriMatcher.match(uri))
        {
            case UNSENT_RATES:
                query = "SELECT * FROM " + UnsentRateTable.TABLE_NAME;
                cursor = this.database.rawQuery(query, null);
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }
                return cursor;

            case UNSENT_SESSION_RATE:

                qb.appendWhere(UnsentRateTable.COLUMN_ID + " = ? AND " + UnsentRateTable.COLUMN_TYPE + "=" + UnsentRateTable.RateType.SESSION.ordinal());
                selectionArgs = new String[] { uri.getLastPathSegment() };
                break;

            case UNSENT_SPEAKER_RATE:

                qb.appendWhere(UnsentRateTable.COLUMN_ID + " = ? AND " + UnsentRateTable.COLUMN_TYPE + "=" + UnsentRateTable.RateType.SPEAKER.ordinal());
                selectionArgs = new String[] { uri.getLastPathSegment() };
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        throw new IllegalArgumentException("Unsupported URI: " + uri);
    }
}
