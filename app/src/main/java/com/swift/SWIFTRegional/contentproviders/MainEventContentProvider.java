package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.activities.MainActivity;
import com.swift.SWIFTRegional.database.DatabaseHelper;
import com.swift.SWIFTRegional.database.tables.MainEventTable;
import com.swift.SWIFTRegional.database.tables.RegionInfoTable;
import com.swift.SWIFTRegional.database.tables.TypeInfoTable;
import com.swift.SWIFTRegional.models.event.Event;
import com.swift.SWIFTRegional.models.event.RegionInfo;
import com.swift.SWIFTRegional.models.event.TypeInfo;
import com.swift.SWIFTRegional.utils.SibosUtils;

import java.util.ArrayList;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.StringUtils;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

public class MainEventContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.MainEventContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_REGION = Uri.parse("content://" + PROVIDER_NAME + "/byregion");
    public static final Uri CONTENT_URI_TYPE = Uri.parse("content://" + PROVIDER_NAME + "/bytype");
    public static final Uri CONTENT_URI_MAINEVENT = Uri.parse("content://" + PROVIDER_NAME + "/mainevents");
    public static final Uri CONTENT_URI_SUGGESTED = Uri.parse("content://" + PROVIDER_NAME + "/suggested");
    public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + PROVIDER_NAME + "/search");


    private static final int EVENTS = 1;
    private static final int EVENT_ID = 2;
    private static final int BYREGION = 3;
    private static final int BYTYPE = 4;
    private static final int SUGGESTED = 5;
    private static final int SUGGESTED_SEARCH = 6;
    private static final int SEARCH = 7;

    public static String searchIn;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, EVENTS);
        uriMatcher.addURI(PROVIDER_NAME, "mainevents/*", EVENT_ID);
        uriMatcher.addURI(PROVIDER_NAME, "suggested", SUGGESTED);
        uriMatcher.addURI(PROVIDER_NAME, "suggested/*", SUGGESTED_SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "search/*", SEARCH);
        uriMatcher.addURI(PROVIDER_NAME, "byregion/*", BYREGION);
        uriMatcher.addURI(PROVIDER_NAME, "bytype/*", BYTYPE);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();
        MainActivity.selectedEventProvider=this;
        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case EVENTS:
            case SEARCH:
            case BYREGION:
            case BYTYPE:
            case SUGGESTED:
            case SUGGESTED_SEARCH:
                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case EVENT_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(MainEventTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {

        switch (uriMatcher.match(uri))
        {
            case EVENTS:

                return this.database.delete(MainEventTable.TABLE_NAME, selection, null);

            case EVENT_ID:

                String whereClause = MainEventTable.COLUMN_ID + "=" + uri.getLastPathSegment();
                return this.database.delete(MainEventTable.TABLE_NAME, whereClause, null);
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String query;
        Cursor cursor;
        String searchText = null;

        switch (uriMatcher.match(uri))
        {
            case SEARCH:

                query = this.getBaseQuery()
                        + " WHERE "
                        + MainEventTable.COLUMN_SEARCH_NORMALIZED
                        + " LIKE ?";

                selectionArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");
                for (int i = 0; i < selectionArgs.length; i++)
                {
                    if (i >= 1)
                    {
                        query += " AND " + MainEventTable.COLUMN_SEARCH_NORMALIZED + " LIKE ? ";
                    }
                    selectionArgs[i] = "%" + selectionArgs[i] + "%";
                }

                if(!StringUtils.isEmpty(searchIn))
                {
                    query+=searchIn;
                }

                query +=   " ORDER BY " + MainEventTable.COLUMN_STARTDATE + ","+MainEventTable.COLUMN_TITLE+" COLLATE NOCASE ASC";


                cursor = this.database.rawQuery(query, selectionArgs);

                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case BYREGION:

                qb.setTables(MainEventTable.TABLE_NAME);
                selectionArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");

                query = this.getBaseQuery()
                        + " WHERE "
                        + MainEventTable.COLUMN_REGIONID
                        + " = ?"
                        + " ORDER BY " + MainEventTable.COLUMN_STARTDATE + " COLLATE NOCASE ASC";

                cursor = this.database.rawQuery(query, selectionArgs);
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case BYTYPE:

                qb.setTables(MainEventTable.TABLE_NAME);
                selectionArgs = SibosUtils.getNormalizedText(uri.getLastPathSegment()).split(" ");

                query = this.getBaseQuery()
                        + " WHERE "
                        + MainEventTable.COLUMN_TYPEID
                        + " = ?"
                        + " ORDER BY " + MainEventTable.COLUMN_STARTDATE + " COLLATE NOCASE ASC";

                cursor = this.database.rawQuery(query, selectionArgs);
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case EVENTS:
                qb.setTables(MainEventTable.TABLE_NAME);

                query = this.getBaseQuery();

                query += " ORDER BY " + MainEventTable.COLUMN_STARTDATE + " COLLATE NOCASE ASC";


                ArrayList<String> argsa = new ArrayList<String>();

                cursor = this.database.rawQuery(query, argsa.toArray(new String[argsa.size()]));
                if (cursor != null)
                {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }

                return cursor;

            case EVENT_ID:
                qb.setTables(MainEventTable.TABLE_NAME);
                qb.appendWhere(MainEventTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (TextUtils.isEmpty(sortOrder))
        {
            sortOrder = "UPPER(" + MainEventTable.COLUMN_STARTDATE + ") ASC";
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    /**
     * Returns a query that will join with the favourites table to include the extra isFavourite field in the result.
     */
    private String getBaseQuery()
    {
        return "SELECT * FROM "
                + MainEventTable.TABLE_NAME;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case EVENTS:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, MainEventTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(MainEventTable.COLUMN_ID, contentValues.getAsLong(MainEventTable.COLUMN_ID));
                        inserter.bind(MainEventTable.COLUMN_TYPEID, contentValues.getAsLong(MainEventTable.COLUMN_TYPEID));
                        inserter.bind(MainEventTable.COLUMN_REGIONID, contentValues.getAsString(MainEventTable.COLUMN_REGIONID));
                        inserter.bind(MainEventTable.COLUMN_TITLE, contentValues.getAsString(MainEventTable.COLUMN_TITLE));
                        inserter.bind(MainEventTable.COLUMN_DESCRIPTION, contentValues.getAsString(MainEventTable.COLUMN_DESCRIPTION));
                        inserter.bind(MainEventTable.COLUMN_STARTDATE, contentValues.getAsString(MainEventTable.COLUMN_STARTDATE));
                        inserter.bind(MainEventTable.COLUMN_SELECTED, contentValues.getAsInteger(MainEventTable.COLUMN_SELECTED));
                        inserter.bind(MainEventTable.COLUMN_PICTUREURL, contentValues.getAsString(MainEventTable.COLUMN_PICTUREURL));
                        inserter.bind(MainEventTable.COLUMN_ENDDATE, contentValues.getAsString(MainEventTable.COLUMN_ENDDATE));
                        inserter.bind(MainEventTable.COLUMN_LOCATION, contentValues.getAsString(MainEventTable.COLUMN_LOCATION));
                        inserter.bind(MainEventTable.COLUMN_SEARCH_NORMALIZED, contentValues.getAsString(MainEventTable.COLUMN_SEARCH_NORMALIZED));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("MainEventContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                    this.getContext().getContentResolver().notifyChange(MainEventContentProvider.CONTENT_URI, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    public void selectEvent(long eventId)
    {
        this.database.beginTransaction();
        ContentValues cv=new ContentValues();
        cv.put(MainEventTable.COLUMN_SELECTED,0);
        this.database.update(MainEventTable.TABLE_NAME,cv,null,null);

        cv=new ContentValues();
        cv.put(MainEventTable.COLUMN_SELECTED,1);
        this.database.update(MainEventTable.TABLE_NAME,cv,MainEventTable.COLUMN_ID+"="+eventId,null);

    this.database.setTransactionSuccessful();
        this.database.endTransaction();

    }

    public boolean isEventSelected() {

        String q = getBaseQuery() + " Where "+MainEventTable.COLUMN_SELECTED+"=1";

        Cursor c = this.database.rawQuery(q,null);
        if(c.getCount()>0)
        {
            c.close();
            return  true;
        }
c.close();
        return false;
    }

    public Event getSelectedEvent() {

        String q = getBaseQuery() + " Where "+MainEventTable.COLUMN_SELECTED+"=1";

        Cursor c = this.database.rawQuery(q,null);
        if(c.getCount()>0)
        {
            Event selectedEvent=new Event();
            if (c.moveToFirst()){
                selectedEvent.constructFromCursor(c);
                //get the type and region also
                String qRegion= "Select * From "+ RegionInfoTable.TABLE_NAME+" Where "+RegionInfoTable.COLUMN_ID+"="+selectedEvent.getRegionId();
                Cursor cRegion = this.database.rawQuery(qRegion,null);
                if(cRegion.moveToFirst())
                {
                    RegionInfo r = new RegionInfo();
                    r.constructFromCursor(cRegion);
                    selectedEvent.setRegionName(r.getName());
                }

                //get the Type
                String qType= "Select * From "+ TypeInfoTable.TABLE_NAME+" Where "+TypeInfoTable.COLUMN_ID+"="+selectedEvent.getTypeId();
                Cursor cType = this.database.rawQuery(qType,null);
                if(cType.moveToFirst())
                {
                    TypeInfo r = new TypeInfo();
                    r.constructFromCursor(cType);
                    selectedEvent.setTypeName(r.getName());
                }

c.close();
                return selectedEvent;
            }

        }

        c.close();
        return null;
    }

    public void fillEventInfo(Event event) {

        String qRegion= "Select * From "+ RegionInfoTable.TABLE_NAME+" Where "+RegionInfoTable.COLUMN_ID+"="+event.getRegionId();
        Cursor cRegion = this.database.rawQuery(qRegion,null);
        if(cRegion.moveToFirst())
        {
            RegionInfo r = new RegionInfo();
            r.constructFromCursor(cRegion);
            event.setRegionName(r.getName());
        }

        //get the Type
        String qType= "Select * From "+ TypeInfoTable.TABLE_NAME+" Where "+TypeInfoTable.COLUMN_ID+"="+event.getTypeId();
        Cursor cType = this.database.rawQuery(qType,null);
        if(cType.moveToFirst())
        {
            TypeInfo r = new TypeInfo();
            r.constructFromCursor(cType);
            event.setTypeName(r.getName());
        }

        cType.close();
        cRegion.close();

    }

    public RegionInfo getRegionInfo(String regionId) {


        String qRegion= "Select * From "+ RegionInfoTable.TABLE_NAME+" Where "+RegionInfoTable.COLUMN_ID+"="+regionId;
        Cursor cRegion = this.database.rawQuery(qRegion,null);
        if(cRegion.moveToFirst())
        {
            RegionInfo r = new RegionInfo();
            r.constructFromCursor(cRegion);
            cRegion.close();
            return r;
        }

        cRegion.close();
        return null;
    }

    public TypeInfo getTypeInfo(String typeId) {
        String qType= "Select * From "+ TypeInfoTable.TABLE_NAME+" Where "+TypeInfoTable.COLUMN_ID+"="+typeId;
        Cursor cType = this.database.rawQuery(qType,null);
        if(cType.moveToFirst())
        {
            TypeInfo r = new TypeInfo();
            r.constructFromCursor(cType);
            cType.close();
            return  r;
        }
        cType.close();
        return  null;

    }
}
