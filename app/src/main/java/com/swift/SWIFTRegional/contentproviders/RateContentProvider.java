package com.swift.SWIFTRegional.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.swift.SWIFTRegional.database.DatabaseHelper;

import com.goodcoresoftware.android.common.utils.Log;
import com.goodcoresoftware.android.common.utils.database.ITPQueryHelper;

import com.swift.SWIFTRegional.database.tables.RateTable;

public class RateContentProvider extends ContentProvider
{
    public static final String PROVIDER_NAME = "com.swift.SWIFTRegional.contentproviders.RateContentProvider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/");
    public static final Uri CONTENT_URI_SESSION = Uri.parse("content://" + PROVIDER_NAME + "/session");
    public static final Uri CONTENT_URI_SPEAKER = Uri.parse("content://" + PROVIDER_NAME + "/speaker");

    private static final int RATES = 1;
    private static final int SESSION_ID = 2;
    private static final int SPEAKER_ID = 3;

    private static UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, null, RATES);
        uriMatcher.addURI(PROVIDER_NAME, "session/*", SESSION_ID);
        uriMatcher.addURI(PROVIDER_NAME, "speaker/*", SPEAKER_ID);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate()
    {
        final Context context = this.getContext();
        final DatabaseHelper helper = new DatabaseHelper(context);
        this.database = helper.getWritableDatabase();

        return this.database != null;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case RATES:

                return "vnd.android.cursor.dir/" + PROVIDER_NAME;

            case SESSION_ID:
            case SPEAKER_ID:

                return "vnd.android.cursor.item/" + PROVIDER_NAME;
        }

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        int count;

        switch (uriMatcher.match(uri)){

            case SESSION_ID:
            {
                final String whereClause = RateTable.COLUMN_ID + "='" + uri.getLastPathSegment()
                        + "'" + RateTable.COLUMN_TYPE + "=" + RateTable.RateType.SESSION.ordinal()
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");

                count = database.update(RateTable.TABLE_NAME, values, whereClause, selectionArgs);

                break;
            }

            case SPEAKER_ID:
            {
                final String whereClause = RateTable.COLUMN_ID + "='" + uri.getLastPathSegment()
                        + "'" + RateTable.COLUMN_TYPE + "=" + RateTable.RateType.SPEAKER.ordinal()
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");

                count = database.update(RateTable.TABLE_NAME, values, whereClause, selectionArgs);

                break;
            }

            default:

                throw new IllegalArgumentException("Unsupported URI " + uri );

        }

        getContext().getContentResolver().notifyChange(CONTENT_URI, null);

        return count;

    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Add record
        long rowID = this.database.replace(RateTable.TABLE_NAME, null, values);

        // If record is added successfully
        if (rowID > 0)
        {
            final Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            this.getContext().getContentResolver().notifyChange(CONTENT_URI, null);

            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        switch (uriMatcher.match(uri))
        {
            case RATES:

                return this.database.delete(RateTable.TABLE_NAME, selection, null);

            case SESSION_ID:
            {
                final String whereClause = RateTable.COLUMN_ID + "='" + uri.getLastPathSegment()
                        + "'" + RateTable.COLUMN_TYPE + "=" + RateTable.RateType.SESSION.ordinal()
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");

                return this.database.delete(RateTable.TABLE_NAME, whereClause, null);
            }

            case SPEAKER_ID:
            {
                final String whereClause = RateTable.COLUMN_ID + "='" + uri.getLastPathSegment()
                        + "'" + RateTable.COLUMN_TYPE + "=" + RateTable.RateType.SPEAKER.ordinal()
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");

                return this.database.delete(RateTable.TABLE_NAME, whereClause, null);
            }
        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(RateTable.TABLE_NAME);

        switch (uriMatcher.match(uri))
        {
            case RATES:

                break;

            case SESSION_ID:

                qb.appendWhere(RateTable.COLUMN_ID + " = ? AND " + RateTable.COLUMN_TYPE + "=" + RateTable.RateType.SESSION.ordinal());
                selectionArgs = new String[] { uri.getLastPathSegment() };
                break;

            case SPEAKER_ID:

                qb.appendWhere(RateTable.COLUMN_ID + " = ? AND " + RateTable.COLUMN_TYPE + "=" + RateTable.RateType.SPEAKER.ordinal());
                selectionArgs = new String[] { uri.getLastPathSegment() };
                break;

            default:

                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        final Cursor c = qb.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        switch (uriMatcher.match(uri))
        {
            case RATES:

                int insertedRows = 0;
                ITPQueryHelper inserter = new ITPQueryHelper(this.database, RateTable.TABLE_NAME);

                this.database.beginTransaction();
                try
                {
                    ContentValues contentValues;

                    for (int i = 0; i < values.length; i++)
                    {
                        contentValues = values[i];

                        inserter.prepareForInsert();
                        inserter.bind(RateTable.COLUMN_ID, contentValues.getAsLong(RateTable.COLUMN_ID));
                        inserter.bind(RateTable.COLUMN_TYPE, contentValues.getAsInteger(RateTable.COLUMN_TYPE));
                        inserter.bind(RateTable.COLUMN_RATE, contentValues.getAsInteger(RateTable.COLUMN_RATE));

                        long rowId = inserter.execute();

                        if (rowId != -1)
                        {
                            insertedRows++;
                        }
                    }

                    this.database.setTransactionSuccessful();
                }
                catch (Exception e)
                {
                    Log.d("RateContentProvider", e.getMessage());
                }
                finally
                {
                    this.database.endTransaction();
                    inserter.close();
                }

                if (insertedRows > 0)
                {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                }

                return insertedRows;

            default:

                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}