SIBOS
===

## Building the project

* Add the dependencies in the directory 'maven_artifacts' to your maven repository.
* Add a reference to your maven repository in the topmost build.gradle.
* Place your own keystore in the folder 'keystore'.
* Adapt 'sibos/build.gradle' to refer to this keystore.
* Adapt the version number in gradle.properties if desired.

## Converting images for tiling using ImageMagick

fullsize.png is the full resolution input image to 512x512 tiles

    convert fullsize.png -resize 50%% map500.png
    convert fullsize.png -resize 25%% map250.png
    convert fullsize.png -resize 12.5%% map125.png

    mkdir downsamples

    convert fullsize.png -resize 1000x1000 ./downsamples/map.png

    mkdir tiles

    convert map125.png -crop 512x512 -set filename:tile "%[fx:page.x/512]_%[fx:page.y/512]" +repage +adjoin "./tiles2/125_%[filename:tile].png"
    convert map250.png -crop 512x512 -set filename:tile "%[fx:page.x/512]_%[fx:page.y/512]" +repage +adjoin "./tiles2/250_%[filename:tile].png"
    convert map500.png -crop 512x512 -set filename:tile "%[fx:page.x/512]_%[fx:page.y/512]" +repage +adjoin "./tiles2/500_%[filename:tile].png"
    convert fullsize.png -crop 512x512 -set filename:tile "%[fx:page.x/512]_%[fx:page.y/512]" +repage +adjoin "./tiles2/1000_%[filename:tile].png"